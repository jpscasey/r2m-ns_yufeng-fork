#!/usr/bin/env bash
# This script adds authentication for trimble-rail-maven repository.

cat << EOF > $HOME/.m2/settings.xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
    https://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>trimble-rail-maven</id>
            <username>$AWS_ACCESS_KEY_ID</username>
            <password>$AWS_SECRET_ACCESS_KEY</password>
        </server>
    </servers>
</settings>
EOF
