import datetime as dt
import random
import pypyodbc

rollingStockStartNumber = 1
numberOfRollingStockUnits = 700
numberOfMeasurements = 100
duration = 3600*24

startTime = dt.datetime.now()

sqlList = []
for time in range(0,duration):
    pointInTime = startTime + dt.timedelta(0,time)
    pointInTimeStr = pointInTime.strftime("%Y-%m-%dT%H:%M:%S.000")
    for rollingStockUnit in range(0,numberOfRollingStockUnits):
        colList = 'INSERT INTO PerformanceTestData (Timestamp, UnitID'
        valList = ') VALUES (\'' + pointInTimeStr + '\', (SELECT ID FROM Unit WHERE UnitNumber = \'PERF' + str(rollingStockUnit+rollingStockStartNumber) + '\')'

        for measurement in range(0,numberOfMeasurements):
            colList += ', '
            valList += ', '

            colList += 'col' + str(measurement + 1)
            valList += str(random.randint(-100, 100))
            
        sql = colList + valList + ');\n'
        sqlList.append(sql)
        
    connSqlServer = pypyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=nedtrainDevSpectrum;UID=nedtraindbadmin;PWD=om46MBHAti39i')
    cursor = connSqlServer.cursor()
    cursor.execute(''.join(sqlList))
    connSqlServer.commit()
    connSqlServer.close()
    sqlList = []

    if time % 60 == 0:
        print('%s - Records inserted for last minute' % pointInTimeStr)
