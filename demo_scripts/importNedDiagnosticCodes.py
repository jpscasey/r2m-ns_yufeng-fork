import csv

# Read rolling stock csv, and create an sql file with all the insert statements for loading into the database
# header=['FaultCode', 'Priority', 'Class', 'Urgence', 'Description', 'advice']

print('Create Fault Meta SQL File')

wfile = open("loadFaultMeta.sql","wt")
rfile = open("colors.csv","rt")
reader = csv.reader(rfile, delimiter=';')

colors = {'Rood': '#CC0000','Oranje': '#FF6D05','Paars': '#CC99FF','Blauw': '#0095FF', 'Groen': '#239023'}

print('Reading colors csv...')
rowNumber = 0
for row in reader:
    if rowNumber > 0:
        priority = row[0]
        color = colors[row[1]]
        
        wfile.write('INSERT INTO FaultType (Name, DisplayColor, Priority) VALUES (\'Priority ' + priority +'\', \'' + color + '\', ' + priority + ')\n')
    rowNumber += 1
rfile.close()

rfile = open("diagnosticmessagesMasterData.csv","rt")
reader = csv.reader(rfile, delimiter=';')

faultCategories = set()

print('Reading diagnosticmessagesMasterData csv...')
rowNumber = 0
for row in reader:
    if rowNumber > 0:
        code = row[0]
        priority = row[1]
        category = 'NA' if len(row[2]) == 0 else row[2].upper()
        urgency = row[3]
        description = row[4].replace('\'', '\'\'')
        
        wfile.write('INSERT INTO FaultMeta (FaultCode, Description, CategoryID, FaultTypeID, AdditionalInfo) VALUES (\'' + code +'\', \'' + description + '\', ')
        wfile.write('(SELECT ID FROM FaultCategory WHERE Category = \'' + category + '\'), (SELECT ID FROM FaultType WHERE Priority = \'' + priority + '\'), \'Urgence: ' +  urgency + '\')\n')
        
        faultCategories.add(category)
    if rowNumber % 100 == 0:
        print(str(rowNumber) + ' Diagnostic Message lines read')
    rowNumber += 1
print(str(rowNumber) + ' Diagnostic Message lines read')
rfile.close()

for category in faultCategories:
    wfile.write('INSERT INTO FaultCategory (Category, ReportingOnly) VALUES (\'' + category + '\', 0)\n')
wfile.close()

print('Rolling Stock SQL File completed')