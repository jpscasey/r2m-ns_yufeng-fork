import csv
import datetime
import pypyodbc

class OccurredEvent(object):
    def __init__(self, stockid, date):
        self.stockid = stockid
        self.date = date
        self.channels = dict()
    
    def addChannel(self, channelid, value):
        self.channels[channelid] = value

eventsListFromFile = []
eventsListMerged = []
        
#################################################
## OCCURED DIAGNOSTIC MESSAGES / INSERT FAULTS ##
#################################################
rfile = open("occurred_diagnosticmessages.csv","rt")
reader = csv.reader(rfile, delimiter=';')
wfile = open("insertFault.sql","wt")

rownum = 0
for row in reader:
    if rownum > 0: # Skip header
        stockid = row[1]
        faultcode = row[2]
        startDate = None if len(row[3]) == 0 or row[3].upper() == 'NA' else datetime.datetime.strptime(row[3], "%Y-%m-%dT%H:%M:%S")
        endDate = None if len(row[4]) == 0 or row[4].upper() == 'NA' else datetime.datetime.strptime(row[4], "%Y-%m-%dT%H:%M:%S")
        longitude = row[8].replace(',','.')
        latitude = row[9].replace(',','.')
        speed = '%.2f' % float(row[10].replace(',','.'))
        gpsValid = row[11]
        
        wfile.write('EXEC NSP_InsertFaultInterface \'' + faultcode + '\', ' + ('NULL' if startDate is None else ('\'' + startDate.strftime('%Y-%m-%d %H:%M:%S')) + '\'') + ', ' + ('NULL' if endDate is None else ('\'' + endDate.strftime('%Y-%m-%d %H:%M:%S')) + '\'') + ', \'' + stockid + '\'\n')
        
        if gpsValid.upper() == 'TRUE':
            gpsEvent = OccurredEvent(stockid, startDate)
            gpsEvent.addChannel(1, latitude)
            gpsEvent.addChannel(2, longitude)
            gpsEvent.addChannel(3, speed)
            eventsListFromFile.append(gpsEvent)
        
    if rownum % 100 == 0:
        print(str(rownum) + ' Diagnostic Message lines read')
    rownum += 1
    
print('All ' + str(rownum) + ' Diagnostic Message lines read')
wfile.close()
rfile.close()

############################################
## OCCURED EVENTS / INSERT CHANNEL VALUES ##
############################################
# Step 1 - Read from file
rfile = open("occurred_events.csv","rt")
reader = csv.reader(rfile, delimiter=';')

rownum = 0
for row in reader:
    if rownum > 0:
        stockid = row[1]
        channelid = int(row[2])
        date = datetime.datetime.strptime(row[4], "%Y-%m-%dT%H:%M:%S")
        
        currentEvent = OccurredEvent(stockid, date)
        currentEvent.addChannel(int(channelid) + 4, '1')
        eventsListFromFile.append(currentEvent)
        
    if rownum % 1000 == 0:
        print(str(rownum) + ' Occured Events lines read')
    rownum += 1
    
print('All ' + str(rownum) + ' Occured Events lines read')
rfile.close()

# Step 2 - Sort Events from File by Date and Stock
eventsListFromFile.sort(key=lambda event: (event.date, event.stockid))

lastEvent = None
rownum = 0
for event in eventsListFromFile:
    # If this event matches the same date and stock as the last, merge channels together
    if lastEvent is not None and lastEvent.date == event.date and lastEvent.stockid == event.stockid:
        for key, value in event.channels.items():
            lastEvent.addChannel(key, value)
    else:
        eventsListMerged.append(event)
        lastEvent = event
    
    if rownum % 1000 == 0:
        print(str(rownum) + ' events merged')
    rownum += 1
    
print('All ' + str(rownum) + ' events merged')

fileNum = 1
#wfile = open("insertChannelData" + str(fileNum) + ".sql","wt")

connSqlServer = pypyodbc.connect('DRIVER={SQL Server};SERVER=52.212.104.96;DATABASE=nedtrainDevSpectrum;UID=nedtraindbadmin;PWD=om46MBHAti39i')
cursor = connSqlServer.cursor()

# Used to keep the same value in a channel until we get a event that says to switch
unitExtraChannels = dict()

rownum = 0
for event in eventsListMerged:
    if unitExtraChannels.get(event.stockid) is None:
        unitExtraChannels[event.stockid] = ['NULL','NULL','NULL', '3.67', '3.51']

    columnListStr = 'UnitID, RecordInsert, TimeStamp, '
    valueListStr = '(SELECT ID FROM Unit WHERE UnitNumber = \'' + event.stockid + '\'), GETDATE(), \'' + event.date.strftime('%Y-%m-%d %H:%M:%S') + '\', '
    maxColumnNumber = 383;
    
    if 9 in event.channels:
        unitExtraChannels[event.stockid][0] = '1'
    elif 10 in event.channels:
        unitExtraChannels[event.stockid][1] = '1'
    elif 11 in event.channels:
        unitExtraChannels[event.stockid][2] = '1'
    elif 12 in event.channels:
        unitExtraChannels[event.stockid][1] = '0'
    elif 13 in event.channels:
        unitExtraChannels[event.stockid][0] = '0'
    elif 14 in event.channels:
        unitExtraChannels[event.stockid][2] = '0'
    elif 25 in event.channels:
        unitExtraChannels[event.stockid][3] = '4.87'
    elif 26 in event.channels:
        unitExtraChannels[event.stockid][4] = '4.91'
    elif 27 in event.channels:
        unitExtraChannels[event.stockid][3] = '2.47'
    elif 28 in event.channels:
        unitExtraChannels[event.stockid][4] = '2.31'

    event.addChannel(379, unitExtraChannels[event.stockid][0])
    event.addChannel(380, unitExtraChannels[event.stockid][1])
    event.addChannel(381, unitExtraChannels[event.stockid][2])
    event.addChannel(382, unitExtraChannels[event.stockid][3])
    event.addChannel(383, unitExtraChannels[event.stockid][4])
    
    for i in range(1, maxColumnNumber + 1):
        if i > 1:
            columnListStr += ', '
            valueListStr += ', '

        columnListStr += 'Col' + str(i)
        if i in event.channels:
            valueListStr += event.channels[i]
        else:
            valueListStr += '0'

    #wfile.write('INSERT INTO ChannelValue (' + columnListStr + ') VALUES (' + valueListStr + ')\n')
    try:
        sql = 'INSERT INTO ChannelValue (' + columnListStr + ') VALUES (' + valueListStr + ')'
        cursor.execute(sql)
        connSqlServer.commit()
    except:
        print('Error when inserting line ' + str(rownum + 1))

    if rownum > 0 and rownum % 1000 == 0:
        print(str(rownum) + ' inserts attempted')
    if rownum > 0 and rownum % 1000 == 0:
        fileNum += 1
        #wfile.close()
        #wfile = open("insertChannelData" + str(fileNum) + ".sql","wt")
    rownum += 1

connSqlServer.close()
#wfile.close()
print('All ' + str(rownum) + ' inserts attempted')

print('done')
