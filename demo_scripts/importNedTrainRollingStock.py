import csv

# Read rolling stock csv, and create an sql file with all the insert statements for loading into the database
# header=['StockId', 'StockSeries', 'StockType', 'SoftwareVersion', '', '']

print('Create Rolling Stock SQL File')

wfile = open("loadUnits.sql","wt")
rfile = open("rollingStockIds.csv","rt")
reader = csv.reader(rfile, delimiter=';')

fleets = set()

print('Reading rollingstock csv...')
rowNumber = 0
for row in reader:
    if rowNumber > 0:
        wfile.write('INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES (\'' + row[0] +'\', \'' + row[2] + '\', 1, (SELECT ID FROM Fleet WHERE Code = \'' + row[1] + '\'))\n')
        fleets.add(row[1])
    if rowNumber % 100 == 0:
        print(str(rowNumber) + ' Rolling Stock lines read')
    rowNumber += 1
print(str(rowNumber) + ' Rolling Stock lines read')
rfile.close()

for fleet in fleets:
    wfile.write('INSERT INTO Fleet (Name, Code) VALUES (\'' + fleet + '\', \'' + fleet + '\')\n')
wfile.close()
print('Rolling Stock SQL File completed')