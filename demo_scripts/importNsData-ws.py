import cherrypy
import pypyodbc
import pymssql
import threading
import time
import queue
import ujson
import pymssql
import pytds

dQueues = [queue.Queue(), queue.Queue()]
bgtasks = []

def connect(thread_index): 
    conn = pypyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=nedtrainDevSpectrum;UID=nedtraindbadmin;PWD=om46MBHAti39i', autocommit=False)
    #conn = pytds.connect('localhost', 'nedtrainDevSpectrum', 'nedtraindbadmin', 'om46MBHAti39i')
    #conn = pymssql.connect('localhost', 'nedtraindbadmin', 'om46MBHAti39i', "nedtrainDevSpectrum")
    return conn

def disconnect(connection): 
    connection.close()

    
class BackgroundQueueProcessor():
    
    thread = None
    
    def __init__(self, q, qwait=0.5, safe_stop=True):
        self.q = q
        self.qwait = qwait
        #self.query = "insert into PerformanceTestData values(?, ?, %s)" % (
        #    ', '.join(['?' for x in range(1, 101)])
        #)
        self.query = "insert into PerformanceTestData(TimeStamp, UnitId, %s) values(?, (SELECT ID FROM Unit WHERE UnitNumber = ?), %s)" % (
            ', '.join(['col' + str(x) for x in range(1, 101)]),
            ', '.join(['?' for x in range(1, 101)])
        )
        self.safe_stop = safe_stop
        
    def start(self):
        self.running = True
        self.connection = connect(None)
        if not self.thread:
            self.thread = threading.Thread(target=self.run)
            self.thread.start()
    
    def stop(self):
        if self.safe_stop:
            self.running = "draining"
        else:
            self.running = False
        
        if self.thread:
            self.thread.join()
            self.thread = None
        self.running = False
        disconnect(self.connection)
    
    def run(self):
        while self.running:
            try:
                temp = []
                while not self.q.empty():
                    temp.append(self.q.get())

                if len(temp) > 0:
                    self.connection.cursor().executemany(self.query, temp)
                    self.connection.commit()
                    
            except Exception as e:
                print(e)
                pass
            
            time.sleep(self.qwait)


for q in dQueues:
    bgtask = BackgroundQueueProcessor(q)
    bgtasks.append(bgtask)

for bgtask in bgtasks:
    bgtask.start()
    
class NedtrainLoad(object):
    @cherrypy.expose
    #@cherrypy.tools.json_in()
    def index(self):
        body = cherrypy.request.body.read().decode('utf-8')
        nedtrainData = ujson.loads(body)
        #nedtrainData = cherrypy.request.json

        parameterList = []
        dataDate = nedtrainData['Materieeldata']['Meetwaarden']['datetimeMeasure']
        dataRollingStock = nedtrainData['Materieeldata']['Meetwaarden']['Materieeleenheid']
        #dataRollingStock = 1
        
        parameterList = (
            dataDate,
            ('PERF%d' % dataRollingStock),
            nedtrainData['Materieeldata']['Meetwaarden']['mpt1'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt2'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt3'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt4'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt5'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt6'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt7'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt8'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt9'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt10'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt11'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt12'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt13'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt14'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt15'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt16'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt17'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt18'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt19'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt20'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt21'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt22'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt23'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt24'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt25'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt26'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt27'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt28'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt29'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt30'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt31'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt32'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt33'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt34'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt35'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt36'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt37'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt38'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt39'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt40'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt41'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt42'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt43'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt44'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt45'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt46'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt47'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt48'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt49'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt50'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt51'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt52'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt53'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt54'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt55'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt56'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt57'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt58'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt59'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt60'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt61'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt62'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt63'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt64'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt65'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt66'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt67'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt68'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt69'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt70'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt71'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt72'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt73'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt74'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt75'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt76'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt77'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt78'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt79'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt80'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt81'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt82'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt83'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt84'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt85'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt86'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt87'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt88'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt89'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt90'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt91'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt92'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt93'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt94'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt95'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt96'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt97'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt98'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt99'],
            nedtrainData['Materieeldata']['Meetwaarden']['mpt0']
        )

        dQueues[dataRollingStock % len(dQueues)].put(parameterList)

        return "OK"

cherrypy.config.update({
    'server.socket_port': 8080,
    'server.socket_host': '0.0.0.0',
    'server.thread_pool': 100,
    'log.screen': False
})

cherrypy.quickstart(NedtrainLoad())
