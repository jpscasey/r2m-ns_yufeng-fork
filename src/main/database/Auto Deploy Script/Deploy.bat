rem SET SLT_DATABASE_NAME=Spectrum_SLT
rem SET VIRM_DATABASE_NAME=Spectrum_VIRM
rem SET SNG_DATABASE_NAME=Spectrum_SNG
SET SQLSERVER=localhost

@if %SLT_DATABASE_NAME% == "" (GOTO error_slt)
@if %VIRM_DATABASE_NAME% == "" (GOTO error_virm)
@if %SNG_DATABASE_NAME% == "" (GOTO error_sng)

SET SLT_DBSTART =
rem SET SLT_DBSTART=dbstart

SET VIRM_DBSTART =
rem SET VIRM_DBSTART=dbstart

SET SNG_DBSTART =
rem SET SNG_DBSTART=dbstart

cd database
cd Spectrum_SNG
call update_spectrum_db.bat %SNG_DATABASE_NAME% %SQLSERVER% %SLT_DATABASE_NAME% %SNG_DBSTART% >output_SNG.txt

move output_SNG.txt ..\..
cd ..

call update_spectrum_db.bat %SLT_DATABASE_NAME% %SQLSERVER% %VIRM_DATABASE_NAME% %SNG_DATABASE_NAME% %SLT_DBSTART% >output_SLT.txt

move output_SLT.txt ..

cd Spectrum_VIRM
call update_spectrum_db.bat %VIRM_DATABASE_NAME% %SQLSERVER% %SLT_DATABASE_NAME% %VIRM_DBSTART% >output_VIRM.txt

move output_VIRM.txt ..\..

cd ..\..

sqlcmd -S %SQLSERVER% -d %SLT_DATABASE_NAME% -Q "SELECT * FROM SchemaChangeLog" -o SchemaChangeLog_SLT.csv
sqlcmd -S %SQLSERVER% -d %VIRM_DATABASE_NAME% -Q "SELECT * FROM SchemaChangeLog" -o SchemaChangeLog_VIRM.csv
sqlcmd -S %SQLSERVER% -d %SNG_DATABASE_NAME% -Q "SELECT * FROM SchemaChangeLog" -o SchemaChangeLog_SNG.csv

@echo Please ensure that the SQL logins are correct.
GOTO end

:error_slt
@echo No SLT database name given, please edit me
GOTO end

:error_virm
@echo No VIRM database name given, please edit me
GOTO end

:error_sng
@echo No SNG database name given, please edit me
GOTO end

:end
