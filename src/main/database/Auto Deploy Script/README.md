# R2M Database Upgrade Scripts

The Deploy.bat file can be used to create brand new SLT, VIRM, and SNG databases, or to upgrade existing SLT, VIRM, and SNG databases.

The purpose of this document is to help guide the database upgrade.

## Package Contents

In this zip package, you should find:
* This README
* The Deploy.bat.rename file
* A database directory, in which you can find:
  * a update_spectrum_db.bat.RENAME file
  * SQL scripts
  * A Spectrum_VIRM directory, in which you can find:
    * a update_spectrum_db.bat.RENAME file
    * More SQL scripts
  * A Spectrum_SNG directory, in which you can find:
    * a update_spectrum_db.bat.RENAME file
    * More SQL scripts

## Before You Start

There are a few steps to follow before running the Deploy script:
1. Rename ./database/update_spectrum_db.bat.RENAME to ./database/update_spectrum_db.bat
2. Rename ./database/Spectrum_VIRM/update_spectrum_db.bat.RENAME to ./database/Spectrum_VIRM/update_spectrum_db.bat
3. Rename ./database/Spectrum_SNG/update_spectrum_db.bat.RENAME to ./database/Spectrum_SNG/update_spectrum_db.bat
4. Open Deploy.bat.RENAME in a text editor
  1. change the line: "rem SET SLT_DATABASE_NAME=Spectrum_SLT" to "SET SLT_DATABASE_NAME={db_name_SLT}" where {db_name_SLT} is the name of the existing SLT database
  2. change the line: "rem SET VIRM_DATABASE_NAME=Spectrum_VIRM" to "SET VIRM_DATABASE_NAME={db_name_VIRM}" where {db_name_VIRM} is the name of the existing VIRM database
  3. change the line: "rem SET SNG_DATABASE_NAME=Spectrum_SNG" to "SET SNG_DATABASE_NAME={db_name_SNG}" where {db_name_SNG} is the name of the existing SNG database
  4. Save the file
5. *OPTIONAL* Only perform this step if starting a new database (Usually not the case) *OPTIONAL*
  1. *OPTIONAL* Open Deploy.bat.RENAME in a text editor *OPTIONAL*
  2. *OPTIONAL* If the SLT database needs to be created, change the line: "rem SET SLT_DBSTART = dbstart" to "SET SLT_DBSTART = dbstart" *OPTIONAL*
  3. *OPTIONAL* If the VIRM database needs to be created, change the line: "rem SET VIRM_DBSTART = dbstart" to "SET VIRM_DBSTART = dbstart" *OPTIONAL*
  4. *OPTIONAL* If the SNG database needs to be created, change the line: "rem SET SNG_DBSTART = dbstart" to "SET SNG_DBSTART = dbstart" *OPTIONAL*
6. Rename Deploy.bat.RENAME to Deploy.bat

## Running the Scripts

1. run the bat file Deploy.bat

#### We do recommend that you backup the databases before running these scripts
