RAISERROR ('-- Updating NSP_SimulateSLTEvents', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SimulateSLTEvents' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_SimulateSLTEvents AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE dbo.NSP_SimulateSLTEvents
AS
BEGIN

	DECLARE @Lower int
	DECLARE @Upper int
	DECLARE @UnitIdRnd int 
	DECLARE @EventHasEndTimeRnd bit
	DECLARE @ChannelEventIDRnd int
	DECLARE @TimestampEndTime datetime2(3)
	DECLARE @Timestamp datetime2(3)

	DECLARE @lat decimal (9,6)
	DECLARE @lng decimal (9,6)
	DECLARE @EventValue int
	DECLARE @LocationIdRnd int
	DECLARE @SpeedRnd int
	DECLARE @NumberOfChannelsRnd int
	DECLARE @StringSql nvarchar(max)

	-- 4 cars
	SET @Lower = 2401
	SET @Upper = 2469
	SELECT @UnitIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 5000
	SET @Upper = 5999
	SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 0
	SET @Upper = 1
	SELECT @EventHasEndTimeRnd = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

	SET @Lower = 2
	SET @Upper = 10
	SELECT @Timestamp = DateAdd(ss,-FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)), getdate())
	SELECT @TimestampEndTime = @Timestamp

	If @EventHasEndTimeRnd = 1
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIdRnd, @Value = 0
	Else 
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = null, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIDRnd, @Value = 1


	-- will select a randon location from table
	-- will select a randon number of randon channels and call proc to insert channelvalue

	-------- insert channelvalues
	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd)  + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitIdRnd) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql

	-- insert event for 6 cars
	SET @Lower = 2601
	SET @Upper = 2662
	SELECT @UnitIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 5000
	SET @Upper = 5999
	SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 0
	SET @Upper = 1
	SELECT @EventHasEndTimeRnd = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

	SET @Lower = 2
	SET @Upper = 10
	SELECT @Timestamp = DateAdd(ss,-FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)), getdate())
	SELECT @TimestampEndTime = @Timestamp

	If @EventHasEndTimeRnd = 1
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIdRnd, @Value = 0
	Else 
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = null, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIDRnd, @Value = 1

	-------- insert channelvalues
	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd)  + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitIdRnd) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql
END
GO


RAISERROR ('-- Updating NSP_SimulateSLTClosingEvent', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SimulateSLTClosingEvent' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_SimulateSLTClosingEvent AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE dbo.NSP_SimulateSLTClosingEvent
AS
BEGIN

	DECLARE @UnitId int
	DECLARE @ChannelId int
	DECLARE @TimeStamp datetime2
	DECLARE @TimestampEndTime datetime2 = getdate()

	SELECT	TOP 1 @UnitId = UnitID,
			@ChannelId = ChannelId,
			@TimeStamp = TimeStamp
	FROM dbo.EventChannelValue
	WHERE TimeStampEndTime is null

	IF @@ROWCOUNT > 0
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitId, @ChannelId = @ChannelId, @Value = 0
END
GO


RAISERROR ('-- Updating NSP_SimulateSLTChannelValues', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SimulateSLTChannelValues' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_SimulateSLTChannelValues AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE dbo.NSP_SimulateSLTChannelValues
AS
BEGIN

	-- will select a randon location from table
	-- will select a randon number of randon channels and call proc to insert channelvalue
	DECLARE @Lower int
	DECLARE @Upper int
	DECLARE @UnitId int 
	DECLARE @lat decimal (9,6)
	DECLARE @lng decimal (9,6)
	DECLARE @EventValue int
	DECLARE @LocationIdRnd int
	DECLARE @ChannelEventIDRnd int
	DECLARE @SpeedRnd int
	DECLARE @TimestampEndTime datetime2(3)
	DECLARE @Timestamp datetime2(3)
	DECLARE @NumberOfChannelsRnd int
	DECLARE @StringSql nvarchar(max)

	SELECT TOP 1 @UnitId = UnitId, @Timestamp = Timestamp
	FROM EventChannelValue
	ORDER BY TimeStamp desc

	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd)  + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitID) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql
END
GO
