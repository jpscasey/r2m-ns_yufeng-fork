SELECT [Fault Code] INTO #KnownFaultCodes
  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
  WHERE  [Fault Code] in (SELECT FaultCode FROM FaultMeta)

  --Codes Returned here will not be Loaded
SELECT 'Code not loaded', [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes

BEGiN TRAN



  INSERT iNTO FaultMeta (FaultCode,Description,Summary,AdditionalInfo,Url,CategoryID,FaultTypeID,HasRecovery,RecoveryProcessPath)
  SELECT [Fault Code]
      ,[Fault Description]
      ,[Summary]
      ,[Additional Information]
      ,[URL]
      ,(  SELECT ID  FROM FaultCategory where FaultCategory.Category =[TestFaultUpload].[Category] )
      ,[Type]
      ,CASE WHEN [Has Recovery] ='Y' THEN 1 ELSE 0 END
      ,[Recovery path]
  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
  WHERE  [Fault Code] not in (SELECT FaultCode FROM FaultMeta)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Train Crew Advice', [Train Crew Advice]   
FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Train Crew Advice]   is not NULL AND [Train Crew Advice]   <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Control Room Advise', [Control Room Advise]  
FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Control Room Advise]   is not NULL AND [Control Room Advise]  <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Engineer Advise', [Engineer Advise]  
FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Engineer Advise] is not NULL AND [Engineer Advise] <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Quality Profile', [Quality Profile]  
FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE  [Quality Profile]   is not NULL AND  [Quality Profile]  <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Control Room Priority', [Control Room Priority] 
 FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
 WhERE [Control Room Priority]  is not NULL AND [Control Room Priority]  <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Safety Priority', [Safety Priority]  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Safety Priority]  is not NULL AND [Safety Priority]  <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Service Request Priority', [Service Request Priority]  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Service Request Priority] is not NULL AND [Service Request Priority] <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'Maintenance Function', [Maintenance Function]  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [Maintenance Function]  is not NULL AND [Maintenance Function]  <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

INSERT INTO FaultMetaExtraField (FaultMetaId,Field,Value)
SELECT (SELECT ID FROM FaultMeta where [Fault Code] = FaultCode), 'System Code', [System Code]  FROM [NedTrain_Spectrum_SLT].[dbo].[TestFaultUpload]
WhERE [System Code] is not NULL AND [System Code] <>''
AND [Fault Code] not in (SELECT [Fault Code] as ExistingFaultCodes FROM #KnownFaultCodes)

COMMIT
