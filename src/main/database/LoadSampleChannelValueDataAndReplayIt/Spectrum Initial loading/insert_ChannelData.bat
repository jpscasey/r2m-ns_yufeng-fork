@echo on
@REM setlocal
@echo param1 - %1
@echo param2 - %2

@if "%1" == "" (GOTO dbdefault) ELSE (GOTO setdbdata)

:setdbdata
@echo setdbdata - %1
@SET DB_NAME=%1 
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)


:dbdefault
@echo setdbdata - %1
set DB_NAME="NS_Dev_Spectrum"
set DB_NAME_ADMINISTRATION="NS_Dev_SR_Administration"
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)

:setserverdata
@echo setserverdata - %2
SET SQLSERVER=%2
@GOTO dbstart

:serverdefault
@SET SQLSERVER=localhost

:dbstart

sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData2__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData2__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData2__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData3__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData3__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData3__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData4__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData4__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData4__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData5__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData5__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData5__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData6__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData6__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData6__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData7__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData7__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData7__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData8__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData8__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData8__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData9__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData9__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData9__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData10__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData10__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData10__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData11__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData11__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData11__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData12__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData12__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData12__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData13__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData13__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData13__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData14__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData14__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData14__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData15__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData15__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData15__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData16__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData16__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData16__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData17__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData17__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData17__3.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData18__1.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData18__2.sql -d %DB_NAME% -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i insertChannelData18__3.sql -d %DB_NAME% -E

:CurrentVersion
@pause
@GOTO end

:generalerror
@echo ****************************
@echo **ERROR: Stopped execution**
@echo ****************************
@Pause
@GOTO end

:end