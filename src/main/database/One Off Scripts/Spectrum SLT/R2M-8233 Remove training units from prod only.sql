/*R2M-8233 Remove SLT Training units (249*_T) FROM NS Production environment Only.*/

CREATE TABLE #UnitIds (UnitId int)

INSERT INTO #UnitIds
    SELECT ID FROM Unit WHERE UnitNumber LIKE '249%T'
  
CREATE TABLE #FaultIds (FaultId int)

INSERT INTO #FaultIds
    SELECT ID FROM Fault WHERE FaultUnitID IN (SELECT UnitId FROM #UnitIds)

CREATE TABLE #VehicleIds (VehicleId int)

INSERT INTO #VehicleIds
    SELECT ID FROM Vehicle WHERE UnitID IN (SELECT UnitID FROM #UnitIds)

--delete FROM
/*
ChannelValue
ChannelValueDoor
EventChannelLatestValue
EventChannelValue
FaultChannelValue
FaultChannelValueDoor
FaultEventChannelValue
FaultCount
FleetStatus
FleetStatusStaging
FleetStatusHistory
VehicleDownload
VehicleDownloadProgress
VehicleEvent
ServiceRequest
WorkOrder
RecoveryAuditItem
TmsFault
GeniusInterfaceData
GeniusStaging
LoadGeniusData
IdealRunChannelValue
Unit
Vehicle
Fault
FleetFormation
*/

IF EXISTS (SELECT 1 FROM ChannelValue WHERE UnitId IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM ChannelValue WHERE UnitId IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM ChannelValueDoor WHERE UnitId IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM ChannelValueDoor WHERE UnitId IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM EventChannelLatestValue WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM EventChannelLatestValue WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM EventChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM EventChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FaultChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FaultChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FaultChannelValueDoor WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FaultChannelValueDoor WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FaultEventChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FaultEventChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FaultCount WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FaultCount WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FleetStatus WHERE UnitID IN (Select UnitId FROM #UnitIds) OR UnitList LIKE '249__T')
BEGIN
    DELETE FROM FleetStatus WHERE UnitID IN (Select UnitId FROM #UnitIds) OR UnitList LIKE '249__T'
END

IF EXISTS (SELECT 1 FROM FleetStatus WHERE UnitList LIKE '%249__T%')
BEGIN
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2491_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2492_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2493_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2494_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2495_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2496_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2497_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2498_T,', '') WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatus SET UnitList = REPLACE(UnitList, '2499_T,', '') WHERE UnitList LIKE '%249__T%';
END

IF EXISTS (SELECT 1 FROM FleetStatusStaging WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FleetStatusStaging WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FleetStatusHistory WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM FleetStatusHistory WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM FleetStatusHistory WHERE UnitList LIKE '%249__T%')
BEGIN
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2491_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2492_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2493_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2494_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2495_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2496_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2497_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2498_T,', '')  WHERE UnitList LIKE '%249__T%';
    UPDATE FleetStatusHistory SET UnitList = REPLACE(UnitList, '2499_T,', '')  WHERE UnitList LIKE '%249__T%';
END

IF EXISTS (SELECT 1 FROM VehicleDownload WHERE VehicleId IN (Select VehicleId FROM #VehicleIds))
BEGIN
    DELETE FROM VehicleDownload WHERE VehicleId IN (Select VehicleId FROM #VehicleIds)
END

IF EXISTS (SELECT 1 FROM VehicleDownloadProgress WHERE VehicleId IN (Select VehicleId FROM #VehicleIds))
BEGIN
    DELETE FROM VehicleDownloadProgress WHERE VehicleId IN (Select VehicleId FROM #VehicleIds)
END

IF EXISTS (SELECT 1 FROM VehicleEvent WHERE VehicleId IN (Select VehicleId FROM #VehicleIds))
BEGIN
    DELETE FROM VehicleEvent WHERE VehicleId IN (Select VehicleId FROM #VehicleIds)
END

IF EXISTS (SELECT 1 FROM ServiceRequest WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM ServiceRequest WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM WorkOrder WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM WorkOrder WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM RecoveryAuditItem WHERE Fault_ID IN (Select FaultId FROM #FaultIds))
BEGIN
    DELETE FROM RecoveryAuditItem WHERE Fault_ID IN (Select FaultId FROM #FaultIds)
END

IF EXISTS (SELECT 1 FROM TmsFault WHERE FaultId IN (Select FaultId FROM #FaultIds))
BEGIN
    DELETE FROM TmsFault WHERE FaultId IN (Select FaultId FROM #FaultIds)
END

IF EXISTS (SELECT 1 FROM GeniusInterfaceData WHERE UnitNumber LIKE '249%T')
BEGIN
    DELETE FROM GeniusInterfaceData WHERE UnitNumber LIKE '249%T'
END

IF EXISTS (SELECT 1 FROM GeniusStaging WHERE UnitNumber LIKE '249%T')
BEGIN
    DELETE FROM GeniusStaging WHERE UnitNumber LIKE '249%T'
END

IF EXISTS (SELECT 1 FROM LoadGeniusData WHERE UnitNumber LIKE '249%T')
BEGIN
    DELETE FROM LoadGeniusData WHERE UnitNumber LIKE '249%T'
END

IF EXISTS (SELECT 1 FROM IdealRunChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM IdealRunChannelValue WHERE UnitID IN (Select UnitId FROM #UnitIds)
END

IF EXISTS (SELECT 1 FROM Fault WHERE ID IN (Select FaultId FROM #FaultIds))
BEGIN
    DELETE FROM Fault WHERE ID IN (Select FaultId FROM #FaultIds)
END

IF EXISTS (SELECT 1 FROM FleetFormation WHERE UnitNumberList LIKE '249__T')
BEGIN
    DELETE FROM FleetFormation WHERE UnitNumberList LIKE '249__T';
END

IF EXISTS (SELECT 1 FROM FleetFormation WHERE UnitNumberList LIKE '%249__T%')
BEGIN
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2491_T,', ''), UnitIDList = REPLACE(UnitIDList, '2491,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2492_T,', ''), UnitIDList = REPLACE(UnitIDList, '2492,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2493_T,', ''), UnitIDList = REPLACE(UnitIDList, '2493,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2494_T,', ''), UnitIDList = REPLACE(UnitIDList, '2494,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2495_T,', ''), UnitIDList = REPLACE(UnitIDList, '2495,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2496_T,', ''), UnitIDList = REPLACE(UnitIDList, '2496,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2497_T,', ''), UnitIDList = REPLACE(UnitIDList, '2497,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2498_T,', ''), UnitIDList = REPLACE(UnitIDList, '2498,', '') WHERE UnitNumberList LIKE '%249__T%';
    UPDATE FleetFormation SET UnitNumberList = REPLACE(UnitNumberList, '2499_T,', ''), UnitIDList = REPLACE(UnitIDList, '2499,', '') WHERE UnitNumberList LIKE '%249__T%';
END

IF EXISTS (SELECT 1 FROM Vehicle WHERE ID IN (Select VehicleId FROM #VehicleIds))
BEGIN
    DELETE FROM Vehicle WHERE ID IN (Select VehicleId FROM #VehicleIds)
END

IF EXISTS (SELECT 1 FROM Unit WHERE ID IN (Select UnitId FROM #UnitIds))
BEGIN
    DELETE FROM Unit WHERE ID IN (Select UnitId FROM #UnitIds)
END

IF(OBJECT_ID('tempdb..#UnitIds') IS NOT NULL)
BEGIN
	DROP TABLE #UnitIds
END

IF(OBJECT_ID('tempdb..#FaultIds') IS NOT NULL)
BEGIN
	DROP TABLE #FaultIds
END

IF(OBJECT_ID('tempdb..#VehicleIds') IS NOT NULL)
BEGIN
	DROP TABLE #VehicleIds
END


INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('One Off Script'
           ,1
           ,'R2M-8233 Remove training units'
           ,null)