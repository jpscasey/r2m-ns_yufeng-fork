SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

-------------------------------------------------------------
--- Correct Faults Incorrect FaultMetas
-------------------------------------------------------------

RAISERROR ('-- Remove Schema Version entry', 0, 1) WITH NOWAIT
GO

DELETE FROM dbo.schema_version WHERE description like '%FaultsIncorrectFaultMetas%'

RAISERROR ('-- Correct Faults Incorrect FaultMetas', 0, 1) WITH NOWAIT
GO

BEGIN TRY

	BEGIN TRANSACTION
	
	Update dbo.Fault set FaultMetaId = (Select ID From dbo.FaultMeta fm Where fm.FaultCode = (Select FaultCode from dbo.FaultMeta fm Where fm.ID = Fault.FaultMetaID) and fm.FleetID = (Select ID from dbo.Fleet fl Where fl.Name = 'SLT') 
	and fm.Parentid is null)
	
	COMMIT TRANSACTION
    print '--- Successfully cleaned up the data ---'

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
	BEGIN
		print '--- Failed to clean up the data ---'
		ROLLBACK TRANSACTION
	END

END CATCH 
