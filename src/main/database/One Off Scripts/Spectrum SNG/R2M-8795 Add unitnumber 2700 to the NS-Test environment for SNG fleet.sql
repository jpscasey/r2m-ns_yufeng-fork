
DECLARE @unitNumber VARCHAR(10) = '2700';
DECLARE @unitId INT;
DECLARE @fleetFormationID INT;
DECLARE @unitType VARCHAR(10) = 'SNG IV';

if (SELECT COUNT(*) FROM dbo.Unit WHERE UnitNumber = @unitNumber) = 0
BEGIN
    -- Insert into Unit table
    INSERT INTO dbo.Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES (@unitNumber, @unitType, 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))

    SELECT @unitId = ID FROM dbo.Unit WHERE UnitNumber = @unitNumber;

    -- Insert into FleetFormation table
    INSERT INTO dbo.FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo)
    VALUES(CONVERT(VARCHAR,@unitId),@unitNumber,GETDATE(),NULL)

    SELECT @fleetFormationID = ID FROM dbo.FleetFormation WHERE UnitNumberList = @unitNumber;

    -- Insert into FleetStatus table
    INSERT INTO [dbo].[FleetStatus] ([ValidFrom],[ValidTo],[Setcode],[Diagram],[Headcode],[IsValid],[Loading],[ConfigDate],[UnitList],[LocationIDHeadcodeStart],[LocationIDHeadcodeEnd],[UnitID],[UnitPosition],[UnitOrientation],[FleetFormationID])
    SELECT GETDATE(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,ID,1,1,@fleetFormationID FROM dbo.Unit WHERE UnitNumber = @unitNumber

    -- Insert into ChannelValue table
    INSERT INTO [dbo].[ChannelValue] (UpdateRecord, UnitID, RecordInsert, TimeStamp)
    VALUES (0, @unitId, GETDATE(), GETDATE());
END
GO