SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON


--------------------------------------------------------------------------------------------------------
-- R2M-8891 Data fix to Fault and FaultMeta tables after introducing fleet-specific fault configurations
--------------------------------------------------------------------------------------------------------

BEGIN TRY

	BEGIN TRANSACTION

	RAISERROR ('-- Drop view VW_IX_Fault', 0, 1) WITH NOWAIT
	
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_IX_Fault' AND TABLE_TYPE = 'VIEW')
	BEGIN
		DROP VIEW dbo.VW_IX_Fault;
	END
	
	
	
	----------------------------------------------------
	-- Drop all Foreign keys referencing to Fault table
	----------------------------------------------------
	
	-- Drop the Foreigh Key FK_Fault_Fault2ExternalReferenceLink
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_Fault2ExternalReferenceLink', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_Fault2ExternalReferenceLink')	
	BEGIN
		ALTER TABLE [dbo].[Fault2ExternalReferenceLink]
		DROP CONSTRAINT [FK_Fault_Fault2ExternalReferenceLink]
	END
	
	
	-- Drop the Foreigh Key FK_FaultChannelValue_Fault
	RAISERROR ('-- Drop the Foreigh Key FK_FaultChannelValue_Fault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_FaultChannelValue_Fault')	
	BEGIN
		ALTER TABLE [dbo].[FaultChannelValue]
		DROP CONSTRAINT [FK_FaultChannelValue_Fault]
	END
	
	
	-- Drop the Foreigh Key FK_FaultChannelValueDoor_Fault
	RAISERROR ('-- Drop the Foreigh Key FK_FaultChannelValueDoor_Fault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_FaultChannelValueDoor_Fault')	
	BEGIN
		ALTER TABLE [dbo].[FaultChannelValueDoor]
		DROP CONSTRAINT [FK_FaultChannelValueDoor_Fault]
	END
	
	
	-- Drop the Foreigh Key FK_FaultEventChannelValue_Fault
	RAISERROR ('-- Drop the Foreigh Key FK_FaultEventChannelValue_Fault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_FaultEventChannelValue_Fault')	
	BEGIN
		ALTER TABLE [dbo].[FaultEventChannelValue]
		DROP CONSTRAINT [FK_FaultEventChannelValue_Fault]
	END
	
	
	-- Drop the Foreigh Key FK_Fault_FaultStatusHistory
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_FaultStatusHistory', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_FaultStatusHistory')	
	BEGIN
		ALTER TABLE [dbo].[FaultStatusHistory]
		DROP CONSTRAINT [FK_Fault_FaultStatusHistory]
	END
	
	
	-- Drop the Foreigh Key FK_Fault_RecoveryAuditItem
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_RecoveryAuditItem', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_RecoveryAuditItem')	
	BEGIN
		ALTER TABLE [dbo].[RecoveryAuditItem]
		DROP CONSTRAINT [FK_Fault_RecoveryAuditItem]
	END
	
	
	-- Drop the Foreigh Key FK_Fault_FaultComment
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_FaultComment', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_FaultComment')	
	BEGIN
		ALTER TABLE [dbo].[FaultComment]
		DROP CONSTRAINT [FK_Fault_FaultComment]
	END
	
	
	-- Drop the Foreigh Key FK_Fault_TmsFault
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_TmsFault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_TmsFault')	
	BEGIN
		ALTER TABLE [dbo].[TmsFault]
		DROP CONSTRAINT [FK_Fault_TmsFault]
	END
	
	
	-- Drop the Foreigh Key FK_Fault_FaultGroup
	RAISERROR ('-- Drop the Foreigh Key FK_Fault_FaultGroup', 0, 1) WITH NOWAIT
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Fault_FaultGroup')	
	BEGIN
		ALTER TABLE [dbo].[Fault]
		DROP CONSTRAINT [FK_Fault_FaultGroup]
	END
	
	
	-- Drop the Foreigh Key FK_FaultMeta_Fault
	RAISERROR ('-- Drop the Foreigh Key FK_FaultMeta_Fault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_FaultMeta_Fault')	
	BEGIN
		ALTER TABLE [dbo].[Fault]
		DROP CONSTRAINT [FK_FaultMeta_Fault]
	END
	
	
	-- Drop the Foreigh Key FK_Unit_Fault
	RAISERROR ('-- Drop the Foreigh Key FK_Unit_Fault', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_Unit_Fault')	
	BEGIN
		ALTER TABLE [dbo].[Fault]
		DROP CONSTRAINT [FK_Unit_Fault]
	END
	
	
	
	-- Drop the Foreigh Key FK_FaultMetaExtraField
	RAISERROR ('-- Drop the Foreigh Key FK_FaultMetaExtraField', 0, 1) WITH NOWAIT
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_FaultMetaExtraField')	
	BEGIN
		ALTER TABLE [dbo].[FaultMetaExtraField]
		DROP CONSTRAINT [FK_FaultMetaExtraField]
	END
	
	
	------------------------------------------------------
	-- Clean data from tables
	------------------------------------------------------
	
	-- Truncate data from table Fault2ExternalReferenceLink
	RAISERROR ('-- Truncate data from table Fault2ExternalReferenceLink', 0, 1) WITH NOWAIT
	
	
	TRUNCATE TABLE [dbo].[Fault2ExternalReferenceLink]
	
	
	
	-- Truncate data from table FaultChannelValue
	RAISERROR ('-- Truncate data from table FaultChannelValue', 0, 1) WITH NOWAIT
	
	
	TRUNCATE TABLE [dbo].[FaultChannelValue]
	
	
	
	-- Truncate data from table FaultChannelValueDoor
	RAISERROR ('-- Truncate data from table FaultChannelValueDoor', 0, 1) WITH NOWAIT
	
	
	TRUNCATE TABLE [dbo].[FaultChannelValueDoor]
	
	
	
	-- Truncate data from table FaultEventChannelValue
	RAISERROR ('-- Truncate data from table FaultEventChannelValue', 0, 1) WITH NOWAIT
	
	
	TRUNCATE TABLE [dbo].[FaultEventChannelValue]
	
	
	
	-- Truncate data from table FaultStatusHistory
	RAISERROR ('-- Truncate data from table FaultStatusHistory', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[FaultStatusHistory]
	
	
	
	-- Truncate data from table RecoveryAuditItem
	RAISERROR ('-- Truncate data from table RecoveryAuditItem', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[RecoveryAuditItem]
	
	
	
	-- Truncate data from table FaultComment
	RAISERROR ('-- Truncate data from table FaultComment', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[FaultComment]
	
	
	
	-- Truncate data from table TmsFault
	RAISERROR ('-- Truncate data from table TmsFault', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[TmsFault]
	
	
	
	-- Truncate data from table ServiceRequest
	RAISERROR ('-- Truncate data from table ServiceRequest', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[ServiceRequest]
	
	
	
	-- Truncate data from table FaultCount
	RAISERROR ('-- Truncate data from table FaultCount', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[FaultCount]
	
	
	
	-- Truncate data from table Fault
	RAISERROR ('-- Truncate data from table Fault', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[Fault]
	
	
	
	-- Truncate data from table FaultMetaE2M
	RAISERROR ('-- Truncate data from table FaultMetaE2M', 0, 1) WITH NOWAIT
	
	TRUNCATE TABLE [dbo].[FaultMetaE2M]
	
	
	
	-- Truncate data from table FaultMetaExtraField
	RAISERROR ('-- Truncate data from table FaultMetaExtraField', 0, 1) WITH NOWAIT
	 
	TRUNCATE TABLE [dbo].[FaultMetaExtraField]
	
	
	
	-- Truncate data from table FaultMeta
	RAISERROR ('-- Truncate data from table FaultMeta', 0, 1) WITH NOWAIT
	 
	TRUNCATE TABLE [dbo].[FaultMeta]
	
	
	-------------------------------------------
	
	
	---------------------------------------------------
	--  Recreate the dropped foreign keys
	---------------------------------------------------
	
	
	-- Recreate the Foreign Key FK_Fault_Fault2ExternalReferenceLink
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_Fault2ExternalReferenceLink', 0, 1) WITH NOWAIT
	
	
	ALTER TABLE [dbo].[Fault2ExternalReferenceLink]  WITH CHECK ADD  CONSTRAINT [FK_Fault_Fault2ExternalReferenceLink] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[Fault2ExternalReferenceLink] CHECK CONSTRAINT [FK_Fault_Fault2ExternalReferenceLink]
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_FaultChannelValue_Fault
	RAISERROR ('-- Recreate the Foreign Key FK_FaultChannelValue_Fault', 0, 1) WITH NOWAIT
	
	
	ALTER TABLE [dbo].[FaultChannelValue]  WITH NOCHECK ADD  CONSTRAINT [FK_FaultChannelValue_Fault] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[FaultChannelValue] CHECK CONSTRAINT [FK_FaultChannelValue_Fault]
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_FaultChannelValueDoor_Fault
	RAISERROR ('-- Recreate the Foreign Key FK_FaultChannelValueDoor_Fault', 0, 1) WITH NOWAIT
	
	
	ALTER TABLE [dbo].[FaultChannelValueDoor]  WITH NOCHECK ADD  CONSTRAINT [FK_FaultChannelValueDoor_Fault] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[FaultChannelValueDoor] CHECK CONSTRAINT [FK_FaultChannelValueDoor_Fault]
	
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_FaultEventChannelValue_Fault
	RAISERROR ('-- Recreate the Foreign Key FK_FaultEventChannelValue_Fault', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[FaultEventChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_FaultEventChannelValue_Fault] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[FaultEventChannelValue] CHECK CONSTRAINT [FK_FaultEventChannelValue_Fault]
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_Fault_FaultStatusHistory
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_FaultStatusHistory', 0, 1) WITH NOWAIT
	
	
	ALTER TABLE [dbo].[FaultStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_Fault_FaultStatusHistory] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[FaultStatusHistory] CHECK CONSTRAINT [FK_Fault_FaultStatusHistory]
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_Fault_RecoveryAuditItem
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_RecoveryAuditItem', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[RecoveryAuditItem]  WITH CHECK ADD  CONSTRAINT [FK_Fault_RecoveryAuditItem] FOREIGN KEY([Fault_ID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[RecoveryAuditItem] CHECK CONSTRAINT [FK_Fault_RecoveryAuditItem]
	
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_Fault_FaultComment
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_FaultComment', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[FaultComment]  WITH CHECK ADD  CONSTRAINT [FK_Fault_FaultComment] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[FaultComment] CHECK CONSTRAINT [FK_Fault_FaultComment]
	
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_Fault_TmsFault
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_TmsFault', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[TmsFault]  WITH CHECK ADD  CONSTRAINT [FK_Fault_TmsFault] FOREIGN KEY([FaultID])
	REFERENCES [dbo].[Fault] ([ID])
	
	
	ALTER TABLE [dbo].[TmsFault] CHECK CONSTRAINT [FK_Fault_TmsFault]
	
	
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_Fault_FaultGroup
	RAISERROR ('-- Recreate the Foreign Key FK_Fault_FaultGroup', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[Fault]  WITH CHECK ADD  CONSTRAINT [FK_Fault_FaultGroup] FOREIGN KEY([FaultGroupID])
	REFERENCES [dbo].[FaultGroup] ([ID])
	
	
	ALTER TABLE [dbo].[Fault] CHECK CONSTRAINT [FK_Fault_FaultGroup]
	
	
	

	--------------------------------------------------
	-- Recreate the Foreign Key FK_FaultMeta_Fault
	RAISERROR ('-- Recreate the Foreign Key FK_FaultMeta_Fault', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[Fault]  WITH CHECK ADD  CONSTRAINT [FK_FaultMeta_Fault] FOREIGN KEY([FaultMetaID])
	REFERENCES [dbo].[FaultMeta] ([ID])
	
	
	ALTER TABLE [dbo].[Fault] CHECK CONSTRAINT [FK_FaultMeta_Fault]
	


	--------------------------------------------------
	-- Recreate the Foreign Key FK_Unit_Fault
	RAISERROR ('-- Recreate the Foreign Key FK_Unit_Fault', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[Fault]  WITH CHECK ADD  CONSTRAINT [FK_Unit_Fault] FOREIGN KEY([FaultUnitID])
	REFERENCES [dbo].[Unit] ([ID])
	
	
	ALTER TABLE [dbo].[Fault] CHECK CONSTRAINT [FK_Unit_Fault]
	
	
	--------------------------------------------------
	-- Recreate the Foreign Key FK_FaultMetaExtraField
	RAISERROR ('-- Recreate the Foreign Key FK_FaultMetaExtraField', 0, 1) WITH NOWAIT
	
	ALTER TABLE [dbo].[FaultMetaExtraField]  WITH CHECK ADD  CONSTRAINT [FK_FaultMetaExtraField] FOREIGN KEY([FaultMetaId])
	REFERENCES [dbo].[FaultMeta] ([ID])
	
	
	ALTER TABLE [dbo].[FaultMetaExtraField] CHECK CONSTRAINT [FK_FaultMetaExtraField]
	
	
	------------------------------------------------------
	-- Clean the column 'Value' from table RuleEngineCurrentId
	------------------------------------------------------
	RAISERROR ('-- Clean the column Value from table RuleEngineCurrentId', 0, 1) WITH NOWAIT
	
	UPDATE [dbo].[RuleEngineCurrentId] SET [Value] = null
	WHERE ConfigurationName LIKE '%Fault%'
	
	
	COMMIT TRANSACTION
	
	print '--- Successfully clean up the data ---'
	
	
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
	BEGIN
		print '--- Failed to clean up the data ---'
		ROLLBACK TRANSACTION
	END
END CATCH 
		
------------------------------------------------------
-- Recreating views
------------------------------------------------------

RAISERROR ('-- Recreating view VW_IX_Fault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_IX_Fault' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_IX_Fault AS SELECT 1 AS X;')
END
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_CL_U_VW_IX_Fault_ID')
    DROP INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_CreateTime')
    DROP INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_FaultType_CreateTime')
    DROP INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged')
    DROP INDEX [IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_EndTime')
    DROP INDEX [IX_VW_IX_Fault_IsCurrent_EndTime] ON [dbo].[VW_IX_Fault] 
GO


ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		, f.FaultGroupID
		, f.IsGroupLead
		, f.RuleName
		, f.ServiceRequestID
		, f.CountSinceLastMaint
		, uf.UnitStatus
		, f.UnitStatus AS UnitStatusFault --UnitStatus at time of the fault
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 06/04/2018 10:41:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 06/04/2018 10:41:47 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 06/04/2018 10:41:47 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged')
    CREATE NONCLUSTERED INDEX IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged
    ON [dbo].[VW_IX_Fault] (IsCurrent, [ReportingOnly], [IsAcknowledged])


IF NOT EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_EndTime')
CREATE NONCLUSTERED INDEX IX_VW_IX_Fault_IsCurrent_EndTime
ON [dbo].[VW_IX_Fault] (IsCurrent, [EndTime] DESC)
		
	
