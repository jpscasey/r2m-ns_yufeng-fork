/*R2M-8520 Cleaning Production database before going live*/
--NOTE: deleting entries in the Fault table will take some time

TRUNCATE TABLE ChannelValue
TRUNCATE TABLE EventChannelValue
TRUNCATE TABLE EventChannelLatestValue
TRUNCATE TABLE FaultChannelValue
TRUNCATE TABLE FaultEventChannelValue 
TRUNCATE TABLE FaultComment
TRUNCATE TABLE FaultStatusHistory
TRUNCATE TABLE RecoveryAuditItem 
TRUNCATE TABLE Fault2ExternalReferenceLink

--Fault is used in indexed views so can't use truncate
DELETE FROM Fault

--there needs to be an entry in channel value for rules engine
EXEC NSP_PopulateChannelValues

--reset the numbers in the RuleEngineCurrentId table
UPDATE RuleEngineCurrentID SET Value = 1