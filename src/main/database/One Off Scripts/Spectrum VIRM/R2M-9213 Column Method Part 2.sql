/*
For Column Add rename method.
Part 2
This script takes 4 Parameters 
@TableName the TAble name that you are modifing
@Counter The size of the batch to be copied 
@ID the staring ID to be copied defualts to the smallest ID in the Table
@MaxID the last ID to be copied 

Copies date from Current to new Columns

*/

DECLARE @Counter int = 5000
DECLARE @TableName varchar(100) = 'Fault' -- Table to have rows copied
DECLARE @ID bigint -- Change this to limit rows copied
DECLARE @MaxID  Bigint --Places a max Row to be copied 

DECLARE @sql varchar(max)
DECLARE @sqlCols varchar(max)


SELECT @sqlCols = STUFF( (SELECT ', '+c.name +'_new = '++c.name
FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name = @TableName 
FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1,' ')

IF @sqlCols is null RAISERROR ('-- No Columns Need to be changed', 20, 1) WITH LOG


DECLARE @sqlMin nvarchar(200) = 'SELECT @ID = Min(ID) FROM '+@TableName

DECLARE  @i int 

exec sp_executesql @sqlMin, N'@ID int out', @ID out

if @ID is null set @ID =1
WHILE 1=1 and (@ID < @MaxID OR @MaxID is null)
BEgiN 
SET @SQL = 'UPDATE '+@TableName+' set '+@sqlCols+' where ID BETWEEN '+cast(@ID as varchar(10)) +' AND ' +cast(@ID +@Counter as varchar(10))

EXEC (@sql)
IF @@ROWCOUNT =0 BREAK

SET @ID = @ID +@Counter+1

WAITFOR DELAY '00:00:01'

END

GO




