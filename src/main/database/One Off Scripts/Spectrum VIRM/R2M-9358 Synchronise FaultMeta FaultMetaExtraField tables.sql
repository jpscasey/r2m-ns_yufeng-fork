SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- R2M-9358
-- Update FaultMeta, FaultMetaExtraField tables for VIRM
-- fleet with synchronised data taken from the SLT database
------------------------------------------------------------

RAISERROR ('-- Update FaultMeta, FaultMetaExtraField tables for currently out of sync VIRM fleet', 0, 1) WITH NOWAIT
GO

BEGIN TRAN



IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FaultMeta_bak_replace_VIRM'))
BEGIN
    DROP TABLE dbo.FaultMeta_bak_replace_VIRM;
END

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FaultMetaExtraField_bak_replace_VIRM'))
BEGIN
	DROP TABLE dbo.FaultMetaExtraField_bak_replace_VIRM;
END



DECLARE @SLT_DBNAME varchar(100)
SELECT @SLT_DBNAME = name FROM sys.databases where name = 'INSERT_SLT_DBNAME_HERE' --For example: NS_Dev_Spectrum
DECLARE @query varchar(max)



ALTER TABLE dbo.FaultMeta NOCHECK CONSTRAINT ALL
ALTER TABLE dbo.Fault NOCHECK CONSTRAINT ALL

SELECT * INTO dbo.FaultMeta_bak_replace_VIRM FROM dbo.FaultMeta
SELECT * INTO dbo.FaultMetaExtraField_bak_replace_VIRM FROM dbo.FaultMetaExtraField

DELETE FROM dbo.FaultMetaExtraField
DELETE FROM dbo.FaultMeta



SET IDENTITY_INSERT dbo.FaultMeta ON

SET @query = '
   INSERT INTO dbo.FaultMeta (ID,FaultCode,Description,Summary,CategoryID,RecoveryProcessPath,FaultTypeID,Url,GroupID,AdditionalInfo,Username,HasRecovery,FromDate,ToDate,ParentID,FleetID)
   SELECT ID,FaultCode,Description,Summary,CategoryID,RecoveryProcessPath,FaultTypeID,Url,GroupID,AdditionalInfo,Username,HasRecovery,FromDate,ToDate,ParentID,FleetID
   FROM ' + @SLT_DBNAME + '.dbo.FaultMeta'
EXEC( @query)

SET IDENTITY_INSERT dbo.FaultMeta OFF



SET @query = '
   INSERT INTO dbo.FaultMetaExtraField (FaultMetaId,Field,Value)
   SELECT FaultMetaId,Field,Value FROM ' + @SLT_DBNAME + '.dbo.FaultMetaExtraField'
EXEC( @query)


UPDATE f SET f.faultMetaID = fm.ID 
FROM dbo.Fault f 
INNER JOIN dbo.FaultMeta_bak_replace_VIRM fm_old  ON f.FaultMetaID = fm_old.ID 
INNER JOIN dbo.FaultMeta fm ON fm.FaultCode = fm_old.FaultCode 
AND (
    (fm.ParentID is null AND fm_old.ParentID is null)
OR
(
    SELECT FaultCode FROM dbo.FaultMeta fm_parent_new where fm_parent_new.ID = fm.ParentID) = (SELECT FaultCode FROM dbo.FaultMeta_bak_replace_VIRM fm_parent_old where fm_parent_old.ID = fm_old.ParentID)
)



ALTER TABLE dbo.FaultMeta WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE dbo.Fault WITH CHECK CHECK CONSTRAINT ALL;

DROP TABLE dbo.FaultMeta_bak_replace_VIRM;
DROP TABLE dbo.FaultMetaExtraField_bak_replace_VIRM;

COMMIT

GO