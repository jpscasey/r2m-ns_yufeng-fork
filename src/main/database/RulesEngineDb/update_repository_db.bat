@echo on
@REM setlocal
@echo param1 - %1
@echo param2 - %2

@if "%1" == "" (GOTO dbdefault) ELSE (GOTO setdbdata)

:setdbdata
@echo setdbdata - %1
@SET DB_NAME=%1 
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)


:dbdefault
@echo setdbdata - %1
set DB_NAME="NS_Spectrum"
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)

:setserverdata
@echo setserverdata - %2
SET SQLSERVER=%2
@if "%3" == "dbstart" (GOTO dbstart) ELSE (GOTO script001)
@GOTO script001

:serverdefault
@SET SQLSERVER=localhost
@if "%3" == "dbstart" (GOTO dbstart) ELSE (GOTO script001)
@GOTO script001

:dbstart
rem Dbstart was request
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i create_repository_db.sql -d master% -E -b
:script001
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_repository_db_001.sql -d %DB_NAME% -E -b
@if %ERRORLEVEL% GEQ 1 GOTO generalerror
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_repository_db_002.sql -d %DB_NAME% -E -b
@if %ERRORLEVEL% GEQ 1 GOTO generalerror

:CurrentVersion
@pause
@GOTO end

:generalerror
@echo ****************************
@echo **ERROR: Stopped execution**
@echo ****************************
@Pause
@GOTO end

:end