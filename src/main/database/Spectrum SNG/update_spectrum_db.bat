@echo on
@REM setlocal
@echo param1 - %1
@echo param2 - %2

@if "%1" == "" (GOTO dbdefault) ELSE (GOTO setdbdata)

:setdbdata
@echo setdbdata - %1
@SET DB_NAME=%1 
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)


:dbdefault
@echo setdbdata - %1
set DB_NAME="Spectrum_SNG"
@if "%2" == "" (GOTO serverdefault) ELSE (GOTO setserverdata)

:setserverdata
@echo setserverdata - %2
SET SQLSERVER=%2
@if "%3" == "" (GOTO sltdefault) ELSE (GOTO setsltdb)

:serverdefault
@SET SQLSERVER=localhost
@if "%3" == "" (GOTO sltdefault) ELSE (GOTO setsltdb)

:sltdefault
set DB_NAME_SLT="NedTrain_Spectrum_SLT"
@if "%4" == "dbstart" (GOTO dbstart) ELSE (GOTO script001)
@GOTO script001

:setsltdb
@echo setsltdb - %3
SET DB_NAME_SLT=%3
@if "%4" == "dbstart" (GOTO dbstart) ELSE (GOTO script001)
@GOTO script001


:dbstart
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i create_spectrum_db.sql -d master% -E
:script001
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_001.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_002.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_003.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_004.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_005.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_006.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_007.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_008.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_009.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_010.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_011.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_012.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_013.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_001.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_002.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_003.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_014.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_004.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_015.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_005.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_006.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_007.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_016.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_008.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_017.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_018.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_009.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_019.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_020.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_010.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_011.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_012.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_021.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_013.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_014.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_015.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_016.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_022.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_023.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_017.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_024.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_018.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_019.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_020.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_025.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_026.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_027.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_021.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_028.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_029.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_030.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_031.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_032.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_033.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_034.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_035.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_022.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_023.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_036.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_037.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_038.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_039.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_040.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_024.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_025.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_041.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_042.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_043.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_044.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_045.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_026.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_046.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_047.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_048.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_049.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_050.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_051.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_027.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_052.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_053.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_054.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_028.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_055.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_056.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_057.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_058.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_059.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_060.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_061.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_jobs_db_001.sql -d %DB_NAME%  -E
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_062.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_063.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_029.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_064.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_065.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_030.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_066.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_067.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_031.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_068.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_069.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_070.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_071.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_072.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_073.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_074.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_075.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_076.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_032.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_077.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_078.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_079.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_080.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_081.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_082.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_083.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_084.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_085.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_086.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_087.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_088.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_089.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_090.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_091.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_092.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_093.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_094.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_095.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_096.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_097.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_098.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_033.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_099.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_100.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_101.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_102.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_103.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_104.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_105.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_106.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_107.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_108.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_109.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_110.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_034.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_111.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_035.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_112.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_113.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_114.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_115.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_116.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_117.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_118.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_119.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_036.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_120.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_121.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_037.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_122.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_123.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_124.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_125.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_126.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_127.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_128.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_129.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_130.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_131.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_132.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_133.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_134.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_038.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_135.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_136.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_137.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_039.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_040.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_138.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_041.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_042.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_139.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_140.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_043.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_141.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_142.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_143.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_144.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_145.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_044.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_045.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_146.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_147.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_046.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_047.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_048.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_049.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_148.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_149.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_050.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_051.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_150.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_151.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_052.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_053.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_054.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% db_name_slt=%DB_NAME_SLT% -i update_spectrum_db_load_055.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% db_name_slt=%DB_NAME_SLT% -i update_spectrum_db_load_056.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_152.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_057.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_153.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_058.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_059.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_060.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_154.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_061.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_155.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_062.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_156.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_063.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_064.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_157.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_158.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_159.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_load_065.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_160.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_161.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_162.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_163.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_164.sql -d %DB_NAME% -E -b
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i update_spectrum_db_165.sql -d %DB_NAME% -E -b

:CurrentVersion
@GOTO end

:generalerror
@echo ****************************
@echo **ERROR: Stopped execution**
@echo ****************************
@Pause
@GOTO end

:end