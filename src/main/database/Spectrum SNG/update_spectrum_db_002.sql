:setvar scriptNumber 002

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
-- TABLES
------------------------------------------------------------


-------------------------------------------------------------------------------
RAISERROR ('-- Create ALL tables', 0, 1) WITH NOWAIT
/****** Object:  Table [dbo].[ActualRunAnalysisStat]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualRunAnalysisStat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActualRunAnalysisStat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateValue] [date] NULL,
	[ActualJourneyID] [int] NULL,
	[AnalysisTime] [datetime2](3) NULL,
	[Headcode] [char](4) NULL,
	[StartTime] [time](0) NULL,
	[SectionPointNumber] [int] NULL,
	[SectionPointNumberRecognized] [int] NULL,
	[VehicleA] [int] NULL,
	[VehicleARowCount] [int] NULL,
	[VehicleAValidGpsRowCount] [int] NULL,
	[VehicleADataTimeRange] [int] NULL,
	[VehicleB] [int] NULL,
	[VehicleBRowCount] [int] NULL,
	[VehicleBValidGpsRowCount] [int] NULL,
	[VehicleBDataTimeRange] [int] NULL,
	[AnalysedVehicleID] [int] NULL,
	[AnalysisDuration] [int] NULL,
 CONSTRAINT [PK_ActualRunAnalysisStat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[AllUKTiploc]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AllUKTiploc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AllUKTiploc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Tiploc] [char](7) NULL,
	[Name] [varchar](50) NULL,
	[Type] [varchar](20) NULL,
	[Lat] [decimal](9, 6) NULL,
	[Lng] [decimal](9, 6) NULL,
 CONSTRAINT [PK_CL_AllUKTiploc] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ArchivingLog]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArchivingLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ArchivingLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[FinishTime] [datetime] NOT NULL,
	[TableName] [varchar](128) NOT NULL,
	[Records] [int] NOT NULL,
	[Runs] [int] NOT NULL,
	[ErrorMsg] [varchar](2000) NULL,
 CONSTRAINT [PK_CL_ArchivingLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Channel]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Channel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Channel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NULL,
	[EngColumnNo] [smallint] NULL,
	[ChannelGroupID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Header] [varchar](100) NOT NULL,
	[Ref] [varchar](10) NOT NULL,
	[DataType] [varchar](20) NOT NULL,
	[TypeID] [int] NOT NULL,
	[ChannelGroupOrder] [tinyint] NULL,
	[UOM] [varchar](10) NULL,
	[MinValue] [int] NULL,
	[MaxValue] [int] NULL,
	[HardwareType] [char](5) NOT NULL CONSTRAINT [DF_Channel_HardwareType]  DEFAULT ('OTMR'),
	[StorageMethod] [char](1) NULL,
	[Comment] [char](100) NULL,
	[VisibleOnFaultOnly] [bit] NULL,
	[IsAlwaysDisplayed] [bit] NOT NULL CONSTRAINT [DF_Channel_IsAlwaysDisplayed]  DEFAULT ((1)),
	[IsLookup] [bit] NOT NULL CONSTRAINT [DF_Channel_IsLookup]  DEFAULT ((0)),
	[DefaultValue] [bit] NULL,
	[FleetGroup] [varchar](20) NULL,
	[IsDisplayedAsDifference] [bit] NOT NULL CONSTRAINT [DF_Channel_isDisplayedAsDifference]  DEFAULT ((0)),
 CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_Channel_Header_HardwareType_VehicleID] UNIQUE NONCLUSTERED 
(
	[Header] ASC,
	[HardwareType] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_Channel_Name_HardwareType_VehicleID] UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[HardwareType] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelGroup]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChannelGroupName] [varchar](50) NOT NULL,
	[Notes] [varchar](100) NULL,
	[IsVisible] [bit] NULL CONSTRAINT [DF_ChannelGroup_IsVisible]  DEFAULT ((1)),
 CONSTRAINT [PK_ChannelGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_ChannelGroup_Name] UNIQUE NONCLUSTERED 
(
	[ChannelGroupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelMmiDisplay]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelMmiDisplay]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelMmiDisplay](
	[ChannelID] [int] NOT NULL,
	[Type] [char](1) NOT NULL,
	[DisplayOnValue] [bit] NOT NULL,
	[MmiName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ChannelMmiDisplay] PRIMARY KEY CLUSTERED 
(
	[ChannelID] ASC,
	[DisplayOnValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelRule]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelRule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelRule](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[ChannelID] [int] NOT NULL,
	[ChannelStatusID] [smallint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ChannelRule_Active]  DEFAULT ((1)),
 CONSTRAINT [PK_ChannelRule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelRuleValidation]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelRuleValidation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelRuleValidation](
	[ChannelRuleID] [smallint] NOT NULL,
	[ChannelID] [int] NOT NULL,
	[MinValue] [decimal](18, 9) NULL,
	[MaxValue] [decimal](18, 9) NULL,
	[MinInclusive] [bit] NULL,
	[MaxInclusive] [bit] NULL,
 CONSTRAINT [PK_ChannelRuleValidation] PRIMARY KEY CLUSTERED 
(
	[ChannelRuleID] ASC,
	[ChannelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChannelStatus]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelStatus](
	[ID] [smallint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Priority] [tinyint] NOT NULL,
 CONSTRAINT [PK_ChannelStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelType]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](16) NOT NULL,
 CONSTRAINT [PK_ChannelType] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelValue]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelValue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UpdateRecord] [bit] NOT NULL CONSTRAINT [DF_ChannelValue_UpdatesRecord]  DEFAULT ((0)),
	[UnitID] [int] NULL,
	[RecordInsert] [datetime] NOT NULL CONSTRAINT [DF_ChannelValue_RecordInsert]  DEFAULT (getdate()),
	[TimeStamp] [datetime] NOT NULL,
		[Col1] decimal (9,6) NULL,
		[Col2] decimal (9,6) NULL,
		[Col3] decimal (9,3) NULL,
		[Col4] bit NULL,
		[Col5] bit NULL,
		[Col6] bit NULL,
		[Col7] bit NULL,
		[Col8] bit NULL,
		[Col9] bit NULL,
		[Col10] bit NULL,
		[Col11] bit NULL,
		[Col12] bit NULL,
		[Col13] bit NULL,
		[Col14] bit NULL,
		[Col15] bit NULL,
		[Col16] bit NULL,
		[Col17] bit NULL,
		[Col18] bit NULL,
		[Col19] bit NULL,
		[Col20] bit NULL,
		[Col21] bit NULL,
		[Col22] bit NULL,
		[Col23] bit NULL,
		[Col24] bit NULL,
		[Col25] bit NULL,
		[Col26] bit NULL,
		[Col27] bit NULL,
		[Col28] bit NULL,
		[Col29] bit NULL,
		[Col30] bit NULL,
		[Col31] bit NULL,
		[Col32] bit NULL,
		[Col33] bit NULL,
		[Col34] bit NULL,
		[Col35] bit NULL,
		[Col36] bit NULL,
		[Col37] bit NULL,
		[Col38] bit NULL,
		[Col39] bit NULL,
		[Col40] bit NULL,
		[Col41] bit NULL,
		[Col42] bit NULL,
		[Col43] bit NULL,
		[Col44] bit NULL,
		[Col45] bit NULL,
		[Col46] bit NULL,
		[Col47] bit NULL,
		[Col48] bit NULL,
		[Col49] bit NULL,
		[Col50] bit NULL,
		[Col51] bit NULL,
		[Col52] bit NULL,
		[Col53] bit NULL,
		[Col54] bit NULL,
		[Col55] bit NULL,
		[Col56] bit NULL,
		[Col57] bit NULL,
		[Col58] bit NULL,
		[Col59] bit NULL,
		[Col60] bit NULL,
		[Col61] bit NULL,
		[Col62] bit NULL,
		[Col63] bit NULL,
		[Col64] bit NULL,
		[Col65] bit NULL,
		[Col66] bit NULL,
		[Col67] bit NULL,
		[Col68] bit NULL,
		[Col69] bit NULL,
		[Col70] bit NULL,
		[Col71] bit NULL,
		[Col72] bit NULL,
		[Col73] bit NULL,
		[Col74] bit NULL,
		[Col75] bit NULL,
		[Col76] bit NULL,
		[Col77] bit NULL,
		[Col78] bit NULL,
		[Col79] bit NULL,
		[Col80] bit NULL,
		[Col81] bit NULL,
		[Col82] bit NULL,
		[Col83] bit NULL,
		[Col84] bit NULL,
		[Col85] bit NULL,
		[Col86] bit NULL,
		[Col87] bit NULL,
		[Col88] bit NULL,
		[Col89] bit NULL,
		[Col90] bit NULL,
		[Col91] bit NULL,
		[Col92] bit NULL,
		[Col93] bit NULL,
		[Col94] bit NULL,
		[Col95] bit NULL,
		[Col96] bit NULL,
		[Col97] bit NULL,
		[Col98] bit NULL,
		[Col99] bit NULL,
		[Col100] bit NULL,
		[Col101] bit NULL,
		[Col102] bit NULL,
		[Col103] bit NULL,
		[Col104] bit NULL,
		[Col105] bit NULL,
		[Col106] bit NULL,
		[Col107] bit NULL,
		[Col108] bit NULL,
		[Col109] bit NULL,
		[Col110] bit NULL,
		[Col111] bit NULL,
		[Col112] bit NULL,
		[Col113] bit NULL,
		[Col114] bit NULL,
		[Col115] bit NULL,
		[Col116] bit NULL,
		[Col117] bit NULL,
		[Col118] bit NULL,
		[Col119] bit NULL,
		[Col120] bit NULL,
		[Col121] bit NULL,
		[Col122] bit NULL,
		[Col123] bit NULL,
		[Col124] bit NULL,
		[Col125] bit NULL,
		[Col126] bit NULL,
		[Col127] bit NULL,
		[Col128] bit NULL,
		[Col129] bit NULL,
		[Col130] bit NULL,
		[Col131] bit NULL,
		[Col132] bit NULL,
		[Col133] bit NULL,
		[Col134] bit NULL,
		[Col135] bit NULL,
		[Col136] bit NULL,
		[Col137] bit NULL,
		[Col138] bit NULL,
		[Col139] bit NULL,
		[Col140] bit NULL,
		[Col141] bit NULL,
		[Col142] bit NULL,
		[Col143] bit NULL,
		[Col144] bit NULL,
		[Col145] bit NULL,
		[Col146] bit NULL,
		[Col147] bit NULL,
		[Col148] bit NULL,
		[Col149] bit NULL,
		[Col150] bit NULL,
		[Col151] bit NULL,
		[Col152] bit NULL,
		[Col153] bit NULL,
		[Col154] bit NULL,
		[Col155] bit NULL,
		[Col156] bit NULL,
		[Col157] bit NULL,
		[Col158] bit NULL,
		[Col159] bit NULL,
		[Col160] bit NULL,
		[Col161] bit NULL,
		[Col162] bit NULL,
		[Col163] bit NULL,
		[Col164] bit NULL,
		[Col165] bit NULL,
		[Col166] bit NULL,
		[Col167] bit NULL,
		[Col168] bit NULL,
		[Col169] bit NULL,
		[Col170] bit NULL,
		[Col171] bit NULL,
		[Col172] bit NULL,
		[Col173] bit NULL,
		[Col174] bit NULL,
		[Col175] bit NULL,
		[Col176] bit NULL,
		[Col177] bit NULL,
		[Col178] bit NULL,
		[Col179] bit NULL,
		[Col180] bit NULL,
		[Col181] bit NULL,
		[Col182] bit NULL,
		[Col183] bit NULL,
		[Col184] bit NULL,
		[Col185] bit NULL,
		[Col186] bit NULL,
		[Col187] bit NULL,
		[Col188] bit NULL,
		[Col189] bit NULL,
		[Col190] bit NULL,
		[Col191] bit NULL,
		[Col192] bit NULL,
		[Col193] bit NULL,
		[Col194] bit NULL,
		[Col195] bit NULL,
		[Col196] bit NULL,
		[Col197] bit NULL,
		[Col198] bit NULL,
		[Col199] bit NULL,
		[Col200] bit NULL,
		[Col201] bit NULL,
		[Col202] bit NULL,
		[Col203] bit NULL,
		[Col204] bit NULL,
		[Col205] bit NULL,
		[Col206] bit NULL,
		[Col207] bit NULL,
		[Col208] bit NULL,
		[Col209] bit NULL,
		[Col210] bit NULL,
		[Col211] bit NULL,
		[Col212] bit NULL,
		[Col213] bit NULL,
		[Col214] bit NULL,
		[Col215] bit NULL,
		[Col216] bit NULL,
		[Col217] bit NULL,
		[Col218] bit NULL,
		[Col219] bit NULL,
		[Col220] bit NULL,
		[Col221] bit NULL,
		[Col222] bit NULL,
		[Col223] bit NULL,
		[Col224] bit NULL,
		[Col225] bit NULL,
		[Col226] bit NULL,
		[Col227] bit NULL,
		[Col228] bit NULL,
		[Col229] bit NULL,
		[Col230] bit NULL,
		[Col231] bit NULL,
		[Col232] bit NULL,
		[Col233] bit NULL,
		[Col234] bit NULL,
		[Col235] bit NULL,
		[Col236] bit NULL,
		[Col237] bit NULL,
		[Col238] bit NULL,
		[Col239] bit NULL,
		[Col240] bit NULL,
		[Col241] bit NULL,
		[Col242] bit NULL,
		[Col243] bit NULL,
		[Col244] bit NULL,
		[Col245] bit NULL,
		[Col246] bit NULL,
		[Col247] bit NULL,
		[Col248] bit NULL,
		[Col249] bit NULL,
		[Col250] bit NULL,
		[Col251] bit NULL,
		[Col252] bit NULL,
		[Col253] bit NULL,
		[Col254] bit NULL,
		[Col255] bit NULL,
		[Col256] bit NULL,
		[Col257] bit NULL,
		[Col258] bit NULL,
		[Col259] bit NULL,
		[Col260] bit NULL,
		[Col261] bit NULL,
		[Col262] bit NULL,
		[Col263] bit NULL,
		[Col264] bit NULL,
		[Col265] bit NULL,
		[Col266] bit NULL,
		[Col267] bit NULL,
		[Col268] bit NULL,
		[Col269] bit NULL,
		[Col270] bit NULL,
		[Col271] bit NULL,
		[Col272] bit NULL,
		[Col273] bit NULL,
		[Col274] bit NULL,
		[Col275] bit NULL,
		[Col276] bit NULL,
		[Col277] bit NULL,
		[Col278] bit NULL,
		[Col279] bit NULL,
		[Col280] bit NULL,
		[Col281] bit NULL,
		[Col282] bit NULL,
		[Col283] bit NULL,
		[Col284] bit NULL,
		[Col285] bit NULL,
		[Col286] bit NULL,
		[Col287] bit NULL,
		[Col288] bit NULL,
		[Col289] bit NULL,
		[Col290] bit NULL,
		[Col291] bit NULL,
		[Col292] bit NULL,
		[Col293] bit NULL,
		[Col294] bit NULL,
		[Col295] bit NULL,
		[Col296] bit NULL,
		[Col297] bit NULL,
		[Col298] bit NULL,
		[Col299] bit NULL,
		[Col300] bit NULL,
		[Col301] bit NULL,
		[Col302] bit NULL,
		[Col303] bit NULL,
		[Col304] bit NULL,
		[Col305] bit NULL,
		[Col306] bit NULL,
		[Col307] bit NULL,
		[Col308] bit NULL,
		[Col309] bit NULL,
		[Col310] bit NULL,
		[Col311] bit NULL,
		[Col312] bit NULL,
		[Col313] bit NULL,
		[Col314] bit NULL,
		[Col315] bit NULL,
		[Col316] bit NULL,
		[Col317] bit NULL,
		[Col318] bit NULL,
		[Col319] bit NULL,
		[Col320] bit NULL,
		[Col321] bit NULL,
		[Col322] bit NULL,
		[Col323] bit NULL,
		[Col324] bit NULL,
		[Col325] bit NULL,
		[Col326] bit NULL,
		[Col327] bit NULL,
		[Col328] bit NULL,
		[Col329] bit NULL,
		[Col330] bit NULL,
		[Col331] bit NULL,
		[Col332] bit NULL,
		[Col333] bit NULL,
		[Col334] bit NULL,
		[Col335] bit NULL,
		[Col336] bit NULL,
		[Col337] bit NULL,
		[Col338] bit NULL,
		[Col339] bit NULL,
		[Col340] bit NULL,
		[Col341] bit NULL,
		[Col342] bit NULL,
		[Col343] bit NULL,
		[Col344] bit NULL,
		[Col345] bit NULL,
		[Col346] bit NULL,
		[Col347] bit NULL,
		[Col348] bit NULL,
		[Col349] bit NULL,
		[Col350] bit NULL,
		[Col351] bit NULL,
		[Col352] bit NULL,
		[Col353] bit NULL,
		[Col354] bit NULL,
		[Col355] bit NULL,
		[Col356] bit NULL,
		[Col357] bit NULL,
		[Col358] bit NULL,
		[Col359] bit NULL,
		[Col360] bit NULL,
		[Col361] bit NULL,
		[Col362] bit NULL,
		[Col363] bit NULL,
		[Col364] bit NULL,
		[Col365] bit NULL,
		[Col366] bit NULL,
		[Col367] bit NULL,
		[Col368] bit NULL,
		[Col369] bit NULL,
		[Col370] bit NULL,
		[Col371] bit NULL,
		[Col372] bit NULL,
		[Col373] bit NULL,
		[Col374] bit NULL,
		[Col375] bit NULL,
		[Col376] bit NULL,
		[Col377] bit NULL,
		[Col378] bit NULL,
		[Col379] bit NULL,
		[Col380] bit NULL,
		[Col381] bit NULL,
		[Col382] decimal(9,2) NULL,
		[Col383] decimal(9,2) NULL,
		[Col384] bit NULL,
		[Col385] bit NULL,
		[Col386] bit NULL,
		[Col387] bit NULL,
		[Col388] bit NULL,
		[Col389] bit NULL,
		[Col390] bit NULL,
		[Col391] bit NULL,
		[Col392] bit NULL,
		[Col393] bit NULL,
		[Col394] bit NULL,
		[Col395] bit NULL,
		[Col396] bit NULL,
		[Col397] bit NULL,
		[Col398] bit NULL,
		[Col399] bit NULL,
 CONSTRAINT [PK_ChannelValue] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO


SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChannelValueLookup]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueLookup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChannelValueLookup](
	[ChannelID] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[DisplayValue] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ChannelValueLookup] PRIMARY KEY CLUSTERED 
(
	[ChannelID] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Chart]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chart]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Chart](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [smallint] NULL,
	[Name] [varchar](100) NOT NULL,
	[IsSystem] [bit] NOT NULL CONSTRAINT [DF_Chart_IsSystem]  DEFAULT ((0)),
	[Ticks] [tinyint] NOT NULL CONSTRAINT [DF_Chart_Ticks]  DEFAULT ((10)),
 CONSTRAINT [PK_Chart] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ChartChannel]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChartChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChartChannel](
	[ChartID] [int] NOT NULL,
	[ChannelID] [int] NOT NULL,
	[Scale] [decimal](9, 2) NULL,
	[Sequence] [int] NOT NULL,
 CONSTRAINT [PK_ChartChannel] PRIMARY KEY CLUSTERED 
(
	[ChartID] ASC,
	[ChannelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Configuration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Configuration](
	[PropertyName] [varchar](50) NOT NULL,
	[PropertyValue] [varchar](500) NOT NULL,
 CONSTRAINT [PK_CL_Configuration] PRIMARY KEY CLUSTERED 
(
	[PropertyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[DataArchiveLog]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataArchiveLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DataArchiveLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Eventdate] [datetime2](7) NULL,
	[LogDesc] [varchar](500) NULL,
	[LogLocation] [varchar](100) NULL,
 CONSTRAINT [PK_DataArchiveLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[DelayReason]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DelayReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DelayReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Priority] [varchar](20) NULL,
	[ChartColor] [char](6) NULL,
 CONSTRAINT [PK_DelayReason] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Fault]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fault](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[HeadCode] [varchar](50) NULL,
	[SetCode] [varchar](50) NULL,
	[LocationID] [int] NULL,
	[IsCurrent] [bit] NOT NULL CONSTRAINT [DF_Fault_IsCurrent]  DEFAULT ((1)),
	[EndTime] [datetime] NULL,
	[RecoveryID] [int] NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[FaultMetaID] [int] NOT NULL,
	[FaultUnitID] [int] NULL,
	[IsDelayed] [int] NULL,
	[PrimaryUnitID] [int] NOT NULL,
	[SecondaryUnitID] [int] NULL,
	[TertiaryUnitID] [int] NULL,
	[IsAcknowledged] [bit] NOT NULL CONSTRAINT [DF_Fault_IsAcknowledged]  DEFAULT ((0)),
	[AcknowledgedTime] [datetime] NULL,
	[RecoveryStatus] [varchar](16) NULL,
 CONSTRAINT [PK_Fault] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FaultCategory]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[ReportingOnly] [bit] NOT NULL CONSTRAINT [DF_FaultCategory_ReportingOnly]  DEFAULT ((0)),
 CONSTRAINT [PK_FaultCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO


/****** Object:  Table [dbo].[FaultChannelValue]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultChannelValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultChannelValue](
	[ID] [bigint] NOT NULL,
	[FaultID] [int] NOT NULL,
	[FaultUnit] [bit] NOT NULL CONSTRAINT [DF_FaultChannelValue_FaultUnit]  DEFAULT ((0)),
	[UpdateRecord] [bit] NOT NULL,
	[UnitID] [int] NOT NULL,
	[RecordInsert] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
		[Col1] decimal (9,6) NULL,
		[Col2] decimal (9,6) NULL,
		[Col3] bit NULL,
		[Col4] bit NULL,
		[Col5] bit NULL,
		[Col6] bit NULL,
		[Col7] bit NULL,
		[Col8] bit NULL,
		[Col9] bit NULL,
		[Col10] bit NULL,
		[Col11] bit NULL,
		[Col12] bit NULL,
		[Col13] bit NULL,
		[Col14] bit NULL,
		[Col15] bit NULL,
		[Col16] bit NULL,
		[Col17] bit NULL,
		[Col18] bit NULL,
		[Col19] bit NULL,
		[Col20] bit NULL,
		[Col21] bit NULL,
		[Col22] bit NULL,
		[Col23] bit NULL,
		[Col24] bit NULL,
		[Col25] bit NULL,
		[Col26] bit NULL,
		[Col27] bit NULL,
		[Col28] bit NULL,
		[Col29] bit NULL,
		[Col30] bit NULL,
		[Col31] bit NULL,
		[Col32] bit NULL,
		[Col33] bit NULL,
		[Col34] bit NULL,
		[Col35] bit NULL,
		[Col36] bit NULL,
		[Col37] bit NULL,
		[Col38] bit NULL,
		[Col39] bit NULL,
		[Col40] bit NULL,
		[Col41] bit NULL,
		[Col42] bit NULL,
		[Col43] bit NULL,
		[Col44] bit NULL,
		[Col45] bit NULL,
		[Col46] bit NULL,
		[Col47] bit NULL,
		[Col48] bit NULL,
		[Col49] bit NULL,
		[Col50] bit NULL,
		[Col51] bit NULL,
		[Col52] bit NULL,
		[Col53] bit NULL,
		[Col54] bit NULL,
		[Col55] bit NULL,
		[Col56] bit NULL,
		[Col57] bit NULL,
		[Col58] bit NULL,
		[Col59] bit NULL,
		[Col60] bit NULL,
		[Col61] bit NULL,
		[Col62] bit NULL,
		[Col63] bit NULL,
		[Col64] bit NULL,
		[Col65] bit NULL,
		[Col66] bit NULL,
		[Col67] bit NULL,
		[Col68] bit NULL,
		[Col69] bit NULL,
		[Col70] bit NULL,
		[Col71] bit NULL,
		[Col72] bit NULL,
		[Col73] bit NULL,
		[Col74] bit NULL,
		[Col75] bit NULL,
		[Col76] bit NULL,
		[Col77] bit NULL,
		[Col78] bit NULL,
		[Col79] bit NULL,
		[Col80] bit NULL,
		[Col81] bit NULL,
		[Col82] bit NULL,
		[Col83] bit NULL,
		[Col84] bit NULL,
		[Col85] bit NULL,
		[Col86] bit NULL,
		[Col87] bit NULL,
		[Col88] bit NULL,
		[Col89] bit NULL,
		[Col90] bit NULL,
		[Col91] bit NULL,
		[Col92] bit NULL,
		[Col93] bit NULL,
		[Col94] bit NULL,
		[Col95] bit NULL,
		[Col96] bit NULL,
		[Col97] bit NULL,
		[Col98] bit NULL,
		[Col99] bit NULL,
		[Col100] bit NULL,
		[Col101] bit NULL,
		[Col102] bit NULL,
		[Col103] bit NULL,
		[Col104] bit NULL,
		[Col105] bit NULL,
		[Col106] bit NULL,
		[Col107] bit NULL,
		[Col108] bit NULL,
		[Col109] bit NULL,
		[Col110] bit NULL,
		[Col111] bit NULL,
		[Col112] bit NULL,
		[Col113] bit NULL,
		[Col114] bit NULL,
		[Col115] bit NULL,
		[Col116] bit NULL,
		[Col117] bit NULL,
		[Col118] bit NULL,
		[Col119] bit NULL,
		[Col120] bit NULL,
		[Col121] bit NULL,
		[Col122] bit NULL,
		[Col123] bit NULL,
		[Col124] bit NULL,
		[Col125] bit NULL,
		[Col126] bit NULL,
		[Col127] bit NULL,
		[Col128] bit NULL,
		[Col129] bit NULL,
		[Col130] bit NULL,
		[Col131] bit NULL,
		[Col132] bit NULL,
		[Col133] bit NULL,
		[Col134] bit NULL,
		[Col135] bit NULL,
		[Col136] bit NULL,
		[Col137] bit NULL,
		[Col138] bit NULL,
		[Col139] bit NULL,
		[Col140] bit NULL,
		[Col141] bit NULL,
		[Col142] bit NULL,
		[Col143] bit NULL,
		[Col144] bit NULL,
		[Col145] bit NULL,
		[Col146] bit NULL,
		[Col147] bit NULL,
		[Col148] bit NULL,
		[Col149] bit NULL,
		[Col150] bit NULL,
		[Col151] bit NULL,
		[Col152] bit NULL,
		[Col153] bit NULL,
		[Col154] bit NULL,
		[Col155] bit NULL,
		[Col156] bit NULL,
		[Col157] bit NULL,
		[Col158] bit NULL,
		[Col159] bit NULL,
		[Col160] bit NULL,
		[Col161] bit NULL,
		[Col162] bit NULL,
		[Col163] bit NULL,
		[Col164] bit NULL,
		[Col165] bit NULL,
		[Col166] bit NULL,
		[Col167] bit NULL,
		[Col168] bit NULL,
		[Col169] bit NULL,
		[Col170] bit NULL,
		[Col171] bit NULL,
		[Col172] bit NULL,
		[Col173] bit NULL,
		[Col174] bit NULL,
		[Col175] bit NULL,
		[Col176] bit NULL,
		[Col177] bit NULL,
		[Col178] bit NULL,
		[Col179] bit NULL,
		[Col180] bit NULL,
		[Col181] bit NULL,
		[Col182] bit NULL,
		[Col183] bit NULL,
		[Col184] bit NULL,
		[Col185] bit NULL,
		[Col186] bit NULL,
		[Col187] bit NULL,
		[Col188] bit NULL,
		[Col189] bit NULL,
		[Col190] bit NULL,
		[Col191] bit NULL,
		[Col192] bit NULL,
		[Col193] bit NULL,
		[Col194] bit NULL,
		[Col195] bit NULL,
		[Col196] bit NULL,
		[Col197] bit NULL,
		[Col198] bit NULL,
		[Col199] bit NULL,
		[Col200] bit NULL,
		[Col201] bit NULL,
		[Col202] bit NULL,
		[Col203] bit NULL,
		[Col204] bit NULL,
		[Col205] bit NULL,
		[Col206] bit NULL,
		[Col207] bit NULL,
		[Col208] bit NULL,
		[Col209] bit NULL,
		[Col210] bit NULL,
		[Col211] bit NULL,
		[Col212] bit NULL,
		[Col213] bit NULL,
		[Col214] bit NULL,
		[Col215] bit NULL,
		[Col216] bit NULL,
		[Col217] bit NULL,
		[Col218] bit NULL,
		[Col219] bit NULL,
		[Col220] bit NULL,
		[Col221] bit NULL,
		[Col222] bit NULL,
		[Col223] bit NULL,
		[Col224] bit NULL,
		[Col225] bit NULL,
		[Col226] bit NULL,
		[Col227] bit NULL,
		[Col228] bit NULL,
		[Col229] bit NULL,
		[Col230] bit NULL,
		[Col231] bit NULL,
		[Col232] bit NULL,
		[Col233] bit NULL,
		[Col234] bit NULL,
		[Col235] bit NULL,
		[Col236] bit NULL,
		[Col237] bit NULL,
		[Col238] bit NULL,
		[Col239] bit NULL,
		[Col240] bit NULL,
		[Col241] bit NULL,
		[Col242] bit NULL,
		[Col243] bit NULL,
		[Col244] bit NULL,
		[Col245] bit NULL,
		[Col246] bit NULL,
		[Col247] bit NULL,
		[Col248] bit NULL,
		[Col249] bit NULL,
		[Col250] bit NULL,
		[Col251] bit NULL,
		[Col252] bit NULL,
		[Col253] bit NULL,
		[Col254] bit NULL,
		[Col255] bit NULL,
		[Col256] bit NULL,
		[Col257] bit NULL,
		[Col258] bit NULL,
		[Col259] bit NULL,
		[Col260] bit NULL,
		[Col261] bit NULL,
		[Col262] bit NULL,
		[Col263] bit NULL,
		[Col264] bit NULL,
		[Col265] bit NULL,
		[Col266] bit NULL,
		[Col267] bit NULL,
		[Col268] bit NULL,
		[Col269] bit NULL,
		[Col270] bit NULL,
		[Col271] bit NULL,
		[Col272] bit NULL,
		[Col273] bit NULL,
		[Col274] bit NULL,
		[Col275] bit NULL,
		[Col276] bit NULL,
		[Col277] bit NULL,
		[Col278] bit NULL,
		[Col279] bit NULL,
		[Col280] bit NULL,
		[Col281] bit NULL,
		[Col282] bit NULL,
		[Col283] bit NULL,
		[Col284] bit NULL,
		[Col285] bit NULL,
		[Col286] bit NULL,
		[Col287] bit NULL,
		[Col288] bit NULL,
		[Col289] bit NULL,
		[Col290] bit NULL,
		[Col291] bit NULL,
		[Col292] bit NULL,
		[Col293] bit NULL,
		[Col294] bit NULL,
		[Col295] bit NULL,
		[Col296] bit NULL,
		[Col297] bit NULL,
		[Col298] bit NULL,
		[Col299] bit NULL,
		[Col300] bit NULL,
		[Col301] bit NULL,
		[Col302] bit NULL,
		[Col303] bit NULL,
		[Col304] bit NULL,
		[Col305] bit NULL,
		[Col306] bit NULL,
		[Col307] bit NULL,
		[Col308] bit NULL,
		[Col309] bit NULL,
		[Col310] bit NULL,
		[Col311] bit NULL,
		[Col312] bit NULL,
		[Col313] bit NULL,
		[Col314] bit NULL,
		[Col315] bit NULL,
		[Col316] bit NULL,
		[Col317] bit NULL,
		[Col318] bit NULL,
		[Col319] bit NULL,
		[Col320] bit NULL,
		[Col321] bit NULL,
		[Col322] bit NULL,
		[Col323] bit NULL,
		[Col324] bit NULL,
		[Col325] bit NULL,
		[Col326] bit NULL,
		[Col327] bit NULL,
		[Col328] bit NULL,
		[Col329] bit NULL,
		[Col330] bit NULL,
		[Col331] bit NULL,
		[Col332] bit NULL,
		[Col333] bit NULL,
		[Col334] bit NULL,
		[Col335] bit NULL,
		[Col336] bit NULL,
		[Col337] bit NULL,
		[Col338] bit NULL,
		[Col339] bit NULL,
		[Col340] bit NULL,
		[Col341] bit NULL,
		[Col342] bit NULL,
		[Col343] bit NULL,
		[Col344] bit NULL,
		[Col345] bit NULL,
		[Col346] bit NULL,
		[Col347] bit NULL,
		[Col348] bit NULL,
		[Col349] bit NULL,
		[Col350] bit NULL,
		[Col351] bit NULL,
		[Col352] bit NULL,
		[Col353] bit NULL,
		[Col354] bit NULL,
		[Col355] bit NULL,
		[Col356] bit NULL,
		[Col357] bit NULL,
		[Col358] bit NULL,
		[Col359] bit NULL,
		[Col360] bit NULL,
		[Col361] bit NULL,
		[Col362] bit NULL,
		[Col363] bit NULL,
		[Col364] bit NULL,
		[Col365] bit NULL,
		[Col366] bit NULL,
		[Col367] bit NULL,
		[Col368] bit NULL,
		[Col369] bit NULL,
		[Col370] bit NULL,
		[Col371] bit NULL,
		[Col372] bit NULL,
		[Col373] bit NULL,
		[Col374] bit NULL,
		[Col375] bit NULL,
		[Col376] bit NULL,
		[Col377] bit NULL,
		[Col378] bit NULL,
		[Col379] bit NULL,
		[Col380] bit NULL,
		[Col381] bit NULL,
		[Col382] bit NULL,
		[Col383] bit NULL,
		[Col384] bit NULL,
		[Col385] bit NULL,
		[Col386] bit NULL,
		[Col387] bit NULL,
		[Col388] bit NULL,
		[Col389] bit NULL,
		[Col390] bit NULL,
		[Col391] bit NULL,
		[Col392] bit NULL,
		[Col393] bit NULL,
		[Col394] bit NULL,
		[Col395] bit NULL,
		[Col396] bit NULL,
		[Col397] bit NULL,
		[Col398] bit NULL,
		[Col399] bit NULL,
CONSTRAINT [PK_FaultChannelValue] PRIMARY KEY CLUSTERED 
(
	[FaultID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[FaultComment]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultComment](
	[FaultID] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime2](3) NOT NULL,
	[UserID] [int] NOT NULL,
	[Comment] [varchar](1000) NOT NULL,
 CONSTRAINT [PK_CL_FaultComment] PRIMARY KEY CLUSTERED 
(
	[FaultID] ASC,
	[Timestamp] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FaultMeta]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultMeta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultMeta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FaultCode] [varchar](100) NOT NULL,
	[Description] [varchar](100) NULL,
	[Summary] [varchar](1000) NULL,
	[CategoryID] [int] NOT NULL,
	[RecoveryProcessPath] [varchar](100) NULL,
	[FaultTypeID] [tinyint] NOT NULL,
	[Url] [varchar](500) NULL,
	[GroupID] [int] NULL,
 CONSTRAINT [PK_FaultMeta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_FaultMeta_FaultCode] UNIQUE NONCLUSTERED 
(
	[FaultCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FaultMetaE2M]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultMetaE2M]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultMetaE2M](
	[FaultMetaID] [int] NOT NULL,
	[E2MFaultCode] [varchar](10) NOT NULL,
	[E2MDescription] [varchar](100) NULL,
	[E2MPriorityCode] [varchar](10) NOT NULL,
	[E2MPriority] [varchar](100) NOT NULL,
 CONSTRAINT [PK_FaultMetaE2M] PRIMARY KEY CLUSTERED 
(
	[FaultMetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FaultType]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FaultType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FaultType](
	[ID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DisplayColor] [varchar](10) NOT NULL,
	[Priority] [tinyint] NOT NULL,
 CONSTRAINT [PK_CL_FaultType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FileFieldDefinition]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileFieldDefinition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileFieldDefinition](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[FileTypeID] [smallint] NOT NULL,
	[ChannelID] [int] NULL,
	[FieldType] [varchar](20) NOT NULL,
	[FieldPosition] [tinyint] NULL,
	[AddressByte] [smallint] NULL,
	[AddressBit] [tinyint] NULL,
	[AddressLength] [tinyint] NULL,
	[ScalingGain] [decimal](9, 6) NULL,
	[ScalingOffset] [decimal](9, 6) NULL,
	[ScalingInversion] [bit] NULL,
	[Source] [varchar](20) NULL,
	[ProcessingAlgorithm] [varchar](20) NULL,
	[ProcessingPattern] [varchar](50) NULL,
	[IdentificationTag] [varchar](50) NULL,
	[Note] [varchar](1000) NULL,
	[ConditionFieldID] [smallint] NULL,
	[ConditionOperator] [varchar](3) NULL,
	[ConditionValue] [smallint] NULL,
	[LookupInsert] [bit] NULL,
	[AddressByteConditionFieldID] [smallint] NULL,
	[AddressByteConditionOperator] [varchar](3) NULL,
	[AddressByteConditionValue] [smallint] NULL,
	[AddressByteFactor] [smallint] NULL,
	[DependenceFieldID] [smallint] NULL,
 CONSTRAINT [PK_FileDefinition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FileType]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[FilenameFieldSeparator] [char](1) NOT NULL,
	[FileDataType] [varchar](10) NULL,
	[FieldSeparator] [char](1) NULL,
	[TextQualifier] [char](1) NULL,
	[Processor] [varchar](20) NOT NULL,
	[StoredProcedureName] [varchar](100) NOT NULL,
	[HeaderRowsToSkip] [smallint] NOT NULL,
 CONSTRAINT [Key5] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FirmwareCurrentStatus]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FirmwareCurrentStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FirmwareCurrentStatus](
	[StatusTime] [datetime] NOT NULL CONSTRAINT [DF_FirmwareCurrentStatus_StatusTime]  DEFAULT (sysdatetime()),
	[Id] [int] NOT NULL,
	[FirmwareVersion] [nvarchar](20) NULL,
	[ConfigurationVersion] [nvarchar](20) NULL,
	[ConfigurationDateTime] [datetime] NULL,
	[VehicleNumber] [nvarchar](16) NULL,
	[FirmwareUpdateAvailableVersion] [nvarchar](100) NULL,
	[ConfigurationUpdateAvailableVersion] [nvarchar](100) NULL,
	[FirmwareUpdateURI] [nvarchar](255) NULL,
	[ConfigurationUpdateURI] [nvarchar](255) NULL,
	[RMDLocation] [int] NULL,
	[FirmwareUpdateImmediate] [bit] NULL,
	[ConfigurationUpdateImmediate] [bit] NULL,
	[FirmwareUpdateForce] [bit] NULL,
	[ConfigurationUpdateForce] [bit] NULL,
	[LastPowerOn] [datetime] NULL,
	[RecordValid] [bit] NULL,
	[FirmwareUpdateAvailable] [bit] NULL,
	[ConfigurationUpdateAvailable] [bit] NULL,
	[SerialNumber] [nvarchar](100) NULL,
	[WheelDia1] [int] NULL,
	[WheelDia2] [int] NULL,
 CONSTRAINT [PK_FirmwareCurrentStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[StatusTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[FirmwareUpdateSchedule]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FirmwareUpdateSchedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FirmwareUpdateSchedule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[FirmwareUpdateAvailableVersion] [varchar](8) NULL,
	[ConfigurationUpdateAvailableVersion] [varchar](8) NULL,
	[FirmwareUpdateURI] [varchar](255) NULL,
	[ConfigurationUpdateURI] [varchar](255) NULL,
	[FirmwareUpdateImmediate] [bit] NULL,
	[ConfigurationUpdateImmediate] [bit] NULL,
	[FirmwareUpdateForce] [bit] NULL,
	[ConfigurationUpdateForce] [bit] NULL,
	[RecordValid] [bit] NULL,
	[FirmwareUpdateAvailable] [bit] NULL,
	[ConfigurationUpdateAvailable] [bit] NULL,
	[TimestampCreated] [datetime] NOT NULL,
	[TimestampScheduled] [datetime] NOT NULL,
	[TimestampApplied] [datetime] NULL,
	[TimestampCompleted] [datetime] NULL,
	[FirmwareUpdateStatus] [varchar](20) NULL,
	[ConfigurationUpdateStatus] [varchar](20) NULL,
 CONSTRAINT [PK_FirmwareUpdateSchedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Fleet]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fleet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Fleet](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NOT NULL,
 CONSTRAINT [PK_CL_Fleet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_U_Fleet_Code] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_U_Fleet_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FleetChannel]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FleetChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FleetChannel](
	[FleetID] [smallint] NOT NULL,
	[ChannelID] [int] NOT NULL,
 CONSTRAINT [PK_CL_FleetChannel] PRIMARY KEY CLUSTERED 
(
	[FleetID] ASC,
	[ChannelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[FleetFormation]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FleetFormation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FleetFormation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnitIDList] [varchar](100) NOT NULL,
	[UnitNumberList] [varchar](100) NOT NULL,
	[ValidFrom] [datetime2](3) NOT NULL,
	[ValidTo] [datetime2](3) NULL,
 CONSTRAINT [PK_FleetFormation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FleetStatus]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FleetStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FleetStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ValidFrom] [datetime2](3) NOT NULL,
	[ValidTo] [datetime2](3) NULL,
	[Setcode] [varchar](50) NULL,
	[Diagram] [varchar](50) NULL,
	[Headcode] [varchar](50) NULL,
	[IsValid] [bit] NULL,
	[Loading] [tinyint] NULL,
	[ConfigDate] [datetime2](3) NULL,
	[UnitList] [varchar](100) NULL,
	[LocationIDHeadcodeStart] [int] NULL,
	[LocationIDHeadcodeEnd] [int] NULL,
	[UnitID] [int] NOT NULL,
	[UnitPosition] [tinyint] NOT NULL,
	[UnitOrientation] [bit] NOT NULL CONSTRAINT [DF_FleetStatus_UnitOrientation]  DEFAULT ((1)),
	[FleetFormationID] [int] NULL,
 CONSTRAINT [PK_FleetStatus] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FleetStatusHistory]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FleetStatusHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FleetStatusHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ValidFrom] [datetime2](3) NULL,
	[ValidTo] [datetime2](3) NULL,
	[Setcode] [varchar](50) NULL,
	[Diagram] [varchar](50) NULL,
	[Headcode] [varchar](50) NULL,
	[IsValid] [bit] NULL,
	[Loading] [tinyint] NULL,
	[ConfigDate] [datetime2](3) NULL,
	[UnitList] [varchar](100) NULL,
	[LocationIDHeadcodeStart] [int] NULL,
	[LocationIDHeadcodeEnd] [int] NULL,
	[UnitID] [int] NOT NULL,
	[UnitPosition] [tinyint] NOT NULL,
	[UnitOrientation] [bit] NOT NULL,
	[FleetFormationID] [int] NULL,
 CONSTRAINT [PK_FleetStatusHistory] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[FleetStatusStaging]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FleetStatusStaging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnitId] [int] NOT NULL,
	[DiagramNumber] [varchar](10) NOT NULL,
	[HeadCode] [varchar](10) NOT NULL,
	[TrainStartLocationId] [int] NULL,
	[TrainStartLocationTiploc] [varchar](10) NOT NULL,
	[TrainStartTime] [datetime] NOT NULL,
	[TrainEndLocationId] [int] NULL,
	[TrainEndLocationTiploc] [varchar](10) NULL,
	[TrainEndTime] [datetime] NOT NULL,
	[StartLocationId] [int] NULL,
	[StartLocationTiploc] [varchar](10) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndLocationId] [int] NULL,
	[EndLocationTipLoc] [varchar](10) NULL,
	[EndTime] [datetime] NOT NULL,
	[Length] [decimal](5, 2) NOT NULL,
	[CoupledUnits] [varchar](55) NULL,
	[UnitPosition] [int] NULL,
	[VehicleFormation] [varchar](200) NULL,
	[SequenceNumber] [int] NOT NULL CONSTRAINT [DF_FleetStatusStaging_SequenceNumber]  DEFAULT ((1)),
 CONSTRAINT [PK_C_FleetStatusStaging] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[GeniusInterfaceData]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeniusInterfaceData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GeniusInterfaceFileId] [int] NULL,
	[DiagramId] [varchar](50) NULL,
	[UnitNumber] [varchar](50) NULL,
	[HeadCode] [varchar](50) NULL,
	[TrainStartLocation] [varchar](50) NULL,
	[TrainStartTime] [varchar](50) NULL,
	[LegStartLocation] [varchar](50) NULL,
	[LegStartTime] [varchar](50) NULL,
	[LegEndLocation] [varchar](50) NULL,
	[LegEndTime] [varchar](50) NULL,
	[LegLength] [varchar](50) NULL,
	[VehicleFormation] [varchar](255) NULL,
	[CoupledUnits] [varchar](255) NULL,
	[UnitPosition] [varchar](50) NULL,
	[SequenceNumber] [varchar](50) NULL,
 CONSTRAINT [PK_CL_GeniusInterfaceData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[GeniusInterfaceFile]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeniusInterfaceFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[FileTimeStamp] [datetime] NOT NULL,
	[ImportTimeStamp] [datetime] NOT NULL CONSTRAINT [DF_GeniusInterfaceFile_ImportTimeStamp]  DEFAULT (getdate()),
	[ProcessingStatus] [varchar](100) NOT NULL CONSTRAINT [DF_GeniusInterfaceFile_ProcessingStatus]  DEFAULT ('Started processing file'),
 CONSTRAINT [PK_CL_GeniusInterfaceFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[GeniusLog]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeniusLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeniusLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GeniusInterfaceFileId] [int] NOT NULL,
	[LogTime] [datetime] NOT NULL CONSTRAINT [DF_GeniusLog_LogTime]  DEFAULT (getutcdate()),
	[LogType] [tinyint] NOT NULL CONSTRAINT [DF_GeniusLog_LogType]  DEFAULT ((1)),
	[LogText] [varchar](500) NOT NULL,
 CONSTRAINT [PK_C_GeniusLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[GeniusStaging]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeniusStaging]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeniusStaging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GeniusInterfaceFileID] [int] NOT NULL,
	[DiagramNumber] [varchar](10) NOT NULL,
	[UnitNumber] [varchar](16) NOT NULL,
	[HeadCode] [varchar](10) NOT NULL,
	[TrainStartLocation] [varchar](7) NOT NULL,
	[TrainStartTime] [datetime] NOT NULL,
	[TrainEndLocation] [varchar](7) NULL,
	[TrainEndTime] [datetime] NULL,
	[LegStartLocation] [varchar](7) NOT NULL,
	[LegStartTime] [datetime] NOT NULL,
	[LegEndLocation] [varchar](7) NOT NULL,
	[LegEndTime] [datetime] NOT NULL,
	[LegLength] [decimal](5, 2) NOT NULL,
	[VehicleFormation] [varchar](255) NULL,
	[CoupledUnits] [varchar](55) NULL,
	[UnitPosition] [tinyint] NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [PK_CL_GeniusStaging] PRIMARY KEY CLUSTERED 
(
	[GeniusInterfaceFileID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[GmtToBstConversion]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GmtToBstConversion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GmtToBstConversion](
	[GmtDatetimeFrom] [datetime] NOT NULL,
	[GmtDatetimeTo] [datetime] NOT NULL,
	[TimeOffset] [tinyint] NULL,
 CONSTRAINT [PK_CL_GmtToBstConversion] PRIMARY KEY CLUSTERED 
(
	[GmtDatetimeFrom] ASC,
	[GmtDatetimeTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[HardwareChannel]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HardwareChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HardwareChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HardwareTypeID] [smallint] NOT NULL,
	[ChannelID] [int] NULL,
	[Ref] [varchar](50) NULL,
	[ScalingGain] [decimal](10, 6) NULL,
	[ScalingOffset] [decimal](10, 4) NULL,
	[ScalingInversion] [bit] NULL,
	[RMD] [smallint] NULL,
	[Source] [smallint] NULL,
	[Card] [smallint] NULL,
	[Channel] [smallint] NULL,
 CONSTRAINT [PK_HardwareChannel] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[HardwareType]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HardwareType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HardwareType](
	[ID] [smallint] NOT NULL,
	[TypeName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_HardwareType] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[IdealRun]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IdealRun]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IdealRun](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JourneyID] [int] NOT NULL,
	[SegmentID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[LeadingVehicleID] [int] NULL,
	[StartDatetime] [datetime] NULL,
	[SectionTime] [int] NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
 CONSTRAINT [PK_IdealRun] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[IdealRunChannelValue]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IdealRunChannelValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IdealRunChannelValue](
	[ID] [bigint] NOT NULL,
	[IdealRunID] [int] NOT NULL,
	[UpdateRecord] [bit] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[UnitID] [int] NULL,
	[RecordInsert] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[IsComplete] [bit] NOT NULL,
	[Col1] [decimal](9, 4) NULL,
	[Col2] [decimal](9, 4) NULL,
	[Col3] [decimal](9, 6) NULL,
	[Col4] [decimal](9, 6) NULL,
	[Col5] [decimal](9, 4) NULL,
	[Col6] [decimal](9, 4) NULL,
	[Col7] [bit] NULL,
	[Col8] [bit] NULL,
	[Col9] [bit] NULL,
	[Col10] [bit] NULL,
	[Col11] [bit] NULL,
	[Col12] [bit] NULL,
	[Col13] [bit] NULL,
	[Col14] [bit] NULL,
	[Col15] [bit] NULL,
	[Col16] [bit] NULL,
	[Col17] [bit] NULL,
	[Col18] [bit] NULL,
	[Col19] [bit] NULL,
	[Col20] [bit] NULL,
	[Col21] [bit] NULL,
	[Col22] [bit] NULL,
	[Col23] [bit] NULL,
	[Col24] [bit] NULL,
	[Col25] [bit] NULL,
	[Col26] [bit] NULL,
	[Col27] [bit] NULL,
	[Col28] [bit] NULL,
	[Col29] [bit] NULL,
	[Col30] [bit] NULL,
	[Col31] [bit] NULL,
	[Col32] [bit] NULL,
	[Col33] [bit] NULL,
	[Col34] [bit] NULL,
	[Col35] [bit] NULL,
	[Col36] [bit] NULL,
	[Col37] [bit] NULL,
	[Col38] [bit] NULL,
	[Col39] [bit] NULL,
	[Col40] [bit] NULL,
	[Col41] [bit] NULL,
	[Col42] [bit] NULL,
	[Col43] [bit] NULL,
	[Col44] [bit] NULL,
	[Col45] [bit] NULL,
	[Col46] [bit] NULL,
	[Col47] [bit] NULL,
	[Col48] [bit] NULL,
	[Col49] [bit] NULL,
	[Col50] [bit] NULL,
	[Col51] [bit] NULL,
	[Col52] [bit] NULL,
	[Col53] [bit] NULL,
	[Col54] [bit] NULL,
	[Col55] [bit] NULL,
	[Col56] [bit] NULL,
	[Col57] [bit] NULL,
	[Col58] [bit] NULL,
	[Col59] [bit] NULL,
	[Col60] [bit] NULL,
	[Col61] [bit] NULL,
	[Col62] [bit] NULL,
	[Col63] [bit] NULL,
	[Col64] [bit] NULL,
	[Col65] [bit] NULL,
	[Col66] [bit] NULL,
	[Col67] [bit] NULL,
	[Col68] [bit] NULL,
	[Col69] [bit] NULL,
	[Col70] [bit] NULL,
	[Col71] [bit] NULL,
	[Col72] [bit] NULL,
	[Col73] [bit] NULL,
	[Col74] [bit] NULL,
	[Col75] [bit] NULL,
	[Col76] [bit] NULL,
	[Col77] [bit] NULL,
	[Col78] [bit] NULL,
	[Col79] [bit] NULL,
	[Col80] [bit] NULL,
 CONSTRAINT [PK_IdealRunChannelValue] PRIMARY KEY NONCLUSTERED 
(
	[IdealRunID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[InterfaceFile]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterfaceFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InterfaceFile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileTypeID] [smallint] NULL,
	[ParentFileID] [int] NULL,
	[Filename] [varchar](200) NOT NULL,
	[ImportTimestamp] [datetime2](3) NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_InterfaceFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[InterfaceFileData]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterfaceFileData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InterfaceFileData](
	[InterfaceFileID] [int] NOT NULL,
	[Data] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_InterfaceFileData] PRIMARY KEY CLUSTERED 
(
	[InterfaceFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Journey]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Journey]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Journey](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Headcode] [varchar](10) NOT NULL,
	[StartLocationID] [int] NOT NULL,
	[EndLocationID] [int] NOT NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[TrainUID] [char](6) NOT NULL,
	[StpType] [char](1) NOT NULL,
	[DaysRun] [char](7) NOT NULL,
	[FileID] [int] NOT NULL,
	[LineNumber] [int] NOT NULL,
	[HeadcodeDesc] [varchar](50) NOT NULL,
	[LastFileID] [int] NOT NULL,
 CONSTRAINT [PK_Journey] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Journey_TMP]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Journey_TMP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Journey_TMP](
	[ID] [varchar](50) NULL,
	[Headcode] [varchar](50) NULL,
	[StartLocationID] [varchar](50) NULL,
	[EndLocationID] [varchar](50) NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
	[ValidFrom] [varchar](50) NULL,
	[ValidTo] [varchar](50) NULL,
	[TrainUID] [varchar](50) NULL,
	[StpType] [varchar](50) NULL,
	[DaysRun] [varchar](50) NULL,
	[FileID] [varchar](50) NULL,
	[LineNumber] [varchar](50) NULL,
	[HeadcodeDesc] [varchar](50) NULL,
	[LastFileID] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[JourneyDate]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[JourneyDate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[JourneyDate](
	[PermanentJourneyID] [int] NOT NULL,
	[DateValue] [date] NOT NULL,
	[ActualJourneyID] [int] NULL,
	[RunActualRunAnalysis] [bit] NULL,
	[TimestampAnalysisStarted] [datetime] NULL,
	[TimestampAnalysisCompleted] [datetime] NULL,
	[AnalysedSuccessfully] [bit] NULL,
 CONSTRAINT [PK_JourneyDate] PRIMARY KEY CLUSTERED 
(
	[PermanentJourneyID] ASC,
	[DateValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[JourneyDetail_TMP]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[JourneyDetail_TMP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[JourneyDetail_TMP](
	[PermanentJourneyID] [varchar](50) NULL,
	[DateValue] [varchar](50) NULL,
	[ActualJourneyID] [varchar](50) NULL,
	[RunActualRunAnalysis] [varchar](50) NULL,
	[TimestampAnalysisStarted] [varchar](50) NULL,
	[TimestampAnalysisCompleted] [varchar](50) NULL,
	[AnalysedSuccessfully] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LoadCifFile]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadCifFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoadCifFile](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](255) NOT NULL,
	[OriginalFileName] [varchar](255) NOT NULL,
	[FileTimeStamp] [datetime] NULL,
	[ImportTimeStamp] [datetime] NULL,
	[FileLastModified] [datetime] NULL,
	[Status] [varchar](255) NULL,
 CONSTRAINT [PK_LoadCifFile_Id] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LoadCifFileData]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadCifFileData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoadCifFileData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileID] [int] NOT NULL,
	[FileLineNumber] [int] NULL,
	[Data] [char](80) NOT NULL,
	[Status] [varchar](255) NULL,
 CONSTRAINT [PK_LoadCifFileData] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LoadCifFileFormat]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadCifFileFormat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoadCifFileFormat](
	[FieldType] [char](2) NOT NULL,
	[FieldOrder] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Size] [int] NOT NULL,
	[Format] [varchar](100) NOT NULL,
	[Comment] [varchar](500) NULL,
	[Header] [varchar](50) NOT NULL,
 CONSTRAINT [PK_LoadCifFileFormat] PRIMARY KEY NONCLUSTERED 
(
	[FieldType] ASC,
	[FieldOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LoadGeniusData]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadGeniusData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoadGeniusData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoadGeniusFileID] [int] NOT NULL,
	[UnitNumber] [varchar](25) NULL,
	[DatetimeStart] [varchar](25) NULL,
	[LocationStart] [varchar](25) NULL,
	[DatetimeEnd] [varchar](25) NULL,
	[LocationEnd] [varchar](25) NULL,
	[UnitPosition] [varchar](25) NULL,
	[Diagram] [varchar](25) NULL,
	[Headcode] [varchar](25) NULL,
 CONSTRAINT [PK_LoadGeniusData] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LoadGeniusFile]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadGeniusFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoadGeniusFile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](255) NOT NULL,
	[OriginalFileName] [varchar](255) NOT NULL,
	[FileTimeStamp] [datetime] NOT NULL,
	[ImportTimeStamp] [datetime] NOT NULL,
	[Status] [varchar](255) NOT NULL,
 CONSTRAINT [PK_LoadGeniusFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Location]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Location](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LocationCode] [varchar](10) NULL,
	[LocationName] [varchar](100) NOT NULL,
	[Tiploc] [varchar](20) NOT NULL,
	[Lat] [decimal](9, 6) NULL,
	[Lng] [decimal](9, 6) NULL,
	[Type] [char](1) NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [IX_U_Location_Tiploc] UNIQUE NONCLUSTERED 
(
	[Tiploc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LocationArea]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationArea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LocationArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[MinLatitude] [decimal](9, 6) NOT NULL,
	[MaxLatitude] [decimal](9, 6) NOT NULL,
	[MinLongitude] [decimal](9, 6) NOT NULL,
	[MaxLongitude] [decimal](9, 6) NOT NULL,
	[Priority] [tinyint] NOT NULL,
	[Type] [char](1) NOT NULL,
 CONSTRAINT [PK_LocationArea] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[LocationSection]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationSection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LocationSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartLocationID] [int] NOT NULL,
	[EndLocationID] [int] NOT NULL,
	[Distance] [decimal](9, 4) NULL,
 CONSTRAINT [PK_LocationSection] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RecoveryAuditItem]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RecoveryAuditItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Context_ID] [bigint] NULL,
	[Fault_ID] [int] NOT NULL,
	[Preceding_ID] [int] NULL,
	[AuditItemType_ID] [int] NOT NULL,
	[Notes] [varchar](200) NULL,
 CONSTRAINT [PK_RecoveryAuditItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[RecoveryAuditItemType]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItemType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RecoveryAuditItemType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Question] [bit] NOT NULL,
	[ItemText] [varchar](100) NULL,
	[Answer] [varchar](100) NULL,
	[UserAction] [bit] NOT NULL,
	[Reversion] [bit] NOT NULL,
	[Execution] [bit] NOT NULL,
	[Outcome] [bit] NOT NULL,
	[NextStep] [bit] NOT NULL,
 CONSTRAINT [PK_RecoveryAuditItemType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ReportAdhesion]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportAdhesion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ReportAdhesion](
	[ID] [bigint] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Latitude] [decimal](9, 6) NOT NULL,
	[Longitude] [decimal](9, 6) NOT NULL,
	[GCourse] [decimal](9, 3) NULL,
	[EventType] [tinyint] NOT NULL,
	[Value] [decimal](9, 4) NOT NULL,
	[Duration] [int] NULL,
	[EventEndTimestamp] [datetime2](3) NULL,
 CONSTRAINT [PK_CL_ReportAdhesion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RuleEngineCurrentId]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RuleEngineCurrentId]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RuleEngineCurrentId](
	[ConfigurationName] [varchar](100) NOT NULL,
	[Position] [int] NOT NULL,
	[Value] [bigint] NULL,
 CONSTRAINT [pk_RuleEngineCurrentId] PRIMARY KEY CLUSTERED 
(
	[ConfigurationName] ASC,
	[Position] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ScheduledAnalysis]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysis]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScheduledAnalysis](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ObjectID] [int] NOT NULL,
	[TimestampStart] [datetime] NOT NULL,
	[TimestampEnd] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[TimestampScheduled] [datetime] NULL,
	[TimestampAnalysisStarted] [datetime] NULL,
	[TimestampAnalysisCompleted] [datetime] NULL,
 CONSTRAINT [PK_ScheduledAnalysis] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ScheduledAnalysisRule]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysisRule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScheduledAnalysisRule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledAnalysisID] [int] NOT NULL,
	[RulePath] [varchar](1000) NOT NULL,
 CONSTRAINT [PK_ScheduledAnalysisRule] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ScheduledAnalysisStatus]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysisStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScheduledAnalysisStatus](
	[ID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ScheduledAnalysisStatus] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[SchemaChangeLog]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SchemaChangeLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SchemaChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScriptType] [varchar](50) NOT NULL,
	[ScriptNumber] [int] NOT NULL,
	[ScriptName] [varchar](50) NOT NULL,
	[DateApplied] [smalldatetime] NOT NULL CONSTRAINT [DF_SchemaChangeLog_DateApplied]  DEFAULT (getdate()),
	[ApplicationVersion] [nvarchar](20) NULL,
 CONSTRAINT [PK_SchemaChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Section]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Section]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Section](
	[JourneyID] [int] NOT NULL,
	[SegmentID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[StartLocationID] [int] NOT NULL,
	[EndLocationID] [int] NOT NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[RecoveryTime] [int] NOT NULL,
	[IdealRunID] [int] NULL,
	[IdealRunTime] [int] NULL,
	[Distance] [decimal](9, 4) NULL,
 CONSTRAINT [PK_Section] PRIMARY KEY NONCLUSTERED 
(
	[JourneyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Section_TMP]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Section_TMP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Section_TMP](
	[JourneyID] [varchar](50) NULL,
	[SegmentID] [varchar](50) NULL,
	[ID] [varchar](50) NULL,
	[StartLocationID] [varchar](50) NULL,
	[EndLocationID] [varchar](50) NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
	[RecoveryTime] [varchar](50) NULL,
	[IdealRunID] [varchar](50) NULL,
	[IdealRunTime] [varchar](50) NULL,
	[Distance] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[SectionPoint]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectionPoint]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SectionPoint](
	[JourneyID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[LocationSuffix] [tinyint] NOT NULL,
	[ScheduledArrival] [time](0) NULL,
	[ScheduledDeparture] [time](0) NULL,
	[ScheduledPass] [time](0) NULL,
	[PublicArrival] [time](0) NULL,
	[PublicDeparture] [time](0) NULL,
	[RecoveryTime] [int] NOT NULL,
	[SectionPointType] [char](1) NOT NULL,
 CONSTRAINT [PK_SectionPoint] PRIMARY KEY NONCLUSTERED 
(
	[JourneyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[SectionPoint_TMP]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectionPoint_TMP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SectionPoint_TMP](
	[JourneyID] [varchar](50) NULL,
	[ID] [varchar](50) NULL,
	[LocationID] [varchar](50) NULL,
	[LocationSuffix] [varchar](50) NULL,
	[ScheduledArrival] [varchar](50) NULL,
	[ScheduledDeparture] [varchar](50) NULL,
	[ScheduledPass] [varchar](50) NULL,
	[PublicArrival] [varchar](50) NULL,
	[PublicDeparture] [varchar](50) NULL,
	[RecoveryTime] [varchar](50) NULL,
	[SectionPointType] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Segment]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Segment](
	[JourneyID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[StartLocationID] [int] NOT NULL,
	[EndLocationID] [int] NOT NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[RecoveryTime] [int] NOT NULL,
	[IdealRunTime] [int] NULL,
 CONSTRAINT [PK_Segment] PRIMARY KEY NONCLUSTERED 
(
	[JourneyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Segment_TMP]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segment_TMP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Segment_TMP](
	[JourneyID] [varchar](50) NULL,
	[ID] [varchar](50) NULL,
	[StartLocationID] [varchar](50) NULL,
	[EndLocationID] [varchar](50) NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
	[RecoveryTime] [varchar](50) NULL,
	[IdealRunTime] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Tally]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tally]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tally](
	[N] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Tally] PRIMARY KEY CLUSTERED 
(
	[N] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TmsFault]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TmsFault]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TmsFault](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FaultID] [int] NULL,
	[VehicleNumber] [varchar](10) NOT NULL,
	[FaultDateTime] [datetime2](3) NULL,
	[CarNo] [int] NULL,
	[FaultNo] [int] NOT NULL,
	[TsnCplId] [smallint] NULL,
	[CapId] [smallint] NULL,
	[McsId] [smallint] NULL,
	[DirectionId] [smallint] NULL,
	[VoltageId] [smallint] NULL,
	[SpeedSetId] [smallint] NULL,
	[BrakeContId] [smallint] NULL,
	[EmerBrakeId] [smallint] NULL,
	[StabledId] [smallint] NULL,
	[MlpId] [smallint] NULL,
	[BrakePrId] [smallint] NULL,
	[BatteryId] [smallint] NULL,
	[LoadshedId] [smallint] NULL,
	[TracShoreSupId] [smallint] NULL,
	[AuxShoreSupId] [smallint] NULL,
	[CarBmCoupledId] [smallint] NULL,
	[CarBjCoupledId] [smallint] NULL,
	[Tmcc1Id] [smallint] NULL,
	[Speed] [smallint] NULL,
	[RecordInsert] [datetime2](3) NOT NULL,
 CONSTRAINT [PK_CL_TmsFault] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[TmsFaultLookup]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TmsFaultLookup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TmsFaultLookup](
	[FaultNo] [int] NOT NULL,
	[Description] [varchar](300) NOT NULL,
	[FixingInstr] [varchar](300) NOT NULL,
	[SwRef] [varchar](50) NOT NULL,
	[TmsCategoryID] [int] NOT NULL,
	[TmsFunctionID] [int] NOT NULL,
 CONSTRAINT [PK_CL_TmsFaultLookup] PRIMARY KEY CLUSTERED 
(
	[FaultNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[TmsLookup]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TmsLookup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TmsLookup](
	[Type] [varchar](30) NOT NULL,
	[ValueID] [smallint] NOT NULL,
	[Value] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CL_TmsLookup] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[ValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[TrainPassage]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainPassage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JourneyID] [int] NOT NULL,
	[Headcode] [varchar](50) NOT NULL,
	[Setcode] [varchar](50) NULL,
	[StartDatetime] [datetime] NOT NULL,
	[EndDatetime] [datetime] NOT NULL,
	[AnalyseDatetime] [datetime] NOT NULL,
	[EndAVehicleID] [int] NULL,
	[EndBVehicleID] [int] NULL,
	[Loading] [tinyint] NULL,
	[IsIncludedInAnalysis] [bit] NOT NULL CONSTRAINT [DF_TrainPassage_IsIncludedInAnalysis]  DEFAULT ((1)),
 CONSTRAINT [PK_TrainPassage] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[TrainPassageSection]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainPassageSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainPassageID] [int] NULL,
	[JourneyID] [int] NOT NULL,
	[SegmentID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
	[IdealRunID] [int] NULL,
	[ActualStartDatetime] [datetime] NULL,
	[ActualEndDatetime] [datetime] NULL,
	[LeadingVehicleID] [int] NULL,
	[DwellTime] [int] NULL,
 CONSTRAINT [PK_TrainPassageSection] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TrainPassageSectionDelayReason]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionDelayReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainPassageSectionDelayReason](
	[TrainPassageSectionID] [int] NOT NULL,
	[DelayReasonID] [int] NOT NULL,
	[DelayDuration] [int] NOT NULL,
 CONSTRAINT [PK_TrainPassageSectionDelayReason] PRIMARY KEY NONCLUSTERED 
(
	[TrainPassageSectionID] ASC,
	[DelayReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TrainPassageSectionPoint]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainPassageSectionPoint](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainPassageID] [int] NOT NULL,
	[JourneyID] [int] NOT NULL,
	[SectionPointID] [int] NOT NULL,
	[ActualDepartureDatetime] [datetime] NULL,
	[ActualArrivalDatetime] [datetime] NULL,
 CONSTRAINT [PK_TrainPassageSectionPoint] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TrainPassageSegment]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainPassageSegment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainPassageID] [int] NULL,
	[JourneyID] [int] NOT NULL,
	[SegmentID] [int] NOT NULL,
	[ActualDatetimeStart] [datetime] NOT NULL,
	[ActualDatetimeEnd] [datetime] NOT NULL,
	[DwellTime] [int] NOT NULL,
	[LeadingVehicleID] [int] NOT NULL,
	[AverageSpeed] [decimal](9, 4) NULL,
	[MotDmdTime] [decimal](9, 4) NULL,
	[EnergyRealMove] [decimal](18, 4) NULL,
	[EnergyRealStop] [decimal](18, 4) NULL,
	[EnergyReactiveMove] [decimal](18, 4) NULL,
	[EnergyReactiveStop] [decimal](18, 4) NULL,
 CONSTRAINT [PK_TrainPassageSegment] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Unit]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Unit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Unit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnitNumber] [varchar](16) NOT NULL,
	[UnitType] [varchar](16) NOT NULL,
	[IsValid] [bit] NULL CONSTRAINT [DF_Unit_IsValid]  DEFAULT ((1)),
	[FleetID] [smallint] NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_Unit_UnitNumber] UNIQUE NONCLUSTERED 
(
	[UnitNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Vehicle](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleNumber] [varchar](16) NOT NULL,
	[Type] [varchar](50) NULL,
	[VehicleOrder] [tinyint] NULL,
	[CabEnd] [char](1) NULL,
	[UnitID] [int] NULL,
	[Comments] [varchar](50) NULL,
	[ConfigDate] [datetime] NOT NULL,
	[LastUpdate] [datetime] NOT NULL CONSTRAINT [DF_Vehicle_LastUpdate]  DEFAULT (sysdatetime()),
	[IsValid] [bit] NULL CONSTRAINT [DF_Vehicle_IsValid]  DEFAULT ((1)),
	[HardwareTypeID] [smallint] NULL,
	[OtmrSn] [varchar](50) NULL,
	[NoOpenRestriction] [int] NULL,
	[NoP1WorkOrders] [int] NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY],
 CONSTRAINT [IX_U_Vehicle_VehicleNumber] UNIQUE NONCLUSTERED 
(
	[VehicleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[VehicleDownload]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleDownload]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleDownload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[DateTimeRequested] [datetime2](3) NOT NULL,
	[DateTimeCompleted] [datetime2](3) NULL,
	[IsManual] [bit] NOT NULL,
	[FileType] [tinyint] NOT NULL,
	[Filename] [varchar](100) NULL,
 CONSTRAINT [PK_VehicleDownload] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[VehicleDownloadProgress]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleDownloadProgress]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleDownloadProgress](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[VehicleDownloadID] [int] NULL,
	[VehicleEventID] [bigint] NULL,
	[RecordInsert] [datetime2](3) NOT NULL CONSTRAINT [DF_VehicleDownloadProgress_RecordInsert]  DEFAULT (sysdatetime()),
	[EventCategory] [smallint] NULL,
	[EventCode] [smallint] NULL,
	[IsProcessed] [bit] NULL CONSTRAINT [DF_VehicleDownloadProgress_IsProcessed]  DEFAULT ((0)),
	[StatusUpdated] [bit] NULL CONSTRAINT [DF_VehicleDownloadProgress_StatusUpdated]  DEFAULT ((0)),
	[StatusUpdatedDatetime] [datetime2](3) NULL,
 CONSTRAINT [PK_CL_VehicleDownloadProgress] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ncu](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[NcuModelID] [smallint] NOT NULL,
	[NcuTypeID] [smallint] NOT NULL,
	[SerialNo] [char](10) NOT NULL,
	[Comment] [varchar](1000) NULL,
 CONSTRAINT [PK_Ncu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NcuModel](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Model] [varchar](20) NULL,
 CONSTRAINT [PK_NcuModel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NcuType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_NcuConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NcuTypeChannelConfiguration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Slot] [tinyint] NOT NULL,
	[NcuTypeID] [smallint] NOT NULL,
	[InputNo] [smallint] NOT NULL,
	[ChannelID] [int] NOT NULL,
	[ScalingGain] [decimal](18, 6) NULL,
	[ScalingOffset] [decimal](18, 6) NULL,
	[ScalingInversion] [bit] NULL,
 CONSTRAINT [PK_NcuChannelConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NcuTypeModuleConfiguration](
	[NcuTypeID] [smallint] NOT NULL,
	[Slot] [tinyint] NOT NULL,
	[ModuleConfigurationID] [smallint] NOT NULL,
 CONSTRAINT [PK_NcuTypeModuleConfiguration] PRIMARY KEY CLUSTERED 
(
	[Slot] ASC,
	[NcuTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Module]    Script Date: 17/08/2016 10:10:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Module](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[ModuleConfiguration]    Script Date: 17/08/2016 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModuleConfiguration](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[ModuleID] [smallint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[PinSetup] [varchar](100) NULL,
 CONSTRAINT [PK_ModuleConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[ModuleConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Module_ModuleConfiguration] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[Module] ([ID])
GO
ALTER TABLE [dbo].[ModuleConfiguration] CHECK CONSTRAINT [FK_Module_ModuleConfiguration]
GO

/****** Object:  Table [dbo].[VehicleEvent]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleEvent](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UpdateRecord] [bit] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[RecordInsert] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[EventCategory] [smallint] NOT NULL,
	[EventCode] [smallint] NOT NULL,
	[EventDescription] [varchar](100) NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
 CONSTRAINT [PK_VehicleEvent] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_CL_ChannelValue_TimeStamp_UnitID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValue]') AND name = N'IX_CL_ChannelValue_TimeStamp_UnitID')
CREATE CLUSTERED INDEX [IX_CL_ChannelValue_TimeStamp_UnitID] ON [dbo].[ChannelValue]
(
	[TimeStamp] ASC,
	[UnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_CL_LoadCifFileData_FileID_FileLineNumber]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LoadCifFileData]') AND name = N'IX_CL_LoadCifFileData_FileID_FileLineNumber')
CREATE CLUSTERED INDEX [IX_CL_LoadCifFileData_FileID_FileLineNumber] ON [dbo].[LoadCifFileData]
(
	[FileID] ASC,
	[FileLineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ChannelValue_UnitID_TimeStamp]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValue]') AND name = N'IX_ChannelValue_UnitID_TimeStamp')
CREATE NONCLUSTERED INDEX [IX_ChannelValue_UnitID_TimeStamp] ON [dbo].[ChannelValue]
(
	[UnitID] ASC,
	[TimeStamp] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_Fault_CreateTime_FaultCode_FaultVehicleID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fault]') AND name = N'IX_Fault_CreateTime_FaultCode_FaultVehicleID')
CREATE NONCLUSTERED INDEX [IX_Fault_CreateTime_FaultCode_FaultVehicleID] ON [dbo].[Fault]
(
	[CreateTime] ASC,
	[FaultMetaID] ASC,
	[FaultUnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO
/****** Object:  Index [IX_Fault_FaultVehicleID_FaultMetaID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fault]') AND name = N'IX_Fault_FaultVehicleID_FaultMetaID')
CREATE NONCLUSTERED INDEX [IX_Fault_FaultVehicleID_FaultMetaID] ON [dbo].[Fault]
(
	[FaultUnitID] ASC,
	[FaultMetaID] ASC
)
WHERE ([IsCurrent]=(1))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO
/****** Object:  Index [IX_U_FaultType_Priority]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FaultType]') AND name = N'IX_U_FaultType_Priority')
CREATE UNIQUE NONCLUSTERED INDEX [IX_U_FaultType_Priority] ON [dbo].[FaultType]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_FleetStatus_ValidFrom_ValidTo_UnitID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FleetStatus]') AND name = N'IX_FleetStatus_ValidFrom_ValidTo_UnitID')
CREATE NONCLUSTERED INDEX [IX_FleetStatus_ValidFrom_ValidTo_UnitID] ON [dbo].[FleetStatus]
(
	[ValidFrom] ASC,
	[ValidTo] ASC,
	[UnitID] ASC
)
INCLUDE ( 	[Headcode],
	[LocationIDHeadcodeStart]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_GeniusInterfaceData]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceData]') AND name = N'IX_GeniusInterfaceData')
CREATE NONCLUSTERED INDEX [IX_GeniusInterfaceData] ON [dbo].[GeniusInterfaceData]
(
	[GeniusInterfaceFileId] ASC,
	[HeadCode] ASC,
	[TrainStartLocation] ASC,
	[TrainStartTime] ASC,
	[LegEndTime] DESC
)
INCLUDE ( 	[LegEndLocation]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GeniusInterfaceData_FileID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceData]') AND name = N'IX_GeniusInterfaceData_FileID')
CREATE NONCLUSTERED INDEX [IX_GeniusInterfaceData_FileID] ON [dbo].[GeniusInterfaceData]
(
	[GeniusInterfaceFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Journey_StpType_Headcode_ValidTo_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Journey]') AND name = N'IX_Journey_StpType_Headcode_ValidTo_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_Journey_StpType_Headcode_ValidTo_INCLUDE] ON [dbo].[Journey]
(
	[StpType] ASC,
	[Headcode] ASC,
	[ValidTo] ASC
)
INCLUDE ( 	[ID],
	[StartTime],
	[EndTime],
	[ValidFrom],
	[HeadcodeDesc]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_JourneyDate_RunActualRunAnalysis_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[JourneyDate]') AND name = N'IX_JourneyDate_RunActualRunAnalysis_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_JourneyDate_RunActualRunAnalysis_INCLUDE] ON [dbo].[JourneyDate]
(
	[RunActualRunAnalysis] ASC
)
INCLUDE ( 	[PermanentJourneyID],
	[DateValue],
	[ActualJourneyID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Location_TEST]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'IX_Location_TEST')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Location_TEST] ON [dbo].[Location]
(
	[ID] ASC
)
INCLUDE ( 	[LocationCode],
	[LocationName],
	[Lat],
	[Lng]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SectionPoint_JourneyID_SectionPointType_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SectionPoint]') AND name = N'IX_SectionPoint_JourneyID_SectionPointType_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_SectionPoint_JourneyID_SectionPointType_INCLUDE] ON [dbo].[SectionPoint]
(
	[JourneyID] ASC,
	[SectionPointType] ASC
)
INCLUDE ( 	[LocationID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SectionPoint_SectionPointType_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SectionPoint]') AND name = N'IX_SectionPoint_SectionPointType_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_SectionPoint_SectionPointType_INCLUDE] ON [dbo].[SectionPoint]
(
	[SectionPointType] ASC
)
INCLUDE ( 	[JourneyID],
	[LocationID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TrainPassageSectionPoint_TrainPassageID_SectionPointID_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]') AND name = N'IX_TrainPassageSectionPoint_TrainPassageID_SectionPointID_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_TrainPassageSectionPoint_TrainPassageID_SectionPointID_INCLUDE] ON [dbo].[TrainPassageSectionPoint]
(
	[TrainPassageID] ASC,
	[SectionPointID] ASC
)
INCLUDE ( 	[ActualDepartureDatetime],
	[ActualArrivalDatetime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TrainPassageSegment_JourneyID_SegmentID_ActualDatetimeStart_INCLUDE]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]') AND name = N'IX_TrainPassageSegment_JourneyID_SegmentID_ActualDatetimeStart_INCLUDE')
CREATE NONCLUSTERED INDEX [IX_TrainPassageSegment_JourneyID_SegmentID_ActualDatetimeStart_INCLUDE] ON [dbo].[TrainPassageSegment]
(
	[JourneyID] ASC,
	[SegmentID] ASC,
	[ActualDatetimeStart] ASC
)
INCLUDE ( 	[TrainPassageID],
	[ActualDatetimeEnd]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VehicleDownloadProgress_ID_FILTERED]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleDownloadProgress]') AND name = N'IX_VehicleDownloadProgress_ID_FILTERED')
CREATE NONCLUSTERED INDEX [IX_VehicleDownloadProgress_ID_FILTERED] ON [dbo].[VehicleDownloadProgress]
(
	[ID] ASC
)
WHERE ([IsProcessed]=(0))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CL_VehicleEvent_TimeStamp_VehicleID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleEvent]') AND name = N'IX_CL_VehicleEvent_TimeStamp_VehicleID')
CREATE NONCLUSTERED INDEX [IX_CL_VehicleEvent_TimeStamp_VehicleID] ON [dbo].[VehicleEvent]
(
	[TimeStamp] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_VehicleEvent_TimeStamp_EventCategory_VehicleID]    Script Date: 14/07/2016 15:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleEvent]') AND name = N'IX_VehicleEvent_TimeStamp_EventCategory_VehicleID')
CREATE NONCLUSTERED INDEX [IX_VehicleEvent_TimeStamp_EventCategory_VehicleID] ON [dbo].[VehicleEvent]
(
	[TimeStamp] ASC,
	[EventCategory] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_FileType_HeaderRowsToSkip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FileType] ADD  CONSTRAINT [DF_FileType_HeaderRowsToSkip]  DEFAULT ((0)) FOR [HeaderRowsToSkip]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_FleetStatusHistory_UnitOrientation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FleetStatusHistory] ADD  CONSTRAINT [DF_FleetStatusHistory_UnitOrientation]  DEFAULT ((1)) FOR [UnitOrientation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_InterfaceFile_ImportTimestamp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[InterfaceFile] ADD  CONSTRAINT [DF_InterfaceFile_ImportTimestamp]  DEFAULT (sysdatetime()) FOR [ImportTimestamp]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_InterfaceFile_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[InterfaceFile] ADD  CONSTRAINT [DF_InterfaceFile_Status]  DEFAULT ('Inserting') FOR [Status]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TmsFault_RecordInsert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TmsFault] ADD  CONSTRAINT [DF_TmsFault_RecordInsert]  DEFAULT (sysdatetime()) FOR [RecordInsert]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_VehicleEvent_RecordInsert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[VehicleEvent] ADD  CONSTRAINT [DF_VehicleEvent_RecordInsert]  DEFAULT (getdate()) FOR [RecordInsert]
END

GO
ALTER TABLE [dbo].[Ncu]  WITH CHECK ADD  CONSTRAINT [FK_NcuModel_Ncu] FOREIGN KEY([NcuModelID])
REFERENCES [dbo].[NcuModel] ([ID])
GO
ALTER TABLE [dbo].[Ncu] CHECK CONSTRAINT [FK_NcuModel_Ncu]
GO
ALTER TABLE [dbo].[Ncu]  WITH CHECK ADD  CONSTRAINT [FK_NcuType_Ncu] FOREIGN KEY([NcuTypeID])
REFERENCES [dbo].[NcuType] ([ID])
GO
ALTER TABLE [dbo].[Ncu] CHECK CONSTRAINT [FK_NcuType_Ncu]
GO
ALTER TABLE [dbo].[NcuTypeChannelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Channel_NcuTypeChannelConfiguration] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO
ALTER TABLE [dbo].[NcuTypeChannelConfiguration] CHECK CONSTRAINT [FK_Channel_NcuTypeChannelConfiguration]
GO
ALTER TABLE [dbo].[NcuTypeChannelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_NcuTypeModuleConfiguration_NcuTypeChannelConfiguration] FOREIGN KEY([Slot], [NcuTypeID])
REFERENCES [dbo].[NcuTypeModuleConfiguration] ([Slot], [NcuTypeID])
GO
ALTER TABLE [dbo].[NcuTypeChannelConfiguration] CHECK CONSTRAINT [FK_NcuTypeModuleConfiguration_NcuTypeChannelConfiguration]
GO
ALTER TABLE [dbo].[NcuTypeModuleConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_ModuleConfiguration_NcuTypeModuleConfiguration] FOREIGN KEY([ModuleConfigurationID])
REFERENCES [dbo].[ModuleConfiguration] ([ID])
GO
ALTER TABLE [dbo].[NcuTypeModuleConfiguration] CHECK CONSTRAINT [FK_ModuleConfiguration_NcuTypeModuleConfiguration]
GO
ALTER TABLE [dbo].[NcuTypeModuleConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_NcuType_NcuTypeModuleConfiguration] FOREIGN KEY([NcuTypeID])
REFERENCES [dbo].[NcuType] ([ID])
GO
ALTER TABLE [dbo].[NcuTypeModuleConfiguration] CHECK CONSTRAINT [FK_NcuType_NcuTypeModuleConfiguration]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH NOCHECK ADD  CONSTRAINT [FK_Channel_ChannelGroup] FOREIGN KEY([ChannelGroupID])
REFERENCES [dbo].[ChannelGroup] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_Channel_ChannelGroup]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_TypeID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH NOCHECK ADD  CONSTRAINT [FK_Channel_TypeID] FOREIGN KEY([TypeID])
REFERENCES [dbo].[ChannelType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_TypeID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_Channel_TypeID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH NOCHECK ADD  CONSTRAINT [FK_Channel_Vehicle] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_Channel_Vehicle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelGroup_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH CHECK ADD  CONSTRAINT [FK_ChannelGroup_Channel] FOREIGN KEY([ChannelGroupID])
REFERENCES [dbo].[ChannelGroup] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelGroup_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_ChannelGroup_Channel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelType_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH CHECK ADD  CONSTRAINT [FK_ChannelType_Channel] FOREIGN KEY([TypeID])
REFERENCES [dbo].[ChannelType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelType_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_ChannelType_Channel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_Channel] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_Channel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Channel]'))
ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_Vehicle_Channel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelMmiDisplay]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelMmiDisplay]'))
ALTER TABLE [dbo].[ChannelMmiDisplay]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChannelMmiDisplay] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelMmiDisplay]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelMmiDisplay]'))
ALTER TABLE [dbo].[ChannelMmiDisplay] CHECK CONSTRAINT [FK_Channel_ChannelMmiDisplay]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRule]'))
ALTER TABLE [dbo].[ChannelRule]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChannelRule] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRule]'))
ALTER TABLE [dbo].[ChannelRule] CHECK CONSTRAINT [FK_Channel_ChannelRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelStatus_ChannelRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRule]'))
ALTER TABLE [dbo].[ChannelRule]  WITH CHECK ADD  CONSTRAINT [FK_ChannelStatus_ChannelRule] FOREIGN KEY([ChannelStatusID])
REFERENCES [dbo].[ChannelStatus] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelStatus_ChannelRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRule]'))
ALTER TABLE [dbo].[ChannelRule] CHECK CONSTRAINT [FK_ChannelStatus_ChannelRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelRuleValidation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRuleValidation]'))
ALTER TABLE [dbo].[ChannelRuleValidation]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChannelRuleValidation] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelRuleValidation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRuleValidation]'))
ALTER TABLE [dbo].[ChannelRuleValidation] CHECK CONSTRAINT [FK_Channel_ChannelRuleValidation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelRule_ChannelRuleValidation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRuleValidation]'))
ALTER TABLE [dbo].[ChannelRuleValidation]  WITH CHECK ADD  CONSTRAINT [FK_ChannelRule_ChannelRuleValidation] FOREIGN KEY([ChannelRuleID])
REFERENCES [dbo].[ChannelRule] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelRule_ChannelRuleValidation]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelRuleValidation]'))
ALTER TABLE [dbo].[ChannelRuleValidation] CHECK CONSTRAINT [FK_ChannelRule_ChannelRuleValidation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelValue_Unit]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValue]'))
ALTER TABLE [dbo].[ChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_ChannelValue_Unit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Unit] ([ID])
GO
ALTER TABLE [dbo].[ChannelValue] CHECK CONSTRAINT [FK_ChannelValue_Unit]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_ChannelValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValue]'))
ALTER TABLE [dbo].[ChannelValue] CHECK CONSTRAINT [FK_Vehicle_ChannelValue]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelValueLookup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValueLookup]'))
ALTER TABLE [dbo].[ChannelValueLookup]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChannelValueLookup] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChannelValueLookup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValueLookup]'))
ALTER TABLE [dbo].[ChannelValueLookup] CHECK CONSTRAINT [FK_Channel_ChannelValueLookup]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChartChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChartChannel]'))
ALTER TABLE [dbo].[ChartChannel]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChartChannel] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_ChartChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChartChannel]'))
ALTER TABLE [dbo].[ChartChannel] CHECK CONSTRAINT [FK_Channel_ChartChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Chart_ChartChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChartChannel]'))
ALTER TABLE [dbo].[ChartChannel]  WITH CHECK ADD  CONSTRAINT [FK_Chart_ChartChannel] FOREIGN KEY([ChartID])
REFERENCES [dbo].[Chart] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Chart_ChartChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChartChannel]'))
ALTER TABLE [dbo].[ChartChannel] CHECK CONSTRAINT [FK_Chart_ChartChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultMeta_Fault]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fault]'))
ALTER TABLE [dbo].[Fault]  WITH CHECK ADD  CONSTRAINT [FK_FaultMeta_Fault] FOREIGN KEY([FaultMetaID])
REFERENCES [dbo].[FaultMeta] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultMeta_Fault]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fault]'))
ALTER TABLE [dbo].[Fault] CHECK CONSTRAINT [FK_FaultMeta_Fault]
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[Fault]  WITH CHECK ADD  CONSTRAINT [FK_Unit_Fault] FOREIGN KEY([FaultUnitID])
REFERENCES [dbo].[Unit] ([ID])
GO
ALTER TABLE [dbo].[Fault] CHECK CONSTRAINT [FK_Unit_Fault]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultChannelValue_Fault]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultChannelValue]'))
ALTER TABLE [dbo].[FaultChannelValue]  WITH NOCHECK ADD  CONSTRAINT [FK_FaultChannelValue_Fault] FOREIGN KEY([FaultID])
REFERENCES [dbo].[Fault] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultChannelValue_Fault]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultChannelValue]'))
ALTER TABLE [dbo].[FaultChannelValue] CHECK CONSTRAINT [FK_FaultChannelValue_Fault]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_FaultComment]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultComment]'))
ALTER TABLE [dbo].[FaultComment]  WITH CHECK ADD  CONSTRAINT [FK_Fault_FaultComment] FOREIGN KEY([FaultID])
REFERENCES [dbo].[Fault] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_FaultComment]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultComment]'))
ALTER TABLE [dbo].[FaultComment] CHECK CONSTRAINT [FK_Fault_FaultComment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultMeta_FaultCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultMeta]'))
ALTER TABLE [dbo].[FaultMeta]  WITH CHECK ADD  CONSTRAINT [FK_FaultMeta_FaultCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[FaultCategory] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultMeta_FaultCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultMeta]'))
ALTER TABLE [dbo].[FaultMeta] CHECK CONSTRAINT [FK_FaultMeta_FaultCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultType_FaultMeta]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultMeta]'))
ALTER TABLE [dbo].[FaultMeta]  WITH CHECK ADD  CONSTRAINT [FK_FaultType_FaultMeta] FOREIGN KEY([FaultTypeID])
REFERENCES [dbo].[FaultType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultType_FaultMeta]') AND parent_object_id = OBJECT_ID(N'[dbo].[FaultMeta]'))
ALTER TABLE [dbo].[FaultMeta] CHECK CONSTRAINT [FK_FaultType_FaultMeta]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_FileFieldDefinition]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileFieldDefinition]'))
ALTER TABLE [dbo].[FileFieldDefinition]  WITH CHECK ADD  CONSTRAINT [FK_Channel_FileFieldDefinition] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_FileFieldDefinition]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileFieldDefinition]'))
ALTER TABLE [dbo].[FileFieldDefinition] CHECK CONSTRAINT [FK_Channel_FileFieldDefinition]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileType_FileFieldDefinition]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileFieldDefinition]'))
ALTER TABLE [dbo].[FileFieldDefinition]  WITH CHECK ADD  CONSTRAINT [FK_FileType_FileFieldDefinition] FOREIGN KEY([FileTypeID])
REFERENCES [dbo].[FileType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileType_FileFieldDefinition]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileFieldDefinition]'))
ALTER TABLE [dbo].[FileFieldDefinition] CHECK CONSTRAINT [FK_FileType_FileFieldDefinition]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_FleetChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetChannel]'))
ALTER TABLE [dbo].[FleetChannel]  WITH CHECK ADD  CONSTRAINT [FK_Channel_FleetChannel] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Channel_FleetChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetChannel]'))
ALTER TABLE [dbo].[FleetChannel] CHECK CONSTRAINT [FK_Channel_FleetChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fleet_FleetChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetChannel]'))
ALTER TABLE [dbo].[FleetChannel]  WITH CHECK ADD  CONSTRAINT [FK_Fleet_FleetChannel] FOREIGN KEY([FleetID])
REFERENCES [dbo].[Fleet] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fleet_FleetChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetChannel]'))
ALTER TABLE [dbo].[FleetChannel] CHECK CONSTRAINT [FK_Fleet_FleetChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FleetFormation_FleetStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatus]'))
ALTER TABLE [dbo].[FleetStatus]  WITH CHECK ADD  CONSTRAINT [FK_FleetFormation_FleetStatus] FOREIGN KEY([FleetFormationID])
REFERENCES [dbo].[FleetFormation] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FleetFormation_FleetStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatus]'))
ALTER TABLE [dbo].[FleetStatus] CHECK CONSTRAINT [FK_FleetFormation_FleetStatus]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FleetFormation_FleetStatusHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusHistory]'))
ALTER TABLE [dbo].[FleetStatusHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_FleetFormation_FleetStatusHistory] FOREIGN KEY([FleetFormationID])
REFERENCES [dbo].[FleetFormation] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FleetFormation_FleetStatusHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusHistory]'))
ALTER TABLE [dbo].[FleetStatusHistory] CHECK CONSTRAINT [FK_FleetFormation_FleetStatusHistory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_End_Location_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging]  WITH NOCHECK ADD  CONSTRAINT [FK_End_Location_FleetStatusStaging] FOREIGN KEY([EndLocationId])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_End_Location_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging] CHECK CONSTRAINT [FK_End_Location_FleetStatusStaging]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_FleetStatusStaging_TrainEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging]  WITH NOCHECK ADD  CONSTRAINT [FK_Location_FleetStatusStaging_TrainEnd] FOREIGN KEY([TrainEndLocationId])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_FleetStatusStaging_TrainEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging] CHECK CONSTRAINT [FK_Location_FleetStatusStaging_TrainEnd]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_FleetStatusStaging_TrainStart]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging]  WITH NOCHECK ADD  CONSTRAINT [FK_Location_FleetStatusStaging_TrainStart] FOREIGN KEY([TrainStartLocationId])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_FleetStatusStaging_TrainStart]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging] CHECK CONSTRAINT [FK_Location_FleetStatusStaging_TrainStart]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Start_Location_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging]  WITH NOCHECK ADD  CONSTRAINT [FK_Start_Location_FleetStatusStaging] FOREIGN KEY([StartLocationId])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Start_Location_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging] CHECK CONSTRAINT [FK_Start_Location_FleetStatusStaging]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Unit_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging]  WITH NOCHECK ADD  CONSTRAINT [FK_Unit_FleetStatusStaging] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Unit_FleetStatusStaging]') AND parent_object_id = OBJECT_ID(N'[dbo].[FleetStatusStaging]'))
ALTER TABLE [dbo].[FleetStatusStaging] CHECK CONSTRAINT [FK_Unit_FleetStatusStaging]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GeniusInterfaceData_GeniusInterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceData]'))
ALTER TABLE [dbo].[GeniusInterfaceData]  WITH NOCHECK ADD  CONSTRAINT [FK_GeniusInterfaceData_GeniusInterfaceFile] FOREIGN KEY([GeniusInterfaceFileId])
REFERENCES [dbo].[GeniusInterfaceFile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GeniusInterfaceData_GeniusInterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusInterfaceData]'))
ALTER TABLE [dbo].[GeniusInterfaceData] CHECK CONSTRAINT [FK_GeniusInterfaceData_GeniusInterfaceFile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GeniusLog_GeniusInterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusLog]'))
ALTER TABLE [dbo].[GeniusLog]  WITH NOCHECK ADD  CONSTRAINT [FK_GeniusLog_GeniusInterfaceFile] FOREIGN KEY([GeniusInterfaceFileId])
REFERENCES [dbo].[GeniusInterfaceFile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GeniusLog_GeniusInterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusLog]'))
ALTER TABLE [dbo].[GeniusLog] CHECK CONSTRAINT [FK_GeniusLog_GeniusInterfaceFile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_IdealRun]') AND parent_object_id = OBJECT_ID(N'[dbo].[IdealRun]'))
ALTER TABLE [dbo].[IdealRun]  WITH CHECK ADD  CONSTRAINT [FK_Section_IdealRun] FOREIGN KEY([JourneyID], [SectionID])
REFERENCES [dbo].[Section] ([JourneyID], [ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_IdealRun]') AND parent_object_id = OBJECT_ID(N'[dbo].[IdealRun]'))
ALTER TABLE [dbo].[IdealRun] CHECK CONSTRAINT [FK_Section_IdealRun]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_IdealRun_IdealRunChannelValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[IdealRunChannelValue]'))
ALTER TABLE [dbo].[IdealRunChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_IdealRun_IdealRunChannelValue] FOREIGN KEY([IdealRunID])
REFERENCES [dbo].[IdealRun] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_IdealRun_IdealRunChannelValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[IdealRunChannelValue]'))
ALTER TABLE [dbo].[IdealRunChannelValue] CHECK CONSTRAINT [FK_IdealRun_IdealRunChannelValue]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileType_InterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFile]'))
ALTER TABLE [dbo].[InterfaceFile]  WITH CHECK ADD  CONSTRAINT [FK_FileType_InterfaceFile] FOREIGN KEY([FileTypeID])
REFERENCES [dbo].[FileType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileType_InterfaceFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFile]'))
ALTER TABLE [dbo].[InterfaceFile] CHECK CONSTRAINT [FK_FileType_InterfaceFile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InterfaceFile_InterfaceFile_ParentFileID]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFile]'))
ALTER TABLE [dbo].[InterfaceFile]  WITH CHECK ADD  CONSTRAINT [FK_InterfaceFile_InterfaceFile_ParentFileID] FOREIGN KEY([ParentFileID])
REFERENCES [dbo].[InterfaceFile] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InterfaceFile_InterfaceFile_ParentFileID]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFile]'))
ALTER TABLE [dbo].[InterfaceFile] CHECK CONSTRAINT [FK_InterfaceFile_InterfaceFile_ParentFileID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InterfaceFile_InterfaceFileData]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFileData]'))
ALTER TABLE [dbo].[InterfaceFileData]  WITH CHECK ADD  CONSTRAINT [FK_InterfaceFile_InterfaceFileData] FOREIGN KEY([InterfaceFileID])
REFERENCES [dbo].[InterfaceFile] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InterfaceFile_InterfaceFileData]') AND parent_object_id = OBJECT_ID(N'[dbo].[InterfaceFileData]'))
ALTER TABLE [dbo].[InterfaceFileData] CHECK CONSTRAINT [FK_InterfaceFile_InterfaceFileData]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Journey_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Journey]'))
ALTER TABLE [dbo].[Journey]  WITH CHECK ADD  CONSTRAINT [FK_Location_Journey_EndLocationID] FOREIGN KEY([EndLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Journey_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Journey]'))
ALTER TABLE [dbo].[Journey] CHECK CONSTRAINT [FK_Location_Journey_EndLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Journey_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Journey]'))
ALTER TABLE [dbo].[Journey]  WITH CHECK ADD  CONSTRAINT [FK_Location_Journey_StartLocationID] FOREIGN KEY([StartLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Journey_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Journey]'))
ALTER TABLE [dbo].[Journey] CHECK CONSTRAINT [FK_Location_Journey_StartLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JourneyDate_Headcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[JourneyDate]'))
ALTER TABLE [dbo].[JourneyDate]  WITH CHECK ADD  CONSTRAINT [FK_JourneyDate_Headcode] FOREIGN KEY([PermanentJourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JourneyDate_Headcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[JourneyDate]'))
ALTER TABLE [dbo].[JourneyDate] CHECK CONSTRAINT [FK_JourneyDate_Headcode]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JourneyDate_Journey]') AND parent_object_id = OBJECT_ID(N'[dbo].[JourneyDate]'))
ALTER TABLE [dbo].[JourneyDate]  WITH CHECK ADD  CONSTRAINT [FK_JourneyDate_Journey] FOREIGN KEY([ActualJourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JourneyDate_Journey]') AND parent_object_id = OBJECT_ID(N'[dbo].[JourneyDate]'))
ALTER TABLE [dbo].[JourneyDate] CHECK CONSTRAINT [FK_JourneyDate_Journey]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoadCifFile_LoadCifFileData]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoadCifFileData]'))
ALTER TABLE [dbo].[LoadCifFileData]  WITH NOCHECK ADD  CONSTRAINT [FK_LoadCifFile_LoadCifFileData] FOREIGN KEY([FileID])
REFERENCES [dbo].[LoadCifFile] ([FileID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoadCifFile_LoadCifFileData]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoadCifFileData]'))
ALTER TABLE [dbo].[LoadCifFileData] CHECK CONSTRAINT [FK_LoadCifFile_LoadCifFileData]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoadGeniusFile_LoadGeniusData]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoadGeniusData]'))
ALTER TABLE [dbo].[LoadGeniusData]  WITH NOCHECK ADD  CONSTRAINT [FK_LoadGeniusFile_LoadGeniusData] FOREIGN KEY([LoadGeniusFileID])
REFERENCES [dbo].[LoadGeniusFile] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoadGeniusFile_LoadGeniusData]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoadGeniusData]'))
ALTER TABLE [dbo].[LoadGeniusData] CHECK CONSTRAINT [FK_LoadGeniusFile_LoadGeniusData]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationArea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationArea]'))
ALTER TABLE [dbo].[LocationArea]  WITH CHECK ADD  CONSTRAINT [FK_Location_LocationArea] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationArea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationArea]'))
ALTER TABLE [dbo].[LocationArea] CHECK CONSTRAINT [FK_Location_LocationArea]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationSection_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationSection]'))
ALTER TABLE [dbo].[LocationSection]  WITH CHECK ADD  CONSTRAINT [FK_Location_LocationSection_EndLocationID] FOREIGN KEY([EndLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationSection_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationSection]'))
ALTER TABLE [dbo].[LocationSection] CHECK CONSTRAINT [FK_Location_LocationSection_EndLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationSection_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationSection]'))
ALTER TABLE [dbo].[LocationSection]  WITH CHECK ADD  CONSTRAINT [FK_Location_LocationSection_StartLocationID] FOREIGN KEY([StartLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_LocationSection_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[LocationSection]'))
ALTER TABLE [dbo].[LocationSection] CHECK CONSTRAINT [FK_Location_LocationSection_StartLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_RecoveryAuditItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItem]'))
ALTER TABLE [dbo].[RecoveryAuditItem]  WITH CHECK ADD  CONSTRAINT [FK_Fault_RecoveryAuditItem] FOREIGN KEY([Fault_ID])
REFERENCES [dbo].[Fault] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_RecoveryAuditItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItem]'))
ALTER TABLE [dbo].[RecoveryAuditItem] CHECK CONSTRAINT [FK_Fault_RecoveryAuditItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RecoveryAuditItemType_RecoveryAuditItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItem]'))
ALTER TABLE [dbo].[RecoveryAuditItem]  WITH CHECK ADD  CONSTRAINT [FK_RecoveryAuditItemType_RecoveryAuditItem] FOREIGN KEY([AuditItemType_ID])
REFERENCES [dbo].[RecoveryAuditItemType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RecoveryAuditItemType_RecoveryAuditItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[RecoveryAuditItem]'))
ALTER TABLE [dbo].[RecoveryAuditItem] CHECK CONSTRAINT [FK_RecoveryAuditItemType_RecoveryAuditItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ScheduledAnalysisStatus_ScheduledAnalysis]') AND parent_object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysis]'))
ALTER TABLE [dbo].[ScheduledAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledAnalysisStatus_ScheduledAnalysis] FOREIGN KEY([StatusID])
REFERENCES [dbo].[ScheduledAnalysisStatus] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ScheduledAnalysisStatus_ScheduledAnalysis]') AND parent_object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysis]'))
ALTER TABLE [dbo].[ScheduledAnalysis] CHECK CONSTRAINT [FK_ScheduledAnalysisStatus_ScheduledAnalysis]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ScheduledAnalysis_ScheduledAnalysisRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysisRule]'))
ALTER TABLE [dbo].[ScheduledAnalysisRule]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledAnalysis_ScheduledAnalysisRule] FOREIGN KEY([ScheduledAnalysisID])
REFERENCES [dbo].[ScheduledAnalysis] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ScheduledAnalysis_ScheduledAnalysisRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[ScheduledAnalysisRule]'))
ALTER TABLE [dbo].[ScheduledAnalysisRule] CHECK CONSTRAINT [FK_ScheduledAnalysis_ScheduledAnalysisRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Section_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_Location_Section_EndLocationID] FOREIGN KEY([EndLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Section_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_Location_Section_EndLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Section_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_Location_Section_StartLocationID] FOREIGN KEY([StartLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Section_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_Location_Section_StartLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Segment_Section]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_Segment_Section] FOREIGN KEY([JourneyID], [SegmentID])
REFERENCES [dbo].[Segment] ([JourneyID], [ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Segment_Section]') AND parent_object_id = OBJECT_ID(N'[dbo].[Section]'))
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_Segment_Section]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_SectionPoint_JourneyID]') AND parent_object_id = OBJECT_ID(N'[dbo].[SectionPoint]'))
ALTER TABLE [dbo].[SectionPoint]  WITH CHECK ADD  CONSTRAINT [FK_Journey_SectionPoint_JourneyID] FOREIGN KEY([JourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_SectionPoint_JourneyID]') AND parent_object_id = OBJECT_ID(N'[dbo].[SectionPoint]'))
ALTER TABLE [dbo].[SectionPoint] CHECK CONSTRAINT [FK_Journey_SectionPoint_JourneyID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_SectionPoint_LocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[SectionPoint]'))
ALTER TABLE [dbo].[SectionPoint]  WITH CHECK ADD  CONSTRAINT [FK_Location_SectionPoint_LocationID] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_SectionPoint_LocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[SectionPoint]'))
ALTER TABLE [dbo].[SectionPoint] CHECK CONSTRAINT [FK_Location_SectionPoint_LocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_Segment]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment]  WITH CHECK ADD  CONSTRAINT [FK_Journey_Segment] FOREIGN KEY([JourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_Segment]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment] CHECK CONSTRAINT [FK_Journey_Segment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Segment_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment]  WITH CHECK ADD  CONSTRAINT [FK_Location_Segment_EndLocationID] FOREIGN KEY([EndLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Segment_EndLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment] CHECK CONSTRAINT [FK_Location_Segment_EndLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Segment_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment]  WITH CHECK ADD  CONSTRAINT [FK_Location_Segment_StartLocationID] FOREIGN KEY([StartLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Segment_StartLocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Segment]'))
ALTER TABLE [dbo].[Segment] CHECK CONSTRAINT [FK_Location_Segment_StartLocationID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_TmsFault]') AND parent_object_id = OBJECT_ID(N'[dbo].[TmsFault]'))
ALTER TABLE [dbo].[TmsFault]  WITH CHECK ADD  CONSTRAINT [FK_Fault_TmsFault] FOREIGN KEY([FaultID])
REFERENCES [dbo].[Fault] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fault_TmsFault]') AND parent_object_id = OBJECT_ID(N'[dbo].[TmsFault]'))
ALTER TABLE [dbo].[TmsFault] CHECK CONSTRAINT [FK_Fault_TmsFault]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TmsFaultLookup_TmsFault]') AND parent_object_id = OBJECT_ID(N'[dbo].[TmsFault]'))
ALTER TABLE [dbo].[TmsFault]  WITH CHECK ADD  CONSTRAINT [FK_TmsFaultLookup_TmsFault] FOREIGN KEY([FaultNo])
REFERENCES [dbo].[TmsFaultLookup] ([FaultNo])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TmsFaultLookup_TmsFault]') AND parent_object_id = OBJECT_ID(N'[dbo].[TmsFault]'))
ALTER TABLE [dbo].[TmsFault] CHECK CONSTRAINT [FK_TmsFaultLookup_TmsFault]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_TrainPassage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassage]'))
ALTER TABLE [dbo].[TrainPassage]  WITH CHECK ADD  CONSTRAINT [FK_Journey_TrainPassage] FOREIGN KEY([JourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_TrainPassage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassage]'))
ALTER TABLE [dbo].[TrainPassage] CHECK CONSTRAINT [FK_Journey_TrainPassage]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_IdealRun_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection]  WITH CHECK ADD  CONSTRAINT [FK_IdealRun_TrainPassageSection] FOREIGN KEY([IdealRunID])
REFERENCES [dbo].[IdealRun] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_IdealRun_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection] CHECK CONSTRAINT [FK_IdealRun_TrainPassageSection]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection]  WITH CHECK ADD  CONSTRAINT [FK_Section_TrainPassageSection] FOREIGN KEY([JourneyID], [SectionID])
REFERENCES [dbo].[Section] ([JourneyID], [ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection] CHECK CONSTRAINT [FK_Section_TrainPassageSection]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection]  WITH CHECK ADD  CONSTRAINT [FK_TrainPassage_TrainPassageSection] FOREIGN KEY([TrainPassageID])
REFERENCES [dbo].[TrainPassage] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection] CHECK CONSTRAINT [FK_TrainPassage_TrainPassageSection]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_TrainPassageSection] FOREIGN KEY([LeadingVehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_TrainPassageSection]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSection]'))
ALTER TABLE [dbo].[TrainPassageSection] CHECK CONSTRAINT [FK_Vehicle_TrainPassageSection]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DelayReason_TrainPassageSectionDelayReason]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionDelayReason]'))
ALTER TABLE [dbo].[TrainPassageSectionDelayReason]  WITH CHECK ADD  CONSTRAINT [FK_DelayReason_TrainPassageSectionDelayReason] FOREIGN KEY([DelayReasonID])
REFERENCES [dbo].[DelayReason] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DelayReason_TrainPassageSectionDelayReason]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionDelayReason]'))
ALTER TABLE [dbo].[TrainPassageSectionDelayReason] CHECK CONSTRAINT [FK_DelayReason_TrainPassageSectionDelayReason]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSectionDelayReason]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionDelayReason]'))
ALTER TABLE [dbo].[TrainPassageSectionDelayReason]  WITH CHECK ADD  CONSTRAINT [FK_TrainPassage_TrainPassageSectionDelayReason] FOREIGN KEY([TrainPassageSectionID])
REFERENCES [dbo].[TrainPassageSection] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSectionDelayReason]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionDelayReason]'))
ALTER TABLE [dbo].[TrainPassageSectionDelayReason] CHECK CONSTRAINT [FK_TrainPassage_TrainPassageSectionDelayReason]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SectionPoint_TrainPassageSectionPoint]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]'))
ALTER TABLE [dbo].[TrainPassageSectionPoint]  WITH CHECK ADD  CONSTRAINT [FK_SectionPoint_TrainPassageSectionPoint] FOREIGN KEY([JourneyID], [SectionPointID])
REFERENCES [dbo].[SectionPoint] ([JourneyID], [ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SectionPoint_TrainPassageSectionPoint]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]'))
ALTER TABLE [dbo].[TrainPassageSectionPoint] CHECK CONSTRAINT [FK_SectionPoint_TrainPassageSectionPoint]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSectionPoint]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]'))
ALTER TABLE [dbo].[TrainPassageSectionPoint]  WITH CHECK ADD  CONSTRAINT [FK_TrainPassage_TrainPassageSectionPoint] FOREIGN KEY([TrainPassageID])
REFERENCES [dbo].[TrainPassage] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSectionPoint]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSectionPoint]'))
ALTER TABLE [dbo].[TrainPassageSectionPoint] CHECK CONSTRAINT [FK_TrainPassage_TrainPassageSectionPoint]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment]  WITH CHECK ADD  CONSTRAINT [FK_Journey_TrainPassageSegment] FOREIGN KEY([JourneyID])
REFERENCES [dbo].[Journey] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Journey_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment] CHECK CONSTRAINT [FK_Journey_TrainPassageSegment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment]  WITH CHECK ADD  CONSTRAINT [FK_Section_TrainPassageSegment] FOREIGN KEY([JourneyID], [SegmentID])
REFERENCES [dbo].[Segment] ([JourneyID], [ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Section_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment] CHECK CONSTRAINT [FK_Section_TrainPassageSegment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment]  WITH CHECK ADD  CONSTRAINT [FK_TrainPassage_TrainPassageSegment] FOREIGN KEY([TrainPassageID])
REFERENCES [dbo].[TrainPassage] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainPassage_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment] CHECK CONSTRAINT [FK_TrainPassage_TrainPassageSegment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_TrainPassageSegment] FOREIGN KEY([LeadingVehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_TrainPassageSegment]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainPassageSegment]'))
ALTER TABLE [dbo].[TrainPassageSegment] CHECK CONSTRAINT [FK_Vehicle_TrainPassageSegment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fleet_Unit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Unit]'))
ALTER TABLE [dbo].[Unit]  WITH CHECK ADD  CONSTRAINT [FK_Fleet_Unit] FOREIGN KEY([FleetID])
REFERENCES [dbo].[Fleet] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fleet_Unit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Unit]'))
ALTER TABLE [dbo].[Unit] CHECK CONSTRAINT [FK_Fleet_Unit]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HardwareType_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Vehicle]'))
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_HardwareType_Vehicle] FOREIGN KEY([HardwareTypeID])
REFERENCES [dbo].[HardwareType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HardwareType_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Vehicle]'))
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_HardwareType_Vehicle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Unit_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Vehicle]'))
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Unit_Vehicle] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Unit] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Unit_Vehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[Vehicle]'))
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_Unit_Vehicle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_VehicleDownload]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleDownload]'))
ALTER TABLE [dbo].[VehicleDownload]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_VehicleDownload] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_VehicleDownload]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleDownload]'))
ALTER TABLE [dbo].[VehicleDownload] CHECK CONSTRAINT [FK_Vehicle_VehicleDownload]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_VehicleEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleEvent]'))
ALTER TABLE [dbo].[VehicleEvent]  WITH NOCHECK ADD  CONSTRAINT [FK_Vehicle_VehicleEvent] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicle] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Vehicle_VehicleEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleEvent]'))
ALTER TABLE [dbo].[VehicleEvent] NOCHECK CONSTRAINT [FK_Vehicle_VehicleEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_GeniusLog_LogType]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusLog]'))
ALTER TABLE [dbo].[GeniusLog]  WITH CHECK ADD  CONSTRAINT [CK_GeniusLog_LogType] CHECK  (([LogType]>=(1) AND [LogType]<=(3)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_GeniusLog_LogType]') AND parent_object_id = OBJECT_ID(N'[dbo].[GeniusLog]'))
ALTER TABLE [dbo].[GeniusLog] CHECK CONSTRAINT [CK_GeniusLog_LogType]
GO
/****** Object:  Trigger [dbo].[TR_VehicleEvent]    Script Date: 14/07/2016 15:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TR_VehicleEvent]'))
EXEC dbo.sp_executesql @statement = N'
CREATE TRIGGER [dbo].[TR_VehicleEvent]
   ON  [dbo].[VehicleEvent]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT OFF;

	-- copy RMD and PL related		
	IF EXISTS (
		SELECT TOP 1 EventCategory 
		FROM inserted 
		WHERE EventCategory IN (24, 255)
	)
		INSERT INTO VehicleDownloadProgress(
			VehicleEventID
			, VehicleID
			, EventCategory
			, EventCode)
		SELECT
			ID
			, VehicleID
			, EventCategory
			, EventCode
		FROM Inserted

END
' 
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'VehicleID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to Vehicle table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'VehicleID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'EngColumnNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column number in ChannelValue table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'EngColumnNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'ChannelGroupID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to ChannelGroup table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'ChannelGroupID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Channel name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'Header'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Channel header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'Header'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'Ref'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference Code (eg. D01, D02)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'Ref'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'DataType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL data type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'DataType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Channel', N'COLUMN',N'TypeID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to ChannelType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Channel', @level2type=N'COLUMN',@level2name=N'TypeID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelGroup', N'COLUMN',N'ChannelGroupName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Channel Group Name (e.g. Traction, Air, Brake)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelGroup', @level2type=N'COLUMN',@level2name=N'ChannelGroupName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelGroup', N'COLUMN',N'Notes'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelGroup', @level2type=N'COLUMN',@level2name=N'Notes'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelValue', N'COLUMN',N'UpdateRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates if record was inserted live 
1 for ?live? records
0 for buffered records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelValue', @level2type=N'COLUMN',@level2name=N'UpdateRecord'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelValue', N'COLUMN',N'UnitID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to Unit table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelValue', @level2type=N'COLUMN',@level2name=N'UnitID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelValue', N'COLUMN',N'RecordInsert'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time when record was inserted into database' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelValue', @level2type=N'COLUMN',@level2name=N'RecordInsert'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ChannelValue', N'COLUMN',N'TimeStamp'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timestamp for record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChannelValue', @level2type=N'COLUMN',@level2name=N'TimeStamp'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'GeniusLog', N'COLUMN',N'LogType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Info 2 = Error 3 = Warning' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeniusLog', @level2type=N'COLUMN',@level2name=N'LogType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'HardwareTypeID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to HardwareType table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'HardwareTypeID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'ChannelID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to Channel table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'ChannelID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'Ref'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Channel Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'Ref'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'ScalingGain'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scaling Factor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'ScalingGain'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'ScalingOffset'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scaling Factor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'ScalingOffset'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'ScalingInversion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scaling Factor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'ScalingInversion'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'RMD'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On Train Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'RMD'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'Source'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On Train Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'Source'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'Card'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On Train Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'Card'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareChannel', N'COLUMN',N'Channel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On Train Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareChannel', @level2type=N'COLUMN',@level2name=N'Channel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'HardwareType', N'COLUMN',N'TypeName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hardware Type (e.g. OTMR Type)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HardwareType', @level2type=N'COLUMN',@level2name=N'TypeName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'LocationArea', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allowd values: C, O; C - Covering Geofence, it is used to determine location for Fleet Summary; O - Small Geofence, 500x500m used in Ops Analysis' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LocationArea', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ReportAdhesion', N'COLUMN',N'EventType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - WSP, 2 - Sand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportAdhesion', @level2type=N'COLUMN',@level2name=N'EventType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Vehicle', N'COLUMN',N'VehicleNumber'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicle', @level2type=N'COLUMN',@level2name=N'VehicleNumber'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Vehicle', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Type ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicle', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Vehicle', N'COLUMN',N'UnitID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to Unit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicle', @level2type=N'COLUMN',@level2name=N'UnitID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Vehicle', N'COLUMN',N'ConfigDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Train set was configured' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicle', @level2type=N'COLUMN',@level2name=N'ConfigDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleDownload', N'COLUMN',N'Status'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - requested, 2 - confirmed, 0 - completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleDownload', @level2type=N'COLUMN',@level2name=N'Status'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleDownload', N'COLUMN',N'IsManual'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allowd values: 0 - automatic scheduled download, 1 - user requested download' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleDownload', @level2type=N'COLUMN',@level2name=N'IsManual'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleDownload', N'COLUMN',N'FileType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - OTMR download' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleDownload', @level2type=N'COLUMN',@level2name=N'FileType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'UpdateRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates if record was inserted live 
1 for ?live? records
0 for buffered records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'UpdateRecord'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'VehicleID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign Key to Vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'VehicleID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'RecordInsert'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time when record was inserted into database' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'RecordInsert'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'TimeStamp'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timestamp for record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'TimeStamp'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'Latitude'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GPS Latitude' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'Latitude'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleEvent', N'COLUMN',N'Longitude'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GPS Longitude' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleEvent', @level2type=N'COLUMN',@level2name=N'Longitude'
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,$(scriptNumber)
           ,'update_spectrum_db_$(scriptNumber).sql'
           ,'1.0.01')
GO