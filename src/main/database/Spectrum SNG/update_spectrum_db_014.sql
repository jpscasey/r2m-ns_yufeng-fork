SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 014
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_SearchFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_SearchFault
**  Description:    Returns list faults for a keyword
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Keyword varchar(50)
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_SearchFaultAdvanced
**  Description:    Returns list of Faults matching parameters
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Headcode varchar(10)
**                  @UnitNumber varchar(10)
**                  @Vehicle varchar(10)
**                  @Location varchar(10)
**                  @FaultMetaID int
**                  @Description varchar(50)
**                  @Type varchar(10)
**                  @CategoryID int
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
)
AS
BEGIN 

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @Type = ''       SET @Type = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
			, FleetCode 
			, fm.AdditionalInfo
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLive AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultLive
**	Description:	Returns list of all Current Faults
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
AS

	SET NOCOUNT ON;
	
    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
		, FleetCode
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultNotLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLive AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLive
**	Description:	Returns list of N recent Faults
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
	@N int
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
)
AS
	SET NOCOUNT ON;
	
 	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
		AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
		AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnit AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultLiveUnit
**	Description:	Returns list of all Current Faults for a unit
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
	@UnitID int
)
AS

	SET NOCOUNT ON;

	SELECT
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultNotLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLiveUnit AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLiveUnit
**	Description:	Returns list of N recent Faults for set
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**					@UnitID
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/


ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
	@N int
	, @UnitID int
)
AS
	SET NOCOUNT ON;

	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_GetFaultDetail
**  Description:    Returns details of the Fault
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
			, CreateTime    
			, HeadCode  
			, SetCode   
			, f.FaultCode   
			, LocationCode  
			, IsCurrent 
			, EndTime   
			, RecoveryID    
			, Latitude  
			, Longitude    
			, f.FaultMetaID   
			--, UnitNumber   
		    , FleetCode  
			, FaultUnitID    
            , FaultUnitNumber
			, IsDelayed 
			, f.Description   
			, f.Summary   
			, FaultType
			, FaultTypeColor
			, Category
			, f.CategoryID
			, ReportingOnly
			, '' as TransmissionStatus
			, fme.E2MFaultCode
			, fme.E2MDescription
			, fme.E2MPriorityCode
			, fme.E2MPriority
            , fm.Url
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE f.ID = @FaultID

END
GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultSince', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultSince AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultSince
**	Description:	Returns list of all Faults created after the specified time argument
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultSince]
	@since datetime
AS
BEGIN

	SET NOCOUNT ON;
	
    
	SET NOCOUNT ON;
	
    SELECT 
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
	FROM VW_IX_Fault f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    WHERE CreateTime > @since
	    AND ReportingOnly = 0
    ORDER BY CreateTime DESC

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelData AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultChannelData
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelData]
(
	@FaultID int
	, @UnitID int
)
AS
BEGIN

		SELECT
			cv.[ID]
			,cv.[FaultID]
			,cv.[UpdateRecord]
			,cv.[RecordInsert]
			,cv.[TimeStamp]
			,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399] 
		FROM dbo.FaultChannelValue cv
		WHERE cv.FaultID = @FaultID
			AND cv.UnitID = @UnitID

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating view VW_ChannelDefinition', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'VW_ChannelDefinition')
    EXEC ('CREATE VIEW dbo.VW_ChannelDefinition AS Select 1 as Temp;')
GO

/******************************************************************************
**	Name:			VW_ChannelDefinition
**	Description:	Wrapper view for VW_ChannelDefinition table. 
*******************************************************************************/	

ALTER VIEW [dbo].[VW_ChannelDefinition]
AS
    SELECT     
        c.ID
        , c.VehicleID
        , c.EngColumnNo AS ColumnID
        , c.ChannelGroupID
        , c.Header AS Name
        , c.Name AS Description
        , c.ChannelGroupOrder
        , ct.Name AS Type
        , c.UOM
        , c.HardwareType
        , c.StorageMethod
        , c.Ref
        , c.MinValue
        , c.MaxValue
        , c.Comment
        , c.VisibleOnFaultOnly
        , c.IsAlwaysDisplayed
        , c.IsLookup
        , c.DefaultValue
        , c.IsDisplayedAsDifference
        , rc.Name AS RelatedChannelName
        , rc.ID AS RelatedChannelID
		, c.DataType
    FROM dbo.Channel AS c 
    INNER JOIN dbo.ChannelType AS ct ON c.TypeID = ct.ID
    LEFT JOIN RelatedChannel rc ON c.RelatedChannelID = rc.ID
GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_SearchChannelDefinition', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchChannelDefinition AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           [NSP_SearchChannelDefinition]
**  Description:    Returns list channel definition for a keyword
**  Call frequency: Called from Channel Definition module
**  Parameters:     @ChannelId int
**                  @Keyword varchar(50)
**  Return values:  
*******************************************************************************/
        
ALTER PROCEDURE [dbo].[NSP_SearchChannelDefinition]
    @ChannelId int = NULL,
    @Keyword varchar(50) = NULL
AS
BEGIN
    IF @ChannelId IS NULL
    BEGIN
        IF @Keyword IS NULL OR LEN(@Keyword) = 0
            SET @Keyword = '%'
        ELSE
            SET @Keyword = '%' + @Keyword + '%'
    END

    SELECT
            vcd.ID
            , vcd.VehicleID
            , vcd.ColumnID
            , vcd.ChannelGroupID
            , vcd.Name
            , vcd.Description
            , vcd.ChannelGroupOrder
            , vcd.Type
            , vcd.UOM
            , vcd.HardwareType
            , vcd.StorageMethod
            , vcd.Ref
            , vcd.MinValue
            , vcd.MaxValue
            , vcd.Comment
            , vcd.VisibleOnFaultOnly
            , vcd.IsAlwaysDisplayed
            , vcd.IsLookup
            , vcd.DefaultValue
            , vcd.IsDisplayedAsDifference
            , vcd.RelatedChannelID
            , vcd.RelatedChannelName
			, vcd.DataType
    FROM VW_ChannelDefinition as vcd
    INNER JOIN ChannelGroup as cgd ON vcd.ChannelGroupID = cgd.ID
    WHERE
        (@ChannelId IS NULL
            AND (vcd.ID LIKE @Keyword
                OR cgd.Notes LIKE @Keyword
                OR vcd.Description LIKE @Keyword
                OR vcd.Name LIKE @Keyword
                OR vcd.Ref LIKE @Keyword
                OR vcd.Type LIKE @Keyword
                OR vcd.UOM LIKE @Keyword))
        OR
        (@ChannelId IS NOT NULL AND vcd.ID = @ChannelId)
    ORDER BY vcd.ID
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,014
           ,'update_spectrum_db_014.sql'
           ,'1.0.01')
GO