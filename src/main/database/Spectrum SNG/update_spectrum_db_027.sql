SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 027
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- UPDATE PROCEDURE NSP_EaReport
--------------------------------------------
RAISERROR ('--Updating procedure NSP_EaReport', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaReport' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_EaReport AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_EaReport
**  Description:
**  Call frequency: Called from Event Analysis
**  Parameters:
**  Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaReport]
(
    @FleetCode varchar(10)
    , @DateFrom datetime
    , @DateTo datetime
    -- additional parameters: need to be in alphabetical order
    , @CategoryID varchar(100)
    , @FaultMetaID varchar(max)
    , @FaultTypeID varchar(50)
    , @LocationID varchar(max)
    , @UnitID varchar(10)
    , @VehicleID varchar(max)
)
AS
BEGIN

    DECLARE @FaultCategoryTable TABLE (CategoryID varchar(10))
    INSERT INTO @FaultCategoryTable
    SELECT * from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultMetaTable TABLE (FaultMetaID varchar(10))
    INSERT INTO @FaultMetaTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultMetaID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeID varchar(10))
    INSERT INTO @FaultTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultTypeID,',')

    DECLARE @LocationTable TABLE (LocationID varchar(10))
    INSERT INTO @LocationTable
    SELECT * from [dbo].SplitStrings_CTE(@LocationID,',')

    DECLARE @UnitIDTable TABLE (UnitID varchar(10))
    INSERT INTO @UnitIDTable
    SELECT * from [dbo].SplitStrings_CTE(@UnitID,',')

    DECLARE @VehicleIDTable TABLE (VehicleID varchar(100))
    INSERT INTO @VehicleIDTable
    SELECT * from [dbo].SplitStrings_CTE(@VehicleID,',')

    IF @CategoryID = '' SET @CategoryID = NULL
    IF @FaultMetaID = '' SET @FaultMetaID = NULL
    IF @FaultTypeID = '' SET @FaultTypeID = NULL
    IF @LocationID = '' SET @LocationID = NULL
    IF @VehicleID = '' SET @VehicleID = NULL
    IF @UnitID = '' SET @UnitID = NULL

    DECLARE @VehicleList TABLE (ID int)
    DECLARE @FaultMetaList TABLE (ID int)

    INSERT INTO @VehicleList
    SELECT ID
    FROM Vehicle
    WHERE (ID IN (SELECT * FROM @VehicleIDTable) OR @VehicleID IS NULL OR (@VehicleID = '-1' AND ID IS NULL))
        AND (UnitID  IN (SELECT * FROM @UnitIDTable) OR @UnitID IS NULL OR (@UnitID = '-1' AND Type IS NULL))

    INSERT INTO @FaultMetaList
    SELECT ID
    FROM FaultMeta
    WHERE (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultTypeID = '-1' AND FaultTypeID IS NULL))
        AND (ID IN (SELECT * FROM @FaultMetaTable) OR @FaultMetaID IS NULL OR (@FaultMetaID = '-1' AND ID IS NULL))
        AND (CategoryID IN (SELECT * FROM @FaultCategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))

    SELECT
        FaultID = f.ID
        , CreateTime
        , EndTime
        , IsCurrent

        , HeadCode
        , SetCode
        , FleetCode
        , FaultUnitNumber AS UnitNumber
        , UnitID    = v.UnitID
        , FaultUnitID
        , FaultVehicleNumber = v.VehicleNumber
		, FaultVehicleID = NULL

        , FaultMetaID
        , FaultCode
        , Description

        , FaultTypeID
        , FaultType
        , FaultTypeColor
        , Priority

        , CategoryID
        , Category

        , Latitude
        , Longitude
        , LocationID
        , LocationCode

    FROM VW_IX_Fault f (NOEXPAND)
    INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
    LEFT JOIN Location l ON l.ID = LocationID
    WHERE f.FaultUnitID IN (SELECT ID FROM @VehicleList)
        AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
        AND f.CreateTime BETWEEN @DateFrom AND @DateTo
        AND (LocationID = @LocationID OR @LocationID IS NULL OR (@LocationID = -1 AND LocationID IS NULL))
        AND (FleetCode = @FleetCode OR @FleetCode IS NULL)

END



GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,027
           ,'update_spectrum_db_027.sql'
           ,'1.1.01')
GO