SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 033
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- UPDATE PROCEDURE NSP_InsertChannelValue
--------------------------------------------
RAISERROR ('--Updating procedure NSP_InsertChannelValue', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
	    @UpdateRecord bit = null,
		@col1 decimal(9,6) = null,
		@col2 decimal(9,6) = null,
		@col3 smallint = null,
		@col4 smallint = null,
		@col5 bit = null,
		@col6 bit = null,
		@col7 bit = null,
		@col8 bit = null,
		@col9 bit = null,
		@col10 bit = null,
		@col11 bit = null,
		@col12 bit = null,
		@col13 bit = null,
		@col14 bit = null,
		@col15 bit = null,
		@col16 bit = null,
		@col17 bit = null,
		@col18 bit = null,
		@col19 bit = null,
		@col20 bit = null,
		@col21 bit = null,
		@col22 bit = null,
		@col23 bit = null,
		@col24 bit = null,
		@col25 bit = null,
		@col26 bit = null,
		@col27 bit = null,
		@col28 bit = null,
		@col29 bit = null,
		@col30 bit = null,
		@col31 bit = null,
		@col32 bit = null,
		@col33 bit = null,
		@col34 bit = null,
		@col35 bit = null,
		@col36 bit = null,
		@col37 bit = null,
		@col38 bit = null,
		@col39 bit = null,
		@col40 bit = null,
		@col41 bit = null,
		@col42 bit = null,
		@col43 bit = null,
		@col44 bit = null,
		@col45 bit = null,
		@col46 bit = null,
		@col47 bit = null,
		@col48 bit = null,
		@col49 bit = null,
		@col50 bit = null,
		@col51 bit = null,
		@col52 bit = null,
		@col53 bit = null,
		@col54 bit = null,
		@col55 bit = null,
		@col56 bit = null,
		@col57 bit = null,
		@col58 bit = null,
		@col59 bit = null,
		@col60 bit = null,
		@col61 smallint = null,
		@col62 smallint = null,
		@col63 smallint = null,
		@col64 smallint = null,
		@col65 smallint = null,
		@col66 bit = null,
		@col67 bit = null,
		@col68 bit = null,
		@col69 bit = null,
		@col70 bit = null,
		@col71 bit = null,
		@col72 bit = null,
		@col73 bit = null,
		@col74 bit = null,
		@col75 bit = null,
		@col76 bit = null,
		@col77 bit = null,
		@col78 bit = null,
		@col79 bit = null,
		@col80 bit = null,
		@col81 bit = null,
		@col82 smallint = null,
		@col83 tinyint = null,
		@col84 tinyint = null,
		@col85 smallint = null,
		@col86 smallint = null,
		@col87 smallint = null,
		@col88 smallint = null,
		@col89 smallint = null,
		@col90 smallint = null,
		@col91 smallint = null,
		@col92 smallint = null,
		@col93 bit = null,
		@col94 bit = null,
		@col95 bit = null,
		@col96 bit = null,
		@col97 bit = null,
		@col98 bit = null,
		@col99 bit = null,
		@col100 bit = null,
		@col101 bit = null,
		@col102 bit = null,
		@col103 bit = null,
		@col104 bit = null,
		@col105 bit = null,
		@col106 bit = null,
		@col107 bit = null,
		@col108 bit = null,
		@col109 smallint = null,
		@col110 tinyint = null,
		@col111 tinyint = null,
		@col112 smallint = null,
		@col113 smallint = null,
		@col114 smallint = null,
		@col115 tinyint = null,
		@col116 tinyint = null,
		@col117 bit = null,
		@col118 bit = null,
		@col119 bit = null,
		@col120 tinyint = null,
		@col121 tinyint = null,
		@col122 bit = null,
		@col123 bit = null,
		@col124 bit = null,
		@col125 bit = null,
		@col126 bit = null,
		@col127 bit = null,
		@col128 bit = null,
		@col129 bit = null,
		@col130 bit = null,
		@col131 bit = null,
		@col132 bit = null,
		@col133 bit = null,
		@col134 bit = null,
		@col135 bit = null,
		@col136 bit = null,
		@col137 bit = null,
		@col138 bit = null,
		@col139 bit = null,
		@col140 bit = null,
		@col141 bit = null,
		@col142 bit = null,
		@col143 bit = null,
		@col144 bit = null,
		@col145 bit = null,
		@col146 bit = null,
		@col147 bit = null,
		@col148 bit = null,
		@col149 bit = null,
		@col150 bit = null,
		@col151 bit = null,
		@col152 bit = null,
		@col153 bit = null,
		@col154 bit = null,
		@col155 bit = null,
		@col156 bit = null,
		@col157 bit = null,
		@col158 bit = null,
		@col159 bit = null,
		@col160 bit = null,
		@col161 bit = null,
		@col162 bit = null,
		@col163 bit = null,
		@col164 bit = null,
		@col165 bit = null,
		@col166 bit = null,
		@col167 bit = null,
		@col168 bit = null,
		@col169 tinyint = null,
		@col170 tinyint = null,
		@col171 tinyint = null,
		@col172 tinyint = null,
		@col173 tinyint = null,
		@col174 bit = null,
		@col175 bit = null,
		@col176 bit = null,
		@col177 bit = null,
		@col178 bit = null,
		@col179 bit = null,
		@col180 bit = null,
		@col181 bit = null,
		@col182 bit = null,
		@col183 bit = null,
		@col184 bit = null,
		@col185 bit = null,
		@col186 bit = null,
		@col187 bit = null,
		@col188 bit = null,
		@col189 bit = null,
		@col190 bit = null,
		@col191 bit = null,
		@col192 bit = null,
		@col193 bit = null,
		@col194 bit = null,
		@col195 bit = null,
		@col196 bit = null,
		@col197 bit = null,
		@col198 bit = null,
		@col199 bit = null,
		@col200 bit = null,
		@col201 bit = null,
		@col202 bit = null,
		@col203 bit = null,
		@col204 bit = null,
		@col205 bit = null,
		@col206 bit = null,
		@col207 bit = null,
		@col208 bit = null,
		@col209 bit = null,
		@col210 bit = null,
		@col211 bit = null,
		@col212 bit = null,
		@col213 bit = null,
		@col214 bit = null,
		@col215 bit = null,
		@col216 bit = null,
		@col217 bit = null,
		@col218 bit = null,
		@col219 bit = null,
		@col220 bit = null,
		@col221 bit = null,
		@col222 bit = null,
		@col223 bit = null,
		@col224 bit = null,
		@col225 bit = null,
		@col226 bit = null,
		@col227 bit = null,
		@col228 bit = null,
		@col229 bit = null,
		@col230 bit = null,
		@col231 bit = null,
		@col232 bit = null,
		@col233 bit = null,
		@col234 bit = null,
		@col235 bit = null,
		@col236 bit = null,
		@col237 bit = null,
		@col238 bit = null,
		@col239 bit = null,
		@col240 bit = null,
		@col241 bit = null,
		@col242 bit = null,
		@col243 bit = null,
		@col244 bit = null,
		@col245 bit = null,
		@col246 bit = null,
		@col247 bit = null,
		@col248 bit = null,
		@col249 bit = null,
		@col250 bit = null,
		@col251 bit = null,
		@col252 smallint = null,
		@col253 smallint = null,
		@col254 smallint = null,
		@col255 smallint = null,
		@col256 bit = null,
		@col257 bit = null,
		@col258 bit = null,
		@col259 bit = null,
		@col260 bit = null,
		@col261 bit = null,
		@col262 bit = null,
		@col263 bit = null,
		@col264 bit = null,
		@col265 bit = null,
		@col266 bit = null,
		@col267 bit = null,
		@col268 bit = null,
		@col269 bit = null,
		@col270 bit = null,
		@col271 bit = null,
		@col272 bit = null,
		@col273 bit = null,
		@col274 bit = null,
		@col275 bit = null,
		@col276 bit = null,
		@col277 bit = null,
		@col278 bit = null,
		@col279 bit = null,
		@col280 bit = null,
		@col281 bit = null,
		@col282 bit = null,
		@col283 bit = null,
		@col284 bit = null,
		@col285 bit = null,
		@col286 bit = null,
		@col287 bit = null,
		@col288 bit = null,
		@col289 bit = null,
		@col290 bit = null,
		@col291 bit = null,
		@col292 bit = null,
		@col293 bit = null,
		@col294 bit = null,
		@col295 bit = null,
		@col296 bit = null,
		@col297 smallint = null,
		@col298 smallint = null,
		@col299 smallint = null,
		@col300 smallint = null,
		@col301 smallint = null,
		@col302 smallint = null,
		@col303 smallint = null,
		@col304 smallint = null,
		@col305 smallint = null,
		@col306 smallint = null,
		@col307 smallint = null,
		@col308 smallint = null,
		@col309 smallint = null,
		@col310 smallint = null,
		@col311 smallint = null,
		@col312 smallint = null,
		@col313 smallint = null,
		@col314 smallint = null,
		@col315 smallint = null,
		@col316 smallint = null,
		@col317 smallint = null,
		@col318 smallint = null,
		@col319 smallint = null,
		@col320 smallint = null,
		@col321 smallint = null,
		@col322 smallint = null,
		@col323 smallint = null,
		@col324 smallint = null,
		@col325 smallint = null,
		@col326 smallint = null,
		@col327 smallint = null,
		@col328 smallint = null,
		@col329 smallint = null,
		@col330 smallint = null,
		@col331 smallint = null,
		@col332 smallint = null,
		@col333 smallint = null,
		@col334 smallint = null,
		@col335 smallint = null,
		@col336 smallint = null,
		@col337 smallint = null,
		@col338 bit = null,
		@col339 bit = null,
		@col340 bit = null,
		@col341 bit = null,
		@col342 bit = null,
		@col343 bit = null,
		@col344 bit = null,
		@col345 bit = null,
		@col346 bit = null,
		@col347 bit = null,
		@col348 bit = null,
		@col349 bit = null,
		@col350 bit = null,
		@col351 bit = null,
		@col352 bit = null,
		@col353 bit = null,
		@col354 bit = null,
		@col355 bit = null,
		@col356 bit = null,
		@col357 bit = null,
		@col358 bit = null,
		@col359 bit = null,
		@col360 bit = null,
		@col361 bit = null,
		@col362 tinyint = null,
		@col363 smallint = null,
		@col364 tinyint = null,
		@col365 tinyint = null,
		@col366 tinyint = null,
		@col367 tinyint = null,
		@col368 smallint = null,
		@col369 smallint = null,
		@col370 smallint = null,
		@col371 smallint = null,
		@col372 smallint = null,
		@col373 smallint = null,
		@col374 smallint = null,
		@col375 smallint = null,
		@col376 smallint = null,
		@col377 smallint = null,
		@col378 smallint = null,
		@col379 smallint = null,
		@col380 smallint = null,
		@col381 smallint = null,
		@col382 smallint = null,
		@col383 smallint = null,
		@col384 smallint = null,
		@col385 smallint = null,
		@col386 smallint = null,
		@col387 smallint = null,
		@col388 smallint = null,
		@col389 smallint = null,
		@col390 smallint = null,
		@col391 smallint = null,
		@col392 smallint = null,
		@col393 smallint = null,
		@col394 smallint = null,
		@col395 smallint = null,
		@col396 smallint = null,
		@col397 smallint = null,
		@col398 smallint = null,
		@col399 smallint = null,
		@col400 smallint = null,
		@col401 smallint = null,
		@col402 smallint = null,
		@col403 smallint = null,
		@col404 smallint = null,
		@col405 smallint = null,
		@col406 smallint = null,
		@col407 smallint = null,
		@col408 smallint = null,
		@col409 smallint = null,
		@col410 smallint = null,
		@col411 smallint = null,
		@col412 smallint = null,
		@col413 smallint = null,
		@col414 smallint = null,
		@col415 smallint = null,
		@col416 smallint = null,
		@col417 smallint = null,
		@col418 smallint = null,
		@col419 smallint = null,
		@col420 smallint = null,
		@col421 smallint = null,
		@col422 smallint = null,
		@col423 smallint = null,
		@col424 smallint = null,
		@col425 smallint = null,
		@col426 smallint = null,
		@col427 smallint = null,
		@col428 smallint = null,
		@col429 smallint = null,
		@col430 smallint = null,
		@col431 smallint = null,
		@col432 smallint = null,
		@col433 smallint = null,
		@col434 smallint = null,
		@col435 smallint = null,
		@col436 smallint = null,
		@col437 smallint = null,
		@col438 smallint = null,
		@col439 smallint = null,
		@col440 smallint = null,
		@col441 smallint = null,
		@col442 smallint = null,
		@col443 smallint = null,
		@col444 smallint = null,
		@col445 smallint = null,
		@col446 smallint = null,
		@col447 smallint = null,
		@col448 smallint = null,
		@col449 smallint = null,
		@col450 tinyint = null,
		@col451 tinyint = null,
		@col452 tinyint = null,
		@col453 tinyint = null,
		@col454 tinyint = null,
		@col455 tinyint = null,
		@col456 tinyint = null,
		@col457 tinyint = null,
		@col458 tinyint = null,
		@col459 tinyint = null,
		@col460 tinyint = null,
		@col461 smallint = null,
		@col462 tinyint = null,
		@col463 smallint = null,
		@col464 smallint = null,
		@col465 smallint = null,
		@col466 smallint = null,
		@col467 bit = null,
		@col468 bit = null,
		@col469 bit = null,
		@col470 bit = null,
		@col471 bit = null,
		@col472 bit = null,
		@col473 bit = null,
		@col474 bit = null,
		@col475 bit = null,
		@col476 bit = null,
		@col477 bit = null,
		@col478 bit = null,
		@col479 bit = null,
		@col480 bit = null,
		@col481 bit = null,
		@col482 bit = null,
		@col483 bit = null,
		@col484 bit = null,
		@col485 bit = null,
		@col486 bit = null,
		@col487 bit = null,
		@col488 bit = null,
		@col489 bit = null,
		@col490 bit = null,
		@col491 bit = null,
		@col492 bit = null,
		@col493 bit = null,
		@col494 bit = null,
		@col495 bit = null,
		@col496 bit = null,
		@col497 bit = null,
		@col498 bit = null,
		@col499 bit = null,
		@col500 bit = null,
		@col501 bit = null,
		@col502 bit = null,
		@col503 bit = null,
		@col504 bit = null,
		@col505 bit = null,
		@col506 bit = null,
		@col507 bit = null,
		@col508 bit = null,
		@col509 bit = null,
		@col510 bit = null,
		@col511 bit = null,
		@col512 bit = null,
		@col513 bit = null,
		@col514 bit = null,
		@col515 bit = null,
		@col516 bit = null,
		@col517 bit = null,
		@col518 bit = null,
		@col519 bit = null,
		@col520 bit = null,
		@col521 bit = null,
		@col522 bit = null,
		@col523 bit = null,
		@col524 bit = null,
		@col525 bit = null,
		@col526 bit = null,
		@col527 bit = null,
		@col528 bit = null,
		@col529 bit = null,
		@col530 bit = null,
		@col531 bit = null,
		@col532 bit = null,
		@col533 bit = null,
		@col534 bit = null,
		@col535 bit = null,
		@col536 bit = null,
		@col537 bit = null,
		@col538 bit = null,
		@col539 bit = null,
		@col540 bit = null,
		@col541 bit = null,
		@col542 bit = null,
		@col543 bit = null,
		@col544 bit = null,
		@col545 tinyint = null,
		@col546 tinyint = null,
		@col547 tinyint = null,
		@col548 tinyint = null,
		@col549 tinyint = null,
		@col550 tinyint = null,
		@col551 smallint = null,
		@col552 smallint = null,
		@col553 tinyint = null,
		@col554 tinyint = null,
		@col555 tinyint = null,
		@col556 tinyint = null,
		@col557 tinyint = null,
		@col558 tinyint = null,
		@col559 tinyint = null,
		@col560 tinyint = null,
		@col561 tinyint = null,
		@col562 bit = null,
		@col563 bit = null,
		@col564 smallint = null,
		@col565 smallint = null,
		@col566 bit = null,
		@col567 tinyint = null,
		@col568 smallint = null,
		@col569 smallint = null,
		@col570 tinyint = null,
		@col571 tinyint = null,
		@col572 smallint = null,
		@col573 tinyint = null,
		@col574 bit = null,
		@col575 bit = null,
		@col576 bit = null,
		@col577 bit = null,
		@col578 bit = null,
		@col579 bit = null,
		@col580 smallint = null,
		@col581 smallint = null,
		@col582 smallint = null,
		@col583 smallint = null,
		@col584 smallint = null,
		@col585 smallint = null,
		@col586 smallint = null,
		@col587 smallint = null,
		@col588 bit = null,
		@col589 bit = null,
		@col590 tinyint = null,
		@col591 tinyint = null,
		@col592 bit = null,
		@col593 bit = null,
		@col594 tinyint = null,
		@col595 bit = null,
		@col596 bit = null,
		@col597 tinyint = null,
		@col598 tinyint = null,
		@col599 smallint = null,
		@col600 tinyint = null,
		@col601 tinyint = null,
		@col602 tinyint = null,
		@col603 bit = null,
		@col604 tinyint = null,
		@col605 tinyint = null,
		@col606 tinyint = null,
		@col607 tinyint = null,
		@col608 tinyint = null,
		@col609 tinyint = null,
		@col610 tinyint = null,
		@col611 tinyint = null,
		@col612 tinyint = null,
		@col613 tinyint = null,
		@col614 tinyint = null,
		@col615 tinyint = null,
		@col616 tinyint = null,
		@col617 tinyint = null,
		@col618 tinyint = null,
		@col619 tinyint = null,
		@col620 tinyint = null,
		@col621 tinyint = null,
		@col622 tinyint = null,
		@col623 tinyint = null,
		@col624 tinyint = null,
		@col625 tinyint = null,
		@col626 tinyint = null,
		@col627 tinyint = null,
		@col628 tinyint = null,
		@col629 tinyint = null,
		@col630 tinyint = null,
		@col631 tinyint = null,
		@col632 tinyint = null,
		@col633 tinyint = null,
		@col634 tinyint = null,
		@col635 tinyint = null,
		@col636 tinyint = null,
		@col637 tinyint = null,
		@col638 tinyint = null,
		@col639 tinyint = null,
		@col640 tinyint = null,
		@col641 tinyint = null,
		@col642 tinyint = null,
		@col643 tinyint = null,
		@col644 tinyint = null,
		@col645 tinyint = null,
		@col646 bit = null,
		@col647 bit = null,
		@col648 smallint = null,
		@col649 smallint = null,
		@col650 smallint = null,
		@col651 bit = null,
		@col652 smallint = null,
		@col653 bit = null,
		@col654 bit = null,
		@col655 smallint = null,
		@col656 smallint = null,
		@col657 smallint = null,
		@col658 bit = null,
		@col659 smallint = null,
		@col660 tinyint = null,
		@col661 bit = null,
		@col662 bit = null,
		@col663 bit = null
)
AS
BEGIN

	INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[RecordInsert]
           ,[TimeStamp]
		   ,[UpdateRecord]
           ,[col1]           ,[col2]
           ,[col3]           ,[col4]
           ,[col5]           ,[col6]
           ,[col7]           ,[col8]
           ,[col9]           ,[col10]
           ,[col11]           ,[col12]
           ,[col13]           ,[col14]
           ,[col15]           ,[col16]
           ,[col17]           ,[col18]
           ,[col19]           ,[col20]
           ,[col21]           ,[col22]
           ,[col23]           ,[col24]
           ,[col25]           ,[col26]
           ,[col27]           ,[col28]
           ,[col29]           ,[col30]
           ,[col31]           ,[col32]
           ,[col33]           ,[col34]
           ,[col35]           ,[col36]
           ,[col37]           ,[col38]
           ,[col39]           ,[col40]
           ,[col41]           ,[col42]
           ,[col43]           ,[col44]
           ,[col45]           ,[col46]
           ,[col47]           ,[col48]
           ,[col49]           ,[col50]
           ,[col51]           ,[col52]
           ,[col53]           ,[col54]
           ,[col55]           ,[col56]
           ,[col57]           ,[col58]
           ,[col59]           ,[col60]
           ,[col61]           ,[col62]
           ,[col63]           ,[col64]
           ,[col65]           ,[col66]
           ,[col67]           ,[col68]
           ,[col69]           ,[col70]
           ,[col71]           ,[col72]
           ,[col73]           ,[col74]
           ,[col75]           ,[col76]
           ,[col77]           ,[col78]
           ,[col79]           ,[col80]
           ,[col81]           ,[col82]
           ,[col83]           ,[col84]
           ,[col85]           ,[col86]
           ,[col87]           ,[col88]
           ,[col89]           ,[col90]
           ,[col91]           ,[col92]
           ,[col93]           ,[col94]
           ,[col95]           ,[col96]
           ,[col97]           ,[col98]
           ,[col99]           ,[col100]
           ,[col101]
           ,[col102]
           ,[col103]
           ,[col104]
           ,[col105]
           ,[col106]
           ,[col107]
           ,[col108]
           ,[col109]
           ,[col110]
           ,[col111]
           ,[col112]
           ,[col113]
           ,[col114]
           ,[col115]
           ,[col116]
           ,[col117]
           ,[col118]
           ,[col119]
           ,[col120]
           ,[col121]
           ,[col122]
           ,[col123]
           ,[col124]
           ,[col125]
           ,[col126]
           ,[col127]
           ,[col128]
           ,[col129]
           ,[col130]
           ,[col131]
           ,[col132]
           ,[col133]
           ,[col134]
           ,[col135]
           ,[col136]
           ,[col137]
           ,[col138]
           ,[col139]
           ,[col140]
           ,[col141]
           ,[col142]
           ,[col143]
           ,[col144]
           ,[col145]
           ,[col146]
           ,[col147]
           ,[col148]
           ,[col149]
           ,[col150]
           ,[col151]
           ,[col152]
           ,[col153]
           ,[col154]
           ,[col155]
           ,[col156]
           ,[col157]
           ,[col158]
           ,[col159]
           ,[col160]
           ,[col161]
           ,[col162]
           ,[col163]
           ,[col164]
           ,[col165]
           ,[col166]
           ,[col167]
           ,[col168]
           ,[col169]
           ,[col170]
           ,[col171]
           ,[col172]
           ,[col173]
           ,[col174]
           ,[col175]
           ,[col176]
           ,[col177]
           ,[col178]
           ,[col179]
           ,[col180]
           ,[col181]
           ,[col182]
           ,[col183]
           ,[col184]
           ,[col185]
           ,[col186]
           ,[col187]
           ,[col188]
           ,[col189]
           ,[col190]
           ,[col191]
           ,[col192]
           ,[col193]
           ,[col194]
           ,[col195]
           ,[col196]
           ,[col197]
           ,[col198]
           ,[col199]
           ,[col200]
           ,[col201]
           ,[col202]
           ,[col203]
           ,[col204]
           ,[col205]
           ,[col206]
           ,[col207]
           ,[col208]
           ,[col209]
           ,[col210]
           ,[col211]
           ,[col212]
           ,[col213]
           ,[col214]
           ,[col215]
           ,[col216]
           ,[col217]
           ,[col218]
           ,[col219]
           ,[col220]
           ,[col221]
           ,[col222]
           ,[col223]
           ,[col224]
           ,[col225]
           ,[col226]
           ,[col227]
           ,[col228]
           ,[col229]
           ,[col230]
           ,[col231]
           ,[col232]
           ,[col233]
           ,[col234]
           ,[col235]
           ,[col236]
           ,[col237]
           ,[col238]
           ,[col239]
           ,[col240]
           ,[col241]
           ,[col242]
           ,[col243]
           ,[col244]
           ,[col245]
           ,[col246]
           ,[col247]
           ,[col248]
           ,[col249]
           ,[col250]
           ,[col251]
           ,[col252]
           ,[col253]
           ,[col254]
           ,[col255]
           ,[col256]
           ,[col257]
           ,[col258]
           ,[col259]
           ,[col260]
           ,[col261]
           ,[col262]
           ,[col263]
           ,[col264]
           ,[col265]
           ,[col266]
           ,[col267]
           ,[col268]
           ,[col269]
           ,[col270]
           ,[col271]
           ,[col272]
           ,[col273]
           ,[col274]
           ,[col275]
           ,[col276]
           ,[col277]
           ,[col278]
           ,[col279]
           ,[col280]
           ,[col281]
           ,[col282]
           ,[col283]
           ,[col284]
           ,[col285]
           ,[col286]
           ,[col287]
           ,[col288]
           ,[col289]
           ,[col290]
           ,[col291]
           ,[col292]
           ,[col293]
           ,[col294]
           ,[col295]
           ,[col296]
           ,[col297]
           ,[col298]
           ,[col299]
           ,[col300]
           ,[col301]
           ,[col302]
           ,[col303]
           ,[col304]
           ,[col305]
           ,[col306]
           ,[col307]
           ,[col308]
           ,[col309]
           ,[col310]
           ,[col311]
           ,[col312]
           ,[col313]
           ,[col314]
           ,[col315]
           ,[col316]
           ,[col317]
           ,[col318]
           ,[col319]
           ,[col320]
           ,[col321]
           ,[col322]
           ,[col323]
           ,[col324]
           ,[col325]
           ,[col326]
           ,[col327]
           ,[col328]
           ,[col329]
           ,[col330]
           ,[col331]
           ,[col332]
           ,[col333]
           ,[col334]
           ,[col335]
           ,[col336]
           ,[col337]
           ,[col338]
           ,[col339]
           ,[col340]
           ,[col341]
           ,[col342]
           ,[col343]
           ,[col344]
           ,[col345]
           ,[col346]
           ,[col347]
           ,[col348]
           ,[col349]
           ,[col350]
           ,[col351]
           ,[col352]
           ,[col353]
           ,[col354]
           ,[col355]
           ,[col356]
           ,[col357]
           ,[col358]
           ,[col359]
           ,[col360]
           ,[col361]
           ,[col362]
           ,[col363]
           ,[col364]
           ,[col365]
           ,[col366]
           ,[col367]
           ,[col368]
           ,[col369]
           ,[col370]
           ,[col371]
           ,[col372]
           ,[col373]
           ,[col374]
           ,[col375]
           ,[col376]
           ,[col377]
           ,[col378]
           ,[col379]
           ,[col380]
           ,[col381]
           ,[col382]
           ,[col383]
           ,[col384]
           ,[col385]
           ,[col386]
           ,[col387]
           ,[col388]
           ,[col389]
           ,[col390]
           ,[col391]
           ,[col392]
           ,[col393]
           ,[col394]
           ,[col395]
           ,[col396]
           ,[col397]
           ,[col398]
           ,[col399]
           ,[col400]
           ,[col401]
           ,[col402]
           ,[col403]
           ,[col404]
           ,[col405]
           ,[col406]
           ,[col407]
           ,[col408]
           ,[col409]
           ,[col410]
           ,[col411]
           ,[col412]
           ,[col413]
           ,[col414]
           ,[col415]
           ,[col416]
           ,[col417]
           ,[col418]
           ,[col419]
           ,[col420]
           ,[col421]
           ,[col422]
           ,[col423]
           ,[col424]
           ,[col425]
           ,[col426]
           ,[col427]
           ,[col428]
           ,[col429]
           ,[col430]
           ,[col431]
           ,[col432]
           ,[col433]
           ,[col434]
           ,[col435]
           ,[col436]
           ,[col437]
           ,[col438]
           ,[col439]
           ,[col440]
           ,[col441]
           ,[col442]
           ,[col443]
           ,[col444]
           ,[col445]
           ,[col446]
           ,[col447]
           ,[col448]
           ,[col449]
           ,[col450]
           ,[col451]
           ,[col452]
           ,[col453]
           ,[col454]
           ,[col455]
           ,[col456]
           ,[col457]
           ,[col458]
           ,[col459]
           ,[col460]
           ,[col461]
           ,[col462]
           ,[col463]
           ,[col464]
           ,[col465]
           ,[col466]
           ,[col467]
           ,[col468]
           ,[col469]
           ,[col470]
           ,[col471]
           ,[col472]
           ,[col473]
           ,[col474]
           ,[col475]
           ,[col476]
           ,[col477]
           ,[col478]
           ,[col479]
           ,[col480]
           ,[col481]
           ,[col482]
           ,[col483]
           ,[col484]
           ,[col485]
           ,[col486]
           ,[col487]
           ,[col488]
           ,[col489]
           ,[col490]
           ,[col491]
           ,[col492]
           ,[col493]
           ,[col494]
           ,[col495]
           ,[col496]
           ,[col497]
           ,[col498]
           ,[col499]
           ,[col500]
           ,[col501]
           ,[col502]
           ,[col503]
           ,[col504]
           ,[col505]
           ,[col506]
           ,[col507]
           ,[col508]
           ,[col509]
           ,[col510]
           ,[col511]
           ,[col512]
           ,[col513]
           ,[col514]
           ,[col515]
           ,[col516]
           ,[col517]
           ,[col518]
           ,[col519]
           ,[col520]
           ,[col521]
           ,[col522]
           ,[col523]
           ,[col524]
           ,[col525]
           ,[col526]
           ,[col527]
           ,[col528]
           ,[col529]
           ,[col530]
           ,[col531]
           ,[col532]
           ,[col533]
           ,[col534]
           ,[col535]
           ,[col536]
           ,[col537]
           ,[col538]
           ,[col539]
           ,[col540]
           ,[col541]
           ,[col542]
           ,[col543]
           ,[col544]
           ,[col545]
           ,[col546]
           ,[col547]
           ,[col548]
           ,[col549]
           ,[col550]
           ,[col551]
           ,[col552]
           ,[col553]
           ,[col554]
           ,[col555]
           ,[col556]
           ,[col557]
           ,[col558]
           ,[col559]
           ,[col560]
           ,[col561]
           ,[col562]
           ,[col563]
           ,[col564]
           ,[col565]
           ,[col566]
           ,[col567]
           ,[col568]
           ,[col569]
           ,[col570]
           ,[col571]
           ,[col572]
           ,[col573]
           ,[col574]
           ,[col575]
           ,[col576]
           ,[col577]
           ,[col578]
           ,[col579]
           ,[col580]
           ,[col581]
           ,[col582]
           ,[col583]
           ,[col584]
           ,[col585]
           ,[col586]
           ,[col587]
           ,[col588]
           ,[col589]
           ,[col590]
           ,[col591]
           ,[col592]
           ,[col593]
           ,[col594]
           ,[col595]
           ,[col596]
           ,[col597]
           ,[col598]
           ,[col599]
           ,[col600]
           ,[col601]
           ,[col602]
           ,[col603]
           ,[col604]
           ,[col605]
           ,[col606]
           ,[col607]
           ,[col608]
           ,[col609]
           ,[col610]
           ,[col611]
           ,[col612]
           ,[col613]
           ,[col614]
           ,[col615]
           ,[col616]
           ,[col617]
           ,[col618]
           ,[col619]
           ,[col620]
           ,[col621]
           ,[col622]
           ,[col623]
           ,[col624]
           ,[col625]
           ,[col626]
           ,[col627]
           ,[col628]
           ,[col629]
           ,[col630]
           ,[col631]
           ,[col632]
           ,[col633]
           ,[col634]
           ,[col635]
           ,[col636]
           ,[col637]
           ,[col638]
           ,[col639]
           ,[col640]
           ,[col641]
           ,[col642]
           ,[col643]
           ,[col644]
           ,[col645]
           ,[col646]
           ,[col647]
           ,[col648]
           ,[col649]
           ,[col650]
           ,[col651]
           ,[col652]
           ,[col653]
           ,[col654]
           ,[col655]
           ,[col656]
           ,[col657]
           ,[col658]
           ,[col659]
           ,[col660]
           ,[col661]
           ,[col662]
           ,[col663])
  	SELECT
		@UnitID, 
		getdate(),
		@Timestamp,
		@UpdateRecord,
		@col1,
		@col2,
		@col3 ,
		@col4 ,
		@col5 ,
		@col6 ,
		@col7 ,
		@col8 ,
		@col9 ,
		@col10 ,
		@col11 ,
		@col12 ,
		@col13 ,
		@col14 ,
		@col15 ,
		@col16 ,
		@col17 ,
		@col18 ,
		@col19 ,
		@col20 ,
		@col21 ,
		@col22 ,
		@col23 ,
		@col24 ,
		@col25 ,
		@col26 ,
		@col27 ,
		@col28 ,
		@col29 ,
		@col30 ,
		@col31 ,
		@col32 ,
		@col33 ,
		@col34 ,
		@col35 ,
		@col36 ,
		@col37 ,
		@col38 ,
		@col39 ,
		@col40 ,
		@col41 ,
		@col42 ,
		@col43 ,
		@col44 ,
		@col45 ,
		@col46 ,
		@col47 ,
		@col48 ,
		@col49 ,
		@col50 ,
		@col51 ,
		@col52 ,
		@col53 ,
		@col54 ,
		@col55 ,
		@col56 ,
		@col57 ,
		@col58 ,
		@col59 ,
		@col60 ,
		@col61 ,
		@col62 ,
		@col63 ,
		@col64 ,
		@col65 ,
		@col66 ,
		@col67 ,
		@col68 ,
		@col69 ,
		@col70 ,
		@col71 ,
		@col72 ,
		@col73 ,
		@col74 ,
		@col75 ,
		@col76 ,
		@col77 ,
		@col78 ,
		@col79 ,
		@col80 ,
		@col81 ,
		@col82 ,
		@col83 ,
		@col84 ,
		@col85 ,
		@col86 ,
		@col87 ,
		@col88 ,
		@col89 ,
		@col90 ,
		@col91 ,
		@col92 ,
		@col93 ,
		@col94 ,
		@col95 ,
		@col96 ,
		@col97 ,
		@col98 ,
		@col99 ,
		@col100 ,
		@col101 ,
		@col102 ,
		@col103 ,
		@col104 ,
		@col105 ,
		@col106 ,
		@col107 ,
		@col108 ,
		@col109 ,
		@col110 ,
		@col111 ,
		@col112 ,
		@col113 ,
		@col114 ,
		@col115 ,
		@col116 ,
		@col117 ,
		@col118 ,
		@col119 ,
		@col120 ,
		@col121 ,
		@col122 ,
		@col123 ,
		@col124 ,
		@col125 ,
		@col126 ,
		@col127 ,
		@col128 ,
		@col129 ,
		@col130 ,
		@col131 ,
		@col132 ,
		@col133 ,
		@col134 ,
		@col135 ,
		@col136 ,
		@col137 ,
		@col138 ,
		@col139 ,
		@col140 ,
		@col141 ,
		@col142 ,
		@col143 ,
		@col144 ,
		@col145 ,
		@col146 ,
		@col147 ,
		@col148 ,
		@col149 ,
		@col150 ,
		@col151 ,
		@col152 ,
		@col153 ,
		@col154 ,
		@col155 ,
		@col156 ,
		@col157 ,
		@col158 ,
		@col159 ,
		@col160 ,
		@col161 ,
		@col162 ,
		@col163 ,
		@col164 ,
		@col165 ,
		@col166 ,
		@col167 ,
		@col168 ,
		@col169 ,
		@col170 ,
		@col171 ,
		@col172 ,
		@col173 ,
		@col174 ,
		@col175 ,
		@col176 ,
		@col177 ,
		@col178 ,
		@col179 ,
		@col180 ,
		@col181 ,
		@col182 ,
		@col183 ,
		@col184 ,
		@col185 ,
		@col186 ,
		@col187 ,
		@col188 ,
		@col189 ,
		@col190 ,
		@col191 ,
		@col192 ,
		@col193 ,
		@col194 ,
		@col195 ,
		@col196 ,
		@col197 ,
		@col198 ,
		@col199 ,
		@col200 ,
		@col201 ,
		@col202 ,
		@col203 ,
		@col204 ,
		@col205 ,
		@col206 ,
		@col207 ,
		@col208 ,
		@col209 ,
		@col210 ,
		@col211 ,
		@col212 ,
		@col213 ,
		@col214 ,
		@col215 ,
		@col216 ,
		@col217 ,
		@col218 ,
		@col219 ,
		@col220 ,
		@col221 ,
		@col222 ,
		@col223 ,
		@col224 ,
		@col225 ,
		@col226 ,
		@col227 ,
		@col228 ,
		@col229 ,
		@col230 ,
		@col231 ,
		@col232 ,
		@col233 ,
		@col234 ,
		@col235 ,
		@col236 ,
		@col237 ,
		@col238 ,
		@col239 ,
		@col240 ,
		@col241 ,
		@col242 ,
		@col243 ,
		@col244 ,
		@col245 ,
		@col246 ,
		@col247 ,
		@col248 ,
		@col249 ,
		@col250 ,
		@col251 ,
		@col252 ,
		@col253 ,
		@col254 ,
		@col255 ,
		@col256 ,
		@col257 ,
		@col258 ,
		@col259 ,
		@col260 ,
		@col261 ,
		@col262 ,
		@col263 ,
		@col264 ,
		@col265 ,
		@col266 ,
		@col267 ,
		@col268 ,
		@col269 ,
		@col270 ,
		@col271 ,
		@col272 ,
		@col273 ,
		@col274 ,
		@col275 ,
		@col276 ,
		@col277 ,
		@col278 ,
		@col279 ,
		@col280 ,
		@col281 ,
		@col282 ,
		@col283 ,
		@col284 ,
		@col285 ,
		@col286 ,
		@col287 ,
		@col288 ,
		@col289 ,
		@col290 ,
		@col291 ,
		@col292 ,
		@col293 ,
		@col294 ,
		@col295 ,
		@col296 ,
		@col297 ,
		@col298 ,
		@col299 ,
		@col300 ,
		@col301 ,
		@col302 ,
		@col303 ,
		@col304 ,
		@col305 ,
		@col306 ,
		@col307 ,
		@col308 ,
		@col309 ,
		@col310 ,
		@col311 ,
		@col312 ,
		@col313 ,
		@col314 ,
		@col315 ,
		@col316 ,
		@col317 ,
		@col318 ,
		@col319 ,
		@col320 ,
		@col321 ,
		@col322 ,
		@col323 ,
		@col324 ,
		@col325 ,
		@col326 ,
		@col327 ,
		@col328 ,
		@col329 ,
		@col330 ,
		@col331 ,
		@col332 ,
		@col333 ,
		@col334 ,
		@col335 ,
		@col336 ,
		@col337 ,
		@col338 ,
		@col339 ,
		@col340 ,
		@col341 ,
		@col342 ,
		@col343 ,
		@col344 ,
		@col345 ,
		@col346 ,
		@col347 ,
		@col348 ,
		@col349 ,
		@col350 ,
		@col351 ,
		@col352 ,
		@col353 ,
		@col354 ,
		@col355 ,
		@col356 ,
		@col357 ,
		@col358 ,
		@col359 ,
		@col360 ,
		@col361 ,
		@col362 ,
		@col363 ,
		@col364 ,
		@col365 ,
		@col366 ,
		@col367 ,
		@col368 ,
		@col369 ,
		@col370 ,
		@col371 ,
		@col372 ,
		@col373 ,
		@col374 ,
		@col375 ,
		@col376 ,
		@col377 ,
		@col378 ,
		@col379 ,
		@col380 ,
		@col381 ,
		@col382 ,
		@col383 ,
		@col384 ,
		@col385 ,
		@col386 ,
		@col387 ,
		@col388 ,
		@col389 ,
		@col390 ,
		@col391 ,
		@col392 ,
		@col393 ,
		@col394 ,
		@col395 ,
		@col396 ,
		@col397 ,
		@col398 ,
		@col399 ,
		@col400 ,
		@col401 ,
		@col402 ,
		@col403 ,
		@col404 ,
		@col405 ,
		@col406 ,
		@col407 ,
		@col408 ,
		@col409 ,
		@col410 ,
		@col411 ,
		@col412 ,
		@col413 ,
		@col414 ,
		@col415 ,
		@col416 ,
		@col417 ,
		@col418 ,
		@col419 ,
		@col420 ,
		@col421 ,
		@col422 ,
		@col423 ,
		@col424 ,
		@col425 ,
		@col426 ,
		@col427 ,
		@col428 ,
		@col429 ,
		@col430 ,
		@col431 ,
		@col432 ,
		@col433 ,
		@col434 ,
		@col435 ,
		@col436 ,
		@col437 ,
		@col438 ,
		@col439 ,
		@col440 ,
		@col441 ,
		@col442 ,
		@col443 ,
		@col444 ,
		@col445 ,
		@col446 ,
		@col447 ,
		@col448 ,
		@col449 ,
		@col450 ,
		@col451 ,
		@col452 ,
		@col453 ,
		@col454 ,
		@col455 ,
		@col456 ,
		@col457 ,
		@col458 ,
		@col459 ,
		@col460 ,
		@col461 ,
		@col462 ,
		@col463 ,
		@col464 ,
		@col465 ,
		@col466 ,
		@col467 ,
		@col468 ,
		@col469 ,
		@col470 ,
		@col471 ,
		@col472 ,
		@col473 ,
		@col474 ,
		@col475 ,
		@col476 ,
		@col477 ,
		@col478 ,
		@col479 ,
		@col480 ,
		@col481 ,
		@col482 ,
		@col483 ,
		@col484 ,
		@col485 ,
		@col486 ,
		@col487 ,
		@col488 ,
		@col489 ,
		@col490 ,
		@col491 ,
		@col492 ,
		@col493 ,
		@col494 ,
		@col495 ,
		@col496 ,
		@col497 ,
		@col498 ,
		@col499 ,
		@col500 ,
		@col501 ,
		@col502 ,
		@col503 ,
		@col504 ,
		@col505 ,
		@col506 ,
		@col507 ,
		@col508 ,
		@col509 ,
		@col510 ,
		@col511 ,
		@col512 ,
		@col513 ,
		@col514 ,
		@col515 ,
		@col516 ,
		@col517 ,
		@col518 ,
		@col519 ,
		@col520 ,
		@col521 ,
		@col522 ,
		@col523 ,
		@col524 ,
		@col525 ,
		@col526 ,
		@col527 ,
		@col528 ,
		@col529 ,
		@col530 ,
		@col531 ,
		@col532 ,
		@col533 ,
		@col534 ,
		@col535 ,
		@col536 ,
		@col537 ,
		@col538 ,
		@col539 ,
		@col540 ,
		@col541 ,
		@col542 ,
		@col543 ,
		@col544 ,
		@col545 ,
		@col546 ,
		@col547 ,
		@col548 ,
		@col549 ,
		@col550 ,
		@col551 ,
		@col552 ,
		@col553 ,
		@col554 ,
		@col555 ,
		@col556 ,
		@col557 ,
		@col558 ,
		@col559 ,
		@col560 ,
		@col561 ,
		@col562 ,
		@col563 ,
		@col564 ,
		@col565 ,
		@col566 ,
		@col567 ,
		@col568 ,
		@col569 ,
		@col570 ,
		@col571 ,
		@col572 ,
		@col573 ,
		@col574 ,
		@col575 ,
		@col576 ,
		@col577 ,
		@col578 ,
		@col579 ,
		@col580 ,
		@col581 ,
		@col582 ,
		@col583 ,
		@col584 ,
		@col585 ,
		@col586 ,
		@col587 ,
		@col588 ,
		@col589 ,
		@col590 ,
		@col591 ,
		@col592 ,
		@col593 ,
		@col594 ,
		@col595 ,
		@col596 ,
		@col597 ,
		@col598 ,
		@col599 ,
		@col600 ,
		@col601 ,
		@col602 ,
		@col603 ,
		@col604 ,
		@col605 ,
		@col606 ,
		@col607 ,
		@col608 ,
		@col609 ,
		@col610 ,
		@col611 ,
		@col612 ,
		@col613 ,
		@col614 ,
		@col615 ,
		@col616 ,
		@col617 ,
		@col618 ,
		@col619 ,
		@col620 ,
		@col621 ,
		@col622 ,
		@col623 ,
		@col624 ,
		@col625 ,
		@col626 ,
		@col627 ,
		@col628 ,
		@col629 ,
		@col630 ,
		@col631 ,
		@col632 ,
		@col633 ,
		@col634 ,
		@col635 ,
		@col636 ,
		@col637 ,
		@col638 ,
		@col639 ,
		@col640 ,
		@col641 ,
		@col642 ,
		@col643 ,
		@col644 ,
		@col645 ,
		@col646 ,
		@col647 ,
		@col648 ,
		@col649 ,
		@col650 ,
		@col651 ,
		@col652 ,
		@col653 ,
		@col654 ,
		@col655 ,
		@col656 ,
		@col657 ,
		@col658 ,
		@col659 ,
		@col660 ,
		@col661 ,
		@col662 ,
		@col663 
END
GO



--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,033
           ,'update_spectrum_db_033.sql'
           ,'1.1.02')
GO