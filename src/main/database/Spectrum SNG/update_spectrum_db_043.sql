SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 043
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- Create table EventChannelValue
--------------------------------------------
RAISERROR ('--Create table EventChannelValue', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EventChannelValue')
    DROP TABLE [dbo].EventChannelValue
GO

CREATE TABLE [dbo].[EventChannelValue](
	[ID] [bigint] IDENTITY NOT NULL,
	[UnitID] [int] NOT NULL,
	[RecordInsert] [datetime2](3) NOT NULL,
	[TimeStamp] [datetime2](3) NOT NULL,
	[TimeStampEndTime] [datetime2](3) NULL,
	[ChannelID] [int] NOT NULL,
	[Value] bit NOT NULL
	CONSTRAINT [PK_EventChannelValue] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[EventChannelValue] ADD  CONSTRAINT [DF_EventChannelValue_RecordInsert]  DEFAULT (sysdatetime()) FOR [RecordInsert]
GO

ALTER TABLE [dbo].[EventChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_Unit_EventChannelValue] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Unit] ([ID])
GO

ALTER TABLE [dbo].[EventChannelValue] CHECK CONSTRAINT [FK_Unit_EventChannelValue]
GO

ALTER TABLE [dbo].[EventChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_Channel_EventChannelValue] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO

ALTER TABLE [dbo].[EventChannelValue] CHECK CONSTRAINT [FK_Channel_EventChannelValue]
GO

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,043
           ,'update_spectrum_db_043.sql'
           ,'1.1.01')
GO