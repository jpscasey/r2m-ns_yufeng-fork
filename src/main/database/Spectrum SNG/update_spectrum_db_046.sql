SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 046
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('-- review clustered indexes on the whole database', 0, 1) WITH NOWAIT
GO


ALTER TABLE [dbo].[TrainPassageSectionDelayReason] DROP CONSTRAINT [FK_DelayReason_TrainPassageSectionDelayReason];
ALTER TABLE [dbo].[DelayReason] DROP CONSTRAINT [PK_DelayReason]
ALTER TABLE  [dbo].[DelayReason] ADD  CONSTRAINT [PK_DelayReason] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[TrainPassageSectionDelayReason] ADD CONSTRAINT [FK_DelayReason_TrainPassageSectionDelayReason] FOREIGN KEY ([DelayReasonID]) REFERENCES [dbo].[DelayReason]([ID]);

ALTER TABLE [dbo].[RecoveryAuditItem] DROP CONSTRAINT [FK_Fault_RecoveryAuditItem];
ALTER TABLE [dbo].[TmsFault] DROP CONSTRAINT [FK_Fault_TmsFault];
ALTER TABLE [dbo].[FaultChannelValue] DROP CONSTRAINT [FK_FaultChannelValue_Fault];
ALTER TABLE [dbo].[FaultEventChannelValue] DROP CONSTRAINT [FK_FaultEventChannelValue_Fault];
ALTER TABLE [dbo].[FaultComment] DROP CONSTRAINT [FK_Fault_FaultComment];
ALTER TABLE [dbo].[Fault] DROP CONSTRAINT [PK_Fault]
ALTER TABLE  [dbo].[Fault] ADD  CONSTRAINT [PK_Fault] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[RecoveryAuditItem] ADD CONSTRAINT [FK_Fault_RecoveryAuditItem] FOREIGN KEY ([Fault_ID]) REFERENCES [dbo].[Fault]([ID]);
ALTER TABLE [dbo].[TmsFault] ADD CONSTRAINT [FK_Fault_TmsFault] FOREIGN KEY ([FaultID]) REFERENCES [dbo].[Fault]([ID]);
ALTER TABLE [dbo].[FaultChannelValue] ADD CONSTRAINT [FK_FaultChannelValue_Fault] FOREIGN KEY ([FaultID]) REFERENCES [dbo].[Fault]([ID]);
ALTER TABLE [dbo].[FaultEventChannelValue] ADD CONSTRAINT [FK_FaultEventChannelValue_Fault] FOREIGN KEY ([FaultID]) REFERENCES [dbo].[Fault]([ID]);
ALTER TABLE [dbo].[FaultComment] ADD CONSTRAINT [FK_Fault_FaultComment] FOREIGN KEY ([FaultID]) REFERENCES [dbo].[Fault]([ID]);
 
ALTER TABLE [dbo].[FleetStatus] DROP CONSTRAINT [PK_FleetStatus]
ALTER TABLE  [dbo].[FleetStatus] ADD  CONSTRAINT [PK_FleetStatus] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[FleetStatusHistory] DROP CONSTRAINT [PK_FleetStatusHistory]
ALTER TABLE  [dbo].[FleetStatusHistory] ADD  CONSTRAINT [PK_FleetStatusHistory] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[HardwareChannel] DROP CONSTRAINT [PK_HardwareChannel]
ALTER TABLE  [dbo].[HardwareChannel] ADD  CONSTRAINT [PK_HardwareChannel] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 

ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_HardwareType_Vehicle];
ALTER TABLE [dbo].[HardwareType] DROP CONSTRAINT [PK_HardwareType]
ALTER TABLE  [dbo].[HardwareType] ADD  CONSTRAINT [PK_HardwareType] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[Vehicle] ADD CONSTRAINT [FK_HardwareType_Vehicle] FOREIGN KEY ([HardwareTypeID]) REFERENCES [dbo].[HardwareType]([ID]);

ALTER TABLE [dbo].[IdealRunChannelValue] DROP CONSTRAINT [FK_IdealRun_IdealRunChannelValue];
ALTER TABLE [dbo].[TrainPassageSection] DROP CONSTRAINT [FK_IdealRun_TrainPassageSection];
ALTER TABLE [dbo].[IdealRun] DROP CONSTRAINT [PK_IdealRun]
ALTER TABLE  [dbo].[IdealRun] ADD  CONSTRAINT [PK_IdealRun] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[IdealRunChannelValue] ADD CONSTRAINT [FK_IdealRun_IdealRunChannelValue] FOREIGN KEY ([IdealRunID]) REFERENCES [dbo].[IdealRun]([ID]);
ALTER TABLE [dbo].[TrainPassageSection] ADD CONSTRAINT [FK_IdealRun_TrainPassageSection] FOREIGN KEY ([IdealRunID]) REFERENCES [dbo].[IdealRun]([ID]);
 
ALTER TABLE [dbo].[IdealRunChannelValue] DROP CONSTRAINT [PK_IdealRunChannelValue]
ALTER TABLE  [dbo].[IdealRunChannelValue] ADD  CONSTRAINT [PK_IdealRunChannelValue] PRIMARY KEY CLUSTERED  ([IdealRunID],[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 

ALTER TABLE [dbo].[JourneyDate] DROP CONSTRAINT [FK_JourneyDate_Headcode];
ALTER TABLE [dbo].[JourneyDate] DROP CONSTRAINT [FK_JourneyDate_Journey];
ALTER TABLE [dbo].[SectionPoint] DROP CONSTRAINT [FK_Journey_SectionPoint_JourneyID];
ALTER TABLE [dbo].[Segment] DROP CONSTRAINT [FK_Journey_Segment];
ALTER TABLE [dbo].[TrainPassage] DROP CONSTRAINT [FK_Journey_TrainPassage];
ALTER TABLE [dbo].[TrainPassageSegment] DROP CONSTRAINT [FK_Journey_TrainPassageSegment];
ALTER TABLE [dbo].[Journey] DROP CONSTRAINT [PK_Journey]
ALTER TABLE  [dbo].[Journey] ADD  CONSTRAINT [PK_Journey] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[JourneyDate] ADD CONSTRAINT [FK_JourneyDate_Headcode] FOREIGN KEY ([PermanentJourneyID]) REFERENCES [dbo].[Journey]([ID]);
ALTER TABLE [dbo].[JourneyDate] ADD CONSTRAINT [FK_JourneyDate_Journey] FOREIGN KEY ([ActualJourneyID]) REFERENCES [dbo].[Journey]([ID]);
ALTER TABLE [dbo].[SectionPoint] ADD CONSTRAINT [FK_Journey_SectionPoint_JourneyID] FOREIGN KEY ([JourneyID]) REFERENCES [dbo].[Journey]([ID]);
ALTER TABLE [dbo].[Segment] ADD CONSTRAINT [FK_Journey_Segment] FOREIGN KEY ([JourneyID]) REFERENCES [dbo].[Journey]([ID]);
ALTER TABLE [dbo].[TrainPassage] ADD CONSTRAINT [FK_Journey_TrainPassage] FOREIGN KEY ([JourneyID]) REFERENCES [dbo].[Journey]([ID]);
ALTER TABLE [dbo].[TrainPassageSegment] ADD CONSTRAINT [FK_Journey_TrainPassageSegment] FOREIGN KEY ([JourneyID]) REFERENCES [dbo].[Journey]([ID]);
 
ALTER TABLE [dbo].[LoadCifFileFormat] DROP CONSTRAINT [PK_LoadCifFileFormat]
ALTER TABLE  [dbo].[LoadCifFileFormat] ADD  CONSTRAINT [PK_LoadCifFileFormat] PRIMARY KEY CLUSTERED  ([FieldType],[FieldOrder]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[LoadGeniusData] DROP CONSTRAINT [PK_LoadGeniusData]
ALTER TABLE  [dbo].[LoadGeniusData] ADD  CONSTRAINT [PK_LoadGeniusData] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 

ALTER TABLE [dbo].[FleetStatusStaging] DROP CONSTRAINT [FK_End_Location_FleetStatusStaging];
ALTER TABLE [dbo].[FleetStatusStaging] DROP CONSTRAINT [FK_Location_FleetStatusStaging_TrainEnd];
ALTER TABLE [dbo].[FleetStatusStaging] DROP CONSTRAINT [FK_Location_FleetStatusStaging_TrainStart];
ALTER TABLE [dbo].[FleetStatusStaging] DROP CONSTRAINT [FK_Start_Location_FleetStatusStaging];
ALTER TABLE [dbo].[Journey] DROP CONSTRAINT [FK_Location_Journey_EndLocationID];
ALTER TABLE [dbo].[Journey] DROP CONSTRAINT [FK_Location_Journey_StartLocationID];
ALTER TABLE [dbo].[LocationArea] DROP CONSTRAINT [FK_Location_LocationArea];
ALTER TABLE [dbo].[LocationSection] DROP CONSTRAINT [FK_Location_LocationSection_EndLocationID];
ALTER TABLE [dbo].[LocationSection] DROP CONSTRAINT [FK_Location_LocationSection_StartLocationID];
ALTER TABLE [dbo].[Section] DROP CONSTRAINT [FK_Location_Section_EndLocationID];
ALTER TABLE [dbo].[Section] DROP CONSTRAINT [FK_Location_Section_StartLocationID];
ALTER TABLE [dbo].[SectionPoint] DROP CONSTRAINT [FK_Location_SectionPoint_LocationID];
ALTER TABLE [dbo].[Segment] DROP CONSTRAINT [FK_Location_Segment_EndLocationID];
ALTER TABLE [dbo].[Segment] DROP CONSTRAINT [FK_Location_Segment_StartLocationID];
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [PK_Location]
ALTER TABLE  [dbo].[Location] ADD  CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

IF EXISTS (SELECT 1 FROM SYS.indexes where name ='IX_Location_TEST')
	DROP INDEX [IX_Location_TEST] ON [dbo].[Location]
GO


ALTER TABLE [dbo].[FleetStatusStaging] ADD CONSTRAINT [FK_End_Location_FleetStatusStaging] FOREIGN KEY ([EndLocationId]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[FleetStatusStaging] ADD CONSTRAINT [FK_Location_FleetStatusStaging_TrainEnd] FOREIGN KEY ([TrainEndLocationId]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[FleetStatusStaging] ADD CONSTRAINT [FK_Location_FleetStatusStaging_TrainStart] FOREIGN KEY ([TrainStartLocationId]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[FleetStatusStaging] ADD CONSTRAINT [FK_Start_Location_FleetStatusStaging] FOREIGN KEY ([StartLocationId]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Journey] ADD CONSTRAINT [FK_Location_Journey_EndLocationID] FOREIGN KEY ([EndLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Journey] ADD CONSTRAINT [FK_Location_Journey_StartLocationID] FOREIGN KEY ([StartLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[LocationArea] ADD CONSTRAINT [FK_Location_LocationArea] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[LocationSection] ADD CONSTRAINT [FK_Location_LocationSection_EndLocationID] FOREIGN KEY ([EndLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[LocationSection] ADD CONSTRAINT [FK_Location_LocationSection_StartLocationID] FOREIGN KEY ([StartLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Section] ADD CONSTRAINT [FK_Location_Section_EndLocationID] FOREIGN KEY ([EndLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Section] ADD CONSTRAINT [FK_Location_Section_StartLocationID] FOREIGN KEY ([StartLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[SectionPoint] ADD CONSTRAINT [FK_Location_SectionPoint_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Segment] ADD CONSTRAINT [FK_Location_Segment_EndLocationID] FOREIGN KEY ([EndLocationID]) REFERENCES [dbo].[Location]([ID]);
ALTER TABLE [dbo].[Segment] ADD CONSTRAINT [FK_Location_Segment_StartLocationID] FOREIGN KEY ([StartLocationID]) REFERENCES [dbo].[Location]([ID]);
 
ALTER TABLE [dbo].[LocationArea] DROP CONSTRAINT [PK_LocationArea]
ALTER TABLE  [dbo].[LocationArea] ADD  CONSTRAINT [PK_LocationArea] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY] 
 
ALTER TABLE [dbo].[LocationSection] DROP CONSTRAINT [PK_LocationSection]
ALTER TABLE  [dbo].[LocationSection] ADD  CONSTRAINT [PK_LocationSection] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[ScheduledAnalysisRule] DROP CONSTRAINT [PK_ScheduledAnalysisRule]
ALTER TABLE  [dbo].[ScheduledAnalysisRule] ADD  CONSTRAINT [PK_ScheduledAnalysisRule] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 

ALTER TABLE [dbo].[ScheduledAnalysis] DROP CONSTRAINT [FK_ScheduledAnalysisStatus_ScheduledAnalysis];
ALTER TABLE [dbo].[ScheduledAnalysisStatus] DROP CONSTRAINT [PK_ScheduledAnalysisStatus]
ALTER TABLE  [dbo].[ScheduledAnalysisStatus] ADD  CONSTRAINT [PK_ScheduledAnalysisStatus] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[ScheduledAnalysis] ADD CONSTRAINT [FK_ScheduledAnalysisStatus_ScheduledAnalysis] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[ScheduledAnalysisStatus]([ID]);

ALTER TABLE [dbo].[IdealRun] DROP CONSTRAINT [FK_Section_IdealRun];
ALTER TABLE [dbo].[TrainPassageSection] DROP CONSTRAINT [FK_Section_TrainPassageSection];
ALTER TABLE [dbo].[Section] DROP CONSTRAINT [PK_Section]
ALTER TABLE  [dbo].[Section] ADD  CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED  ([JourneyID],[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[IdealRun] ADD CONSTRAINT [FK_Section_IdealRun] FOREIGN KEY ([JourneyID],[SectionID]) REFERENCES [dbo].[Section]([JourneyID],[ID]);
ALTER TABLE [dbo].[TrainPassageSection] ADD CONSTRAINT [FK_Section_TrainPassageSection] FOREIGN KEY ([JourneyID],[SectionID]) REFERENCES [dbo].[Section]([JourneyID],[ID]);

ALTER TABLE [dbo].[TrainPassageSectionPoint] DROP CONSTRAINT [FK_SectionPoint_TrainPassageSectionPoint];
ALTER TABLE [dbo].[SectionPoint] DROP CONSTRAINT [PK_SectionPoint]
ALTER TABLE  [dbo].[SectionPoint] ADD  CONSTRAINT [PK_SectionPoint] PRIMARY KEY CLUSTERED  ([JourneyID],[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[TrainPassageSectionPoint] ADD CONSTRAINT [FK_SectionPoint_TrainPassageSectionPoint] FOREIGN KEY ([JourneyID],[SectionPointID]) REFERENCES [dbo].[SectionPoint]([JourneyID],[ID]);

ALTER TABLE [dbo].[Section] DROP CONSTRAINT [FK_Segment_Section];
ALTER TABLE [dbo].[TrainPassageSegment] DROP CONSTRAINT [FK_Section_TrainPassageSegment];
ALTER TABLE [dbo].[Segment] DROP CONSTRAINT [PK_Segment]
ALTER TABLE  [dbo].[Segment] ADD  CONSTRAINT [PK_Segment] PRIMARY KEY CLUSTERED  ([JourneyID],[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[Section] ADD CONSTRAINT [FK_Segment_Section] FOREIGN KEY ([JourneyID],[SegmentID]) REFERENCES [dbo].[Segment]([JourneyID],[ID]);
ALTER TABLE [dbo].[TrainPassageSegment] ADD CONSTRAINT [FK_Section_TrainPassageSegment] FOREIGN KEY ([JourneyID],[SegmentID]) REFERENCES [dbo].[Segment]([JourneyID],[ID]);

ALTER TABLE [dbo].[TrainPassageSection] DROP CONSTRAINT [FK_TrainPassage_TrainPassageSection];
ALTER TABLE [dbo].[TrainPassageSectionPoint] DROP CONSTRAINT [FK_TrainPassage_TrainPassageSectionPoint];
ALTER TABLE [dbo].[TrainPassageSegment] DROP CONSTRAINT [FK_TrainPassage_TrainPassageSegment];
ALTER TABLE [dbo].[TrainPassage] DROP CONSTRAINT [PK_TrainPassage]
ALTER TABLE  [dbo].[TrainPassage] ADD  CONSTRAINT [PK_TrainPassage] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[TrainPassageSection] ADD CONSTRAINT [FK_TrainPassage_TrainPassageSection] FOREIGN KEY ([TrainPassageID]) REFERENCES [dbo].[TrainPassage]([ID]);
ALTER TABLE [dbo].[TrainPassageSectionPoint] ADD CONSTRAINT [FK_TrainPassage_TrainPassageSectionPoint] FOREIGN KEY ([TrainPassageID]) REFERENCES [dbo].[TrainPassage]([ID]);
ALTER TABLE [dbo].[TrainPassageSegment] ADD CONSTRAINT [FK_TrainPassage_TrainPassageSegment] FOREIGN KEY ([TrainPassageID]) REFERENCES [dbo].[TrainPassage]([ID]);

ALTER TABLE [dbo].[TrainPassageSectionDelayReason] DROP CONSTRAINT [FK_TrainPassage_TrainPassageSectionDelayReason];
ALTER TABLE [dbo].[TrainPassageSection] DROP CONSTRAINT [PK_TrainPassageSection]
ALTER TABLE  [dbo].[TrainPassageSection] ADD  CONSTRAINT [PK_TrainPassageSection] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE [dbo].[TrainPassageSectionDelayReason] ADD CONSTRAINT [FK_TrainPassage_TrainPassageSectionDelayReason] FOREIGN KEY ([TrainPassageSectionID]) REFERENCES [dbo].[TrainPassageSection]([ID]);
 
ALTER TABLE [dbo].[TrainPassageSectionDelayReason] DROP CONSTRAINT [PK_TrainPassageSectionDelayReason]
ALTER TABLE  [dbo].[TrainPassageSectionDelayReason] ADD  CONSTRAINT [PK_TrainPassageSectionDelayReason] PRIMARY KEY CLUSTERED  ([TrainPassageSectionID],[DelayReasonID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[TrainPassageSectionPoint] DROP CONSTRAINT [PK_TrainPassageSectionPoint]
ALTER TABLE  [dbo].[TrainPassageSectionPoint] ADD  CONSTRAINT [PK_TrainPassageSectionPoint] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[TrainPassageSegment] DROP CONSTRAINT [PK_TrainPassageSegment]
ALTER TABLE  [dbo].[TrainPassageSegment] ADD  CONSTRAINT [PK_TrainPassageSegment] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
 
ALTER TABLE [dbo].[VehicleEvent] DROP CONSTRAINT [PK_VehicleEvent]
ALTER TABLE  [dbo].[VehicleEvent] ADD  CONSTRAINT [PK_VehicleEvent] PRIMARY KEY CLUSTERED  ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
 
alter index PK_Location on dbo.Location REBUILD WITH (SORT_IN_TEMPDB = ON,FILLFACTOR = 100)
alter index PK_LocationArea on LocationArea REBUILD WITH (SORT_IN_TEMPDB = ON,FILLFACTOR = 100)
alter index PK_ChannelRule on ChannelRule REBUILD WITH (SORT_IN_TEMPDB = ON,FILLFACTOR = 100)
alter index PK_FleetFormation on FleetFormation REBUILD WITH (SORT_IN_TEMPDB = ON,FILLFACTOR = 100)

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME='Journey_TMP')
	drop table Journey_TMP
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME='JourneyDetail_TMP')
	drop table JourneyDetail_TMP

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME='Section_TMP')
	drop table Section_TMP
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME='SectionPoint_TMP')
	drop table SectionPoint_TMP
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME='Segment_TMP')
	drop table Segment_TMP

GO



GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,046
           ,'update_spectrum_db_046.sql'
           ,'1.1.03')
GO