SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 047
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- update NSP_GetFaultEventChannelValueLatest', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueLatest] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatest]
    @DatetimeStart DATETIME
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH EventChannelValueLatest
    AS (
        SELECT MAX(ecv.TimeStamp) AS TimeStamp
            ,ecv.UnitID
            ,ecv.ChannelID
        FROM EventChannelValue ecv
        WHERE ecv.TimeStamp <= @DatetimeStart
            AND ecv.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND ecv.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY ecv.UnitID
            ,ecv.ChannelID
        )
    SELECT ecv.ID
    ,ecv.UnitID
    ,ecv.ChannelID
    ,ecv.TimeStamp
    ,ecv.Value
    FROM EventChannelValueLatest ecvl
    JOIN EventChannelValue ecv
        ON ecvl.UnitID = ecv.UnitID
        AND ecvl.ChannelID = ecv.ChannelID
        AND ecvl.TimeStamp = ecv.TimeStamp
        
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,047
           ,'update_spectrum_db_047.sql'
           ,'1.1.02')
GO