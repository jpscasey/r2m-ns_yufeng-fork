SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 050
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- Create table EventChannelLatestValue
--------------------------------------------
RAISERROR ('--Create table EventChannelLatestValue', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EventChannelLatestValue')
    DROP TABLE [dbo].EventChannelLatestValue
GO

CREATE TABLE [dbo].[EventChannelLatestValue](
	[UnitID] [int] NOT NULL,
	[ChannelID] [int] NOT NULL,
	[TimeLastUpdated] [datetime2](3) NOT NULL,
	[Value]  [bit] NOT NULL

	CONSTRAINT [PK_EventChannelLatestValue] PRIMARY KEY CLUSTERED 
	(
		[UnitID] ASC, [ChannelID]
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].EventChannelLatestValue  WITH CHECK ADD  CONSTRAINT [FK_EventChannelLatestValue_Unit] FOREIGN KEY(UnitID)
REFERENCES [dbo].Unit ([ID])
GO

ALTER TABLE [dbo].EventChannelLatestValue  WITH CHECK ADD  CONSTRAINT [FK_EventChannelLatestValue_Channel] FOREIGN KEY(ChannelID)
REFERENCES [dbo].Channel ([ID])
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,050
           ,'update_spectrum_db_050.sql'
           ,'1.1.01')
GO