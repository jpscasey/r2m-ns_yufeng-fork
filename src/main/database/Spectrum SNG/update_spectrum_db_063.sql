SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 063
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('--Create table FaultStatusHistory', 0, 1) WITH NOWAIT
GO

IF Exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='FaultStatusHistory')
	DROP TABLE FaultStatusHistory

CREATE TABLE FaultStatusHistory(
FaultID int,
timestamp datetime2(3),
UserName varchar(255),
IsCurrent bit
CONSTRAINT [PK_CL_FaultStatusHistory] PRIMARY KEY CLUSTERED 
(
	[FaultID] ASC,
	[Timestamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[FaultStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_Fault_FaultStatusHistory] FOREIGN KEY([FaultID])
REFERENCES [dbo].[Fault] ([ID])
GO

ALTER TABLE [dbo].[FaultStatusHistory] CHECK CONSTRAINT [FK_Fault_FaultStatusHistory]
GO

RAISERROR ('-- Updating NSP_FaultIsCurrentUpdate', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaultIsCurrentUpdate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_FaultIsCurrentUpdate AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE NSP_FaultIsCurrentUpdate
(
@FaultID int
,@IsCurrent bit
,@Username varchar(255)
)
AS
BEGIN 

UPDATE Fault set IsCurrent = @IsCurrent where ID = @FaultID

INSERT INTO FaultStatusHistory(FaultID,UserName,IsCurrent,timestamp)
VALUES(@FaultID,@Username,@IsCurrent,SYSDATETIME())

END

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,063
           ,'update_spectrum_db_063.sql'
           ,'1.2.02')
GO