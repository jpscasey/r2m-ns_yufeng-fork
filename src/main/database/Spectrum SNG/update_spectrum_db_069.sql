SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 069
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

ALTER TABLE FAULT ADD RowVersion rowversion

GO

ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 	


GO

/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 11/01/2017 13:33:46 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 11/01/2017 13:33:54 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 11/01/2017 13:34:03 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER VIEW [dbo].[VW_IX_FaultLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
	FROM dbo.Fault f
	INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
	INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
	INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
	INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
	INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
	WHERE IsCurrent = 1

GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultLive_ID] ON [dbo].[VW_IX_FaultLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultLive_CreateTime] ON [dbo].[VW_IX_FaultLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER VIEW [dbo].[VW_IX_FaultNotLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
		WHERE IsCurrent = 0


GO

/******************************************************************************
**	Name:			VW_FaultHistory
**	Description:	View returns list of faults for Fault History screen
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

ALTER VIEW [dbo].[VW_FaultHistory]
AS

	SELECT 
		f.ID
		, f.FaultMetaID
		, fv.UnitNumber			AS UnitNumber
		, f.CreateTime			AS CreateTime
		, f.IsCurrent			AS IsCurrent
		, f.RecoveryID			AS RecoveryID
		, fv.VehicleNumber		AS FaultVehicle
		, f.EndTime				AS EndTime
		, f.Headcode			AS HeadCode
		, l.Tiploc				AS LocationCode
		, f.Latitude			AS Latitude
		, f.Longitude			AS Longitude
		, f.FaultCode			AS FaultCode
		, f.FaultType
		, f.FaultTypeColor
		, f.Description			AS FaultDescription
		, f.Summary				AS FaultSummary
		, f.Category			AS FaultCategory
		, fv.FleetID			AS FleetID
		, fv.FleetCode			AS FleetCode
		, f.AdditionalInfo		AS AdditionalInfo
		, f.HasRecovery			AS HasRecovery
		, f.RowVersion          AS RowVersion

	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN VW_Vehicle fv ON fv.UnitID = f.FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID

GO

/****** Object:  Index [IX_CL_U_VW_IX_FaultNotLive_ID]    Script Date: 11/01/2017 13:41:08 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultNotLive_ID] ON [dbo].[VW_IX_FaultNotLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_FaultNotLive_CreateTime]    Script Date: 11/01/2017 13:41:17 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultNotLive_CreateTime] ON [dbo].[VW_IX_FaultNotLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER VIEW [dbo].[VW_FaultLive]
AS
    SELECT 
            ID    
          , CreateTime 
          , HeadCode 
          , SetCode 
          , FaultCode 
          , LocationID 
          , IsCurrent 
          , EndTime 
          , RecoveryID 
          , Latitude 
          , Longitude 
          , FaultMetaID 
          , FaultUnitID 
          , FaultUnitNumber 
          , PrimaryUnitID 
          , SecondaryUnitID 
          , TertiaryUnitID 
          , IsDelayed 
          , Description 
          , Summary 
          , CategoryID 
          , Category 
          , ReportingOnly 
          , FaultTypeID 
          , FaultType 
          , FaultTypeColor 
          , IsAcknowledged 
          , AcknowledgedTime 
          , Priority 
          , RecoveryStatus
		  , FleetCode 
		  , AdditionalInfo
		  , HasRecovery
		  , RowVersion
    FROM dbo.VW_IX_FaultLive (NOEXPAND)     

GO

ALTER VIEW [dbo].[VW_FaultNotLive]
AS
    SELECT 
    ID
      , CreateTime 
      , HeadCode 
      , SetCode 
      , FaultCode 
      , LocationID 
      , IsCurrent 
      , EndTime 
      , RecoveryID 
      , Latitude 
      , Longitude 
      , FaultMetaID 
      , FaultUnitID 
      , FaultUnitNumber 
      , PrimaryUnitID 
      , SecondaryUnitID 
      , TertiaryUnitID 
      , IsDelayed 
      , Description 
      , Summary 
      , CategoryID 
      , Category 
      , ReportingOnly 
      , FaultTypeID 
      , FaultType 
      , FaultTypeColor 
      , IsAcknowledged 
      , AcknowledgedTime 
      , Priority 
      , RecoveryStatus
	  , FleetCode 
	  , HasRecovery
	  , RowVersion
        FROM dbo.VW_IX_FaultNotLive (NOEXPAND)


GO

------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , fm.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , RowVersion
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ID = xref.ID
        WHERE f.ID = @FaultID

END
GO


IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaultIsCurrentUpdate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_FaultIsCurrentUpdate AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE NSP_FaultIsCurrentUpdate
(
@FaultID int
,@IsCurrent bit
,@Username varchar(255)
,@Version varbinary(8)
)
AS
BEGIN 

DECLARE @rowcount int
	
UPDATE Fault set IsCurrent = @IsCurrent where ID = @FaultID AND RowVersion = @Version

SELECT
   @rowcount = @@ROWCOUNT
   
IF @rowcount = 1
INSERT INTO FaultStatusHistory(FaultID,UserName,IsCurrent,timestamp) VALUES(@FaultID,@Username,@IsCurrent,SYSDATETIME())


SELECT @rowcount
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,069
           ,'update_spectrum_db_069.sql'
           ,'1.2.03')
GO