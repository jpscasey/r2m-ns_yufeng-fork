SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 071
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- update NSP_GetAllFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetAllFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetAllFaultLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetAllFaultLive]
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , null as LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , null AS MaximoId
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    ORDER BY CreateTime DESC
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,071
           ,'update_spectrum_db_071.sql'
           ,'1.2.03')
GO