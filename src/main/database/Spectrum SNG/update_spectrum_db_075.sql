SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 075
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_HistoricChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelData AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelData]
	@DateTimeEnd datetime
	, @Interval int -- number of seconds
	, @UnitID int
	, @SelectColList varchar(4000)
	, @N int = NULL --record number
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql varchar(max)

	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)
	
	SET @sql = '
SELECT TOP (' + CAST(ISNULL(@N, 30000) AS varchar(10)) +  ')
	TimeStamp = CAST(cv.Timestamp AS datetime)
	, cv.UnitID
	, ' + @SelectColList + '
FROM dbo.ChannelValue cv
WHERE 
	cv.UnitID = ' + CAST(@UnitID AS varchar(10)) + '
	AND cv.Timestamp BETWEEN ''' + CONVERT(varchar(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(varchar(23), @DateTimeEnd, 121)  + '''
ORDER BY 1 ASC'

	EXEC (@sql)
	
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,075
           ,'update_spectrum_db_075.sql'
           ,'1.2.03')
GO