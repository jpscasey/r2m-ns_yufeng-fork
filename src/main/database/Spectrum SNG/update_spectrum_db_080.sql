SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 080
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
GO



ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, f.FleetCode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, Description
		, Summary
		, Category
		, CategoryID
		, CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = xref.ExternalCode
		, IsGroupLead
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
		LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
	WHERE 
		f.FaultGroupID = @FaultGroupID

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,080
           ,'update_spectrum_db_080.sql'
           ,'1.3.01')
GO