SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 085
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- update NSP_GetCurrentFaults', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFaults' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetCurrentFaults AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaults]
    @N int
    , @isLive         bit          = NULL   --Live
    , @isAcknowledged bit          = NULL   --Acknowledged
    , @FleetCodes     varchar(100) = NULL   --Fleet
    , @FaultTypeIDs   varchar(100) = NULL   --Severity
    , @CategoryIDs    varchar(100) = NULL   --Category
    , @HasRecoveryOnly    bit      = NULL   --IAS
    , @UnitTypes   	  varchar(100) = NULL   --UnitType
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @FleetTable TABLE (ID varchar(100))
    INSERT INTO @FleetTable
    SELECT * from dbo.SplitStrings_CTE(@FleetCodes,',')

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

    DECLARE @CategoryTable TABLE (ID int)
    INSERT INTO @CategoryTable
    SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')

    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitTypes,',')

    IF @FleetCodes   = '' SET @FleetCodes   = NULL
    IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
    IF @CategoryIDs  = '' SET @CategoryIDs  = NULL
    IF @UnitTypes    = '' SET @UnitTypes    = NULL

    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , IsAcknowledged
        , MaximoID = xref.ExternalCode
    FROM 
        dbo.VW_IX_Fault f
        LEFT JOIN dbo.Location ON f.LocationID = Location.ID
        LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
        LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
        LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    WHERE 
            (f.IsCurrent      = @isLive                             OR @isLive         IS NULL)
        AND (f.IsAcknowledged = @isAcknowledged                     OR @isAcknowledged IS NULL)
        AND (f.FaultTypeID    IN (SELECT ID FROM @TypeTable)        OR @FaultTypeIDs   IS NULL)
        AND (f.CategoryID     IN (SELECT ID FROM @CategoryTable)    OR @CategoryIDs    IS NULL)
        AND (f.FleetCode      IN (SELECT ID FROM @FleetTable)       OR @FleetCodes     IS NULL)
        AND (f.HasRecovery    = @HasRecoveryOnly                    OR @HasRecoveryOnly    IS NULL
            OR @HasRecoveryOnly = 0)
        AND (u.UnitType    IN (SELECT ID FROM @UnitTypeTable)    OR @UnitTypes     IS NULL)
        AND ReportingOnly = 0
    ORDER BY 
        CreateTime DESC

END
GO

--------------------------------------------------------------------------------------

RAISERROR ('-- update NSP_GetCurrentFaultsCount', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFaultsCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetCurrentFaultsCount AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaultsCount]
    @isLive         bit          = NULL   --Live
    , @isAcknowledged bit          = NULL   --Acknowledged
    , @FleetCodes     varchar(100) = NULL   --Fleet
    , @FaultTypeIDs   varchar(100) = NULL   --Severity
    , @CategoryIDs    varchar(100) = NULL   --Category
    , @HasRecoveryOnly    bit      = NULL   --IAS
    , @UnitTypes   	  varchar(100) = NULL   --UnitType
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @FleetTable TABLE (ID varchar(100))
    INSERT INTO @FleetTable
    SELECT * from dbo.SplitStrings_CTE(@FleetCodes,',')

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

    DECLARE @CategoryTable TABLE (ID int)
    INSERT INTO @CategoryTable
    SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')
    
    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitTypes,',')

    IF @FleetCodes   = '' SET @FleetCodes   = NULL
    IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
    IF @CategoryIDs  = '' SET @CategoryIDs  = NULL
    IF @UnitTypes    = '' SET @UnitTypes  = NULL

    SELECT COUNT (*) 
    FROM 
        dbo.VW_IX_Fault f
        LEFT JOIN dbo.Location ON f.LocationID = Location.ID
        LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
        LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
        LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ID = xref.ID
    WHERE 
            (f.IsCurrent      = @isLive                             OR @isLive         IS NULL)
        AND (f.IsAcknowledged = @isAcknowledged                     OR @isAcknowledged IS NULL)
        AND (f.FaultTypeID    IN (SELECT ID FROM @TypeTable)        OR @FaultTypeIDs   IS NULL)
        AND (f.CategoryID     IN (SELECT ID FROM @CategoryTable)    OR @CategoryIDs    IS NULL)
        AND (f.FleetCode      IN (SELECT ID FROM @FleetTable)       OR @FleetCodes     IS NULL)
        AND (f.HasRecovery    = @HasRecoveryOnly                    OR @HasRecoveryOnly    IS NULL
            OR @HasRecoveryOnly = 0)
        AND (u.UnitType    IN (SELECT ID FROM @UnitTypeTable)    OR @UnitTypes     IS NULL)
        AND ReportingOnly = 0
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,085
           ,'update_spectrum_db_085.sql'
           ,'1.3.02')
GO