SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 107
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- update NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, f.FleetCode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, Description
		, Summary
		, Category
		, CategoryID
		, f.CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = sr.ExternalCode
		, MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, IsGroupLead
		, AdditionalInfo
		, IsAcknowledged
		, RowVersion
		, FaultGroupID
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
	WHERE 
		f.FaultGroupID = @FaultGroupID

END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,107
           ,'update_spectrum_db_107.sql'
           ,'1.5.02')
GO