SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 124
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


------------------------------------
-- Update NSP_InsertFault
------------------------------------

RAISERROR ('-- Update NSP_GetFaultChannelValue_L2', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue_L2' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValue_L2 AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue_L2]
    @DatetimeStart datetime
    , @DatetimeEnd datetime
    , @UnitID int = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        f.ID
        , FaultMetaID       = f.FaultMetaID
        , FaultCode         = fm.FaultCode
        , FaultCategoryID   = fm.CategoryID
        , FaultCategory     = fc.Category
        , FaultTypeID       = fm.FaultTypeID
        , FaultType         = ft.Name
        , UnitID         = u.ID
        , UnitNumber        = u.UnitNumber
        , UnitType          = u.UnitType
        , TimeStamp         = f.CreateTime
		, FaultCount		= f.CountSinceLastMaint
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm
        ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.Unit u
        ON f.FaultUnitID = u.ID
    INNER JOIN dbo.FaultType ft
        ON fm.FaultTypeID = ft.ID
    INNER JOIN dbo.FaultCategory fc
        ON fm.CategoryID = fc.ID
    WHERE f.CreateTime BETWEEN @DatetimeStart AND @DatetimeEnd
    AND (u.ID = @UnitID OR @UnitID IS NULL)
    ORDER BY TimeStamp
    
END
GO


--------------------------------------
-- Update NSP_InsertFaultInterface
--------------------------------------

RAISERROR ('-- Update NSP_GetFaultChannelValueByID_L2', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValueByID_L2' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueByID_L2 AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueByID_L2]
(
    @ID bigint
)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.Fault), 0) - 1;
	
	DECLARE @MinID BIGINT = (SELECT MIN(ID) -1  FROM dbo.Fault WHERE ID > @ID)
	
	IF @MinID > @ID SET @ID = @MinID;

    DECLARE @dataSetSize int = 5000
    
    SELECT 
        f.ID
        , FaultMetaID       = f.FaultMetaID
        , FaultCode         = fm.FaultCode
        , FaultCategoryID   = fm.CategoryID
        , FaultCategory     = fc.Category
        , FaultTypeID       = fm.FaultTypeID
        , FaultType         = ft.Name
        , UnitID            = u.ID
        , UnitNumber        = u.UnitNumber
        , UnitType          = u.UnitType
        , TimeStamp         = f.CreateTime
		, FaultCount		= f.CountSinceLastMaint
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.Unit u ON f.FaultUnitID = u.ID
    INNER JOIN dbo.FaultType ft ON fm.FaultTypeID = ft.ID
    INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
    WHERE f.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
    ORDER BY ID

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,124
           ,'update_spectrum_db_124.sql'
           ,'1.7.01')
GO