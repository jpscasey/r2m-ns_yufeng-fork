SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 128
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update NSP_GetFaultLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
    @UnitID int
)
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
		, f.IsAcknowledged
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO

----------------------------------------------------------------------------------------

RAISERROR ('-- Update NSP_GetFaultNotLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
    @N int
    , @UnitID int
)
AS
    SET NOCOUNT ON;

    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
		, f.IsAcknowledged
    FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,128
           ,'update_spectrum_db_128.sql'
           ,'1.7.02')
GO