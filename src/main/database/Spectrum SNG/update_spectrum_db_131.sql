SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 131
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update NSP_InsertServiceRequest', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertServiceRequest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertServiceRequest AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertServiceRequest](
    @FaultID int
    , @ServiceRequestId varchar(50)
	, @CreatedByRule bit
)
AS
BEGIN

	DECLARE @ServiceRequestInternalID int
	DECLARE @FaultGroupID int

	INSERT INTO ServiceRequest (FaultID, UnitID, ExternalCode, Status, CreateTime, LastUpdateTime, CreatedByRule) 
	VALUES (@FaultID, (SELECT FaultUnitID FROM dbo.Fault WHERE ID = @FaultID), @ServiceRequestId, 'New', GETDATE(), GETDATE(), @CreatedByRule)
	
	SELECT @ServiceRequestInternalID = @@IDENTITY
	SET @FaultGroupID = (SELECT FaultGroupID FROM FAULT WHERE ID = @FaultID)

	UPDATE Fault
	SET LastUpdateTime = GETDATE(),
	    ServiceRequestID = @ServiceRequestInternalID,
	    IsAcknowledged = 1,
	    AcknowledgedTime = GETDATE()
	WHERE ID = @FaultID
		OR ( @FaultGroupID IS NOT NULL AND FaultGroupID = @FaultGroupID )
END

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,131
           ,'update_spectrum_db_131.sql'
           ,'1.7.02')
GO

