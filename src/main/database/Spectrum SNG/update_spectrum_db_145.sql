SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 145
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------

RAISERROR ('-- alter EventChanelLatestValue table, add LastInsertTime column', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventChannelLatestValue' AND COLUMN_NAME='LastInsertTime')
BEGIN
    ALTER TABLE [dbo].[EventChannelLatestValue]
      ADD [LastInsertTime] datetime2(3)
END
GO

----------------------------------------------------------------------------

RAISERROR ('-- update NSP_InsertEventChannelValue', 0, 1) WITH NOWAIT
GO

-- update NSP_InsertEventChannelValue to only insert newer channel values to EventChannelLatestValue, also record the Insert Time
ALTER PROCEDURE [dbo].[NSP_InsertEventChannelValue] (
    @Timestamp DATETIME2(3)
    , @TimestampEndTime DATETIME2(3) = NULL
    , @UnitNumber Varchar(16)
    , @ChannelName VarChar(100)
	, @Value bit
    )
AS
/******************************************************************************
**  Name:           NSP_InsertEventChannelValue
**  Description:    Inserts a new event-based channel value for the SLT fleet
**  Call frequency: Called by the telemetry service when the event happens. 
**                  The event is considered started if a start time (timeStamp) only is given.
**                  The event is considered finished when a end date is given.
**                  
**  Parameters:     @timestamp the timestamp when the event happens
**                  @timestamp the timestamp when the event happens
**                  @UnitId the Unit ID
**                  @ChannelId the channel ID
******************************************************************************/
BEGIN
	DECLARE @SqlStr varchar(max),
	@ChannelId int,
	@UnitID int
	SELECT @ChannelId = ID FROM Channel where Name = @ChannelName
	SELECT @UnitID = ID FROM Unit where UnitNumber = @UnitNumber

	-- Insert a record into EventChannelValue
    INSERT INTO dbo.EventChannelValue (TimeStamp, TimestampEndTime, UnitID, ChannelId, Value)
    VALUES (@timestamp, @TimestampEndTime, @UnitId, @ChannelId, @Value)

	IF not @TimestampEndTime is null
		SET @timestamp = @TimestampEndTime-- for the latest value I'm expect that TimestampEndTime will always have the latest datetime

	-- Update EventChannelLatestValue
    IF EXISTS (SELECT top 1 1 FROM dbo.EventChannelLatestValue WHERE UnitID = @UnitId and ChannelID = @ChannelId)
		SET @SqlStr = '
			UPDATE dbo.EventChannelLatestValue 
			SET TimeLastUpdated = ''' + CAST(@timestamp AS VARCHAR(25)) + ''',
				Value = ' + CAST(@value AS VARCHAR(5)) + ' , LastInsertTime = ''' + CAST(SYSDATETIME() AS VARCHAR(25)) + '''
			WHERE UnitID = ' + CAST(@UnitId AS VARCHAR(5)) + ' and ChannelID = ' + CAST(@ChannelId AS VARCHAR(5)) + ' 
			and (TimeLastUpdated < ''' + CAST(@timestamp AS VARCHAR(25)) + ''')'
		
    ELSE
	    SET @SqlStr = '
	        INSERT INTO dbo.EventChannelLatestValue(TimeLastUpdated, UnitID, ChannelID,Value, LastInsertTime)  
		    VALUES (''' + CAST(@timestamp AS VARCHAR(25)) + ''', ' + CAST(@UnitId AS VARCHAR(5)) + ', ' + CAST(@ChannelId AS VARCHAR(5)) +', ' + CAST(@value AS VARCHAR(5)) + ', ''' + CAST(SYSDATETIME() AS VARCHAR(25)) + ''')
	    '

    EXEC(@SqlStr)
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,145
           ,'update_spectrum_db_145.sql'
           ,'1.8.03')
GO