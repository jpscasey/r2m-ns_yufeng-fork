SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 148
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_PopulateOtherFaultCategoryMeata' AND ROUTINE_TYPE = 'PROCEDURE')  
	DROP PROCEDURE NSP_PopulateOtherFaultCategoryMeata

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_PopulateOtherFaultCategoryMeta' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_PopulateOtherFaultCategoryMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PRocedure [dbo].[NSP_PopulateOtherFaultCategoryMeta]
AS

BEGIN

DECLARE @ID int =0
DECLARE @sql varchar(max)

	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql = 'EXEC [dbo].[NSP_InsertFaultCategory_VIRM] ' + cast(ID as varchar(10)) +' , '''+Category + ''', ' +cast(ReportingOnly as varchar(10)) 
			,@ID = ID
		FROM FaultCategory
		where ID > @ID
		ORDER BY ID

		IF @sql is null BREAK;

		BEGIN TRY
			EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

	SET @ID = 0
	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql= 'EXEC [dbo].[NSP_InsertFaultMeta_VIRM] ' + cast(ID as varchar(10)) +' , '
		+''''+ isnull(Username,'') + ''', ' 
		+''''+ isnull(FaultCode,'') + ''', ' 
		+''''+ isnull(Description,'') + ''', ' 
		+''''+ isnull(Summary,'') + ''', ' 
		+''''+ isnull(AdditionalInfo,'') + ''', ' 
		+''''+ isnull(Url,'') + ''', ' 
		+isnull(cast(CategoryID as varchar(10)),'null') + ', ' 
		+isnull(cast(HasRecovery as varchar(10)),'null') + ', ' 
		+''''+ isnull(RecoveryProcessPath,'') + ''', '
		+isnull(cast(+FaultTypeID as varchar(10)),'null') + ', '
		+isnull(cast(GroupID  as varchar(10)),'null')
		,@ID = ID
		FROM FaultMeta
		where ID > @ID
		ORDER BY ID

		IF @sql is null BREAK;

		BEGIN TRY
		EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

END

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,148
           ,'update_spectrum_db_148.sql'
           ,'1.9.01')
GO