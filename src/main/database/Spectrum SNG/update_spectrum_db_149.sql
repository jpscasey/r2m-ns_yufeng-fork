SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 149
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SaveChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_SaveChannelDefinition AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SaveChannelDefinition]
    @ChannelDefinitionParam varchar(max)
AS
BEGIN
    DECLARE @channelDoc xml = N''+@ChannelDefinitionParam;

    --read the channel definition details
    DECLARE @channelId int;
    DECLARE @channelGroupID int;
    DECLARE @channelRef varchar(10);
    DECLARE @channelUnitOfMeasure varchar(10);
    DECLARE @channelGroupOrder int;
    DECLARE @channelAlwaysDisplayed bit;
	DECLARE @channelComment varchar(500);
	DECLARE @channelDescription varchar(100);
    DECLARE @previousOder int;

    SELECT
        @channelId = t.c.value(N'@id', N'int'),
        @channelGroupId = t.c.value(N'@channelgroupid', N'int'),
        @channelRef = t.c.value(N'@ref', N'varchar(10)'),
        @channelUnitOfMeasure = t.c.value(N'@unitofmeasure', N'varchar(10)'),
        @channelGroupOrder = t.c.value(N'@channelgrouporder', N'int'),
        @channelAlwaysDisplayed = t.c.value(N'@alwaysdisplayed', N'bit'),
		@channelComment = t.c.value(N'@comment', N'varchar(500)'),
		@channelDescription = t.c.value(N'@description', N'varchar(100)')

    FROM @channelDoc.nodes('/channel') t(c);
    
    -- Get previous order  
    SELECT 
        @previousOder = ChannelGroupOrder 
    FROM Channel 
    WHERE ID = @channelId   
    
    IF @channelRef = ''
        SET @channelRef = NULL;

    IF @channelUnitOfMeasure = ''
        SET @channelUnitOfMeasure = NULL;

    IF @channelGroupOrder = ''
        SET @channelGroupOrder = NULL;

    BEGIN TRANSACTION;
        --move the channel group order 1 position ahead
        IF NOT @previousOder = @channelGroupOrder
            UPDATE
                [Channel]
            SET
                [ChannelGroupOrder] = [ChannelGroupOrder] + 1
            WHERE
                [ChannelGroupID] = @channelGroupId
                AND [ChannelGroupOrder] >= @channelGroupOrder;

        --update channel definition
        UPDATE
            [Channel]
        SET
            [ChannelGroupID] = @channelGroupId,
            [Ref] = @channelRef,
            [UOM] = @channelUnitOfMeasure,
            [ChannelGroupOrder] = @channelGroupOrder,
            [IsAlwaysDisplayed] = @channelAlwaysDisplayed,
			[Comment] = @channelComment,
			[Description] = @channelDescription
        WHERE
            [ID] = @channelId;

        --remove all rule/validations for the channel
        DELETE
            FROM [ChannelRuleValidation]
            WHERE
                ChannelRuleID IN (SELECT ID FROM [ChannelRule] WHERE ChannelID = @channelId);

        DELETE
            FROM [ChannelRule]
            WHERE ChannelID = @channelId;

        --process rules
        DECLARE @ruleIndex int; 
        DECLARE @rulesCount int;
        DECLARE @ruleXml xml;

        SELECT 
            @ruleIndex = 1,
            @rulesCount = @channelDoc.value('count(/channel/rule)','int');

        WHILE @ruleIndex <= @rulesCount BEGIN
            DECLARE @ruleName varchar(100);
            DECLARE @ruleStatusId int;
            DECLARE @ruleActive bit;
            DECLARE @ruleId int;

            SELECT
                @ruleXml = @channelDoc.query('/channel/rule[position()=sql:variable("@ruleIndex")]')

            SELECT
                @ruleName = t.c.value(N'@name', N'varchar(100)'),
                @ruleStatusId = t.c.value(N'@statusid', N'int'),
                @ruleActive = t.c.value(N'@active', N'bit')
            FROM @ruleXml.nodes('/rule') t(c);

            --insert the rule
            INSERT INTO ChannelRule (ChannelID, ChannelStatusID, Name, Active)
                VALUES (@channelId, @ruleStatusId, @ruleName, @ruleActive);

            SET @ruleId = SCOPE_IDENTITY();
            
            --process validations
            DECLARE @validationIndex int; 
            DECLARE @validationsCount int;
            DECLARE @validationXml xml;
            
            SELECT 
                @validationIndex = 1,
                @validationsCount = @ruleXml.value('count(/rule/validation)','int');

            WHILE @validationIndex <= @validationsCount BEGIN
                DECLARE @validationChannelId int;
                DECLARE @validationMinValue float;
                DECLARE @validationMaxValue float;
                DECLARE @validationMinInc bit;
                DECLARE @validationMaxInc bit;
                DECLARE @validationMinStringValue varchar(10);
                DECLARE @validationMaxStringValue varchar(10);
                
                SELECT
                    @validationXml = @ruleXml.query('/rule/validation[position()=sql:variable("@validationIndex")]')

                SELECT
                    @validationChannelId = t.c.value(N'@channelid', N'int'),
                    @validationMinStringValue = t.c.value(N'@minvalue', N'varchar(10)'),
                    @validationMaxStringValue = t.c.value(N'@maxvalue', N'varchar(10)'),
                    @validationMinValue = t.c.value(N'@minvalue', N'float'),
                    @validationMaxValue = t.c.value(N'@maxvalue', N'float'),
                    @validationMinInc = t.c.value(N'@mininclusive', N'bit'),
                    @validationMaxInc = t.c.value(N'@maxinclusive', N'bit')
                FROM @validationXml.nodes('/validation') t(c);

                IF @validationMinStringValue = ''
                    SET @validationMinValue = NULL
                ELSE 
                    SET @validationMinValue = CONVERT(float, @validationMinStringValue)

                IF @validationMaxStringValue = ''
                    SET @validationMaxValue = NULL
                ELSE 
                    SET @validationMaxValue = CONVERT(float, @validationMaxStringValue)
        
                --insert validation
                INSERT INTO ChannelRuleValidation (ChannelRuleID, ChannelID, MinValue, MaxValue, MinInclusive, MaxInclusive)
                    VALUES (@ruleId, @validationChannelId, @validationMinValue, @validationMaxValue, @validationMinInc, @validationMaxInc);
                
                SELECT @validationIndex = @validationIndex + 1;
            END
            
            SELECT @ruleIndex = @ruleIndex + 1;
        END

        --update configuration to reload the channel definitions and the validation rules
        UPDATE
            [Configuration]
        SET
            [PropertyValue] = 1
        WHERE
            [PropertyName] = 'spectrum.refresh.channelconfigurations'
            OR
            [PropertyName] = 'spectrum.refresh.validationrules';
            
    COMMIT TRANSACTION;
END

GO


-------------------------------------------------------------------------------------------------------------------------
-- update Channel table Comment column
-------------------------------------------------------------------------------------------------------------------------

RAISERROR ('-- Alter Comment column', 0, 1) WITH NOWAIT
GO

ALTER TABLE Channel
ALTER COLUMN [Comment] varchar(500)

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,149
           ,'update_spectrum_db_149.sql'
           ,'1.9.01')
GO