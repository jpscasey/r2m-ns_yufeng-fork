SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 151
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

-----------------------------------------------------------------------------------
-- Update NSP_InsertChannelValue_Text correcting typo, 'Reserve' renamed 'reserved'
-----------------------------------------------------------------------------------

RAISERROR ('-- Update NSP_InsertChannelValue_Text', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue_Text' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue_Text AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue_Text](
		@Timestamp datetime2(3),
		@UnitNumber varchar(16),
	    @UpdateRecord bit 
,@MCG_ILatitude decimal(9,6) = NULL ,@MCG_ILongitude decimal(9,6) = NULL ,@DIRECTION_CONTROL bit = NULL ,@CURRENT_SPEED smallint = NULL ,@AUX1_CDeratingReq_1_1 bit=NULL ,@AUX1_I3ACContactorOn_1_1 bit = NULL 
,@AUX1_I3ACContactorOn_2_1 bit = NULL ,@AUX1_I3ACContactorOn_2_2 bit = NULL ,@AUX1_I3ACLoadEnabled_1_1 bit = NULL ,@AUX1_I3ACOk_1_1 bit = NULL ,@AUX1_IInputVoltageOk_1_1 bit = NULL 
,@AUX1_IInverterEnabled_1_1 bit = NULL ,@AUX1_ILineContactorOn_1_1 bit = NULL ,@AUX1_IOutputVoltageOk_1_1 bit = NULL ,@AUX1_IPreChargContOn_1_1 bit = NULL ,@AUX1_ITempHeatSinkOk_1_1 bit = NULL 
,@AUX2_CDeratingReq_1_2 bit = NULL ,@AUX2_I3ACContactorOn_1_2 bit = NULL ,@AUX2_I3ACContactorOn_2_1 bit = NULL ,@AUX2_I3ACContactorOn_2_2 bit = NULL ,@AUX2_I3ACLoadEnabled_1_2 bit = NULL 
,@AUX2_I3ACOk_1_2 bit = NULL ,@AUX2_IInputVoltageOk_1_2 bit = NULL ,@AUX2_IInverterEnabled_1_2 bit = NULL ,@AUX2_ILineContactorOn_1_2 bit = NULL ,@AUX2_IOutputVoltageOk_1_2 bit = NULL 
,@AUX2_IPreChargContOn_1_2 bit = NULL ,@AUX2_ITempHeatSinkOk_1_2 bit = NULL ,@AUXY_IBatCont1On bit = NULL ,@AUXY_IBatCont1On_1_1 bit = NULL ,@AUXY_IBatCont1On_1_2 bit = NULL 
,@AUXY_IBatCont1On_2_1 bit = NULL ,@AUXY_IBatCont1On_2_2 bit = NULL ,@AUXY_IBatCont1On_3_1 bit = NULL ,@AUXY_IBatCont1On_3_2 bit = NULL ,@AUXY_IBatCont1On_8_1 bit = NULL 
,@AUXY_IBatCont1On_8_2 bit = NULL ,@AUXY_IBatCont1On_8_3 bit = NULL ,@AUXY_IBatCont1On_8_4 bit = NULL ,@AUXY_IBatCont1On_8_5 bit = NULL ,@AUXY_IBatCont1On_8_6 bit = NULL 
,@AUXY_IBatCont1On_8_7 bit = NULL ,@AUXY_IBatCont1On_8_8 bit = NULL ,@AUXY_IBatCont1On_9 bit = NULL ,@AUXY_IBatCont2On bit = NULL ,@AUXY_IBatCont2On_1_1 bit = NULL 
,@AUXY_IBatCont2On_1_2 bit = NULL ,@AUXY_IBatCont2On_2_1 bit = NULL ,@AUXY_IBatCont2On_2_2 bit = NULL ,@AUXY_IBatCont2On_3_1 bit = NULL ,@AUXY_IBatCont2On_3_2 bit = NULL 
,@AUXY_IBatCont2On_8_1 bit = NULL ,@AUXY_IBatCont2On_8_2 bit = NULL ,@AUXY_IBatCont2On_8_3 bit = NULL ,@AUXY_IBatCont2On_8_4 bit = NULL ,@AUXY_IBatCont2On_8_5 bit = NULL 
,@AUXY_IBatCont2On_8_6 bit = NULL ,@AUXY_IBatCont2On_8_7 bit = NULL ,@AUXY_IBatCont2On_8_8 bit = NULL ,@AUXY_IBatCont2On_9 bit = NULL ,@BCHA1_I110VAuxPowSup_2_1 decimal(5,1) = NULL 
,@BCHA1_IBattCurr_2_1 decimal(5,1) = NULL ,@BCHA1_IBattOutputCurr_2_1 decimal(5,1) = NULL ,@BCHA1_IChargCurrMax_2_1 decimal(5,1) = NULL ,@BCHA1_IDcLinkVoltage_2_1 decimal(5,1) = NULL ,@BCHA1_IExternal400Vac bit = NULL 
,@BCHA1_IExternal400Vac_1_1 bit = NULL ,@BCHA1_IExternal400Vac_1_2 bit = NULL ,@BCHA1_IExternal400Vac_2_1 bit = NULL ,@BCHA1_IExternal400Vac_2_2 bit = NULL ,@BCHA1_IExternal400Vac_3_1 bit = NULL 
,@BCHA1_IExternal400Vac_3_2 bit = NULL ,@BCHA1_IExternal400Vac_8_1 bit = NULL ,@BCHA1_IExternal400Vac_8_2 bit = NULL ,@BCHA1_IExternal400Vac_8_3 bit = NULL ,@BCHA1_IExternal400Vac_8_4 bit = NULL 
,@BCHA1_IExternal400Vac_8_5 bit = NULL ,@BCHA1_IExternal400Vac_8_6 bit = NULL ,@BCHA1_IExternal400Vac_8_7 bit = NULL ,@BCHA1_IExternal400Vac_8_8 bit = NULL ,@BCHA1_IExternal400Vac_9 bit = NULL 
,@BCHA1_IOutputPower_2_1 smallint = NULL ,@BCHA1_ISpecSystNokMod_2_1 tinyint = NULL ,@BCHA1_ISpecSystOkMode_2_1 tinyint = NULL ,@BCHA1_ITempBattSens1_2_1 smallint = NULL ,@BCHA1_ITempBattSens2_2_1 smallint = NULL 
,@BCHA1_ITempHeatSink_2_1 smallint = NULL ,@BCHA2_I110VAuxPowSup_2_2 decimal(5,1) = NULL ,@BCHA2_IBattCurr_2_2 decimal(5,1) = NULL ,@BCHA2_IBattOutputCurr_2_2 decimal(5,1) = NULL ,@BCHA2_IChargCurrMax_2_2 decimal(5,1) = NULL 
,@BCHA2_IDcLinkVoltage_2_2 decimal(5,1) = NULL ,@BCHA2_IExternal400Vac bit = NULL ,@BCHA2_IExternal400Vac_1_1 bit = NULL ,@BCHA2_IExternal400Vac_1_2 bit = NULL ,@BCHA2_IExternal400Vac_2_1 bit = NULL 
,@BCHA2_IExternal400Vac_2_2 bit = NULL ,@BCHA2_IExternal400Vac_3_1 bit = NULL ,@BCHA2_IExternal400Vac_3_2 bit = NULL ,@BCHA2_IExternal400Vac_8_1 bit = NULL ,@BCHA2_IExternal400Vac_8_2 bit = NULL 
,@BCHA2_IExternal400Vac_8_3 bit = NULL ,@BCHA2_IExternal400Vac_8_4 bit = NULL ,@BCHA2_IExternal400Vac_8_5 bit = NULL ,@BCHA2_IExternal400Vac_8_6 bit = NULL ,@BCHA2_IExternal400Vac_8_7 bit = NULL 
,@BCHA2_IExternal400Vac_8_8 bit = NULL ,@BCHA2_IExternal400Vac_9 bit = NULL ,@BCHA2_IOutputPower_2_2 smallint = NULL ,@BCHA2_ISpecSystNokMod_2_2 tinyint = NULL ,@BCHA2_ISpecSystOkMode_2_2 tinyint = NULL 
,@BCHA2_ITempBattSens1_2_2 smallint = NULL ,@BCHA2_ITempBattSens2_2_2 smallint = NULL ,@BCHA2_ITempHeatSink_2_2 smallint = NULL ,@BCU1_IBrPipePressureInt_5_1 decimal(5,1) = NULL ,@BCU1_IMainAirResPipeInt_5_1 decimal(5,1) = NULL 
,@BCU1_IParkBrApplied_5_1 bit = NULL ,@BCU1_IParkBrLocked_5_1 bit = NULL ,@BCU1_IParkBrReleased_5_1 bit = NULL ,@BCU2_IBrPipePressureInt_5_2 decimal(5,1) = NULL ,@BCU2_IMainAirResPipeInt_5_2 decimal(5,1) = NULL 
,@BCU2_IParkBrApplied_5_2 bit = NULL ,@BCU2_IParkBrLocked_5_2 bit = NULL ,@BCU2_IParkBrReleased_5_2 bit = NULL ,@BCUm_IBrTestRun_5_1 bit = NULL ,@BCUm_IBrTestRun_5_2 bit = NULL 
,@BCUx_IBogie1Locked_5_1 bit = NULL ,@BCUx_IBogie1Locked_5_2 bit = NULL ,@BCUx_IBogie2Locked_5_1 bit = NULL ,@BCUx_IBogie2Locked_5_2 bit = NULL ,@BCUx_IBogie3Locked_5_1 bit = NULL 
,@BCUx_IBogie3Locked_5_2 bit = NULL ,@BCUx_IBogie4Locked_5_1 bit = NULL ,@BCUx_IBogie4Locked_5_2 bit = NULL ,@BCUx_IBogie5Locked_5_1 bit = NULL ,@BCUx_IBogie5Locked_5_2 bit = NULL 
,@BCUx_IBogie6Locked_5_1 bit = NULL ,@BCUx_IBogie6Locked_5_2 bit = NULL ,@BCUx_IBogie7Locked_5_1 bit = NULL ,@BCUx_IBogie7Locked_5_2 bit = NULL ,@BCUx_IWspBogie1Slide_3_1 bit = NULL 
,@BCUx_IWspBogie1Slide_3_2 bit = NULL ,@BCUx_IWspBogie1Slide_5_1 bit = NULL ,@BCUx_IWspBogie1Slide_5_2 bit = NULL ,@BCUx_IWspBogie2Slide_3_1 bit = NULL ,@BCUx_IWspBogie2Slide_3_2 bit = NULL 
,@BCUx_IWspBogie2Slide_5_1 bit = NULL ,@BCUx_IWspBogie2Slide_5_2 bit = NULL ,@BCUx_IWspBogie3Slide_3_1 bit = NULL ,@BCUx_IWspBogie3Slide_3_2 bit = NULL ,@BCUx_IWspBogie3Slide_5_1 bit = NULL 
,@BCUx_IWspBogie3Slide_5_2 bit = NULL ,@BCUx_IWspBogie4Slide_3_1 bit = NULL ,@BCUx_IWspBogie4Slide_3_2 bit = NULL ,@BCUx_IWspBogie4Slide_5_1 bit = NULL ,@BCUx_IWspBogie4Slide_5_2 bit = NULL 
,@BCUx_IWspBogie5Slide_3_1 bit = NULL ,@BCUx_IWspBogie5Slide_3_2 bit = NULL ,@BCUx_IWspBogie5Slide_5_1 bit = NULL ,@BCUx_IWspBogie5Slide_5_2 bit = NULL ,@BCUx_IWspBogie6Slide_3_1 bit = NULL 
,@BCUx_IWspBogie6Slide_3_2 bit = NULL ,@BCUx_IWspBogie6Slide_5_1 bit = NULL ,@BCUx_IWspBogie6Slide_5_2 bit = NULL ,@BCUx_IWspBogie7Slide_3_1 bit = NULL ,@BCUx_IWspBogie7Slide_3_2 bit = NULL 
,@BCUx_IWspBogie7Slide_5_1 bit = NULL ,@BCUx_IWspBogie7Slide_5_2 bit = NULL ,@CABIN bit = NULL ,@CABIN_IST bit = NULL ,@CABIN_SOLL bit = NULL 
,@CABIN_STATUS bit = NULL ,@CYCLE_COUNTER tinyint = NULL ,@DBC_CBCU1ModeTowing_5_1 bit = NULL ,@DBC_CBCU1ModeTowing_5_2 bit = NULL ,@DBC_CBCU2ModeTowing_5_1 bit = NULL 
,@DBC_CBCU2ModeTowing_5_2 bit = NULL ,@DBC_Cbrake_M bit = NULL ,@DBC_Cbrake_M_1_1 bit = NULL ,@DBC_Cbrake_M_1_2 bit = NULL ,@DBC_Cbrake_M_2_1 bit = NULL 
,@DBC_Cbrake_M_2_2 bit = NULL ,@DBC_Cbrake_M_3_1 bit = NULL ,@DBC_Cbrake_M_3_2 bit = NULL ,@DBC_Cbrake_M_4_1 bit = NULL ,@DBC_Cbrake_M_4_10 bit = NULL 
,@DBC_Cbrake_M_4_11 bit = NULL ,@DBC_Cbrake_M_4_12 bit = NULL ,@DBC_Cbrake_M_4_13 bit = NULL ,@DBC_Cbrake_M_4_14 bit = NULL ,@DBC_Cbrake_M_4_15 bit = NULL 
,@DBC_Cbrake_M_4_16 bit = NULL ,@DBC_Cbrake_M_4_17 bit = NULL ,@DBC_Cbrake_M_4_18 bit = NULL ,@DBC_Cbrake_M_4_19 bit = NULL ,@DBC_Cbrake_M_4_2 bit = NULL 
,@DBC_Cbrake_M_4_20 bit = NULL ,@DBC_Cbrake_M_4_3 bit = NULL ,@DBC_Cbrake_M_4_4 bit = NULL ,@DBC_Cbrake_M_4_5 bit = NULL ,@DBC_Cbrake_M_4_6 bit = NULL 
,@DBC_Cbrake_M_4_7 bit = NULL ,@DBC_Cbrake_M_4_8 bit = NULL ,@DBC_Cbrake_M_4_9 bit = NULL ,@DBC_Cbrake_M_5_1 bit = NULL ,@DBC_Cbrake_M_5_2 bit = NULL 
,@DBC_Cbrake_M_8_1 bit = NULL ,@DBC_Cbrake_M_8_2 bit = NULL ,@DBC_Cbrake_M_8_3 bit = NULL ,@DBC_Cbrake_M_8_4 bit = NULL ,@DBC_Cbrake_M_8_5 bit = NULL 
,@DBC_Cbrake_M_8_6 bit = NULL ,@DBC_Cbrake_M_8_7 bit = NULL ,@DBC_Cbrake_M_8_8 bit = NULL ,@DBC_CBrakeQuick_M bit = NULL ,@DBC_CBrakeQuick_M_1_1 bit = NULL 
,@DBC_CBrakeQuick_M_1_2 bit = NULL ,@DBC_CBrakeQuick_M_2_1 bit = NULL ,@DBC_CBrakeQuick_M_2_2 bit = NULL ,@DBC_CBrakeQuick_M_3_1 bit = NULL ,@DBC_CBrakeQuick_M_3_2 bit = NULL 
,@DBC_CBrakeQuick_M_4_1 bit = NULL ,@DBC_CBrakeQuick_M_4_10 bit = NULL ,@DBC_CBrakeQuick_M_4_11 bit = NULL ,@DBC_CBrakeQuick_M_4_12 bit = NULL ,@DBC_CBrakeQuick_M_4_13 bit = NULL 
,@DBC_CBrakeQuick_M_4_14 bit = NULL ,@DBC_CBrakeQuick_M_4_15 bit = NULL ,@DBC_CBrakeQuick_M_4_16 bit = NULL ,@DBC_CBrakeQuick_M_4_17 bit = NULL ,@DBC_CBrakeQuick_M_4_18 bit = NULL 
,@DBC_CBrakeQuick_M_4_19 bit = NULL ,@DBC_CBrakeQuick_M_4_2 bit = NULL ,@DBC_CBrakeQuick_M_4_20 bit = NULL ,@DBC_CBrakeQuick_M_4_3 bit = NULL ,@DBC_CBrakeQuick_M_4_4 bit = NULL 
,@DBC_CBrakeQuick_M_4_5 bit = NULL ,@DBC_CBrakeQuick_M_4_6 bit = NULL ,@DBC_CBrakeQuick_M_4_7 bit = NULL ,@DBC_CBrakeQuick_M_4_8 bit = NULL ,@DBC_CBrakeQuick_M_4_9 bit = NULL 
,@DBC_CBrakeQuick_M_5_1 bit = NULL ,@DBC_CBrakeQuick_M_5_2 bit = NULL ,@DBC_CBrakeQuick_M_8_1 bit = NULL ,@DBC_CBrakeQuick_M_8_2 bit = NULL ,@DBC_CBrakeQuick_M_8_3 bit = NULL 
,@DBC_CBrakeQuick_M_8_4 bit = NULL ,@DBC_CBrakeQuick_M_8_5 bit = NULL ,@DBC_CBrakeQuick_M_8_6 bit = NULL ,@DBC_CBrakeQuick_M_8_7 bit = NULL ,@DBC_CBrakeQuick_M_8_8 bit = NULL 
,@DBC_CBrakingEffortInt_3_1 tinyint = NULL ,@DBC_CBrakingEffortInt_3_2 tinyint = NULL ,@DBC_CBrakingEffortInt_5_1 tinyint = NULL ,@DBC_CBrakingEffortInt_5_2 tinyint = NULL ,@DBC_Cdrive_M bit = NULL 
,@DBC_Cdrive_M_1_1 bit = NULL ,@DBC_Cdrive_M_1_2 bit = NULL ,@DBC_Cdrive_M_2_1 bit = NULL ,@DBC_Cdrive_M_2_2 bit = NULL ,@DBC_Cdrive_M_3_1 bit = NULL 
,@DBC_Cdrive_M_3_2 bit = NULL ,@DBC_Cdrive_M_4_1 bit = NULL ,@DBC_Cdrive_M_4_10 bit = NULL ,@DBC_Cdrive_M_4_11 bit = NULL ,@DBC_Cdrive_M_4_12 bit = NULL 
,@DBC_Cdrive_M_4_13 bit = NULL ,@DBC_Cdrive_M_4_14 bit = NULL ,@DBC_Cdrive_M_4_15 bit = NULL ,@DBC_Cdrive_M_4_16 bit = NULL ,@DBC_Cdrive_M_4_17 bit = NULL 
,@DBC_Cdrive_M_4_18 bit = NULL ,@DBC_Cdrive_M_4_19 bit = NULL ,@DBC_Cdrive_M_4_2 bit = NULL ,@DBC_Cdrive_M_4_20 bit = NULL ,@DBC_Cdrive_M_4_3 bit = NULL 
,@DBC_Cdrive_M_4_4 bit = NULL ,@DBC_Cdrive_M_4_5 bit = NULL ,@DBC_Cdrive_M_4_6 bit = NULL ,@DBC_Cdrive_M_4_7 bit = NULL ,@DBC_Cdrive_M_4_8 bit = NULL 
,@DBC_Cdrive_M_4_9 bit = NULL ,@DBC_Cdrive_M_5_1 bit = NULL ,@DBC_Cdrive_M_5_2 bit = NULL ,@DBC_Cdrive_M_8_1 bit = NULL ,@DBC_Cdrive_M_8_2 bit = NULL 
,@DBC_Cdrive_M_8_3 bit = NULL ,@DBC_Cdrive_M_8_4 bit = NULL ,@DBC_Cdrive_M_8_5 bit = NULL ,@DBC_Cdrive_M_8_6 bit = NULL ,@DBC_Cdrive_M_8_7 bit = NULL 
,@DBC_Cdrive_M_8_8 bit = NULL ,@DBC_CModeTowing_M_5_1 bit = NULL ,@DBC_CModeTowing_M_5_2 bit = NULL ,@DBC_CTCU1GrInTracConv1_3_1 bit = NULL ,@DBC_CTCU2GrInTracConv2_3_2 bit = NULL 
,@DBC_CTractiveEffort_3_1 tinyint = NULL ,@DBC_CTractiveEffort_3_2 tinyint = NULL ,@DBC_ITrainSpeedInt smallint = NULL ,@DBC_ITrainSpeedInt_1_1 smallint = NULL ,@DBC_ITrainSpeedInt_1_2 smallint = NULL 
,@DBC_ITrainSpeedInt_15 smallint = NULL ,@DBC_ITrainSpeedInt_2_1 smallint = NULL ,@DBC_ITrainSpeedInt_2_2 smallint = NULL ,@DBC_ITrainSpeedInt_3_1 smallint = NULL ,@DBC_ITrainSpeedInt_3_2 smallint = NULL 
,@DBC_ITrainSpeedInt_4_1 smallint = NULL ,@DBC_ITrainSpeedInt_4_10 smallint = NULL ,@DBC_ITrainSpeedInt_4_11 smallint = NULL ,@DBC_ITrainSpeedInt_4_12 smallint = NULL ,@DBC_ITrainSpeedInt_4_13 smallint = NULL 
,@DBC_ITrainSpeedInt_4_14 smallint = NULL ,@DBC_ITrainSpeedInt_4_15 smallint = NULL ,@DBC_ITrainSpeedInt_4_16 smallint = NULL ,@DBC_ITrainSpeedInt_4_17 smallint = NULL ,@DBC_ITrainSpeedInt_4_18 smallint = NULL 
,@DBC_ITrainSpeedInt_4_19 smallint = NULL ,@DBC_ITrainSpeedInt_4_2 smallint = NULL ,@DBC_ITrainSpeedInt_4_20 smallint = NULL ,@DBC_ITrainSpeedInt_4_3 smallint = NULL ,@DBC_ITrainSpeedInt_4_4 smallint = NULL 
,@DBC_ITrainSpeedInt_4_5 smallint = NULL ,@DBC_ITrainSpeedInt_4_6 smallint = NULL ,@DBC_ITrainSpeedInt_4_7 smallint = NULL ,@DBC_ITrainSpeedInt_4_8 smallint = NULL ,@DBC_ITrainSpeedInt_4_9 smallint = NULL 
,@DBC_ITrainSpeedInt_5_1 smallint = NULL ,@DBC_ITrainSpeedInt_5_2 smallint = NULL ,@DBC_ITrainSpeedInt_8_1 smallint = NULL ,@DBC_ITrainSpeedInt_8_2 smallint = NULL ,@DBC_ITrainSpeedInt_8_3 smallint = NULL 
,@DBC_ITrainSpeedInt_8_4 smallint = NULL ,@DBC_ITrainSpeedInt_8_5 smallint = NULL ,@DBC_ITrainSpeedInt_8_6 smallint = NULL ,@DBC_ITrainSpeedInt_8_7 smallint = NULL ,@DBC_ITrainSpeedInt_8_8 smallint = NULL 
,@DBC_ITrainSpeedInt_9 smallint = NULL ,@DBC_IZrSpdInd_4_1 bit = NULL ,@DBC_IZrSpdInd_4_10 bit = NULL ,@DBC_IZrSpdInd_4_11 bit = NULL ,@DBC_IZrSpdInd_4_12 bit = NULL 
,@DBC_IZrSpdInd_4_13 bit = NULL ,@DBC_IZrSpdInd_4_14 bit = NULL ,@DBC_IZrSpdInd_4_15 bit = NULL ,@DBC_IZrSpdInd_4_16 bit = NULL ,@DBC_IZrSpdInd_4_17 bit = NULL 
,@DBC_IZrSpdInd_4_18 bit = NULL ,@DBC_IZrSpdInd_4_19 bit = NULL ,@DBC_IZrSpdInd_4_2 bit = NULL ,@DBC_IZrSpdInd_4_20 bit = NULL ,@DBC_IZrSpdInd_4_3 bit = NULL 
,@DBC_IZrSpdInd_4_4 bit = NULL ,@DBC_IZrSpdInd_4_5 bit = NULL ,@DBC_IZrSpdInd_4_6 bit = NULL ,@DBC_IZrSpdInd_4_7 bit = NULL ,@DBC_IZrSpdInd_4_8 bit = NULL 
,@DBC_IZrSpdInd_4_9 bit = NULL ,@DBC_PRTcu1GrOut_3_1 bit = NULL ,@DBC_PRTcu2GrOut_3_2 bit = NULL ,@DRIVERBRAKE_APPLIED_SUFF bit = NULL ,@DRIVERBRAKE_OPERATED bit = NULL 
,@EB_STATUS bit = NULL ,@ERROR_CODE_INTERNAL bit = NULL ,@ERRORCODE_1 bit = NULL ,@ERRORCODE_2 bit = NULL ,@ERRORCODE_3 bit = NULL 
,@GW_IGwOrientation tinyint = NULL ,@HAC_ITempOutsideInt smallint = NULL ,@HAC_ITempOutsideInt_1_1 smallint = NULL ,@HAC_ITempOutsideInt_1_2 smallint = NULL ,@HAC_ITempOutsideInt_2_1 smallint = NULL 
,@HAC_ITempOutsideInt_2_2 smallint = NULL ,@HAC_ITempOutsideInt_3_1 smallint = NULL ,@HAC_ITempOutsideInt_3_2 smallint = NULL ,@HAC_ITempOutsideInt_4_1 smallint = NULL ,@HAC_ITempOutsideInt_4_10 smallint = NULL 
,@HAC_ITempOutsideInt_4_11 smallint = NULL ,@HAC_ITempOutsideInt_4_12 smallint = NULL ,@HAC_ITempOutsideInt_4_13 smallint = NULL ,@HAC_ITempOutsideInt_4_14 smallint = NULL ,@HAC_ITempOutsideInt_4_15 smallint = NULL 
,@HAC_ITempOutsideInt_4_16 smallint = NULL ,@HAC_ITempOutsideInt_4_17 smallint = NULL ,@HAC_ITempOutsideInt_4_18 smallint = NULL ,@HAC_ITempOutsideInt_4_19 smallint = NULL ,@HAC_ITempOutsideInt_4_2 smallint = NULL 
,@HAC_ITempOutsideInt_4_20 smallint = NULL ,@HAC_ITempOutsideInt_4_3 smallint = NULL ,@HAC_ITempOutsideInt_4_4 smallint = NULL ,@HAC_ITempOutsideInt_4_5 smallint = NULL ,@HAC_ITempOutsideInt_4_6 smallint = NULL 
,@HAC_ITempOutsideInt_4_7 smallint = NULL ,@HAC_ITempOutsideInt_4_8 smallint = NULL ,@HAC_ITempOutsideInt_4_9 smallint = NULL ,@HAC_ITempOutsideInt_5_1 smallint = NULL ,@HAC_ITempOutsideInt_5_2 smallint = NULL 
,@HAC_ITempOutsideInt_9 smallint = NULL ,@HVAC1_ITempAirMixedInt smallint = NULL ,@HVAC1_ITempDuctAir1Int smallint = NULL ,@HVAC1_ITempDuctAir2Int smallint = NULL ,@HVAC1_ITempInComInt smallint = NULL 
,@HVAC1_ITempOutsideInt smallint = NULL ,@HVAC1_ITempSetpActDuctInt smallint = NULL ,@HVAC1_ITempSetpActInt smallint = NULL ,@HVAC2_ITempAirMixedInt smallint = NULL ,@HVAC2_ITempDuctAir1Int smallint = NULL 
,@HVAC2_ITempDuctAir2Int smallint = NULL ,@HVAC2_ITempInComInt smallint = NULL ,@HVAC2_ITempOutsideInt smallint = NULL ,@HVAC2_ITempSetpActDuctInt smallint = NULL ,@HVAC2_ITempSetpActInt smallint = NULL 
,@HVAC3_ITempAirMixedInt smallint = NULL ,@HVAC3_ITempDuctAir1Int smallint = NULL ,@HVAC3_ITempDuctAir2Int smallint = NULL ,@HVAC3_ITempInComInt smallint = NULL ,@HVAC3_ITempOutsideInt smallint = NULL 
,@HVAC3_ITempSetpActDuctInt smallint = NULL ,@HVAC3_ITempSetpActInt smallint = NULL ,@HVAC4_ITempAirMixedInt smallint = NULL ,@HVAC4_ITempDuctAir1Int smallint = NULL ,@HVAC4_ITempDuctAir2Int smallint = NULL 
,@HVAC4_ITempInComInt smallint = NULL ,@HVAC4_ITempOutsideInt smallint = NULL ,@HVAC4_ITempSetpActDuctInt smallint = NULL ,@HVAC4_ITempSetpActInt smallint = NULL ,@HVAC5_ITempAirMixedInt smallint = NULL 
,@HVAC5_ITempDuctAir1Int smallint = NULL ,@HVAC5_ITempDuctAir2Int smallint = NULL ,@HVAC5_ITempInComInt smallint = NULL ,@HVAC5_ITempOutsideInt smallint = NULL ,@HVAC5_ITempSetpActDuctInt smallint = NULL 
,@HVAC5_ITempSetpActInt smallint = NULL ,@HVAC6_ITempAirMixedInt smallint = NULL ,@HVAC6_ITempDuctAir1Int smallint = NULL ,@HVAC6_ITempDuctAir2Int smallint = NULL ,@HVAC6_ITempInComInt smallint = NULL 
,@HVAC6_ITempOutsideInt smallint = NULL ,@HVAC6_ITempSetpActDuctInt smallint = NULL ,@HVAC6_ITempSetpActInt smallint = NULL ,@HVCC1_ITempDuctAir1Int smallint = NULL ,@HVCC1_ITempInComInt smallint = NULL 
,@HVCC1_ITempOutsideInt smallint = NULL ,@HVCC1_ITempSetpActDuctInt smallint = NULL ,@HVCC1_ITempSetpActInt smallint = NULL ,@HVCC2_ITempDuctAir1Int smallint = NULL ,@HVCC2_ITempInComInt smallint = NULL 
,@HVCC2_ITempOutsideInt smallint = NULL ,@HVCC2_ITempSetpActDuctInt smallint = NULL ,@HVCC2_ITempSetpActInt smallint = NULL ,@INadiCtrlInfo1 tinyint = NULL ,@INadiCtrlInfo2 tinyint = NULL 
,@INadiCtrlInfo3 tinyint = NULL ,@INadiNumberEntries tinyint = NULL ,@INadiTcnAddr1 tinyint = NULL ,@INadiTcnAddr2 tinyint = NULL ,@INadiTcnAddr3 tinyint = NULL 
,@INadiTopoCount smallint = NULL ,@INadiUicAddr1 tinyint = NULL ,@INadiUicAddr2 tinyint = NULL ,@INadiUicAddr3 tinyint = NULL ,@INTERNAL_ERROR_CODE smallint = NULL 
,@INTERNAL_STATE bit = NULL ,@ITrainsetNumber1 smallint = NULL ,@ITrainsetNumber2 smallint = NULL ,@ITrainsetNumber3 smallint = NULL ,@MCG_IGsmSigStrength smallint = NULL 
,@MIO_IContBusBar400On bit = NULL ,@MIO_IContBusBar400On_1_1 bit = NULL ,@MIO_IContBusBar400On_1_2 bit = NULL ,@MIO_IContBusBar400On_2_1 bit = NULL ,@MIO_IContBusBar400On_2_2 bit = NULL 
,@MIO_IContBusBar400On_3_1 bit = NULL ,@MIO_IContBusBar400On_3_2 bit = NULL ,@MIO_IContBusBar400On_8_1 bit = NULL ,@MIO_IContBusBar400On_8_2 bit = NULL ,@MIO_IContBusBar400On_8_3 bit = NULL 
,@MIO_IContBusBar400On_8_4 bit = NULL ,@MIO_IContBusBar400On_8_5 bit = NULL ,@MIO_IContBusBar400On_8_6 bit = NULL ,@MIO_IContBusBar400On_8_7 bit = NULL ,@MIO_IContBusBar400On_8_8 bit = NULL 
,@MIO_IContBusBar400On_9 bit = NULL ,@MIO_IHscbOff bit = NULL ,@MIO_IHscbOff_1_1 bit = NULL ,@MIO_IHscbOff_1_2 bit = NULL ,@MIO_IHscbOff_2_1 bit = NULL 
,@MIO_IHscbOff_2_2 bit = NULL ,@MIO_IHscbOff_3_1 bit = NULL ,@MIO_IHscbOff_3_2 bit = NULL ,@MIO_IHscbOff_8_1 bit = NULL ,@MIO_IHscbOff_8_2 bit = NULL 
,@MIO_IHscbOff_8_3 bit = NULL ,@MIO_IHscbOff_8_4 bit = NULL ,@MIO_IHscbOff_8_5 bit = NULL ,@MIO_IHscbOff_8_6 bit = NULL ,@MIO_IHscbOff_8_7 bit = NULL 
,@MIO_IHscbOff_8_8 bit = NULL ,@MIO_IHscbOn bit = NULL ,@MIO_IHscbOn_1_1 bit = NULL ,@MIO_IHscbOn_1_2 bit = NULL ,@MIO_IHscbOn_2_1 bit = NULL 
,@MIO_IHscbOn_2_2 bit = NULL ,@MIO_IHscbOn_3_1 bit = NULL ,@MIO_IHscbOn_3_2 bit = NULL ,@MIO_IHscbOn_8_1 bit = NULL ,@MIO_IHscbOn_8_2 bit = NULL 
,@MIO_IHscbOn_8_3 bit = NULL ,@MIO_IHscbOn_8_4 bit = NULL ,@MIO_IHscbOn_8_5 bit = NULL ,@MIO_IHscbOn_8_6 bit = NULL ,@MIO_IHscbOn_8_7 bit = NULL 
,@MIO_IHscbOn_8_8 bit = NULL ,@MIO_IPantoPrssSw1On bit = NULL ,@MIO_IPantoPrssSw1On_1_1 bit = NULL ,@MIO_IPantoPrssSw1On_1_2 bit = NULL ,@MIO_IPantoPrssSw1On_2_1 bit = NULL 
,@MIO_IPantoPrssSw1On_2_2 bit = NULL ,@MIO_IPantoPrssSw1On_3_1 bit = NULL ,@MIO_IPantoPrssSw1On_3_2 bit = NULL ,@MIO_IPantoPrssSw1On_8_1 bit = NULL ,@MIO_IPantoPrssSw1On_8_2 bit = NULL 
,@MIO_IPantoPrssSw1On_8_3 bit = NULL ,@MIO_IPantoPrssSw1On_8_4 bit = NULL ,@MIO_IPantoPrssSw1On_8_5 bit = NULL ,@MIO_IPantoPrssSw1On_8_6 bit = NULL ,@MIO_IPantoPrssSw1On_8_7 bit = NULL 
,@MIO_IPantoPrssSw1On_8_8 bit = NULL ,@MIO_IPantoPrssSw1On_9 bit = NULL ,@MIO_IPantoPrssSw2On bit = NULL ,@MIO_IPantoPrssSw2On_1_1 bit = NULL ,@MIO_IPantoPrssSw2On_1_2 bit = NULL 
,@MIO_IPantoPrssSw2On_2_1 bit = NULL ,@MIO_IPantoPrssSw2On_2_2 bit = NULL ,@MIO_IPantoPrssSw2On_3_1 bit = NULL ,@MIO_IPantoPrssSw2On_3_2 bit = NULL ,@MIO_IPantoPrssSw2On_8_1 bit = NULL 
,@MIO_IPantoPrssSw2On_8_2 bit = NULL ,@MIO_IPantoPrssSw2On_8_3 bit = NULL ,@MIO_IPantoPrssSw2On_8_4 bit = NULL ,@MIO_IPantoPrssSw2On_8_5 bit = NULL ,@MIO_IPantoPrssSw2On_8_6 bit = NULL 
,@MIO_IPantoPrssSw2On_8_7 bit = NULL ,@MIO_IPantoPrssSw2On_8_8 bit = NULL ,@MIO_IPantoPrssSw2On_9 bit = NULL ,@MMI_EB_STATUS tinyint = NULL ,@MMI_M_ACTIVE_CABIN tinyint = NULL 
,@MMI_M_LEVEL bit = NULL ,@MMI_M_MODE tinyint = NULL ,@MMI_M_WARNING tinyint = NULL ,@MMI_SB_STATUS tinyint = NULL ,@MMI_V_PERMITTED tinyint = NULL 
,@MMI_V_TRAIN smallint = NULL ,@NID_STM_1 tinyint = NULL ,@NID_STM_2 smallint = NULL ,@NID_STM_3 smallint = NULL ,@NID_STM_4 smallint = NULL 
,@NID_STM_5 tinyint = NULL ,@NID_STM_6 tinyint = NULL ,@NID_STM_7 tinyint = NULL ,@NID_STM_8 tinyint = NULL ,@NID_STM_DA smallint = NULL 
,@ONE_STM_DA bit = NULL ,@ONE_STM_HS bit = NULL ,@PB_ADDR bit = NULL ,@PERMITTED_SPEED tinyint = NULL ,@Q_ODO bit = NULL 
,@REPORTED_DIRECTION tinyint = NULL ,@REPORTED_SPEED tinyint = NULL ,@REPORTED_SPEED_HISTORIC tinyint = NULL ,@ROTATION_DIRECTION_SDU1 bit = NULL ,@ROTATION_DIRECTION_SDU2 bit = NULL 
,@SAP tinyint = NULL ,@SB_STATUS tinyint = NULL ,@SDU_DIRECTION_SDU1 tinyint = NULL ,@SDU_DIRECTION_SDU2 tinyint = NULL ,@SENSOR_ERROR_STATUS1_TACHO1 bit = NULL 
,@SENSOR_ERROR_STATUS2_TACHO2 bit = NULL ,@SENSOR_ERROR_STATUS3_RADAR1 bit = NULL ,@SENSOR_ERROR_STATUS4_RADAR2 bit = NULL ,@SENSOR_SPEED_HISTORIC_RADAR1 decimal(5,1) = NULL ,@SENSOR_SPEED_HISTORIC_RADAR2 decimal(5,1) = NULL 
,@SENSOR_SPEED_HISTORIC_TACHO1 decimal(5,1) = NULL ,@SENSOR_SPEED_HISTORIC_TACHO2 decimal(5,1) = NULL ,@SENSOR_SPEED_RADAR1 decimal(5,1) = NULL ,@SENSOR_SPEED_RADAR2 decimal(5,1) = NULL ,@SENSOR_SPEED_TACHO1 decimal(5,1) = NULL 
,@SENSOR_SPEED_TACHO2 decimal(5,1) = NULL ,@SLIP_SLIDE_ACC_AXLE1 bit = NULL ,@SLIP_SLIDE_ACC_AXLE2 bit = NULL ,@SLIP_SLIDE_AXLE1 tinyint = NULL ,@SLIP_SLIDE_AXLE2 bit = NULL 
,@SLIP_SLIDE_RADAR_DIFF_AXLE1 bit = NULL ,@SLIP_SLIDE_RADAR_DIFF_AXLE2 bit = NULL ,@SLIP_SLIDE_STATUS tinyint = NULL ,@SLIP_SLIDE_TACHO_DIFF_AXLE1 bit = NULL ,@SLIP_SLIDE_TACHO_DIFF_AXLE2 bit = NULL 
,@SOFTWARE_VERSION VARCHAR(15) = NULL ,@SPL_FUNC_ID smallint = NULL ,@SPL_RETURN smallint = NULL ,@STM_STATE tinyint = NULL ,@STM_SUB_STATE tinyint = NULL 
,@SUPERVISION_STATE tinyint = NULL ,@TC_CCorrectOrient_M tinyint = NULL ,@TC_IConsistOrient tinyint = NULL ,@TC_IGwOrient tinyint = NULL ,@TC_INumConsists tinyint = NULL 
,@TC_INumConsists_1_1 tinyint = NULL ,@TC_INumConsists_1_2 tinyint = NULL ,@TC_INumConsists_2_1 tinyint = NULL ,@TC_INumConsists_2_2 tinyint = NULL ,@TC_INumConsists_3_1 tinyint = NULL 
,@TC_INumConsists_3_2 tinyint = NULL ,@TC_INumConsists_4_1 tinyint = NULL ,@TC_INumConsists_4_10 tinyint = NULL ,@TC_INumConsists_4_11 tinyint = NULL ,@TC_INumConsists_4_12 tinyint = NULL 
,@TC_INumConsists_4_13 tinyint = NULL ,@TC_INumConsists_4_14 tinyint = NULL ,@TC_INumConsists_4_15 tinyint = NULL ,@TC_INumConsists_4_16 tinyint = NULL ,@TC_INumConsists_4_17 tinyint = NULL 
,@TC_INumConsists_4_18 tinyint = NULL ,@TC_INumConsists_4_19 tinyint = NULL ,@TC_INumConsists_4_2 tinyint = NULL ,@TC_INumConsists_4_20 tinyint = NULL ,@TC_INumConsists_4_3 tinyint = NULL 
,@TC_INumConsists_4_4 tinyint = NULL ,@TC_INumConsists_4_5 tinyint = NULL ,@TC_INumConsists_4_6 tinyint = NULL ,@TC_INumConsists_4_7 tinyint = NULL ,@TC_INumConsists_4_8 tinyint = NULL 
,@TC_INumConsists_4_9 tinyint = NULL ,@TC_INumConsists_5_1 tinyint = NULL ,@TC_INumConsists_5_2 tinyint = NULL ,@TC_INumConsists_8_1 tinyint = NULL ,@TC_INumConsists_8_2 tinyint = NULL 
,@TC_INumConsists_8_3 tinyint = NULL ,@TC_INumConsists_8_4 tinyint = NULL ,@TC_INumConsists_8_5 tinyint = NULL ,@TC_INumConsists_8_6 tinyint = NULL ,@TC_INumConsists_8_7 tinyint = NULL 
,@TC_INumConsists_8_8 tinyint = NULL ,@TC_INumConsists_9 tinyint = NULL ,@TC_ITclOrient tinyint = NULL ,@TCO_STATUS tinyint = NULL ,@TCU1_IEdSliding_3_1 bit = NULL 
,@TCU1_IEdSliding_5_1 bit = NULL ,@TCU1_ILineVoltage_3_1 decimal(5,1) = NULL ,@TCU1_ILineVoltage_5_1 decimal(5,1) = NULL ,@TCU1_ILineVoltage_5_2 decimal(5,1) = NULL ,@TCU1_IPowerLimit_3_1 bit = NULL 
,@TCU1_ITracBrActualInt_3_1 decimal(5,1) = NULL ,@TCU2_IEdSliding_3_2 bit = NULL ,@TCU2_IEdSliding_5_2 bit = NULL ,@TCU2_ILineVoltage_3_2 decimal(5,1) = NULL ,@TCU2_ILineVoltage_5_1 decimal(5,1) = NULL 
,@TCU2_ILineVoltage_5_2 decimal(5,1) = NULL ,@TCU2_IPowerLimit_3_2 bit = NULL ,@TCU2_ITracBrActualInt_3_2 decimal(5,1) = NULL ,@TRACK_SIGNAL tinyint = NULL ,@DBC_PRTcu1GrOut bit = NULL 
,@DBC_PRTcu2GrOut bit = NULL ,@MPW_PRHscbEnable bit = NULL ,@MPW_IPnt1UpDrvCount int = NULL ,@MPW_IPnt2UpDrvCount int = NULL ,@MPW_IPnt1UpCount int = NULL 
,@MPW_IPnt2UpCount int = NULL ,@MPW_IPnt1KmCount int = NULL ,@MPW_IPnt2KmCount int = NULL ,@DBC_IKmCounter int = NULL ,@AUXY_IAuxAirCOnCnt int = NULL 
,@MPW_IOnHscb int = NULL ,@AUXY_IAirCSwOnCount int = NULL ,@AUXY_IAirCOnCount int = NULL ,@AUXY_IAuxAirCSwOnCnt int = NULL ,@MPW_PRPantoEnable bit = NULL
,@DIA_Reserve1_AUX1 int = NULL ,@DIA_Reserve1_AUX2 int = NULL ,@DIA_Reserve1_BCHA_1 int = NULL ,@DIA_Reserve1_BCHA_2 int = NULL ,@DIA_Reserve1_BCU_1 int = NULL ,@DIA_Reserve1_BCU_2 int = NULL
,@DIA_Reserve1_CCUO int = NULL ,@DIA_Reserve1_DCU_1 int = NULL ,@DIA_Reserve1_DCU_10 int = NULL ,@DIA_Reserve1_DCU_11 int = NULL ,@DIA_Reserve1_DCU_12 int = NULL ,@DIA_Reserve1_DCU_13 int = NULL
,@DIA_Reserve1_DCU_14 int = NULL ,@DIA_Reserve1_DCU_15 int = NULL ,@DIA_Reserve1_DCU_16 int = NULL ,@DIA_Reserve1_DCU_17 int = NULL ,@DIA_Reserve1_DCU_18 int = NULL ,@DIA_Reserve1_DCU_19 int = NULL
,@DIA_Reserve1_DCU_2 int = NULL ,@DIA_Reserve1_DCU_20 int = NULL ,@DIA_Reserve1_DCU_3 int = NULL ,@DIA_Reserve1_DCU_4 int = NULL ,@DIA_Reserve1_DCU_5 int = NULL ,@DIA_Reserve1_DCU_6 int = NULL
,@DIA_Reserve1_DCU_7 int = NULL ,@DIA_Reserve1_DCU_8 int = NULL ,@DIA_Reserve1_DCU_9 int = NULL ,@DIA_Reserve1_HVAC_1 int = NULL ,@DIA_Reserve1_HVAC_2 int = NULL ,@DIA_Reserve1_HVAC_3 int = NULL
,@DIA_Reserve1_HVAC_4 int = NULL ,@DIA_Reserve1_HVAC_5 int = NULL ,@DIA_Reserve1_HVAC_6 int = NULL ,@DIA_Reserve1_HVCC_1 int = NULL ,@DIA_Reserve1_HVCC_2 int = NULL ,@DIA_Reserve1_PIS int = NULL
,@DIA_Reserve1_TCU_1 int = NULL ,@DIA_Reserve1_TCU_2 int = NULL ,@reserved int = NULL
,@DCU01_CDoorCloseConduc bit = NULL ,@DCU01_IDoorClosedSafe bit = NULL ,@DCU01_IDoorClRelease bit = NULL ,@DCU01_IDoorPbClose bit = NULL ,@DCU01_IDoorPbOpenIn bit = NULL 
,@DCU01_IDoorPbOpenOut bit = NULL ,@DCU01_IDoorReleased bit = NULL ,@DCU01_IFootStepRel bit = NULL ,@DCU01_IForcedClosDoor bit = NULL ,@DCU01_ILeafsStopDoor bit = NULL 
,@DCU01_IObstacleDoor bit = NULL ,@DCU01_IObstacleStep bit = NULL ,@DCU01_IOpenAssistDoor bit = NULL ,@DCU01_IStandstillBack bit = NULL ,@DCU02_CDoorCloseConduc bit = NULL 
,@DCU02_IDoorClosedSafe bit = NULL ,@DCU02_IDoorClRelease bit = NULL ,@DCU02_IDoorPbClose bit = NULL ,@DCU02_IDoorPbOpenIn bit = NULL ,@DCU02_IDoorPbOpenOut bit = NULL 
,@DCU02_IDoorReleased bit = NULL ,@DCU02_IFootStepRel bit = NULL ,@DCU02_IForcedClosDoor bit = NULL ,@DCU02_ILeafsStopDoor bit = NULL ,@DCU02_IObstacleDoor bit = NULL 
,@DCU02_IObstacleStep bit = NULL ,@DCU02_IOpenAssistDoor bit = NULL ,@DCU02_IStandstillBack bit = NULL ,@DCU03_CDoorCloseConduc bit = NULL ,@DCU03_IDoorClosedSafe bit = NULL 
,@DCU03_IDoorClRelease bit = NULL ,@DCU03_IDoorPbClose bit = NULL ,@DCU03_IDoorPbOpenIn bit = NULL ,@DCU03_IDoorPbOpenOut bit = NULL ,@DCU03_IDoorReleased bit = NULL 
,@DCU03_IFootStepRel bit = NULL ,@DCU03_IForcedClosDoor bit = NULL ,@DCU03_ILeafsStopDoor bit = NULL ,@DCU03_IObstacleDoor bit = NULL ,@DCU03_IObstacleStep bit = NULL 
,@DCU03_IOpenAssistDoor bit = NULL ,@DCU03_IStandstillBack bit = NULL ,@DCU04_CDoorCloseConduc bit = NULL ,@DCU04_IDoorClosedSafe bit = NULL ,@DCU04_IDoorClRelease bit = NULL 
,@DCU04_IDoorPbClose bit = NULL ,@DCU04_IDoorPbOpenIn bit = NULL ,@DCU04_IDoorPbOpenOut bit = NULL ,@DCU04_IDoorReleased bit = NULL ,@DCU04_IFootStepRel bit = NULL 
,@DCU04_IForcedClosDoor bit = NULL ,@DCU04_ILeafsStopDoor bit = NULL ,@DCU04_IObstacleDoor bit = NULL ,@DCU04_IObstacleStep bit = NULL ,@DCU04_IOpenAssistDoor bit = NULL 
,@DCU04_IStandstillBack bit = NULL ,@DCU05_CDoorCloseConduc bit = NULL ,@DCU05_IDoorClosedSafe bit = NULL ,@DCU05_IDoorClRelease bit = NULL ,@DCU05_IDoorPbClose bit = NULL 
,@DCU05_IDoorPbOpenIn bit = NULL ,@DCU05_IDoorPbOpenOut bit = NULL ,@DCU05_IDoorReleased bit = NULL ,@DCU05_IFootStepRel bit = NULL ,@DCU05_IForcedClosDoor bit = NULL 
,@DCU05_ILeafsStopDoor bit = NULL ,@DCU05_IObstacleDoor bit = NULL ,@DCU05_IObstacleStep bit = NULL ,@DCU05_IOpenAssistDoor bit = NULL ,@DCU05_IStandstillBack bit = NULL 
,@DCU06_CDoorCloseConduc bit = NULL ,@DCU06_IDoorClosedSafe bit = NULL ,@DCU06_IDoorClRelease bit = NULL ,@DCU06_IDoorPbClose bit = NULL ,@DCU06_IDoorPbOpenIn bit = NULL 
,@DCU06_IDoorPbOpenOut bit = NULL ,@DCU06_IDoorReleased bit = NULL ,@DCU06_IFootStepRel bit = NULL ,@DCU06_IForcedClosDoor bit = NULL ,@DCU06_ILeafsStopDoor bit = NULL 
,@DCU06_IObstacleDoor bit = NULL ,@DCU06_IObstacleStep bit = NULL ,@DCU06_IOpenAssistDoor bit = NULL ,@DCU06_IStandstillBack bit = NULL ,@DCU07_CDoorCloseConduc bit = NULL 
,@DCU07_IDoorClosedSafe bit = NULL ,@DCU07_IDoorClRelease bit = NULL ,@DCU07_IDoorPbClose bit = NULL ,@DCU07_IDoorPbOpenIn bit = NULL ,@DCU07_IDoorPbOpenOut bit = NULL 
,@DCU07_IDoorReleased bit = NULL ,@DCU07_IFootStepRel bit = NULL ,@DCU07_IForcedClosDoor bit = NULL ,@DCU07_ILeafsStopDoor bit = NULL ,@DCU07_IObstacleDoor bit = NULL 
,@DCU07_IObstacleStep bit = NULL ,@DCU07_IOpenAssistDoor bit = NULL ,@DCU07_IStandstillBack bit = NULL ,@DCU08_CDoorCloseConduc bit = NULL ,@DCU08_IDoorClosedSafe bit = NULL 
,@DCU08_IDoorClRelease bit = NULL ,@DCU08_IDoorPbClose bit = NULL ,@DCU08_IDoorPbOpenIn bit = NULL ,@DCU08_IDoorPbOpenOut bit = NULL ,@DCU08_IDoorReleased bit = NULL 
,@DCU08_IFootStepRel bit = NULL ,@DCU08_IForcedClosDoor bit = NULL ,@DCU08_ILeafsStopDoor bit = NULL ,@DCU08_IObstacleDoor bit = NULL ,@DCU08_IObstacleStep bit = NULL 
,@DCU08_IOpenAssistDoor bit = NULL ,@DCU08_IStandstillBack bit = NULL ,@DCU09_CDoorCloseConduc bit = NULL ,@DCU09_IDoorClosedSafe bit = NULL ,@DCU09_IDoorClRelease bit = NULL 
,@DCU09_IDoorPbClose bit = NULL ,@DCU09_IDoorPbOpenIn bit = NULL ,@DCU09_IDoorPbOpenOut bit = NULL ,@DCU09_IDoorReleased bit = NULL ,@DCU09_IFootStepRel bit = NULL 
,@DCU09_IForcedClosDoor bit = NULL ,@DCU09_ILeafsStopDoor bit = NULL ,@DCU09_IObstacleDoor bit = NULL ,@DCU09_IObstacleStep bit = NULL ,@DCU09_IOpenAssistDoor bit = NULL 
,@DCU09_IStandstillBack bit = NULL ,@DCU10_CDoorCloseConduc bit = NULL ,@DCU10_IDoorClosedSafe bit = NULL ,@DCU10_IDoorClRelease bit = NULL ,@DCU10_IDoorPbClose bit = NULL 
,@DCU10_IDoorPbOpenIn bit = NULL ,@DCU10_IDoorPbOpenOut bit = NULL ,@DCU10_IDoorReleased bit = NULL ,@DCU10_IFootStepRel bit = NULL ,@DCU10_IForcedClosDoor bit = NULL 
,@DCU10_ILeafsStopDoor bit = NULL ,@DCU10_IObstacleDoor bit = NULL ,@DCU10_IObstacleStep bit = NULL ,@DCU10_IOpenAssistDoor bit = NULL ,@DCU10_IStandstillBack bit = NULL 
,@DCU11_CDoorCloseConduc bit = NULL ,@DCU11_IDoorClosedSafe bit = NULL ,@DCU11_IDoorClRelease bit = NULL ,@DCU11_IDoorPbClose bit = NULL ,@DCU11_IDoorPbOpenIn bit = NULL 
,@DCU11_IDoorPbOpenOut bit = NULL ,@DCU11_IDoorReleased bit = NULL ,@DCU11_IFootStepRel bit = NULL ,@DCU11_IForcedClosDoor bit = NULL ,@DCU11_ILeafsStopDoor bit = NULL 
,@DCU11_IObstacleDoor bit = NULL ,@DCU11_IObstacleStep bit = NULL ,@DCU11_IOpenAssistDoor bit = NULL ,@DCU11_IStandstillBack bit = NULL ,@DCU12_CDoorCloseConduc bit = NULL 
,@DCU12_IDoorClosedSafe bit = NULL ,@DCU12_IDoorClRelease bit = NULL ,@DCU12_IDoorPbClose bit = NULL ,@DCU12_IDoorPbOpenIn bit = NULL ,@DCU12_IDoorPbOpenOut bit = NULL 
,@DCU12_IDoorReleased bit = NULL ,@DCU12_IFootStepRel bit = NULL ,@DCU12_IForcedClosDoor bit = NULL ,@DCU12_ILeafsStopDoor bit = NULL ,@DCU12_IObstacleDoor bit = NULL 
,@DCU12_IObstacleStep bit = NULL ,@DCU12_IOpenAssistDoor bit = NULL ,@DCU12_IStandstillBack bit = NULL ,@DCU13_CDoorCloseConduc bit = NULL ,@DCU13_IDoorClosedSafe bit = NULL 
,@DCU13_IDoorClRelease bit = NULL ,@DCU13_IDoorPbClose bit = NULL ,@DCU13_IDoorPbOpenIn bit = NULL ,@DCU13_IDoorPbOpenOut bit = NULL ,@DCU13_IDoorReleased bit = NULL 
,@DCU13_IFootStepRel bit = NULL ,@DCU13_IForcedClosDoor bit = NULL ,@DCU13_ILeafsStopDoor bit = NULL ,@DCU13_IObstacleDoor bit = NULL ,@DCU13_IObstacleStep bit = NULL 
,@DCU13_IOpenAssistDoor bit = NULL ,@DCU13_IStandstillBack bit = NULL ,@DCU14_CDoorCloseConduc bit = NULL ,@DCU14_IDoorClosedSafe bit = NULL ,@DCU14_IDoorClRelease bit = NULL 
,@DCU14_IDoorPbClose bit = NULL ,@DCU14_IDoorPbOpenIn bit = NULL ,@DCU14_IDoorPbOpenOut bit = NULL ,@DCU14_IDoorReleased bit = NULL ,@DCU14_IFootStepRel bit = NULL 
,@DCU14_IForcedClosDoor bit = NULL ,@DCU14_ILeafsStopDoor bit = NULL ,@DCU14_IObstacleDoor bit = NULL ,@DCU14_IObstacleStep bit = NULL ,@DCU14_IOpenAssistDoor bit = NULL 
,@DCU14_IStandstillBack bit = NULL ,@DCU15_CDoorCloseConduc bit = NULL ,@DCU15_IDoorClosedSafe bit = NULL ,@DCU15_IDoorClRelease bit = NULL ,@DCU15_IDoorPbClose bit = NULL 
,@DCU15_IDoorPbOpenIn bit = NULL ,@DCU15_IDoorPbOpenOut bit = NULL ,@DCU15_IDoorReleased bit = NULL ,@DCU15_IFootStepRel bit = NULL ,@DCU15_IForcedClosDoor bit = NULL 
,@DCU15_ILeafsStopDoor bit = NULL ,@DCU15_IObstacleDoor bit = NULL ,@DCU15_IObstacleStep bit = NULL ,@DCU15_IOpenAssistDoor bit = NULL ,@DCU15_IStandstillBack bit = NULL 
,@DCU16_CDoorCloseConduc bit = NULL ,@DCU16_IDoorClosedSafe bit = NULL ,@DCU16_IDoorClRelease bit = NULL ,@DCU16_IDoorPbClose bit = NULL ,@DCU16_IDoorPbOpenIn bit = NULL 
,@DCU16_IDoorPbOpenOut bit = NULL ,@DCU16_IDoorReleased bit = NULL ,@DCU16_IFootStepRel bit = NULL ,@DCU16_IForcedClosDoor bit = NULL ,@DCU16_ILeafsStopDoor bit = NULL 
,@DCU16_IObstacleDoor bit = NULL ,@DCU16_IObstacleStep bit = NULL ,@DCU16_IOpenAssistDoor bit = NULL ,@DCU16_IStandstillBack bit = NULL ,@DCU17_CDoorCloseConduc bit = NULL 
,@DCU17_IDoorClosedSafe bit = NULL ,@DCU17_IDoorClRelease bit = NULL ,@DCU17_IDoorPbClose bit = NULL ,@DCU17_IDoorPbOpenIn bit = NULL ,@DCU17_IDoorPbOpenOut bit = NULL 
,@DCU17_IDoorReleased bit = NULL ,@DCU17_IFootStepRel bit = NULL ,@DCU17_IForcedClosDoor bit = NULL ,@DCU17_ILeafsStopDoor bit = NULL ,@DCU17_IObstacleDoor bit = NULL 
,@DCU17_IObstacleStep bit = NULL ,@DCU17_IOpenAssistDoor bit = NULL ,@DCU17_IStandstillBack bit = NULL ,@DCU18_CDoorCloseConduc bit = NULL ,@DCU18_IDoorClosedSafe bit = NULL 
,@DCU18_IDoorClRelease bit = NULL ,@DCU18_IDoorPbClose bit = NULL ,@DCU18_IDoorPbOpenIn bit = NULL ,@DCU18_IDoorPbOpenOut bit = NULL ,@DCU18_IDoorReleased bit = NULL 
,@DCU18_IFootStepRel bit = NULL ,@DCU18_IForcedClosDoor bit = NULL ,@DCU18_ILeafsStopDoor bit = NULL ,@DCU18_IObstacleDoor bit = NULL ,@DCU18_IObstacleStep bit = NULL 
,@DCU18_IOpenAssistDoor bit = NULL ,@DCU18_IStandstillBack bit = NULL ,@DCU19_CDoorCloseConduc bit = NULL ,@DCU19_IDoorClosedSafe bit = NULL ,@DCU19_IDoorClRelease bit = NULL 
,@DCU19_IDoorPbClose bit = NULL ,@DCU19_IDoorPbOpenIn bit = NULL ,@DCU19_IDoorPbOpenOut bit = NULL ,@DCU19_IDoorReleased bit = NULL ,@DCU19_IFootStepRel bit = NULL 
,@DCU19_IForcedClosDoor bit = NULL ,@DCU19_ILeafsStopDoor bit = NULL ,@DCU19_IObstacleDoor bit = NULL ,@DCU19_IObstacleStep bit = NULL ,@DCU19_IOpenAssistDoor bit = NULL 
,@DCU19_IStandstillBack bit = NULL ,@DCU20_CDoorCloseConduc bit = NULL ,@DCU20_IDoorClosedSafe bit = NULL ,@DCU20_IDoorClRelease bit = NULL ,@DCU20_IDoorPbClose bit = NULL 
,@DCU20_IDoorPbOpenIn bit = NULL ,@DCU20_IDoorPbOpenOut bit = NULL ,@DCU20_IDoorReleased bit = NULL ,@DCU20_IFootStepRel bit = NULL ,@DCU20_IForcedClosDoor bit = NULL 
,@DCU20_ILeafsStopDoor bit = NULL ,@DCU20_IObstacleDoor bit = NULL ,@DCU20_IObstacleStep bit = NULL ,@DCU20_IOpenAssistDoor bit = NULL ,@DCU20_IStandstillBack bit = NULL 
,@DRS_CDisDoorRelease_4_1 bit = NULL ,@DRS_CDisDoorRelease_4_10 bit = NULL ,@DRS_CDisDoorRelease_4_11 bit = NULL ,@DRS_CDisDoorRelease_4_12 bit = NULL ,@DRS_CDisDoorRelease_4_13 bit = NULL 
,@DRS_CDisDoorRelease_4_14 bit = NULL ,@DRS_CDisDoorRelease_4_15 bit = NULL ,@DRS_CDisDoorRelease_4_16 bit = NULL ,@DRS_CDisDoorRelease_4_17 bit = NULL ,@DRS_CDisDoorRelease_4_18 bit = NULL 
,@DRS_CDisDoorRelease_4_19 bit = NULL ,@DRS_CDisDoorRelease_4_2 bit = NULL ,@DRS_CDisDoorRelease_4_20 bit = NULL ,@DRS_CDisDoorRelease_4_3 bit = NULL ,@DRS_CDisDoorRelease_4_4 bit = NULL 
,@DRS_CDisDoorRelease_4_5 bit = NULL ,@DRS_CDisDoorRelease_4_6 bit = NULL ,@DRS_CDisDoorRelease_4_7 bit = NULL ,@DRS_CDisDoorRelease_4_8 bit = NULL ,@DRS_CDisDoorRelease_4_9 bit = NULL 
,@DRS_CDoorClosAutDis_4_1 bit = NULL ,@DRS_CDoorClosAutDis_4_10 bit = NULL ,@DRS_CDoorClosAutDis_4_11 bit = NULL ,@DRS_CDoorClosAutDis_4_12 bit = NULL ,@DRS_CDoorClosAutDis_4_13 bit = NULL 
,@DRS_CDoorClosAutDis_4_14 bit = NULL ,@DRS_CDoorClosAutDis_4_15 bit = NULL ,@DRS_CDoorClosAutDis_4_16 bit = NULL ,@DRS_CDoorClosAutDis_4_17 bit = NULL ,@DRS_CDoorClosAutDis_4_18 bit = NULL 
,@DRS_CDoorClosAutDis_4_19 bit = NULL ,@DRS_CDoorClosAutDis_4_2 bit = NULL ,@DRS_CDoorClosAutDis_4_20 bit = NULL ,@DRS_CDoorClosAutDis_4_3 bit = NULL ,@DRS_CDoorClosAutDis_4_4 bit = NULL 
,@DRS_CDoorClosAutDis_4_5 bit = NULL ,@DRS_CDoorClosAutDis_4_6 bit = NULL ,@DRS_CDoorClosAutDis_4_7 bit = NULL ,@DRS_CDoorClosAutDis_4_8 bit = NULL ,@DRS_CDoorClosAutDis_4_9 bit = NULL 
,@DRS_CForcedClosDoor_4_1 bit = NULL ,@DRS_CForcedClosDoor_4_10 bit = NULL ,@DRS_CForcedClosDoor_4_11 bit = NULL ,@DRS_CForcedClosDoor_4_12 bit = NULL ,@DRS_CForcedClosDoor_4_13 bit = NULL 
,@DRS_CForcedClosDoor_4_14 bit = NULL ,@DRS_CForcedClosDoor_4_15 bit = NULL ,@DRS_CForcedClosDoor_4_16 bit = NULL ,@DRS_CForcedClosDoor_4_17 bit = NULL ,@DRS_CForcedClosDoor_4_18 bit = NULL 
,@DRS_CForcedClosDoor_4_19 bit = NULL ,@DRS_CForcedClosDoor_4_2 bit = NULL ,@DRS_CForcedClosDoor_4_20 bit = NULL ,@DRS_CForcedClosDoor_4_3 bit = NULL ,@DRS_CForcedClosDoor_4_4 bit = NULL 
,@DRS_CForcedClosDoor_4_5 bit = NULL ,@DRS_CForcedClosDoor_4_6 bit = NULL ,@DRS_CForcedClosDoor_4_7 bit = NULL ,@DRS_CForcedClosDoor_4_8 bit = NULL ,@DRS_CForcedClosDoor_4_9 bit = NULL 
,@DRS_CTestModeReq_M_4_1 bit = NULL ,@DRS_CTestModeReq_M_4_10 bit = NULL ,@DRS_CTestModeReq_M_4_11 bit = NULL ,@DRS_CTestModeReq_M_4_12 bit = NULL ,@DRS_CTestModeReq_M_4_13 bit = NULL 
,@DRS_CTestModeReq_M_4_14 bit = NULL ,@DRS_CTestModeReq_M_4_15 bit = NULL ,@DRS_CTestModeReq_M_4_16 bit = NULL ,@DRS_CTestModeReq_M_4_17 bit = NULL ,@DRS_CTestModeReq_M_4_18 bit = NULL 
,@DRS_CTestModeReq_M_4_19 bit = NULL ,@DRS_CTestModeReq_M_4_2 bit = NULL ,@DRS_CTestModeReq_M_4_20 bit = NULL ,@DRS_CTestModeReq_M_4_3 bit = NULL ,@DRS_CTestModeReq_M_4_4 bit = NULL 
,@DRS_CTestModeReq_M_4_5 bit = NULL ,@DRS_CTestModeReq_M_4_6 bit = NULL ,@DRS_CTestModeReq_M_4_7 bit = NULL ,@DRS_CTestModeReq_M_4_8 bit = NULL ,@DRS_CTestModeReq_M_4_9 bit = NULL 
,@MIO_I1DoorRelLe_4_1 bit = NULL ,@MIO_I1DoorRelLe_4_10 bit = NULL ,@MIO_I1DoorRelLe_4_11 bit = NULL ,@MIO_I1DoorRelLe_4_12 bit = NULL ,@MIO_I1DoorRelLe_4_13 bit = NULL 
,@MIO_I1DoorRelLe_4_14 bit = NULL ,@MIO_I1DoorRelLe_4_15 bit = NULL ,@MIO_I1DoorRelLe_4_16 bit = NULL ,@MIO_I1DoorRelLe_4_17 bit = NULL ,@MIO_I1DoorRelLe_4_18 bit = NULL 
,@MIO_I1DoorRelLe_4_19 bit = NULL ,@MIO_I1DoorRelLe_4_2 bit = NULL ,@MIO_I1DoorRelLe_4_20 bit = NULL ,@MIO_I1DoorRelLe_4_3 bit = NULL ,@MIO_I1DoorRelLe_4_4 bit = NULL 
,@MIO_I1DoorRelLe_4_5 bit = NULL ,@MIO_I1DoorRelLe_4_6 bit = NULL ,@MIO_I1DoorRelLe_4_7 bit = NULL ,@MIO_I1DoorRelLe_4_8 bit = NULL ,@MIO_I1DoorRelLe_4_9 bit = NULL 
,@MIO_I1DoorRelRi_4_1 bit = NULL ,@MIO_I1DoorRelRi_4_10 bit = NULL ,@MIO_I1DoorRelRi_4_11 bit = NULL ,@MIO_I1DoorRelRi_4_12 bit = NULL ,@MIO_I1DoorRelRi_4_13 bit = NULL 
,@MIO_I1DoorRelRi_4_14 bit = NULL ,@MIO_I1DoorRelRi_4_15 bit = NULL ,@MIO_I1DoorRelRi_4_16 bit = NULL ,@MIO_I1DoorRelRi_4_17 bit = NULL ,@MIO_I1DoorRelRi_4_18 bit = NULL 
,@MIO_I1DoorRelRi_4_19 bit = NULL ,@MIO_I1DoorRelRi_4_2 bit = NULL ,@MIO_I1DoorRelRi_4_20 bit = NULL ,@MIO_I1DoorRelRi_4_3 bit = NULL ,@MIO_I1DoorRelRi_4_4 bit = NULL 
,@MIO_I1DoorRelRi_4_5 bit = NULL ,@MIO_I1DoorRelRi_4_6 bit = NULL ,@MIO_I1DoorRelRi_4_7 bit = NULL ,@MIO_I1DoorRelRi_4_8 bit = NULL ,@MIO_I1DoorRelRi_4_9 bit = NULL 
,@MIO_I2DoorRelLe_4_1 bit = NULL ,@MIO_I2DoorRelLe_4_10 bit = NULL ,@MIO_I2DoorRelLe_4_11 bit = NULL ,@MIO_I2DoorRelLe_4_12 bit = NULL ,@MIO_I2DoorRelLe_4_13 bit = NULL 
,@MIO_I2DoorRelLe_4_14 bit = NULL ,@MIO_I2DoorRelLe_4_15 bit = NULL ,@MIO_I2DoorRelLe_4_16 bit = NULL ,@MIO_I2DoorRelLe_4_17 bit = NULL ,@MIO_I2DoorRelLe_4_18 bit = NULL 
,@MIO_I2DoorRelLe_4_19 bit = NULL ,@MIO_I2DoorRelLe_4_2 bit = NULL ,@MIO_I2DoorRelLe_4_20 bit = NULL ,@MIO_I2DoorRelLe_4_3 bit = NULL ,@MIO_I2DoorRelLe_4_4 bit = NULL 
,@MIO_I2DoorRelLe_4_5 bit = NULL ,@MIO_I2DoorRelLe_4_6 bit = NULL ,@MIO_I2DoorRelLe_4_7 bit = NULL ,@MIO_I2DoorRelLe_4_8 bit = NULL ,@MIO_I2DoorRelLe_4_9 bit = NULL 
,@MIO_I2DoorRelRi_4_1 bit = NULL ,@MIO_I2DoorRelRi_4_10 bit = NULL ,@MIO_I2DoorRelRi_4_11 bit = NULL ,@MIO_I2DoorRelRi_4_12 bit = NULL ,@MIO_I2DoorRelRi_4_13 bit = NULL 
,@MIO_I2DoorRelRi_4_14 bit = NULL ,@MIO_I2DoorRelRi_4_15 bit = NULL ,@MIO_I2DoorRelRi_4_16 bit = NULL ,@MIO_I2DoorRelRi_4_17 bit = NULL ,@MIO_I2DoorRelRi_4_18 bit = NULL 
,@MIO_I2DoorRelRi_4_19 bit = NULL ,@MIO_I2DoorRelRi_4_2 bit = NULL ,@MIO_I2DoorRelRi_4_20 bit = NULL ,@MIO_I2DoorRelRi_4_3 bit = NULL ,@MIO_I2DoorRelRi_4_4 bit = NULL 
,@MIO_I2DoorRelRi_4_5 bit = NULL ,@MIO_I2DoorRelRi_4_6 bit = NULL ,@MIO_I2DoorRelRi_4_7 bit = NULL ,@MIO_I2DoorRelRi_4_8 bit = NULL ,@MIO_I2DoorRelRi_4_9 bit = NULL 
,@MIO_IDoorLoopBypa1_4_1 bit = NULL ,@MIO_IDoorLoopBypa1_4_10 bit = NULL ,@MIO_IDoorLoopBypa1_4_11 bit = NULL ,@MIO_IDoorLoopBypa1_4_12 bit = NULL ,@MIO_IDoorLoopBypa1_4_13 bit = NULL 
,@MIO_IDoorLoopBypa1_4_14 bit = NULL ,@MIO_IDoorLoopBypa1_4_15 bit = NULL ,@MIO_IDoorLoopBypa1_4_16 bit = NULL ,@MIO_IDoorLoopBypa1_4_17 bit = NULL ,@MIO_IDoorLoopBypa1_4_18 bit = NULL 
,@MIO_IDoorLoopBypa1_4_19 bit = NULL ,@MIO_IDoorLoopBypa1_4_2 bit = NULL ,@MIO_IDoorLoopBypa1_4_20 bit = NULL ,@MIO_IDoorLoopBypa1_4_3 bit = NULL ,@MIO_IDoorLoopBypa1_4_4 bit = NULL 
,@MIO_IDoorLoopBypa1_4_5 bit = NULL ,@MIO_IDoorLoopBypa1_4_6 bit = NULL ,@MIO_IDoorLoopBypa1_4_7 bit = NULL ,@MIO_IDoorLoopBypa1_4_8 bit = NULL ,@MIO_IDoorLoopBypa1_4_9 bit = NULL 
,@MIO_IDoorLoopBypa2_4_1 bit = NULL ,@MIO_IDoorLoopBypa2_4_10 bit = NULL ,@MIO_IDoorLoopBypa2_4_11 bit = NULL ,@MIO_IDoorLoopBypa2_4_12 bit = NULL ,@MIO_IDoorLoopBypa2_4_13 bit = NULL 
,@MIO_IDoorLoopBypa2_4_14 bit = NULL ,@MIO_IDoorLoopBypa2_4_15 bit = NULL ,@MIO_IDoorLoopBypa2_4_16 bit = NULL ,@MIO_IDoorLoopBypa2_4_17 bit = NULL ,@MIO_IDoorLoopBypa2_4_18 bit = NULL 
,@MIO_IDoorLoopBypa2_4_19 bit = NULL ,@MIO_IDoorLoopBypa2_4_2 bit = NULL ,@MIO_IDoorLoopBypa2_4_20 bit = NULL ,@MIO_IDoorLoopBypa2_4_3 bit = NULL ,@MIO_IDoorLoopBypa2_4_4 bit = NULL 
,@MIO_IDoorLoopBypa2_4_5 bit = NULL ,@MIO_IDoorLoopBypa2_4_6 bit = NULL ,@MIO_IDoorLoopBypa2_4_7 bit = NULL ,@MIO_IDoorLoopBypa2_4_8 bit = NULL ,@MIO_IDoorLoopBypa2_4_9 bit = NULL 
,@MIO_IDoorLoopCl1_4_1 bit = NULL ,@MIO_IDoorLoopCl1_4_10 bit = NULL ,@MIO_IDoorLoopCl1_4_11 bit = NULL ,@MIO_IDoorLoopCl1_4_12 bit = NULL ,@MIO_IDoorLoopCl1_4_13 bit = NULL 
,@MIO_IDoorLoopCl1_4_14 bit = NULL ,@MIO_IDoorLoopCl1_4_15 bit = NULL ,@MIO_IDoorLoopCl1_4_16 bit = NULL ,@MIO_IDoorLoopCl1_4_17 bit = NULL ,@MIO_IDoorLoopCl1_4_18 bit = NULL 
,@MIO_IDoorLoopCl1_4_19 bit = NULL ,@MIO_IDoorLoopCl1_4_2 bit = NULL ,@MIO_IDoorLoopCl1_4_20 bit = NULL ,@MIO_IDoorLoopCl1_4_3 bit = NULL ,@MIO_IDoorLoopCl1_4_4 bit = NULL 
,@MIO_IDoorLoopCl1_4_5 bit = NULL ,@MIO_IDoorLoopCl1_4_6 bit = NULL ,@MIO_IDoorLoopCl1_4_7 bit = NULL ,@MIO_IDoorLoopCl1_4_8 bit = NULL ,@MIO_IDoorLoopCl1_4_9 bit = NULL 
,@MIO_IDoorLoopCl2_4_1 bit = NULL ,@MIO_IDoorLoopCl2_4_10 bit = NULL ,@MIO_IDoorLoopCl2_4_11 bit = NULL ,@MIO_IDoorLoopCl2_4_12 bit = NULL ,@MIO_IDoorLoopCl2_4_13 bit = NULL 
,@MIO_IDoorLoopCl2_4_14 bit = NULL ,@MIO_IDoorLoopCl2_4_15 bit = NULL ,@MIO_IDoorLoopCl2_4_16 bit = NULL ,@MIO_IDoorLoopCl2_4_17 bit = NULL ,@MIO_IDoorLoopCl2_4_18 bit = NULL 
,@MIO_IDoorLoopCl2_4_19 bit = NULL ,@MIO_IDoorLoopCl2_4_2 bit = NULL ,@MIO_IDoorLoopCl2_4_20 bit = NULL ,@MIO_IDoorLoopCl2_4_3 bit = NULL ,@MIO_IDoorLoopCl2_4_4 bit = NULL 
,@MIO_IDoorLoopCl2_4_5 bit = NULL ,@MIO_IDoorLoopCl2_4_6 bit = NULL ,@MIO_IDoorLoopCl2_4_7 bit = NULL ,@MIO_IDoorLoopCl2_4_8 bit = NULL ,@MIO_IDoorLoopCl2_4_9 bit = NULL 
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT

DECLARE @Id bigint = null
,@unitID int

SELECT @unitId = ID FROM unit where UnitNumber = @UnitNumber

SELECT @id = ID FROM ChannelValue where TimeStamp = @Timestamp and UnitId = @unitID

IF @id IS NULL
BEGIN


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[col9],[col10],[col11],[col12],[col13],[col14],[col15],[col16]
,[col17],[col18],[col19],[col20],[col21],[col22],[col23],[col24],[col25],[col26],[col27],[col28],[col29],[col30],[col31],[col32]
,[col33],[col34],[col35],[col36],[col37],[col38],[col39],[col40],[col41],[col42],[col43],[col44],[col45],[col46],[col47],[col48]
,[col49],[col50],[col51],[col52],[col53],[col54],[col55],[col56],[col57],[col58],[col59],[col60],[col61],[col62],[col63],[col64]
,[col65],[col66],[col67],[col68],[col69],[col70],[col71],[col72],[col73],[col74],[col75],[col76],[col77],[col78],[col79],[col80]
,[col81],[col82],[col83],[col84],[col85],[col86],[col87],[col88],[col89],[col90],[col91],[col92],[col93],[col94],[col95],[col96]
,[col97],[col98],[col99],[col100],[col101],[col102],[col103],[col104],[col105]
,[col106],[col107],[col108],[col109],[col110],[col111],[col112]
,[col113],[col114],[col115],[col116],[col117],[col118],[col119]
,[col120],[col121],[col122],[col123],[col124],[col125],[col126]
,[col127],[col128],[col129],[col130],[col131],[col132],[col133]
,[col134],[col135],[col136],[col137],[col138],[col139],[col140]
,[col141],[col142],[col143],[col144],[col145],[col146],[col147]
,[col148],[col149],[col150],[col151],[col152],[col153],[col154]
,[col155],[col156],[col157],[col158],[col159],[col160],[col161]
,[col162],[col163],[col164],[col165],[col166],[col167],[col168]
,[col169],[col170],[col171],[col172],[col173],[col174],[col175]
,[col176],[col177],[col178],[col179],[col180],[col181],[col182]
,[col183],[col184],[col185],[col186],[col187],[col188],[col189]
,[col190],[col191],[col192],[col193],[col194],[col195],[col196]
,[col197],[col198],[col199],[col200],[col201],[col202],[col203]
,[col204],[col205],[col206],[col207],[col208],[col209],[col210]
,[col211],[col212],[col213],[col214],[col215],[col216],[col217]
,[col218],[col219],[col220],[col221],[col222],[col223],[col224]
,[col225],[col226],[col227],[col228],[col229],[col230],[col231]
,[col232],[col233],[col234],[col235],[col236],[col237],[col238]
,[col239],[col240],[col241],[col242],[col243],[col244],[col245]
,[col246],[col247],[col248],[col249],[col250],[col251],[col252]
,[col253],[col254],[col255],[col256],[col257],[col258],[col259]
,[col260],[col261],[col262],[col263],[col264],[col265],[col266]
,[col267],[col268],[col269],[col270],[col271],[col272],[col273]
,[col274],[col275],[col276],[col277],[col278],[col279],[col280]
,[col281],[col282],[col283],[col284],[col285],[col286],[col287]
,[col288],[col289],[col290],[col291],[col292],[col293],[col294]
,[col295],[col296],[col297],[col298],[col299],[col300],[col301]
,[col302],[col303],[col304],[col305],[col306],[col307],[col308]
,[col309],[col310],[col311],[col312],[col313],[col314],[col315]
,[col316],[col317],[col318],[col319],[col320],[col321],[col322]
,[col323],[col324],[col325],[col326],[col327],[col328],[col329]
,[col330],[col331],[col332],[col333],[col334],[col335],[col336]
,[col337],[col338],[col339],[col340],[col341],[col342],[col343]
,[col344],[col345],[col346],[col347],[col348],[col349],[col350]
,[col351],[col352],[col353],[col354],[col355],[col356],[col357]
,[col358],[col359],[col360],[col361],[col362],[col363],[col364]
,[col365],[col366],[col367],[col368],[col369],[col370],[col371]
,[col372],[col373],[col374],[col375],[col376],[col377],[col378]
,[col379],[col380],[col381],[col382],[col383],[col384],[col385]
,[col386],[col387],[col388],[col389],[col390],[col391],[col392]
,[col393],[col394],[col395],[col396],[col397],[col398],[col399]
,[col400],[col401],[col402],[col403],[col404],[col405],[col406]
,[col407],[col408],[col409],[col410],[col411],[col412],[col413]
,[col414],[col415],[col416],[col417],[col418],[col419],[col420]
,[col421],[col422],[col423],[col424],[col425],[col426],[col427]
,[col428],[col429],[col430],[col431],[col432],[col433],[col434]
,[col435],[col436],[col437],[col438],[col439],[col440],[col441]
,[col442],[col443],[col444],[col445],[col446],[col447],[col448]
,[col449],[col450],[col451],[col452],[col453],[col454],[col455]
,[col456],[col457],[col458],[col459],[col460],[col461],[col462]
,[col463],[col464],[col465],[col466],[col467],[col468],[col469]
,[col470],[col471],[col472],[col473],[col474],[col475],[col476]
,[col477],[col478],[col479],[col480],[col481],[col482],[col483]
,[col484],[col485],[col486],[col487],[col488],[col489],[col490]
,[col491],[col492],[col493],[col494],[col495],[col496],[col497]
,[col498],[col499],[col500],[col501],[col502],[col503],[col504]
,[col505],[col506],[col507],[col508],[col509],[col510],[col511]
,[col512],[col513],[col514],[col515],[col516],[col517],[col518]
,[col519],[col520],[col521],[col522],[col523],[col524],[col525]
,[col526],[col527],[col528],[col529],[col530],[col531],[col532]
,[col533],[col534],[col535],[col536],[col537],[col538],[col539]
,[col540],[col541],[col542],[col543],[col544],[col545],[col546]
,[col547],[col548],[col549],[col550],[col551],[col552],[col553]
,[col554],[col555],[col556],[col557],[col558],[col559],[col560]
,[col561],[col562],[col563],[col564],[col565],[col566],[col567]
,[col568],[col569],[col570],[col571],[col572],[col573],[col574]
,[col575],[col576],[col577],[col578],[col579],[col580],[col581]
,[col582],[col583],[col584],[col585],[col586],[col587],[col588]
,[col589],[col590],[col591],[col592],[col593],[col594],[col595]
,[col596],[col597],[col598],[col599],[col600],[col601],[col602]
,[col603],[col604],[col605],[col606],[col607],[col608],[col609]
,[col610],[col611],[col612],[col613],[col614],[col615],[col616]
,[col617],[col618],[col619],[col620],[col621],[col622],[col623]
,[col624],[col625],[col626],[col627],[col628],[col629],[col630]
,[col631],[col632],[col633],[col634],[col635],[col636],[col637]
,[col638],[col639],[col640],[col641],[col642],[col643],[col644]
,[col645],[col646],[col647],[col648],[col649],[col650],[col651]
,[col652],[col653],[col654],[col655],[col656],[col657],[col658]
,[col659],[col660],[col661],[col662],[col663]
,[Col664],[Col665],[Col666],[Col667],[Col668],[Col669],[Col670]
,[Col671],[Col672],[Col673],[Col674],[Col675],[Col676],[Col677]
,[Col678],[Col679],[Col680],[Col681],[Col682],[Col683],[Col684]
,[Col685],[Col686],[Col687],[Col688],[Col689],[Col690],[Col691]
,[Col692],[Col693],[Col694],[Col695],[Col696],[Col697],[Col698]
,[Col699],[Col700],[Col701],[Col702],[Col703],[Col704],[Col705]
,[Col706],[Col707],[Col708],[Col709],[Col710],[Col711],[Col712]
,[Col713],[Col714],[Col715]
           )
	OUTPUT inserted.ID INTO @IDs(ID) 
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@MCG_ILatitude, col1) ,ISNULL(@MCG_ILongitude, col2) ,ISNULL(@DIRECTION_CONTROL, col3) ,ISNULL(@CURRENT_SPEED, col4),ISNULL(@AUX1_CDeratingReq_1_1,col5),ISNULL(@AUX1_I3ACContactorOn_1_1, col6) 
,ISNULL(@AUX1_I3ACContactorOn_2_1, col7) ,ISNULL(@AUX1_I3ACContactorOn_2_2, col8) ,ISNULL(@AUX1_I3ACLoadEnabled_1_1, col9) ,ISNULL(@AUX1_I3ACOk_1_1, col10) ,ISNULL(@AUX1_IInputVoltageOk_1_1, col11) 
,ISNULL(@AUX1_IInverterEnabled_1_1, col12) ,ISNULL(@AUX1_ILineContactorOn_1_1, col13) ,ISNULL(@AUX1_IOutputVoltageOk_1_1, col14) ,ISNULL(@AUX1_IPreChargContOn_1_1, col15) ,ISNULL(@AUX1_ITempHeatSinkOk_1_1, col16) 
,ISNULL(@AUX2_CDeratingReq_1_2, col17) ,ISNULL(@AUX2_I3ACContactorOn_1_2, col18) ,ISNULL(@AUX2_I3ACContactorOn_2_1, col19) ,ISNULL(@AUX2_I3ACContactorOn_2_2, col20) ,ISNULL(@AUX2_I3ACLoadEnabled_1_2, col21) 
,ISNULL(@AUX2_I3ACOk_1_2, col22) ,ISNULL(@AUX2_IInputVoltageOk_1_2, col23) ,ISNULL(@AUX2_IInverterEnabled_1_2, col24) ,ISNULL(@AUX2_ILineContactorOn_1_2, col25) ,ISNULL(@AUX2_IOutputVoltageOk_1_2, col26) 
,ISNULL(@AUX2_IPreChargContOn_1_2, col27) ,ISNULL(@AUX2_ITempHeatSinkOk_1_2, col28) ,ISNULL(@AUXY_IBatCont1On, col29) ,ISNULL(@AUXY_IBatCont1On_1_1, col30) ,ISNULL(@AUXY_IBatCont1On_1_2, col31) 
,ISNULL(@AUXY_IBatCont1On_2_1, col32) ,ISNULL(@AUXY_IBatCont1On_2_2, col33) ,ISNULL(@AUXY_IBatCont1On_3_1, col34) ,ISNULL(@AUXY_IBatCont1On_3_2, col35) ,ISNULL(@AUXY_IBatCont1On_8_1, col36) 
,ISNULL(@AUXY_IBatCont1On_8_2, col37) ,ISNULL(@AUXY_IBatCont1On_8_3, col38) ,ISNULL(@AUXY_IBatCont1On_8_4, col39) ,ISNULL(@AUXY_IBatCont1On_8_5, col40) ,ISNULL(@AUXY_IBatCont1On_8_6, col41) 
,ISNULL(@AUXY_IBatCont1On_8_7, col42) ,ISNULL(@AUXY_IBatCont1On_8_8, col43) ,ISNULL(@AUXY_IBatCont1On_9, col44) ,ISNULL(@AUXY_IBatCont2On, col45) ,ISNULL(@AUXY_IBatCont2On_1_1, col46) 
,ISNULL(@AUXY_IBatCont2On_1_2, col47) ,ISNULL(@AUXY_IBatCont2On_2_1, col48) ,ISNULL(@AUXY_IBatCont2On_2_2, col49) ,ISNULL(@AUXY_IBatCont2On_3_1, col50) ,ISNULL(@AUXY_IBatCont2On_3_2, col51) 
,ISNULL(@AUXY_IBatCont2On_8_1, col52) ,ISNULL(@AUXY_IBatCont2On_8_2, col53) ,ISNULL(@AUXY_IBatCont2On_8_3, col54) ,ISNULL(@AUXY_IBatCont2On_8_4, col55) ,ISNULL(@AUXY_IBatCont2On_8_5, col56) 
,ISNULL(@AUXY_IBatCont2On_8_6, col57) ,ISNULL(@AUXY_IBatCont2On_8_7, col58) ,ISNULL(@AUXY_IBatCont2On_8_8, col59) ,ISNULL(@AUXY_IBatCont2On_9, col60) ,ISNULL(@BCHA1_I110VAuxPowSup_2_1, col61) 
,ISNULL(@BCHA1_IBattCurr_2_1, col62) ,ISNULL(@BCHA1_IBattOutputCurr_2_1, col63) ,ISNULL(@BCHA1_IChargCurrMax_2_1, col64) ,ISNULL(@BCHA1_IDcLinkVoltage_2_1, col65) ,ISNULL(@BCHA1_IExternal400Vac, col66) 
,ISNULL(@BCHA1_IExternal400Vac_1_1, col67) ,ISNULL(@BCHA1_IExternal400Vac_1_2, col68) ,ISNULL(@BCHA1_IExternal400Vac_2_1, col69) ,ISNULL(@BCHA1_IExternal400Vac_2_2, col70) ,ISNULL(@BCHA1_IExternal400Vac_3_1, col71) 
,ISNULL(@BCHA1_IExternal400Vac_3_2, col72) ,ISNULL(@BCHA1_IExternal400Vac_8_1, col73) ,ISNULL(@BCHA1_IExternal400Vac_8_2, col74) ,ISNULL(@BCHA1_IExternal400Vac_8_3, col75) ,ISNULL(@BCHA1_IExternal400Vac_8_4, col76) 
,ISNULL(@BCHA1_IExternal400Vac_8_5, col77) ,ISNULL(@BCHA1_IExternal400Vac_8_6, col78) ,ISNULL(@BCHA1_IExternal400Vac_8_7, col79) ,ISNULL(@BCHA1_IExternal400Vac_8_8, col80) ,ISNULL(@BCHA1_IExternal400Vac_9, col81) 
,ISNULL(@BCHA1_IOutputPower_2_1, col82) ,ISNULL(@BCHA1_ISpecSystNokMod_2_1, col83) ,ISNULL(@BCHA1_ISpecSystOkMode_2_1, col84) ,ISNULL(@BCHA1_ITempBattSens1_2_1, col85) ,ISNULL(@BCHA1_ITempBattSens2_2_1, col86) 
,ISNULL(@BCHA1_ITempHeatSink_2_1, col87) ,ISNULL(@BCHA2_I110VAuxPowSup_2_2, col88) ,ISNULL(@BCHA2_IBattCurr_2_2, col89) ,ISNULL(@BCHA2_IBattOutputCurr_2_2, col90) ,ISNULL(@BCHA2_IChargCurrMax_2_2, col91) 
,ISNULL(@BCHA2_IDcLinkVoltage_2_2, col92) ,ISNULL(@BCHA2_IExternal400Vac, col93) ,ISNULL(@BCHA2_IExternal400Vac_1_1, col94) ,ISNULL(@BCHA2_IExternal400Vac_1_2, col95) ,ISNULL(@BCHA2_IExternal400Vac_2_1, col96) 
,ISNULL(@BCHA2_IExternal400Vac_2_2, col97) ,ISNULL(@BCHA2_IExternal400Vac_3_1, col98) ,ISNULL(@BCHA2_IExternal400Vac_3_2, col99) ,ISNULL(@BCHA2_IExternal400Vac_8_1, col100) ,ISNULL(@BCHA2_IExternal400Vac_8_2, col101) 
,ISNULL(@BCHA2_IExternal400Vac_8_3, col102) ,ISNULL(@BCHA2_IExternal400Vac_8_4, col103) ,ISNULL(@BCHA2_IExternal400Vac_8_5, col104) ,ISNULL(@BCHA2_IExternal400Vac_8_6, col105) ,ISNULL(@BCHA2_IExternal400Vac_8_7, col106) 
,ISNULL(@BCHA2_IExternal400Vac_8_8, col107) ,ISNULL(@BCHA2_IExternal400Vac_9, col108) ,ISNULL(@BCHA2_IOutputPower_2_2, col109) ,ISNULL(@BCHA2_ISpecSystNokMod_2_2, col110) ,ISNULL(@BCHA2_ISpecSystOkMode_2_2, col111) 
,ISNULL(@BCHA2_ITempBattSens1_2_2, col112) ,ISNULL(@BCHA2_ITempBattSens2_2_2, col113) ,ISNULL(@BCHA2_ITempHeatSink_2_2, col114) ,ISNULL(@BCU1_IBrPipePressureInt_5_1, col115) ,ISNULL(@BCU1_IMainAirResPipeInt_5_1, col116) 
,ISNULL(@BCU1_IParkBrApplied_5_1, col117) ,ISNULL(@BCU1_IParkBrLocked_5_1, col118) ,ISNULL(@BCU1_IParkBrReleased_5_1, col119) ,ISNULL(@BCU2_IBrPipePressureInt_5_2, col120) ,ISNULL(@BCU2_IMainAirResPipeInt_5_2, col121) 
,ISNULL(@BCU2_IParkBrApplied_5_2, col122) ,ISNULL(@BCU2_IParkBrLocked_5_2, col123) ,ISNULL(@BCU2_IParkBrReleased_5_2, col124) ,ISNULL(@BCUm_IBrTestRun_5_1, col125) ,ISNULL(@BCUm_IBrTestRun_5_2, col126) 
,ISNULL(@BCUx_IBogie1Locked_5_1, col127) ,ISNULL(@BCUx_IBogie1Locked_5_2, col128) ,ISNULL(@BCUx_IBogie2Locked_5_1, col129) ,ISNULL(@BCUx_IBogie2Locked_5_2, col130) ,ISNULL(@BCUx_IBogie3Locked_5_1, col131) 
,ISNULL(@BCUx_IBogie3Locked_5_2, col132) ,ISNULL(@BCUx_IBogie4Locked_5_1, col133) ,ISNULL(@BCUx_IBogie4Locked_5_2, col134) ,ISNULL(@BCUx_IBogie5Locked_5_1, col135) ,ISNULL(@BCUx_IBogie5Locked_5_2, col136) 
,ISNULL(@BCUx_IBogie6Locked_5_1, col137) ,ISNULL(@BCUx_IBogie6Locked_5_2, col138) ,ISNULL(@BCUx_IBogie7Locked_5_1, col139) ,ISNULL(@BCUx_IBogie7Locked_5_2, col140) ,ISNULL(@BCUx_IWspBogie1Slide_3_1, col141) 
,ISNULL(@BCUx_IWspBogie1Slide_3_2, col142) ,ISNULL(@BCUx_IWspBogie1Slide_5_1, col143) ,ISNULL(@BCUx_IWspBogie1Slide_5_2, col144) ,ISNULL(@BCUx_IWspBogie2Slide_3_1, col145) ,ISNULL(@BCUx_IWspBogie2Slide_3_2, col146) 
,ISNULL(@BCUx_IWspBogie2Slide_5_1, col147) ,ISNULL(@BCUx_IWspBogie2Slide_5_2, col148) ,ISNULL(@BCUx_IWspBogie3Slide_3_1, col149) ,ISNULL(@BCUx_IWspBogie3Slide_3_2, col150) ,ISNULL(@BCUx_IWspBogie3Slide_5_1, col151) 
,ISNULL(@BCUx_IWspBogie3Slide_5_2, col152) ,ISNULL(@BCUx_IWspBogie4Slide_3_1, col153) ,ISNULL(@BCUx_IWspBogie4Slide_3_2, col154) ,ISNULL(@BCUx_IWspBogie4Slide_5_1, col155) ,ISNULL(@BCUx_IWspBogie4Slide_5_2, col156) 
,ISNULL(@BCUx_IWspBogie5Slide_3_1, col157) ,ISNULL(@BCUx_IWspBogie5Slide_3_2, col158) ,ISNULL(@BCUx_IWspBogie5Slide_5_1, col159) ,ISNULL(@BCUx_IWspBogie5Slide_5_2, col160) ,ISNULL(@BCUx_IWspBogie6Slide_3_1, col161) 
,ISNULL(@BCUx_IWspBogie6Slide_3_2, col162) ,ISNULL(@BCUx_IWspBogie6Slide_5_1, col163) ,ISNULL(@BCUx_IWspBogie6Slide_5_2, col164) ,ISNULL(@BCUx_IWspBogie7Slide_3_1, col165) ,ISNULL(@BCUx_IWspBogie7Slide_3_2, col166) 
,ISNULL(@BCUx_IWspBogie7Slide_5_1, col167) ,ISNULL(@BCUx_IWspBogie7Slide_5_2, col168) ,ISNULL(@CABIN, col169) ,ISNULL(@CABIN_IST, col170) ,ISNULL(@CABIN_SOLL, col171) 
,ISNULL(@CABIN_STATUS, col172) ,ISNULL(@CYCLE_COUNTER, col173) ,ISNULL(@DBC_CBCU1ModeTowing_5_1, col174) ,ISNULL(@DBC_CBCU1ModeTowing_5_2, col175) ,ISNULL(@DBC_CBCU2ModeTowing_5_1, col176) 
,ISNULL(@DBC_CBCU2ModeTowing_5_2, col177) ,ISNULL(@DBC_Cbrake_M, col178) ,ISNULL(@DBC_Cbrake_M_1_1, col179) ,ISNULL(@DBC_Cbrake_M_1_2, col180) ,ISNULL(@DBC_Cbrake_M_2_1, col181) 
,ISNULL(@DBC_Cbrake_M_2_2, col182) ,ISNULL(@DBC_Cbrake_M_3_1, col183) ,ISNULL(@DBC_Cbrake_M_3_2, col184) ,ISNULL(@DBC_Cbrake_M_4_1, col185) ,ISNULL(@DBC_Cbrake_M_4_10, col186) 
,ISNULL(@DBC_Cbrake_M_4_11, col187) ,ISNULL(@DBC_Cbrake_M_4_12, col188) ,ISNULL(@DBC_Cbrake_M_4_13, col189) ,ISNULL(@DBC_Cbrake_M_4_14, col190) ,ISNULL(@DBC_Cbrake_M_4_15, col191) 
,ISNULL(@DBC_Cbrake_M_4_16, col192) ,ISNULL(@DBC_Cbrake_M_4_17, col193) ,ISNULL(@DBC_Cbrake_M_4_18, col194) ,ISNULL(@DBC_Cbrake_M_4_19, col195) ,ISNULL(@DBC_Cbrake_M_4_2, col196) 
,ISNULL(@DBC_Cbrake_M_4_20, col197) ,ISNULL(@DBC_Cbrake_M_4_3, col198) ,ISNULL(@DBC_Cbrake_M_4_4, col199) ,ISNULL(@DBC_Cbrake_M_4_5, col200) ,ISNULL(@DBC_Cbrake_M_4_6, col201) 
,ISNULL(@DBC_Cbrake_M_4_7, col202) ,ISNULL(@DBC_Cbrake_M_4_8, col203) ,ISNULL(@DBC_Cbrake_M_4_9, col204) ,ISNULL(@DBC_Cbrake_M_5_1, col205) ,ISNULL(@DBC_Cbrake_M_5_2, col206) 
,ISNULL(@DBC_Cbrake_M_8_1, col207) ,ISNULL(@DBC_Cbrake_M_8_2, col208) ,ISNULL(@DBC_Cbrake_M_8_3, col209) ,ISNULL(@DBC_Cbrake_M_8_4, col210) ,ISNULL(@DBC_Cbrake_M_8_5, col211) 
,ISNULL(@DBC_Cbrake_M_8_6, col212) ,ISNULL(@DBC_Cbrake_M_8_7, col213) ,ISNULL(@DBC_Cbrake_M_8_8, col214) ,ISNULL(@DBC_CBrakeQuick_M, col215) ,ISNULL(@DBC_CBrakeQuick_M_1_1, col216) 
,ISNULL(@DBC_CBrakeQuick_M_1_2, col217) ,ISNULL(@DBC_CBrakeQuick_M_2_1, col218) ,ISNULL(@DBC_CBrakeQuick_M_2_2, col219) ,ISNULL(@DBC_CBrakeQuick_M_3_1, col220) ,ISNULL(@DBC_CBrakeQuick_M_3_2, col221) 
,ISNULL(@DBC_CBrakeQuick_M_4_1, col222) ,ISNULL(@DBC_CBrakeQuick_M_4_10, col223) ,ISNULL(@DBC_CBrakeQuick_M_4_11, col224) ,ISNULL(@DBC_CBrakeQuick_M_4_12, col225) ,ISNULL(@DBC_CBrakeQuick_M_4_13, col226) 
,ISNULL(@DBC_CBrakeQuick_M_4_14, col227) ,ISNULL(@DBC_CBrakeQuick_M_4_15, col228) ,ISNULL(@DBC_CBrakeQuick_M_4_16, col229) ,ISNULL(@DBC_CBrakeQuick_M_4_17, col230) ,ISNULL(@DBC_CBrakeQuick_M_4_18, col231) 
,ISNULL(@DBC_CBrakeQuick_M_4_19, col232) ,ISNULL(@DBC_CBrakeQuick_M_4_2, col233) ,ISNULL(@DBC_CBrakeQuick_M_4_20, col234) ,ISNULL(@DBC_CBrakeQuick_M_4_3, col235) ,ISNULL(@DBC_CBrakeQuick_M_4_4, col236) 
,ISNULL(@DBC_CBrakeQuick_M_4_5, col237) ,ISNULL(@DBC_CBrakeQuick_M_4_6, col238) ,ISNULL(@DBC_CBrakeQuick_M_4_7, col239) ,ISNULL(@DBC_CBrakeQuick_M_4_8, col240) ,ISNULL(@DBC_CBrakeQuick_M_4_9, col241) 
,ISNULL(@DBC_CBrakeQuick_M_5_1, col242) ,ISNULL(@DBC_CBrakeQuick_M_5_2, col243) ,ISNULL(@DBC_CBrakeQuick_M_8_1, col244) ,ISNULL(@DBC_CBrakeQuick_M_8_2, col245) ,ISNULL(@DBC_CBrakeQuick_M_8_3, col246) 
,ISNULL(@DBC_CBrakeQuick_M_8_4, col247) ,ISNULL(@DBC_CBrakeQuick_M_8_5, col248) ,ISNULL(@DBC_CBrakeQuick_M_8_6, col249) ,ISNULL(@DBC_CBrakeQuick_M_8_7, col250) ,ISNULL(@DBC_CBrakeQuick_M_8_8, col251) 
,ISNULL(@DBC_CBrakingEffortInt_3_1, col252) ,ISNULL(@DBC_CBrakingEffortInt_3_2, col253) ,ISNULL(@DBC_CBrakingEffortInt_5_1, col254) ,ISNULL(@DBC_CBrakingEffortInt_5_2, col255) ,ISNULL(@DBC_Cdrive_M, col256) 
,ISNULL(@DBC_Cdrive_M_1_1, col257) ,ISNULL(@DBC_Cdrive_M_1_2, col258) ,ISNULL(@DBC_Cdrive_M_2_1, col259) ,ISNULL(@DBC_Cdrive_M_2_2, col260) ,ISNULL(@DBC_Cdrive_M_3_1, col261) 
,ISNULL(@DBC_Cdrive_M_3_2, col262) ,ISNULL(@DBC_Cdrive_M_4_1, col263) ,ISNULL(@DBC_Cdrive_M_4_10, col264) ,ISNULL(@DBC_Cdrive_M_4_11, col265) ,ISNULL(@DBC_Cdrive_M_4_12, col266) 
,ISNULL(@DBC_Cdrive_M_4_13, col267) ,ISNULL(@DBC_Cdrive_M_4_14, col268) ,ISNULL(@DBC_Cdrive_M_4_15, col269) ,ISNULL(@DBC_Cdrive_M_4_16, col270) ,ISNULL(@DBC_Cdrive_M_4_17, col271) 
,ISNULL(@DBC_Cdrive_M_4_18, col272) ,ISNULL(@DBC_Cdrive_M_4_19, col273) ,ISNULL(@DBC_Cdrive_M_4_2, col274) ,ISNULL(@DBC_Cdrive_M_4_20, col275) ,ISNULL(@DBC_Cdrive_M_4_3, col276) 
,ISNULL(@DBC_Cdrive_M_4_4, col277) ,ISNULL(@DBC_Cdrive_M_4_5, col278) ,ISNULL(@DBC_Cdrive_M_4_6, col279) ,ISNULL(@DBC_Cdrive_M_4_7, col280) ,ISNULL(@DBC_Cdrive_M_4_8, col281) 
,ISNULL(@DBC_Cdrive_M_4_9, col282) ,ISNULL(@DBC_Cdrive_M_5_1, col283) ,ISNULL(@DBC_Cdrive_M_5_2, col284) ,ISNULL(@DBC_Cdrive_M_8_1, col285) ,ISNULL(@DBC_Cdrive_M_8_2, col286) 
,ISNULL(@DBC_Cdrive_M_8_3, col287) ,ISNULL(@DBC_Cdrive_M_8_4, col288) ,ISNULL(@DBC_Cdrive_M_8_5, col289) ,ISNULL(@DBC_Cdrive_M_8_6, col290) ,ISNULL(@DBC_Cdrive_M_8_7, col291) 
,ISNULL(@DBC_Cdrive_M_8_8, col292) ,ISNULL(@DBC_CModeTowing_M_5_1, col293) ,ISNULL(@DBC_CModeTowing_M_5_2, col294) ,ISNULL(@DBC_CTCU1GrInTracConv1_3_1, col295) ,ISNULL(@DBC_CTCU2GrInTracConv2_3_2, col296) 
,ISNULL(@DBC_CTractiveEffort_3_1, col297) ,ISNULL(@DBC_CTractiveEffort_3_2, col298) ,ISNULL(@DBC_ITrainSpeedInt, col299) ,ISNULL(@DBC_ITrainSpeedInt_1_1, col300) ,ISNULL(@DBC_ITrainSpeedInt_1_2, col301) 
,ISNULL(@DBC_ITrainSpeedInt_15, col302) ,ISNULL(@DBC_ITrainSpeedInt_2_1, col303) ,ISNULL(@DBC_ITrainSpeedInt_2_2, col304) ,ISNULL(@DBC_ITrainSpeedInt_3_1, col305) ,ISNULL(@DBC_ITrainSpeedInt_3_2, col306) 
,ISNULL(@DBC_ITrainSpeedInt_4_1, col307) ,ISNULL(@DBC_ITrainSpeedInt_4_10, col308) ,ISNULL(@DBC_ITrainSpeedInt_4_11, col309) ,ISNULL(@DBC_ITrainSpeedInt_4_12, col310) ,ISNULL(@DBC_ITrainSpeedInt_4_13, col311) 
,ISNULL(@DBC_ITrainSpeedInt_4_14, col312) ,ISNULL(@DBC_ITrainSpeedInt_4_15, col313) ,ISNULL(@DBC_ITrainSpeedInt_4_16, col314) ,ISNULL(@DBC_ITrainSpeedInt_4_17, col315) ,ISNULL(@DBC_ITrainSpeedInt_4_18, col316) 
,ISNULL(@DBC_ITrainSpeedInt_4_19, col317) ,ISNULL(@DBC_ITrainSpeedInt_4_2, col318) ,ISNULL(@DBC_ITrainSpeedInt_4_20, col319) ,ISNULL(@DBC_ITrainSpeedInt_4_3, col320) ,ISNULL(@DBC_ITrainSpeedInt_4_4, col321) 
,ISNULL(@DBC_ITrainSpeedInt_4_5, col322) ,ISNULL(@DBC_ITrainSpeedInt_4_6, col323) ,ISNULL(@DBC_ITrainSpeedInt_4_7, col324) ,ISNULL(@DBC_ITrainSpeedInt_4_8, col325) ,ISNULL(@DBC_ITrainSpeedInt_4_9, col326) 
,ISNULL(@DBC_ITrainSpeedInt_5_1, col327) ,ISNULL(@DBC_ITrainSpeedInt_5_2, col328) ,ISNULL(@DBC_ITrainSpeedInt_8_1, col329) ,ISNULL(@DBC_ITrainSpeedInt_8_2, col330) ,ISNULL(@DBC_ITrainSpeedInt_8_3, col331) 
,ISNULL(@DBC_ITrainSpeedInt_8_4, col332) ,ISNULL(@DBC_ITrainSpeedInt_8_5, col333) ,ISNULL(@DBC_ITrainSpeedInt_8_6, col334) ,ISNULL(@DBC_ITrainSpeedInt_8_7, col335) ,ISNULL(@DBC_ITrainSpeedInt_8_8, col336) 
,ISNULL(@DBC_ITrainSpeedInt_9, col337) ,ISNULL(@DBC_IZrSpdInd_4_1, col338) ,ISNULL(@DBC_IZrSpdInd_4_10, col339) ,ISNULL(@DBC_IZrSpdInd_4_11, col340) ,ISNULL(@DBC_IZrSpdInd_4_12, col341) 
,ISNULL(@DBC_IZrSpdInd_4_13, col342) ,ISNULL(@DBC_IZrSpdInd_4_14, col343) ,ISNULL(@DBC_IZrSpdInd_4_15, col344) ,ISNULL(@DBC_IZrSpdInd_4_16, col345) ,ISNULL(@DBC_IZrSpdInd_4_17, col346) 
,ISNULL(@DBC_IZrSpdInd_4_18, col347) ,ISNULL(@DBC_IZrSpdInd_4_19, col348) ,ISNULL(@DBC_IZrSpdInd_4_2, col349) ,ISNULL(@DBC_IZrSpdInd_4_20, col350) ,ISNULL(@DBC_IZrSpdInd_4_3, col351) 
,ISNULL(@DBC_IZrSpdInd_4_4, col352) ,ISNULL(@DBC_IZrSpdInd_4_5, col353) ,ISNULL(@DBC_IZrSpdInd_4_6, col354) ,ISNULL(@DBC_IZrSpdInd_4_7, col355) ,ISNULL(@DBC_IZrSpdInd_4_8, col356) 
,ISNULL(@DBC_IZrSpdInd_4_9, col357) ,ISNULL(@DBC_PRTcu1GrOut_3_1, col358) ,ISNULL(@DBC_PRTcu2GrOut_3_2, col359) ,ISNULL(@DRIVERBRAKE_APPLIED_SUFF, col360) ,ISNULL(@DRIVERBRAKE_OPERATED, col361) 
,ISNULL(@EB_STATUS, col362) ,ISNULL(@ERROR_CODE_INTERNAL, col363) ,ISNULL(@ERRORCODE_1, col364) ,ISNULL(@ERRORCODE_2, col365) ,ISNULL(@ERRORCODE_3, col366) 
,ISNULL(@GW_IGwOrientation, col367) ,ISNULL(@HAC_ITempOutsideInt, col368) ,ISNULL(@HAC_ITempOutsideInt_1_1, col369) ,ISNULL(@HAC_ITempOutsideInt_1_2, col370) ,ISNULL(@HAC_ITempOutsideInt_2_1, col371) 
,ISNULL(@HAC_ITempOutsideInt_2_2, col372) ,ISNULL(@HAC_ITempOutsideInt_3_1, col373) ,ISNULL(@HAC_ITempOutsideInt_3_2, col374) ,ISNULL(@HAC_ITempOutsideInt_4_1, col375) ,ISNULL(@HAC_ITempOutsideInt_4_10, col376) 
,ISNULL(@HAC_ITempOutsideInt_4_11, col377) ,ISNULL(@HAC_ITempOutsideInt_4_12, col378) ,ISNULL(@HAC_ITempOutsideInt_4_13, col379) ,ISNULL(@HAC_ITempOutsideInt_4_14, col380) ,ISNULL(@HAC_ITempOutsideInt_4_15, col381) 
,ISNULL(@HAC_ITempOutsideInt_4_16, col382) ,ISNULL(@HAC_ITempOutsideInt_4_17, col383) ,ISNULL(@HAC_ITempOutsideInt_4_18, col384) ,ISNULL(@HAC_ITempOutsideInt_4_19, col385) ,ISNULL(@HAC_ITempOutsideInt_4_2, col386) 
,ISNULL(@HAC_ITempOutsideInt_4_20, col387) ,ISNULL(@HAC_ITempOutsideInt_4_3, col388) ,ISNULL(@HAC_ITempOutsideInt_4_4, col389) ,ISNULL(@HAC_ITempOutsideInt_4_5, col390) ,ISNULL(@HAC_ITempOutsideInt_4_6, col391) 
,ISNULL(@HAC_ITempOutsideInt_4_7, col392) ,ISNULL(@HAC_ITempOutsideInt_4_8, col393) ,ISNULL(@HAC_ITempOutsideInt_4_9, col394) ,ISNULL(@HAC_ITempOutsideInt_5_1, col395) ,ISNULL(@HAC_ITempOutsideInt_5_2, col396) 
,ISNULL(@HAC_ITempOutsideInt_9, col397) ,ISNULL(@HVAC1_ITempAirMixedInt, col398) ,ISNULL(@HVAC1_ITempDuctAir1Int, col399) ,ISNULL(@HVAC1_ITempDuctAir2Int, col400) ,ISNULL(@HVAC1_ITempInComInt, col401) 
,ISNULL(@HVAC1_ITempOutsideInt, col402) ,ISNULL(@HVAC1_ITempSetpActDuctInt, col403) ,ISNULL(@HVAC1_ITempSetpActInt, col404) ,ISNULL(@HVAC2_ITempAirMixedInt, col405) ,ISNULL(@HVAC2_ITempDuctAir1Int, col406) 
,ISNULL(@HVAC2_ITempDuctAir2Int, col407) ,ISNULL(@HVAC2_ITempInComInt, col408) ,ISNULL(@HVAC2_ITempOutsideInt, col409) ,ISNULL(@HVAC2_ITempSetpActDuctInt, col410) ,ISNULL(@HVAC2_ITempSetpActInt, col411) 
,ISNULL(@HVAC3_ITempAirMixedInt, col412) ,ISNULL(@HVAC3_ITempDuctAir1Int, col413) ,ISNULL(@HVAC3_ITempDuctAir2Int, col414) ,ISNULL(@HVAC3_ITempInComInt, col415) ,ISNULL(@HVAC3_ITempOutsideInt, col416) 
,ISNULL(@HVAC3_ITempSetpActDuctInt, col417) ,ISNULL(@HVAC3_ITempSetpActInt, col418) ,ISNULL(@HVAC4_ITempAirMixedInt, col419) ,ISNULL(@HVAC4_ITempDuctAir1Int, col420) ,ISNULL(@HVAC4_ITempDuctAir2Int, col421) 
,ISNULL(@HVAC4_ITempInComInt, col422) ,ISNULL(@HVAC4_ITempOutsideInt, col423) ,ISNULL(@HVAC4_ITempSetpActDuctInt, col424) ,ISNULL(@HVAC4_ITempSetpActInt, col425) ,ISNULL(@HVAC5_ITempAirMixedInt, col426) 
,ISNULL(@HVAC5_ITempDuctAir1Int, col427) ,ISNULL(@HVAC5_ITempDuctAir2Int, col428) ,ISNULL(@HVAC5_ITempInComInt, col429) ,ISNULL(@HVAC5_ITempOutsideInt, col430) ,ISNULL(@HVAC5_ITempSetpActDuctInt, col431) 
,ISNULL(@HVAC5_ITempSetpActInt, col432) ,ISNULL(@HVAC6_ITempAirMixedInt, col433) ,ISNULL(@HVAC6_ITempDuctAir1Int, col434) ,ISNULL(@HVAC6_ITempDuctAir2Int, col435) ,ISNULL(@HVAC6_ITempInComInt, col436) 
,ISNULL(@HVAC6_ITempOutsideInt, col437) ,ISNULL(@HVAC6_ITempSetpActDuctInt, col438) ,ISNULL(@HVAC6_ITempSetpActInt, col439) ,ISNULL(@HVCC1_ITempDuctAir1Int, col440) ,ISNULL(@HVCC1_ITempInComInt, col441) 
,ISNULL(@HVCC1_ITempOutsideInt, col442) ,ISNULL(@HVCC1_ITempSetpActDuctInt, col443) ,ISNULL(@HVCC1_ITempSetpActInt, col444) ,ISNULL(@HVCC2_ITempDuctAir1Int, col445) ,ISNULL(@HVCC2_ITempInComInt, col446) 
,ISNULL(@HVCC2_ITempOutsideInt, col447) ,ISNULL(@HVCC2_ITempSetpActDuctInt, col448) ,ISNULL(@HVCC2_ITempSetpActInt, col449) ,ISNULL(@INadiCtrlInfo1, col450) ,ISNULL(@INadiCtrlInfo2, col451) 
,ISNULL(@INadiCtrlInfo3, col452) ,ISNULL(@INadiNumberEntries, col453) ,ISNULL(@INadiTcnAddr1, col454) ,ISNULL(@INadiTcnAddr2, col455) ,ISNULL(@INadiTcnAddr3, col456) 
,ISNULL(@INadiTopoCount, col457) ,ISNULL(@INadiUicAddr1, col458) ,ISNULL(@INadiUicAddr2, col459) ,ISNULL(@INadiUicAddr3, col460) ,ISNULL(@INTERNAL_ERROR_CODE, col461) 
,ISNULL(@INTERNAL_STATE, col462) ,ISNULL(@ITrainsetNumber1, col463) ,ISNULL(@ITrainsetNumber2, col464) ,ISNULL(@ITrainsetNumber3, col465) ,ISNULL(@MCG_IGsmSigStrength, col466) 
,ISNULL(@MIO_IContBusBar400On, col467) ,ISNULL(@MIO_IContBusBar400On_1_1, col468) ,ISNULL(@MIO_IContBusBar400On_1_2, col469) ,ISNULL(@MIO_IContBusBar400On_2_1, col470) ,ISNULL(@MIO_IContBusBar400On_2_2, col471) 
,ISNULL(@MIO_IContBusBar400On_3_1, col472) ,ISNULL(@MIO_IContBusBar400On_3_2, col473) ,ISNULL(@MIO_IContBusBar400On_8_1, col474) ,ISNULL(@MIO_IContBusBar400On_8_2, col475) ,ISNULL(@MIO_IContBusBar400On_8_3, col476) 
,ISNULL(@MIO_IContBusBar400On_8_4, col477) ,ISNULL(@MIO_IContBusBar400On_8_5, col478) ,ISNULL(@MIO_IContBusBar400On_8_6, col479) ,ISNULL(@MIO_IContBusBar400On_8_7, col480) ,ISNULL(@MIO_IContBusBar400On_8_8, col481) 
,ISNULL(@MIO_IContBusBar400On_9, col482) ,ISNULL(@MIO_IHscbOff, col483) ,ISNULL(@MIO_IHscbOff_1_1, col484) ,ISNULL(@MIO_IHscbOff_1_2, col485) ,ISNULL(@MIO_IHscbOff_2_1, col486) 
,ISNULL(@MIO_IHscbOff_2_2, col487) ,ISNULL(@MIO_IHscbOff_3_1, col488) ,ISNULL(@MIO_IHscbOff_3_2, col489) ,ISNULL(@MIO_IHscbOff_8_1, col490) ,ISNULL(@MIO_IHscbOff_8_2, col491) 
,ISNULL(@MIO_IHscbOff_8_3, col492) ,ISNULL(@MIO_IHscbOff_8_4, col493) ,ISNULL(@MIO_IHscbOff_8_5, col494) ,ISNULL(@MIO_IHscbOff_8_6, col495) ,ISNULL(@MIO_IHscbOff_8_7, col496) 
,ISNULL(@MIO_IHscbOff_8_8, col497) ,ISNULL(@MIO_IHscbOn, col498) ,ISNULL(@MIO_IHscbOn_1_1, col499) ,ISNULL(@MIO_IHscbOn_1_2, col500) ,ISNULL(@MIO_IHscbOn_2_1, col501) 
,ISNULL(@MIO_IHscbOn_2_2, col502) ,ISNULL(@MIO_IHscbOn_3_1, col503) ,ISNULL(@MIO_IHscbOn_3_2, col504) ,ISNULL(@MIO_IHscbOn_8_1, col505) ,ISNULL(@MIO_IHscbOn_8_2, col506) 
,ISNULL(@MIO_IHscbOn_8_3, col507) ,ISNULL(@MIO_IHscbOn_8_4, col508) ,ISNULL(@MIO_IHscbOn_8_5, col509) ,ISNULL(@MIO_IHscbOn_8_6, col510) ,ISNULL(@MIO_IHscbOn_8_7, col511) 
,ISNULL(@MIO_IHscbOn_8_8, col512) ,ISNULL(@MIO_IPantoPrssSw1On, col513) ,ISNULL(@MIO_IPantoPrssSw1On_1_1, col514) ,ISNULL(@MIO_IPantoPrssSw1On_1_2, col515) ,ISNULL(@MIO_IPantoPrssSw1On_2_1, col516) 
,ISNULL(@MIO_IPantoPrssSw1On_2_2, col517) ,ISNULL(@MIO_IPantoPrssSw1On_3_1, col518) ,ISNULL(@MIO_IPantoPrssSw1On_3_2, col519) ,ISNULL(@MIO_IPantoPrssSw1On_8_1, col520) ,ISNULL(@MIO_IPantoPrssSw1On_8_2, col521) 
,ISNULL(@MIO_IPantoPrssSw1On_8_3, col522) ,ISNULL(@MIO_IPantoPrssSw1On_8_4, col523) ,ISNULL(@MIO_IPantoPrssSw1On_8_5, col524) ,ISNULL(@MIO_IPantoPrssSw1On_8_6, col525) ,ISNULL(@MIO_IPantoPrssSw1On_8_7, col526) 
,ISNULL(@MIO_IPantoPrssSw1On_8_8, col527) ,ISNULL(@MIO_IPantoPrssSw1On_9, col528) ,ISNULL(@MIO_IPantoPrssSw2On, col529) ,ISNULL(@MIO_IPantoPrssSw2On_1_1, col530) ,ISNULL(@MIO_IPantoPrssSw2On_1_2, col531) 
,ISNULL(@MIO_IPantoPrssSw2On_2_1, col532) ,ISNULL(@MIO_IPantoPrssSw2On_2_2, col533) ,ISNULL(@MIO_IPantoPrssSw2On_3_1, col534) ,ISNULL(@MIO_IPantoPrssSw2On_3_2, col535) ,ISNULL(@MIO_IPantoPrssSw2On_8_1, col536) 
,ISNULL(@MIO_IPantoPrssSw2On_8_2, col537) ,ISNULL(@MIO_IPantoPrssSw2On_8_3, col538) ,ISNULL(@MIO_IPantoPrssSw2On_8_4, col539) ,ISNULL(@MIO_IPantoPrssSw2On_8_5, col540) ,ISNULL(@MIO_IPantoPrssSw2On_8_6, col541) 
,ISNULL(@MIO_IPantoPrssSw2On_8_7, col542) ,ISNULL(@MIO_IPantoPrssSw2On_8_8, col543) ,ISNULL(@MIO_IPantoPrssSw2On_9, col544) ,ISNULL(@MMI_EB_STATUS, col545) ,ISNULL(@MMI_M_ACTIVE_CABIN, col546) 
,ISNULL(@MMI_M_LEVEL, col547) ,ISNULL(@MMI_M_MODE, col548) ,ISNULL(@MMI_M_WARNING, col549) ,ISNULL(@MMI_SB_STATUS, col550) ,ISNULL(@MMI_V_PERMITTED, col551) 
,ISNULL(@MMI_V_TRAIN, col552) ,ISNULL(@NID_STM_1, col553) ,ISNULL(@NID_STM_2, col554) ,ISNULL(@NID_STM_3, col555) ,ISNULL(@NID_STM_4, col556) 
,ISNULL(@NID_STM_5, col557) ,ISNULL(@NID_STM_6, col558) ,ISNULL(@NID_STM_7, col559) ,ISNULL(@NID_STM_8, col560) ,ISNULL(@NID_STM_DA, col561) 
,ISNULL(@ONE_STM_DA, col562) ,ISNULL(@ONE_STM_HS, col563) ,ISNULL(@PB_ADDR, col564) ,ISNULL(@PERMITTED_SPEED, col565) ,ISNULL(@Q_ODO, col566) 
,ISNULL(@REPORTED_DIRECTION, col567) ,ISNULL(@REPORTED_SPEED, col568) ,ISNULL(@REPORTED_SPEED_HISTORIC, col569) ,ISNULL(@ROTATION_DIRECTION_SDU1, col570) ,ISNULL(@ROTATION_DIRECTION_SDU2, col571) 
,ISNULL(@SAP, col572) ,ISNULL(@SB_STATUS, col573) ,ISNULL(@SDU_DIRECTION_SDU1, col574) ,ISNULL(@SDU_DIRECTION_SDU2, col575) ,ISNULL(@SENSOR_ERROR_STATUS1_TACHO1, col576) 
,ISNULL(@SENSOR_ERROR_STATUS2_TACHO2, col577) ,ISNULL(@SENSOR_ERROR_STATUS3_RADAR1, col578) ,ISNULL(@SENSOR_ERROR_STATUS4_RADAR2, col579) ,ISNULL(@SENSOR_SPEED_HISTORIC_RADAR1, col580) ,ISNULL(@SENSOR_SPEED_HISTORIC_RADAR2, col581) 
,ISNULL(@SENSOR_SPEED_HISTORIC_TACHO1, col582) ,ISNULL(@SENSOR_SPEED_HISTORIC_TACHO2, col583) ,ISNULL(@SENSOR_SPEED_RADAR1, col584) ,ISNULL(@SENSOR_SPEED_RADAR2, col585) ,ISNULL(@SENSOR_SPEED_TACHO1, col586) 
,ISNULL(@SENSOR_SPEED_TACHO2, col587) ,ISNULL(@SLIP_SLIDE_ACC_AXLE1, col588) ,ISNULL(@SLIP_SLIDE_ACC_AXLE2, col589) ,ISNULL(@SLIP_SLIDE_AXLE1, col590) ,ISNULL(@SLIP_SLIDE_AXLE2, col591) 
,ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE1, col592) ,ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE2, col593) ,ISNULL(@SLIP_SLIDE_STATUS, col594) ,ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE1, col595) ,ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE2, col596) 
,ISNULL(@SOFTWARE_VERSION, col597) ,ISNULL(@SPL_FUNC_ID, col598) ,ISNULL(@SPL_RETURN, col599) ,ISNULL(@STM_STATE, col600) ,ISNULL(@STM_SUB_STATE, col601) 
,ISNULL(@SUPERVISION_STATE, col602) ,ISNULL(@TC_CCorrectOrient_M, col603) ,ISNULL(@TC_IConsistOrient, col604) ,ISNULL(@TC_IGwOrient, col605) ,ISNULL(@TC_INumConsists, col606) 
,ISNULL(@TC_INumConsists_1_1, col607) ,ISNULL(@TC_INumConsists_1_2, col608) ,ISNULL(@TC_INumConsists_2_1, col609) ,ISNULL(@TC_INumConsists_2_2, col610) ,ISNULL(@TC_INumConsists_3_1, col611) 
,ISNULL(@TC_INumConsists_3_2, col612) ,ISNULL(@TC_INumConsists_4_1, col613) ,ISNULL(@TC_INumConsists_4_10, col614) ,ISNULL(@TC_INumConsists_4_11, col615) ,ISNULL(@TC_INumConsists_4_12, col616) 
,ISNULL(@TC_INumConsists_4_13, col617) ,ISNULL(@TC_INumConsists_4_14, col618) ,ISNULL(@TC_INumConsists_4_15, col619) ,ISNULL(@TC_INumConsists_4_16, col620) ,ISNULL(@TC_INumConsists_4_17, col621) 
,ISNULL(@TC_INumConsists_4_18, col622) ,ISNULL(@TC_INumConsists_4_19, col623) ,ISNULL(@TC_INumConsists_4_2, col624) ,ISNULL(@TC_INumConsists_4_20, col625) ,ISNULL(@TC_INumConsists_4_3, col626) 
,ISNULL(@TC_INumConsists_4_4, col627) ,ISNULL(@TC_INumConsists_4_5, col628) ,ISNULL(@TC_INumConsists_4_6, col629) ,ISNULL(@TC_INumConsists_4_7, col630) ,ISNULL(@TC_INumConsists_4_8, col631) 
,ISNULL(@TC_INumConsists_4_9, col632) ,ISNULL(@TC_INumConsists_5_1, col633) ,ISNULL(@TC_INumConsists_5_2, col634) ,ISNULL(@TC_INumConsists_8_1, col635) ,ISNULL(@TC_INumConsists_8_2, col636) 
,ISNULL(@TC_INumConsists_8_3, col637) ,ISNULL(@TC_INumConsists_8_4, col638) ,ISNULL(@TC_INumConsists_8_5, col639) ,ISNULL(@TC_INumConsists_8_6, col640) ,ISNULL(@TC_INumConsists_8_7, col641) 
,ISNULL(@TC_INumConsists_8_8, col642) ,ISNULL(@TC_INumConsists_9, col643) ,ISNULL(@TC_ITclOrient, col644) ,ISNULL(@TCO_STATUS, col645) ,ISNULL(@TCU1_IEdSliding_3_1, col646) 
,ISNULL(@TCU1_IEdSliding_5_1, col647) ,ISNULL(@TCU1_ILineVoltage_3_1, col648) ,ISNULL(@TCU1_ILineVoltage_5_1, col649) ,ISNULL(@TCU1_ILineVoltage_5_2, col650) ,ISNULL(@TCU1_IPowerLimit_3_1, col651) 
,ISNULL(@TCU1_ITracBrActualInt_3_1, col652) ,ISNULL(@TCU2_IEdSliding_3_2, col653) ,ISNULL(@TCU2_IEdSliding_5_2, col654) ,ISNULL(@TCU2_ILineVoltage_3_2, col655) ,ISNULL(@TCU2_ILineVoltage_5_1, col656) 
,ISNULL(@TCU2_ILineVoltage_5_2, col657) ,ISNULL(@TCU2_IPowerLimit_3_2, col658) ,ISNULL(@TCU2_ITracBrActualInt_3_2, col659) ,ISNULL(@TRACK_SIGNAL, col660) ,ISNULL(@DBC_PRTcu1GrOut, col661) 
,ISNULL(@DBC_PRTcu2GrOut, col662) ,ISNULL(@MPW_PRHscbEnable, col663) ,ISNULL(@MPW_IPnt1UpDrvCount, col664) ,ISNULL(@MPW_IPnt2UpDrvCount, col665) ,ISNULL(@MPW_IPnt1UpCount, col666) 
,ISNULL(@MPW_IPnt2UpCount, col667) ,ISNULL(@MPW_IPnt1KmCount, col668) ,ISNULL(@MPW_IPnt2KmCount, col669) ,ISNULL(@DBC_IKmCounter, col670) ,ISNULL(@AUXY_IAuxAirCOnCnt, col671) 
,ISNULL(@MPW_IOnHscb, col672) ,ISNULL(@AUXY_IAirCSwOnCount, col673) ,ISNULL(@AUXY_IAirCOnCount, col674) ,ISNULL(@AUXY_IAuxAirCSwOnCnt, col675) ,ISNULL(@MPW_PRPantoEnable, col676)
,ISNULL(@DIA_Reserve1_AUX1, col677) ,ISNULL(@DIA_Reserve1_AUX2, col678) ,ISNULL(@DIA_Reserve1_BCHA_1, col679) ,ISNULL(@DIA_Reserve1_BCHA_2, col680) ,ISNULL(@DIA_Reserve1_BCU_1, col681)
,ISNULL(@DIA_Reserve1_BCU_2, col682) ,ISNULL(@DIA_Reserve1_CCUO, col683) ,ISNULL(@DIA_Reserve1_DCU_1, col684) ,ISNULL(@DIA_Reserve1_DCU_10, col685) ,ISNULL(@DIA_Reserve1_DCU_11, col686)
,ISNULL(@DIA_Reserve1_DCU_12, col687) ,ISNULL(@DIA_Reserve1_DCU_13, col688) ,ISNULL(@DIA_Reserve1_DCU_14, col689) ,ISNULL(@DIA_Reserve1_DCU_15, col690) ,ISNULL(@DIA_Reserve1_DCU_16, col691)
,ISNULL(@DIA_Reserve1_DCU_17, col692) ,ISNULL(@DIA_Reserve1_DCU_18, col693) ,ISNULL(@DIA_Reserve1_DCU_19, col694) ,ISNULL(@DIA_Reserve1_DCU_2, col695) ,ISNULL(@DIA_Reserve1_DCU_20, col696)
,ISNULL(@DIA_Reserve1_DCU_3, col697) ,ISNULL(@DIA_Reserve1_DCU_4, col698) ,ISNULL(@DIA_Reserve1_DCU_5, col699) ,ISNULL(@DIA_Reserve1_DCU_6, col700) ,ISNULL(@DIA_Reserve1_DCU_7, col701)
,ISNULL(@DIA_Reserve1_DCU_8, col702) ,ISNULL(@DIA_Reserve1_DCU_9, col703) ,ISNULL(@DIA_Reserve1_HVAC_1, col704) ,ISNULL(@DIA_Reserve1_HVAC_2, col705) ,ISNULL(@DIA_Reserve1_HVAC_3, col706)
,ISNULL(@DIA_Reserve1_HVAC_4, col707) ,ISNULL(@DIA_Reserve1_HVAC_5, col708) ,ISNULL(@DIA_Reserve1_HVAC_6, col709) ,ISNULL(@DIA_Reserve1_HVCC_1, col710) ,ISNULL(@DIA_Reserve1_HVCC_2, col711)
,ISNULL(@DIA_Reserve1_PIS, col712) ,ISNULL(@DIA_Reserve1_TCU_1, col713) ,ISNULL(@DIA_Reserve1_TCU_2, col714) ,ISNULL(@reserved, col715)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

	SELECT @CVID = ID FROM @IDs

	INSERT INTO dbo.ChannelValueDoor (ID,TimeStamp,UnitID,UpdateRecord
	 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
	)
	SELECT TOP 1
	@CVID
	, @Timestamp
	, @UnitID		
	, @UpdateRecord
	,ISNULL(@DCU01_CDoorCloseConduc, col3000) ,ISNULL(@DCU01_IDoorClosedSafe, col3001) ,ISNULL(@DCU01_IDoorClRelease, col3002) ,ISNULL(@DCU01_IDoorPbClose, col3003) ,ISNULL(@DCU01_IDoorPbOpenIn, col3004) 
,ISNULL(@DCU01_IDoorPbOpenOut, col3005) ,ISNULL(@DCU01_IDoorReleased, col3006) ,ISNULL(@DCU01_IFootStepRel, col3007) ,ISNULL(@DCU01_IForcedClosDoor, col3008) ,ISNULL(@DCU01_ILeafsStopDoor, col3009) 
,ISNULL(@DCU01_IObstacleDoor, col3010) ,ISNULL(@DCU01_IObstacleStep, col3011) ,ISNULL(@DCU01_IOpenAssistDoor, col3012) ,ISNULL(@DCU01_IStandstillBack, col3013) ,ISNULL(@DCU02_CDoorCloseConduc, col3014) 
,ISNULL(@DCU02_IDoorClosedSafe, col3015) ,ISNULL(@DCU02_IDoorClRelease, col3016) ,ISNULL(@DCU02_IDoorPbClose, col3017) ,ISNULL(@DCU02_IDoorPbOpenIn, col3018) ,ISNULL(@DCU02_IDoorPbOpenOut, col3019) 
,ISNULL(@DCU02_IDoorReleased, col3020) ,ISNULL(@DCU02_IFootStepRel, col3021) ,ISNULL(@DCU02_IForcedClosDoor, col3022) ,ISNULL(@DCU02_ILeafsStopDoor, col3023) ,ISNULL(@DCU02_IObstacleDoor, col3024) 
,ISNULL(@DCU02_IObstacleStep, col3025) ,ISNULL(@DCU02_IOpenAssistDoor, col3026) ,ISNULL(@DCU02_IStandstillBack, col3027) ,ISNULL(@DCU03_CDoorCloseConduc, col3028) ,ISNULL(@DCU03_IDoorClosedSafe, col3029) 
,ISNULL(@DCU03_IDoorClRelease, col3030) ,ISNULL(@DCU03_IDoorPbClose, col3031) ,ISNULL(@DCU03_IDoorPbOpenIn, col3032) ,ISNULL(@DCU03_IDoorPbOpenOut, col3033) ,ISNULL(@DCU03_IDoorReleased, col3034) 
,ISNULL(@DCU03_IFootStepRel, col3035) ,ISNULL(@DCU03_IForcedClosDoor, col3036) ,ISNULL(@DCU03_ILeafsStopDoor, col3037) ,ISNULL(@DCU03_IObstacleDoor, col3038) ,ISNULL(@DCU03_IObstacleStep, col3039) 
,ISNULL(@DCU03_IOpenAssistDoor, col3040) ,ISNULL(@DCU03_IStandstillBack, col3041) ,ISNULL(@DCU04_CDoorCloseConduc, col3042) ,ISNULL(@DCU04_IDoorClosedSafe, col3043) ,ISNULL(@DCU04_IDoorClRelease, col3044) 
,ISNULL(@DCU04_IDoorPbClose, col3045) ,ISNULL(@DCU04_IDoorPbOpenIn, col3046) ,ISNULL(@DCU04_IDoorPbOpenOut, col3047) ,ISNULL(@DCU04_IDoorReleased, col3048) ,ISNULL(@DCU04_IFootStepRel, col3049) 
,ISNULL(@DCU04_IForcedClosDoor, col3050) ,ISNULL(@DCU04_ILeafsStopDoor, col3051) ,ISNULL(@DCU04_IObstacleDoor, col3052) ,ISNULL(@DCU04_IObstacleStep, col3053) ,ISNULL(@DCU04_IOpenAssistDoor, col3054) 
,ISNULL(@DCU04_IStandstillBack, col3055) ,ISNULL(@DCU05_CDoorCloseConduc, col3056) ,ISNULL(@DCU05_IDoorClosedSafe, col3057) ,ISNULL(@DCU05_IDoorClRelease, col3058) ,ISNULL(@DCU05_IDoorPbClose, col3059) 
,ISNULL(@DCU05_IDoorPbOpenIn, col3060) ,ISNULL(@DCU05_IDoorPbOpenOut, col3061) ,ISNULL(@DCU05_IDoorReleased, col3062) ,ISNULL(@DCU05_IFootStepRel, col3063) ,ISNULL(@DCU05_IForcedClosDoor, col3064) 
,ISNULL(@DCU05_ILeafsStopDoor, col3065) ,ISNULL(@DCU05_IObstacleDoor, col3066) ,ISNULL(@DCU05_IObstacleStep, col3067) ,ISNULL(@DCU05_IOpenAssistDoor, col3068) ,ISNULL(@DCU05_IStandstillBack, col3069) 
,ISNULL(@DCU06_CDoorCloseConduc, col3070) ,ISNULL(@DCU06_IDoorClosedSafe, col3071) ,ISNULL(@DCU06_IDoorClRelease, col3072) ,ISNULL(@DCU06_IDoorPbClose, col3073) ,ISNULL(@DCU06_IDoorPbOpenIn, col3074) 
,ISNULL(@DCU06_IDoorPbOpenOut, col3075) ,ISNULL(@DCU06_IDoorReleased, col3076) ,ISNULL(@DCU06_IFootStepRel, col3077) ,ISNULL(@DCU06_IForcedClosDoor, col3078) ,ISNULL(@DCU06_ILeafsStopDoor, col3079) 
,ISNULL(@DCU06_IObstacleDoor, col3080) ,ISNULL(@DCU06_IObstacleStep, col3081) ,ISNULL(@DCU06_IOpenAssistDoor, col3082) ,ISNULL(@DCU06_IStandstillBack, col3083) ,ISNULL(@DCU07_CDoorCloseConduc, col3084) 
,ISNULL(@DCU07_IDoorClosedSafe, col3085) ,ISNULL(@DCU07_IDoorClRelease, col3086) ,ISNULL(@DCU07_IDoorPbClose, col3087) ,ISNULL(@DCU07_IDoorPbOpenIn, col3088) ,ISNULL(@DCU07_IDoorPbOpenOut, col3089) 
,ISNULL(@DCU07_IDoorReleased, col3090) ,ISNULL(@DCU07_IFootStepRel, col3091) ,ISNULL(@DCU07_IForcedClosDoor, col3092) ,ISNULL(@DCU07_ILeafsStopDoor, col3093) ,ISNULL(@DCU07_IObstacleDoor, col3094) 
,ISNULL(@DCU07_IObstacleStep, col3095) ,ISNULL(@DCU07_IOpenAssistDoor, col3096) ,ISNULL(@DCU07_IStandstillBack, col3097) ,ISNULL(@DCU08_CDoorCloseConduc, col3098) ,ISNULL(@DCU08_IDoorClosedSafe, col3099) 
,ISNULL(@DCU08_IDoorClRelease, col3100) ,ISNULL(@DCU08_IDoorPbClose, col3101) ,ISNULL(@DCU08_IDoorPbOpenIn, col3102) ,ISNULL(@DCU08_IDoorPbOpenOut, col3103) ,ISNULL(@DCU08_IDoorReleased, col3104) 
,ISNULL(@DCU08_IFootStepRel, col3105) ,ISNULL(@DCU08_IForcedClosDoor, col3106) ,ISNULL(@DCU08_ILeafsStopDoor, col3107) ,ISNULL(@DCU08_IObstacleDoor, col3108) ,ISNULL(@DCU08_IObstacleStep, col3109) 
,ISNULL(@DCU08_IOpenAssistDoor, col3110) ,ISNULL(@DCU08_IStandstillBack, col3111) ,ISNULL(@DCU09_CDoorCloseConduc, col3112) ,ISNULL(@DCU09_IDoorClosedSafe, col3113) ,ISNULL(@DCU09_IDoorClRelease, col3114) 
,ISNULL(@DCU09_IDoorPbClose, col3115) ,ISNULL(@DCU09_IDoorPbOpenIn, col3116) ,ISNULL(@DCU09_IDoorPbOpenOut, col3117) ,ISNULL(@DCU09_IDoorReleased, col3118) ,ISNULL(@DCU09_IFootStepRel, col3119) 
,ISNULL(@DCU09_IForcedClosDoor, col3120) ,ISNULL(@DCU09_ILeafsStopDoor, col3121) ,ISNULL(@DCU09_IObstacleDoor, col3122) ,ISNULL(@DCU09_IObstacleStep, col3123) ,ISNULL(@DCU09_IOpenAssistDoor, col3124) 
,ISNULL(@DCU09_IStandstillBack, col3125) ,ISNULL(@DCU10_CDoorCloseConduc, col3126) ,ISNULL(@DCU10_IDoorClosedSafe, col3127) ,ISNULL(@DCU10_IDoorClRelease, col3128) ,ISNULL(@DCU10_IDoorPbClose, col3129) 
,ISNULL(@DCU10_IDoorPbOpenIn, col3130) ,ISNULL(@DCU10_IDoorPbOpenOut, col3131) ,ISNULL(@DCU10_IDoorReleased, col3132) ,ISNULL(@DCU10_IFootStepRel, col3133) ,ISNULL(@DCU10_IForcedClosDoor, col3134) 
,ISNULL(@DCU10_ILeafsStopDoor, col3135) ,ISNULL(@DCU10_IObstacleDoor, col3136) ,ISNULL(@DCU10_IObstacleStep, col3137) ,ISNULL(@DCU10_IOpenAssistDoor, col3138) ,ISNULL(@DCU10_IStandstillBack, col3139) 
,ISNULL(@DCU11_CDoorCloseConduc, col3140) ,ISNULL(@DCU11_IDoorClosedSafe, col3141) ,ISNULL(@DCU11_IDoorClRelease, col3142) ,ISNULL(@DCU11_IDoorPbClose, col3143) ,ISNULL(@DCU11_IDoorPbOpenIn, col3144) 
,ISNULL(@DCU11_IDoorPbOpenOut, col3145) ,ISNULL(@DCU11_IDoorReleased, col3146) ,ISNULL(@DCU11_IFootStepRel, col3147) ,ISNULL(@DCU11_IForcedClosDoor, col3148) ,ISNULL(@DCU11_ILeafsStopDoor, col3149) 
,ISNULL(@DCU11_IObstacleDoor, col3150) ,ISNULL(@DCU11_IObstacleStep, col3151) ,ISNULL(@DCU11_IOpenAssistDoor, col3152) ,ISNULL(@DCU11_IStandstillBack, col3153) ,ISNULL(@DCU12_CDoorCloseConduc, col3154) 
,ISNULL(@DCU12_IDoorClosedSafe, col3155) ,ISNULL(@DCU12_IDoorClRelease, col3156) ,ISNULL(@DCU12_IDoorPbClose, col3157) ,ISNULL(@DCU12_IDoorPbOpenIn, col3158) ,ISNULL(@DCU12_IDoorPbOpenOut, col3159) 
,ISNULL(@DCU12_IDoorReleased, col3160) ,ISNULL(@DCU12_IFootStepRel, col3161) ,ISNULL(@DCU12_IForcedClosDoor, col3162) ,ISNULL(@DCU12_ILeafsStopDoor, col3163) ,ISNULL(@DCU12_IObstacleDoor, col3164) 
,ISNULL(@DCU12_IObstacleStep, col3165) ,ISNULL(@DCU12_IOpenAssistDoor, col3166) ,ISNULL(@DCU12_IStandstillBack, col3167) ,ISNULL(@DCU13_CDoorCloseConduc, col3168) ,ISNULL(@DCU13_IDoorClosedSafe, col3169) 
,ISNULL(@DCU13_IDoorClRelease, col3170) ,ISNULL(@DCU13_IDoorPbClose, col3171) ,ISNULL(@DCU13_IDoorPbOpenIn, col3172) ,ISNULL(@DCU13_IDoorPbOpenOut, col3173) ,ISNULL(@DCU13_IDoorReleased, col3174) 
,ISNULL(@DCU13_IFootStepRel, col3175) ,ISNULL(@DCU13_IForcedClosDoor, col3176) ,ISNULL(@DCU13_ILeafsStopDoor, col3177) ,ISNULL(@DCU13_IObstacleDoor, col3178) ,ISNULL(@DCU13_IObstacleStep, col3179) 
,ISNULL(@DCU13_IOpenAssistDoor, col3180) ,ISNULL(@DCU13_IStandstillBack, col3181) ,ISNULL(@DCU14_CDoorCloseConduc, col3182) ,ISNULL(@DCU14_IDoorClosedSafe, col3183) ,ISNULL(@DCU14_IDoorClRelease, col3184) 
,ISNULL(@DCU14_IDoorPbClose, col3185) ,ISNULL(@DCU14_IDoorPbOpenIn, col3186) ,ISNULL(@DCU14_IDoorPbOpenOut, col3187) ,ISNULL(@DCU14_IDoorReleased, col3188) ,ISNULL(@DCU14_IFootStepRel, col3189) 
,ISNULL(@DCU14_IForcedClosDoor, col3190) ,ISNULL(@DCU14_ILeafsStopDoor, col3191) ,ISNULL(@DCU14_IObstacleDoor, col3192) ,ISNULL(@DCU14_IObstacleStep, col3193) ,ISNULL(@DCU14_IOpenAssistDoor, col3194) 
,ISNULL(@DCU14_IStandstillBack, col3195) ,ISNULL(@DCU15_CDoorCloseConduc, col3196) ,ISNULL(@DCU15_IDoorClosedSafe, col3197) ,ISNULL(@DCU15_IDoorClRelease, col3198) ,ISNULL(@DCU15_IDoorPbClose, col3199) 
,ISNULL(@DCU15_IDoorPbOpenIn, col3200) ,ISNULL(@DCU15_IDoorPbOpenOut, col3201) ,ISNULL(@DCU15_IDoorReleased, col3202) ,ISNULL(@DCU15_IFootStepRel, col3203) ,ISNULL(@DCU15_IForcedClosDoor, col3204) 
,ISNULL(@DCU15_ILeafsStopDoor, col3205) ,ISNULL(@DCU15_IObstacleDoor, col3206) ,ISNULL(@DCU15_IObstacleStep, col3207) ,ISNULL(@DCU15_IOpenAssistDoor, col3208) ,ISNULL(@DCU15_IStandstillBack, col3209) 
,ISNULL(@DCU16_CDoorCloseConduc, col3210) ,ISNULL(@DCU16_IDoorClosedSafe, col3211) ,ISNULL(@DCU16_IDoorClRelease, col3212) ,ISNULL(@DCU16_IDoorPbClose, col3213) ,ISNULL(@DCU16_IDoorPbOpenIn, col3214) 
,ISNULL(@DCU16_IDoorPbOpenOut, col3215) ,ISNULL(@DCU16_IDoorReleased, col3216) ,ISNULL(@DCU16_IFootStepRel, col3217) ,ISNULL(@DCU16_IForcedClosDoor, col3218) ,ISNULL(@DCU16_ILeafsStopDoor, col3219) 
,ISNULL(@DCU16_IObstacleDoor, col3220) ,ISNULL(@DCU16_IObstacleStep, col3221) ,ISNULL(@DCU16_IOpenAssistDoor, col3222) ,ISNULL(@DCU16_IStandstillBack, col3223) ,ISNULL(@DCU17_CDoorCloseConduc, col3224) 
,ISNULL(@DCU17_IDoorClosedSafe, col3225) ,ISNULL(@DCU17_IDoorClRelease, col3226) ,ISNULL(@DCU17_IDoorPbClose, col3227) ,ISNULL(@DCU17_IDoorPbOpenIn, col3228) ,ISNULL(@DCU17_IDoorPbOpenOut, col3229) 
,ISNULL(@DCU17_IDoorReleased, col3230) ,ISNULL(@DCU17_IFootStepRel, col3231) ,ISNULL(@DCU17_IForcedClosDoor, col3232) ,ISNULL(@DCU17_ILeafsStopDoor, col3233) ,ISNULL(@DCU17_IObstacleDoor, col3234) 
,ISNULL(@DCU17_IObstacleStep, col3235) ,ISNULL(@DCU17_IOpenAssistDoor, col3236) ,ISNULL(@DCU17_IStandstillBack, col3237) ,ISNULL(@DCU18_CDoorCloseConduc, col3238) ,ISNULL(@DCU18_IDoorClosedSafe, col3239) 
,ISNULL(@DCU18_IDoorClRelease, col3240) ,ISNULL(@DCU18_IDoorPbClose, col3241) ,ISNULL(@DCU18_IDoorPbOpenIn, col3242) ,ISNULL(@DCU18_IDoorPbOpenOut, col3243) ,ISNULL(@DCU18_IDoorReleased, col3244) 
,ISNULL(@DCU18_IFootStepRel, col3245) ,ISNULL(@DCU18_IForcedClosDoor, col3246) ,ISNULL(@DCU18_ILeafsStopDoor, col3247) ,ISNULL(@DCU18_IObstacleDoor, col3248) ,ISNULL(@DCU18_IObstacleStep, col3249) 
,ISNULL(@DCU18_IOpenAssistDoor, col3250) ,ISNULL(@DCU18_IStandstillBack, col3251) ,ISNULL(@DCU19_CDoorCloseConduc, col3252) ,ISNULL(@DCU19_IDoorClosedSafe, col3253) ,ISNULL(@DCU19_IDoorClRelease, col3254) 
,ISNULL(@DCU19_IDoorPbClose, col3255) ,ISNULL(@DCU19_IDoorPbOpenIn, col3256) ,ISNULL(@DCU19_IDoorPbOpenOut, col3257) ,ISNULL(@DCU19_IDoorReleased, col3258) ,ISNULL(@DCU19_IFootStepRel, col3259) 
,ISNULL(@DCU19_IForcedClosDoor, col3260) ,ISNULL(@DCU19_ILeafsStopDoor, col3261) ,ISNULL(@DCU19_IObstacleDoor, col3262) ,ISNULL(@DCU19_IObstacleStep, col3263) ,ISNULL(@DCU19_IOpenAssistDoor, col3264) 
,ISNULL(@DCU19_IStandstillBack, col3265) ,ISNULL(@DCU20_CDoorCloseConduc, col3266) ,ISNULL(@DCU20_IDoorClosedSafe, col3267) ,ISNULL(@DCU20_IDoorClRelease, col3268) ,ISNULL(@DCU20_IDoorPbClose, col3269) 
,ISNULL(@DCU20_IDoorPbOpenIn, col3270) ,ISNULL(@DCU20_IDoorPbOpenOut, col3271) ,ISNULL(@DCU20_IDoorReleased, col3272) ,ISNULL(@DCU20_IFootStepRel, col3273) ,ISNULL(@DCU20_IForcedClosDoor, col3274) 
,ISNULL(@DCU20_ILeafsStopDoor, col3275) ,ISNULL(@DCU20_IObstacleDoor, col3276) ,ISNULL(@DCU20_IObstacleStep, col3277) ,ISNULL(@DCU20_IOpenAssistDoor, col3278) ,ISNULL(@DCU20_IStandstillBack, col3279) 
,ISNULL(@DRS_CDisDoorRelease_4_1, col3280) ,ISNULL(@DRS_CDisDoorRelease_4_10, col3281) ,ISNULL(@DRS_CDisDoorRelease_4_11, col3282) ,ISNULL(@DRS_CDisDoorRelease_4_12, col3283) ,ISNULL(@DRS_CDisDoorRelease_4_13, col3284) 
,ISNULL(@DRS_CDisDoorRelease_4_14, col3285) ,ISNULL(@DRS_CDisDoorRelease_4_15, col3286) ,ISNULL(@DRS_CDisDoorRelease_4_16, col3287) ,ISNULL(@DRS_CDisDoorRelease_4_17, col3288) ,ISNULL(@DRS_CDisDoorRelease_4_18, col3289) 
,ISNULL(@DRS_CDisDoorRelease_4_19, col3290) ,ISNULL(@DRS_CDisDoorRelease_4_2, col3291) ,ISNULL(@DRS_CDisDoorRelease_4_20, col3292) ,ISNULL(@DRS_CDisDoorRelease_4_3, col3293) ,ISNULL(@DRS_CDisDoorRelease_4_4, col3294) 
,ISNULL(@DRS_CDisDoorRelease_4_5, col3295) ,ISNULL(@DRS_CDisDoorRelease_4_6, col3296) ,ISNULL(@DRS_CDisDoorRelease_4_7, col3297) ,ISNULL(@DRS_CDisDoorRelease_4_8, col3298) ,ISNULL(@DRS_CDisDoorRelease_4_9, col3299) 
,ISNULL(@DRS_CDoorClosAutDis_4_1, col3300) ,ISNULL(@DRS_CDoorClosAutDis_4_10, col3301) ,ISNULL(@DRS_CDoorClosAutDis_4_11, col3302) ,ISNULL(@DRS_CDoorClosAutDis_4_12, col3303) ,ISNULL(@DRS_CDoorClosAutDis_4_13, col3304) 
,ISNULL(@DRS_CDoorClosAutDis_4_14, col3305) ,ISNULL(@DRS_CDoorClosAutDis_4_15, col3306) ,ISNULL(@DRS_CDoorClosAutDis_4_16, col3307) ,ISNULL(@DRS_CDoorClosAutDis_4_17, col3308) ,ISNULL(@DRS_CDoorClosAutDis_4_18, col3309) 
,ISNULL(@DRS_CDoorClosAutDis_4_19, col3310) ,ISNULL(@DRS_CDoorClosAutDis_4_2, col3311) ,ISNULL(@DRS_CDoorClosAutDis_4_20, col3312) ,ISNULL(@DRS_CDoorClosAutDis_4_3, col3313) ,ISNULL(@DRS_CDoorClosAutDis_4_4, col3314) 
,ISNULL(@DRS_CDoorClosAutDis_4_5, col3315) ,ISNULL(@DRS_CDoorClosAutDis_4_6, col3316) ,ISNULL(@DRS_CDoorClosAutDis_4_7, col3317) ,ISNULL(@DRS_CDoorClosAutDis_4_8, col3318) ,ISNULL(@DRS_CDoorClosAutDis_4_9, col3319) 
,ISNULL(@DRS_CForcedClosDoor_4_1, col3320) ,ISNULL(@DRS_CForcedClosDoor_4_10, col3321) ,ISNULL(@DRS_CForcedClosDoor_4_11, col3322) ,ISNULL(@DRS_CForcedClosDoor_4_12, col3323) ,ISNULL(@DRS_CForcedClosDoor_4_13, col3324) 
,ISNULL(@DRS_CForcedClosDoor_4_14, col3325) ,ISNULL(@DRS_CForcedClosDoor_4_15, col3326) ,ISNULL(@DRS_CForcedClosDoor_4_16, col3327) ,ISNULL(@DRS_CForcedClosDoor_4_17, col3328) ,ISNULL(@DRS_CForcedClosDoor_4_18, col3329) 
,ISNULL(@DRS_CForcedClosDoor_4_19, col3330) ,ISNULL(@DRS_CForcedClosDoor_4_2, col3331) ,ISNULL(@DRS_CForcedClosDoor_4_20, col3332) ,ISNULL(@DRS_CForcedClosDoor_4_3, col3333) ,ISNULL(@DRS_CForcedClosDoor_4_4, col3334) 
,ISNULL(@DRS_CForcedClosDoor_4_5, col3335) ,ISNULL(@DRS_CForcedClosDoor_4_6, col3336) ,ISNULL(@DRS_CForcedClosDoor_4_7, col3337) ,ISNULL(@DRS_CForcedClosDoor_4_8, col3338) ,ISNULL(@DRS_CForcedClosDoor_4_9, col3339) 
,ISNULL(@DRS_CTestModeReq_M_4_1, col3340) ,ISNULL(@DRS_CTestModeReq_M_4_10, col3341) ,ISNULL(@DRS_CTestModeReq_M_4_11, col3342) ,ISNULL(@DRS_CTestModeReq_M_4_12, col3343) ,ISNULL(@DRS_CTestModeReq_M_4_13, col3344) 
,ISNULL(@DRS_CTestModeReq_M_4_14, col3345) ,ISNULL(@DRS_CTestModeReq_M_4_15, col3346) ,ISNULL(@DRS_CTestModeReq_M_4_16, col3347) ,ISNULL(@DRS_CTestModeReq_M_4_17, col3348) ,ISNULL(@DRS_CTestModeReq_M_4_18, col3349) 
,ISNULL(@DRS_CTestModeReq_M_4_19, col3350) ,ISNULL(@DRS_CTestModeReq_M_4_2, col3351) ,ISNULL(@DRS_CTestModeReq_M_4_20, col3352) ,ISNULL(@DRS_CTestModeReq_M_4_3, col3353) ,ISNULL(@DRS_CTestModeReq_M_4_4, col3354) 
,ISNULL(@DRS_CTestModeReq_M_4_5, col3355) ,ISNULL(@DRS_CTestModeReq_M_4_6, col3356) ,ISNULL(@DRS_CTestModeReq_M_4_7, col3357) ,ISNULL(@DRS_CTestModeReq_M_4_8, col3358) ,ISNULL(@DRS_CTestModeReq_M_4_9, col3359) 
,ISNULL(@MIO_I1DoorRelLe_4_1, col3360) ,ISNULL(@MIO_I1DoorRelLe_4_10, col3361) ,ISNULL(@MIO_I1DoorRelLe_4_11, col3362) ,ISNULL(@MIO_I1DoorRelLe_4_12, col3363) ,ISNULL(@MIO_I1DoorRelLe_4_13, col3364) 
,ISNULL(@MIO_I1DoorRelLe_4_14, col3365) ,ISNULL(@MIO_I1DoorRelLe_4_15, col3366) ,ISNULL(@MIO_I1DoorRelLe_4_16, col3367) ,ISNULL(@MIO_I1DoorRelLe_4_17, col3368) ,ISNULL(@MIO_I1DoorRelLe_4_18, col3369) 
,ISNULL(@MIO_I1DoorRelLe_4_19, col3370) ,ISNULL(@MIO_I1DoorRelLe_4_2, col3371) ,ISNULL(@MIO_I1DoorRelLe_4_20, col3372) ,ISNULL(@MIO_I1DoorRelLe_4_3, col3373) ,ISNULL(@MIO_I1DoorRelLe_4_4, col3374) 
,ISNULL(@MIO_I1DoorRelLe_4_5, col3375) ,ISNULL(@MIO_I1DoorRelLe_4_6, col3376) ,ISNULL(@MIO_I1DoorRelLe_4_7, col3377) ,ISNULL(@MIO_I1DoorRelLe_4_8, col3378) ,ISNULL(@MIO_I1DoorRelLe_4_9, col3379) 
,ISNULL(@MIO_I1DoorRelRi_4_1, col3380) ,ISNULL(@MIO_I1DoorRelRi_4_10, col3381) ,ISNULL(@MIO_I1DoorRelRi_4_11, col3382) ,ISNULL(@MIO_I1DoorRelRi_4_12, col3383) ,ISNULL(@MIO_I1DoorRelRi_4_13, col3384) 
,ISNULL(@MIO_I1DoorRelRi_4_14, col3385) ,ISNULL(@MIO_I1DoorRelRi_4_15, col3386) ,ISNULL(@MIO_I1DoorRelRi_4_16, col3387) ,ISNULL(@MIO_I1DoorRelRi_4_17, col3388) ,ISNULL(@MIO_I1DoorRelRi_4_18, col3389) 
,ISNULL(@MIO_I1DoorRelRi_4_19, col3390) ,ISNULL(@MIO_I1DoorRelRi_4_2, col3391) ,ISNULL(@MIO_I1DoorRelRi_4_20, col3392) ,ISNULL(@MIO_I1DoorRelRi_4_3, col3393) ,ISNULL(@MIO_I1DoorRelRi_4_4, col3394) 
,ISNULL(@MIO_I1DoorRelRi_4_5, col3395) ,ISNULL(@MIO_I1DoorRelRi_4_6, col3396) ,ISNULL(@MIO_I1DoorRelRi_4_7, col3397) ,ISNULL(@MIO_I1DoorRelRi_4_8, col3398) ,ISNULL(@MIO_I1DoorRelRi_4_9, col3399) 
,ISNULL(@MIO_I2DoorRelLe_4_1, col3400) ,ISNULL(@MIO_I2DoorRelLe_4_10, col3401) ,ISNULL(@MIO_I2DoorRelLe_4_11, col3402) ,ISNULL(@MIO_I2DoorRelLe_4_12, col3403) ,ISNULL(@MIO_I2DoorRelLe_4_13, col3404) 
,ISNULL(@MIO_I2DoorRelLe_4_14, col3405) ,ISNULL(@MIO_I2DoorRelLe_4_15, col3406) ,ISNULL(@MIO_I2DoorRelLe_4_16, col3407) ,ISNULL(@MIO_I2DoorRelLe_4_17, col3408) ,ISNULL(@MIO_I2DoorRelLe_4_18, col3409) 
,ISNULL(@MIO_I2DoorRelLe_4_19, col3410) ,ISNULL(@MIO_I2DoorRelLe_4_2, col3411) ,ISNULL(@MIO_I2DoorRelLe_4_20, col3412) ,ISNULL(@MIO_I2DoorRelLe_4_3, col3413) ,ISNULL(@MIO_I2DoorRelLe_4_4, col3414) 
,ISNULL(@MIO_I2DoorRelLe_4_5, col3415) ,ISNULL(@MIO_I2DoorRelLe_4_6, col3416) ,ISNULL(@MIO_I2DoorRelLe_4_7, col3417) ,ISNULL(@MIO_I2DoorRelLe_4_8, col3418) ,ISNULL(@MIO_I2DoorRelLe_4_9, col3419) 
,ISNULL(@MIO_I2DoorRelRi_4_1, col3420) ,ISNULL(@MIO_I2DoorRelRi_4_10, col3421) ,ISNULL(@MIO_I2DoorRelRi_4_11, col3422) ,ISNULL(@MIO_I2DoorRelRi_4_12, col3423) ,ISNULL(@MIO_I2DoorRelRi_4_13, col3424) 
,ISNULL(@MIO_I2DoorRelRi_4_14, col3425) ,ISNULL(@MIO_I2DoorRelRi_4_15, col3426) ,ISNULL(@MIO_I2DoorRelRi_4_16, col3427) ,ISNULL(@MIO_I2DoorRelRi_4_17, col3428) ,ISNULL(@MIO_I2DoorRelRi_4_18, col3429) 
,ISNULL(@MIO_I2DoorRelRi_4_19, col3430) ,ISNULL(@MIO_I2DoorRelRi_4_2, col3431) ,ISNULL(@MIO_I2DoorRelRi_4_20, col3432) ,ISNULL(@MIO_I2DoorRelRi_4_3, col3433) ,ISNULL(@MIO_I2DoorRelRi_4_4, col3434) 
,ISNULL(@MIO_I2DoorRelRi_4_5, col3435) ,ISNULL(@MIO_I2DoorRelRi_4_6, col3436) ,ISNULL(@MIO_I2DoorRelRi_4_7, col3437) ,ISNULL(@MIO_I2DoorRelRi_4_8, col3438) ,ISNULL(@MIO_I2DoorRelRi_4_9, col3439) 
,ISNULL(@MIO_IDoorLoopBypa1_4_1, col3440) ,ISNULL(@MIO_IDoorLoopBypa1_4_10, col3441) ,ISNULL(@MIO_IDoorLoopBypa1_4_11, col3442) ,ISNULL(@MIO_IDoorLoopBypa1_4_12, col3443) ,ISNULL(@MIO_IDoorLoopBypa1_4_13, col3444) 
,ISNULL(@MIO_IDoorLoopBypa1_4_14, col3445) ,ISNULL(@MIO_IDoorLoopBypa1_4_15, col3446) ,ISNULL(@MIO_IDoorLoopBypa1_4_16, col3447) ,ISNULL(@MIO_IDoorLoopBypa1_4_17, col3448) ,ISNULL(@MIO_IDoorLoopBypa1_4_18, col3449) 
,ISNULL(@MIO_IDoorLoopBypa1_4_19, col3450) ,ISNULL(@MIO_IDoorLoopBypa1_4_2, col3451) ,ISNULL(@MIO_IDoorLoopBypa1_4_20, col3452) ,ISNULL(@MIO_IDoorLoopBypa1_4_3, col3453) ,ISNULL(@MIO_IDoorLoopBypa1_4_4, col3454) 
,ISNULL(@MIO_IDoorLoopBypa1_4_5, col3455) ,ISNULL(@MIO_IDoorLoopBypa1_4_6, col3456) ,ISNULL(@MIO_IDoorLoopBypa1_4_7, col3457) ,ISNULL(@MIO_IDoorLoopBypa1_4_8, col3458) ,ISNULL(@MIO_IDoorLoopBypa1_4_9, col3459) 
,ISNULL(@MIO_IDoorLoopBypa2_4_1, col3460) ,ISNULL(@MIO_IDoorLoopBypa2_4_10, col3461) ,ISNULL(@MIO_IDoorLoopBypa2_4_11, col3462) ,ISNULL(@MIO_IDoorLoopBypa2_4_12, col3463) ,ISNULL(@MIO_IDoorLoopBypa2_4_13, col3464) 
,ISNULL(@MIO_IDoorLoopBypa2_4_14, col3465) ,ISNULL(@MIO_IDoorLoopBypa2_4_15, col3466) ,ISNULL(@MIO_IDoorLoopBypa2_4_16, col3467) ,ISNULL(@MIO_IDoorLoopBypa2_4_17, col3468) ,ISNULL(@MIO_IDoorLoopBypa2_4_18, col3469) 
,ISNULL(@MIO_IDoorLoopBypa2_4_19, col3470) ,ISNULL(@MIO_IDoorLoopBypa2_4_2, col3471) ,ISNULL(@MIO_IDoorLoopBypa2_4_20, col3472) ,ISNULL(@MIO_IDoorLoopBypa2_4_3, col3473) ,ISNULL(@MIO_IDoorLoopBypa2_4_4, col3474) 
,ISNULL(@MIO_IDoorLoopBypa2_4_5, col3475) ,ISNULL(@MIO_IDoorLoopBypa2_4_6, col3476) ,ISNULL(@MIO_IDoorLoopBypa2_4_7, col3477) ,ISNULL(@MIO_IDoorLoopBypa2_4_8, col3478) ,ISNULL(@MIO_IDoorLoopBypa2_4_9, col3479) 
,ISNULL(@MIO_IDoorLoopCl1_4_1, col3480) ,ISNULL(@MIO_IDoorLoopCl1_4_10, col3481) ,ISNULL(@MIO_IDoorLoopCl1_4_11, col3482) ,ISNULL(@MIO_IDoorLoopCl1_4_12, col3483) ,ISNULL(@MIO_IDoorLoopCl1_4_13, col3484) 
,ISNULL(@MIO_IDoorLoopCl1_4_14, col3485) ,ISNULL(@MIO_IDoorLoopCl1_4_15, col3486) ,ISNULL(@MIO_IDoorLoopCl1_4_16, col3487) ,ISNULL(@MIO_IDoorLoopCl1_4_17, col3488) ,ISNULL(@MIO_IDoorLoopCl1_4_18, col3489) 
,ISNULL(@MIO_IDoorLoopCl1_4_19, col3490) ,ISNULL(@MIO_IDoorLoopCl1_4_2, col3491) ,ISNULL(@MIO_IDoorLoopCl1_4_20, col3492) ,ISNULL(@MIO_IDoorLoopCl1_4_3, col3493) ,ISNULL(@MIO_IDoorLoopCl1_4_4, col3494) 
,ISNULL(@MIO_IDoorLoopCl1_4_5, col3495) ,ISNULL(@MIO_IDoorLoopCl1_4_6, col3496) ,ISNULL(@MIO_IDoorLoopCl1_4_7, col3497) ,ISNULL(@MIO_IDoorLoopCl1_4_8, col3498) ,ISNULL(@MIO_IDoorLoopCl1_4_9, col3499) 
,ISNULL(@MIO_IDoorLoopCl2_4_1, col3500) ,ISNULL(@MIO_IDoorLoopCl2_4_10, col3501) ,ISNULL(@MIO_IDoorLoopCl2_4_11, col3502) ,ISNULL(@MIO_IDoorLoopCl2_4_12, col3503) ,ISNULL(@MIO_IDoorLoopCl2_4_13, col3504) 
,ISNULL(@MIO_IDoorLoopCl2_4_14, col3505) ,ISNULL(@MIO_IDoorLoopCl2_4_15, col3506) ,ISNULL(@MIO_IDoorLoopCl2_4_16, col3507) ,ISNULL(@MIO_IDoorLoopCl2_4_17, col3508) ,ISNULL(@MIO_IDoorLoopCl2_4_18, col3509) 
,ISNULL(@MIO_IDoorLoopCl2_4_19, col3510) ,ISNULL(@MIO_IDoorLoopCl2_4_2, col3511) ,ISNULL(@MIO_IDoorLoopCl2_4_20, col3512) ,ISNULL(@MIO_IDoorLoopCl2_4_3, col3513) ,ISNULL(@MIO_IDoorLoopCl2_4_4, col3514) 
,ISNULL(@MIO_IDoorLoopCl2_4_5, col3515) ,ISNULL(@MIO_IDoorLoopCl2_4_6, col3516) ,ISNULL(@MIO_IDoorLoopCl2_4_7, col3517) ,ISNULL(@MIO_IDoorLoopCl2_4_8, col3518) ,ISNULL(@MIO_IDoorLoopCl2_4_9, col3519) 
	FROM dbo.ChannelValueDoor WITH (NOLOCK) 
	WHERE UnitID = @UnitID
	ORDER BY 1 DESC

END
ELSE
BEGIN


UPDATE ChannelValue SET UpdateRecord = 1
,col1=ISNULL(@MCG_ILatitude, col1) ,col2=ISNULL(@MCG_ILongitude, col2) ,col3=ISNULL(@DIRECTION_CONTROL, col3),col4=ISNULL(@CURRENT_SPEED,col4),col5=ISNULL(@AUX1_CDeratingReq_1_1, col5) ,col6=ISNULL(@AUX1_I3ACContactorOn_1_1, col6) 
,col7=ISNULL(@AUX1_I3ACContactorOn_2_1, col7) ,col8=ISNULL(@AUX1_I3ACContactorOn_2_2, col8) ,col9=ISNULL(@AUX1_I3ACLoadEnabled_1_1, col9) ,col10=ISNULL(@AUX1_I3ACOk_1_1, col10) ,col11=ISNULL(@AUX1_IInputVoltageOk_1_1, col11) 
,col12=ISNULL(@AUX1_IInverterEnabled_1_1, col12) ,col13=ISNULL(@AUX1_ILineContactorOn_1_1, col13) ,col14=ISNULL(@AUX1_IOutputVoltageOk_1_1, col14) ,col15=ISNULL(@AUX1_IPreChargContOn_1_1, col15) ,col16=ISNULL(@AUX1_ITempHeatSinkOk_1_1, col16) 
,col17=ISNULL(@AUX2_CDeratingReq_1_2, col17) ,col18=ISNULL(@AUX2_I3ACContactorOn_1_2, col18) ,col19=ISNULL(@AUX2_I3ACContactorOn_2_1, col19) ,col20=ISNULL(@AUX2_I3ACContactorOn_2_2, col20) ,col21=ISNULL(@AUX2_I3ACLoadEnabled_1_2, col21) 
,col22=ISNULL(@AUX2_I3ACOk_1_2, col22) ,col23=ISNULL(@AUX2_IInputVoltageOk_1_2, col23) ,col24=ISNULL(@AUX2_IInverterEnabled_1_2, col24) ,col25=ISNULL(@AUX2_ILineContactorOn_1_2, col25) ,col26=ISNULL(@AUX2_IOutputVoltageOk_1_2, col26) 
,col27=ISNULL(@AUX2_IPreChargContOn_1_2, col27) ,col28=ISNULL(@AUX2_ITempHeatSinkOk_1_2, col28) ,col29=ISNULL(@AUXY_IBatCont1On, col29) ,col30=ISNULL(@AUXY_IBatCont1On_1_1, col30) ,col31=ISNULL(@AUXY_IBatCont1On_1_2, col31) 
,col32=ISNULL(@AUXY_IBatCont1On_2_1, col32) ,col33=ISNULL(@AUXY_IBatCont1On_2_2, col33) ,col34=ISNULL(@AUXY_IBatCont1On_3_1, col34) ,col35=ISNULL(@AUXY_IBatCont1On_3_2, col35) ,col36=ISNULL(@AUXY_IBatCont1On_8_1, col36) 
,col37=ISNULL(@AUXY_IBatCont1On_8_2, col37) ,col38=ISNULL(@AUXY_IBatCont1On_8_3, col38) ,col39=ISNULL(@AUXY_IBatCont1On_8_4, col39) ,col40=ISNULL(@AUXY_IBatCont1On_8_5, col40) ,col41=ISNULL(@AUXY_IBatCont1On_8_6, col41) 
,col42=ISNULL(@AUXY_IBatCont1On_8_7, col42) ,col43=ISNULL(@AUXY_IBatCont1On_8_8, col43) ,col44=ISNULL(@AUXY_IBatCont1On_9, col44) ,col45=ISNULL(@AUXY_IBatCont2On, col45) ,col46=ISNULL(@AUXY_IBatCont2On_1_1, col46) 
,col47=ISNULL(@AUXY_IBatCont2On_1_2, col47) ,col48=ISNULL(@AUXY_IBatCont2On_2_1, col48) ,col49=ISNULL(@AUXY_IBatCont2On_2_2, col49) ,col50=ISNULL(@AUXY_IBatCont2On_3_1, col50) ,col51=ISNULL(@AUXY_IBatCont2On_3_2, col51) 
,col52=ISNULL(@AUXY_IBatCont2On_8_1, col52) ,col53=ISNULL(@AUXY_IBatCont2On_8_2, col53) ,col54=ISNULL(@AUXY_IBatCont2On_8_3, col54) ,col55=ISNULL(@AUXY_IBatCont2On_8_4, col55) ,col56=ISNULL(@AUXY_IBatCont2On_8_5, col56) 
,col57=ISNULL(@AUXY_IBatCont2On_8_6, col57) ,col58=ISNULL(@AUXY_IBatCont2On_8_7, col58) ,col59=ISNULL(@AUXY_IBatCont2On_8_8, col59) ,col60=ISNULL(@AUXY_IBatCont2On_9, col60) ,col61=ISNULL(@BCHA1_I110VAuxPowSup_2_1, col61) 
,col62=ISNULL(@BCHA1_IBattCurr_2_1, col62) ,col63=ISNULL(@BCHA1_IBattOutputCurr_2_1, col63) ,col64=ISNULL(@BCHA1_IChargCurrMax_2_1, col64) ,col65=ISNULL(@BCHA1_IDcLinkVoltage_2_1, col65) ,col66=ISNULL(@BCHA1_IExternal400Vac, col66) 
,col67=ISNULL(@BCHA1_IExternal400Vac_1_1, col67) ,col68=ISNULL(@BCHA1_IExternal400Vac_1_2, col68) ,col69=ISNULL(@BCHA1_IExternal400Vac_2_1, col69) ,col70=ISNULL(@BCHA1_IExternal400Vac_2_2, col70) ,col71=ISNULL(@BCHA1_IExternal400Vac_3_1, col71) 
,col72=ISNULL(@BCHA1_IExternal400Vac_3_2, col72) ,col73=ISNULL(@BCHA1_IExternal400Vac_8_1, col73) ,col74=ISNULL(@BCHA1_IExternal400Vac_8_2, col74) ,col75=ISNULL(@BCHA1_IExternal400Vac_8_3, col75) ,col76=ISNULL(@BCHA1_IExternal400Vac_8_4, col76) 
,col77=ISNULL(@BCHA1_IExternal400Vac_8_5, col77) ,col78=ISNULL(@BCHA1_IExternal400Vac_8_6, col78) ,col79=ISNULL(@BCHA1_IExternal400Vac_8_7, col79) ,col80=ISNULL(@BCHA1_IExternal400Vac_8_8, col80) ,col81=ISNULL(@BCHA1_IExternal400Vac_9, col81) 
,col82=ISNULL(@BCHA1_IOutputPower_2_1, col82) ,col83=ISNULL(@BCHA1_ISpecSystNokMod_2_1, col83) ,col84=ISNULL(@BCHA1_ISpecSystOkMode_2_1, col84) ,col85=ISNULL(@BCHA1_ITempBattSens1_2_1, col85) ,col86=ISNULL(@BCHA1_ITempBattSens2_2_1, col86) 
,col87=ISNULL(@BCHA1_ITempHeatSink_2_1, col87) ,col88=ISNULL(@BCHA2_I110VAuxPowSup_2_2, col88) ,col89=ISNULL(@BCHA2_IBattCurr_2_2, col89) ,col90=ISNULL(@BCHA2_IBattOutputCurr_2_2, col90) ,col91=ISNULL(@BCHA2_IChargCurrMax_2_2, col91) 
,col92=ISNULL(@BCHA2_IDcLinkVoltage_2_2, col92) ,col93=ISNULL(@BCHA2_IExternal400Vac, col93) ,col94=ISNULL(@BCHA2_IExternal400Vac_1_1, col94) ,col95=ISNULL(@BCHA2_IExternal400Vac_1_2, col95) ,col96=ISNULL(@BCHA2_IExternal400Vac_2_1, col96) 
,col97=ISNULL(@BCHA2_IExternal400Vac_2_2, col97) ,col98=ISNULL(@BCHA2_IExternal400Vac_3_1, col98) ,col99=ISNULL(@BCHA2_IExternal400Vac_3_2, col99) ,col100=ISNULL(@BCHA2_IExternal400Vac_8_1, col100) ,col101=ISNULL(@BCHA2_IExternal400Vac_8_2, col101) 
,col102=ISNULL(@BCHA2_IExternal400Vac_8_3, col102) ,col103=ISNULL(@BCHA2_IExternal400Vac_8_4, col103) ,col104=ISNULL(@BCHA2_IExternal400Vac_8_5, col104) ,col105=ISNULL(@BCHA2_IExternal400Vac_8_6, col105) ,col106=ISNULL(@BCHA2_IExternal400Vac_8_7, col106) 
,col107=ISNULL(@BCHA2_IExternal400Vac_8_8, col107) ,col108=ISNULL(@BCHA2_IExternal400Vac_9, col108) ,col109=ISNULL(@BCHA2_IOutputPower_2_2, col109) ,col110=ISNULL(@BCHA2_ISpecSystNokMod_2_2, col110) ,col111=ISNULL(@BCHA2_ISpecSystOkMode_2_2, col111) 
,col112=ISNULL(@BCHA2_ITempBattSens1_2_2, col112) ,col113=ISNULL(@BCHA2_ITempBattSens2_2_2, col113) ,col114=ISNULL(@BCHA2_ITempHeatSink_2_2, col114) ,col115=ISNULL(@BCU1_IBrPipePressureInt_5_1, col115) ,col116=ISNULL(@BCU1_IMainAirResPipeInt_5_1, col116) 
,col117=ISNULL(@BCU1_IParkBrApplied_5_1, col117) ,col118=ISNULL(@BCU1_IParkBrLocked_5_1, col118) ,col119=ISNULL(@BCU1_IParkBrReleased_5_1, col119) ,col120=ISNULL(@BCU2_IBrPipePressureInt_5_2, col120) ,col121=ISNULL(@BCU2_IMainAirResPipeInt_5_2, col121) 
,col122=ISNULL(@BCU2_IParkBrApplied_5_2, col122) ,col123=ISNULL(@BCU2_IParkBrLocked_5_2, col123) ,col124=ISNULL(@BCU2_IParkBrReleased_5_2, col124) ,col125=ISNULL(@BCUm_IBrTestRun_5_1, col125) ,col126=ISNULL(@BCUm_IBrTestRun_5_2, col126) 
,col127=ISNULL(@BCUx_IBogie1Locked_5_1, col127) ,col128=ISNULL(@BCUx_IBogie1Locked_5_2, col128) ,col129=ISNULL(@BCUx_IBogie2Locked_5_1, col129) ,col130=ISNULL(@BCUx_IBogie2Locked_5_2, col130) ,col131=ISNULL(@BCUx_IBogie3Locked_5_1, col131) 
,col132=ISNULL(@BCUx_IBogie3Locked_5_2, col132) ,col133=ISNULL(@BCUx_IBogie4Locked_5_1, col133) ,col134=ISNULL(@BCUx_IBogie4Locked_5_2, col134) ,col135=ISNULL(@BCUx_IBogie5Locked_5_1, col135) ,col136=ISNULL(@BCUx_IBogie5Locked_5_2, col136) 
,col137=ISNULL(@BCUx_IBogie6Locked_5_1, col137) ,col138=ISNULL(@BCUx_IBogie6Locked_5_2, col138) ,col139=ISNULL(@BCUx_IBogie7Locked_5_1, col139) ,col140=ISNULL(@BCUx_IBogie7Locked_5_2, col140) ,col141=ISNULL(@BCUx_IWspBogie1Slide_3_1, col141) 
,col142=ISNULL(@BCUx_IWspBogie1Slide_3_2, col142) ,col143=ISNULL(@BCUx_IWspBogie1Slide_5_1, col143) ,col144=ISNULL(@BCUx_IWspBogie1Slide_5_2, col144) ,col145=ISNULL(@BCUx_IWspBogie2Slide_3_1, col145) ,col146=ISNULL(@BCUx_IWspBogie2Slide_3_2, col146) 
,col147=ISNULL(@BCUx_IWspBogie2Slide_5_1, col147) ,col148=ISNULL(@BCUx_IWspBogie2Slide_5_2, col148) ,col149=ISNULL(@BCUx_IWspBogie3Slide_3_1, col149) ,col150=ISNULL(@BCUx_IWspBogie3Slide_3_2, col150) ,col151=ISNULL(@BCUx_IWspBogie3Slide_5_1, col151) 
,col152=ISNULL(@BCUx_IWspBogie3Slide_5_2, col152) ,col153=ISNULL(@BCUx_IWspBogie4Slide_3_1, col153) ,col154=ISNULL(@BCUx_IWspBogie4Slide_3_2, col154) ,col155=ISNULL(@BCUx_IWspBogie4Slide_5_1, col155) ,col156=ISNULL(@BCUx_IWspBogie4Slide_5_2, col156) 
,col157=ISNULL(@BCUx_IWspBogie5Slide_3_1, col157) ,col158=ISNULL(@BCUx_IWspBogie5Slide_3_2, col158) ,col159=ISNULL(@BCUx_IWspBogie5Slide_5_1, col159) ,col160=ISNULL(@BCUx_IWspBogie5Slide_5_2, col160) ,col161=ISNULL(@BCUx_IWspBogie6Slide_3_1, col161) 
,col162=ISNULL(@BCUx_IWspBogie6Slide_3_2, col162) ,col163=ISNULL(@BCUx_IWspBogie6Slide_5_1, col163) ,col164=ISNULL(@BCUx_IWspBogie6Slide_5_2, col164) ,col165=ISNULL(@BCUx_IWspBogie7Slide_3_1, col165) ,col166=ISNULL(@BCUx_IWspBogie7Slide_3_2, col166) 
,col167=ISNULL(@BCUx_IWspBogie7Slide_5_1, col167) ,col168=ISNULL(@BCUx_IWspBogie7Slide_5_2, col168) ,col169=ISNULL(@CABIN, col169) ,col170=ISNULL(@CABIN_IST, col170) ,col171=ISNULL(@CABIN_SOLL, col171) 
,col172=ISNULL(@CABIN_STATUS, col172) ,col173=ISNULL(@CYCLE_COUNTER, col173) ,col174=ISNULL(@DBC_CBCU1ModeTowing_5_1, col174) ,col175=ISNULL(@DBC_CBCU1ModeTowing_5_2, col175) ,col176=ISNULL(@DBC_CBCU2ModeTowing_5_1, col176) 
,col177=ISNULL(@DBC_CBCU2ModeTowing_5_2, col177) ,col178=ISNULL(@DBC_Cbrake_M, col178) ,col179=ISNULL(@DBC_Cbrake_M_1_1, col179) ,col180=ISNULL(@DBC_Cbrake_M_1_2, col180) ,col181=ISNULL(@DBC_Cbrake_M_2_1, col181) 
,col182=ISNULL(@DBC_Cbrake_M_2_2, col182) ,col183=ISNULL(@DBC_Cbrake_M_3_1, col183) ,col184=ISNULL(@DBC_Cbrake_M_3_2, col184) ,col185=ISNULL(@DBC_Cbrake_M_4_1, col185) ,col186=ISNULL(@DBC_Cbrake_M_4_10, col186) 
,col187=ISNULL(@DBC_Cbrake_M_4_11, col187) ,col188=ISNULL(@DBC_Cbrake_M_4_12, col188) ,col189=ISNULL(@DBC_Cbrake_M_4_13, col189) ,col190=ISNULL(@DBC_Cbrake_M_4_14, col190) ,col191=ISNULL(@DBC_Cbrake_M_4_15, col191) 
,col192=ISNULL(@DBC_Cbrake_M_4_16, col192) ,col193=ISNULL(@DBC_Cbrake_M_4_17, col193) ,col194=ISNULL(@DBC_Cbrake_M_4_18, col194) ,col195=ISNULL(@DBC_Cbrake_M_4_19, col195) ,col196=ISNULL(@DBC_Cbrake_M_4_2, col196) 
,col197=ISNULL(@DBC_Cbrake_M_4_20, col197) ,col198=ISNULL(@DBC_Cbrake_M_4_3, col198) ,col199=ISNULL(@DBC_Cbrake_M_4_4, col199) ,col200=ISNULL(@DBC_Cbrake_M_4_5, col200) ,col201=ISNULL(@DBC_Cbrake_M_4_6, col201) 
,col202=ISNULL(@DBC_Cbrake_M_4_7, col202) ,col203=ISNULL(@DBC_Cbrake_M_4_8, col203) ,col204=ISNULL(@DBC_Cbrake_M_4_9, col204) ,col205=ISNULL(@DBC_Cbrake_M_5_1, col205) ,col206=ISNULL(@DBC_Cbrake_M_5_2, col206) 
,col207=ISNULL(@DBC_Cbrake_M_8_1, col207) ,col208=ISNULL(@DBC_Cbrake_M_8_2, col208) ,col209=ISNULL(@DBC_Cbrake_M_8_3, col209) ,col210=ISNULL(@DBC_Cbrake_M_8_4, col210) ,col211=ISNULL(@DBC_Cbrake_M_8_5, col211) 
,col212=ISNULL(@DBC_Cbrake_M_8_6, col212) ,col213=ISNULL(@DBC_Cbrake_M_8_7, col213) ,col214=ISNULL(@DBC_Cbrake_M_8_8, col214) ,col215=ISNULL(@DBC_CBrakeQuick_M, col215) ,col216=ISNULL(@DBC_CBrakeQuick_M_1_1, col216) 
,col217=ISNULL(@DBC_CBrakeQuick_M_1_2, col217) ,col218=ISNULL(@DBC_CBrakeQuick_M_2_1, col218) ,col219=ISNULL(@DBC_CBrakeQuick_M_2_2, col219) ,col220=ISNULL(@DBC_CBrakeQuick_M_3_1, col220) ,col221=ISNULL(@DBC_CBrakeQuick_M_3_2, col221) 
,col222=ISNULL(@DBC_CBrakeQuick_M_4_1, col222) ,col223=ISNULL(@DBC_CBrakeQuick_M_4_10, col223) ,col224=ISNULL(@DBC_CBrakeQuick_M_4_11, col224) ,col225=ISNULL(@DBC_CBrakeQuick_M_4_12, col225) ,col226=ISNULL(@DBC_CBrakeQuick_M_4_13, col226) 
,col227=ISNULL(@DBC_CBrakeQuick_M_4_14, col227) ,col228=ISNULL(@DBC_CBrakeQuick_M_4_15, col228) ,col229=ISNULL(@DBC_CBrakeQuick_M_4_16, col229) ,col230=ISNULL(@DBC_CBrakeQuick_M_4_17, col230) ,col231=ISNULL(@DBC_CBrakeQuick_M_4_18, col231) 
,col232=ISNULL(@DBC_CBrakeQuick_M_4_19, col232) ,col233=ISNULL(@DBC_CBrakeQuick_M_4_2, col233) ,col234=ISNULL(@DBC_CBrakeQuick_M_4_20, col234) ,col235=ISNULL(@DBC_CBrakeQuick_M_4_3, col235) ,col236=ISNULL(@DBC_CBrakeQuick_M_4_4, col236) 
,col237=ISNULL(@DBC_CBrakeQuick_M_4_5, col237) ,col238=ISNULL(@DBC_CBrakeQuick_M_4_6, col238) ,col239=ISNULL(@DBC_CBrakeQuick_M_4_7, col239) ,col240=ISNULL(@DBC_CBrakeQuick_M_4_8, col240) ,col241=ISNULL(@DBC_CBrakeQuick_M_4_9, col241) 
,col242=ISNULL(@DBC_CBrakeQuick_M_5_1, col242) ,col243=ISNULL(@DBC_CBrakeQuick_M_5_2, col243) ,col244=ISNULL(@DBC_CBrakeQuick_M_8_1, col244) ,col245=ISNULL(@DBC_CBrakeQuick_M_8_2, col245) ,col246=ISNULL(@DBC_CBrakeQuick_M_8_3, col246) 
,col247=ISNULL(@DBC_CBrakeQuick_M_8_4, col247) ,col248=ISNULL(@DBC_CBrakeQuick_M_8_5, col248) ,col249=ISNULL(@DBC_CBrakeQuick_M_8_6, col249) ,col250=ISNULL(@DBC_CBrakeQuick_M_8_7, col250) ,col251=ISNULL(@DBC_CBrakeQuick_M_8_8, col251) 
,col252=ISNULL(@DBC_CBrakingEffortInt_3_1, col252) ,col253=ISNULL(@DBC_CBrakingEffortInt_3_2, col253) ,col254=ISNULL(@DBC_CBrakingEffortInt_5_1, col254) ,col255=ISNULL(@DBC_CBrakingEffortInt_5_2, col255) ,col256=ISNULL(@DBC_Cdrive_M, col256) 
,col257=ISNULL(@DBC_Cdrive_M_1_1, col257) ,col258=ISNULL(@DBC_Cdrive_M_1_2, col258) ,col259=ISNULL(@DBC_Cdrive_M_2_1, col259) ,col260=ISNULL(@DBC_Cdrive_M_2_2, col260) ,col261=ISNULL(@DBC_Cdrive_M_3_1, col261) 
,col262=ISNULL(@DBC_Cdrive_M_3_2, col262) ,col263=ISNULL(@DBC_Cdrive_M_4_1, col263) ,col264=ISNULL(@DBC_Cdrive_M_4_10, col264) ,col265=ISNULL(@DBC_Cdrive_M_4_11, col265) ,col266=ISNULL(@DBC_Cdrive_M_4_12, col266) 
,col267=ISNULL(@DBC_Cdrive_M_4_13, col267) ,col268=ISNULL(@DBC_Cdrive_M_4_14, col268) ,col269=ISNULL(@DBC_Cdrive_M_4_15, col269) ,col270=ISNULL(@DBC_Cdrive_M_4_16, col270) ,col271=ISNULL(@DBC_Cdrive_M_4_17, col271) 
,col272=ISNULL(@DBC_Cdrive_M_4_18, col272) ,col273=ISNULL(@DBC_Cdrive_M_4_19, col273) ,col274=ISNULL(@DBC_Cdrive_M_4_2, col274) ,col275=ISNULL(@DBC_Cdrive_M_4_20, col275) ,col276=ISNULL(@DBC_Cdrive_M_4_3, col276) 
,col277=ISNULL(@DBC_Cdrive_M_4_4, col277) ,col278=ISNULL(@DBC_Cdrive_M_4_5, col278) ,col279=ISNULL(@DBC_Cdrive_M_4_6, col279) ,col280=ISNULL(@DBC_Cdrive_M_4_7, col280) ,col281=ISNULL(@DBC_Cdrive_M_4_8, col281) 
,col282=ISNULL(@DBC_Cdrive_M_4_9, col282) ,col283=ISNULL(@DBC_Cdrive_M_5_1, col283) ,col284=ISNULL(@DBC_Cdrive_M_5_2, col284) ,col285=ISNULL(@DBC_Cdrive_M_8_1, col285) ,col286=ISNULL(@DBC_Cdrive_M_8_2, col286) 
,col287=ISNULL(@DBC_Cdrive_M_8_3, col287) ,col288=ISNULL(@DBC_Cdrive_M_8_4, col288) ,col289=ISNULL(@DBC_Cdrive_M_8_5, col289) ,col290=ISNULL(@DBC_Cdrive_M_8_6, col290) ,col291=ISNULL(@DBC_Cdrive_M_8_7, col291) 
,col292=ISNULL(@DBC_Cdrive_M_8_8, col292) ,col293=ISNULL(@DBC_CModeTowing_M_5_1, col293) ,col294=ISNULL(@DBC_CModeTowing_M_5_2, col294) ,col295=ISNULL(@DBC_CTCU1GrInTracConv1_3_1, col295) ,col296=ISNULL(@DBC_CTCU2GrInTracConv2_3_2, col296) 
,col297=ISNULL(@DBC_CTractiveEffort_3_1, col297) ,col298=ISNULL(@DBC_CTractiveEffort_3_2, col298) ,col299=ISNULL(@DBC_ITrainSpeedInt, col299) ,col300=ISNULL(@DBC_ITrainSpeedInt_1_1, col300) ,col301=ISNULL(@DBC_ITrainSpeedInt_1_2, col301) 
,col302=ISNULL(@DBC_ITrainSpeedInt_15, col302) ,col303=ISNULL(@DBC_ITrainSpeedInt_2_1, col303) ,col304=ISNULL(@DBC_ITrainSpeedInt_2_2, col304) ,col305=ISNULL(@DBC_ITrainSpeedInt_3_1, col305) ,col306=ISNULL(@DBC_ITrainSpeedInt_3_2, col306) 
,col307=ISNULL(@DBC_ITrainSpeedInt_4_1, col307) ,col308=ISNULL(@DBC_ITrainSpeedInt_4_10, col308) ,col309=ISNULL(@DBC_ITrainSpeedInt_4_11, col309) ,col310=ISNULL(@DBC_ITrainSpeedInt_4_12, col310) ,col311=ISNULL(@DBC_ITrainSpeedInt_4_13, col311) 
,col312=ISNULL(@DBC_ITrainSpeedInt_4_14, col312) ,col313=ISNULL(@DBC_ITrainSpeedInt_4_15, col313) ,col314=ISNULL(@DBC_ITrainSpeedInt_4_16, col314) ,col315=ISNULL(@DBC_ITrainSpeedInt_4_17, col315) ,col316=ISNULL(@DBC_ITrainSpeedInt_4_18, col316) 
,col317=ISNULL(@DBC_ITrainSpeedInt_4_19, col317) ,col318=ISNULL(@DBC_ITrainSpeedInt_4_2, col318) ,col319=ISNULL(@DBC_ITrainSpeedInt_4_20, col319) ,col320=ISNULL(@DBC_ITrainSpeedInt_4_3, col320) ,col321=ISNULL(@DBC_ITrainSpeedInt_4_4, col321) 
,col322=ISNULL(@DBC_ITrainSpeedInt_4_5, col322) ,col323=ISNULL(@DBC_ITrainSpeedInt_4_6, col323) ,col324=ISNULL(@DBC_ITrainSpeedInt_4_7, col324) ,col325=ISNULL(@DBC_ITrainSpeedInt_4_8, col325) ,col326=ISNULL(@DBC_ITrainSpeedInt_4_9, col326) 
,col327=ISNULL(@DBC_ITrainSpeedInt_5_1, col327) ,col328=ISNULL(@DBC_ITrainSpeedInt_5_2, col328) ,col329=ISNULL(@DBC_ITrainSpeedInt_8_1, col329) ,col330=ISNULL(@DBC_ITrainSpeedInt_8_2, col330) ,col331=ISNULL(@DBC_ITrainSpeedInt_8_3, col331) 
,col332=ISNULL(@DBC_ITrainSpeedInt_8_4, col332) ,col333=ISNULL(@DBC_ITrainSpeedInt_8_5, col333) ,col334=ISNULL(@DBC_ITrainSpeedInt_8_6, col334) ,col335=ISNULL(@DBC_ITrainSpeedInt_8_7, col335) ,col336=ISNULL(@DBC_ITrainSpeedInt_8_8, col336) 
,col337=ISNULL(@DBC_ITrainSpeedInt_9, col337) ,col338=ISNULL(@DBC_IZrSpdInd_4_1, col338) ,col339=ISNULL(@DBC_IZrSpdInd_4_10, col339) ,col340=ISNULL(@DBC_IZrSpdInd_4_11, col340) ,col341=ISNULL(@DBC_IZrSpdInd_4_12, col341) 
,col342=ISNULL(@DBC_IZrSpdInd_4_13, col342) ,col343=ISNULL(@DBC_IZrSpdInd_4_14, col343) ,col344=ISNULL(@DBC_IZrSpdInd_4_15, col344) ,col345=ISNULL(@DBC_IZrSpdInd_4_16, col345) ,col346=ISNULL(@DBC_IZrSpdInd_4_17, col346) 
,col347=ISNULL(@DBC_IZrSpdInd_4_18, col347) ,col348=ISNULL(@DBC_IZrSpdInd_4_19, col348) ,col349=ISNULL(@DBC_IZrSpdInd_4_2, col349) ,col350=ISNULL(@DBC_IZrSpdInd_4_20, col350) ,col351=ISNULL(@DBC_IZrSpdInd_4_3, col351) 
,col352=ISNULL(@DBC_IZrSpdInd_4_4, col352) ,col353=ISNULL(@DBC_IZrSpdInd_4_5, col353) ,col354=ISNULL(@DBC_IZrSpdInd_4_6, col354) ,col355=ISNULL(@DBC_IZrSpdInd_4_7, col355) ,col356=ISNULL(@DBC_IZrSpdInd_4_8, col356) 
,col357=ISNULL(@DBC_IZrSpdInd_4_9, col357) ,col358=ISNULL(@DBC_PRTcu1GrOut_3_1, col358) ,col359=ISNULL(@DBC_PRTcu2GrOut_3_2, col359) ,col360=ISNULL(@DRIVERBRAKE_APPLIED_SUFF, col360) ,col361=ISNULL(@DRIVERBRAKE_OPERATED, col361) 
,col362=ISNULL(@EB_STATUS, col362) ,col363=ISNULL(@ERROR_CODE_INTERNAL, col363) ,col364=ISNULL(@ERRORCODE_1, col364) ,col365=ISNULL(@ERRORCODE_2, col365) ,col366=ISNULL(@ERRORCODE_3, col366) 
,col367=ISNULL(@GW_IGwOrientation, col367) ,col368=ISNULL(@HAC_ITempOutsideInt, col368) ,col369=ISNULL(@HAC_ITempOutsideInt_1_1, col369) ,col370=ISNULL(@HAC_ITempOutsideInt_1_2, col370) ,col371=ISNULL(@HAC_ITempOutsideInt_2_1, col371) 
,col372=ISNULL(@HAC_ITempOutsideInt_2_2, col372) ,col373=ISNULL(@HAC_ITempOutsideInt_3_1, col373) ,col374=ISNULL(@HAC_ITempOutsideInt_3_2, col374) ,col375=ISNULL(@HAC_ITempOutsideInt_4_1, col375) ,col376=ISNULL(@HAC_ITempOutsideInt_4_10, col376) 
,col377=ISNULL(@HAC_ITempOutsideInt_4_11, col377) ,col378=ISNULL(@HAC_ITempOutsideInt_4_12, col378) ,col379=ISNULL(@HAC_ITempOutsideInt_4_13, col379) ,col380=ISNULL(@HAC_ITempOutsideInt_4_14, col380) ,col381=ISNULL(@HAC_ITempOutsideInt_4_15, col381) 
,col382=ISNULL(@HAC_ITempOutsideInt_4_16, col382) ,col383=ISNULL(@HAC_ITempOutsideInt_4_17, col383) ,col384=ISNULL(@HAC_ITempOutsideInt_4_18, col384) ,col385=ISNULL(@HAC_ITempOutsideInt_4_19, col385) ,col386=ISNULL(@HAC_ITempOutsideInt_4_2, col386) 
,col387=ISNULL(@HAC_ITempOutsideInt_4_20, col387) ,col388=ISNULL(@HAC_ITempOutsideInt_4_3, col388) ,col389=ISNULL(@HAC_ITempOutsideInt_4_4, col389) ,col390=ISNULL(@HAC_ITempOutsideInt_4_5, col390) ,col391=ISNULL(@HAC_ITempOutsideInt_4_6, col391) 
,col392=ISNULL(@HAC_ITempOutsideInt_4_7, col392) ,col393=ISNULL(@HAC_ITempOutsideInt_4_8, col393) ,col394=ISNULL(@HAC_ITempOutsideInt_4_9, col394) ,col395=ISNULL(@HAC_ITempOutsideInt_5_1, col395) ,col396=ISNULL(@HAC_ITempOutsideInt_5_2, col396) 
,col397=ISNULL(@HAC_ITempOutsideInt_9, col397) ,col398=ISNULL(@HVAC1_ITempAirMixedInt, col398) ,col399=ISNULL(@HVAC1_ITempDuctAir1Int, col399) ,col400=ISNULL(@HVAC1_ITempDuctAir2Int, col400) ,col401=ISNULL(@HVAC1_ITempInComInt, col401) 
,col402=ISNULL(@HVAC1_ITempOutsideInt, col402) ,col403=ISNULL(@HVAC1_ITempSetpActDuctInt, col403) ,col404=ISNULL(@HVAC1_ITempSetpActInt, col404) ,col405=ISNULL(@HVAC2_ITempAirMixedInt, col405) ,col406=ISNULL(@HVAC2_ITempDuctAir1Int, col406) 
,col407=ISNULL(@HVAC2_ITempDuctAir2Int, col407) ,col408=ISNULL(@HVAC2_ITempInComInt, col408) ,col409=ISNULL(@HVAC2_ITempOutsideInt, col409) ,col410=ISNULL(@HVAC2_ITempSetpActDuctInt, col410) ,col411=ISNULL(@HVAC2_ITempSetpActInt, col411) 
,col412=ISNULL(@HVAC3_ITempAirMixedInt, col412) ,col413=ISNULL(@HVAC3_ITempDuctAir1Int, col413) ,col414=ISNULL(@HVAC3_ITempDuctAir2Int, col414) ,col415=ISNULL(@HVAC3_ITempInComInt, col415) ,col416=ISNULL(@HVAC3_ITempOutsideInt, col416) 
,col417=ISNULL(@HVAC3_ITempSetpActDuctInt, col417) ,col418=ISNULL(@HVAC3_ITempSetpActInt, col418) ,col419=ISNULL(@HVAC4_ITempAirMixedInt, col419) ,col420=ISNULL(@HVAC4_ITempDuctAir1Int, col420) ,col421=ISNULL(@HVAC4_ITempDuctAir2Int, col421) 
,col422=ISNULL(@HVAC4_ITempInComInt, col422) ,col423=ISNULL(@HVAC4_ITempOutsideInt, col423) ,col424=ISNULL(@HVAC4_ITempSetpActDuctInt, col424) ,col425=ISNULL(@HVAC4_ITempSetpActInt, col425) ,col426=ISNULL(@HVAC5_ITempAirMixedInt, col426) 
,col427=ISNULL(@HVAC5_ITempDuctAir1Int, col427) ,col428=ISNULL(@HVAC5_ITempDuctAir2Int, col428) ,col429=ISNULL(@HVAC5_ITempInComInt, col429) ,col430=ISNULL(@HVAC5_ITempOutsideInt, col430) ,col431=ISNULL(@HVAC5_ITempSetpActDuctInt, col431) 
,col432=ISNULL(@HVAC5_ITempSetpActInt, col432) ,col433=ISNULL(@HVAC6_ITempAirMixedInt, col433) ,col434=ISNULL(@HVAC6_ITempDuctAir1Int, col434) ,col435=ISNULL(@HVAC6_ITempDuctAir2Int, col435) ,col436=ISNULL(@HVAC6_ITempInComInt, col436) 
,col437=ISNULL(@HVAC6_ITempOutsideInt, col437) ,col438=ISNULL(@HVAC6_ITempSetpActDuctInt, col438) ,col439=ISNULL(@HVAC6_ITempSetpActInt, col439) ,col440=ISNULL(@HVCC1_ITempDuctAir1Int, col440) ,col441=ISNULL(@HVCC1_ITempInComInt, col441) 
,col442=ISNULL(@HVCC1_ITempOutsideInt, col442) ,col443=ISNULL(@HVCC1_ITempSetpActDuctInt, col443) ,col444=ISNULL(@HVCC1_ITempSetpActInt, col444) ,col445=ISNULL(@HVCC2_ITempDuctAir1Int, col445) ,col446=ISNULL(@HVCC2_ITempInComInt, col446) 
,col447=ISNULL(@HVCC2_ITempOutsideInt, col447) ,col448=ISNULL(@HVCC2_ITempSetpActDuctInt, col448) ,col449=ISNULL(@HVCC2_ITempSetpActInt, col449) ,col450=ISNULL(@INadiCtrlInfo1, col450) ,col451=ISNULL(@INadiCtrlInfo2, col451) 
,col452=ISNULL(@INadiCtrlInfo3, col452) ,col453=ISNULL(@INadiNumberEntries, col453) ,col454=ISNULL(@INadiTcnAddr1, col454) ,col455=ISNULL(@INadiTcnAddr2, col455) ,col456=ISNULL(@INadiTcnAddr3, col456) 
,col457=ISNULL(@INadiTopoCount, col457) ,col458=ISNULL(@INadiUicAddr1, col458) ,col459=ISNULL(@INadiUicAddr2, col459) ,col460=ISNULL(@INadiUicAddr3, col460) ,col461=ISNULL(@INTERNAL_ERROR_CODE, col461) 
,col462=ISNULL(@INTERNAL_STATE, col462) ,col463=ISNULL(@ITrainsetNumber1, col463) ,col464=ISNULL(@ITrainsetNumber2, col464) ,col465=ISNULL(@ITrainsetNumber3, col465) ,col466=ISNULL(@MCG_IGsmSigStrength, col466) 
,col467=ISNULL(@MIO_IContBusBar400On, col467) ,col468=ISNULL(@MIO_IContBusBar400On_1_1, col468) ,col469=ISNULL(@MIO_IContBusBar400On_1_2, col469) ,col470=ISNULL(@MIO_IContBusBar400On_2_1, col470) ,col471=ISNULL(@MIO_IContBusBar400On_2_2, col471) 
,col472=ISNULL(@MIO_IContBusBar400On_3_1, col472) ,col473=ISNULL(@MIO_IContBusBar400On_3_2, col473) ,col474=ISNULL(@MIO_IContBusBar400On_8_1, col474) ,col475=ISNULL(@MIO_IContBusBar400On_8_2, col475) ,col476=ISNULL(@MIO_IContBusBar400On_8_3, col476) 
,col477=ISNULL(@MIO_IContBusBar400On_8_4, col477) ,col478=ISNULL(@MIO_IContBusBar400On_8_5, col478) ,col479=ISNULL(@MIO_IContBusBar400On_8_6, col479) ,col480=ISNULL(@MIO_IContBusBar400On_8_7, col480) ,col481=ISNULL(@MIO_IContBusBar400On_8_8, col481) 
,col482=ISNULL(@MIO_IContBusBar400On_9, col482) ,col483=ISNULL(@MIO_IHscbOff, col483) ,col484=ISNULL(@MIO_IHscbOff_1_1, col484) ,col485=ISNULL(@MIO_IHscbOff_1_2, col485) ,col486=ISNULL(@MIO_IHscbOff_2_1, col486) 
,col487=ISNULL(@MIO_IHscbOff_2_2, col487) ,col488=ISNULL(@MIO_IHscbOff_3_1, col488) ,col489=ISNULL(@MIO_IHscbOff_3_2, col489) ,col490=ISNULL(@MIO_IHscbOff_8_1, col490) ,col491=ISNULL(@MIO_IHscbOff_8_2, col491) 
,col492=ISNULL(@MIO_IHscbOff_8_3, col492) ,col493=ISNULL(@MIO_IHscbOff_8_4, col493) ,col494=ISNULL(@MIO_IHscbOff_8_5, col494) ,col495=ISNULL(@MIO_IHscbOff_8_6, col495) ,col496=ISNULL(@MIO_IHscbOff_8_7, col496) 
,col497=ISNULL(@MIO_IHscbOff_8_8, col497) ,col498=ISNULL(@MIO_IHscbOn, col498) ,col499=ISNULL(@MIO_IHscbOn_1_1, col499) ,col500=ISNULL(@MIO_IHscbOn_1_2, col500) ,col501=ISNULL(@MIO_IHscbOn_2_1, col501) 
,col502=ISNULL(@MIO_IHscbOn_2_2, col502) ,col503=ISNULL(@MIO_IHscbOn_3_1, col503) ,col504=ISNULL(@MIO_IHscbOn_3_2, col504) ,col505=ISNULL(@MIO_IHscbOn_8_1, col505) ,col506=ISNULL(@MIO_IHscbOn_8_2, col506) 
,col507=ISNULL(@MIO_IHscbOn_8_3, col507) ,col508=ISNULL(@MIO_IHscbOn_8_4, col508) ,col509=ISNULL(@MIO_IHscbOn_8_5, col509) ,col510=ISNULL(@MIO_IHscbOn_8_6, col510) ,col511=ISNULL(@MIO_IHscbOn_8_7, col511) 
,col512=ISNULL(@MIO_IHscbOn_8_8, col512) ,col513=ISNULL(@MIO_IPantoPrssSw1On, col513) ,col514=ISNULL(@MIO_IPantoPrssSw1On_1_1, col514) ,col515=ISNULL(@MIO_IPantoPrssSw1On_1_2, col515) ,col516=ISNULL(@MIO_IPantoPrssSw1On_2_1, col516) 
,col517=ISNULL(@MIO_IPantoPrssSw1On_2_2, col517) ,col518=ISNULL(@MIO_IPantoPrssSw1On_3_1, col518) ,col519=ISNULL(@MIO_IPantoPrssSw1On_3_2, col519) ,col520=ISNULL(@MIO_IPantoPrssSw1On_8_1, col520) ,col521=ISNULL(@MIO_IPantoPrssSw1On_8_2, col521) 
,col522=ISNULL(@MIO_IPantoPrssSw1On_8_3, col522) ,col523=ISNULL(@MIO_IPantoPrssSw1On_8_4, col523) ,col524=ISNULL(@MIO_IPantoPrssSw1On_8_5, col524) ,col525=ISNULL(@MIO_IPantoPrssSw1On_8_6, col525) ,col526=ISNULL(@MIO_IPantoPrssSw1On_8_7, col526) 
,col527=ISNULL(@MIO_IPantoPrssSw1On_8_8, col527) ,col528=ISNULL(@MIO_IPantoPrssSw1On_9, col528) ,col529=ISNULL(@MIO_IPantoPrssSw2On, col529) ,col530=ISNULL(@MIO_IPantoPrssSw2On_1_1, col530) ,col531=ISNULL(@MIO_IPantoPrssSw2On_1_2, col531) 
,col532=ISNULL(@MIO_IPantoPrssSw2On_2_1, col532) ,col533=ISNULL(@MIO_IPantoPrssSw2On_2_2, col533) ,col534=ISNULL(@MIO_IPantoPrssSw2On_3_1, col534) ,col535=ISNULL(@MIO_IPantoPrssSw2On_3_2, col535) ,col536=ISNULL(@MIO_IPantoPrssSw2On_8_1, col536) 
,col537=ISNULL(@MIO_IPantoPrssSw2On_8_2, col537) ,col538=ISNULL(@MIO_IPantoPrssSw2On_8_3, col538) ,col539=ISNULL(@MIO_IPantoPrssSw2On_8_4, col539) ,col540=ISNULL(@MIO_IPantoPrssSw2On_8_5, col540) ,col541=ISNULL(@MIO_IPantoPrssSw2On_8_6, col541) 
,col542=ISNULL(@MIO_IPantoPrssSw2On_8_7, col542) ,col543=ISNULL(@MIO_IPantoPrssSw2On_8_8, col543) ,col544=ISNULL(@MIO_IPantoPrssSw2On_9, col544) ,col545=ISNULL(@MMI_EB_STATUS, col545) ,col546=ISNULL(@MMI_M_ACTIVE_CABIN, col546) 
,col547=ISNULL(@MMI_M_LEVEL, col547) ,col548=ISNULL(@MMI_M_MODE, col548) ,col549=ISNULL(@MMI_M_WARNING, col549) ,col550=ISNULL(@MMI_SB_STATUS, col550) ,col551=ISNULL(@MMI_V_PERMITTED, col551) 
,col552=ISNULL(@MMI_V_TRAIN, col552) ,col553=ISNULL(@NID_STM_1, col553) ,col554=ISNULL(@NID_STM_2, col554) ,col555=ISNULL(@NID_STM_3, col555) ,col556=ISNULL(@NID_STM_4, col556) 
,col557=ISNULL(@NID_STM_5, col557) ,col558=ISNULL(@NID_STM_6, col558) ,col559=ISNULL(@NID_STM_7, col559) ,col560=ISNULL(@NID_STM_8, col560) ,col561=ISNULL(@NID_STM_DA, col561) 
,col562=ISNULL(@ONE_STM_DA, col562) ,col563=ISNULL(@ONE_STM_HS, col563) ,col564=ISNULL(@PB_ADDR, col564) ,col565=ISNULL(@PERMITTED_SPEED, col565) ,col566=ISNULL(@Q_ODO, col566) 
,col567=ISNULL(@REPORTED_DIRECTION, col567) ,col568=ISNULL(@REPORTED_SPEED, col568) ,col569=ISNULL(@REPORTED_SPEED_HISTORIC, col569) ,col570=ISNULL(@ROTATION_DIRECTION_SDU1, col570) ,col571=ISNULL(@ROTATION_DIRECTION_SDU2, col571) 
,col572=ISNULL(@SAP, col572) ,col573=ISNULL(@SB_STATUS, col573) ,col574=ISNULL(@SDU_DIRECTION_SDU1, col574) ,col575=ISNULL(@SDU_DIRECTION_SDU2, col575) ,col576=ISNULL(@SENSOR_ERROR_STATUS1_TACHO1, col576) 
,col577=ISNULL(@SENSOR_ERROR_STATUS2_TACHO2, col577) ,col578=ISNULL(@SENSOR_ERROR_STATUS3_RADAR1, col578) ,col579=ISNULL(@SENSOR_ERROR_STATUS4_RADAR2, col579) ,col580=ISNULL(@SENSOR_SPEED_HISTORIC_RADAR1, col580) ,col581=ISNULL(@SENSOR_SPEED_HISTORIC_RADAR2, col581) 
,col582=ISNULL(@SENSOR_SPEED_HISTORIC_TACHO1, col582) ,col583=ISNULL(@SENSOR_SPEED_HISTORIC_TACHO2, col583) ,col584=ISNULL(@SENSOR_SPEED_RADAR1, col584) ,col585=ISNULL(@SENSOR_SPEED_RADAR2, col585) ,col586=ISNULL(@SENSOR_SPEED_TACHO1, col586) 
,col587=ISNULL(@SENSOR_SPEED_TACHO2, col587) ,col588=ISNULL(@SLIP_SLIDE_ACC_AXLE1, col588) ,col589=ISNULL(@SLIP_SLIDE_ACC_AXLE2, col589) ,col590=ISNULL(@SLIP_SLIDE_AXLE1, col590) ,col591=ISNULL(@SLIP_SLIDE_AXLE2, col591) 
,col592=ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE1, col592) ,col593=ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE2, col593) ,col594=ISNULL(@SLIP_SLIDE_STATUS, col594) ,col595=ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE1, col595) ,col596=ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE2, col596) 
,col597=ISNULL(@SOFTWARE_VERSION, col597) ,col598=ISNULL(@SPL_FUNC_ID, col598) ,col599=ISNULL(@SPL_RETURN, col599) ,col600=ISNULL(@STM_STATE, col600) ,col601=ISNULL(@STM_SUB_STATE, col601) 
,col602=ISNULL(@SUPERVISION_STATE, col602) ,col603=ISNULL(@TC_CCorrectOrient_M, col603) ,col604=ISNULL(@TC_IConsistOrient, col604) ,col605=ISNULL(@TC_IGwOrient, col605) ,col606=ISNULL(@TC_INumConsists, col606) 
,col607=ISNULL(@TC_INumConsists_1_1, col607) ,col608=ISNULL(@TC_INumConsists_1_2, col608) ,col609=ISNULL(@TC_INumConsists_2_1, col609) ,col610=ISNULL(@TC_INumConsists_2_2, col610) ,col611=ISNULL(@TC_INumConsists_3_1, col611) 
,col612=ISNULL(@TC_INumConsists_3_2, col612) ,col613=ISNULL(@TC_INumConsists_4_1, col613) ,col614=ISNULL(@TC_INumConsists_4_10, col614) ,col615=ISNULL(@TC_INumConsists_4_11, col615) ,col616=ISNULL(@TC_INumConsists_4_12, col616) 
,col617=ISNULL(@TC_INumConsists_4_13, col617) ,col618=ISNULL(@TC_INumConsists_4_14, col618) ,col619=ISNULL(@TC_INumConsists_4_15, col619) ,col620=ISNULL(@TC_INumConsists_4_16, col620) ,col621=ISNULL(@TC_INumConsists_4_17, col621) 
,col622=ISNULL(@TC_INumConsists_4_18, col622) ,col623=ISNULL(@TC_INumConsists_4_19, col623) ,col624=ISNULL(@TC_INumConsists_4_2, col624) ,col625=ISNULL(@TC_INumConsists_4_20, col625) ,col626=ISNULL(@TC_INumConsists_4_3, col626) 
,col627=ISNULL(@TC_INumConsists_4_4, col627) ,col628=ISNULL(@TC_INumConsists_4_5, col628) ,col629=ISNULL(@TC_INumConsists_4_6, col629) ,col630=ISNULL(@TC_INumConsists_4_7, col630) ,col631=ISNULL(@TC_INumConsists_4_8, col631) 
,col632=ISNULL(@TC_INumConsists_4_9, col632) ,col633=ISNULL(@TC_INumConsists_5_1, col633) ,col634=ISNULL(@TC_INumConsists_5_2, col634) ,col635=ISNULL(@TC_INumConsists_8_1, col635) ,col636=ISNULL(@TC_INumConsists_8_2, col636) 
,col637=ISNULL(@TC_INumConsists_8_3, col637) ,col638=ISNULL(@TC_INumConsists_8_4, col638) ,col639=ISNULL(@TC_INumConsists_8_5, col639) ,col640=ISNULL(@TC_INumConsists_8_6, col640) ,col641=ISNULL(@TC_INumConsists_8_7, col641) 
,col642=ISNULL(@TC_INumConsists_8_8, col642) ,col643=ISNULL(@TC_INumConsists_9, col643) ,col644=ISNULL(@TC_ITclOrient, col644) ,col645=ISNULL(@TCO_STATUS, col645) ,col646=ISNULL(@TCU1_IEdSliding_3_1, col646) 
,col647=ISNULL(@TCU1_IEdSliding_5_1, col647) ,col648=ISNULL(@TCU1_ILineVoltage_3_1, col648) ,col649=ISNULL(@TCU1_ILineVoltage_5_1, col649) ,col650=ISNULL(@TCU1_ILineVoltage_5_2, col650) ,col651=ISNULL(@TCU1_IPowerLimit_3_1, col651) 
,col652=ISNULL(@TCU1_ITracBrActualInt_3_1, col652) ,col653=ISNULL(@TCU2_IEdSliding_3_2, col653) ,col654=ISNULL(@TCU2_IEdSliding_5_2, col654) ,col655=ISNULL(@TCU2_ILineVoltage_3_2, col655) ,col656=ISNULL(@TCU2_ILineVoltage_5_1, col656) 
,col657=ISNULL(@TCU2_ILineVoltage_5_2, col657) ,col658=ISNULL(@TCU2_IPowerLimit_3_2, col658) ,col659=ISNULL(@TCU2_ITracBrActualInt_3_2, col659) ,col660=ISNULL(@TRACK_SIGNAL, col660) ,col661=ISNULL(@DBC_PRTcu1GrOut, col661) 
,col662=ISNULL(@DBC_PRTcu2GrOut, col662) ,col663=ISNULL(@MPW_PRHscbEnable, col663) ,col664=ISNULL(@MPW_IPnt1UpDrvCount, col664) ,col665=ISNULL(@MPW_IPnt2UpDrvCount, col665) ,col666=ISNULL(@MPW_IPnt1UpCount, col666) 
,col667=ISNULL(@MPW_IPnt2UpCount, col667) ,col668=ISNULL(@MPW_IPnt1KmCount, col668) ,col669=ISNULL(@MPW_IPnt2KmCount, col669) ,col670=ISNULL(@DBC_IKmCounter, col670) ,col671=ISNULL(@AUXY_IAuxAirCOnCnt, col671) 
,col672=ISNULL(@MPW_IOnHscb, col672) ,col673=ISNULL(@AUXY_IAirCSwOnCount, col673) ,col674=ISNULL(@AUXY_IAirCOnCount, col674) ,col675=ISNULL(@AUXY_IAuxAirCSwOnCnt, col675) ,col676=ISNULL(@MPW_PRPantoEnable, col676) 
,col677=ISNULL(@DIA_Reserve1_AUX1, col677) ,col678=ISNULL(@DIA_Reserve1_AUX2, col678) ,col679=ISNULL(@DIA_Reserve1_BCHA_1, col679) ,col680=ISNULL(@DIA_Reserve1_BCHA_2, col680) ,col681=ISNULL(@DIA_Reserve1_BCU_1, col681)
,col682=ISNULL(@DIA_Reserve1_BCU_2, col682) ,col683=ISNULL(@DIA_Reserve1_CCUO, col683) ,col684=ISNULL(@DIA_Reserve1_DCU_1, col684) ,col685=ISNULL(@DIA_Reserve1_DCU_10, col685) ,col686=ISNULL(@DIA_Reserve1_DCU_11, col686)
,col687=ISNULL(@DIA_Reserve1_DCU_12, col687) ,col688=ISNULL(@DIA_Reserve1_DCU_13, col688) ,col689=ISNULL(@DIA_Reserve1_DCU_14, col689) ,col690=ISNULL(@DIA_Reserve1_DCU_15, col690) ,col691=ISNULL(@DIA_Reserve1_DCU_16, col691)
,col692=ISNULL(@DIA_Reserve1_DCU_17, col692) ,col693=ISNULL(@DIA_Reserve1_DCU_18, col693) ,col694=ISNULL(@DIA_Reserve1_DCU_19, col694) ,col695=ISNULL(@DIA_Reserve1_DCU_2, col695) ,col696=ISNULL(@DIA_Reserve1_DCU_20, col696)
,col697=ISNULL(@DIA_Reserve1_DCU_3, col697) ,col698=ISNULL(@DIA_Reserve1_DCU_4, col698) ,col699=ISNULL(@DIA_Reserve1_DCU_5, col699) ,col700=ISNULL(@DIA_Reserve1_DCU_6, col700) ,col701=ISNULL(@DIA_Reserve1_DCU_7, col701)
,col702=ISNULL(@DIA_Reserve1_DCU_8, col702) ,col703=ISNULL(@DIA_Reserve1_DCU_9, col703) ,col704=ISNULL(@DIA_Reserve1_HVAC_1, col704) ,col705=ISNULL(@DIA_Reserve1_HVAC_2, col705) ,col706=ISNULL(@DIA_Reserve1_HVAC_3, col706)
,col707=ISNULL(@DIA_Reserve1_HVAC_4, col707) ,col708=ISNULL(@DIA_Reserve1_HVAC_5, col708) ,col709=ISNULL(@DIA_Reserve1_HVAC_6, col709) ,col710=ISNULL(@DIA_Reserve1_HVCC_1, col710) ,col711=ISNULL(@DIA_Reserve1_HVCC_2, col711)
,col712=ISNULL(@DIA_Reserve1_PIS, col712) ,col713=ISNULL(@DIA_Reserve1_TCU_1, col713) ,col714=ISNULL(@DIA_Reserve1_TCU_2, col714) ,col715=ISNULL(@reserved, col715)
Where Id = @ID

UPDATE ChannelValueDoor SET UpdateRecord = 1
,col3000=ISNULL(@DCU01_CDoorCloseConduc, col3000) ,col3001=ISNULL(@DCU01_IDoorClosedSafe, col3001) ,col3002=ISNULL(@DCU01_IDoorClRelease, col3002) ,col3003=ISNULL(@DCU01_IDoorPbClose, col3003) ,col3004=ISNULL(@DCU01_IDoorPbOpenIn, col3004) 
,col3005=ISNULL(@DCU01_IDoorPbOpenOut, col3005) ,col3006=ISNULL(@DCU01_IDoorReleased, col3006) ,col3007=ISNULL(@DCU01_IFootStepRel, col3007) ,col3008=ISNULL(@DCU01_IForcedClosDoor, col3008) ,col3009=ISNULL(@DCU01_ILeafsStopDoor, col3009) 
,col3010=ISNULL(@DCU01_IObstacleDoor, col3010) ,col3011=ISNULL(@DCU01_IObstacleStep, col3011) ,col3012=ISNULL(@DCU01_IOpenAssistDoor, col3012) ,col3013=ISNULL(@DCU01_IStandstillBack, col3013) ,col3014=ISNULL(@DCU02_CDoorCloseConduc, col3014) 
,col3015=ISNULL(@DCU02_IDoorClosedSafe, col3015) ,col3016=ISNULL(@DCU02_IDoorClRelease, col3016) ,col3017=ISNULL(@DCU02_IDoorPbClose, col3017) ,col3018=ISNULL(@DCU02_IDoorPbOpenIn, col3018) ,col3019=ISNULL(@DCU02_IDoorPbOpenOut, col3019) 
,col3020=ISNULL(@DCU02_IDoorReleased, col3020) ,col3021=ISNULL(@DCU02_IFootStepRel, col3021) ,col3022=ISNULL(@DCU02_IForcedClosDoor, col3022) ,col3023=ISNULL(@DCU02_ILeafsStopDoor, col3023) ,col3024=ISNULL(@DCU02_IObstacleDoor, col3024) 
,col3025=ISNULL(@DCU02_IObstacleStep, col3025) ,col3026=ISNULL(@DCU02_IOpenAssistDoor, col3026) ,col3027=ISNULL(@DCU02_IStandstillBack, col3027) ,col3028=ISNULL(@DCU03_CDoorCloseConduc, col3028) ,col3029=ISNULL(@DCU03_IDoorClosedSafe, col3029) 
,col3030=ISNULL(@DCU03_IDoorClRelease, col3030) ,col3031=ISNULL(@DCU03_IDoorPbClose, col3031) ,col3032=ISNULL(@DCU03_IDoorPbOpenIn, col3032) ,col3033=ISNULL(@DCU03_IDoorPbOpenOut, col3033) ,col3034=ISNULL(@DCU03_IDoorReleased, col3034) 
,col3035=ISNULL(@DCU03_IFootStepRel, col3035) ,col3036=ISNULL(@DCU03_IForcedClosDoor, col3036) ,col3037=ISNULL(@DCU03_ILeafsStopDoor, col3037) ,col3038=ISNULL(@DCU03_IObstacleDoor, col3038) ,col3039=ISNULL(@DCU03_IObstacleStep, col3039) 
,col3040=ISNULL(@DCU03_IOpenAssistDoor, col3040) ,col3041=ISNULL(@DCU03_IStandstillBack, col3041) ,col3042=ISNULL(@DCU04_CDoorCloseConduc, col3042) ,col3043=ISNULL(@DCU04_IDoorClosedSafe, col3043) ,col3044=ISNULL(@DCU04_IDoorClRelease, col3044) 
,col3045=ISNULL(@DCU04_IDoorPbClose, col3045) ,col3046=ISNULL(@DCU04_IDoorPbOpenIn, col3046) ,col3047=ISNULL(@DCU04_IDoorPbOpenOut, col3047) ,col3048=ISNULL(@DCU04_IDoorReleased, col3048) ,col3049=ISNULL(@DCU04_IFootStepRel, col3049) 
,col3050=ISNULL(@DCU04_IForcedClosDoor, col3050) ,col3051=ISNULL(@DCU04_ILeafsStopDoor, col3051) ,col3052=ISNULL(@DCU04_IObstacleDoor, col3052) ,col3053=ISNULL(@DCU04_IObstacleStep, col3053) ,col3054=ISNULL(@DCU04_IOpenAssistDoor, col3054) 
,col3055=ISNULL(@DCU04_IStandstillBack, col3055) ,col3056=ISNULL(@DCU05_CDoorCloseConduc, col3056) ,col3057=ISNULL(@DCU05_IDoorClosedSafe, col3057) ,col3058=ISNULL(@DCU05_IDoorClRelease, col3058) ,col3059=ISNULL(@DCU05_IDoorPbClose, col3059) 
,col3060=ISNULL(@DCU05_IDoorPbOpenIn, col3060) ,col3061=ISNULL(@DCU05_IDoorPbOpenOut, col3061) ,col3062=ISNULL(@DCU05_IDoorReleased, col3062) ,col3063=ISNULL(@DCU05_IFootStepRel, col3063) ,col3064=ISNULL(@DCU05_IForcedClosDoor, col3064) 
,col3065=ISNULL(@DCU05_ILeafsStopDoor, col3065) ,col3066=ISNULL(@DCU05_IObstacleDoor, col3066) ,col3067=ISNULL(@DCU05_IObstacleStep, col3067) ,col3068=ISNULL(@DCU05_IOpenAssistDoor, col3068) ,col3069=ISNULL(@DCU05_IStandstillBack, col3069) 
,col3070=ISNULL(@DCU06_CDoorCloseConduc, col3070) ,col3071=ISNULL(@DCU06_IDoorClosedSafe, col3071) ,col3072=ISNULL(@DCU06_IDoorClRelease, col3072) ,col3073=ISNULL(@DCU06_IDoorPbClose, col3073) ,col3074=ISNULL(@DCU06_IDoorPbOpenIn, col3074) 
,col3075=ISNULL(@DCU06_IDoorPbOpenOut, col3075) ,col3076=ISNULL(@DCU06_IDoorReleased, col3076) ,col3077=ISNULL(@DCU06_IFootStepRel, col3077) ,col3078=ISNULL(@DCU06_IForcedClosDoor, col3078) ,col3079=ISNULL(@DCU06_ILeafsStopDoor, col3079) 
,col3080=ISNULL(@DCU06_IObstacleDoor, col3080) ,col3081=ISNULL(@DCU06_IObstacleStep, col3081) ,col3082=ISNULL(@DCU06_IOpenAssistDoor, col3082) ,col3083=ISNULL(@DCU06_IStandstillBack, col3083) ,col3084=ISNULL(@DCU07_CDoorCloseConduc, col3084) 
,col3085=ISNULL(@DCU07_IDoorClosedSafe, col3085) ,col3086=ISNULL(@DCU07_IDoorClRelease, col3086) ,col3087=ISNULL(@DCU07_IDoorPbClose, col3087) ,col3088=ISNULL(@DCU07_IDoorPbOpenIn, col3088) ,col3089=ISNULL(@DCU07_IDoorPbOpenOut, col3089) 
,col3090=ISNULL(@DCU07_IDoorReleased, col3090) ,col3091=ISNULL(@DCU07_IFootStepRel, col3091) ,col3092=ISNULL(@DCU07_IForcedClosDoor, col3092) ,col3093=ISNULL(@DCU07_ILeafsStopDoor, col3093) ,col3094=ISNULL(@DCU07_IObstacleDoor, col3094) 
,col3095=ISNULL(@DCU07_IObstacleStep, col3095) ,col3096=ISNULL(@DCU07_IOpenAssistDoor, col3096) ,col3097=ISNULL(@DCU07_IStandstillBack, col3097) ,col3098=ISNULL(@DCU08_CDoorCloseConduc, col3098) ,col3099=ISNULL(@DCU08_IDoorClosedSafe, col3099) 
,col3100=ISNULL(@DCU08_IDoorClRelease, col3100) ,col3101=ISNULL(@DCU08_IDoorPbClose, col3101) ,col3102=ISNULL(@DCU08_IDoorPbOpenIn, col3102) ,col3103=ISNULL(@DCU08_IDoorPbOpenOut, col3103) ,col3104=ISNULL(@DCU08_IDoorReleased, col3104) 
,col3105=ISNULL(@DCU08_IFootStepRel, col3105) ,col3106=ISNULL(@DCU08_IForcedClosDoor, col3106) ,col3107=ISNULL(@DCU08_ILeafsStopDoor, col3107) ,col3108=ISNULL(@DCU08_IObstacleDoor, col3108) ,col3109=ISNULL(@DCU08_IObstacleStep, col3109) 
,col3110=ISNULL(@DCU08_IOpenAssistDoor, col3110) ,col3111=ISNULL(@DCU08_IStandstillBack, col3111) ,col3112=ISNULL(@DCU09_CDoorCloseConduc, col3112) ,col3113=ISNULL(@DCU09_IDoorClosedSafe, col3113) ,col3114=ISNULL(@DCU09_IDoorClRelease, col3114) 
,col3115=ISNULL(@DCU09_IDoorPbClose, col3115) ,col3116=ISNULL(@DCU09_IDoorPbOpenIn, col3116) ,col3117=ISNULL(@DCU09_IDoorPbOpenOut, col3117) ,col3118=ISNULL(@DCU09_IDoorReleased, col3118) ,col3119=ISNULL(@DCU09_IFootStepRel, col3119) 
,col3120=ISNULL(@DCU09_IForcedClosDoor, col3120) ,col3121=ISNULL(@DCU09_ILeafsStopDoor, col3121) ,col3122=ISNULL(@DCU09_IObstacleDoor, col3122) ,col3123=ISNULL(@DCU09_IObstacleStep, col3123) ,col3124=ISNULL(@DCU09_IOpenAssistDoor, col3124) 
,col3125=ISNULL(@DCU09_IStandstillBack, col3125) ,col3126=ISNULL(@DCU10_CDoorCloseConduc, col3126) ,col3127=ISNULL(@DCU10_IDoorClosedSafe, col3127) ,col3128=ISNULL(@DCU10_IDoorClRelease, col3128) ,col3129=ISNULL(@DCU10_IDoorPbClose, col3129) 
,col3130=ISNULL(@DCU10_IDoorPbOpenIn, col3130) ,col3131=ISNULL(@DCU10_IDoorPbOpenOut, col3131) ,col3132=ISNULL(@DCU10_IDoorReleased, col3132) ,col3133=ISNULL(@DCU10_IFootStepRel, col3133) ,col3134=ISNULL(@DCU10_IForcedClosDoor, col3134) 
,col3135=ISNULL(@DCU10_ILeafsStopDoor, col3135) ,col3136=ISNULL(@DCU10_IObstacleDoor, col3136) ,col3137=ISNULL(@DCU10_IObstacleStep, col3137) ,col3138=ISNULL(@DCU10_IOpenAssistDoor, col3138) ,col3139=ISNULL(@DCU10_IStandstillBack, col3139) 
,col3140=ISNULL(@DCU11_CDoorCloseConduc, col3140) ,col3141=ISNULL(@DCU11_IDoorClosedSafe, col3141) ,col3142=ISNULL(@DCU11_IDoorClRelease, col3142) ,col3143=ISNULL(@DCU11_IDoorPbClose, col3143) ,col3144=ISNULL(@DCU11_IDoorPbOpenIn, col3144) 
,col3145=ISNULL(@DCU11_IDoorPbOpenOut, col3145) ,col3146=ISNULL(@DCU11_IDoorReleased, col3146) ,col3147=ISNULL(@DCU11_IFootStepRel, col3147) ,col3148=ISNULL(@DCU11_IForcedClosDoor, col3148) ,col3149=ISNULL(@DCU11_ILeafsStopDoor, col3149) 
,col3150=ISNULL(@DCU11_IObstacleDoor, col3150) ,col3151=ISNULL(@DCU11_IObstacleStep, col3151) ,col3152=ISNULL(@DCU11_IOpenAssistDoor, col3152) ,col3153=ISNULL(@DCU11_IStandstillBack, col3153) ,col3154=ISNULL(@DCU12_CDoorCloseConduc, col3154) 
,col3155=ISNULL(@DCU12_IDoorClosedSafe, col3155) ,col3156=ISNULL(@DCU12_IDoorClRelease, col3156) ,col3157=ISNULL(@DCU12_IDoorPbClose, col3157) ,col3158=ISNULL(@DCU12_IDoorPbOpenIn, col3158) ,col3159=ISNULL(@DCU12_IDoorPbOpenOut, col3159) 
,col3160=ISNULL(@DCU12_IDoorReleased, col3160) ,col3161=ISNULL(@DCU12_IFootStepRel, col3161) ,col3162=ISNULL(@DCU12_IForcedClosDoor, col3162) ,col3163=ISNULL(@DCU12_ILeafsStopDoor, col3163) ,col3164=ISNULL(@DCU12_IObstacleDoor, col3164) 
,col3165=ISNULL(@DCU12_IObstacleStep, col3165) ,col3166=ISNULL(@DCU12_IOpenAssistDoor, col3166) ,col3167=ISNULL(@DCU12_IStandstillBack, col3167) ,col3168=ISNULL(@DCU13_CDoorCloseConduc, col3168) ,col3169=ISNULL(@DCU13_IDoorClosedSafe, col3169) 
,col3170=ISNULL(@DCU13_IDoorClRelease, col3170) ,col3171=ISNULL(@DCU13_IDoorPbClose, col3171) ,col3172=ISNULL(@DCU13_IDoorPbOpenIn, col3172) ,col3173=ISNULL(@DCU13_IDoorPbOpenOut, col3173) ,col3174=ISNULL(@DCU13_IDoorReleased, col3174) 
,col3175=ISNULL(@DCU13_IFootStepRel, col3175) ,col3176=ISNULL(@DCU13_IForcedClosDoor, col3176) ,col3177=ISNULL(@DCU13_ILeafsStopDoor, col3177) ,col3178=ISNULL(@DCU13_IObstacleDoor, col3178) ,col3179=ISNULL(@DCU13_IObstacleStep, col3179) 
,col3180=ISNULL(@DCU13_IOpenAssistDoor, col3180) ,col3181=ISNULL(@DCU13_IStandstillBack, col3181) ,col3182=ISNULL(@DCU14_CDoorCloseConduc, col3182) ,col3183=ISNULL(@DCU14_IDoorClosedSafe, col3183) ,col3184=ISNULL(@DCU14_IDoorClRelease, col3184) 
,col3185=ISNULL(@DCU14_IDoorPbClose, col3185) ,col3186=ISNULL(@DCU14_IDoorPbOpenIn, col3186) ,col3187=ISNULL(@DCU14_IDoorPbOpenOut, col3187) ,col3188=ISNULL(@DCU14_IDoorReleased, col3188) ,col3189=ISNULL(@DCU14_IFootStepRel, col3189) 
,col3190=ISNULL(@DCU14_IForcedClosDoor, col3190) ,col3191=ISNULL(@DCU14_ILeafsStopDoor, col3191) ,col3192=ISNULL(@DCU14_IObstacleDoor, col3192) ,col3193=ISNULL(@DCU14_IObstacleStep, col3193) ,col3194=ISNULL(@DCU14_IOpenAssistDoor, col3194) 
,col3195=ISNULL(@DCU14_IStandstillBack, col3195) ,col3196=ISNULL(@DCU15_CDoorCloseConduc, col3196) ,col3197=ISNULL(@DCU15_IDoorClosedSafe, col3197) ,col3198=ISNULL(@DCU15_IDoorClRelease, col3198) ,col3199=ISNULL(@DCU15_IDoorPbClose, col3199) 
,col3200=ISNULL(@DCU15_IDoorPbOpenIn, col3200) ,col3201=ISNULL(@DCU15_IDoorPbOpenOut, col3201) ,col3202=ISNULL(@DCU15_IDoorReleased, col3202) ,col3203=ISNULL(@DCU15_IFootStepRel, col3203) ,col3204=ISNULL(@DCU15_IForcedClosDoor, col3204) 
,col3205=ISNULL(@DCU15_ILeafsStopDoor, col3205) ,col3206=ISNULL(@DCU15_IObstacleDoor, col3206) ,col3207=ISNULL(@DCU15_IObstacleStep, col3207) ,col3208=ISNULL(@DCU15_IOpenAssistDoor, col3208) ,col3209=ISNULL(@DCU15_IStandstillBack, col3209) 
,col3210=ISNULL(@DCU16_CDoorCloseConduc, col3210) ,col3211=ISNULL(@DCU16_IDoorClosedSafe, col3211) ,col3212=ISNULL(@DCU16_IDoorClRelease, col3212) ,col3213=ISNULL(@DCU16_IDoorPbClose, col3213) ,col3214=ISNULL(@DCU16_IDoorPbOpenIn, col3214) 
,col3215=ISNULL(@DCU16_IDoorPbOpenOut, col3215) ,col3216=ISNULL(@DCU16_IDoorReleased, col3216) ,col3217=ISNULL(@DCU16_IFootStepRel, col3217) ,col3218=ISNULL(@DCU16_IForcedClosDoor, col3218) ,col3219=ISNULL(@DCU16_ILeafsStopDoor, col3219) 
,col3220=ISNULL(@DCU16_IObstacleDoor, col3220) ,col3221=ISNULL(@DCU16_IObstacleStep, col3221) ,col3222=ISNULL(@DCU16_IOpenAssistDoor, col3222) ,col3223=ISNULL(@DCU16_IStandstillBack, col3223) ,col3224=ISNULL(@DCU17_CDoorCloseConduc, col3224) 
,col3225=ISNULL(@DCU17_IDoorClosedSafe, col3225) ,col3226=ISNULL(@DCU17_IDoorClRelease, col3226) ,col3227=ISNULL(@DCU17_IDoorPbClose, col3227) ,col3228=ISNULL(@DCU17_IDoorPbOpenIn, col3228) ,col3229=ISNULL(@DCU17_IDoorPbOpenOut, col3229) 
,col3230=ISNULL(@DCU17_IDoorReleased, col3230) ,col3231=ISNULL(@DCU17_IFootStepRel, col3231) ,col3232=ISNULL(@DCU17_IForcedClosDoor, col3232) ,col3233=ISNULL(@DCU17_ILeafsStopDoor, col3233) ,col3234=ISNULL(@DCU17_IObstacleDoor, col3234) 
,col3235=ISNULL(@DCU17_IObstacleStep, col3235) ,col3236=ISNULL(@DCU17_IOpenAssistDoor, col3236) ,col3237=ISNULL(@DCU17_IStandstillBack, col3237) ,col3238=ISNULL(@DCU18_CDoorCloseConduc, col3238) ,col3239=ISNULL(@DCU18_IDoorClosedSafe, col3239) 
,col3240=ISNULL(@DCU18_IDoorClRelease, col3240) ,col3241=ISNULL(@DCU18_IDoorPbClose, col3241) ,col3242=ISNULL(@DCU18_IDoorPbOpenIn, col3242) ,col3243=ISNULL(@DCU18_IDoorPbOpenOut, col3243) ,col3244=ISNULL(@DCU18_IDoorReleased, col3244) 
,col3245=ISNULL(@DCU18_IFootStepRel, col3245) ,col3246=ISNULL(@DCU18_IForcedClosDoor, col3246) ,col3247=ISNULL(@DCU18_ILeafsStopDoor, col3247) ,col3248=ISNULL(@DCU18_IObstacleDoor, col3248) ,col3249=ISNULL(@DCU18_IObstacleStep, col3249) 
,col3250=ISNULL(@DCU18_IOpenAssistDoor, col3250) ,col3251=ISNULL(@DCU18_IStandstillBack, col3251) ,col3252=ISNULL(@DCU19_CDoorCloseConduc, col3252) ,col3253=ISNULL(@DCU19_IDoorClosedSafe, col3253) ,col3254=ISNULL(@DCU19_IDoorClRelease, col3254) 
,col3255=ISNULL(@DCU19_IDoorPbClose, col3255) ,col3256=ISNULL(@DCU19_IDoorPbOpenIn, col3256) ,col3257=ISNULL(@DCU19_IDoorPbOpenOut, col3257) ,col3258=ISNULL(@DCU19_IDoorReleased, col3258) ,col3259=ISNULL(@DCU19_IFootStepRel, col3259) 
,col3260=ISNULL(@DCU19_IForcedClosDoor, col3260) ,col3261=ISNULL(@DCU19_ILeafsStopDoor, col3261) ,col3262=ISNULL(@DCU19_IObstacleDoor, col3262) ,col3263=ISNULL(@DCU19_IObstacleStep, col3263) ,col3264=ISNULL(@DCU19_IOpenAssistDoor, col3264) 
,col3265=ISNULL(@DCU19_IStandstillBack, col3265) ,col3266=ISNULL(@DCU20_CDoorCloseConduc, col3266) ,col3267=ISNULL(@DCU20_IDoorClosedSafe, col3267) ,col3268=ISNULL(@DCU20_IDoorClRelease, col3268) ,col3269=ISNULL(@DCU20_IDoorPbClose, col3269) 
,col3270=ISNULL(@DCU20_IDoorPbOpenIn, col3270) ,col3271=ISNULL(@DCU20_IDoorPbOpenOut, col3271) ,col3272=ISNULL(@DCU20_IDoorReleased, col3272) ,col3273=ISNULL(@DCU20_IFootStepRel, col3273) ,col3274=ISNULL(@DCU20_IForcedClosDoor, col3274) 
,col3275=ISNULL(@DCU20_ILeafsStopDoor, col3275) ,col3276=ISNULL(@DCU20_IObstacleDoor, col3276) ,col3277=ISNULL(@DCU20_IObstacleStep, col3277) ,col3278=ISNULL(@DCU20_IOpenAssistDoor, col3278) ,col3279=ISNULL(@DCU20_IStandstillBack, col3279) 
,col3280=ISNULL(@DRS_CDisDoorRelease_4_1, col3280) ,col3281=ISNULL(@DRS_CDisDoorRelease_4_10, col3281) ,col3282=ISNULL(@DRS_CDisDoorRelease_4_11, col3282) ,col3283=ISNULL(@DRS_CDisDoorRelease_4_12, col3283) ,col3284=ISNULL(@DRS_CDisDoorRelease_4_13, col3284) 
,col3285=ISNULL(@DRS_CDisDoorRelease_4_14, col3285) ,col3286=ISNULL(@DRS_CDisDoorRelease_4_15, col3286) ,col3287=ISNULL(@DRS_CDisDoorRelease_4_16, col3287) ,col3288=ISNULL(@DRS_CDisDoorRelease_4_17, col3288) ,col3289=ISNULL(@DRS_CDisDoorRelease_4_18, col3289) 
,col3290=ISNULL(@DRS_CDisDoorRelease_4_19, col3290) ,col3291=ISNULL(@DRS_CDisDoorRelease_4_2, col3291) ,col3292=ISNULL(@DRS_CDisDoorRelease_4_20, col3292) ,col3293=ISNULL(@DRS_CDisDoorRelease_4_3, col3293) ,col3294=ISNULL(@DRS_CDisDoorRelease_4_4, col3294) 
,col3295=ISNULL(@DRS_CDisDoorRelease_4_5, col3295) ,col3296=ISNULL(@DRS_CDisDoorRelease_4_6, col3296) ,col3297=ISNULL(@DRS_CDisDoorRelease_4_7, col3297) ,col3298=ISNULL(@DRS_CDisDoorRelease_4_8, col3298) ,col3299=ISNULL(@DRS_CDisDoorRelease_4_9, col3299) 
,col3300=ISNULL(@DRS_CDoorClosAutDis_4_1, col3300) ,col3301=ISNULL(@DRS_CDoorClosAutDis_4_10, col3301) ,col3302=ISNULL(@DRS_CDoorClosAutDis_4_11, col3302) ,col3303=ISNULL(@DRS_CDoorClosAutDis_4_12, col3303) ,col3304=ISNULL(@DRS_CDoorClosAutDis_4_13, col3304) 
,col3305=ISNULL(@DRS_CDoorClosAutDis_4_14, col3305) ,col3306=ISNULL(@DRS_CDoorClosAutDis_4_15, col3306) ,col3307=ISNULL(@DRS_CDoorClosAutDis_4_16, col3307) ,col3308=ISNULL(@DRS_CDoorClosAutDis_4_17, col3308) ,col3309=ISNULL(@DRS_CDoorClosAutDis_4_18, col3309) 
,col3310=ISNULL(@DRS_CDoorClosAutDis_4_19, col3310) ,col3311=ISNULL(@DRS_CDoorClosAutDis_4_2, col3311) ,col3312=ISNULL(@DRS_CDoorClosAutDis_4_20, col3312) ,col3313=ISNULL(@DRS_CDoorClosAutDis_4_3, col3313) ,col3314=ISNULL(@DRS_CDoorClosAutDis_4_4, col3314) 
,col3315=ISNULL(@DRS_CDoorClosAutDis_4_5, col3315) ,col3316=ISNULL(@DRS_CDoorClosAutDis_4_6, col3316) ,col3317=ISNULL(@DRS_CDoorClosAutDis_4_7, col3317) ,col3318=ISNULL(@DRS_CDoorClosAutDis_4_8, col3318) ,col3319=ISNULL(@DRS_CDoorClosAutDis_4_9, col3319) 
,col3320=ISNULL(@DRS_CForcedClosDoor_4_1, col3320) ,col3321=ISNULL(@DRS_CForcedClosDoor_4_10, col3321) ,col3322=ISNULL(@DRS_CForcedClosDoor_4_11, col3322) ,col3323=ISNULL(@DRS_CForcedClosDoor_4_12, col3323) ,col3324=ISNULL(@DRS_CForcedClosDoor_4_13, col3324) 
,col3325=ISNULL(@DRS_CForcedClosDoor_4_14, col3325) ,col3326=ISNULL(@DRS_CForcedClosDoor_4_15, col3326) ,col3327=ISNULL(@DRS_CForcedClosDoor_4_16, col3327) ,col3328=ISNULL(@DRS_CForcedClosDoor_4_17, col3328) ,col3329=ISNULL(@DRS_CForcedClosDoor_4_18, col3329) 
,col3330=ISNULL(@DRS_CForcedClosDoor_4_19, col3330) ,col3331=ISNULL(@DRS_CForcedClosDoor_4_2, col3331) ,col3332=ISNULL(@DRS_CForcedClosDoor_4_20, col3332) ,col3333=ISNULL(@DRS_CForcedClosDoor_4_3, col3333) ,col3334=ISNULL(@DRS_CForcedClosDoor_4_4, col3334) 
,col3335=ISNULL(@DRS_CForcedClosDoor_4_5, col3335) ,col3336=ISNULL(@DRS_CForcedClosDoor_4_6, col3336) ,col3337=ISNULL(@DRS_CForcedClosDoor_4_7, col3337) ,col3338=ISNULL(@DRS_CForcedClosDoor_4_8, col3338) ,col3339=ISNULL(@DRS_CForcedClosDoor_4_9, col3339) 
,col3340=ISNULL(@DRS_CTestModeReq_M_4_1, col3340) ,col3341=ISNULL(@DRS_CTestModeReq_M_4_10, col3341) ,col3342=ISNULL(@DRS_CTestModeReq_M_4_11, col3342) ,col3343=ISNULL(@DRS_CTestModeReq_M_4_12, col3343) ,col3344=ISNULL(@DRS_CTestModeReq_M_4_13, col3344) 
,col3345=ISNULL(@DRS_CTestModeReq_M_4_14, col3345) ,col3346=ISNULL(@DRS_CTestModeReq_M_4_15, col3346) ,col3347=ISNULL(@DRS_CTestModeReq_M_4_16, col3347) ,col3348=ISNULL(@DRS_CTestModeReq_M_4_17, col3348) ,col3349=ISNULL(@DRS_CTestModeReq_M_4_18, col3349) 
,col3350=ISNULL(@DRS_CTestModeReq_M_4_19, col3350) ,col3351=ISNULL(@DRS_CTestModeReq_M_4_2, col3351) ,col3352=ISNULL(@DRS_CTestModeReq_M_4_20, col3352) ,col3353=ISNULL(@DRS_CTestModeReq_M_4_3, col3353) ,col3354=ISNULL(@DRS_CTestModeReq_M_4_4, col3354) 
,col3355=ISNULL(@DRS_CTestModeReq_M_4_5, col3355) ,col3356=ISNULL(@DRS_CTestModeReq_M_4_6, col3356) ,col3357=ISNULL(@DRS_CTestModeReq_M_4_7, col3357) ,col3358=ISNULL(@DRS_CTestModeReq_M_4_8, col3358) ,col3359=ISNULL(@DRS_CTestModeReq_M_4_9, col3359) 
,col3360=ISNULL(@MIO_I1DoorRelLe_4_1, col3360) ,col3361=ISNULL(@MIO_I1DoorRelLe_4_10, col3361) ,col3362=ISNULL(@MIO_I1DoorRelLe_4_11, col3362) ,col3363=ISNULL(@MIO_I1DoorRelLe_4_12, col3363) ,col3364=ISNULL(@MIO_I1DoorRelLe_4_13, col3364) 
,col3365=ISNULL(@MIO_I1DoorRelLe_4_14, col3365) ,col3366=ISNULL(@MIO_I1DoorRelLe_4_15, col3366) ,col3367=ISNULL(@MIO_I1DoorRelLe_4_16, col3367) ,col3368=ISNULL(@MIO_I1DoorRelLe_4_17, col3368) ,col3369=ISNULL(@MIO_I1DoorRelLe_4_18, col3369) 
,col3370=ISNULL(@MIO_I1DoorRelLe_4_19, col3370) ,col3371=ISNULL(@MIO_I1DoorRelLe_4_2, col3371) ,col3372=ISNULL(@MIO_I1DoorRelLe_4_20, col3372) ,col3373=ISNULL(@MIO_I1DoorRelLe_4_3, col3373) ,col3374=ISNULL(@MIO_I1DoorRelLe_4_4, col3374) 
,col3375=ISNULL(@MIO_I1DoorRelLe_4_5, col3375) ,col3376=ISNULL(@MIO_I1DoorRelLe_4_6, col3376) ,col3377=ISNULL(@MIO_I1DoorRelLe_4_7, col3377) ,col3378=ISNULL(@MIO_I1DoorRelLe_4_8, col3378) ,col3379=ISNULL(@MIO_I1DoorRelLe_4_9, col3379) 
,col3380=ISNULL(@MIO_I1DoorRelRi_4_1, col3380) ,col3381=ISNULL(@MIO_I1DoorRelRi_4_10, col3381) ,col3382=ISNULL(@MIO_I1DoorRelRi_4_11, col3382) ,col3383=ISNULL(@MIO_I1DoorRelRi_4_12, col3383) ,col3384=ISNULL(@MIO_I1DoorRelRi_4_13, col3384) 
,col3385=ISNULL(@MIO_I1DoorRelRi_4_14, col3385) ,col3386=ISNULL(@MIO_I1DoorRelRi_4_15, col3386) ,col3387=ISNULL(@MIO_I1DoorRelRi_4_16, col3387) ,col3388=ISNULL(@MIO_I1DoorRelRi_4_17, col3388) ,col3389=ISNULL(@MIO_I1DoorRelRi_4_18, col3389) 
,col3390=ISNULL(@MIO_I1DoorRelRi_4_19, col3390) ,col3391=ISNULL(@MIO_I1DoorRelRi_4_2, col3391) ,col3392=ISNULL(@MIO_I1DoorRelRi_4_20, col3392) ,col3393=ISNULL(@MIO_I1DoorRelRi_4_3, col3393) ,col3394=ISNULL(@MIO_I1DoorRelRi_4_4, col3394) 
,col3395=ISNULL(@MIO_I1DoorRelRi_4_5, col3395) ,col3396=ISNULL(@MIO_I1DoorRelRi_4_6, col3396) ,col3397=ISNULL(@MIO_I1DoorRelRi_4_7, col3397) ,col3398=ISNULL(@MIO_I1DoorRelRi_4_8, col3398) ,col3399=ISNULL(@MIO_I1DoorRelRi_4_9, col3399) 
,col3400=ISNULL(@MIO_I2DoorRelLe_4_1, col3400) ,col3401=ISNULL(@MIO_I2DoorRelLe_4_10, col3401) ,col3402=ISNULL(@MIO_I2DoorRelLe_4_11, col3402) ,col3403=ISNULL(@MIO_I2DoorRelLe_4_12, col3403) ,col3404=ISNULL(@MIO_I2DoorRelLe_4_13, col3404) 
,col3405=ISNULL(@MIO_I2DoorRelLe_4_14, col3405) ,col3406=ISNULL(@MIO_I2DoorRelLe_4_15, col3406) ,col3407=ISNULL(@MIO_I2DoorRelLe_4_16, col3407) ,col3408=ISNULL(@MIO_I2DoorRelLe_4_17, col3408) ,col3409=ISNULL(@MIO_I2DoorRelLe_4_18, col3409) 
,col3410=ISNULL(@MIO_I2DoorRelLe_4_19, col3410) ,col3411=ISNULL(@MIO_I2DoorRelLe_4_2, col3411) ,col3412=ISNULL(@MIO_I2DoorRelLe_4_20, col3412) ,col3413=ISNULL(@MIO_I2DoorRelLe_4_3, col3413) ,col3414=ISNULL(@MIO_I2DoorRelLe_4_4, col3414) 
,col3415=ISNULL(@MIO_I2DoorRelLe_4_5, col3415) ,col3416=ISNULL(@MIO_I2DoorRelLe_4_6, col3416) ,col3417=ISNULL(@MIO_I2DoorRelLe_4_7, col3417) ,col3418=ISNULL(@MIO_I2DoorRelLe_4_8, col3418) ,col3419=ISNULL(@MIO_I2DoorRelLe_4_9, col3419) 
,col3420=ISNULL(@MIO_I2DoorRelRi_4_1, col3420) ,col3421=ISNULL(@MIO_I2DoorRelRi_4_10, col3421) ,col3422=ISNULL(@MIO_I2DoorRelRi_4_11, col3422) ,col3423=ISNULL(@MIO_I2DoorRelRi_4_12, col3423) ,col3424=ISNULL(@MIO_I2DoorRelRi_4_13, col3424) 
,col3425=ISNULL(@MIO_I2DoorRelRi_4_14, col3425) ,col3426=ISNULL(@MIO_I2DoorRelRi_4_15, col3426) ,col3427=ISNULL(@MIO_I2DoorRelRi_4_16, col3427) ,col3428=ISNULL(@MIO_I2DoorRelRi_4_17, col3428) ,col3429=ISNULL(@MIO_I2DoorRelRi_4_18, col3429) 
,col3430=ISNULL(@MIO_I2DoorRelRi_4_19, col3430) ,col3431=ISNULL(@MIO_I2DoorRelRi_4_2, col3431) ,col3432=ISNULL(@MIO_I2DoorRelRi_4_20, col3432) ,col3433=ISNULL(@MIO_I2DoorRelRi_4_3, col3433) ,col3434=ISNULL(@MIO_I2DoorRelRi_4_4, col3434) 
,col3435=ISNULL(@MIO_I2DoorRelRi_4_5, col3435) ,col3436=ISNULL(@MIO_I2DoorRelRi_4_6, col3436) ,col3437=ISNULL(@MIO_I2DoorRelRi_4_7, col3437) ,col3438=ISNULL(@MIO_I2DoorRelRi_4_8, col3438) ,col3439=ISNULL(@MIO_I2DoorRelRi_4_9, col3439) 
,col3440=ISNULL(@MIO_IDoorLoopBypa1_4_1, col3440) ,col3441=ISNULL(@MIO_IDoorLoopBypa1_4_10, col3441) ,col3442=ISNULL(@MIO_IDoorLoopBypa1_4_11, col3442) ,col3443=ISNULL(@MIO_IDoorLoopBypa1_4_12, col3443) ,col3444=ISNULL(@MIO_IDoorLoopBypa1_4_13, col3444) 
,col3445=ISNULL(@MIO_IDoorLoopBypa1_4_14, col3445) ,col3446=ISNULL(@MIO_IDoorLoopBypa1_4_15, col3446) ,col3447=ISNULL(@MIO_IDoorLoopBypa1_4_16, col3447) ,col3448=ISNULL(@MIO_IDoorLoopBypa1_4_17, col3448) ,col3449=ISNULL(@MIO_IDoorLoopBypa1_4_18, col3449) 
,col3450=ISNULL(@MIO_IDoorLoopBypa1_4_19, col3450) ,col3451=ISNULL(@MIO_IDoorLoopBypa1_4_2, col3451) ,col3452=ISNULL(@MIO_IDoorLoopBypa1_4_20, col3452) ,col3453=ISNULL(@MIO_IDoorLoopBypa1_4_3, col3453) ,col3454=ISNULL(@MIO_IDoorLoopBypa1_4_4, col3454) 
,col3455=ISNULL(@MIO_IDoorLoopBypa1_4_5, col3455) ,col3456=ISNULL(@MIO_IDoorLoopBypa1_4_6, col3456) ,col3457=ISNULL(@MIO_IDoorLoopBypa1_4_7, col3457) ,col3458=ISNULL(@MIO_IDoorLoopBypa1_4_8, col3458) ,col3459=ISNULL(@MIO_IDoorLoopBypa1_4_9, col3459) 
,col3460=ISNULL(@MIO_IDoorLoopBypa2_4_1, col3460) ,col3461=ISNULL(@MIO_IDoorLoopBypa2_4_10, col3461) ,col3462=ISNULL(@MIO_IDoorLoopBypa2_4_11, col3462) ,col3463=ISNULL(@MIO_IDoorLoopBypa2_4_12, col3463) ,col3464=ISNULL(@MIO_IDoorLoopBypa2_4_13, col3464) 
,col3465=ISNULL(@MIO_IDoorLoopBypa2_4_14, col3465) ,col3466=ISNULL(@MIO_IDoorLoopBypa2_4_15, col3466) ,col3467=ISNULL(@MIO_IDoorLoopBypa2_4_16, col3467) ,col3468=ISNULL(@MIO_IDoorLoopBypa2_4_17, col3468) ,col3469=ISNULL(@MIO_IDoorLoopBypa2_4_18, col3469) 
,col3470=ISNULL(@MIO_IDoorLoopBypa2_4_19, col3470) ,col3471=ISNULL(@MIO_IDoorLoopBypa2_4_2, col3471) ,col3472=ISNULL(@MIO_IDoorLoopBypa2_4_20, col3472) ,col3473=ISNULL(@MIO_IDoorLoopBypa2_4_3, col3473) ,col3474=ISNULL(@MIO_IDoorLoopBypa2_4_4, col3474) 
,col3475=ISNULL(@MIO_IDoorLoopBypa2_4_5, col3475) ,col3476=ISNULL(@MIO_IDoorLoopBypa2_4_6, col3476) ,col3477=ISNULL(@MIO_IDoorLoopBypa2_4_7, col3477) ,col3478=ISNULL(@MIO_IDoorLoopBypa2_4_8, col3478) ,col3479=ISNULL(@MIO_IDoorLoopBypa2_4_9, col3479) 
,col3480=ISNULL(@MIO_IDoorLoopCl1_4_1, col3480) ,col3481=ISNULL(@MIO_IDoorLoopCl1_4_10, col3481) ,col3482=ISNULL(@MIO_IDoorLoopCl1_4_11, col3482) ,col3483=ISNULL(@MIO_IDoorLoopCl1_4_12, col3483) ,col3484=ISNULL(@MIO_IDoorLoopCl1_4_13, col3484) 
,col3485=ISNULL(@MIO_IDoorLoopCl1_4_14, col3485) ,col3486=ISNULL(@MIO_IDoorLoopCl1_4_15, col3486) ,col3487=ISNULL(@MIO_IDoorLoopCl1_4_16, col3487) ,col3488=ISNULL(@MIO_IDoorLoopCl1_4_17, col3488) ,col3489=ISNULL(@MIO_IDoorLoopCl1_4_18, col3489) 
,col3490=ISNULL(@MIO_IDoorLoopCl1_4_19, col3490) ,col3491=ISNULL(@MIO_IDoorLoopCl1_4_2, col3491) ,col3492=ISNULL(@MIO_IDoorLoopCl1_4_20, col3492) ,col3493=ISNULL(@MIO_IDoorLoopCl1_4_3, col3493) ,col3494=ISNULL(@MIO_IDoorLoopCl1_4_4, col3494) 
,col3495=ISNULL(@MIO_IDoorLoopCl1_4_5, col3495) ,col3496=ISNULL(@MIO_IDoorLoopCl1_4_6, col3496) ,col3497=ISNULL(@MIO_IDoorLoopCl1_4_7, col3497) ,col3498=ISNULL(@MIO_IDoorLoopCl1_4_8, col3498) ,col3499=ISNULL(@MIO_IDoorLoopCl1_4_9, col3499) 
,col3500=ISNULL(@MIO_IDoorLoopCl2_4_1, col3500) ,col3501=ISNULL(@MIO_IDoorLoopCl2_4_10, col3501) ,col3502=ISNULL(@MIO_IDoorLoopCl2_4_11, col3502) ,col3503=ISNULL(@MIO_IDoorLoopCl2_4_12, col3503) ,col3504=ISNULL(@MIO_IDoorLoopCl2_4_13, col3504) 
,col3505=ISNULL(@MIO_IDoorLoopCl2_4_14, col3505) ,col3506=ISNULL(@MIO_IDoorLoopCl2_4_15, col3506) ,col3507=ISNULL(@MIO_IDoorLoopCl2_4_16, col3507) ,col3508=ISNULL(@MIO_IDoorLoopCl2_4_17, col3508) ,col3509=ISNULL(@MIO_IDoorLoopCl2_4_18, col3509) 
,col3510=ISNULL(@MIO_IDoorLoopCl2_4_19, col3510) ,col3511=ISNULL(@MIO_IDoorLoopCl2_4_2, col3511) ,col3512=ISNULL(@MIO_IDoorLoopCl2_4_20, col3512) ,col3513=ISNULL(@MIO_IDoorLoopCl2_4_3, col3513) ,col3514=ISNULL(@MIO_IDoorLoopCl2_4_4, col3514) 
,col3515=ISNULL(@MIO_IDoorLoopCl2_4_5, col3515) ,col3516=ISNULL(@MIO_IDoorLoopCl2_4_6, col3516) ,col3517=ISNULL(@MIO_IDoorLoopCl2_4_7, col3517) ,col3518=ISNULL(@MIO_IDoorLoopCl2_4_8, col3518) ,col3519=ISNULL(@MIO_IDoorLoopCl2_4_9, col3519) 

Where Id = @ID

END
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,151
           ,'update_spectrum_db_151.sql'
           ,'1.11.01')
GO