SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 152
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

-----------------------------------------------------------------------------------
-- Update 
-----------------------------------------------------------------------------------

RAISERROR ('-- Update NSP_InsertFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultCategory AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultCategory](
	@ID int
	, @Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
		
	SET IDENTITY_INSERT dbo.FaultCategory ON;
		
	INSERT dbo.FaultCategory(
		ID
		, Category
		, ReportingOnly
	)
	SELECT 
		@ID
		, @Category
		, @ReportingOnly;

	SET IDENTITY_INSERT dbo.FaultCategory OFF;


		
END
GO

RAISERROR ('-- Update NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultMeta AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET IDENTITY_INSERT FaultMeta ON

	INSERT dbo.FaultMeta(
		ID	
		, Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
	)
	SELECT 
		ID			= @ID
		, Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID



END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,152
           ,'update_spectrum_db_152.sql'
           ,'1.11.01')
GO