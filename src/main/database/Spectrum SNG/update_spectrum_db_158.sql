SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 158
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO
----------------------------------------------------------------------------
-- R2M-4945 - EventChannelValue data now shown in Event Details
----------------------------------------------------------------------------

RAISERROR ('-- update NSP_InsertFault with new columns', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @RuleName varchar(255)
    , @Context varchar(50)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
    DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
    DECLARE @faultCount int

    IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
            @faultMetaID = ID 
            , @faultMetaRecovery = RecoveryProcessPath
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
        , @faultLocationID = l.ID
        FROM dbo.ChannelValue
        LEFT JOIN dbo.Location l ON l.ID = (
            SELECT TOP 1 LocationID 
            FROM dbo.LocationArea 
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
         
         -- get current count of faults
        SELECT TOP 1 
            @faultCount = FaultCounter 
            FROM FaultCount fc 
            WHERE fc.UnitID = @faultUnitID 
            AND fc.FaultMetaID = @faultMetaID

        -- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
        IF @faultCount IS NULL
        BEGIN
            INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
            VALUES (@faultUnitID, @faultMetaID, 0)

            SET @faultCount = 0
        END
        
        SET @faultCount = @faultCount + 1   
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO dbo.[Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[RuleName]
            ,[IsDelayed]
            ,[CountSinceLastMaint]
            ,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,@RuleName
            ,ISNULL(@IsDelayed, 0)
            ,@faultCount
            ,RecoveryStatus = CASE
                WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
                ELSE NULL
            
            END

        -- increment the fault counter
        UPDATE dbo.[FaultCount]
        SET FaultCounter = @faultCount
        WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
    
    --insert Channel data
    INSERT INTO dbo.[FaultChannelValue]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20] 
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35]
        )
        SELECT
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20] 
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35]

    FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate

        DECLARE @StopDATE datetime2 = DATEADD(Day ,-1 ,@TimeCreate)

		INSERT INTO dbo.FaultEventChannelValue (ID ,FaultID,UnitID,ChannelID,Value,TimeStamp)
		SELECT ID, @faultID ,@FaultUnitID, ChannelID, Value,timestamp FROM dbo.EventChannelValue where 
		ID in (	SELECT MAX(ID) FROM dbo.EventChannelValue cv where UnitID = @FaultUnitID
						AND TimeStamp BETWEEN @StopDATE 
						AND @TimeCreate GROUP BY ChannelID)

    -- returning faultID
    RETURN @faultID

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,158
           ,'update_spectrum_db_158.sql'
           ,null)
GO