SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 160
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- R2M-8236 Event Analysis unit lookup will work with 2 characters
------------------------------------------------------------

RAISERROR ('-- Update NSP_EaSearchUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_EaSearchUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_EaSearchUnit]
(
	 @UnitString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		FleetCode = f.Code
		, UnitID = u.ID
		, UnitNumber = u.UnitNumber
	FROM dbo.Unit u
	JOIN dbo.Fleet f
	ON f.ID = u.FleetID
	WHERE UnitNumber LIKE '%' + @UnitString + '%'
		
END
GO

------------------------------------------------------------
-- INSERT into SchemaChangeLog
------------------------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,160
           ,'update_spectrum_db_160.sql'
           ,null)
GO