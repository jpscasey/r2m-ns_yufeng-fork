SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 004
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Populate ChannelStatus table', 0, 1) WITH NOWAIT
GO

INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (-1, N'INVALID', 4)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (0, N'NODATA', 0)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (1, N'NORMAL', 3)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (2, N'ON', 2)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (3, N'OFF', 1)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (4, N'WARNING', 5)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (5, N'WARNING1', 5)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (6, N'WARNING2', 5)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (7, N'FAULT', 6)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (8, N'FAULT1', 6)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (9, N'FAULT2', 6)
GO
INSERT [dbo].[ChannelStatus] ([ID], [Name], [Priority]) VALUES (10, N'EVENT', 3)
GO

--------------------------------------------
RAISERROR ('-- Populate FaultType table', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT [dbo].[FaultType] ON 
GO
INSERT [dbo].[FaultType] ([ID], [Name], [DisplayColor], [Priority]) VALUES (1, N'Fault', N'#FF0505', 1)
GO
INSERT [dbo].[FaultType] ([ID], [Name], [DisplayColor], [Priority]) VALUES (2, N'Warning', N'#FF6D05', 2)
GO
SET IDENTITY_INSERT [dbo].[FaultType] OFF
GO

--------------------------------------------
RAISERROR ('-- Add properties to Configuration table', 0, 1) WITH NOWAIT
GO

;MERGE [Configuration] r
USING (
    SELECT [PropertyName] = 'spectrum.refresh.validationrules.interval', [PropertyValue] = '10000'
    UNION ALL SELECT 'spectrum.refresh.lookupvalues.interval', '0'
    UNION ALL SELECT 'spectrum.refresh.channelconfigurations.interval', '10000'
    UNION ALL SELECT 'spectrum.dataplot.slotinseconds', '7200'

) AS nr
ON r.PropertyName = nr.PropertyName
WHEN MATCHED THEN 
UPDATE SET
    [PropertyValue]    = nr.[PropertyValue]
WHEN NOT MATCHED THEN
INSERT (
    PropertyName
    , [PropertyValue])
VALUES (
    nr.PropertyName
    , nr.[PropertyValue]);

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,004
           ,'update_spectrum_db_load_004.sql'
           ,'1.0.01')
GO