SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 017
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- Load Vehicles for fleet 1 - SLT', 0, 1) WITH NOWAIT
GO

DECLARE @UnitNumber varchar(16)

SELECT TOP 1 @UnitNumber = [UnitNumber] --SELECT *
FROM Unit
WHERE [UnitType] = 'SLT-VI'
ORDER BY [UnitNumber]
WHILE @@ROWCOUNT > 0
BEGIN

	INSERT [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
	SELECT source.VehicleNumber, source.Type, source.VehicleOrder, source.CabEnd, source.UnitID, source.Comments, source.ConfigDate, source.LastUpdate, source.IsValid, source.HardwareTypeID, source.OtmrSn, source.NoOpenRestriction, source.NoP1WorkOrders, source.VehicleTypeID
	FROM (
		SELECT @UnitNumber + 'mABk3' as VehicleNumber, 'mABk3' as Type,1 as VehicleOrder,1 CabEnd, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber) as UnitID, 'cab 1' Comments, getdate() ConfigDate, getdate() LastUpdate, 1 IsValid, (select ID from HardwareType where TypeName='SLT V30') HardwareTypeID, null OtmrSn, null as NoOpenRestriction, null NoP1WorkOrders, null as VehicleTypeID UNION
		SELECT @UnitNumber + 'mB2', 'mB2',2,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'AB', 'AB',3,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'B2', 'B2',4,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'mB3', 'mB3',5,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'mABk4', 'mABk4',6,1, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), 'cab 2', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null
	) source
	LEFT OUTER JOIN [dbo].[Vehicle] v on v.VehicleNumber = source.VehicleNumber
	WHERE v.VehicleNumber is null

	SELECT TOP 1 @UnitNumber = [UnitNumber]
	FROM Unit
	WHERE [UnitType] = 'SLT-VI'
	and [UnitNumber] > @UnitNumber
	ORDER BY [UnitNumber]
END


SELECT TOP 1 @UnitNumber = [UnitNumber] --SELECT *
FROM Unit
WHERE [UnitType] = 'SLT-IV'
ORDER BY [UnitNumber]
WHILE @@ROWCOUNT > 0
BEGIN

	INSERT [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
	SELECT source.VehicleNumber, source.Type, source.VehicleOrder, source.CabEnd, source.UnitID, source.Comments, source.ConfigDate, source.LastUpdate, source.IsValid, source.HardwareTypeID, source.OtmrSn, source.NoOpenRestriction, source.NoP1WorkOrders, source.VehicleTypeID
	FROM (
		SELECT @UnitNumber + 'mABK1' as VehicleNumber, 'mABK1' as Type,1 as VehicleOrder,1 CabEnd, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber) as UnitID, 'cab 1' Comments, getdate() ConfigDate, getdate() LastUpdate, 1 IsValid, (select ID from HardwareType where TypeName='SLT V30') HardwareTypeID, null OtmrSn, null as NoOpenRestriction, null NoP1WorkOrders, null as VehicleTypeID UNION
		SELECT @UnitNumber + 'mB1', 'mB1',2,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'B1', 'B1',3,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null UNION
		SELECT @UnitNumber + 'mABk2', 'mABk2',4,1, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), 'cab 2', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='SLT V30'), null, null, null, null
	) source
	LEFT OUTER JOIN [dbo].[Vehicle] v on v.VehicleNumber = source.VehicleNumber
	WHERE v.VehicleNumber is null

	SELECT TOP 1 @UnitNumber = [UnitNumber]
	FROM Unit
	WHERE [UnitType] = 'SLT-IV'
	and [UnitNumber] > @UnitNumber
	ORDER BY [UnitNumber]
END


GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,017
           ,'update_spectrum_db_load_017.sql'
           ,'1.1.02')
GO
