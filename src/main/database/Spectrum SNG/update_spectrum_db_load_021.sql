SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 021
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

-------------------------------
-- update
-------------------------------

RAISERROR ('-- Insert configurations for event analysis', 0, 1) WITH NOWAIT
GO

;MERGE [Configuration] r
USING (
    SELECT [PropertyName] = 'spectrum.eventanalysis.groupings', [PropertyValue] = '1,2,3,4,5'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.1.code', 'UNIT'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.1.label', 'Unit'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.2.code', 'FAULTCODE'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.2.label', 'Fault Code'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.3.code', 'LOCATION'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.3.params', 'latitude=52.1115825, longitude=5.6545435, zoom=8'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.3.label', 'Location'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.4.code', 'DATE'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.4.label', 'Date'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.5.code', 'CATEGORY'
    UNION ALL SELECT 'spectrum.eventanalysis.groupings.5.label', 'Category'

) AS nr
ON r.PropertyName = nr.PropertyName
WHEN MATCHED THEN 
UPDATE SET
    [PropertyValue]    = nr.[PropertyValue]
WHEN NOT MATCHED THEN
INSERT (
    PropertyName
    , [PropertyValue])
VALUES (
    nr.PropertyName
    , nr.[PropertyValue]);

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,021
           ,'update_spectrum_db_load_021.sql'
           ,'1.3.01')
GO