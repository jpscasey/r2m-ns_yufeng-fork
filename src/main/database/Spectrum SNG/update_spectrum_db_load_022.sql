SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 022
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- Loading ChannelCategory ', 0, 1) WITH NOWAIT
GO
INSERT ChannelCategory (ChannelCategoryName, Notes, IsVisible)--select * from ChannelCategory
SELECT categoryTobeUsedAsName, source.notes, 1 as isVisible 
FROM (
	SELECT 'A' as categoryTobeUsedAsName , 'A' as notes UNION
	SELECT 'B','B' UNION
	SELECT 'C','C' UNION
	SELECT 'D','D' UNION
	SELECT 'M','M' UNION
	SELECT 'NA','Non Applicable' UNION
	SELECT 'P','P'
) source
LEFT OUTER JOIN ChannelCategory cg ON ChannelCategoryName = categoryTobeUsedAsName
WHERE ChannelCategoryName is null

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,022
           ,'update_spectrum_db_load_022.sql'
           ,'1.1.02')
GO
