SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 026
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--DROP TABLE #FaultType
IF OBJECT_ID('tempdb..#FaultType') IS NOT NULL
    DROP TABLE #FaultType

CREATE TABLE #FaultType(
	[ID] INT,
	Name [varchar](50) NOT NULL,
	DisplayColor [varchar](10) NOT NULL,
	Priority tinyint  NOT NULL
)

PRINT 'Inserting rows into table: FaultType'

INSERT INTO #FaultType (ID, Name, DisplayColor, Priority) 
SELECT 1	ID,'Priority 1' Name	,	'#CC0000' DisplayColor,	1 Priority UNION
SELECT 2	,'Priority 2',	'#FF6D05',	2 UNION
SELECT 3	,'Priority 3',	'#CC99FF',	3 UNION
SELECT 4	,'Priority 4',	'#0095FF',	4 UNION
SELECT 5	,'Priority 5',	'#239023',	5

SET IDENTITY_INSERT dbo.FaultType ON

MERGE dbo.[FaultType] AS [target]
USING (SELECT * FROM [#FaultType] ff) AS [source]
ON ([target].ID = [source].ID)
WHEN MATCHED THEN
    UPDATE SET [target].Name = [source].Name
			 , [target].DisplayColor = [source].DisplayColor
			 , [target].Priority = [source].Priority
WHEN NOT MATCHED THEN 
    INSERT (ID, Name, DisplayColor, Priority)
    VALUES ([source].ID, [source].Name, [source].DisplayColor, [source].Priority);

SET IDENTITY_INSERT dbo.FaultType OFF

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,026
           ,'update_spectrum_db_load_026.sql'
           ,'1.1.03')
GO