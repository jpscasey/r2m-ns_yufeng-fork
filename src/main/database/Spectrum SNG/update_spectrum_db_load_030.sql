SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 030
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
--UPDATE
---------------------------------------
IF not EXISTS (SELECT 1 FROM Channel Where EngColumnNo = 664)
INSERT INTO Channel (EngColumnNo,ChannelGroupID,Name,HEader,Ref,DataType ,TypeID,UOM,HardwareType,IsAlwaysDisplayed,IsLookup,IsDisplayedAsDifference,ChannelCategoryId,VehicleIn4Car,VehicleIn6Car)
VALUES
(664,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt1UpDrvCount' ,'MPW_IPnt1UpDrvCount' , 'A280', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(665,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt2UpDrvCount' ,'MPW_IPnt2UpDrvCount' , 'A281', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(666,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt1UpCount' ,'MPW_IPnt1UpCount' , 'A282', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(667,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt2UpCount' ,'MPW_IPnt2UpCount' , 'A283', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(668,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt1KmCount' ,'MPW_IPnt1KmCount' , 'A284', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(669,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IPnt2KmCount' ,'MPW_IPnt2KmCount' , 'A285', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(670,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'DBC_IKmCounter' ,'DBC_IKmCounter' , 'A286', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(671,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'AUXY_IAuxAirCOnCnt' ,'AUXY_IAuxAirCOnCnt' , 'A287', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(672,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_IOnHscb' ,'MPW_IOnHscb' , 'A288', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(673,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'AUXY_IAirCSwOnCount' ,'AUXY_IAirCSwOnCount' , 'A289', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(674,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'AUXY_IAirCOnCount' ,'AUXY_IAirCOnCount' , 'A290', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(675,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'AUXY_IAuxAirCSwOnCnt' ,'AUXY_IAuxAirCSwOnCnt' , 'A291', 'int',2, 'none', 'SLTValues',0,0,0,6,1,1 )
,(676,(SELECT ID FROM ChannelGroup where ChannelGroupName = 'SLTValues'),'MPW_PRPantoEnable' ,'MPW_PRPantoEnable' , 'D3914', 'bit',1, 'none', 'SLTValues',0,0,0,6,1,1 )
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,030
           ,'update_spectrum_db_load_030.sql'
           ,'1.2.02')
GO