SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 031
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
-- Update script
---------------------------------------

RAISERROR ('-- Update Channel Groups names', 0, 1) WITH NOWAIT
GO

UPDATE ChannelGroup SET Notes = 'SLT ARR Events' WHERE ChannelGroupName = 'ARR'
UPDATE ChannelGroup SET Notes = 'SLT ATB Events' WHERE ChannelGroupName = 'ATB'
UPDATE ChannelGroup SET Notes = 'SLT BSS Events' WHERE ChannelGroupName = 'BSS'
UPDATE ChannelGroup SET Notes = 'SLT CAB Events' WHERE ChannelGroupName = 'CAB'
UPDATE ChannelGroup SET Notes = 'SLT COM Events' WHERE ChannelGroupName = 'COM'
UPDATE ChannelGroup SET Notes = 'SLT DIA Events' WHERE ChannelGroupName = 'DIA'
UPDATE ChannelGroup SET Notes = 'SLT DRN Events' WHERE ChannelGroupName = 'DRN'
UPDATE ChannelGroup SET Notes = 'SLT HSP Events' WHERE ChannelGroupName = 'HSP'
UPDATE ChannelGroup SET Notes = 'SLT KLM Events' WHERE ChannelGroupName = 'KLM'
UPDATE ChannelGroup SET Notes = 'SLT LSP Events' WHERE ChannelGroupName = 'LSP'
UPDATE ChannelGroup SET Notes = 'SLT LVZ Events' WHERE ChannelGroupName = 'LVZ'
UPDATE ChannelGroup SET Notes = 'SLT PIS Events' WHERE ChannelGroupName = 'PIS'
UPDATE ChannelGroup SET Notes = 'SLT REM Events' WHERE ChannelGroupName = 'REM'
UPDATE ChannelGroup SET Notes = 'SLT TRC Events' WHERE ChannelGroupName = 'TRC'
UPDATE ChannelGroup SET Notes = 'SLT VRL Events' WHERE ChannelGroupName = 'VRL'
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,031
           ,'update_spectrum_db_load_031.sql'
           ,'1.2.02')
GO