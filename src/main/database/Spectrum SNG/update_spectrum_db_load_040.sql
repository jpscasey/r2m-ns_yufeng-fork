SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 040
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO


---------------------------------------
-- UPDATE
---------------------------------------
IF NOT EXISTS (SELECT 1 FROM dbo.Configuration WHERE PropertyName = 'spectrum.dataplot.periods')
	INSERT INTO dbo.Configuration (PropertyName, PropertyValue) 
	VALUES ('spectrum.dataplot.periods', '1000,10000,30000,60000,120000,600000,1800000,3600000,7200000,10800000,14400000,18000000,21600000');
ELSE 
	UPDATE dbo.Configuration 
	SET PropertyValue = '1000,10000,30000,60000,120000,600000,1800000,3600000,7200000,10800000,14400000,18000000,21600000' 
	WHERE PropertyName = 'spectrum.dataplot.periods';
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,040
           ,'update_spectrum_db_load_040.sql'
           ,'1.8.02')
GO