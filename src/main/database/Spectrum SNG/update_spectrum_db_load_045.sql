SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 045
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- Update - NS-680
---------------------

UPDATE [dbo].[FaultMetaExtraField] SET Value = '0110' WHERE Value = '110';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0120' WHERE Value = '120';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0130' WHERE Value = '130';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0140' WHERE Value = '140';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0150' WHERE Value = '150';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0210' WHERE Value = '210';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0220' WHERE Value = '220';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0230' WHERE Value = '230';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0240' WHERE Value = '240';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0250' WHERE Value = '250';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0260' WHERE Value = '260';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0270' WHERE Value = '270';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0310' WHERE Value = '310';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0320' WHERE Value = '320';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0330' WHERE Value = '330';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0410' WHERE Value = '410';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0420' WHERE Value = '420';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0440' WHERE Value = '440';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0450' WHERE Value = '450';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0510' WHERE Value = '510';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0520' WHERE Value = '520';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0530' WHERE Value = '530';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0540' WHERE Value = '540';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0550' WHERE Value = '550';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0560' WHERE Value = '560';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0570' WHERE Value = '570';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0610' WHERE Value = '610';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0620' WHERE Value = '620';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0630' WHERE Value = '630';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0640' WHERE Value = '640';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0650' WHERE Value = '650';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0660' WHERE Value = '660';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0710' WHERE Value = '710';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0720' WHERE Value = '720';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0730' WHERE Value = '730';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0740' WHERE Value = '740';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0750' WHERE Value = '750';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0760' WHERE Value = '760';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0770' WHERE Value = '770';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0780' WHERE Value = '780';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0810' WHERE Value = '810';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0820' WHERE Value = '820';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0830' WHERE Value = '830';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0840' WHERE Value = '840';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0850' WHERE Value = '850';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0860' WHERE Value = '860';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0910' WHERE Value = '910';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0920' WHERE Value = '920';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0930' WHERE Value = '930';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0940' WHERE Value = '940';
UPDATE [dbo].[FaultMetaExtraField] SET Value = '0950' WHERE Value = '950';

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,045
           ,'update_spectrum_db_load_045.sql'
           ,'1.9.01')
GO