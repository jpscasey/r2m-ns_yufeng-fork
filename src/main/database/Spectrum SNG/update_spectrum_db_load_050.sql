SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 050
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- Update script
---------------------

RAISERROR ('-- Remove duplicate channels from [dbo].[FaultEventChannelValue]', 0, 1) WITH NOWAIT
GO

DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0315_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0415_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073D_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073E_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073F_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0740_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0741_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0742_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0743_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0744_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0745_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0746_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0747_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0748_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0749_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074A_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074B_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074C_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074D_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074E_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074F_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0750_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0751_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0752_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'DIA022D_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0116_2')
DELETE FROM [dbo].[FaultEventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0216_2')

RAISERROR ('-- Remove duplicate channels from [dbo].[EventChannelValue]', 0, 1) WITH NOWAIT
GO

DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0315_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0415_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073D_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073E_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073F_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0740_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0741_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0742_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0743_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0744_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0745_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0746_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0747_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0748_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0749_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074A_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074B_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074C_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074D_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074E_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074F_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0750_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0751_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0752_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'DIA022D_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0116_2')
DELETE FROM [dbo].[EventChannelValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0216_2')

GO
RAISERROR ('-- Remove duplicate channels from [dbo].[EventChannelLatestValue]', 0, 1) WITH NOWAIT
GO


DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0315_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0415_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073D_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073E_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM073F_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0740_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0741_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0742_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0743_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0744_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0745_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0746_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0747_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0748_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0749_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074A_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074B_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074C_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074D_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074E_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM074F_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0750_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0751_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'COM0752_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'DIA022D_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0116_2')
DELETE FROM [dbo].[EventChannelLatestValue] WHERE ChannelID = (SELECT ID FROM Channel WHERE Name = 'HSP0216_2')

GO
RAISERROR ('-- Remove duplicate channels from [dbo].[Channel]', 0, 1) WITH NOWAIT
GO

DELETE FROM [dbo].[Channel] WHERE Name = 'COM0315_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0415_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM073D_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM073E_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM073F_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0740_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0741_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0742_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0743_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0744_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0745_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0746_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0747_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0748_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0749_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074A_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074B_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074C_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074D_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074E_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM074F_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0750_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0751_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'COM0752_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'DIA022D_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'HSP0116_2'
DELETE FROM [dbo].[Channel] WHERE Name = 'HSP0216_2'

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,050
           ,'update_spectrum_db_load_050.sql'
           ,'1.10.02')
GO