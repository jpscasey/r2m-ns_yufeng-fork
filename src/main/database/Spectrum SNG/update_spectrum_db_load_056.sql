SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 056
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------------------------------
-- NS-805 Data fix for VIRM tables FaultCategory and FaultMeta
---------------------------------------------------------------

RAISERROR ('Clear fault tables, copy FaultMeta and FaultCategory from SLT', 0, 1) WITH NOWAIT
GO

DELETE FROM [dbo].[FaultStatusHistory]
DELETE FROM [dbo].[Fault2ExternalReferenceLink]
DELETE FROM [dbo].[FaultCount]
DELETE FROM [dbo].[FaultChannelValue]
DELETE FROM [dbo].[FaultEventChannelValue]
DELETE FROM [dbo].[FaultComment]
DELETE FROM [dbo].[Fault]
DELETE FROM [dbo].[FaultMeta]
DELETE FROM [dbo].[FaultMetaExtraField]
DELETE FROM [dbo].[FaultCategory]
DELETE FROM [dbo].[FaultGroup]

-- ignoring the following because it will fail when creating the SNG database if the SP does not exist in SLT
--RAISERROR ('Repopulate tables with NSP_PopulateOtherFaultCategoryMetaSNG', 0, 1) WITH NOWAIT
--GO
--EXEC [$(db_name_slt)].[dbo].[NSP_PopulateOtherFaultCategoryMetaSNG]
--GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,056
           ,'update_spectrum_db_load_056.sql'
           ,'1.11.01')
GO