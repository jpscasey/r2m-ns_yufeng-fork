:setvar scriptNumber 059

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

---------------------------------------
-- R2M-4679 Add SNG units to R2M
---------------------------------------

RAISERROR ('-- Load Units for SNG fleet', 0, 1) WITH NOWAIT
GO

-- load units - SNG III
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2301', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2302', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2303', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2304', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2305', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2306', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2307', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2308', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2309', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2310', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2311', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2312', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2313', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2314', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2315', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2316', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2317', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2318', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2319', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2320', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2321', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2322', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2323', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2324', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2325', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2326', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2327', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2328', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2329', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2330', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2331', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2332', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2333', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2334', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2335', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2336', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2337', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2338', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2339', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2340', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2341', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2342', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2343', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2344', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2345', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2346', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2347', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2348', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2349', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2350', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2351', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2352', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2353', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2354', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2355', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2356', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2357', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2358', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2359', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2360', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2361', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2362', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2363', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2364', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2365', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2366', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2367', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2368', 'SNG III', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))


-- load units SNG IV
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2701', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2702', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2703', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2704', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2705', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2706', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2707', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2708', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2709', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2710', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2711', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2712', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2713', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2714', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2715', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2716', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2717', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2718', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2719', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2720', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2721', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2722', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2723', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2724', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2725', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2726', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2727', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2728', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2729', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2730', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2731', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2732', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2733', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2734', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2735', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2736', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2737', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2738', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2739', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2740', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2741', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2742', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2743', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2744', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2745', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2746', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2747', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2748', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2749', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('2750', 'SNG IV', 1, (SELECT ID FROM Fleet WHERE Code = 'SNG'))

GO

RAISERROR ('-- Load Vehicles for SNG fleet', 0, 1) WITH NOWAIT
GO

DECLARE @UnitNumber varchar(16)

SELECT TOP 1 @UnitNumber = [UnitNumber] --SELECT *
FROM Unit
WHERE [UnitType] = 'SNG III'
ORDER BY [UnitNumber]
WHILE @@ROWCOUNT > 0
BEGIN

    INSERT [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
    SELECT source.VehicleNumber, source.Type, source.VehicleOrder, source.CabEnd, source.UnitID, source.Comments, source.ConfigDate, source.LastUpdate, source.IsValid, source.HardwareTypeID, 
        source.OtmrSn, source.NoOpenRestriction, source.NoP1WorkOrders, source.VehicleTypeID
    FROM (
        SELECT @UnitNumber + 'A1' as VehicleNumber, 'A1' as Type,1 as VehicleOrder,1 CabEnd, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber) as UnitID, 'cab 1' Comments, getdate() ConfigDate, getdate() LastUpdate, 
            1 IsValid, (select ID from HardwareType where TypeName='OTMR1') HardwareTypeID, null OtmrSn, null as NoOpenRestriction, null NoP1WorkOrders, null as VehicleTypeID UNION
        SELECT @UnitNumber + 'B', 'B',2,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='OTMR1'), null, null, null, null UNION
        SELECT @UnitNumber + 'A2', 'A2',3,1, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), 'cab 2', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='OTMR1'), null, null, null, null
    ) source
    LEFT OUTER JOIN [dbo].[Vehicle] v on v.VehicleNumber = source.VehicleNumber
    WHERE v.VehicleNumber is null

    SELECT TOP 1 @UnitNumber = [UnitNumber]
    FROM Unit
    WHERE [UnitType] = 'SNG III'
    and [UnitNumber] > @UnitNumber
    ORDER BY [UnitNumber]
END


SELECT TOP 1 @UnitNumber = [UnitNumber] --SELECT *
FROM Unit
WHERE [UnitType] = 'SNG IV'
ORDER BY [UnitNumber]
WHILE @@ROWCOUNT > 0
BEGIN

    INSERT [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
    SELECT source.VehicleNumber, source.Type, source.VehicleOrder, source.CabEnd, source.UnitID, source.Comments, source.ConfigDate, source.LastUpdate, source.IsValid, source.HardwareTypeID, 
        source.OtmrSn, source.NoOpenRestriction, source.NoP1WorkOrders, source.VehicleTypeID
    FROM (
        SELECT @UnitNumber + 'A1' as VehicleNumber, 'A1' as Type,1 as VehicleOrder,1 CabEnd, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber) as UnitID, 'cab 1' Comments, getdate() ConfigDate, getdate() LastUpdate, 
            1 IsValid, (select ID from HardwareType where TypeName='OTMR1') HardwareTypeID, null OtmrSn, null as NoOpenRestriction, null NoP1WorkOrders, null as VehicleTypeID UNION
        SELECT @UnitNumber + 'B', 'B',2,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='OTMR1'), null, null, null, null UNION
        SELECT @UnitNumber + 'C1', 'C1',3,0, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), '', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='OTMR1'), null, null, null, null UNION
        SELECT @UnitNumber + 'A2', 'A2',4,1, (SELECT ID FROM UNIT WHERE UnitNumber=@UnitNumber), 'cab 2', getdate(), getdate(), 1, (select ID from HardwareType where TypeName='OTMR1'), null, null, null, null
    ) source
    LEFT OUTER JOIN [dbo].[Vehicle] v on v.VehicleNumber = source.VehicleNumber
    WHERE v.VehicleNumber is null

    SELECT TOP 1 @UnitNumber = [UnitNumber]
    FROM Unit
    WHERE [UnitType] = 'SNG IV'
    and [UnitNumber] > @UnitNumber
    ORDER BY [UnitNumber]
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,$(scriptNumber)
           ,'update_spectrum_db_load_$(scriptNumber).sql'
           ,NULL)
GO