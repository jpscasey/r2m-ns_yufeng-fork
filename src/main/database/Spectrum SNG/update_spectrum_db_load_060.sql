:setvar scriptNumber 060

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

---------------------------------------
-- R2M-8118 Add new Database for SNG fleet
---------------------------------------

RAISERROR ('-- Populate FleetFormation and FleetStatus tables', 0, 1) WITH NOWAIT
GO

BEGIN TRAN

DELETE FROM dbo.FleetStatus
DELETE FROM [dbo].FleetStatusHistory 
DELETE FROM [dbo].[FleetFormation]

INSERT [dbo].[FleetFormation] (UnitIDList, UnitNumberList, ValidFrom, ValidTo)
SELECT ID, UnitNumber, getdate()-1, null
FROM dbo.Unit

INSERT dbo.FleetStatus (ValidFrom, ValidTo, Setcode, Diagram, Headcode, IsValid, Loading, ConfigDate, UnitList, LocationIDHeadcodeStart, LocationIDHeadcodeEnd, UnitID, UnitPosition, UnitOrientation, FleetFormationID)
SELECT ValidFrom, null as ValidTo, null as Setcode, null as Diagram, null as Headcode, null as IsValid, null as Loading, null as ConfigDate, null as UnitList, null as LocationIDHeadcodeStart, null as LocationIDHeadcodeEnd, UnitIDList as UnitID, 1 as UnitPosition, 1 as UnitOrientation, [FleetFormation].ID as FleetFormationID
FROM [dbo].[FleetFormation] 

COMMIT

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,$(scriptNumber)
           ,'update_spectrum_db_load_$(scriptNumber).sql'
           ,NULL)
GO