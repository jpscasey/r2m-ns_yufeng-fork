:setvar scriptNumber 061

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

---------------------------------------
-- R2M-8115 - SNG fleet - Rules engine
---------------------------------------

RAISERROR ('-- Initialize RuleEngineCurrentId table', 0, 1) WITH NOWAIT
GO

DELETE FROM RuleEngineCurrentId
GO

INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('SNG', 1, 0, NULL)
INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('SNG', 2, 0, NULL)
INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('SNGFault', 1, 0, NULL)
INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('SNGFault', 2, 0, NULL)
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,$(scriptNumber)
           ,'update_spectrum_db_load_$(scriptNumber).sql'
           ,NULL)
GO
