USE master
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'$(db_name)')
BEGIN
	RAISERROR('******** Database already exists ******** ', 20, 1) with log
END
GO

CREATE DATABASE $(db_name) 
COLLATE Latin1_General_CI_AS

WAITFOR DELAY '00:00:30'

GO
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'$(db_name)')
	RAISERROR('******** Database not created ******** ', 20, 1) with log
GO


ALTER AUTHORIZATION ON DATABASE::$(db_name) to sa;
GO

ALTER DATABASE $(db_name) SET ANSI_NULL_DEFAULT ON 
GO

ALTER DATABASE $(db_name) SET ANSI_NULLS ON
GO

ALTER DATABASE $(db_name) SET ANSI_PADDING ON 
GO

ALTER DATABASE $(db_name) SET ANSI_WARNINGS ON 
GO

ALTER DATABASE $(db_name) SET ARITHABORT ON
GO

ALTER DATABASE $(db_name) SET AUTO_CLOSE OFF 
GO

ALTER DATABASE $(db_name) SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE $(db_name) SET AUTO_SHRINK OFF 
GO

ALTER DATABASE $(db_name) SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE $(db_name) SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE $(db_name) SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE $(db_name) SET CONCAT_NULL_YIELDS_NULL ON
GO

ALTER DATABASE $(db_name) SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE $(db_name) SET QUOTED_IDENTIFIER ON 
GO

ALTER DATABASE $(db_name) SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE $(db_name) SET  READ_WRITE 
GO

ALTER DATABASE $(db_name) SET RECOVERY FULL 
GO

ALTER DATABASE $(db_name) SET  MULTI_USER 
GO


ALTER DATABASE $(db_name) MODIFY FILE ( NAME = N'$(db_name)', SIZE = 65536KB , FILEGROWTH = 32768KB )
GO

ALTER DATABASE $(db_name) MODIFY FILE ( NAME = N'$(db_name)_log', SIZE = 32768KB , FILEGROWTH = 32768KB )
GO



USE $(db_name)
GO

-------------------------------------------------------------------------------
RAISERROR ('-- Create SchemaChangeLog', 0, 1) WITH NOWAIT

CREATE TABLE [dbo].[SchemaChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScriptType] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
	[ScriptNumber] [int] NOT NULL,
	[ScriptName] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
	[DateApplied] [datetime2](0) NOT NULL,
	[ApplicationVersion] [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_SchemaChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (FILLFACTOR = 100, PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SchemaChangeLog] ADD  CONSTRAINT [DF_SchemaChangeLog_DateApplied]  DEFAULT (SYSDATETIME()) FOR [DateApplied]
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database create'
           ,0
           ,'create_spectrum_db.sql'
           ,'1.0.01')
GO