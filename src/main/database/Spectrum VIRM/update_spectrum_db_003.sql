:setvar scriptNumber 003

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
-- FUNCTIONS
------------------------------------------------------------


-------------------------------------------------------------------------------
RAISERROR ('-- Create ALL FUNCTIONS', 0, 1) WITH NOWAIT
/****** Object:  UserDefinedFunction [dbo].[FN_CalculateDistance]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/**************************************************************************
DESCRIPTION: Gets the distance in miles or kilometers between two points on the earth

PARAMETERS:
		@LatitudeA		- Latitude of first point
		@LongitudeA		- Longitude of first point
		@LatitudeB		- Latitude of second point
		@LongitudeB		- Longitude of second point
		@InKilometers	- 1 if return distance in kilometers
					  0 if return distance in miles

RETURNS:
		Float value of distance between two points in miles or kilometers	

AUTHOR:	Karen Gayda

DATE: 	04/12/2006

MODIFICATION HISTORY:
	WHO		DATE		DESCRIPTION
	---		----------	---------------------------------------------------

***************************************************************************/
CREATE FUNCTION [dbo].[FN_CalculateDistance]
	(@LatitudeA 	float = NULL, 
	 @LongitudeA 	float = NULL,
	 @LatitudeB 	float = NULL, 
	 @LongitudeB 	float = NULL,
	 @InKilometers	BIT = 0
	 )
RETURNS float
AS
BEGIN
	
	DECLARE @Distance FLOAT

	SET @Distance = (SIN(RADIANS(@LatitudeA)) *
              SIN(RADIANS(@LatitudeB)) +
              COS(RADIANS(@LatitudeA)) *
              COS(RADIANS(@LatitudeB)) *
              COS(RADIANS(@LongitudeA - @LongitudeB)))

	--Get distance in miles
	IF @Distance > 1 
		SET @Distance = 1
	
	IF @Distance < -1
		SET @Distance = -1
	
  	SET @Distance = (DEGREES(ACOS(@Distance))) * 69.09

	--If specified, convert to kilometers
	IF @InKilometers = 1
		SET @Distance = @Distance * 1.609344 

	RETURN @Distance

END


GO
/****** Object:  UserDefinedFunction [dbo].[FN_CamelCase]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_CamelCase](
	@Str varchar(8000)
)
RETURNS varchar(8000) AS
BEGIN
	DECLARE @Result varchar(2000)

	SET @Str = LOWER(@Str) + ' '

	SET @Result = ''

	WHILE 1=1
	BEGIN

		IF PATINDEX('%[ ,_,-,/,.]%',@Str) = 0 BREAK

		SET @Result = @Result + UPPER(Left(@Str, 1)) + SUBSTRING(@Str, 2, PATINDEX('%[ ,_,-,/,.]%', @Str) - 1)
		SET @Str = SUBSTRING(@Str, PATINDEX('%[ ,_,-,/,.]%', @Str) + 1, LEN(@Str))

	END

	SET @Result = Left(@Result,Len(@Result))

	RETURN @Result

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_CheckZero]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************************************************************************
**	Name: FN_CheckZero
**	Description: Checks a charcter value if it evaluates to 0 or not
**				 returns 0, when character value evaluates to 0 ('0', '00000', '000'...)
**				 returns -1, if any other character or digit is found
**				 returns -2, if @val is null
*************************************************************************************************************************
**	CVS
*************************************************************************************************************************
**	$$Author: heino $$
**	$$Date: 2012/03/16 13:31:00 $$
**	$$Revision: 1.1 $$
**	$$Source: /home/cvs/spectrum/lser/src/main/database/GeniusInterface/dbobjects/fn_CheckZero.sql,v $$
*************************************************************************************************************************
**	Change History
*************************************************************************************************************************
**	Date:				Author:		Description:
**	2012-01-26		    HeZ			creation on database
************************************************************************************************************************/
CREATE FUNCTION [dbo].[FN_CheckZero]
	(@val varchar(50))  
	RETURNS int 
as
begin
	declare @numVal int, @charCount int
	
	if (@val is null)
		return -2;
	
	set @charCount = 1

	while @charCount <= len(@val)
	begin
		if (substring(@val, @charCount, 1) <> '0')
			return -1;
		set @charCount = @charCount + 1;
	end

	return 0;
end -- function

GO
/****** Object:  UserDefinedFunction [dbo].[FN_CompareJorney]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			FN_CompareJorney
**	Description:	Function checks if 2 timetables are identical.(contain the 
**					same Section Points, the same arrival, departure, passing,
**					recovery times for section points) 
**	Call frequency:	Run from NSP_CifImportProcessFile on CIF Timetable import
**	Parameters:		@JourneyID1 int
**					@JourneyID2 int
**	Return values:	1 - timetables are identical
**					0 - timetables are different 
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:		Description:
**	2012-04-11	    RT			creation on database
*******************************************************************************/	

CREATE FUNCTION [dbo].[FN_CompareJorney]
(
	@JourneyID1 int
	, @JourneyID2 int
)
RETURNS bit
AS
BEGIN

	DECLARE @isIdentical bit
	
	IF NOT EXISTS 
	(
		SELECT 1 
		FROM
		(
			SELECT 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
			FROM [SectionPoint]
			WHERE [JourneyID] = @JourneyID1
			
			UNION ALL
			
			SELECT 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
			FROM [SectionPoint]
			WHERE [JourneyID] = @JourneyID2
		) tmp
		GROUP BY 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
		HAVING COUNT(*) = 1
	)
	
		SET @isIdentical = 1
	ELSE
		SET @isIdentical = 0
	
	RETURN @isIdentical
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertBstToGmt]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_ConvertBstToGmt] 
(
	@BstDatetime datetime
)
RETURNS datetime
AS
BEGIN

	DECLARE @timeOffset smallint

	SET @timeOffset = (
		SELECT TOP 1 -TimeOffset 
		FROM GmtToBstConversion 
		WHERE @BstDatetime BETWEEN 
			DATEADD(hh, TimeOffset, GmtDatetimeFrom) AND DATEADD(hh, TimeOffset, GmtDatetimeTo)
		)
		
	RETURN DATEADD(hh, @timeOffset, @BstDatetime)

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertCifEngAllowance]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			FN_ConvertCifEngAllowance
**	Description:	Converts string MH to number of seconds. This is format 
**					used in CIF file. Input string may have different 3 formats
**					1. MM - 1 or 2 digit number of minutes ie. 5, 15, 45
**					2. MH - 1 digit number of minutes and H character 
**					ie. 1H (90secondes), 2H (150seconds), etc.
**					3. H - single H character indicating 30s
**	Call frequency:	Run from NSP_CifImportProcessFile on CIF Timetable import
**	Parameters:		Time string in MH format
**	Return values:	Number of second as int
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:		Description:
**	2012-04-11	    RT			creation on database
*******************************************************************************/	

CREATE FUNCTION [dbo].[FN_ConvertCifEngAllowance]
(
	@CifEngAllowance char(2)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @engAllowanceSec int
	
	SET @CifEngAllowance = RIGHT(' ' + RTRIM(@CifEngAllowance),2)

	SET @engAllowanceSec = CASE RIGHT(@CifEngAllowance, 1)
		WHEN 'H' THEN CAST(SUBSTRING(@CifEngAllowance, 1, 1) AS int) * 60 + 30
		ELSE CAST(@CifEngAllowance AS int) * 60
	END

	RETURN @engAllowanceSec
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertCifTime]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			FN_ConvertCifTime
**	Description:	Converts HHMMX to time value. This is the time format used
**					in CIF file:
**					HH - hours
**					MM - minutes
**					X - optional character. H means half of the minute, 
**					example:
**					0900H -> 09:00:30
**					1255  -> 12:55:00
**					1255H -> 12:55:30
**	Call frequency:	Run from NSP_CifImportProcessFile on CIF Timetable import
**	Parameters:		Time string in HHMMX format
**	Return values:	time value
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:		Description:
**	2012-04-11	    RT			creation on database
*******************************************************************************/	

CREATE FUNCTION [dbo].[FN_ConvertCifTime]
(
	@CifTime char(5)
)
RETURNS time(0)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @time time(0)
	
	IF LEN(@CifTime) < 4
		RETURN NULL

	SET @time = CASE RIGHT(@CifTime, 1)
		WHEN 'H' THEN CAST(SUBSTRING(@CifTime, 1, 2) AS char(2)) + ':' + CAST(SUBSTRING(@CifTime, 3, 2) AS char(2)) + ':30'
		ELSE CAST(SUBSTRING(@CifTime, 1, 2) AS char(2)) + ':' + CAST(SUBSTRING(@CifTime, 3, 2) AS char(2)) + ':00'
	END

	RETURN @time
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertGmtToBst]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_ConvertGmtToBst] 
(
	@GmtDatetime datetime
)
RETURNS datetime
AS
BEGIN

	DECLARE @timeOffset smallint

	SET @timeOffset = (
		SELECT TOP 1 TimeOffset 
		FROM GmtToBstConversion 
		WHERE @GmtDatetime BETWEEN GmtDatetimeFrom AND GmtDatetimeTo
		)
		
	RETURN DATEADD(hh, @timeOffset, @GmtDatetime)

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertSecondToTime]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_ConvertSecondToTime]
(
	@NumberOfSeconds int
)
RETURNS varchar(7)
AS
BEGIN
	DECLARE @string char(7)

	IF @NumberOfSeconds BETWEEN 0 AND 24*60*60
		SET @string =
			CASE 
				WHEN @NumberOfSeconds < 600
					THEN '0' + CAST((@NumberOfSeconds - (@NumberOfSeconds % 60)) / 60 AS varchar(3))
				ELSE CAST((@NumberOfSeconds - (@NumberOfSeconds % 60)) / 60 AS varchar(3))
			END
			+ ':'
			+ RIGHT(CAST(100 + @NumberOfSeconds % 60 AS CHAR(3)), 2)
	ELSE
		SET @string = '-'
	-- Return the result of the function
	RETURN @string

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ConvertSecondToTimeLong]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_ConvertSecondToTimeLong]
(
	@NumberOfSeconds int
)
RETURNS varchar(8)
AS
BEGIN
	DECLARE @string char(8)

	SET @string = CONVERT(char(8), DATEADD(s, @NumberOfSeconds, 0), 108)
	
	IF @NumberOfSeconds IS NULL OR @string IS NULL
		SET @string = '-'
		
	-- Return the result of the function
	RETURN @string

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_DataForGPSVisualizer]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[FN_DataForGPSVisualizer]
(
	@LocationName varchar(50)
	, @MinLat decimal(8,6)
	, @MaxLat decimal(8,6)
	, @MinLng decimal(8,6)
	, @MaxLng decimal(8,6)
)
RETURNS varchar(1000)
AS
BEGIN
	DECLARE @string varchar(1000)
	
	SET @string = 'name,latitude,longitude
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MaxLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MaxLat AS varchar(9)) + ', ' + CAST(@MaxLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MaxLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9))

	RETURN @string
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_FleetFormationByUnitID]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************************************************************************
** Author:		Paul Mone
** Create date: 2014-11-25
** Description:	Returns fleet formation ID when given a unit ID
** Input:
**	@UnitID - UnitID from FleetStatus table
************************************************************************************************************************/
CREATE FUNCTION  [dbo].[FN_FleetFormationByUnitID]
(
	@UnitID INT
	, @Datetime DATETIME2(2)
)
RETURNS INT
AS
BEGIN

	DECLARE @FleetFormationID INT
	
	SET @FleetFormationID = (
				SELECT TOP (1) ff.ID
				FROM dbo.FleetFormation ff
				CROSS APPLY dbo.SplitString(ff.UnitIDList,',') ss
				WHERE ss.Item = @UnitID
					AND ff.ValidFrom >= @Datetime
					AND ff.ValidTo < @Datetime
				ORDER BY ff.ValidTo DESC)

	RETURN @FleetFormationID 

END -- function

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GeniusGetTimestampFromFilename]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FN_GeniusGetTimestampFromFilename]
(
	@Filename varchar(100)
)
RETURNS datetime2(0)
AS
BEGIN

	--FileFormat:
	-- DateProcessed_TimeProcessed_Unit_DateGenerated_TimeGenereated
	
	-- Find datetime file was generated
	DECLARE @separator char(1) = '_';
	
	DECLARE @p1 tinyint = CHARINDEX(@separator, @Filename, 0);
	DECLARE @p2 tinyint = CHARINDEX(@separator, @Filename, @p1 + 1);
	DECLARE @p3 tinyint = CHARINDEX(@separator, @Filename, @p2 + 1);
	
	DECLARE @dateStringDay varchar(10)	= SUBSTRING(@Filename, @p3 + 1, 2);
	DECLARE @dateStringMonth varchar(10)	= SUBSTRING(@Filename, @p3 + 3, 2);
	DECLARE @dateStringYear varchar(10)	= SUBSTRING(@Filename, @p3 + 5, 4);
	DECLARE @timeStringHour varchar(10)	= SUBSTRING(@Filename, @p3 + 10, 2);
	DECLARE @timeStringMinute varchar(10)	= SUBSTRING(@Filename, @p3 + 12, 2);
	
	DECLARE @date datetime2(0) = CONVERT(datetime2(0), @dateStringYear + '-' + @dateStringMonth + '-' + @dateStringDay + ' ' + @timeStringHour + ':' + @timeStringMinute)


	RETURN @date;
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GeniusGetUnitFromFilename]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FN_GeniusGetUnitFromFilename]
(
	@Filename varchar(100)
)
RETURNS varchar(100)
AS
BEGIN

	--FileFormat:
	-- DateProcessed_TimeProcessed_Unit_DateGenerated_TimeGenereated
	
	-- Find datetime file was generated
	DECLARE @separator char(1) = '_';
	
	DECLARE @p1 tinyint = CHARINDEX(@separator, @Filename, 0);
	DECLARE @p2 tinyint = CHARINDEX(@separator, @Filename, @p1 + 1);
	DECLARE @p3 tinyint = CHARINDEX(@separator, @Filename, @p2 + 1);

	DECLARE @unit varchar(100) = SUBSTRING(@Filename, @p2 + 1, @p3 - @p2 - 1)

	RETURN @unit;
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetBstToGmtTimeDiff]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_GetBstToGmtTimeDiff] 
(
	@BstDatetime datetime
)
RETURNS smallint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @timeOffset smallint
	

	SET @timeOffset = (
		SELECT TOP 1 -TimeOffset 
		FROM GmtToBstConversion 
		WHERE @BstDatetime BETWEEN 
			DATEADD(hh, TimeOffset, GmtDatetimeFrom) AND DATEADD(hh, TimeOffset, GmtDatetimeTo)
		)

	RETURN @timeOffset

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetDaysRunDescription]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			FN_GetDaysRunDescription
**	Description:	Convert weekdays from format 0000000 (position 1 - Monday,
**					2 - Tuesday etc) to more descriptive one ie.
**					1111100 to Mon-Fri
**					0000001 to Sun
**	Call frequency:	Run from NSP_CifImportProcessFile on CIF Timetable import
**	Parameters:		String in 0000000 format
**	Return values:	Weekdays decription
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:		Description:
**	2012-04-11	    RT			creation on database
*******************************************************************************/	

CREATE FUNCTION [dbo].[FN_GetDaysRunDescription] 
(
	@DaysRun char(7)
)
RETURNS varchar(50)
AS
BEGIN
	DECLARE @desc varchar(50)
	
	SET @desc = CASE @DaysRun
		WHEN '1000000' THEN 'Mon'
		WHEN '0100000' THEN 'Tue'
		WHEN '0010000' THEN 'Wed'
		WHEN '0001000' THEN 'Thr'
		WHEN '0000100' THEN 'Fri'
		WHEN '0000010' THEN 'Sat'
		WHEN '0000001' THEN 'Sun'
		WHEN '1111111' THEN 'Mon-Sun'
		WHEN '1111110' THEN 'Mon-Sat'
		WHEN '1111100' THEN 'Mon-Fri'
		WHEN '1111000' THEN 'Mon-Thr'
		WHEN '1110000' THEN 'Mon-Wed'
		WHEN '1100000' THEN 'Mon-Tue'
		WHEN '0111111' THEN 'Tue-Sun'
		WHEN '0111110' THEN 'Tue-Sat'
		WHEN '0111100' THEN 'Tue-Fri'
		WHEN '0111000' THEN 'The-Thr'
		
		WHEN '0110000' THEN 'Tue-Wed'
		WHEN '1001000' THEN 'Mon, Thr'
		WHEN '0011000' THEN 'Wed-Thr'
		WHEN '0001100' THEN 'Thr-Fri'
		WHEN '1011000' THEN 'Mon, Wed, Thr'
		ELSE @DaysRun

	END

	RETURN @desc

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetDiagramByHeadcodeAndDate]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_GetDiagramByHeadcodeAndDate]
(
	@Headcode char(4)
	, @StartDate datetime
	, @EndDate datetime
)
RETURNS varchar(500)
AS
BEGIN
	DECLARE @result varchar(500) = '';
	
	

	WITH CteJourneyList AS
	(
		SELECT 
			JourneyID
			, COUNT(*) AS RecordNumber
		FROM TrainPassage tp
		WHERE Headcode = @Headcode
			AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		GROUP BY 
			JourneyID
	)
	SELECT
		@result = @result + 
			ISNULL(
				RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
				+ ' ' + sl.Tiploc
				+ '-' + el.Tiploc
				+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
			, '') + ' ('+ CAST(SUM(RecordNumber) AS varchar(5)) + ') ' + HeadcodeDesc + '
'
	FROM CteJourneyList
	INNER JOIN Journey ON CteJourneyList.JourneyID = Journey.ID
	LEFT JOIN SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN Location el ON el.ID = esp.LocationID
	GROUP BY
		StartTime
		, sl.Tiploc
		, el.Tiploc
		, EndTime
		, HeadcodeDesc

	SET @result = LEFT(@result, LEN(@result) - 2 )

	-- Return the result of the function
	RETURN @result

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetGmtToBstTimeDiff]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_GetGmtToBstTimeDiff] 
(
	@GmtDatetime datetime
)
RETURNS smallint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @timeOffset smallint
	

	SET @timeOffset = (SELECT TOP 1 TimeOffset FROM GmtToBstConversion WHERE @GmtDatetime BETWEEN GmtDatetimeFrom AND GmtDatetimeTo)

	RETURN @timeOffset

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetServiceDate]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			FN_GetServiceDate
**	Description:	Function returns list of dates between From and To dates, 
**					for provided weekday pattern
**	Call frequency:	Run from NSP_CifImportProcessFile on CIF Timetable import
**	Parameters:		@DateRunsFrom date
**					@DateRunsTo date
**					@DaysRun char(7) - pattern - 7 character binary field 
**						i.e '1111100', '0000001'
**						Position 1 = Monday
**						Position 2 = Tuesday
**						Position 3 = Wednesday, etc
**	Return values:	Set of dates
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:		Description:
**	2012-04-11	    RT			creation on database
*******************************************************************************/	

CREATE FUNCTION [dbo].[FN_GetServiceDate] 
(	
	@DateRunsFrom date
	, @DateRunsTo date
	, @DaysRun char(7)
)
RETURNS 
@result TABLE 
(
	DateValue date
)
AS
BEGIN

	;WITH CteDate AS
	(
		SELECT @DateRunsFrom DateValue
		UNION ALL
		SELECT DATEADD(d, 1, DateValue)
		FROM CteDate   
		WHERE DATEADD(d, 1, DateValue) <= @DateRunsTo
	)
	INSERT INTO @result (
		DateValue)
	SELECT DateValue
	FROM CteDate
	WHERE 
		-- Function uses DATENAME (not DATEPART) to be independenf from server setting DATEFIRST
		(DATENAME(WEEKDAY, DateValue) = 'Monday'		AND SUBSTRING(@DaysRun, 1,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Tuesday'	AND SUBSTRING(@DaysRun, 2,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Wednesday'	AND SUBSTRING(@DaysRun, 3,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Thursday'	AND SUBSTRING(@DaysRun, 4,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Friday'	AND SUBSTRING(@DaysRun, 5,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Saturday'	AND SUBSTRING(@DaysRun, 6,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Sunday'	AND SUBSTRING(@DaysRun, 7,1) = '1')
	OPTION (MAXRECURSION 366)
	
	RETURN 
END


GO
/****** Object:  UserDefinedFunction [dbo].[FN_GetVehicleList]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Radek Toma
-- Create date: 2011-08-29
-- Description:	Returns comma separated list of vehicles for set
-- Input:
--	@FleetStatusID - ID from FleetStatus table
-- =============================================

CREATE FUNCTION  [dbo].[FN_GetVehicleList]
(
	@FleetStatusID varchar(100)
)
RETURNS varchar(100)
AS
BEGIN

	DECLARE @vehicleList varchar(150)

	SET @vehicleList = ''

		SELECT @vehicleList = @vehicleList + ', ' + VehicleNumber 
		FROM dbo.FleetStatusVehicle
		INNER JOIN Vehicle ON Vehicle.ID = VehicleID
		WHERE FleetStatusID = @FleetStatusID
		ORDER BY VehiclePosition

	IF LEN(@vehicleList) > 2
		SET @vehicleList = RIGHT(@vehicleList, LEN(@vehicleList)-2) 

	RETURN @vehicleList 

END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_RemoveSpecialChar]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			FN_RemoveSpecialChar
**	Description:	Function removes all Special charcters from string. 
**					It leaves only a-z, A-Z, 0-9
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE FUNCTION [dbo].[FN_RemoveSpecialChar] (@s varchar(1000)) 
RETURNS varchar(1000) 
BEGIN 

	IF @s IS NULL 
		RETURN NULL 

	DECLARE @s2 varchar(1000) = '' 
	DECLARE @l int  = LEN(@s)
	DECLARE @p int = 1 

	WHILE @p <= @l 
	BEGIN 
		DECLARE @c int = Ascii(Substring(@s, @p, 1)) 

		IF @c BETWEEN 48 AND 57 
			OR @c BETWEEN 65 AND 90 
			OR @c BETWEEN 97 AND 122 
			SET @s2 = @s2 + Char(@c) 

		SET @p = @p + 1 
	END 

	IF LEN(@s2) = 0 
		RETURN NULL 

	RETURN @s2 
END

GO
/****** Object:  UserDefinedFunction [dbo].[FN_ValidateTimeDiff]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_ValidateTimeDiff]
(
	@DiffInSeconds int
)
RETURNS int
AS
BEGIN
	DECLARE @validatedInt int
	
	
	SET @validatedInt =	CASE
			WHEN @DiffInSeconds < - 43200
				THEN @DiffInSeconds + 86400 
			WHEN @DiffInSeconds > 43200
				THEN @DiffInSeconds - 86400 
			ELSE @DiffInSeconds
		END
		
	RETURN @validatedInt

END


GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 15/07/2016 09:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SplitString] (
      @InputString                  VARCHAR(8000),
      @Delimiter                    VARCHAR(50)
)

RETURNS @Items TABLE (
      Item                          VARCHAR(8000)
)

AS
BEGIN
      IF @Delimiter = ' '
      BEGIN
            SET @Delimiter = ','
            SET @InputString = REPLACE(@InputString, ' ', @Delimiter)
      END

      IF (@Delimiter IS NULL OR @Delimiter = '')
            SET @Delimiter = ','

--INSERT INTO @Items VALUES (@Delimiter) -- Diagnostic
--INSERT INTO @Items VALUES (@InputString) -- Diagnostic

      DECLARE @Item                 VARCHAR(8000)
      DECLARE @ItemList       VARCHAR(8000)
      DECLARE @DelimIndex     INT

      SET @ItemList = @InputString
      SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
      WHILE (@DelimIndex != 0)
      BEGIN
            SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex)
            INSERT INTO @Items VALUES (@Item)

            -- Set @ItemList = @ItemList minus one less item
            SET @ItemList = SUBSTRING(@ItemList, @DelimIndex+1, LEN(@ItemList)-@DelimIndex)
            SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
      END -- End WHILE

      IF @Item IS NOT NULL -- At least one delimiter was encountered in @InputString
      BEGIN
            SET @Item = @ItemList
            INSERT INTO @Items VALUES (@Item)
      END

      -- No delimiters were encountered in @InputString, so just return @InputString
      ELSE INSERT INTO @Items VALUES (@InputString)

      RETURN

END -- End Function

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,$(scriptNumber)
           ,'update_spectrum_db_$(scriptNumber).sql'
           ,'1.0.01')
GO