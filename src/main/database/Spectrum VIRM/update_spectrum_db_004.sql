SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 004
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
-- VIEWS
------------------------------------------------------------


-------------------------------------------------------------------------------
RAISERROR ('-- Create ALL VIEWS', 0, 1) WITH NOWAIT

/****** Object:  View [dbo].[VW_IX_Vehicle]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			VW_IX_Vehicle
**	Description:	View retunrs list of all vehicles with theirs Unit details
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

CREATE VIEW [dbo].[VW_IX_Vehicle] WITH SCHEMABINDING 
AS

--
--	Please use VW_Vehicle view
--

	SELECT 
		v.ID
		, v.VehicleNumber
		, v.Type
		, VehicleTypeID		= 1 
		, v.VehicleOrder
		, UnitID			= u.ID
		, u.UnitNumber
		, u.UnitType
		, FleetID			= f.ID
		, FleetCode			= f.Code
		, FleetName			= f.Name 
	FROM dbo.Vehicle v
	INNER JOIN dbo.Unit u ON u.ID = v.UnitID
	INNER JOIN dbo.Fleet f ON f.ID = u.FleetID
	WHERE v.HardwareTypeID IS NOT NULL


GO
/****** Object:  Index [IX_CL_U_VW_IX_Vehicle_ID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Vehicle_ID] ON [dbo].[VW_IX_Vehicle]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Vehicle_ID_VehicleTypeID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Vehicle_ID_VehicleTypeID] ON [dbo].[VW_IX_Vehicle]
(
	[ID] ASC,
	[VehicleTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO

/****** Object:  View [dbo].[VW_Vehicle]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_Vehicle]
AS

SELECT 
	ID
	, VehicleNumber
	, Type
	, VehicleTypeID
	, UnitID
	, UnitNumber
	, UnitType
	, VehicleOrder
	, FleetID
	, FleetCode
	, FleetName
FROM dbo.VW_IX_Vehicle WITH (NOEXPAND)

GO
/****** Object:  View [dbo].[VW_IX_FleetStatus]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			VW_IX_FleetStatus
**	Description:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	


CREATE VIEW [dbo].[VW_IX_FleetStatus] WITH SCHEMABINDING 
AS
--
-- Please use VW_FleetStatus View
--

	SELECT
		fs.ID AS ID 
		,Unit.ID AS UnitID
		,UnitNumber
		,UnitType
		,UnitPosition
		,UnitOrientation
		,FleetFormationID
		,Diagram
		,Headcode
		,Loading
		,'' AS VehicleList
		,Vehicle.ID AS VehicleID
		,Vehicle.VehicleOrder AS VehiclePosition
		,VehicleNumber
		,Type AS VehicleType
		,1 AS VehicleTypeID
		,LocationIDHeadcodeStart
		,LocationIDHeadcodeEnd
	FROM dbo.FleetStatus fs
	INNER JOIN dbo.Unit ON Unit.ID = fs.UnitID
	INNER JOIN dbo.Vehicle ON Vehicle.UnitID = Unit.ID
	WHERE ValidTo IS NULL
		AND Vehicle.HardwareTypeID IS NOT NULL


GO
/****** Object:  Index [IX_CL_U_VW_IX_FleetStatus_ID_VehicleID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FleetStatus_ID_VehicleID] ON [dbo].[VW_IX_FleetStatus]
(
	[ID] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  View [dbo].[VW_FleetStatus]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_FleetStatus]
AS
SELECT
	ID 
	,UnitID
	,UnitNumber
	,UnitType
	,UnitPosition
	,UnitOrientation
	,FleetFormationID
	,Diagram
	,Headcode
	,Loading
	,VehicleList
	,VehicleID
	,VehiclePosition
	,VehicleNumber
	,VehicleType
	,VehicleTypeID
	,LocationIDHeadcodeStart
	,LocationIDHeadcodeEnd
FROM VW_IX_FleetStatus WITH (NOEXPAND)

GO

/****** Object:  View [dbo].[VW_IX_Fault]    Script Date: 17/08/2016 15:50:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 	

GO
/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 15/07/2016 09:41:30 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 15/07/2016 09:41:30 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO


/****** Object:  View [dbo].[VW_FaultHistory]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			VW_FaultHistory
**	Description:	View returns list of faults for Fault History screen
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

CREATE VIEW [dbo].[VW_FaultHistory]
AS

	SELECT 
		f.ID
		, f.FaultMetaID
		, fv.UnitNumber			AS UnitNumber
		, f.CreateTime			AS CreateTime
		, f.IsCurrent			AS IsCurrent
		, f.RecoveryID			AS RecoveryID
		, fv.VehicleNumber		AS FaultVehicle
		, f.EndTime				AS EndTime
		, f.Headcode			AS HeadCode
		, l.Tiploc				AS LocationCode
		, f.Latitude			AS Latitude
		, f.Longitude			AS Longitude
		, f.FaultCode			AS FaultCode
		, f.FaultType
		, f.FaultTypeColor
		, f.Description			AS FaultDescription
		, f.Summary				AS FaultSummary
		, f.Category			AS FaultCategory
		, fv.FleetID			AS FleetID
		, fv.FleetCode			AS FleetCode

	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN VW_Vehicle fv ON fv.UnitID = f.FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID


GO
/****** Object:  View [dbo].[VW_IX_Fault]    Script Date: 17/08/2016 15:57:43 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO
--CREATE VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
--	AS
--		SELECT 
--		f.ID    
--		, f.CreateTime    
--		, f.HeadCode  
--		, f.SetCode   
--		, fm.FaultCode   
--		, f.LocationID 
--		, f.IsCurrent 
--		, f.EndTime   
--		, f.RecoveryID    
--		, f.Latitude  
--		, f.Longitude 
--		, f.FaultMetaID   
--		, f.FaultUnitID
--		, FaultUnitNumber		= uf.UnitNumber 
--		, f.PrimaryUnitID
--		, f.SecondaryUnitID
--		, f.TertiaryUnitID
--		, f.IsDelayed 
--		, fm.Description   
--		, fm.Summary   
--		, fm.CategoryID
--		, fc.Category
--		, fc.ReportingOnly
--		, fm.FaultTypeID
--		, FaultType			= ft.Name
--		, FaultTypeColor	= ft.DisplayColor
--		, f.IsAcknowledged	
--		, f.AcknowledgedTime
--		, ft.Priority
--		, f.RecoveryStatus
--		FROM dbo.Fault f
--		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
--		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
--		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
--		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID	

--GO
/****** Object:  View [dbo].[VW_IX_FaultLive]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_IX_FaultLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
	FROM dbo.Fault f
	INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
	INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
	INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
	INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
	INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
	WHERE IsCurrent = 1

GO


/****** Object:  Index [IX_CL_U_VW_IX_FaultLive_ID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultLive_ID] ON [dbo].[VW_IX_FaultLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

/****** Object:  Index [IX_VW_IX_FaultLive_CreateTime]    Script Date: 15/07/2016 09:41:30 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultLive_CreateTime] ON [dbo].[VW_IX_FaultLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  View [dbo].[VW_FaultLive]    Script Date: 17/08/2016 16:01:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_FaultLive]
AS
    SELECT 
            ID    
          , CreateTime 
          , HeadCode 
          , SetCode 
          , FaultCode 
          , LocationID 
          , IsCurrent 
          , EndTime 
          , RecoveryID 
          , Latitude 
          , Longitude 
          , FaultMetaID 
          , FaultUnitID 
          , FaultUnitNumber 
          , PrimaryUnitID 
          , SecondaryUnitID 
          , TertiaryUnitID 
          , IsDelayed 
          , Description 
          , Summary 
          , CategoryID 
          , Category 
          , ReportingOnly 
          , FaultTypeID 
          , FaultType 
          , FaultTypeColor 
          , IsAcknowledged 
          , AcknowledgedTime 
          , Priority 
          , RecoveryStatus
		  , FleetCode 
    FROM dbo.VW_IX_FaultLive (NOEXPAND)     
GO

/****** Object:  View [dbo].[VW_IX_FaultNotLive]    Script Date: 17/08/2016 16:02:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_IX_FaultNotLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
		WHERE IsCurrent = 0

GO

/****** Object:  Index [IX_CL_U_VW_IX_FaultNotLive_ID]    Script Date: 15/07/2016 09:41:30 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultNotLive_ID] ON [dbo].[VW_IX_FaultNotLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO

/****** Object:  Index [IX_VW_IX_FaultNotLive_CreateTime]    Script Date: 15/07/2016 09:41:30 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultNotLive_CreateTime] ON [dbo].[VW_IX_FaultNotLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  View [dbo].[VW_FaultNotLive]    Script Date: 17/08/2016 16:03:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_FaultNotLive]
AS
    SELECT 
    ID
      , CreateTime 
      , HeadCode 
      , SetCode 
      , FaultCode 
      , LocationID 
      , IsCurrent 
      , EndTime 
      , RecoveryID 
      , Latitude 
      , Longitude 
      , FaultMetaID 
      , FaultUnitID 
      , FaultUnitNumber 
      , PrimaryUnitID 
      , SecondaryUnitID 
      , TertiaryUnitID 
      , IsDelayed 
      , Description 
      , Summary 
      , CategoryID 
      , Category 
      , ReportingOnly 
      , FaultTypeID 
      , FaultType 
      , FaultTypeColor 
      , IsAcknowledged 
      , AcknowledgedTime 
      , Priority 
      , RecoveryStatus
	  , FleetCode 
        FROM dbo.VW_IX_FaultNotLive (NOEXPAND)

GO

/****** Object:  View [dbo].[VW_ChannelDefinition]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			VW_ChannelDefinition
**	Description:	Wrapper view for VW_ChannelDefinition table. 
*******************************************************************************/	

CREATE VIEW [dbo].[VW_ChannelDefinition]
AS
	SELECT     
		c.ID
		, c.VehicleID
		, c.EngColumnNo AS ColumnID
		, c.ChannelGroupID
		, c.Header AS Name
		, c.Name AS Description
		, c.ChannelGroupOrder
		, ct.Name AS Type
		, c.UOM
		, c.HardwareType
		, c.StorageMethod
		, c.Ref
		, c.MinValue
		, c.MaxValue
		, c.Comment
		, c.VisibleOnFaultOnly
		, c.IsAlwaysDisplayed
		, c.IsLookup
		, c.DefaultValue
		, c.IsDisplayedAsDifference
	FROM dbo.Channel AS c 
	INNER JOIN dbo.ChannelType AS ct ON c.TypeID = ct.ID

GO
/****** Object:  View [dbo].[VW_ChannelGroup]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_ChannelGroup]
AS
SELECT 
	EngColumnNo
	, Channel.Name
	, Header
	, ChannelGroupName
	, ChannelType.Name AS ChannelType
	, 1 as VehicleTypeID
	, ChannelGroupOrder
FROM Channel 
INNER JOIN ChannelGroup ON ChannelGroupID = ChannelGroup.ID
INNER JOIN ChannelType ON TypeID = ChannelType.ID

GO
/****** Object:  View [dbo].[VW_ChannelValueLookup]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name: VW_ChannelValueLookup
**	Description:	What does it do? What does it return? 
**					Who/what uses it? How often is it used?
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

CREATE VIEW [dbo].[VW_ChannelValueLookup]
AS

SELECT
	ChannelID
	, EngColumnNo
	, Value
	, DisplayValue
FROM Channel
INNER JOIN ChannelValueLookup ON Channel.ID = ChannelID

GO
/****** Object:  View [dbo].[VW_ReportAdhesion]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_ReportAdhesion] 
AS
SELECT
    TimeRange	= CASE
        WHEN Timestamp < DATEADD(n, -1440, SYSDATETIME()) THEN 3
        WHEN Timestamp < DATEADD(n, -360, SYSDATETIME()) THEN 2
		ELSE 1
    END
    , WSP		= Value
	, GCourse
	, Latitude
	, Longitude
	, LocationCode = (
		SELECT TOP 1 LocationCode --CRS
		FROM Location
		INNER JOIN LocationArea ON LocationID = Location.ID
		WHERE Latitude BETWEEN MinLatitude AND MaxLatitude
			AND Longitude BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	, Timestamp		= CONVERT(datetime, Timestamp)
	, v.VehicleNumber
FROM ReportAdhesion AS ra
INNER JOIN Vehicle AS v ON ra.VehicleID = v.ID
WHERE TimeStamp >= DATEADD(n, -7*1440, GETDATE())
	AND EventType = 1 -- WSP


GO
/****** Object:  View [dbo].[VW_TrainPassageValid]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			VW_TrainPassageValid
**	Description:	Filters TrainPassage table with IsIncludedInAnalysis = 1
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

CREATE VIEW [dbo].[VW_TrainPassageValid]
AS
	SELECT
		ID
		,JourneyID
		,Headcode
		,Setcode
		,StartDatetime
		,EndDatetime
		,AnalyseDatetime
		,EndAVehicleID
		,EndBVehicleID
		,Loading
--		,ToBeProcessed
--		,TimestampAnalysisStarted
--		,TimestampAnalysisCompleted
		,IsIncludedInAnalysis
	FROM dbo.TrainPassage 
	WHERE IsIncludedInAnalysis = 1
	


GO
/****** Object:  View [dbo].[VW_VehicleDownloadProgress]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_VehicleDownloadProgress]
AS
SELECT 
	[ID]
	,[VehicleID]
	,[VehicleDownloadID]
	,[VehicleEventID]
	,[RecordInsert]
	,[EventCategory]
	,[EventCode]
	,[IsProcessed]
	,[StatusUpdated]
	,[StatusUpdatedDatetime]
	,[DetailedStatusDesc] = CASE
		WHEN EventCategory = 24 AND EventCode = 0 THEN '[INFO] RMD - Connected to TMS'
		WHEN EventCategory = 24 AND EventCode = 1 THEN '[FAILED] RMD - Connect timed out'
		WHEN EventCategory = 24 AND EventCode = 2 THEN '[IN PROGRESS] RMD - Manual download request'
		WHEN EventCategory = 24 AND EventCode = 3 THEN '[INFO] RMD - Auto download request'
		WHEN EventCategory = 24 AND EventCode = 4 THEN '[IN PROGRESS] RMD - Download completed ok'
		WHEN EventCategory = 24 AND EventCode = 5 THEN '[FAILED] RMD - Download timed out'
		WHEN EventCategory = 24 AND EventCode = 6 THEN '[FAILED] RMD - Download failed'
		WHEN EventCategory = 24 AND EventCode = 7 THEN '[FAILED] RMD - Got a download command and TMS log not enabled'
		WHEN EventCategory = 24 AND EventCode = 8 THEN '[FAILED] RMD - ATMS Buffer Overrun'

		WHEN EventCategory = 255 AND EventCode = 32 THEN '[FAILED] PL - File upload failed: CRC Error'
		WHEN EventCategory = 255 AND EventCode = 33 THEN '[FAILED] PL - File upload failed: Timeout'
		WHEN EventCategory = 255 AND EventCode = 34 THEN '[FAILED] PL - File upload failed: Aborted'
		WHEN EventCategory = 255 AND EventCode = 35 THEN '[FAILED] PL - File upload failed: Unknown'
		WHEN EventCategory = 255 AND EventCode = 36 THEN '[IN PROGRESS] PL - FileTransfer OK'

		WHEN EventCategory = 1000 AND EventCode = 0 THEN '[COMPLETED] File load - File load completed'
		WHEN EventCategory = 1000 AND EventCode = 1 THEN '[FAILED] File load - File load failed'

		WHEN EventCategory = 1001 AND EventCode = 0 THEN '[IN PROGRESS] SQLiteUpdate - Update OK'
		WHEN EventCategory = 1001 AND EventCode = 1 THEN '[FAILED] SQLiteUpdate - Failed'

	END
FROM [dbo].[VehicleDownloadProgress]

GO
/****** Object:  View [dbo].[VW_VehicleLastChannelValueTimestamp]    Script Date: 15/07/2016 09:41:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			VW_VehicleLastChannelValueTimestamp
**	Description:	View retunrs latest data timestamps for eache vehicle
**					Useed in NSP_FleetSummary
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	


/****** Object:  View [dbo].[VW_UnitLastChannelValueTimestamp]    Script Date: 17/08/2016 16:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
**	Name:			VW_VehicleLastChannelValueTimestamp
**	Description:	View retunrs latest data timestamps for eache vehicle
**					for both for TELOC and TMDS Monitoring
**					Useed in NSP_FleetSummary
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	


CREATE VIEW [dbo].[VW_UnitLastChannelValueTimestamp]
AS 
	SELECT 
		UnitID		= Unit.ID
		, UnitNumber
		, LastTimeStamp = (
			SELECT TOP 1 TimeStamp
			FROM ChannelValue
			WHERE TimeStamp > DATEADD(d, -7, GETDATE())
				AND UnitID = Unit.ID 
			ORDER BY TimeStamp DESC
		)
	FROM Unit 

GO



--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,004
           ,'update_spectrum_db_004.sql'
           ,'1.0.01')
GO