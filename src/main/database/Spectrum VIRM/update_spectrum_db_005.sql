SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 005
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
-- Stored Procedures
------------------------------------------------------------


-------------------------------------------------------------------------------
RAISERROR ('-- Create ALL Stored Procedures', 0, 1) WITH NOWAIT
GO
/****** Object:  StoredProcedure [dbo].[NSP_GetOrInsertLocationIdForTiploc]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Radek Toma
-- Create date: 2011-07-31
-- Description:	Stored procedure returns Location.ID for Tiploc. 
--	If Tiploc doesnt exist, new record is created.
-- =============================================
CREATE PROCEDURE [dbo].[NSP_GetOrInsertLocationIdForTiploc]
	@Tiploc varchar(20)
	, @LocationID int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @LocationID = (SELECT ID FROM Location WHERE Tiploc = @tiploc)
	 					
	IF @LocationID IS NULL --insert tiploc if not exists in database
	BEGIN
		INSERT INTO Location (LocationCode, LocationName, Tiploc, Type)
		SELECT @tiploc, @tiploc, @tiploc, 'U'

		SET @locationID = @@IDENTITY
	END

END
GO

/****** Object:  StoredProcedure [dbo].[NSP_RethrowError]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[NSP_RethrowError]
as
/******************************************************************************
**  Name: usp_RethrowError
**  Description: Re raise the original value with MessageNr: 50000 (default)
**          
**      Example: ....
**               begin catch
**                  if xact_state() <> 0 rollback transaction
**                  EXEC usp_RethrowError;
**               end catch
**          
**          with seterror : Sets the @@ERROR and ERROR_NUMBER values to 50000, 
**          regardless of the severity level
**
**      Called by:  every catch block if it's necessary   
**  
**  Return values:  
*******************************************************************************
**  Change History
*******************************************************************************
**  Date:           Author:     Description:
**  2012-02-01      HeZ         creation on database
*******************************************************************************/    
begin
    -- Return if there is no error information to retrieve.
    IF ERROR_NUMBER() IS NULL
        RETURN;

    DECLARE 
        @ErrorMessage    NVARCHAR(4000),
        @ErrorNumber     INT,
        @ErrorSeverity   INT,
        @ErrorState      INT,
        @ErrorLine       INT,
        @ErrorProcedure  NVARCHAR(200);

    -- Assign variables to error-handling functions that 
    -- capture information for RAISERROR.
    SELECT 
        @ErrorNumber =      ERROR_NUMBER(),
        @ErrorSeverity =    ERROR_SEVERITY(),
        @ErrorState =       ERROR_STATE(),
        @ErrorLine =        ERROR_LINE(),
        @ErrorProcedure =   ISNULL(ERROR_PROCEDURE(), '-');

    -- Build the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();

    -- Raise an error: msg_str parameter of RAISERROR will contain
    -- the original error information.
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        ) with seterror;

END
GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_UpdateFileStatus]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[NSP_Genius_UpdateFileStatus]
	 @fileId int = null
	,@fileName varchar(255) = null
	,@status varchar(255)
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**					status to 'loaded'   
**	Return values:  returns -1 if an error occurred
*******************************************************************************
**	Parameters:
**	Input
**	-----------
**      @fileId		id of the imported file
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:				Author:		Description:
**	2011-09-21		    HeZ			creation on database
*******************************************************************************/	

begin
    SET NOCOUNT ON;

	begin try
		-- either filename or file id must be provided
		if (@fileId is null and @fileName is null)
		begin
			raiserror(N'Either file name or file Id must be provided!', 11, 1);
			return -1;
		end
		
		-- update the status
		update
			dbo.GeniusInterfaceFile
		set
			ProcessingStatus = @status
		where
			ID = isnull(@fileId, ID)
			and Name = isnull(@fileName, Name)
		;
		
	end try
	begin catch
		exec dbo.NSP_RethrowError;
		return -1;
	end catch    
		
    return 0;
end
GO

CREATE PROCEDURE [dbo].[NSP_Genius_WriteLog]
	 @fileId int 
	,@logType tinyint = 1
	,@logText varchar(500)
as 
/******************************************************************************
**	Name: NSP_Genius_WriteLog
**	Description:	
**	Return values:  returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-10-28	    HeZ	creation on database
*******************************************************************************/	

begin
    SET NOCOUNT ON;

	begin try
	-- write log entry
	insert
	dbo.GeniusLog
	(GeniusInterfaceFileId
	,LogType
	,LogText)
	values
	(@fileId
	,@logType
	,@logText)
	;
	
	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch    
	
    return 0;
end

GO

/****** Object:  StoredProcedure [dbo].[NSP_CalculateActualRunPerHeadcode]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_CalculateActualRunPerHeadcode] AS BEGIN RETURN(1) END;
GO


/****** Object:  StoredProcedure [dbo].[NSP_CalculateActualRun]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_CalculateActualRun
**	Description:	SP runs Actual Run analysis for Ops Analysis
**	Call frequency:	Once a day. Run by SQL Agent at 3AM
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/04/26 07:39:20 $$
**	$$Revision: 1.3 $$
**	$$Source: /home/cvs/spectrum/eastcoast/src/main/database/update_spectrum_db_068.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-03-28		RT		Changing isoation level, adding TRY/CATCH block	
**	2012-04-25		RT		Fix detection of headcodes running over midnight					
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_CalculateActualRun]
AS
BEGIN	

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @permanentJourneyID int
	DECLARE @actualJourneyID int
	DECLARE @headcode varchar(10)
	DECLARE @dateValue date
	DECLARE @msg varchar(200)
	DECLARE @counter int
	
	-- Flag rows to be processed:
	-- This are unprocessed rows in last 30 days
	UPDATE JourneyDate SET
		RunActualRunAnalysis = 1
	FROM JourneyDate
	INNER JOIN Journey ON Journey.ID = JourneyDate.ActualJourneyID
	WHERE RunActualRunAnalysis IS NULL
		AND DateValue > DATEADD(D, -30, GETDATE())
		AND 
		(CASE 
			WHEN StartTime < EndTime	-- headcode not running over midnight
				THEN CAST(DateValue AS datetime) + CAST(EndTime AS DATETIME)
			ELSE DATEADD(d, 1, CAST(DateValue AS datetime) + CAST(EndTime AS DATETIME))
		END < DATEADD(hh, -2, GETDATE()))


	WHILE 1=1
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM JourneyDate WHERE RunActualRunAnalysis = 1)
			BREAK
		----------------------------------------------

		SELECT TOP 1
			@permanentJourneyID = PermanentJourneyID
			, @actualJourneyID = ActualJourneyID
			, @headcode = Headcode
			, @dateValue = DateValue
		FROM JourneyDate
		INNER JOIN Journey ON Journey.ID = PermanentJourneyID
		WHERE  RunActualRunAnalysis = 1
		ORDER BY DateValue, Headcode
		
		SET @counter = ISNULL(@counter, 0) + 1
		SET @msg = CAST(SYSDATETIME() AS varchar(30)) + ': ' + CAST(@counter AS CHAR(10)) + ': ' + ISNULL(@headcode, 'NULL') + ': EXEC NSP_CalculateActualRunPerHeadcode ' + CAST(@actualJourneyID AS varchar(10)) + ', ''' + CAST(@dateValue AS varchar(10)) + ''''
		RAISERROR(@msg, 10, 1) WITH NOWAIT
		
		-- Run Actual Run Analysis Per Headcode 
		BEGIN TRY
		
			UPDATE JourneyDate SET 
				RunActualRunAnalysis = 0
				, TimestampAnalysisStarted = GETDATE()
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			EXEC NSP_CalculateActualRunPerHeadcode @actualJourneyID, @dateValue
			
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = GETDATE()
				, AnalysedSuccessfully = 1
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			PRINT 'Processed'
			
		END TRY
		BEGIN CATCH
		
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = GETDATE()
				, AnalysedSuccessfully = 0
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue

			SET @msg = '*** ERROR WHEN PROCESSING ' + ISNULL(@headcode, 'NULL') + ' ***'
			RAISERROR(@msg, 10, 1) WITH NOWAIT

		END CATCH
		
	END

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_CifImportGetNextFilename]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_CifImportGetNextFilename
**	Description:	Generates name of CIF file that supposet to be processed 
**					next
**	Call frequency:	Run from SSIS Package for CIF Timetable Interface.
**	Parameters:		@FileNameInitial - name of first file to be porcessed 
**	Return values:	@FileNameNext - name of the next file to be processed
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-04-11		RT		creation on database
*******************************************************************************/
CREATE PROCEDURE [dbo].[NSP_CifImportGetNextFilename]
(
	@FileNameInitial varchar(20)
	, @FileNameNext varchar(20) OUTPUT
)
AS
BEGIN
	DECLARE @fileLast varchar(20)

	-- get latest successfully processed file (without .cif extention)
	SET @fileLast = (
		SELECT TOP 1 LEFT(OriginalFileName, CHARINDEX('.', OriginalFileName) - 1) 
		FROM LoadCifFile
		WHERE Status = 'Processed'
		ORDER BY FileID DESC)

	IF @FileNameInitial IS NOT NULL
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			LEFT(@fileLast, LEN(@fileLast) -1 )
			+ CASE RIGHT(@fileLast, 1)
				WHEN 'Z' THEN 'A'
				ELSE CHAR(ASCII(RIGHT(@fileLast, 1)) + 1)
			END
	END
	ELSE
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			CASE 
				WHEN CHARINDEX('.', @FileNameInitial) > 1 
					THEN LEFT(@FileNameInitial, CHARINDEX('.', @FileNameInitial) - 1) 
				ELSE @FileNameInitial
			END
	END

	-- convert generated filename to upper-cases
	SET @FileNameNext = UPPER(@FileNameNext)
	
END -- stored procedure

GO
/****** Object:  StoredProcedure [dbo].[NSP_CifImportParseData]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_CifImportParseData]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @sqlQuery varchar(max)
	--------------
	-- HD Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineHD') IS NOT NULL
		DROP TABLE ##CifLineHD

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'HD' AND Name <> 'Spare'
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineHD
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''HD''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BS Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBS') IS NOT NULL
		DROP TABLE ##CifLineBS

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BS' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBS
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BS''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BX Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBX') IS NOT NULL
		DROP TABLE ##CifLineBX

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BX' AND Name NOT IN('Spare', 'Reserved field')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBX
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BX''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LO Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLO') IS NOT NULL
		DROP TABLE ##CifLineLO

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LO' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLO
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LO''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- LI Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLI') IS NOT NULL
		DROP TABLE ##CifLineLI

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LI' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLI
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LI''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- CR Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineCR') IS NOT NULL
		DROP TABLE ##CifLineCR

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'CR' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineCR
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''CR''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LT Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLT') IS NOT NULL
		DROP TABLE ##CifLineLT

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LT' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLT
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LT''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--Add indexes
	CREATE NONCLUSTERED INDEX IX_LI ON [dbo].[##CifLineLI] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LO ON [dbo].[##CifLineLO] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LT ON [dbo].[##CifLineLT] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BS ON [dbo].[##CifLineBS] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BX ON [dbo].[##CifLineBX] ([FileLineNumber])

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_CifImportPopulateSectionData]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_CifImportPopulateSectionData
**	Description:	Populate Section and Segment data based on Section Points 
**					loaded from CIF file
**	Call frequency:	Run for each new timetable
**	Parameters:		@JourneyID 
**	Return values:	@FileNameNext - name of the next file to be processed
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-04-11		RT		creation on database
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_CifImportPopulateSectionData]
	@JourneyID int
AS
BEGIN
	SET NOCOUNT ON;

	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)

	INSERT INTO [Segment]
		([JourneyID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime]
		,[IdealRunTime])
	SELECT 
		[JourneyID]
		, SegmentID
		, (SELECT StartLocationID FROM CteJourtneySegment cjs WHERE cjs.ID =  MIN(CteJourtneySegment.ID)) -- StartSectionID
		, (SELECT EndLocationID FROM CteJourtneySegment cjs WHERE cjs.ID =  MAX(CteJourtneySegment.ID)) -- EndSectionID
		, MIN([StartTime])		AS [StartTime]
		, MAX([EndTime])		AS [EndTime]
		, SUM(ISNULL([RecoveryTime], 0))	AS [RecoveryTime]
		, NULL					AS [IdealRunTime]
	FROM CteJourtneySegment
	GROUP BY 
		[JourneyID]
		, SegmentID


	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)
	INSERT INTO [Section]
		([JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime])
	SELECT
		[JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,ISNULL([RecoveryTime], 0)
	FROM CteJourtneySegment
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_CifImportProcessFile]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_CifImportPopulateSectionData
**	Description:	Process a Cif Timetable file, after it is loaded into 
**					staging table by SSIS package
**	Call frequency:	Run for each new timetable
**	Parameters:		@FileID - FileID from LoadCifFile
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_CifImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	SET DATEFORMAT mdy;
	SET datefirst 7;
	-- operaor code - only records with this code will be processed:
	-- SR	- Scotrail
	-- GR	- Eastcoast
	-- SW	- SWT
	DECLARE @atocCode varchar(10) = 'SR'
	
	------------------------------------------------------------------------------
	-- Check file status, porcess only if status is imported
	------------------------------------------------------------------------------
	IF @FileID IS NOT NULL AND (SELECT Status FROM LoadCifFile WHERE FileID = @FileID) <> 'Imported'
	BEGIN
		UPDATE LoadCifFile SET Status = 'Error - File status different than Imported' WHERE FileID = @FileID
		RAISERROR('Error - File status different than Imported', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		UPDATE LoadCifFile SET Status = 'Processing' WHERE FileID = @FileID
	END

	------------------------------------------------------------------------------
	-- Parse file data into a set or temporary tables, one for each row type  
	------------------------------------------------------------------------------
	DECLARE @spResult int 
	EXEC @spResult = NSP_CifImportParseData @FileID

	IF @spResult <> 0
	BEGIN
		UPDATE LoadCifFile SET Status = 'Error - Error when parsing file' WHERE FileID = @FileID
		PRINT 'Error - Error when parsing file'
		RAISERROR('Error - Error when parsing file', 16, 1)
		RETURN
	END

	------------------------------------------------------------------------------
	-- Reset Status column
	------------------------------------------------------------------------------
	UPDATE LoadCifFileData SET 
		Status = NULL
	WHERE FileID = @FileID

	------------------------------------------------------------------------------
	-- Check this is correct file, not 1 month old
	------------------------------------------------------------------------------
	DECLARE @fileType char(1) -- F - Full, U - update
	DECLARE @dateOfExtract char(6)

	SELECT 
		@fileType = [Bleed-off/UpdateInd]
		, @dateOfExtract = DateofExtract
	FROM ##CifLineHD

/*
	IF DATEDIFF(d, CAST('20'+ SUBSTRING(@dateOfExtract, 5,2) + '-' + SUBSTRING(@dateOfExtract, 3,2) + '-' + SUBSTRING(@dateOfExtract, 1,2) AS date), GETDATE()) > 20 
	BEGIN
		UPDATE LoadCifFile SET Status = 'Error - File older than 20 days' WHERE FileID = @FileID
		PRINT 'Error - File older than 20 days'
		RAISERROR('Error - File older than 20 days', 16, 1)
		RETURN
	END
*/

	------------------------------------------------------------------------------
	-- Delete all records from today going forward (for Full extract only)
	------------------------------------------------------------------------------
	IF @fileType = 'F' 
	BEGIN
		DELETE FROM JourneyDate
		WHERE DateValue >= CAST(GETDATE() AS date)
	END		
	
	------------------------------------------------------------------------------
	-- Start import:
	------------------------------------------------------------------------------
	DECLARE @bsLine int
	
	IF OBJECT_ID('tempdb..#tempJourney') IS NOT NULL
		DROP TABLE #tempJourney

	CREATE TABLE #tempJourney 
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[TrainUID] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
		[Headcode] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartLocationID] [int] NULL,
		[EndLocationID] [int] NULL,
		[StartTime] [time](0) NULL,
		[EndTime] [time](0) NULL,
		[ValidFrom] [datetime] NOT NULL,
		[ValidTo] [datetime] NOT NULL,
		[DaysRun] [char](7) COLLATE Latin1_General_CI_AS NOT NULL,
		[StpType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	IF OBJECT_ID('tempdb..#tempSectionPoint') IS NOT NULL
		DROP TABLE #tempSectionPoint

	CREATE TABLE #tempSectionPoint
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LocationID] [int] NOT NULL,
		[LocationSuffix] [tinyint] NOT NULL,
		[ScheduledArrival] [time](0) NULL,
		[ScheduledDeparture] [time](0) NULL,
		[ScheduledPass] [time](0) NULL,
		[PublicArrival] [time](0) NULL,
		[PublicDeparture] [time](0) NULL,
		[RecoveryTime] [int] NOT NULL,
		[SectionPointType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	-- loop on BS records
	WHILE 1=1
	BEGIN
		SET @bsLine = (SELECT MIN(FileLineNumber) FROM ##CifLineBS WHERE FileLineNumber > ISNULL(@bsLine, 0))
		IF @bsLine IS NULL 
			BREAK
		-----------------------------------------------------------------------
		DECLARE @bsTransactionType char(1) = (SELECT TransactionType FROM ##CifLineBS WHERE FileLineNumber = @bsLine)

		DECLARE @headcode varchar(10) = NULL
		DECLARE @dateRunsFrom date = NULL
		DECLARE @dateRunsTo date = NULL
		DECLARE @daysRun char(7) = NULL
		DECLARE @bankHolidayRunning char(1) = NULL
		DECLARE @journeyID int = NULL
		DECLARE @stpType char(1) = NULL
		DECLARE @trainUID char(6) = NULL
		DECLARE @printOut varchar(200) = NULL
		DECLARE @logEntry varchar(200) = NULL
		DECLARE @existingJourneyID int = NULL
		DECLARE @useExistingJourneyID bit = NULL

		SELECT 
			@headcode = TrainIdentity
			, @dateRunsFrom = DateRunsFrom
			, @dateRunsTo = DateRunsTo
			, @daysRun = DaysRun
			, @bankHolidayRunning = BankHolidayRunning
			, @stpType = STPIndicator
			, @trainUID = TrainUID
		FROM ##CifLineBS 
		WHERE FileLineNumber = @bsLine

		SET @printOut = CAST(@bsLine AS char(6))
			 + ' | ' + CAST(@trainUID AS char(8))
			 + ' | ' + CAST(@headcode as char(8))
			 + ' | ' + CAST(@dateRunsFrom as char(12))
			 + ' | ' + CAST(@dateRunsTo as char(10))
			 + ' | ' + CAST(@daysRun as char(7))
			 + ' | ' + CAST(@bankHolidayRunning as char(9))
			 + ' | ' + CAST(@bsTransactionType as char(7))
			 + ' | ' + CAST(@stpType as char(7))
			 + ' | '
		
		IF @bsTransactionType NOT IN ('N') -- Delete
		BEGIN
			SET @logEntry = @printOut + 'Not processed - Transaction Type not supported'
			UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
		END

		IF @bsTransactionType IN ('N') -- New
		BEGIN 

			-- C - Stp cancellation of Permanent Schedule
			IF @stpType = 'C'  
			BEGIN
				
				UPDATE JourneyDate SET
					ActualJourneyID = NULL
				FROM JourneyDate
				INNER JOIN Journey ON Journey.ID = PermanentJourneyID
				WHERE TrainUID = @trainUID
					AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))

				SET @logEntry = @printOut + 'Stp cancellation of Permanent Schedule. Updated: ' + STR(@@ROWCOUNT)

				UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber =  @bsLine AND FileID = @FileID
						
			END
			
			-- N - New Stp Schedule (not an overlay)
			IF @stpType = 'N'
			BEGIN
				SET @logEntry = @printOut + 'No Action: New Stp Schedule (not an overlay)'

				UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber =  @bsLine AND FileID = @FileID
				
			END

			-- P - New Pemanenet
			-- O - New Overlay Schedule
			IF @stpType IN ('P', 'O')
			BEGIN
			
				-- Check if Headcode is for required ATOC Operator
				IF (SELECT ATOCCode FROM ##CifLineBX WHERE FileLineNumber =  @bsLine + 1) <> @atocCode
				BEGIN
					SET @logEntry = @printOut + 'Headcode for another operator'
					UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber =  @bsLine AND FileID = @FileID 
					CONTINUE
				END 
				
				TRUNCATE TABLE #tempJourney
				TRUNCATE TABLE #tempSectionPoint

				INSERT INTO #tempJourney
					([TrainUID]
					,[Headcode]
					,[ValidFrom]
					,[ValidTo]
					,[DaysRun]
					,[StpType])
				SELECT
					TrainUID		--[TrainUID]
					,TrainIdentity	--[Headcode]
					,DateRunsFrom	--[ValidFrom]
					,DateRunsTo		--[ValidTo]
					,DaysRun		--[DaysRun]
					,STPIndicator	--[StpType]
				FROM ##CifLineBS 
				WHERE FileLineNumber = @bsLine


					-- loop on all lines for a Headcode (Journey)
					DECLARE @lineNumber int = @bsLine
					
					WHILE 1 = 1
					BEGIN
					
						SET @lineNumber = @lineNumber + 1
						DECLARE @lineType char(2) = (SELECT LEFT(Data, 2) FROM LoadCifFileData WHERE FileID = @FileID AND FileLineNumber = @lineNumber)
						
						IF @lineType IN ('ZZ','BS') -- ZZ - end of file, BS - next headcode
						BREAK
						-----------------------------------------
						
						IF @lineType NOT IN ('LO', 'LU', 'LI', 'CR', 'LT') -- we are interested only in these lines
						CONTINUE
						-----------------------------------------
						
						DECLARE @tiploc varchar(8)
						DECLARE @locationID int
						
						-- Origin Location
						IF @lineType = 'LO'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLO WHERE FileLineNumber = @lineNumber)

							EXEC NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
							   ([LocationID]
							   ,[LocationSuffix] 
							   ,[ScheduledArrival]
							   ,[ScheduledDeparture]
							   ,[ScheduledPass]
							   ,[PublicArrival]
							   ,[PublicDeparture]
							   ,[RecoveryTime]
							   ,[SectionPointType])
							SELECT
								@locationID --[LocationID]
							   ,0			--[LocationSuffix] 
							   ,NULL		--[ScheduledArrival]
							   ,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
							   ,NULL		--[ScheduledPass]
							   ,NULL		--[PublicArrival]
							   ,dbo.FN_ConvertCifTime([PublicDeparture])	--[PublicDeparture]
   							   ,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)	--[RecoveryTime]
							   ,'B'			--[SectionPointType]
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
							
							UPDATE #tempJourney SET
								StartLocationID = @locationID
								, StartTime = dbo.FN_ConvertCifTime([ScheduledDeparture]) 
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
			 
						END
						
						-- Intermediate Location
						IF @lineType = 'LI'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLI WHERE FileLineNumber = @lineNumber)

							EXEC NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT

							INSERT INTO #tempSectionPoint
							   ([LocationID]
							   ,[LocationSuffix] 
							   ,[ScheduledArrival]
							   ,[ScheduledDeparture]
							   ,[ScheduledPass]
							   ,[PublicArrival]
							   ,[PublicDeparture]
							   ,[RecoveryTime]
							   ,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								, CASE 
									WHEN ISNUMERIC(LocationSuffix) = 1 
										THEN LocationSuffix
									ELSE 1
								END
							   ,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
							   ,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
							   ,dbo.FN_ConvertCifTime([ScheduledPass])		--[ScheduledPass]
							   ,CASE 
									WHEN [PublicArrival] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicArrival])
								END			--[PublicArrival]
							   ,CASE 
									WHEN [PublicDeparture] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicDeparture])
								END			--[PublicDeparture]
							   ,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)
								--[RecoveryTime]
							   ,CASE 
									WHEN LEN([ScheduledPass]) > 0 THEN 'T'	--Timing Point
									ELSE 'S'	--Station
								END			--[SectionPointType]
							FROM ##CifLineLI
							WHERE FileLineNumber = @lineNumber

						END

						-- Terminating Location
						IF @lineType = 'LT'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLT WHERE FileLineNumber = @lineNumber)

							EXEC NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
							   ([LocationID]
							   ,[LocationSuffix] 
							   ,[ScheduledArrival]
							   ,[ScheduledDeparture]
							   ,[ScheduledPass]
							   ,[PublicArrival]
							   ,[PublicDeparture]
							   ,[RecoveryTime]
							   ,[SectionPointType])
							SELECT
								@locationID --[LocationID]
							   ,0			--[LocationSuffix] 
							   ,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
							   ,NULL		--[ScheduledDeparture]
							   ,NULL		--[ScheduledPass]
							   ,dbo.FN_ConvertCifTime([PublicArrival])		--[PublicArrival]
							   ,NULL		--[PublicDeparture]
							   ,0			--[RecoveryTime]
							   ,'E'			--[SectionPointType]
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber

							UPDATE #tempJourney SET
								EndLocationID = @locationID
								, EndTime = dbo.FN_ConvertCifTime([ScheduledArrival])
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber
								
							BREAK -- We dont need any more records for this headcode
						END			
						
					END --WHILE -- loop on all lines for a Headcode (Journey)

					-- Check if journey is already in database
					SET @existingJourneyID = (
						SELECT ID
						FROM Journey 
						WHERE 
							[Headcode] = @headcode
							AND [ValidFrom] = @dateRunsFrom
							AND [ValidTo] = @dateRunsTo
							AND [TrainUID] = @trainUID
							AND [DaysRun] = @daysRun
							AND [StpType] = @stpType
					)
					
					-- if exists check if section points are the same
					IF @existingJourneyID IS NOT NULL 
					BEGIN

							
						-- delete JourneyDate records if not a full extract
						IF @fileType <> 'F'
						BEGIN
							
							DELETE JourneyDate
							WHERE PermanentJourneyID = @existingJourneyID
								AND DateValue >= CAST(GETDATE() AS date)
							
						END	
						
							
						IF NOT EXISTS 
						(
							SELECT 1 
							FROM
							(
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
								FROM [SectionPoint]
								WHERE [JourneyID] = @existingJourneyID
								
								UNION ALL
								
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]				
								FROM #tempSectionPoint

							) tmp
							GROUP BY 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
							HAVING COUNT(*) = 1
						)
						-- Jounreys in CIF and in DB are identincal:
						BEGIN
							SET @useExistingJourneyID = 1
							
							-- update @journeyID with existing ID
							SET @journeyID = @existingJourneyID
						
							UPDATE Journey SET 
								LastFileID = @FileID
							WHERE ID = @journeyID
							
						END
						ELSE
						-- Jounreys in CIF and in DB are different:
						BEGIN
							SET @useExistingJourneyID = 0

							-- close existing Journey
							UPDATE Journey SET 
								[ValidTo] = GETDATE()
							WHERE ID = @existingJourneyID
							
						END
						
					END
					ELSE
					BEGIN
						SET @useExistingJourneyID = 0
					END --IF @existingJourneyID IS NOT NULL 

					IF @useExistingJourneyID = 0
					BEGIN
						-- insert Journey read from Cif file
					
						-- Populate Journey table
						INSERT INTO [Journey]
							([Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,[FileID]
							,[LineNumber]
							,[HeadcodeDesc]
							,[LastFileID])
						SELECT
							[Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,@FileID
							,@bsLine
							,@headcode + ' (' + [dbo].[FN_GetDaysRunDescription](DaysRun) + ')'
							,@FileID
						FROM #tempJourney

						SET @journeyID = @@IDENTITY

						-- Populate SectionPoint table
						INSERT INTO [SectionPoint]
							([JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType])
						SELECT
							@journeyID --[JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType]
						FROM #tempSectionPoint
						
						-- Populate Segment and Section 
						EXEC NSP_CifImportPopulateSectionData @journeyID

					END -- ELSE - IF @useExistingJourneyID = 1

					-- Populate HeadcodeID
					IF @stpType = 'P'
					BEGIN

						-- for existing JourneyID populate only dates from today forward
						INSERT INTO JourneyDate
							([PermanentJourneyID]
							,[DateValue]
							,[ActualJourneyID])
						SELECT
							@journeyID
							,DateValue
							,@journeyID
						FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun)
						WHERE DateValue >= CAST(GETDATE() AS date) OR @useExistingJourneyID = 0

						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Perm Sched, Existing JourneyID:' 
								ELSE 'Perm Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber =  @bsLine AND FileID = @FileID 
						
					END
					ELSE -- @stpType = '0'
					BEGIN
						
						-- for existing JourneyID populate only dates from today forward
						UPDATE JourneyDate SET
							[ActualJourneyID] = @journeyID
						FROM JourneyDate
						INNER JOIN Journey ON Journey.ID = PermanentJourneyID
						WHERE TrainUID = @trainUID
							AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))
							AND dbo.FN_CompareJorney(PermanentJourneyID, @journeyID) = 0
							AND (DateValue >= CAST(GETDATE() AS date) OR @useExistingJourneyID = 0)
					
						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Over Sched, Existing JourneyID:' 
								ELSE 'Over Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days updated: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE LoadCifFileData SET Status = @logEntry WHERE FileLineNumber =  @bsLine AND FileID = @FileID 
							
					END
					
			END -- IF @stpType IN ('P', 'O') 

		END -- IF @bsTransactionType IN ('N')
		
	END
	
	-- File was fully processed
	UPDATE LoadCifFile SET Status = 'Processed' WHERE FileID = @FileID

END -- end stored procedure


GO
/****** Object:  StoredProcedure [dbo].[NSP_DataArchiving]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_DataArchiving]
(
	@StopDate as date
)
AS
BEGIN
	
DECLARE @outputChannelValue TABLE(
	ID bigint);
	
DECLARE @outputFault TABLE(
	ID bigint);

	
DECLARE 
	 @batch int = 10000
	,@loopmax int = 10000
	,@archived int = 0
	,@stop datetime2 
	,@tempStop datetime2
	,@start datetime2 
	,@id int
	,@d datetime2
	,@t datetime2 = SYSDATETIME()
	,@cnt int 
	,@loop int = 0
	,@unitid int = 0
	,@msg varchar(1000)
	,@StartTime datetime2 = GETDATE();


WHILE 1 = 1
BEGIN

	SET @start = (SELECT MIN(timestamp)	FROM $(db_name).dbo.ChannelValue)

	SELECT
		@stop = @StopDate 
		, @t = SYSDATETIME()
		, @archived = 0
		, @tempStop = DATEADD(N, 5, @start)

	IF @tempStop > @stop
	BEGIN

		INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,'Data Archiving Finished')
		BREAK;

	END
	


--	SET @msg = 'Start: ' + convert(varchar, @start, 121)+ '; Stop: ' +convert(varchar, @stop, 121) + '; TempStop: ' + convert(varchar, @tempStop, 121)

--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
--		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,@msg)
	
		

	---------------------------------------------------------------------------
	-- Fault and FaultChannelValue tables
	---------------------------------------------------------------------------
	BEGIN TRAN

	BEGIN TRY

		SET @archived = 0
		DELETE @outputFault

		INSERT $(db_name)_Archive.dbo.Fault_Archive
		OUTPUT INSERTED.ID
			INTO @outputFault
		SELECT TOP (@batch / 10) * FROM $(db_name).dbo.Fault 
		WHERE CreateTime <= DATEADD(n, 5, @tempStop)
			AND CreateTime <= @stop
		ORDER BY 
			CreateTime ASC
			, ID ASC;
		
		SET @archived = @@ROWCOUNT;

		IF @archived > 0
		BEGIN

			INSERT $(db_name)_Archive.dbo.FaultChannelValue_Archive
			SELECT *
			FROM $(db_name).dbo.FaultChannelValue fcv 
			WHERE FaultID IN (SELECT ID FROM @outputFault)

			DELETE $(db_name).dbo.FaultChannelValue
			WHERE FaultID IN (SELECT ID FROM @outputFault)
			
			DELETE $(db_name).dbo.Fault
			WHERE ID IN (SELECT ID FROM @outputFault)
				AND CreateTime <= DATEADD(n, 5, @tempStop)
			
		END
		
		COMMIT;
		
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK;
		EXEC dbo.nsp_RethrowError;
		RETURN 1
	END CATCH

	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',@archived,@loop,'Faults and FaultChannelValues: ' + convert(varchar(10), @archived) 
		+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms')
	



	---------------------------------------------------------------------------
	-- ChannelValue table
	---------------------------------------------------------------------------
	BEGIN TRAN

	BEGIN TRY
	
		DELETE @outputChannelValue
		
		INSERT $(db_name)_Archive.dbo.ChannelValue_Archive
		OUTPUT INSERTED.ID
			INTO @outputChannelValue
		SELECT TOP(@batch) * 
		FROM $(db_name).dbo.ChannelValue 
		WHERE TimeStamp <= @tempStop
		ORDER BY TimeStamp asc;
		
		DELETE
		FROM $(db_name).dbo.ChannelValue
		WHERE ID IN (SELECT ID FROM @outputChannelValue)
		
		SELECT @archived = @@ROWCOUNT;
		
		COMMIT;
		
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK;
		EXEC dbo.nsp_RethrowError;
		RETURN 1
	END CATCH

		INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',@archived,@loop,'ChannelValue:                  ' + convert(varchar(10), @archived)
		+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms')


	WHILE @@TRANCOUNT > 0
		ROLLBACK;


--	SET @msg = 'Iteration: ' + convert(varchar, @loop)
--		+ ';		Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms'
--		+ ';		Open Transactions: ' + convert(varchar(10), @@trancount);

--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
--		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,@msg)
	------------------------------------------------------------------------------
	SET @loop = @loop + 1
	IF @loop > @loopmax
		BREAK

	WAITFOR DELAY '00:00:02'

END

END
GO
/****** Object:  StoredProcedure [dbo].[NSP_DataArchivingBackupAndTruncate]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_DataArchivingBackupAndTruncate]
AS
BEGIN

	DECLARE @faultStart datetime	
	DECLARE @faultStop datetime	

	BEGIN TRY

		SELECT @faultStart = Min(timestamp) FROM $(db_name)_Archive.dbo.ChannelValue_Archive
		SELECT @faultStop = Max(timestamp) FROM $(db_name)_Archive.dbo.ChannelValue_Archive

		DECLARE @backupString nvarchar(100) = 'D:\!Brian\$(db_name)_Archive_' +  isnull(CONVERT(varchar(8), @faultStop, 112),'') + 'at_'+CONVERT(varchar(8), GETDATE(), 112)+'.bak'

		DECLARE @msg varchar(1000) = 'Backing up database to ' + @backupString
		RAISERROR(@msg, 10, 1) WITH NOWAIT

		BACKUP DATABASE [$(db_name)] TO DISK = @backupString
			WITH COMPRESSION, CHECKSUM, STATS = 10;

	
		RESTORE VERIFYONLY FROM  DISK = @backupString
	END TRY
	BEGIN CATCH
		RETURN 1
	END CATCH
	

	TRUNCATE TABLE [$(db_name)]..ChannelValue_Archive
	TRUNCATE TABLE [$(db_name)]..FaultChannelValue_Archive
	TRUNCATE TABLE [$(db_name)]..Fault_Archive

	RETURN 0
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_DbaDataArchiving]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[NSP_DbaDataArchiving]
AS
BEGIN

	SET NOCOUNT ON; 

	RETURN

	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE ChannelValue', 10, 1) WITH NOWAIT;
	SET NOCOUNT ON;
	DECLARE @msg varchar(150)
	DECLARE @timestamp datetime2(3) = (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM ChannelValue (NOLOCK)) --'2012-04-01 00:00:00'
	DECLARE @maxTimestamp datetime2(3) = DATEADD(D, -7, SYSDATETIME())
	DECLARE @t datetime2(3)
	DECLARE @backupFile varchar(1000) 
	DECLARE @interval int = 30
	DECLARE @interval_OLD int = 1440 -- interval to be used for data older than 1 month - ScotRail can insert a spurious record from a LONG time ago, screwing up the time to complete the archiving
	DECLARE @sql varchar(1000)

	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM ChannelValue 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '   Delete duration: ' 
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(23), DATEDIFF(ms, @t, SYSDATETIME())), 10), 'NULL')
			+ '   Records deleted: '
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
		SET @timestamp = DATEADD(n, @interval, @timestamp);
				
		WAITFOR DELAY '00:00:01'
				
	END
	-------------------------------------------------------------------------------
	-- VehicleEvent
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE VehicleEvent', 10, 1) WITH NOWAIT;
	SET @t				= NULL
	SET @msg			= NULL
	SET @timestamp		= (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM VehicleEvent (NOLOCK)) --'2012-04-01 00:00:00'
	SET @maxTimestamp	= DATEADD(D, -7, SYSDATETIME())
	SET @interval		= 30 --mins
	
	
	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM VehicleEvent 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '   Delete duration: ' 
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '   Records deleted: '
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
	
		IF @timestamp < DATEADD(month,-1,SYSDATETIME())
		BEGIN
			SET @timestamp = DATEADD(n, @interval_OLD, @timestamp)
		END
		ELSE
		BEGIN
			SET @timestamp = DATEADD(n, @interval, @timestamp);
		END
				
		WAITFOR DELAY '00:00:01'
				
	END


	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE Fault', 10, 1) WITH NOWAIT;

	DECLARE @chunk int	= 1000
	DECLARE @rowCount int
	DECLARE @rowCountFaultChannelValue int
	SET @t				= NULL
	SET @msg			= NULL
	
	DECLARE @FaultToDelete TABLE(
		ID int
	)


	WHILE 1=1
	BEGIN
		SET @t = SYSDATETIME()
		
		INSERT INTO @FaultToDelete(ID)
		SELECT TOP (@chunk) ID
		FROM Fault 
		WHERE CreateTime < DATEADD(d, -7, SYSDATETIME());
	
		DELETE FaultChannelValue
		WHERE FaultID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCountFaultChannelValue = @@ROWCOUNT
			
		DELETE Fault
		WHERE ID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCount = @@ROWCOUNT 

		IF @rowCount = 0 
			BREAK

		SET @msg = 
			'Current Time: ' 
			+ CONVERT(char(23), SYSDATETIME(), 121)
			+ '   Delete duration: ' 
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '   Records deleted: '
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(20), @rowCount), 10), 'NULL')
			+ '/'
			+ ISNULL(RIGHT('          ' + CONVERT(varchar(20), @rowCountFaultChannelValue), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
			
		WAITFOR DELAY '00:00:01'
	END

END -- stored procedure

GO
/****** Object:  StoredProcedure [dbo].[NSP_DeleteFaultCategory]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[NSP_DeleteFaultCategory](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY

		DELETE dbo.FaultCategory 
		WHERE ID = @ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
	
END



GO
/****** Object:  StoredProcedure [dbo].[NSP_DeleteFaultMeta]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[NSP_DeleteFaultMeta](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY

		DELETE dbo.FaultMeta 
		WHERE ID = @ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		IF ERROR_NUMBER() = 547 -- FOREIGN KEY ERROR
    		BEGIN
    			RAISERROR ('ERR_FAULT_EXIST', 14, 1) WITH NOWAIT
    		END
    	ELSE
    		BEGIN
    			EXEC NSP_RethrowError;
    		END
    		
		ROLLBACK TRAN;
		
	END CATCH
	
END



GO
/****** Object:  StoredProcedure [dbo].[NSP_EaFaultCategories]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_EaFaultCategories
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaFaultCategories]
AS
BEGIN
	
	SELECT
		FaultCategory = ID
		, FaultCategoryName = Category
	FROM FaultCategory	
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_EaFaultTypes]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_EaFaultTypes
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaFaultTypes]
AS
BEGIN
	
	SELECT 
		FaultType		= ID
		, FaultTypeName	= Name
		, FaultColor	= DisplayColor
	FROM FaultType
		
		
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_EaReport]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**  Name:           NSP_EaReport
**  Description:    
**  Call frequency: Called from Event Analysis 
**  Parameters:     
**  Return values:  
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaReport]
(   @FleetCode varchar(10)
    , @DateFrom datetime
    , @DateTo datetime
    -- additional parameters: need to be in alphabetical order
    , @CategoryID varchar(100)
    , @FaultMetaID varchar(max)
    , @FaultTypeID varchar(50)
    , @Headcode varchar(10)
    , @LocationID varchar(max)
    , @UnitID varchar(max)
    , @VehicleID varchar(max)  
)
AS
BEGIN
    
    DECLARE @FaultCategoryTable TABLE (CategoryID varchar(10))
    INSERT INTO @FaultCategoryTable
    SELECT * from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultMetaTable TABLE (FaultMetaID varchar(10))
    INSERT INTO @FaultMetaTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultMetaID,',')

    DECLARE @HeadcodeTable TABLE (Headcode varchar(10))
    INSERT INTO @HeadcodeTable
    SELECT * from [dbo].SplitStrings_CTE(@Headcode,',')

    DECLARE @CategoryTable TABLE (CategoryID varchar(10))
    INSERT INTO @CategoryTable
    SELECT * from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeID varchar(10))
    INSERT INTO @FaultTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultTypeID,',')

    DECLARE @LocationTable TABLE (LocationID varchar(10))
    INSERT INTO @LocationTable
    SELECT * from [dbo].SplitStrings_CTE(@LocationID,',')

    DECLARE @UnitTable TABLE (UnitID varchar(100))
    INSERT INTO @UnitTable
    SELECT * from [dbo].SplitStrings_CTE(@UnitID,',')

    DECLARE @VehicleTable TABLE (VehicleID varchar(100))
    INSERT INTO @VehicleTable
    SELECT * from [dbo].SplitStrings_CTE(@VehicleID,',')
    

    IF @CategoryID = '' SET @CategoryID = NULL
    IF @FaultTypeID = '' SET @FaultTypeID = NULL
    IF @FaultMetaID = '' SET @FaultMetaID = NULL
    IF @Headcode = '' SET @Headcode = NULL
    IF @LocationID = '' SET @LocationID = NULL
    IF @UnitID = '' SET @UnitID = NULL
    IF @VehicleID = '' SET @VehicleID = NULL

    SELECT 
        FaultID = f.ID    
        , CreateTime    
        , EndTime   
        , IsCurrent

        , HeadCode  
        , SetCode   
        , FleetCode = NULL
        , UnitNumber    
        , UnitID    = FaultUnitID
        , FaultVehicleID    = NULL
        , FaultVehicleNumber = NULL

        , FaultMetaID   
        , FaultCode   
        , Description

        , FaultTypeID
        , FaultType
        , FaultTypeColor
        , Priority

        , CategoryID
        , Category

        , Latitude  
        , Longitude    
        , LocationID
        , LocationCode  
        
    FROM VW_IX_Fault f (NOEXPAND)
    INNER JOIN dbo.Unit u ON u.ID = FaultUnitID
    LEFT JOIN dbo.Location l ON l.ID = LocationID
    WHERE (f.FaultUnitID IN (SELECT * FROM @UnitTable) OR @UnitID IS NULL)
        AND (f.FaultMetaID IN (SELECT * FROM @FaultMetaTable) OR @FaultMetaID IS NULL)
        AND (f.CreateTime BETWEEN @DateFrom AND @DateTo)
        AND (LocationID IN (SELECT * FROM @LocationTable) OR @LocationID IS NULL OR (@LocationID = '-1' AND LocationID IS NULL))
        AND (HeadCode = @Headcode OR @Headcode IS NULL OR (@Headcode = '-1' AND HeadCode IS NULL))
        --AND (FleetCode = @FleetCode OR @FleetCode IS NULL)
		AND (CategoryID IN (SELECT * FROM @CategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))
        AND (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultMetaID = '-1' AND FaultTypeID IS NULL))

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_EaSearchFaultMeta]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**  Name:           NSP_EaSearchFaultMeta
**  Description:    
**  Call frequency: Called from Ops Analysis 
**  Parameters:     
**  Return values:  
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/
CREATE PROCEDURE [dbo].[NSP_EaSearchFaultMeta]
(
    @FaultString varchar(50)
    , @FaultCategoryID int
    , @FaultTypeID int
)
AS
BEGIN
    SELECT 
        FaultID     = fm.ID
        , FaultDesc = fm.Description
        , FaultCode
    FROM FaultMeta fm
    INNER JOIN FaultType ft ON ft.ID = fm.FaultTypeID
    INNER JOIN FaultCategory fc ON fc.ID = fm.CategoryID
    WHERE
        ft.ID = ISNULL(@FaultTypeID, ft.ID)
        AND fc.ID = ISNULL(@FaultCategoryID, fc.ID)
        AND
        (
            Description LIKE '%' +  @FaultString + '%'
            OR
            FaultCode LIKE '%' +  @FaultString + '%'
        )

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_EaSearchLocation]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_EaSearchLocation
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaSearchLocation]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
		, LocationCode
	FROM Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationCode LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_EaSearchUnit]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**  Name:           NSP_EaSearchUnit
**  Description:    
**  Call frequency: Called from Ops Analysis 
**  Parameters:     
**  Return values:  
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaSearchUnit]
(
     @UnitString varchar(50)
)
AS
BEGIN

    SELECT
        FleetCode = f.Code
        , UnitID  = u.ID
        , UnitNumber = u.UnitNumber
    FROM Unit u
    JOIN Fleet f
    ON f.ID = u.FleetID
    WHERE UnitNumber LIKE '%' +  @UnitString + '%' 
        
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_EaSearchVehicle]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**  Name:           NSP_EaSearchVehicle
**  Description:    
**  Call frequency: Called from Ops Analysis 
**  Parameters:     
**  Return values:  
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaSearchVehicle]
(
    @VehicleType varchar(50)
    , @VehicleString varchar(50)
    , @UnitID int = NULL
)
AS
BEGIN

    SELECT
        FleetCode = f.Code
        , VehicleID   = v.ID
        , VehicleNumber = v.VehicleNumber
        , VehicleType = v.Type
    FROM Vehicle v
    JOIN Unit u
    ON u.ID = v.UnitID
    JOIN Fleet f
    ON f.ID = u.FleetID
    WHERE VehicleNumber LIKE '%' +  @VehicleString + '%' 
        AND (Type = @VehicleType OR @VehicleType IS NULL)
        AND (UnitID = @UnitID OR @UnitID IS NULL)
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_EaVehicleTypes]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_EaVehicleTypes
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_EaVehicleTypes]
AS
BEGIN

	SELECT DISTINCT
		VehicleType		= Type
	FROM Vehicle
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetFaultList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaGetFaultList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetFaultList]
(
	@FaultTypeID int
	, @FaultString varchar(50)
)
AS
BEGIN

	SELECT 
		FaultID		= fm.ID
		, FaultDesc	= fm.Description
	FROM FaultMeta fm
	INNER JOIN FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND
		(
			Description LIKE '%' +  @FaultString + '%'
			OR
			FaultCode LIKE '%' +  @FaultString + '%'
		)


END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetFaultTypeList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaGetFaultTypeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetFaultTypeList]
AS
BEGIN
	
	SELECT 
		FaultType		= ID
		, FaultTypeName	= Name
	FROM FaultType
		
		
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetLocationList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaGetLocationList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetLocationList]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
		, LocationCode
	FROM Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationCode LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetUnitList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_FaGetUnitList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetUnitList]
(
	 @UnitString varchar(50)
)
AS
BEGIN

	SELECT 
		UnitID	= ID
		, UnitNumber
	FROM Unit
	WHERE UnitNumber LIKE '%' +  @UnitString + '%' 
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetVehicleList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaGetVehicleList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetVehicleList]
(
	@VehicleType varchar(50)
	, @VehicleString varchar(50)
	, @UnitID int = NULL
)
AS
BEGIN

	SELECT 
		VehicleID	= ID
		, VehicleNumber
	FROM Vehicle
	WHERE VehicleNumber LIKE '%' +  @VehicleString + '%' 
		AND (Type = @VehicleType OR @VehicleType IS NULL)
		AND (UnitID = @UnitID OR @UnitID IS NULL)
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaGetVehicleTypeList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaGetVehicleTypeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaGetVehicleTypeList]
AS
BEGIN

	SELECT DISTINCT
		VehicleType		= Type
	FROM Vehicle
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultByDate]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByDate
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultByDate]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)	

	SELECT 
		CAST(CAST(CreateTime AS DATE) AS DATETIME) CreateTime
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN Vehicle v ON v.unitid = faultunitid
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		CAST(CAST(CreateTime AS DATE) AS DATETIME)
	ORDER BY CAST(CAST(CreateTime AS DATE) AS DATETIME) ASC

		

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultByFaultCode]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByFaultCode
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultByFaultCode]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)


	SELECT
		FaultMetaID
		, FaultCode
		, Description
		, Category
		, Type = FaultType
		, EventCount			= COUNT(*)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		FaultMetaID
		, FaultCode
		, Description
		, Category
		, FaultType
	ORDER BY EventCount DESC
		

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultByLocation]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByLocation
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultByLocation]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
		LocationID				= ISNULL(LocationID, -1) 
		, LocationName			= ISNULL(LocationName, 'N/A')
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
		, Lat					= ISNULL(Lat,0)
		, Lng					= ISNULL(Lng,0)
		, DisplayColor			= FaultTypeColor
		, Priority
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		LocationID
		, LocationName
		, FaultTypeColor
		, Priority
		, Lat
		, Lng
	ORDER BY 7 DESC

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultByUnit]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByUnit
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultByUnit]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT fm.ID
	FROM FaultMeta fm
	INNER JOIN FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND	fm.ID = ISNULL(@FaultMetaID, fm.ID)	

	SELECT 
		UnitID					= v.UnitID 
		, UnitNumber			= v.UnitNumber
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN VW_Vehicle v ON v.UnitID = f.FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (f.LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		v.UnitID 
		, v.UnitNumber	
	ORDER BY EventCount DESC
		

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultByVehicle]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_FaReportFaultByVehicle
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultByVehicle]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT 
		VehicleID				= v.ID 
		, VehicleNumber
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		v.ID	
		, VehicleNumber
	ORDER BY EventCount DESC
		

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FaReportFaultList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_FaReportFaultList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FaReportFaultList]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	IF @LocationID = -1 SET @LocationID = NULL

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND 
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
			f.ID    
			, CreateTime    
			, HeadCode  
			, SetCode   
			, FaultCode   
			, LocationCode  
			, IsCurrent 
			, EndTime   
			, RecoveryID    
			, Latitude  
			, Longitude    
			, FaultMetaID   
			, FaultUnitNumber    
			, v.ID as FaultVehicleID    
			, FaultVehicleNumber = v.VehicleNumber
			, IsDelayed 
			, Description   
			, Summary   
			, FaultType
			, FaultTypeColor
			, Priority
			, Category
			, ReportingOnly
	FROM VW_IX_Fault f (NOEXPAND)
	INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (f.LocationID = @LocationID OR @LocationID IS NULL)
	ORDER BY CreateTime DESC
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_FleetSummary]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/******************************************************************************
**	Name:			NSP_FleetSummary
**	Description:	Returns live channel data for the application
**	Call frequency:	Every 5s
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FleetSummary]
AS
BEGIN

    SET NOCOUNT ON;
    
    -- Get totals for Vehicle based faults
    DECLARE @currentUnitFault TABLE(
        UnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
    )
    
    INSERT INTO @currentUnitFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
    FROM VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID IS NOT NULL
        AND Category <> 'Test'
        AND ReportingOnly = 0
    GROUP BY FaultUnitID
    
    -- Get todays headcodes
    DECLARE @headcode TABLE(
        Headcode char(4)
        , Diagram varchar(50)
    )

    -- return data
    SELECT 
        fs.[UnitID]
        , fs.[UnitNumber]
        , [UnitType]
        , [FleetFormationID]
        , [UnitPosition]
        , [Diagram]
        , [Headcode] 
        , [Loading]
        ,fs.[VehicleID]
        ,fs.[VehicleNumber]
        ,fs.[VehicleType]
        ,fs.[VehicleTypeID]
        ,fs.[VehiclePosition]
        --,fs.FleetCode
		,NULL AS FleetCode
        ,Location =ISNULL(l.LocationCode, '-') -- ISNULL

        , ISNULL(vf.FaultNumber,0) AS FaultNumber
        , ISNULL(vf.WarningNumber,0) AS WarningNumber
        , VehicleList
        , ServiceStatus = dbo.FN_GetSetState(
                                             fs.UnitNumber
                                            , l.LocationCode
                                            , IsValid
                                            , fs.Diagram
                                        )
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        -- temporary solution for datetime and JTDS driver 
        , TimeStamp = CAST(vlc.LastTimeStamp AS datetime)
		,
        -- Channel Data 
		[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
 FROM VW_FleetStatus fs WITH (NOLOCK)
    INNER JOIN VW_UnitLastChannelValueTimestamp vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
    LEFT JOIN ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
    LEFT JOIN @currentUnitFault vf ON fs.UnitID = vf.UnitID
    LEFT JOIN Location l ON l.ID = (
                                    SELECT TOP 1 LocationID 
                                    FROM LocationArea 
                                    WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                                        AND Col2 BETWEEN MinLongitude AND MaxLongitude
                                    ORDER BY Priority
                                )
END




GO
/****** Object:  StoredProcedure [dbo].[NSP_FleetSummaryDrilldown]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_FleetSummaryDrilldown
**	Description:	Returns live channel data for the application
**	Call frequency:	When user click on grouped cell on Fleet Summary
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
	@UnitIdList varchar(30)
	, @SelectColList varchar(4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @sql varchar(max)
	
	SET @sql = '
SELECT 
	fs.VehicleID
	,fs.VehicleNumber
	, ' + @SelectColList + '
FROM VW_FleetStatus fs
INNER JOIN VW_VehicleLastChannelValueTimestamp vlc ON vlc.VehicleID = fs.VehicleID
LEFT JOIN ChannelValue cv ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.VehicleID = cv.VehicleID
WHERE fs.UnitID in (' + @UnitIdList + ')
ORDER BY 
	fs.FleetFormationID
	, fs.UnitPosition
	, fs.VehiclePosition
'

	EXEC (@sql)
	
	

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_CreateNewFile]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NSP_Genius_CreateNewFile]
	 @file varchar(255)
	,@fileTimeStamp datetime
	,@fileId int out
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	creates a new entry for a new import file in the Genius 
**	Interface tables   
**	Return values:  returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-22	    HeZ	creation on database
*******************************************************************************/	

begin
    SET NOCOUNT ON;

	begin try
	-- insert filename
	insert
	dbo.GeniusInterfaceFile
	(Name
	,FileTimeStamp)
	values
	(@file
	,@fileTimeStamp)
	;
	
	set @fileId = scope_identity();	
	
	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch    
	
end

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_ImportDataFromStaging]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NSP_Genius_ImportDataFromStaging]
	 @fileId int
	,@cutoffTime time = '03:00'	 
as 
/******************************************************************************
**	Name: NSP_Genius_ImportDataFromStaging
**	Description:	imports the data from the staging table into the Spectrum
**	data tables
**	
**	Return values:  returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History 
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-25	    HeZ	creation on database
**  2012-03-14	HeZ	adapted for real life data from Genius
*******************************************************************************/	

begin
    SET NOCOUNT ON;

	DECLARE @unitNumber varchar(10) = (
	SELECT TOP 1 UnitNumber 
	FROM GeniusStaging 
	WHERE GeniusInterfaceFileID = @fileId 
	AND UnitNumber IS NOT NULL
	)
	DECLARE @unitID int = (SELECT ID FROM Unit WHERE UnitNumber = @unitNumber)
	DECLARE @logText varchar(2000);
	DECLARE @rowNumber int
	--begin tran

	begin try

--debug
--PRINT ISNULL(@unitId, '-5000')

	BEGIN TRAN
	DELETE dbo.FleetStatusStaging WHERE UnitID = @unitId;
	
	WITH CteGeniusStaging AS
	(
	select distinct
	 gs.DiagramNumber	as DiagramNumber
	,@UnitID	as UnitId
	,gs.HeadCode	as HeadCode
	,gs.TrainStartLocation	as TrainStartLocation
	,glts.ID	as TrainStartLocationId
	,gs.TrainStartTime	as TrainStartTime
	,te.LegEndLocation	as TrainEndLocation
	,glte.ID	as TrainEndLocationId
	,te.LegEndTime	as TrainEndTime
	,gs.LegStartLocation	as LegStartLocation
	,glls.ID	as LegStartLocationId
	,gs.LegStartTime	as LegStartTime
	,gs.LegEndLocation	as LegEndLocation
	,glle.ID	as LegEndLocationId
	,gs.LegEndTime	as LegEndTime
	,gs.LegLength	as LegLength
	,gs.CoupledUnits	as CoupledUnits	
	,isnull(gs.UnitPosition, 1)	as UnitPosition
	,gs.VehicleFormation	as VehicleFormation
	,/*d.DiagramDate*/ NULL	as DiagramDate
	,row_number() over 
	(partition by gs.DiagramNumber 
	 order by gs.LegStartTime asc)	as SequenceNumber
	from
	dbo.GeniusStaging gs
	inner hash join (
	select HeadCode, DiagramNumber, LegEndLocation, LegEndTime
	from dbo.GeniusStaging gs2
	where LegEndTime = (select max(LegEndTime) from dbo.GeniusStaging gs3
	where gs2.DiagramNumber = gs3.DiagramNumber
	and gs2.HeadCode = gs3.HeadCode
	and gs3.GeniusInterfaceFileID = @fileId)
	and gs2.GeniusInterfaceFileID = @fileId) as te
	on gs.DiagramNumber = te.DiagramNumber and gs.HeadCode = te.HeadCode
	left outer join dbo.Location glts on glts.Tiploc = gs.TrainStartLocation
	left outer join dbo.Location glte on glte.Tiploc = gs.TrainEndLocation
	left outer join dbo.Location glls on glls.Tiploc = gs.LegStartLocation
	left outer join dbo.Location glle on glle.Tiploc = gs.LegEndLocation
	where 
	gs.GeniusInterfaceFileID = @fileId
	)
	INSERT INTO [dbo].[FleetStatusStaging]
	   ([UnitId]
	   ,[DiagramNumber]
	   ,[HeadCode]
	   ,[TrainStartLocationId]
	   ,[TrainStartLocationTiploc]
	   ,[TrainStartTime]
	   ,[TrainEndLocationId]
	   ,[TrainEndLocationTiploc]
	   ,[TrainEndTime]
	   ,[StartLocationId]
	   ,[StartLocationTiploc]
	   ,[StartTime]
	   ,[EndLocationId]
	   ,[EndLocationTipLoc]
	   ,[EndTime]
	   ,[Length]
	   ,[CoupledUnits]
	   ,[UnitPosition]
	   ,[VehicleFormation]
	   ,[SequenceNumber])
	 SELECT
	[UnitId]
	   ,DiagramNumber
	   ,[HeadCode]
	           
	   ,[TrainStartLocationId]
	   ,[TrainStartLocation]
	   ,dbo.FN_ConvertBstToGmt([TrainStartTime])
	           
	   ,[TrainEndLocationId]
	   ,[TrainEndLocation]
	   ,dbo.FN_ConvertBstToGmt([TrainEndTime])
	           
	   ,[LegStartLocationId]
	   ,[LegStartLocation]
	   ,dbo.FN_ConvertBstToGmt([LegStartTime])
	           
	   ,[LegEndLocationId]
	   ,[LegEndLocation]
	   ,dbo.FN_ConvertBstToGmt([LegEndTime])
	           
	   ,[LegLength]
	   ,[CoupledUnits]
	   ,[UnitPosition]
	   ,[VehicleFormation]
	   ,[SequenceNumber]
	FROM CteGeniusStaging
	
	SET @rowNumber = @@ROWCOUNT

	COMMIT


	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileId, @status = 'File fully imported.'
	select @logText =  convert(varchar, @rowNumber) + ' Rows successfully imported.'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;
	--commit;
	
	end try
	begin catch
	--if @@trancount > 0 rollback;
	    SELECT @logText =  ERROR_MESSAGE();	
	exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch    
	
    return 0;
end

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_SetFileId]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NSP_Genius_SetFileId]
	 @fileid int
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**	status to 'loaded'   
**	Return values:  returns -1 if an error occurred
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-21	    HeZ	creation on database
*******************************************************************************/	

begin
    SET NOCOUNT ON;

	begin try
	-- set file id for loaded records
	update
	dbo.GeniusInterfaceData
	set
	GeniusInterfaceFileId = @fileid
	where
	GeniusInterfaceFileId is null
	;
	
	-- update file processing status
	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileid, @status = 'Data loaded from file'

	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch    
	
    return 0;
end

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_ValidateAndCopy]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NSP_Genius_ValidateAndCopy]
	 @FileId int
AS
/******************************************************************************
**	Name:	NSP_Genius_ValidateAndCopy
**	Description:	Checks the imported data against various rules
**	if all checks complete, data is copied to staging table	
**	Return values:  Returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************/	

begin
    SET NOCOUNT ON;
    
    DECLARE @unitNo varchar(16);
	DECLARE @logText varchar(500);
	
	begin try
	-- check 1: only one unit per file
	if (select count(distinct UnitNumber) 
	from dbo.GeniusInterfaceData
	where GeniusInterfaceFileId = @fileId) <> 1
	begin
	raiserror(N'File contains more than one Unit Number!', 11, 1);
	exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File contains more than one Unit Number!';	
	end
	
	-- check 2: is unit on this side of the fleet?
	select top(1)
	@unitNo = UnitNumber
	from
	dbo.GeniusInterfaceData
	where 
	GeniusInterfaceFileId = @fileId
	;
	
	if not exists (select 1 from dbo.Unit where UnitNumber = @unitNo)
	begin
		-- unit doesn't exist on this side
		exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. Unit Number not in database.'
		exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File closed. Unit Number not in database.';	
		raiserror(N'File closed. Unit Number not in database.', 11, 1);
		--return 0;	
	end
	
	select @logText = 'Importing data for Unit: ' + @unitNo;
	
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;	

	-- check 3: only deallocations may have no Headcode
	if exists ( 
	select 1 
	from dbo.GeniusInterfaceData 
	where GeniusInterfaceFileId = @fileId
	and HeadCode is null
	and dbo.fn_CheckZero(DiagramId) <> 0)
	begin
	-- Headcode missing
	exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. headcode missing.'
	exec NSP_Genius_WriteLog @fileId, 2, 'For at least one Leg the headcode is missing.';	
	raiserror(N'For at least one Leg the headcode is missing.', 11, 1);
	end

	
	BEGIN TRAN
	
	DELETE FROM dbo.GeniusStaging
	WHERE UnitNumber = @unitNo
	
	INSERT INTO dbo.GeniusStaging
	(GeniusInterfaceFileID
	,DiagramNumber
	,UnitNumber
	,HeadCode
	,TrainStartLocation
	,TrainStartTime
	,TrainEndLocation
	,TrainEndTime
	,LegStartLocation
	,LegStartTime
	,LegEndLocation
	,LegEndTime
	,LegLength
	,VehicleFormation
	,CoupledUnits
	,UnitPosition
	,SequenceNumber)
	SELECT 
	 GeniusInterfaceFileId
	,isnull(DiagramId, 'NA-' + UnitNumber)
	,UnitNumber
	,isnull(HeadCode, 'NA-'+UnitNumber)
	,TrainStartLocation
	,convert(datetime, TrainStartTime, 103)
	,(	
	SELECT TOP 1 LegEndLocation
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndLocation
	,(	
	SELECT TOP 1 convert(datetime, LegEndTime, 103)
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndTime
	,LegStartLocation
	,convert(datetime, LegStartTime, 103)
	,LegEndLocation
	,convert(datetime, LegEndTime, 103)
	,convert(decimal(5,2), isnull(LegLength, 0))
	,VehicleFormation
	,CoupledUnits
	,convert(tinyint, UnitPosition)
	,convert(int, isnull(SequenceNumber, 0))
	FROM 
	dbo.GeniusInterfaceData gid1
	WHERE 
	GeniusInterfaceFileId = @fileId
	;
	IF @@ERROR <> 0
	ROLLBACK
	ELSE 
	COMMIT TRAN
	
	
	exec dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Loaded to Staging Table'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, 'Loaded to Staging Table';	
	
	end try
	begin catch
	    SELECT @logText = ERROR_MESSAGE();
	    exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch    
	
    return 0;
end

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_ImportDataToFleetSummaryStaging]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NSP_Genius_ImportDataToFleetSummaryStaging]
AS
BEGIN

	--------------------------------------------------------------------------------
	-- Ignore files for units not existing in Spectrum DB
	--------------------------------------------------------------------------------

	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - UnitNumber does not exist in Unit table'
	WHERE ProcessingStatus = 'Data loaded from file'
	AND	dbo.FN_GeniusGetUnitFromFilename(Name) NOT IN (SELECT UnitNumber FROM Unit)

	--------------------------------------------------------------------------------
	-- Ignore obsolete files
	--------------------------------------------------------------------------------
	;WITH CteFilesToProcess
	AS
	(
	SELECT 
	ID
	, TimeGenerated	= dbo.FN_GeniusGetTimestampFromFilename(Name)
	, Unit	= dbo.FN_GeniusGetUnitFromFilename(Name)
	, RowNumberPerUnit	= ROW_NUMBER() OVER (PARTITION BY dbo.FN_GeniusGetUnitFromFilename(Name) ORDER BY dbo.FN_GeniusGetTimestampFromFilename(Name) DESC)
	FROM dbo.GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	)
	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - Oboslete data for unit'
	WHERE ID IN (
	SELECT ID 
	FROM CteFilesToProcess
	WHERE RowNumberPerUnit <> 1
	)


	--------------------------------------------------------------------------------
	-- Validate and Copy to FleetSummaryStaging
	--------------------------------------------------------------------------------
	DECLARE @FileID int
	WHILE 1 = 1
	BEGIN
	SET @FileID = (
	SELECT MIN(ID)
	FROM GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	AND ID > ISNULL(@FileID, 0)
	)
	IF @FileID IS NULL BREAK
	-----------------------------------------------------------

	-- Truncate GeniusStaging table
	TRUNCATE TABLE dbo.GeniusStaging
	
	PRINT '--------------------------------------------------------------------------------'
	
	PRINT 'NSP_Genius_ValidateAndCopy FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ValidateAndCopy @FileID
	
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Loaded to Staging Table'
	BEGIN
	PRINT 'NSP_Genius_ImportDataFromStaging FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ImportDataFromStaging @FileID
	END
	
	-- If File status has not changed rais error
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Data loaded from file'
	BEGIN
	
	EXEC dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Error - Unknown error in NSP_Genius_ImportDataToFleetSummaryStaging'
	END
	END

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_Genius_WriteLog]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[NSP_GeniusImportProcessFile]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GeniusImportProcessFile
**	Description:	Updates GeniusStaging table with data from the file
**	Call frequency:	Every time new Genius file is received
**	Parameters:		@FileID - ID from LoadGeniusFile
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/07/06 13:41:13 $$
**	$$Revision: 1.2.2.1 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/Attic/update_spectrum_db_009.sql,v $$
******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GeniusImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET DATEFORMAT Mdy

	IF (SELECT COUNT(1) FROM LoadGeniusData WHERE LoadGeniusFileID = @FileID) > 0 
	BEGIN

		DECLARE @bstToGmtTimeOffset smallint
		SET @bstToGmtTimeOffset = - dbo.FN_GetGmtToBstTimeDiff(GETDATE())
	
		BEGIN TRAN
		
		TRUNCATE TABLE [GeniusStaging]

		INSERT INTO dbo.GeniusStaging
			([GeniusInterfaceFileID]
			,[DiagramNumber]
			,Headcode
			,[TrainStartLocation]
			,[TrainEndLocation]
			,[TrainStartTime]
			,[TrainEndTime]
			,[UnitNumber]
			,[UnitPosition])
		SELECT 
			@FileID																--LoadGeniusFileID
			, REPLACE(REPLACE(Diagram, CHAR(10), ''), CHAR(13), '')				--Diagram
			, LEFT(REPLACE(REPLACE(Headcode, CHAR(10), ''), CHAR(13), ''), 4)	--Headcode
			, RTRIM(REPLACE(REPLACE(LocationStart, CHAR(10), ''), CHAR(13), ''))--LocationStart
			, RTRIM(REPLACE(REPLACE(LocationEnd, CHAR(10), ''), CHAR(13), ''))	--LocationEnd
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeStart)					--TimeStart
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeEnd)					--TimeEnd
			, REPLACE(REPLACE(REPLACE(UnitNumber, CHAR(10), ''), CHAR(13), ''),'455','5') --UnitNumber
			, CASE 
				WHEN ISNUMERIC(UnitPosition) = 1
					THEN UnitPosition
				ELSE NULL
			END														--Position
		FROM LoadGeniusData 
		WHERE LoadGeniusFileID = @FileID
			AND Headcode IS NOT NULL
			AND Headcode <> ''
			
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK
			UPDATE LoadGeniusFile SET 
				Status = 'ERROR - Insert to LoadGeniusData failed'
			WHERE ID = @FileID
		END
		ELSE
			COMMIT
			
	END

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_GeniusUpdateFleetStatus]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_GeniusUpdateFleetStatus
**	Description:	Updates FleetStatus table with current Headcode details
**	Call frequency:	Every 60s
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/10/02 13:58:48 $$
**	$$Revision: 1.2.2.1 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/Attic/update_spectrum_db_013.sql,v $$
******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GeniusUpdateFleetStatus] 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @currentTimestamp datetime		= DATEADD(ms, - DATEPART(ms, GETDATE()), GETDATE()) --timestamp with ms = 000
	DECLARE @gmtToBstTimeOffset smallint	= 0		-- FCC stores channel data with timestamps including DLS -- dbo.FN_GetGmtToBstTimeDiff(GETUTCDATE())
	DECLARE @headcodeBeforeDeparture int	= 20	-- number of minutes before departure time that Headcode is displayed
	DECLARE @headcodeAfterArrival int		= 60	-- number of minutes after arrival time that Headcode is displayed (unless new headcode is on
	DECLARE @unitID int
	DECLARE @fleetFormationID int
	

	IF OBJECT_ID('tempdb.dbo.#FleetConfig') IS NOT NULL
		DROP TABLE #FleetConfig
		
	;WITH CteHeadcode
	AS
	(
		-- Current headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 1 AS Priority
		FROM FleetStatusStaging
		WHERE @currentTimestamp >= TrainStartTime 
			AND @currentTimestamp < TrainEndTime

		UNION ALL
		
		-- Following headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 2 AS Priority
		FROM FleetStatusStaging
		WHERE 
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN DATEADD(n, - @headcodeBeforeDeparture, TrainStartTime) AND TrainEndTime

		UNION ALL

		-- Finished headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 3 AS Priority
		FROM FleetStatusStaging
		WHERE
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN TrainStartTime AND DATEADD(n, @headcodeAfterArrival, TrainEndTime)
	)
	SELECT 
		ID AS UnitID	
		, UnitNumber
		, CASE
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1 ORDER BY TrainStartTime DESC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2 ORDER BY TrainStartTime ASC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3 ORDER BY TrainEndTime DESC)
		END HeadcodeID
	INTO #FleetConfig
	FROM Unit

	WHILE 1 = 1 
	BEGIN
		
		SET @unitID = (SELECT MIN(UnitID) FROM #FleetConfig WHERE UnitID > ISNULL(@unitID, 0))
		IF @unitID IS NULL BREAK
		------------------------------------------------------------------------------------------------------
		
		SET @fleetFormationID = NULL
		
		BEGIN TRY
		
			--Insert formation if does not exist
			SET @fleetFormationID = (
				SELECT ID
				FROM dbo.FleetFormation ff
				WHERE EXISTS 
					(SELECT  1
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
						AND TrainStartTime = ff.ValidFrom
						AND TrainEndTime = ff.ValidTo
						AND CoupledUnits = ff.UnitNumberList
					) 
				)
		
			IF @fleetFormationID IS NULL
			BEGIN
			
				INSERT INTO dbo.FleetFormation(
					UnitNumberList
					, UnitIDList
					, ValidFrom
					, ValidTo)
				SELECT 
					CoupledUnits
					, ''
					, TrainStartTime
					, TrainEndTime
				FROM #FleetConfig fc
				LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID 
				WHERE  fc.UnitID = @unitID
					AND TrainStartTime IS NOT NULL
					AND TrainEndTime IS NOT NULL
					AND CoupledUnits IS NOT NULL

				IF @@ROWCOUNT > 0
					SET @fleetFormationID = (SELECT MAX(ID) FROM FleetFormation)
				
			END

			-- Check if unit status to be updated
			IF EXISTS (
				SELECT 1
				FROM dbo.FleetStatus
				LEFT JOIN (
					SELECT 
						UnitID				= fc.UnitID
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				) AS CteFleetConfig ON FleetStatus.UnitID = CteFleetConfig.UnitID
			WHERE ISNULL(FleetStatus.Diagram, 'NULL') <> ISNULL(HeadcodeDiagram, 'NULL')
				AND FleetStatus.UnitID = @unitID
				AND FleetStatus.ValidTo IS NULL	
)
			
			BEGIN

				-- 1. Close Headcodes that are completed
				UPDATE FleetStatus SET
					ValidTo = @currentTimestamp
				WHERE UnitID = @unitID
					AND ValidTo IS NULL

				-- 2. Add new headcodes
				;WITH CteFleetConfig
				AS
				(
					SELECT 
						UnitID				= fc.UnitID
						, UnitNumber		= fc.UnitNumber
						, Diagram			= DiagramNumber
						, Headcode			= LEFT(Headcode, 4)
						, LocationStart		= TrainStartLocationTiploc
						, LocationEnd		= TrainEndLocationTiploc
						, LocationIdStart	= TrainStartLocationId
						, LocationIdEnd		= TrainEndLocationId
						, TimeStart			= TrainStartTime
						, TimeEnd			= TrainEndTime
						, Position			= UnitPosition
						, CoupledUnits		= REPLACE(CoupledUnits, ';',',')
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				)
				INSERT INTO dbo.FleetStatus(
					UnitID
					, ValidFrom
					, ValidTo
					, Setcode
					, Diagram
					, Headcode
					, IsValid
					, Loading
					, ConfigDate
					, UnitList
					, LocationIDHeadcodeStart
					, LocationIDHeadcodeEnd
					, UnitPosition
					, FleetFormationID
				)
				SELECT
					UnitID
					, ValidFrom		= @currentTimestamp
					, ValidTo		= NULL
					, Setcode		= NULL
					, Diagram		= CteFleetConfig.HeadcodeDiagram
					, Headcode		= CteFleetConfig.Headcode
					, IsValid		= 1
					, Loading		= NULL
					, ConfigDate	= SYSDATETIME()
					, UnitList		= CoupledUnits
					, LocationIDHeadcodeStart	= LocationIdStart
					, LocationIDHeadcodeEnd		= LocationIdEnd
					, UnitPosition				= ISNULL(Position, 1)
					, FleetFormationID			= @fleetFormationID
				FROM CteFleetConfig
				WHERE UnitID = @unitID
				
			END
			
		END TRY
		BEGIN CATCH
			PRINT 'Error: Unknown error when updating headcode'
			PRINT 'UnitID           ' + CAST(@unitID AS varchar(10))
			PRINT 'CurrentTimestamp ' + CONVERT(char(20), @currentTimestamp, 121)
		END CATCH


	END -- WHILE

END -- stored procedure

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultChannelData]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
**	Name:			NSP_GetFaultChannelData
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetFaultChannelData]
(
	@FaultID int
	, @UnitID int
)
AS
BEGIN

		SELECT
			[ID]
			,[FaultID]
			,[FaultVehicle]
			,[UpdateRecord]
			,[VehicleID]
			,[RecordInsert]
			,[TimeStamp]
			,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]

		FROM dbo.FaultChannelValue cv
		LEFT JOIN dbo.FaultChannelMeasurement cvm ON cv.FaultID = cv.FaultID
		WHERE FaultID = @FaultID
			AND UnitID = @UnitID

END



GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultChannelValue]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultChannelValue
**	Description:	Returns data to fault engine
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultChannelValue]
	@DatetimeStart datetime
	, @DatetimeEnd datetime
	, @UnitID int = NULL
	, @UpdateRecord bit = NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		cv.UnitID
		, u.UnitNumber
		--, v.VehicleTypeID
		, Headcode = CAST(NULL AS varchar(7))
		, u.UnitNumber
		, u.UnitType
		, ServiceStatus	= dbo.FN_GetSetState(
											fs.Setcode
											, Location.LocationCode
											, u.IsValid
											, fs.Diagram
										)
		, [TimeStamp]			= CONVERT(datetime, cv.TimeStamp)
		, Location.ID AS LocationID
		, Location.Tiploc AS Tiploc
		, Location.LocationName AS LocationName
		,
		-- Channel Data	
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM ChannelValue cv
	INNER JOIN Unit u ON cv.UnitID = u.ID
	LEFT JOIN FleetStatus fs ON
		cv.TimeStamp > ValidFrom 
		AND cv.TimeStamp <= ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
		AND cv.UnitID = fs.UnitID
	LEFT JOIN Location ON Location.ID = (
										SELECT TOP 1 LocationID 
										FROM LocationArea 
										WHERE col1 BETWEEN MinLatitude AND MaxLatitude
											AND col2 BETWEEN MinLongitude AND MaxLongitude
										ORDER BY Priority
									)
	WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
		AND (cv.UnitID = @UnitID OR @UnitID IS NULL)
		AND (cv.UpdateRecord = @UpdateRecord OR @UpdateRecord IS NULL)
	ORDER BY TimeStamp

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultChannelValueByID]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
	@ID bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM ChannelValue), 0) - 1;
	ELSE
		SET @ID = (
		SELECT TOP 1 (ChannelValue.ID - 1) 
		FROM ChannelValue WITH(NOLOCK)
		WHERE ChannelValue.ID > @ID
		ORDER BY ChannelValue.ID ASC
		)
		
	DECLARE @dataSetSize int = 5000
	
	SELECT 
		cv.ID
		, Headcode		= fs.Headcode
		, UnitID		= u.ID 
		, UnitNumber	= u.UnitNumber
		, UnitType		= u.UnitType
		, FleetCode		= '15x' --v.FleetCode
		
		--  Do NOT use FN_GetSetState function Setcode is always NULL
		, ServiceStatus	= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, TimeStamp			= CONVERT(datetime, cv.TimeStamp)
		, LocationID	= l.ID
		, LocationCode	= l.LocationCode
		, LocationName	= l.LocationName 
		,
		-- Channel Data	
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]

		
	FROM ChannelValue cv WITH(INDEX(PK_ChannelValue))
	INNER JOIN Unit u ON cv.UnitID = u.ID
	LEFT JOIN FleetStatus fs ON
		cv.TimeStamp > ValidFrom AND (cv.Timestamp <= ValidTo OR ValidTo IS NULL)
--		AND DATEADD(d, -1, cv.TimeStamp) <= ISNULL(ValidTo, SYSDATETIME()) -- adding 1 day in case live data is inserted with 
		AND cv.UnitID = fs.UnitID
	LEFT JOIN Location l ON l.ID = (
									SELECT TOP 1 LocationID 
									FROM LocationArea 
									WHERE col1 BETWEEN MinLatitude AND MaxLatitude
										AND col2 BETWEEN MinLongitude AND MaxLongitude
									ORDER BY Priority
								)
	WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
	ORDER BY ID

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultCount]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultCount
**	Description:	Returns fault count after specific date
**	Call frequency:	Every 5s 
**	Parameters:		@timestamp
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetFaultCount](
	@timestamp datetime
)
AS
BEGIN

	SELECT 
		COUNT(*) AS FaultCount
	FROM VW_IX_Fault (NOEXPAND)
	WHERE CreateTime >= @timestamp
	    AND ReportingOnly = 0

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**  Name:           NSP_GetFaultDetail
**  Description:    Returns details of the Fault
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN
SELECT
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , RecoveryStatus
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        WHERE f.ID = @FaultID

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultFormation]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_GetFaultFormation
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetFaultFormation]
(
	@FaultID int
)
AS
BEGIN
	
	DECLARE @primaryUnitID int
	DECLARE @secondaryUnitID int
	DECLARE @tertiaryUnitID int
	DECLARE @faultUnitID int
	
	SELECT 
		@primaryUnitID		= PrimaryUnitID
		, @secondaryUnitID	= SecondaryUnitID
		, @tertiaryUnitID	= TertiaryUnitID 
		, @faultUnitID		= FaultUnitID
	FROM Fault
	WHERE ID = @FaultID
	
	;WITH CteUnit AS
	(
		SELECT 
			UnitID		= @primaryUnitID
			, UnitOrder	= 1
		UNION 
		SELECT 
			UnitID		= @secondaryUnitID
			, UnitOrder	= 2
		UNION 
		SELECT 
			UnitID		= @tertiaryUnitID
			, UnitOrder	= 3
		
	)
	SELECT 
		UnitID			= Unit.ID 
		, UnitNumber
		, UnitOrder
		, IsFaultUnit	= CASE Unit.ID
			WHEN @faultUnitID THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END
	FROM CteUnit
	INNER JOIN Unit ON Unit.ID = CteUnit.UnitID
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultLive]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultLive
**	Description:	Returns list of all Current Faults
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
AS

	SET NOCOUNT ON;
	
    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC


GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultLiveCount]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultLiveCount
**	Description:	Returns live fault count
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetFaultLiveCount]
AS
BEGIN

	SELECT 
		COUNT(*) AS FaultCount
	FROM VW_IX_FaultLive (NOEXPAND)
	WHERE ReportingOnly = 0

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultLiveUnit]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultLiveUnit
**	Description:	Returns list of all Current Faults for a unit
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
	@UnitID int
)
AS

	SET NOCOUNT ON;

	SELECT
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultNotLive]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLive
**	Description:	Returns list of N recent Faults
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
	@N int
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
)
AS
	SET NOCOUNT ON;
	
 	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
		AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
		AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultNotLiveUnit]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLiveUnit
**	Description:	Returns list of N recent Faults for set
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**					@UnitID
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/


CREATE PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
	@N int
	, @UnitID int
)
AS
	SET NOCOUNT ON;

	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC


GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultRetractedFlag]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
**	Name:			NSP_GetFaultRetractedFlag
**	Description:	Returns 1 if there was a fault retracted since the timestamp
**					passed as parameter, otherwise 0
**	Call frequency:	Every 5s for each user
**	Parameters:		@timestamp
**	Return values:	0 if successful, else an error is raised
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetFaultRetractedFlag](
	@timestamp datetime
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(
		SELECT 1
		FROM VW_IX_Fault (NOEXPAND)
		WHERE (EndTime >= @timestamp)
	)
	    SELECT 1 AS Flag
	ELSE
		SELECT 0 AS Flag
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultSince]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultSince
**	Description:	Returns list of all Faults created after the specified time argument
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultSince]
	@since datetime
AS
BEGIN

	SET NOCOUNT ON;
	
    
	SET NOCOUNT ON;
	
    SELECT 
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
	FROM VW_IX_Fault f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    WHERE CreateTime > @since
	    AND ReportingOnly = 0
    ORDER BY CreateTime DESC

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetFaultTmsData]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_GetFaultTmsData
**	Description:	Returns TMS data for the FaultID
**	Call frequency:	Once for each fault detail request
**	Parameters:		
**	Return values:	0 if successful, else an error is raised:
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_GetFaultTmsData]
	@FaultID int
AS
BEGIN
		
	SELECT
		tf.VehicleNumber
		, tf.FaultDateTime
		, tf.CarNo
		, tf.FaultNo
		, tfl.Description
		, tfl.SwRef
		, tfc.Value	AS CategoryName
		, tff.Value	AS FunctionName
		, cablookup.Value AS Cab
		, mcslookup.Value AS MCS
		, dirlookup.Value AS Direction
		, vltlookup.Value AS Voltage
		, spdlookup.Value AS SpeedSet
		, bctlookup.Value AS BrkCont
		, ebklookup.Value AS EmmerBrk
		, stdlookup.Value AS Stabled
		, mlplookup.Value AS MLP
		, bprlookup.Value AS BrkPr
		, batlookup.Value AS Battery
		, ldslookup.Value AS Loadshed
		, tshlookup.Value AS TracShoreSup
		, ashlookup.Value AS AuxShoreSup
		, bmclookup.Value AS CarBMCoupled
		, bjclookup.Value AS CarBJCoupled
		, tmclookup.Value AS TMCC1
		, tf.Speed
	FROM dbo.TmsFault tf
	LEFT JOIN dbo.TmsFaultLookup tfl ON tf.FaultNo = tfl.FaultNo
	LEFT JOIN dbo.TmsLookup AS tfc ON tfl.TmsCategoryID = tfc.ValueID AND tfc.Type = 'tmsFaultCategory'
	LEFT JOIN dbo.TmsLookup AS tff ON tfl.TmsFunctionID =  tff.ValueID AND tff.Type = 'tmsFaultFunction'
	LEFT JOIN dbo.TmsLookup AS cablookup ON tf.CapId = cablookup.ValueId AND cablookup.Type = 'cab'
	LEFT JOIN dbo.TmsLookup AS mcslookup ON tf.McsId = mcslookup.ValueId AND mcslookup.Type = 'mcs'
	LEFT JOIN dbo.TmsLookup AS dirlookup ON tf.DirectionId = dirlookup.ValueId AND dirlookup.Type = 'direction'
	LEFT JOIN dbo.TmsLookup AS vltlookup ON tf.VoltageId = vltlookup.ValueId AND vltlookup.Type = 'voltage'
	LEFT JOIN dbo.TmsLookup AS spdlookup ON tf.SpeedSetId = spdlookup.ValueId AND spdlookup.Type = 'speedSet'
	LEFT JOIN dbo.TmsLookup AS bctlookup ON tf.BrakeContId = bctlookup.ValueId AND bctlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS ebklookup ON tf.EmerBrakeId = ebklookup.ValueId AND ebklookup.Type = 'emergencyBrake'
	LEFT JOIN dbo.TmsLookup AS stdlookup ON tf.StabledId = stdlookup.ValueId AND stdlookup.Type = 'stabled'
	LEFT JOIN dbo.TmsLookup AS mlplookup ON tf.MlpId = mlplookup.ValueId AND mlplookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS bprlookup ON tf.BrakePrId = bprlookup.ValueId AND bprlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS batlookup ON tf.BatteryId = batlookup.ValueId AND batlookup.Type = 'battery'
	LEFT JOIN dbo.TmsLookup AS ldslookup ON tf.LoadshedId = ldslookup.ValueId AND ldslookup.Type = 'loadshed'
	LEFT JOIN dbo.TmsLookup AS tshlookup ON tf.TracShoreSupId = tshlookup.ValueId AND tshlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS ashlookup ON tf.AuxShoreSupId = ashlookup.ValueId AND ashlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS bmclookup ON tf.CarBmCoupledId = bmclookup.ValueId AND bmclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS bjclookup ON tf.CarBjCoupledId = bjclookup.ValueId AND bjclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS tmclookup ON tf.Tmcc1Id = tmclookup.ValueId AND tmclookup.Type = 'tmcc1'
	WHERE FaultID = @FaultID
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetLastChannelDataForVehicle]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**  Name:           NSP_GetLastChannelDataForVehicle
**  Description:    Returns latest recod for the vehicles for VehicleID in
**                  paramter. Stored procedure searches for latest record in
**                  last 24h before timestamp passed as paramter 
**  Call frequency: Every 5s from Cabview screen 
**  Parameters:     @DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**                  to return last live record for unit
**                  @VehicleID int 
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetLastChannelDataForVehicle]
    @DateTimeStart datetime
    , @VehicleID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, GETDATE())
                
    ;WITH CteLatestRecords 
    AS
    (
		SELECT 
			VehicleID	= Vehicle.ID
			, UnitID	= Vehicle.UnitID
			, LastTimeStamp = (
				SELECT TOP 1 TimeStamp
				FROM ChannelValue
				WHERE  TimeStamp BETWEEN DATEADD(d, -7, @DateTimeStart) AND @DateTimeStart
					AND ChannelValue.UnitID = Vehicle.UnitID 
				ORDER BY TimeStamp DESC
			)
		FROM VW_Vehicle AS Vehicle
		WHERE Vehicle.ID = @VehicleID
    )   

    SELECT 
        UnitNumber
        , Headcode	= CONVERT(varchar(10), NULL)
		, Diagram 	= CONVERT(varchar(10), NULL)
        , Vehicle.ID AS VehicleID
        , Vehicle.Type AS VehicleType
        , Vehicle.VehicleTypeID AS VehicleTypeID
        , Vehicle.VehicleNumber AS VehicleNumber
        , (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE col3 BETWEEN MinLatitude AND MaxLatitude
                AND col4 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) AS Location
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp      

		,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
    FROM VW_Vehicle Vehicle 
    INNER JOIN CteLatestRecords     ON Vehicle.UnitID = CteLatestRecords.UnitID
    LEFT JOIN ChannelValue cv       ON Vehicle.UnitID = cv.UnitID  AND CteLatestRecords.LastTimeStamp = cv.TimeStamp

            
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_GetStockSiblings]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_GetStockSiblings
**	Description:	Given a Vehicle ID, returns Vehicle IDs with the same
**					parent (Unit)
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE  PROCEDURE [dbo].[NSP_GetStockSiblings]
(
	@VehicleID int
)
AS
BEGIN
	SELECT 
		ID			= Vehicle.ID 
		, StockNumber		= VehicleNumber	
		, StockOrder		= VehicleOrder
	FROM Vehicle
	WHERE UnitID = (
		SELECT UnitID
		FROM Vehicle
		WHERE ID = @VehicleID
	)
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_HistoricChannelData]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NSP_HistoricChannelData]
	@DateTimeEnd datetime
	, @Interval int -- number of seconds
	, @VehicleID int
	, @SelectColList varchar(4000)
	, @N int = NULL --record number
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql varchar(max)

	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)
	
	DECLARE @vehicleTypeID int = (SELECT VehicleTypeID FROM VW_Vehicle WHERE ID = @VehicleID)
	
	SET @sql = '
SELECT TOP (' + CAST(ISNULL(@N, 30000) AS varchar(10)) +  ')
	TimeStamp = CAST(Timestamp AS datetime)
	, ' + ISNULL(CAST(@vehicleTypeID AS varchar(10)), 'NULL') +  ' AS VehicleTypeID
	, ' + @SelectColList + '
FROM ChannelValue
WHERE 
	VehicleID = ' + CAST(@VehicleID AS varchar(10)) + '
	AND Timestamp BETWEEN ''' + CONVERT(varchar(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(varchar(23), @DateTimeEnd, 121)  + '''
ORDER BY 1 ASC'

	EXEC (@sql)
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_HistoricChannelValue]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_HistoricChannelValue
**	Description:	Returns latest record for each of the vehicles for UnitID
**					in parameter. Stored procedure searches for last record in 
**					last 24h before timestamp passed as parameter 
**	Call frequency:	Every 5s 
**	Parameters:		@DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**						to return last live record for unit
**					@UnitID int 
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_HistoricChannelValue]
	@DateTimeStart datetime
	, @UnitID int
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    IF @UnitID IS NULL
    BEGIN
        PRINT 'Unit Number not in a database'
        RETURN -1
    END

    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, GETDATE())

    -- Get totals for Vehicle based faults
    DECLARE @currentUnitFault TABLE(
        UnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
    )

    INSERT INTO @currentUnitFault
    SELECT
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
    FROM VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID IS NOT NULL
        AND Category <> 'Test'
        AND ReportingOnly = 0
    GROUP BY FaultUnitID

    -- Get Headcode
    DECLARE @diagram varchar(50)
    DECLARE @headcode varchar(50)

    SELECT
        @diagram    = Diagram
        , @headcode = Headcode
    FROM FleetStatus
    WHERE UnitID = @UnitID
        AND @DateTimeStart BETWEEN ValidFrom AND ISNULL(ValidTo, @DateTimeStart)

    ;WITH CteLatestRecords
    AS
    (
        SELECT
            VehicleID   = Vehicle.ID
            , UnitID    = Vehicle.UnitID
            , LastTimeStamp = (
                SELECT TOP 1 TimeStamp
                FROM ChannelValue
                WHERE  TimeStamp BETWEEN DATEADD(d, -7, @DateTimeStart) AND @DateTimeStart
                    AND ChannelValue.UnitID = Vehicle.UnitID
                ORDER BY TimeStamp DESC
            )
        FROM VW_Vehicle AS Vehicle
        WHERE UnitID = @UnitID
    )
    SELECT
        Vehicle.UnitNumber
        , Headcode          = @headcode
        , Diagram           = @diagram
        , Vehicle.UnitID
        , Vehicle.ID AS VehicleID
        , Vehicle.Type AS VehicleType
        , Vehicle.VehicleTypeID AS VehicleTypeID
        , Vehicle.VehicleNumber AS VehicleNumber
        , FaultNumber   = ISNULL(vf.FaultNumber,0)
        , WarningNumber = ISNULL(vf.WarningNumber,0)
        , l.LocationCode AS Location
        , vehicle.FleetCode
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        , TimeStamp = CAST(cv.[TimeStamp] AS datetime)
		,
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]


    FROM VW_Vehicle Vehicle
    INNER JOIN CteLatestRecords vlc ON Vehicle.UnitID = vlc.UnitID
    LEFT JOIN ChannelValue cv WITH (NOLOCK) ON vlc.UnitID = cv.UnitID  AND vlc.LastTimeStamp = cv.TimeStamp
    LEFT JOIN @currentUnitFault vf ON Vehicle.UnitID = vf.UnitID
    LEFT JOIN VW_FleetStatus fs ON Vehicle.ID = fs.VehicleID
    LEFT JOIN Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM LocationArea 
        WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
            AND Col2 BETWEEN MinLongitude AND MaxLongitude
        ORDER BY Priority )

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_FleetLocation]
AS
/******************************************************************************
**  Name:           NSP_FleetLocation
**  Description:    Returns live channel data for the application
**  Call frequency: Every 5s
**  Parameters:     None
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/
BEGIN
    SET NOCOUNT ON;
    
    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        UnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
    )
    
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
    FROM VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID IS NOT NULL
        AND Category <> 'Test'
    GROUP BY FaultUnitID

    -- return data
    SELECT 
        fs.UnitID
        , fs.UnitNumber
        , fs.UnitType
        , fs.FleetFormationID
        , fs.SetCode
        , fs.UnitPosition
        , fs.Headcode
        , fs.Diagram
        , fs.Loading
        , fs.VehicleID
        , fs.VehicleNumber
        , fs.VehicleType
        , fs.VehicleTypeID
        , fs.VehiclePosition
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE 
                (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                    AND Col2 BETWEEN MinLongitude AND MaxLongitude)
            ORDER BY Priority
        )
        , FaultNumber           = ISNULL(vf.FaultNumber,0)
        , WarningNumber     = ISNULL(vf.WarningNumber,0)
        , VehicleList
        , ServiceStatus         = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                        SELECT TOP 1 LocationID
                        FROM LocationArea
                        WHERE 
                            (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                                AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                        ORDER BY Priority
                    )
                    THEN 'Ready for Service'
                WHEN fs.Headcode IS NOT NULL
                    THEN 'In Service'
                ELSE 'Out of Service'
            END
        ,cv.UpdateRecord
        ,cv.RecordInsert
        -- temporary solution for datetime and JTDS driver 
        ,CASE WHEN isnull(CAST(cve.[TimeStamp] AS datetime),'01-Jan-1970') > isnull(CAST(cv.[TimeStamp] AS datetime),'01-Jan-1970') THEN CAST(cve.[TimeStamp] AS datetime) ELSE CAST(cv.[TimeStamp] AS datetime) END AS 'TimeStamp'
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
        , ServiceStatusEKE      = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                    SELECT TOP 1 LocationID
                    FROM LocationArea
                    WHERE 
                        (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                            AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                    ORDER BY Priority
                )
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        ,UpdateRecordEKE = cve.[UpdateRecord]
        ,RecordInsertEKE = cve.[RecordInsert]
    
    FROM VW_FleetStatus fs WITH (NOLOCK)
    INNER JOIN VW_VehicleLastChannelValueTimestamp vlc WITH (NOLOCK) ON vlc.VehicleID = fs.VehicleID
    LEFT JOIN ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.VehicleID = cv.VehicleID
    LEFT JOIN ChannelValueEKE cve WITH (NOLOCK) ON cve.TimeStamp = vlc.LastEKETimeStamp AND vlc.VehicleID = cve.VehicleID
    LEFT JOIN @currentVehicleFault vf ON fs.UnitID = vf.UnitID
END
GO
/****** Object:  StoredProcedure [dbo].[NSP_HistoricFleetSummary]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_HistoricFleetSummary
**	Description:	Returns historical Fleet level data 
**	Call frequency:	Every 5s from FleetLocation screen
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_HistoricFleetSummary](
	@DateTimeEnd datetime
)
AS
BEGIN

	SET NOCOUNT ON;
		DECLARE @interval int = 3600 -- value in seconds 
	
	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @interval, @DateTimeEnd);

	-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
	DECLARE @LatestChannelValue TABLE(
		VehicleID int
		, LastTimeStamp datetime2(3)
	)
	
	INSERT @LatestChannelValue(
		VehicleID
		, LastTimeStamp
	)
	SELECT 
		VehicleID		= VW_Vehicle.ID
		, LastTimeStamp = (
			SELECT TOP 1 TimeStamp
			FROM ChannelValue
			WHERE 
				Timestamp >= @DateTimeStart
				AND Timestamp <= @DateTimeEnd
				AND UnitID =	VW_Vehicle.UnitID 
			ORDER BY TimeStamp DESC
		)
	FROM VW_Vehicle	

	SELECT 
		v.UnitID
		, v.UnitNumber
		, v.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, Diagram				= fs.Diagram
		, Headcode				= fs.Headcode
		, VehicleID				= v.ID
		, VehicleNumber			= v.VehicleNumber
		, VehiclePosition		= v.VehicleOrder
		, VehicleType			= v.Type
		, Location				= (
			SELECT TOP 1 
				LocationCode --CRS
			FROM Location
			INNER JOIN LocationArea ON LocationID = Location.ID
			WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
				AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
		, ServiceStatus	= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,TimeStamp				= CAST(cv.[TimeStamp] AS datetime)
        -- gps location
        , cv.[Col1]
        , cv.[Col2]
	FROM VW_Vehicle v 

	INNER JOIN @LatestChannelValue lcv 
		ON v.ID = lcv.VehicleID

    INNER JOIN ChannelValue cv WITH (NOLOCK) 
        ON cv.TimeStamp = lcv.LastTimeStamp 
        AND cv.UnitID = v.UnitID

	LEFT JOIN FleetStatusHistory fs ON
		v.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 

	LEFT JOIN Location l ON l.ID = (
		SELECT TOP 1 LocationID 
		FROM LocationArea 
		WHERE col3 BETWEEN MinLatitude AND MaxLatitude
			AND col4 BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertChannelLimitAnalog]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
**	Name:			NSP_InsertChannelLimitAnalog
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/
CREATE PROCEDURE [dbo].[NSP_InsertChannelLimitAnalog]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @ErrorLowMin varchar(10)
	, @ErrorLowMax varchar(10)
	, @WarningLowMin varchar(10)
	, @WarningLowMax varchar(10)
	, @NormalMin varchar(10)
	, @NormalMax varchar(10)
	, @WarningHighMin varchar(10)
	, @WarningHighMax varchar(10)
	, @ErrorHighMin varchar(10)
	, @ErrorHighMax varchar(10)
	, @InvalidLowMax varchar(10) = NULL
	, @InvalidHighMin varchar(10) = NULL
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT * FROM Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM Channel WHERE ID = @ChannelID)


--	PRINT '** ' + CAST(@ChannelID AS varchar(10)) + ' - ' + ISNULL(@ChannelName, 'NULL') + ' details:'

	-- ERROR LOW
	IF ISNUMERIC(@ErrorLowMin) = 1 OR ISNUMERIC(@ErrorLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7
			, @ChannelName + ' - FAULT LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMin) = 1 THEN @ErrorLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMax) = 1 THEN @ErrorLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Warning LOW
	IF ISNUMERIC(@WarningLowMin) = 1 OR ISNUMERIC(@WarningLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningLowMin) = 1 THEN @WarningLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningLowMax) = 1 THEN @WarningLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Normal
	IF ISNUMERIC(@NormalMin) = 1 OR ISNUMERIC(@NormalMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@NormalMin) = 1 THEN @NormalMin
			END 
			, CASE	
				WHEN ISNUMERIC(@NormalMax) = 1 THEN @NormalMax
			END 
			, 1
			, 1
			
	END
	
	-- Warning HIGH
	IF ISNUMERIC(@WarningHighMin) = 1 OR ISNUMERIC(@WarningHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningHighMin) = 1 THEN @WarningHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningHighMax) = 1 THEN @WarningHighMax
			END 
			, 0
			, 1
			
	END

	
	-- ERROR HIGH
	IF ISNUMERIC(@ErrorHighMin) = 1 OR ISNUMERIC(@ErrorHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMin) = 1 THEN @ErrorHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMax) = 1 THEN @ErrorHighMax
			END 
			, 0
			, 1
			
	END


	-- INVALID High
	IF ISNUMERIC(@InvalidHighMin) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@InvalidHighMin) = 1 THEN @InvalidHighMin
			END 
			, NULL
			, 0
			, 0
			
	END
	
	-- INVALID Low
	IF ISNUMERIC(@InvalidLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, NULL
			, CASE	
				WHEN ISNUMERIC(@InvalidLowMax) = 1 THEN @InvalidLowMax
			END 
			, 0
			, 0
			
	END

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertChannelLimitDigital]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_InsertChannelLimitDigital
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_InsertChannelLimitDigital]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @Red varchar(1)
	, @Orange varchar(1)
	, @Green varchar(1)
	, @Blue varchar(1)
	, @Yellow varchar(1)
	, @Grey varchar(1)
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT * FROM Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM Channel WHERE ID = @ChannelID)
	
	-- @Red - FAULT
	IF ISNUMERIC(@Red) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Red, @Red, 1, 1
			
	END

	-- @Orange - WARNING
	IF ISNUMERIC(@Orange) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Orange, @Orange, 1, 1
			
	END
	
	-- @Green - NORMAL
	IF ISNUMERIC(@Green) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1 --Fault
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Green, @Green, 1, 1
			
	END

	-- @Blue -- OFF
	IF ISNUMERIC(@Blue) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 3
			, @ChannelName + ' - OFF' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Blue, @Blue, 1, 1
			
	END

	-- @Yellow -- ON
	IF ISNUMERIC(@Yellow) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 2
			, @ChannelName + ' - ON' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Yellow, @Yellow, 1, 1
			
	END
		
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertFault]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_InsertFault
**	Description:	Creates a fault. Inserts data into Fault and 
**					FaultChannelValue tables
**	Call frequency:	Called by fault engine
**	Parameters:		@FaultCode varchar(100)
**					@TimeCreate datetime
**					@TimeEnd datetime = NULL
**					@IsDelayed bit = 0
**	Return values:	ID of Fault inserted, else
**					-1	Duplicate Fault
**					-2	Error when inserting into Fault table
**					-3	There is a Live fault overlapping with the fault
**					-4	FaultCode does not exist
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
	DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)

    IF (SELECT COUNT(*) FROM FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
			@faultMetaID = ID 
			, @faultMetaRecovery = RecoveryProcessPath
        FROM FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
		, @faultLocationID = l.ID
        FROM ChannelValue
		LEFT JOIN Location l ON l.ID = (
			SELECT TOP 1 LocationID 
			FROM LocationArea 
			WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
				AND Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
            
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO [Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[IsDelayed]
			,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,ISNULL(@IsDelayed, 0)
            ,RecoveryStatus = CASE
				WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
				ELSE NULL
			END

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
	
	--insert Channel data
	INSERT INTO [FaultChannelValue]
	(
	        [ID]
        ,[FaultID]
        ,[FaultUnit]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	)
	SELECT
		[ID]
		,@faultID
		,CASE UnitID
			WHEN @FaultUnitID THEN 1
			ELSE 0
		END -- [FaultVehicle]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		  ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM ChannelValue 
			WHERE UnitID = (
				SELECT UnitID
				FROM Vehicle
				WHERE ID = @FaultUnitID
				)			
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate

	-- returning faultID
	RETURN @faultID
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertFaultCategory]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[NSP_InsertFaultCategory](
	@Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	
	DECLARE @ID int = ISNULL((
		SELECT MAX(ID) + 1
		FROM FaultCategory
		
	), 1)
	
	IF @ID IS NULL
		RAISERROR('Error when looking for next ID', 13, 1); 
		
	BEGIN TRAN
	BEGIN TRY 
		
		SET IDENTITY_INSERT dbo.FaultCategory ON;
		
		INSERT dbo.FaultCategory(
			ID
			, Category
			, ReportingOnly
		)
		SELECT 
			@ID
			, @Category
			, @ReportingOnly;

		SET IDENTITY_INSERT dbo.FaultCategory OFF;

		COMMIT TRAN

		SELECT @ID AS InsertedID
			
	END TRY
	BEGIN CATCH
		
		EXEC NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
		
END



GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertFaultMeta]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[NSP_InsertFaultMeta](
    @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @Url varchar(500)
    , @CategoryID int
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
BEGIN

    SET NOCOUNT ON;
    
    INSERT dbo.FaultMeta(
        FaultCode
        , Description
        , Summary
        , Url
        , CategoryID
        , RecoveryProcessPath
        , FaultTypeID
        , GroupID
    )
    SELECT 
         FaultCode      = @FaultCode
        , Description   = @Description 
        , Summary       = @Summary
        , Url           = @Url
        , CategoryID    = @CategoryID
        , RecoveryProcessPath   = @RecoveryProcessPath
        , FaultTypeID   = @FaultTypeID
        , GroupID       = @GroupID
        
    SELECT SCOPE_IDENTITY() AS InsertedID

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertLocation]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_InsertLocation]
	@Tiploc varchar(20)
	, @LocationName varchar(100)
	, @LocationCode varchar(10)
	, @Lat decimal(9,6) = NULL
	, @Lng decimal(9,6) = NULL
	, @IsDepot bit = NULL
	, @IsStation bit = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM dbo.Location WHERE Tiploc = @Tiploc)
		UPDATE dbo.Location SET
			[LocationCode] = @LocationCode
			,[LocationName] = @LocationName
			,[Tiploc] = @Tiploc
			,[Lat] = @Lat
			,[Lng] = @Lng
			,[Type] = CASE
				WHEN @IsDepot = 1 THEN 'D'
				WHEN @IsStation = 1 THEN 'S'
				ELSE 'T'
			END
		WHERE Tiploc = @Tiploc
		
	ELSE
	INSERT INTO [dbo].[Location]
		([LocationCode]
		,[LocationName]
		,[Tiploc]
		,[Lat]
		,[Lng]
		,[Type])
	SELECT
		@LocationCode
		,@LocationName
		,@Tiploc
		,@Lat
		,@Lng
		,CASE
			WHEN @IsDepot = 1 THEN 'D'
			WHEN @IsStation = 1 THEN 'S'
			ELSE 'T'
		END 

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertLocationArea]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_InsertLocationArea]
(
	@Tiploc varchar(20)
	, @MinLat decimal(9,6)
	, @MaxLat decimal(9,6)
	, @MinLng decimal(9,6)
	, @MaxLng decimal(9,6)
	, @Priority int
	, @Type char(1)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[LocationArea]
		([LocationID]
		,[MinLatitude]
		,[MaxLatitude]
		,[MinLongitude]
		,[MaxLongitude]
		,[Priority]
		,[Type])
	SELECT
		(SELECT ID FROM Location WHERE Tiploc = @Tiploc)
		, @MinLat
		, @MaxLat
		, @MinLng
		, @MaxLng
		, @Priority
		, @Type

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_InsertTmsLog]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_InsertTmsLog
**	Description:	Inserts TMS Log record read by interface
**	Call frequency:	Once for every line in imported TMS Log
**	Parameters:		
**	Return values:	0 if successful, else an error is raised:
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/
CREATE PROCEDURE [dbo].[NSP_InsertTmsLog]
	@VehicleNo varchar(10)
	, @FaultDate date
	, @FaultTime time
	, @CarNo varchar(10)
	, @FaultNoMostSignificantByte int
	, @FaultNoLessSignificantByte int
	, @TsnCplMostSignificantByte int
	, @TsnCplLessSignificantByte int
	, @CabByte int
	, @McsByte int
	, @DirectionByte int
	, @Voltage bit
	, @SpeedSet bit
	, @BrakeCont bit
	, @EmerBrakeIsolated bit
	, @EmerBrakeState bit
	, @Stabled bit
	, @Mlp bit
	, @BrakePr bit
	, @Battery bit
	, @Loadshed bit
	, @TractShoreSup bit
	, @AuxShoreSup bit
	, @CarBmCoupled bit
	, @CarBjCoupled bit
	, @Tmcc1 bit
	, @Speed int
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @vehicleNumber varchar(10) = REPLACE(@VehicleNo, 'v','');
	DECLARE @faultNo int		= @FaultNoMostSignificantByte + @FaultNoLessSignificantByte;
	DECLARE @tsnCpl int			= @TsnCplMostSignificantByte + @TsnCplLessSignificantByte;
	DECLARE @cab int			= @CabByte / 16;
	DECLARE @mcs int			= (@McsByte % 16) / 4;
	DECLARE @direction int		= @DirectionByte % 4;
	DECLARE @emerBrake int;
	DECLARE @faultMetaID int;
	DECLARE @faultUnitID int;
	DECLARE @faultID int;
	DECLARE @createTime datetime2(3) = CONVERT(varchar(10), @faultDate, 121) + ' ' + CONVERT(varchar(13), @faultTime);
	
	IF(@EmerBrakeIsolated = 1)
		SET @emerBrake = 2;
	ELSE
		SET @emerBrake = @EmerBrakeState;

	-- Add record in TmsFaultLookup, if corresponding FaultNo does not exist
	IF NOT EXISTS (SELECT 1 FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo) 
	BEGIN

		INSERT INTO [dbo].[TmsFaultLookup]
		   ([FaultNo]
		   ,[Description]
		   ,[FixingInstr]
		   ,[SwRef]
		   ,[TmsCategoryID]
		   ,[TmsFunctionID])
		SELECT
		   @faultNo
		   ,'Unknown FaultNo: ' + CONVERT(varchar(100), @faultNo)
		   ,'?'
		   ,'?'
		   ,0
		   ,0

	END
	
	-- get @faultMetaID
	SET @faultMetaID = (
		SELECT ID 
		FROM FaultMeta
		WHERE FaultCode = 'F_TMS_' + CONVERT(varchar(10), @faultNo)
	)

	IF @faultMetaID IS NULL
	BEGIN

		INSERT INTO dbo.FaultMeta(
			FaultCode	
			, Description	
			, Summary	
			, CategoryID	
			, FaultTypeID
			, RecoveryProcessPath)
		SELECT
			'F_TMS_' + CONVERT(varchar(10), @faultNo)
			, (SELECT Description FROM TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT Description FROM TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT ID FROM FaultCategory WHERE Category = 'TMS')
			, (SELECT ID FROM FaultType WHERE Name = 'Fault')
			, NULL
		
		SET @faultMetaID = SCOPE_IDENTITY()

	END
	
	-- get @faultVehicleID -- first vehicle on the unit
	SET @faultUnitID = (
		SELECT UnitID 
		FROM Vehicle 
		WHERE VehicleNumber = @vehicleNumber
	)
	
	IF @faultUnitID IS NOT NULL
		AND @faultMetaID IS NOT NULL
		AND @createTime IS NOT NULL
		AND NOT EXISTS (
			SELECT 1
			FROM dbo.Fault
			WHERE FaultUnitID = @faultUnitID
				AND FaultMetaID = @faultMetaID
				AND CreateTime = CONVERT(datetime, @createTime)
			)
	BEGIN
		
		INSERT INTO [Fault]
			(CreateTime
			,HeadCode
			,SetCode
			,LocationID
			,IsCurrent
			,EndTime
			,Latitude
			,Longitude
			,FaultMetaID
			,FaultUnitID
			,IsDelayed)
		SELECT
			CreateTime	= @createTime
			,HeadCode	= NULL
			,SetCode	= NULL
			,LocationID	= NULL
			,IsCurrent	= 0
			,EndTime	= DATEADD(n, 1, @createTime)
			,Latitude	= NULL
			,Longitude	= NULL
			,FaultMetaID	= @faultMetaID
			,FaultUnitID	= @faultUnitID
			,IsDelayed		= 0
			
		SET @faultID = SCOPE_IDENTITY()
		
	END
	
	INSERT INTO TmsFault (
		FaultID
		, VehicleNumber
		, FaultDateTime
		, CarNo
		, FaultNo
		, TsnCplId
		, CapId
		, McsId
		, DirectionId
		, VoltageId
		, SpeedSetId
		, BrakeContId
		, EmerBrakeId
		, StabledId
		, MlpId
		, BrakePrId
		, BatteryId
		, LoadshedId
		, TracShoreSupId
		, AuxShoreSupId
		, CarBmCoupledId
		, CarBjCoupledId
		, Tmcc1Id
		, Speed)
	VALUES (
		@faultID
		, @vehicleNumber
		, @createTime
		, @CarNo
		, @faultNo
		, @tsnCpl
		, @cab
		, @mcs
		, @direction
		, @voltage
		, @speedSet
		, @brakeCont
		, @emerBrake
		, @stabled
		, @mlp
		, @brakePr
		, @battery
		, @loadshed
		, @tractShoreSup
		, @auxShoreSup
		, @carBmCoupled
		, @carBjCoupled
		, @tmcc1
		, @speed
		);

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_OaArrivalDeparture]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaArrivalDeparture
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaArrivalDeparture]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN
	
	SET DATEFORMAT ymd
	DECLARE @journeyNumber int
	DECLARE @extendedDwellLimit int	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int 	-- in seconds
	DECLARE @lateDepartureLimit int -- in seconds
	
	-- Read Dwell definition limits 
	SET @extendedDwellLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_extendeddwell')) = 1
			THEN (SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_extendeddwell')
		ELSE NULL
	END	
	
	SET @earlyArrivalLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_earlyarrival')) = 1
			THEN (SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_earlyarrival')
		ELSE NULL
	END	
	
	SET @lateDepartureLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_latedeparture')) = 1
			THEN (SELECT PropertyValue FROM Configuration WHERE PropertyName = 'OA_latedeparture')
		ELSE NULL
	END
	
	-- If not defined set up default
	IF @extendedDwellLimit IS NULL 
		SET @extendedDwellLimit = 60
		
	IF @earlyArrivalLimit IS NULL 
		SET @earlyArrivalLimit = 60
		
	IF @lateDepartureLimit IS NULL 
		SET @lateDepartureLimit = 60
	
	SET @EndDate = DATEADD(d, 1, @EndDate)

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM VW_TrainPassageValid tp 		
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
	)
		
	---------------------------------------------------------------------------
	-- header information
	---------------------------------------------------------------------------
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM Journey 
	LEFT JOIN SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all section points for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CteSectionPoint') IS NOT NULL
		DROP TABLE #CteSectionPoint
	
	SELECT
		SectionPointID
		, TrainPassageID
		, ActualArrivalTime		= DATEDIFF(s, '00:00:00.000', CAST(ActualArrivalDatetime AS time))
		, ActualDepartureTime	= DATEDIFF(s, '00:00:00.000', CAST(ActualDepartureDatetime AS time))
		, ActualArrivalDatetime
		, ActualDepartureDatetime
		, StartDatetime			= CAST(StartDatetime AS date)
		, SectionPointType
	INTO #CteSectionPoint
	FROM VW_TrainPassageValid tp
	INNER JOIN TrainPassageSectionPoint ON TrainPassageID = tp.ID
	INNER JOIN SectionPoint ON SectionPoint.ID = SectionPointID AND tp.JourneyID = SectionPoint.JourneyID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate


	---------------------------------------------------------------------------
	-- to avoid divide by 0 problem in next query use NULL value for @journeyNumber if it equals 0
	---------------------------------------------------------------------------
	IF @journeyNumber = 0 
		SET @journeyNumber = NULL

	---------------------------------------------------------------------------
	-- Main report dataset
	-- display Arrival Date for (S - Station, E - End location)
	-- display Departure Date for (S - Station, B - Start location, T - Timing Points)
	---------------------------------------------------------------------------
	SELECT
		Loc						= Location.Tiploc 
		, BookedArrive			= ScheduledArrival 
		, BookedDeparture		= ISNULL(ScheduledDeparture, ScheduledPass)
		, RecoveryTime

		, ActualMinArrive		= CASE 
			WHEN SectionPointType IN ('S', 'E')			
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMinDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID)
			END 
		
		, ActualMaxArrive		= CASE
			WHEN SectionPointType IN ('S', 'E') 
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMaxDeparture	= CASE 
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMeanArrive		= CASE
			WHEN SectionPointType IN ('S', 'E')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, ActualMeanDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, BookedDwell			= dbo.FN_ConvertSecondToTime(DATEDIFF(s, ScheduledArrival, ScheduledDeparture))
		, MinDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MIN(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
		, MaxDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MAX(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
	
		-- Percentages 
		, [% Extended Dwell] = CASE 
			WHEN SectionPointType IN ('S') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) >= DATEDIFF(s, ScheduledArrival, ScheduledDeparture) + @extendedDwellLimit
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Early Arrival] = CASE 
			WHEN SectionPointType IN ('S', 'E') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Late Departure] = CASE 
				WHEN SectionPointType IN ('S', 'B') 
					THEN CAST(
						(SELECT COUNT(1)
						FROM #CteSectionPoint
						WHERE SectionPointID = SectionPoint.ID
							AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
						) / (@journeyNumber * 0.01)
					AS decimal(9, 2))
			END
		
		-- Dates for Min/Max passages
		, MinArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime ASC) END
		, MaxArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime DESC) END
		, MinDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime ASC) END
		, MaxDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime DESC) END
		, MinDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) ASC) END
		, MaxDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) DESC) END

		, IsStation			= CASE 
				WHEN SectionPointType IN ('B', 'S', 'E')
					THEN CAST(1 AS bit)
				ELSE CAST(0 AS bit)
			END
	FROM SectionPoint
	LEFT JOIN Location ON Location.ID = LocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SectionPoint.ID

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaDelayDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_OaDelayDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaDelayDetail]
(
	@TrainPassageID int
	, @SegmentID int
	, @SectionID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	IF @SectionID <> 0
		-- select data for Section
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM TrainPassageSection
		INNER JOIN TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SectionID = @SectionID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	 
	ELSE
		-- select data for Segment
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM TrainPassageSection
		INNER JOIN TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SegmentID = @SegmentID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaDelaySummary]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaDelaySummary
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaDelaySummary]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @ServiceGroupID int = NULL
)
AS
BEGIN

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT 
		DelayReasonID
		, TotalDuration		= CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM VW_TrainPassageValid tp
	INNER JOIN TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
	GROUP BY 
		DelayReasonID
		, Name
		, ChartColor	
	ORDER BY TotalDuration DESC
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaDelaySummaryDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_OaDelaySummaryDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/


CREATE PROCEDURE [dbo].[NSP_OaDelaySummaryDetail]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @DelayReasonID int
)
AS
BEGIN

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
		JourneyID			= tp.JourneyID
		, TrainPassageID	= tp.ID
		, Headcode 
		, DelayDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, DelayName			= dr.Name
		, TiplocStart		= sl.Tiploc
		, TiplocEnd			= el.Tiploc
		, JourneyDate		= CAST(tps.ActualStartDatetime AS date)
		, LeadingVehicle	= v.VehicleNumber
	FROM VW_TrainPassageValid tp
	INNER JOIN TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN Location sl ON s.StartLocationID = sl.ID
	INNER JOIN Location el ON s.EndLocationID = el.ID
	INNER JOIN TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	LEFT JOIN Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
		AND DelayReasonID = @DelayReasonID
	ORDER BY DelayDuration DESC
		, tps.ActualStartDatetime
		, Headcode

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaGetHeadcodeList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_OaGetHeadcodeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaGetHeadcodeList]
(
	@HeadcodeString varchar(50)
)
AS
BEGIN

	DECLARE @currentDate date = GETDATE()

	SELECT
		JourneyID		= Journey.ID
		, Headcode
		, HeadcodeDesc	= HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, ValidFrom
		, ValidTo
		, Period		= 
			CASE
				WHEN @currentDate BETWEEN ValidFrom AND ValidTo 
					THEN 'Current'
				ELSE 'Previous'
			END
	FROM Journey
	LEFT JOIN SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN Location el ON el.ID = esp.LocationID
	WHERE Headcode LIKE '%' + @HeadcodeString + '%'
		AND ValidTo > DATEADD(YYYY, -1, GETDATE())
		AND StpType = 'P'
	ORDER BY 
		ValidTo DESC
		, ValidFrom ASC

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaGetLocationList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_OaGetLocationList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaGetLocationList]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
	FROM Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaGetSegmentList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaGetSegmentList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaGetSegmentList]
(
	@Keyword varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT 
		StartLocationID
		, EndLocationID
		, SegmentName		= sl.Tiploc + ' - ' + el.Tiploc
		, SegmentLongName	= sl.LocationName + ' - ' + el.LocationName
	FROM Segment s
	INNER JOIN Location sl ON s.StartLocationID = sl.ID
	INNER JOIN Location el ON s.EndLocationID = el.ID
	WHERE 
		sl.Tiploc + ' - ' + el.Tiploc LIKE '%' + @Keyword + '%'
		OR sl.LocationName + ' - ' + el.LocationName LIKE '%' + @Keyword + '%'
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaIndividualJourney]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaIndividualJourney
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaIndividualJourney]
(
	@TrainPassageID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	DECLARE @JourneyID int = (SELECT JourneyID FROM TrainPassage WHERE ID = @TrainPassageID)

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------

	SELECT 
		CASE StpType
			WHEN 'O' THEN HeadcodeDesc + ' STP'
			ELSE HeadcodeDesc
		END AS Headcode
		, StartDatetime	
		, ISNULL(va.VehicleNumber, '') + ' - ' + ISNULL(vb.VehicleNumber, '')  AS VehicleNumber
		, AnalyseDatetime
	FROM TrainPassage tp 
	INNER JOIN Journey ON Journey.ID = JourneyID
	LEFT JOIN Vehicle va ON EndAVehicleID = va.ID
	LEFT JOIN Vehicle vb ON EndBVehicleID = vb.ID
	WHERE tp.ID = @TrainPassageID
	
	---------------------------------------------------------------------------
	-- Load section data into a temporary table
	---------------------------------------------------------------------------
	;WITH CteTrainPassageSection
	AS
	(
		SELECT
			TrainPassageSectionID	= ID 
			, TrainPassageSection.SectionID
			, ActualStartDatetime
			, ActualEndDatetime
			, ActualStartTime		= CAST(ActualStartDatetime AS Time(0)) 
			, ActualEndTime			= CAST(ActualEndDatetime AS Time(0))
			, ActualTime			= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
			, TotalAttributed		= (SELECT CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) FROM TrainPassageSectionDelayReason WHERE TrainPassageSectionID = TrainPassageSection.ID AND DelayDuration >= 1000)
		FROM TrainPassageSection
		WHERE TrainPassageID = @TrainPassageID
	)
	SELECT
		SegmentID				= s.SegmentID
		, SectionID				= s.ID
		, ID					= TrainPassageSectionID
		, StartLoc				= StartLocation.Tiploc
		, EndLoc				= EndLocation.Tiploc
		, StartTime				= CAST(s.StartTime AS datetime)
		, EndTime				= CAST(s.EndTime AS datetime)
		, BookedTime			= DATEDIFF(s, CAST(s.StartTime AS datetime), CAST(s.EndTime AS datetime))
		, ActualTime			= ActualTime
		, ActualStartDatetime
		, ActualEndDatetime
		, ScheduleDeviationSection = 
				ActualTime - 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) < - 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) + 86400 
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) > 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) - 86400 
					ELSE DATEDIFF(s, s.StartTime, s.EndTime)
				END
		, BookedArrivalTime		= s.EndTime
		, ActualArrivalTime		= tps.ActualEndTime
		, ScheduleDeviationCumulative = 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) < - 43200 -- -12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) + 86400 -- +24h
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) > 43200 -- 12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) - 86400 -- -24h
					ELSE DATEDIFF(s, s.EndTime, tps.ActualEndTime) 
				END
		, TotalAttributed		= TotalAttributed
		, RecoveryTime			= RecoveryTime
		, OptimalJourneyDate	= CONVERT(date, ir.StartDatetime)
		, OptimalTime			= ir.SectionTime
	INTO #SectionData
	FROM Section s
	LEFT JOIN Location AS StartLocation ON StartLocation.ID = s.StartLocationID
	LEFT JOIN Location AS EndLocation ON EndLocation.ID = s.EndLocationID
	LEFT JOIN CteTrainPassageSection tps ON SectionID = s.ID
	LEFT JOIN IdealRun ir ON 
		ir.JourneyID = s.JourneyID
		AND ir.SectionID = s.ID
		AND ir.ValidTo IS NULL
	WHERE s.JourneyID = @journeyID
	ORDER BY s.ID	

	---------------------------------------------------------------------------
	-- Main report dataset (calculate segment data, merge with sections, return)
	---------------------------------------------------------------------------
	
	SELECT
		SegmentID	
		, SectionID	
		, ID	
		, StartLoc	
		, EndLoc	
		, BookedTime					= dbo.FN_ConvertSecondToTime(BookedTime)
		, ActualTime					= dbo.FN_ConvertSecondToTime(ActualTime)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(ScheduleDeviationSection)
		, BookedArrivalTime				= CONVERT(char(8), BookedArrivalTime, 108)
		, ActualArrivalTime				= CONVERT(char(8), ActualArrivalTime, 108)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime(ScheduleDeviationCumulative)
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(TotalAttributed)
		, RecoveryTime					= 			
			CASE RecoveryTime
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(RecoveryTime) 
			END
		, OptimalJourneyDate
		, OptimalTime					= dbo.FN_ConvertSecondToTime(OptimalTime)
	FROM #SectionData
	
	UNION ALL
	
	SELECT
		SegmentID	
		, SectionID						= 0
		, ID							= 0
		, StartLoc						= (SELECT TOP 1 StartLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
		, EndLoc						= (SELECT TOP 1 EndLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, BookedTime					= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ActualTime = dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
			- DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, BookedArrivalTime				= (SELECT TOP 1 CONVERT(char(8), BookedArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ActualArrivalTime				= (SELECT TOP 1 CONVERT(char(8), ActualArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime((SELECT TOP 1 ScheduleDeviationCumulative FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC))
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(SUM(TotalAttributed))
		, RecoveryTime					= 			
			CASE SUM(RecoveryTime)
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(SUM(RecoveryTime)) 
			END
		-- THIS IS NOT POPULATED: need more feedback how should it be calculated
		, OptimalJourneyDate			= NULL --MIN(OptimalJourneyDate)	
		, OptimalTime					= dbo.FN_ConvertSecondToTime(SUM(OptimalTime))
	FROM #SectionData sd1
	GROUP BY 		
		SegmentID
	ORDER BY 		
		SegmentID
		, SectionID


	---------------------------------------------------------------------------
	-- Return Delay Breakdown	
	---------------------------------------------------------------------------
	SELECT
		SegmentID			= tps.SegmentID 
		, SectionID			= tps.SectionID
		, LocationStart		= sl.Tiploc
		, LocationEnd		= el.Tiploc
		, TrainPassageSectionID
		, DelayReasonID
		, TotalDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM TrainPassageSectionDelayReason tpsdr
	INNER JOIN TrainPassageSection tps ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN DelayReason dr ON DelayReasonID = dr.ID
	LEFT JOIN Location sl ON sl.ID = s.StartLocationID
	LEFT JOIN Location el ON el.ID = s.EndLocationID
	
	WHERE TrainPassageID = @TrainPassageID
		AND DelayDuration >= 1000

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaJourneyProfile]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaJourneyProfile
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaJourneyProfile] 
(
	@TrainPassageID int
	, @SectionID int = NULL
)
AS
BEGIN

	DECLARE @Date datetime = (SELECT StartDatetime FROM TrainPassage WHERE ID = @TrainPassageID)
	DECLARE @JourneyID datetime = (SELECT JourneyID FROM TrainPassage WHERE ID = @TrainPassageID)

	DECLARE @gmtToBstTimeOffset smallint = dbo.FN_GetGmtToBstTimeDiff(CONVERT(varchar(10), @Date, 121) + ' 04:00:00.000')
	DECLARE @bstToGmtTimeOffset smallint = - @gmtToBstTimeOffset

	-- Sections - veritical markers are based on this dataset
	IF OBJECT_ID('tempdb..#Section') IS NOT NULL
		DROP TABLE #Section

	SELECT
		SectionID	= Section.ID
		, IsStation	= 
			CASE
				WHEN SectionPoint.SectionPointType IN ('B', 'S', 'E') 
					THEN 1
				ELSE 0
			END
		, StartLoc	= StartLocation.Tiploc 
		, EndLoc	= EndLocation.Tiploc
		-- if distance cannot be calulcated base on LocationSection table, will be calculated in straight line:
		, Distance	= ISNULL(Distance, dbo.FN_CalculateDistance (StartLocation.Lat, StartLocation.Lng, EndLocation.Lat, EndLocation.Lng, 0))
	INTO #Section
	FROM Section
	INNER JOIN SectionPoint ON SectionPoint.ID = Section.ID AND SectionPoint.JourneyID = Section.JourneyID
	LEFT JOIN Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE Section.JourneyID = @JourneyID
	ORDER BY Section.ID

	SELECT 
		SectionID
		, IsStation
		, StartLoc 
		, EndLoc
		, Distance
	FROM #Section
		

	-- Ideal Run data
	IF OBJECT_ID('tempdb..#IdealRunData') IS NOT NULL
		DROP TABLE #IdealRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col6 AS CIRSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #IdealRunData
	FROM Section
	INNER JOIN Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN IdealRun ON IdealRun.SectionID = Section.ID AND IdealRun.JourneyID = Section.JourneyID
	LEFT JOIN IdealRunChannelValue ON IdealRun.ID = IdealRunChannelValue.IdealRunID --ON VehicleID = LeadingVehicleID AND TimeStamp BETWEEN StartDatetime AND DATEADD(s, SectionTime, StartDatetime)
	WHERE Section.JourneyID = @JourneyID 
		AND IdealRun.ValidTo IS NULL
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)

	;WITH CteIdealRunData AS
	(
		SELECT
			SectionID	
			, SectionIDRecordID	
			, CIRSpeed	
			, col3	
			, col4	
			, StartLocationLat	
			, StartLocationLng
			, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
		FROM #IdealRunData 
		WHERE SectionIDRecordID = 1
		
		UNION ALL 
		
		SELECT
			ird.SectionID	
			, ird.SectionIDRecordID	
			, ird.CIRSpeed	
			, ird.col3	
			, ird.col4	
			, ird.StartLocationLat	
			, ird.StartLocationLng
			, CteIdealRunData.SectionDistance + dbo.FN_CalculateDistance (CteIdealRunData.col3, CteIdealRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
		FROM #IdealRunData ird
		INNER JOIN CteIdealRunData ON ird.SectionIDRecordID = CteIdealRunData.SectionIDRecordID + 1 AND ird.SectionID = CteIdealRunData.SectionID
	)
	SELECT 
		SectionID
		, CIRSpeed
		, SectionDistance
	FROM CteIdealRunData
	ORDER BY SectionID
		, SectionIDRecordID
	OPTION (MAXRECURSION 20000)
	
	--Actual Run Data
	--DECLARE @trainPassageID int = (SELECT TOP 1 ID FROM VW_TrainPassageValid WHERE JourneyID = @JourneyID AND CAST(StartDatetime AS date) = @Date)
	
	IF OBJECT_ID('tempdb..#ActualRunData') IS NOT NULL
	DROP TABLE #ActualRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col6 AS ActualSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #ActualRunData
	FROM Section
	INNER JOIN Location StartLocation ON StartLocation.ID = StartLocationID
	INNER JOIN TrainPassageSection ON Section.JourneyID = TrainPassageSection.JourneyID AND Section.ID = SectionID
	LEFT JOIN VW_TrainPassageValid AS TrainPassage ON TrainPassage.ID = TrainPassageID
	LEFT JOIN dbo.Vehicle v ON v.ID = LeadingVehicleID
	LEFT JOIN ChannelValue cv ON cv.UnitID = v.UnitID AND TimeStamp BETWEEN DATEADD(hh, @bstToGmtTimeOffset, ActualStartDatetime) AND DATEADD(hh, @bstToGmtTimeOffset, ActualEndDatetime)

	WHERE TrainPassageID = @trainPassageID
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)
	
		
	;WITH CteActualRunData AS
		(
			SELECT
				SectionID	
				, SectionIDRecordID	
				, ActualSpeed	
				, col3	
				, col4	
				, StartLocationLat	
				, StartLocationLng
				, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
			FROM #ActualRunData 
			WHERE SectionIDRecordID = 1
			
			UNION ALL 
			
			SELECT
				ird.SectionID	
				, ird.SectionIDRecordID	
				, ird.ActualSpeed	
				, ird.col3	
				, ird.col4	
				, ird.StartLocationLat	
				, ird.StartLocationLng
				, CteActualRunData.SectionDistance + dbo.FN_CalculateDistance (CteActualRunData.col3, CteActualRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
			FROM #ActualRunData ird
			INNER JOIN CteActualRunData ON ird.SectionIDRecordID = CteActualRunData.SectionIDRecordID + 1 AND ird.SectionID = CteActualRunData.SectionID
		)
		SELECT 
			SectionID
			, ActualSpeed
			, SectionDistance
		FROM CteActualRunData
		ORDER BY SectionID
			, SectionIDRecordID
		OPTION (MAXRECURSION 20000)
					
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaJourneyTime]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaJourneyTime
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaJourneyTime]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @journeyNumber int
	
	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM VW_TrainPassageValid tp	
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	)
	

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------
	
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM Journey 
	LEFT JOIN SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all segments for @JourneyID in selected time ranges
	---------------------------------------------------------------------------	
	IF OBJECT_ID('tempdb..#TempSegment') IS NOT NULL
		DROP TABLE #TempSegment
		
	SELECT
		SectionID		= 0 --This is segment level data SecionID = 0 to allow grouping results
		, SegmentID
		, TrainPassageID
		, ActualTime	= SUM(DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)) 
		, StartDate		= CAST(StartDatetime AS date)
	INTO #TempSegment
	FROM VW_TrainPassageValid tp
	INNER JOIN TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND TrainPassageSection.ActualStartDatetime IS NOT NULL
		AND TrainPassageSection.ActualEndDatetime IS NOT NULL
	GROUP BY
		SegmentID
		, TrainPassageID
		, StartDatetime
	-- to exclude segments not having Actual Times for all sections
	HAVING  COUNT(*) = (
		SELECT COUNT(*) 
		FROM Section 
		WHERE JourneyID = @JourneyID 
			AND Section.SegmentID = TrainPassageSection.SegmentID
		)

	---------------------------------------------------------------------------
	-- create temporary table with all sections for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#TempSection') IS NOT NULL
		DROP TABLE #TempSection

	SELECT
		SectionID			= TrainPassageSection.SectionID
		, SegmentID
		, TrainPassageID
		, ActualTime		= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
		, StartDate			= CAST(tp.StartDatetime AS date)
	INTO #TempSection
	FROM VW_TrainPassageValid tp
	INNER JOIN TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate

	
	---------------------------------------------------------------------------
	-- Data to be displayed in OpsAnalysis table
	---------------------------------------------------------------------------

	SELECT
		SegmentID			= Segment.ID
		, SectionID			= 0
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime)  
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
	FROM Segment
	LEFT JOIN Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID

	UNION ALL
	
	SELECT
		SegmentID
		, SectionID			= Section.ID
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime)
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime DESC)
	FROM Section
	LEFT JOIN Location AS StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN Location AS EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SegmentID, SectionID

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------
	;WITH CteChartData AS 
	(
		SELECT
			StartLoc		= StartLocation.Tiploc
			, EndLoc		= EndLocation.Tiploc 
			, Booked		=
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
						THEN DATEDIFF(s, StartTime, EndTime) + 86400 
					WHEN DATEDIFF(s, StartTime, EndTime) > 43200
						THEN DATEDIFF(s, StartTime, EndTime) - 86400 
					ELSE DATEDIFF(s, StartTime, EndTime)
				END
			, RecoveryTime 
			, IdealRunTime
			, ActualMin		= (SELECT MIN(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMax		= (SELECT MAX(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMean	= (SELECT AVG(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMinDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
			, ActualMaxDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
			, SegmentID		= Segment.ID
		FROM Segment
		LEFT JOIN Location StartLocation ON StartLocation.ID = StartLocationID
		LEFT JOIN Location EndLocation ON EndLocation.ID = EndLocationID
		WHERE JourneyID = @JourneyID
	)
	SELECT 
		StartLoc
		, EndLoc
		, OptimalDiff		= IdealRunTime - Booked
		, ActualMinDiff		= ActualMin - Booked
		, ActualMaxDiff		= ActualMax - Booked
		, ActualMeanDiff	= ActualMean - Booked
		, ActualMinDate
		, ActualMaxDate
	FROM CteChartData
	ORDER BY SegmentID ASC

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaJourneyTimeList]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************
**	Name:			NSP_OaJourneyTimeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaJourneyTimeList]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @actualJourneyID int
	

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @actualJourneyID = (
			SELECT ActualJourneyID 
			FROM JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------

	SELECT
		TrainPassageID			= tp.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		--Scotrail:
		, SetFormation			= (SELECT UnitNumber FROM VW_Vehicle WHERE VW_Vehicle.ID = EndAVehicleID) + ' (' + ISNULL(va.VehicleNumber, '') + '/' + ISNULL(vb.VehicleNumber, '') + ')'
		--ECML:
		--, SetFormation			= ISNULL(tp.Setcode, '') + ' / ' + ISNULL(va.VehicleNumber, '') + ' / ' + ISNULL(vb.VehicleNumber, '') 
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) --/ 60  
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) --/ 60
		, IsIncludedInAnalysis 
	FROM TrainPassage tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.JourneyID IN (@JourneyID, @actualJourneyID)
		AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	GROUP BY 
		tp.ID
		, CAST(tp.StartDatetime AS date)
		, EndAVehicleID
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		, IsIncludedInAnalysis 
	ORDER BY DateStart DESC
			
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_OaRunningTime]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaRunningTime
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaRunningTime]
(
	@DateStart date
	, @DateEnd date
	, @LocationIdStart int
	, @LocationIdEnd int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
	    JourneyID               = s.JourneyID
	    , JourneySegmenID		= s.ID
		, Headcode				= j.Headcode 
		, HeadcodeDescription	= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram				= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + jsl.Tiploc
			+ ' - ' + jel.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.EndTime, 108), ':', ''), 4) + 10000, 4)
		, NoOfPassages		= COUNT(*)
		, BookedTime		= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime))))
		, RecoveryTime		= dbo.FN_ConvertSecondToTime((
				SELECT SUM(RecoveryTime)
				FROM Section sc
				WHERE sc.SegmentID = s.ID
					AND sc.JourneyID = s.JourneyID
			)
		)

		-- OptimalTime - calculation should be simplified:
		, OptimalTime		= MIN(OptimalTrainPassage.OptimalTime)  -- There is only 1 record so MIN/MAX grouping function doesnt matter
		, ActualMin			= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMax			= dbo.FN_ConvertSecondToTime(MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMean		= dbo.FN_ConvertSecondToTime(AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
	
		, TrainPassageIdOptimal				= MIN(OptimalTrainPassage.TrainPassageID)	-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMin			= MIN(MinTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMax			= MIN(MaxTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		
	FROM Segment s 
	INNER JOIN Journey j ON j.ID = s.JourneyID
	INNER JOIN TrainPassageSegment tps ON 
		s.JourneyID = tps.JourneyID
		AND s.ID = tps.SegmentID
	INNER JOIN Location jsl ON jsl.ID = j.StartLocationID
	LEFT JOIN Location jel ON jel.ID = j.EndLocationID
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) ASC
	) AS MinTrainPassage
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) DESC
	) AS MaxTrainPassage
	OUTER APPLY (
		SELECT TOP 1 
			TrainPassageID
			, OptimalTime = dbo.FN_ConvertSecondToTime(DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd))
		FROM TrainPassageSegment tps1
		INNER JOIN Segment s1 ON
			s1.JourneyID = tps1.JourneyID
			AND s1.ID = tps1.SegmentID
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			ABS(
				DATEDIFF(S, s1.StartTime, s1.EndTime) -- booked time
				- DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd)
			) ASC
	) AS OptimalTrainPassage
	
	WHERE 
		s.StartLocationID = @LocationIdStart
		AND s.EndLocationID = @LocationIdEnd
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
	GROUP BY 
		s.ID
		, s.JourneyID
		, j.Headcode 
		, j.HeadcodeDesc
		, j.StartTime
		, j.EndTime
		, jsl.Tiploc
		, jel.Tiploc

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaRunningTimeDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaRunningTimeDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaRunningTimeDetail]
(
	@DateStart date
	, @DateEnd date
	, @JourneyID int
	, @SectionID int
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		StartLocation		= sl.Tiploc
		, EndLocation		= el.Tiploc
		, BookedStartTime	= s.StartTime
		, BookedEndTime		= s.EndTime
		, BookedTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime)))
		, TrainPassageDate 	= CONVERT(date, tps.ActualDatetimeStart)
		, TrainPassageWeekDay 	= LEFT(DATENAME(W, tps.ActualDatetimeStart), 3)
		, ActualStartTime	= CONVERT(time(0),tps.ActualDatetimeStart)
		, ActualEndTime		= CONVERT(time(0),tps.ActualDatetimeEnd)
		, ActualTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, tps.ActualDatetimeStart, tps.ActualDatetimeEnd)))
		, LeadingVehicle	= v.VehicleNumber
		, AverageSpeed		= tps.AverageSpeed
		, TotalEnergy		= tps.EnergyRealMove + tps.EnergyReactiveStop
		
	FROM TrainPassageSegment tps
	INNER JOIN Segment s ON 
		s.ID = tps.SegmentID
		AND s.JourneyID = tps.JourneyID
	INNER JOIN Location sl ON sl.ID = s.StartLocationID
	INNER JOIN Location el ON el.ID = s.EndLocationID
	LEFT JOIN Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE tps.JourneyID = @JourneyID
		AND tps.SegmentID = @SectionID
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
		
	ORDER BY tps.ActualDatetimeStart
	
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaTimetableByHeadcode]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByHeadcode
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaTimetableByHeadcode]
(
	@StartDate datetime
	, @EndDate datetime
	, @N int = NULL
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	-- number of records returned
	IF @N IS NULL OR @N < 0
		SET @N = 1000

	
	SELECT 
		TrainPassageID		= tp.ID								
		, Headcode			= tp.Headcode
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber 
		, Setcode			= tp.Setcode
		, DelayOnArrival	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) / 60.  
		, DelayOnDeparture	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0)))) / 60.
	INTO #DelayAtDestination
	FROM VW_TrainPassageValid tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)  
	
	SELECT
		TrainPassageID			= tp.ID
		, Headcode				= tp.Headcode
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM VW_TrainPassageValid tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR (tp.ID IN (select dad.TrainPassageID from #DelayAtDestination dad )))  
	GROUP BY 
		tp.ID
		, tp.Headcode
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode

	
	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival)  
			, MaxDelayOnArrival = MAX(DelayOnArrival)  
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
	SELECT TOP (@N)
		Headcode
		, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(SectionPointTypeNumber_S)
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B))
			END
		
	

	FROM #DelayAtStation
	GROUP BY
		Headcode		
	ORDER BY 4 DESC 

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaTimetableByHeadcodeDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByHeadcodeDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaTimetableByHeadcodeDetail]
(
	@StartDate datetime
	, @EndDate datetime
	, @Headcode varchar(4)
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	
	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds
	
	SELECT 
		ValidID		= tp.ID
	INTO #ValidTrainPassages
	FROM VW_TrainPassageValid tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)

	SELECT
		TrainPassageID		= tp.ID
		, JourneyID			= tp.JourneyID
		, Headcode			= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram			= dbo.FN_GetDiagramByHeadcodeAndDate(tp.Headcode, tp.StartDatetime, tp.StartDatetime)
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber
		, tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60.
		, MaxDelayOnArrival		= MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, DelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(
			CASE
				WHEN SectionPointType = 'E'
					THEN DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))
			END) / 60.)
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM VW_TrainPassageValid tp
	INNER JOIN Journey j ON j.ID = tp.JourneyID
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND tp.Headcode = @Headcode
		AND tp.id IN (SELECT vtp.ValidID from #ValidTrainPassages vtp)
	GROUP BY 
		tp.ID
		, tp.JourneyID
		, j.StartTime
		, tp.Headcode
		, j.HeadcodeDesc
		, tp.StartDatetime	--CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		
	SELECT
		ds.Headcode
		, ds.Diagram
		, TrainPassageID
		, JourneyID
		, ds.DateStart
		, DestinationDelay		= DelayOnArrival  
		, AvgStationDelay		= AvgDelayOnArrival
		, MaxStationDelay		= MaxDelayOnArrival
		, ExtendedDwellsPerc	= CASE
			WHEN SectionPointTypeNumber_S > 0
				THEN 100.0 * ExtendedDwellsNumber / SectionPointTypeNumber_S
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * EarlyArrivalsNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_E)
			END
		, LateDeparturesPerc	= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * LateDeparturesNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_B)
			END
	FROM #DelayAtStation AS ds
	ORDER BY ds.DateStart ASC

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaTimetableByLocation]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NSP_OaTimetableByLocation]
(
	@StartDate datetime
	, @EndDate datetime
	, @N int = NULL
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	-- number of records returned
	IF @N IS NULL OR @N < 0
		SET @N = 1000

	SELECT
		TrainPassageID			= tp.ID
		, Location				= l.Tiploc
		, LocationName			= l.LocationName
		, LocationID			= l.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM VW_TrainPassageValid tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	INNER JOIN Location l ON sp.LocationID = l.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)  
	GROUP BY 
		tp.ID
		, l.Tiploc
		, l.ID
		, l.LocationName
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode


--SELECT * FROM #DelayAtStation WHERE Location = 'KNGX'
	
/*	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival)  
			, MaxDelayOnArrival = MAX(DelayOnArrival)  
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
*/	SELECT TOP (@N)
		Location
		, LocationID
		, LocationName
--		, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
--		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
--		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(ISNULL(SectionPointTypeNumber_S, 0))
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)))
			END
		
	FROM #DelayAtStation
	GROUP BY
		Location		
		, LocationID
		, LocationName			
	ORDER BY 4 DESC 

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_OaTimetableByLocationDetail]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByLocationDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_OaTimetableByLocationDetail]
(
	@StartDate datetime
	, @EndDate datetime
	, @LocationID int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	SELECT
		TrainPassageID			= tp.ID
		, Location				= l.Tiploc
		, LocationID			= l.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM VW_TrainPassageValid tp
	INNER JOIN SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN Vehicle vb ON EndBVehicleID = vb.ID
	INNER JOIN Location l ON sp.LocationID = l.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND l.ID = @LocationID
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)
	GROUP BY 
		tp.ID
		, l.Tiploc
		, l.ID
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode


--SELECT * FROM #DelayAtStation WHERE Location = 'KNGX'
	
/*	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival)  
			, MaxDelayOnArrival = MAX(DelayOnArrival)  
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
*/	SELECT
		Location
		, LocationID
		, DateStart
--, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
--		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
--		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(ISNULL(SectionPointTypeNumber_S, 0))
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)))
			END
		
	FROM #DelayAtStation
	GROUP BY
		Location		
		, LocationID
		, DateStart
	ORDER BY DateStart ASC 

END

GO
/****** Object:  StoredProcedure [dbo].[NSP_RuleEngineGetCurrentIds]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_RuleEngineGetCurrentIds
**	Description:	Returns data to fault engine
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_RuleEngineGetCurrentIds]
	@ConfigurationName varchar(100)
AS
BEGIN

SELECT Value FROM RuleEngineCurrentId ORDER BY Position asc

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_RuleEngineUpdateCurrentIds]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_RuleEngineUpdateCurrentIds
**	Description:	Returns data to fault engine
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_RuleEngineUpdateCurrentIds]
	@ConfigurationName varchar(100),
	@Value bigint = NULL
	
AS
BEGIN
	
	;MERGE RuleEngineCurrentId AS r 
    USING (
    	
    	SELECT Position = '1', ConfigurationName = @ConfigurationName, Value = @Value
		
    ) AS nr
    ON r.Position = nr.Position
    	AND r.ConfigurationName = nr.ConfigurationName
    WHEN MATCHED THEN 
    UPDATE SET
    	Value = nr.Value
    WHEN NOT MATCHED THEN
    INSERT (
    	ConfigurationName
    	, Position
    	, Value)
    VALUES(
    	nr.ConfigurationName
    	, nr.Position
    	, nr.Value);

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_SaveChannelDefinition]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_SaveChannelDefinition
**	Description:	Saves a channel definition
**	Call frequency:	Called from Channel Definition module
**	Parameters:		@ChannelDefinitionDoc xml
**	Return values:	
*******************************************************************************/
        
CREATE PROCEDURE [dbo].[NSP_SaveChannelDefinition]
	@ChannelDefinitionParam varchar(max)
AS
BEGIN
	DECLARE @channelDoc xml = N''+@ChannelDefinitionParam;

	--read the channel definition details
	DECLARE @channelId int;
	DECLARE @channelGroupID int;
	DECLARE @channelRef varchar(10);
	DECLARE @channelUnitOfMeasure varchar(10);
	DECLARE @channelGroupOrder int;
	DECLARE @channelAlwaysDisplayed bit;
	DECLARE @previousOder int;

	SELECT
		@channelId = t.c.value(N'@id', N'int'),
		@channelGroupId = t.c.value(N'@channelgroupid', N'int'),
		@channelRef = t.c.value(N'@ref', N'varchar(10)'),
		@channelUnitOfMeasure = t.c.value(N'@unitofmeasure', N'varchar(10)'),
		@channelGroupOrder = t.c.value(N'@channelgrouporder', N'int'),
		@channelAlwaysDisplayed = t.c.value(N'@alwaysdisplayed', N'bit')
	FROM @channelDoc.nodes('/channel') t(c);
	
	-- Get previous order  
    SELECT 
        @previousOder = ChannelGroupOrder 
    FROM Channel 
    WHERE ID = @channelId   
    
	IF @channelRef = ''
		SET @channelRef = NULL;

	IF @channelUnitOfMeasure = ''
		SET @channelUnitOfMeasure = NULL;

	IF @channelGroupOrder = ''
		SET @channelGroupOrder = NULL;

	BEGIN TRANSACTION;
		--move the channel group order 1 position ahead
		IF NOT @previousOder = @channelGroupOrder
			UPDATE
				[Channel]
			SET
				[ChannelGroupOrder] = [ChannelGroupOrder] + 1
			WHERE
				[ChannelGroupID] = @channelGroupId
				AND [ChannelGroupOrder] >= @channelGroupOrder;

		--update channel definition
		UPDATE
			[Channel]
		SET
			[ChannelGroupID] = @channelGroupId,
			[Ref] = @channelRef,
			[UOM] = @channelUnitOfMeasure,
			[ChannelGroupOrder] = @channelGroupOrder,
			[IsAlwaysDisplayed] = @channelAlwaysDisplayed
		WHERE
			[ID] = @channelId;

		--remove all rule/validations for the channel
		DELETE
			FROM [ChannelRuleValidation]
			WHERE
				ChannelRuleID IN (SELECT ID FROM [ChannelRule] WHERE ChannelID = @channelId);

		DELETE
			FROM [ChannelRule]
			WHERE ChannelID = @channelId;

		--process rules
		DECLARE @ruleIndex int; 
		DECLARE @rulesCount int;
		DECLARE @ruleXml xml;

		SELECT 
			@ruleIndex = 1,
			@rulesCount = @channelDoc.value('count(/channel/rule)','int');

		WHILE @ruleIndex <= @rulesCount BEGIN
			DECLARE @ruleName varchar(100);
			DECLARE @ruleStatusId int;
			DECLARE @ruleActive bit;
			DECLARE @ruleId int;

			SELECT
				@ruleXml = @channelDoc.query('/channel/rule[position()=sql:variable("@ruleIndex")]')

			SELECT
				@ruleName = t.c.value(N'@name', N'varchar(100)'),
				@ruleStatusId = t.c.value(N'@statusid', N'int'),
				@ruleActive = t.c.value(N'@active', N'bit')
			FROM @ruleXml.nodes('/rule') t(c);

			--insert the rule
			INSERT INTO ChannelRule (ChannelID, ChannelStatusID, Name, Active)
				VALUES (@channelId, @ruleStatusId, @ruleName, @ruleActive);

			SET @ruleId = SCOPE_IDENTITY();
			
			--process validations
			DECLARE @validationIndex int; 
			DECLARE @validationsCount int;
			DECLARE @validationXml xml;
			
			SELECT 
				@validationIndex = 1,
				@validationsCount = @ruleXml.value('count(/rule/validation)','int');

			WHILE @validationIndex <= @validationsCount BEGIN
				DECLARE @validationChannelId int;
				DECLARE @validationMinValue float;
				DECLARE @validationMaxValue float;
				DECLARE @validationMinInc bit;
				DECLARE @validationMaxInc bit;
				
				SELECT
					@validationXml = @ruleXml.query('/rule/validation[position()=sql:variable("@validationIndex")]')

				SELECT
					@validationChannelId = t.c.value(N'@channelid', N'int'),
					@validationMinValue = t.c.value(N'@minvalue', N'float'),
					@validationMaxValue = t.c.value(N'@maxvalue', N'float'),
					@validationMinInc = t.c.value(N'@mininclusive', N'bit'),
					@validationMaxInc = t.c.value(N'@maxinclusive', N'bit')
				FROM @validationXml.nodes('/validation') t(c);
		
				--insert validation
				INSERT INTO ChannelRuleValidation (ChannelRuleID, ChannelID, MinValue, MaxValue, MinInclusive, MaxInclusive)
					VALUES (@ruleId, @validationChannelId, @validationMinValue, @validationMaxValue, @validationMinInc, @validationMaxInc);
				
				SELECT @validationIndex = @validationIndex + 1;
			END
			
			SELECT @ruleIndex = @ruleIndex + 1;
		END

			
	COMMIT TRANSACTION;
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_SearchChannelDefinition]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-------------------------------------------------------------------------------

/******************************************************************************
**	Name:			[NSP_SearchChannelDefinition]
**	Description:	Returns list channel definition for a keyword
**	Call frequency:	Called from Channel Definition module
**	Parameters:		@ChannelId int
**					@Keyword varchar(50)
**	Return values:	
*******************************************************************************/
        
CREATE PROCEDURE [dbo].[NSP_SearchChannelDefinition]
	@ChannelId int = NULL,
	@Keyword varchar(50) = NULL
AS
BEGIN
	IF @ChannelId IS NULL
	BEGIN
		IF @Keyword IS NULL OR LEN(@Keyword) = 0
			SET @Keyword = '%'
		ELSE
			SET @Keyword = '%' + @Keyword + '%'
	END

	SELECT
			vcd.ID
            , vcd.VehicleID
            , vcd.ColumnID
            , vcd.ChannelGroupID
            , vcd.Name
            , vcd.Description
            , vcd.ChannelGroupOrder
            , vcd.Type
            , vcd.UOM
            , vcd.HardwareType
            , vcd.StorageMethod
            , vcd.Ref
            , vcd.MinValue
            , vcd.MaxValue
            , vcd.Comment
            , vcd.VisibleOnFaultOnly
            , vcd.IsAlwaysDisplayed
            , vcd.IsLookup
            , vcd.DefaultValue
            , vcd.IsDisplayedAsDifference
	FROM VW_ChannelDefinition as vcd
	INNER JOIN ChannelGroup as cgd ON vcd.ChannelGroupID = cgd.ID
	WHERE
		(@ChannelId IS NULL
			AND (vcd.ID LIKE @Keyword
				OR cgd.Notes LIKE @Keyword
				OR vcd.Description LIKE @Keyword
				OR vcd.Name LIKE @Keyword
				OR vcd.Ref LIKE @Keyword
				OR vcd.Type LIKE @Keyword
				OR vcd.UOM LIKE @Keyword))
		OR
		(@ChannelId IS NOT NULL AND vcd.ID = @ChannelId)
	ORDER BY vcd.ID
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_SearchFault]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**  Name:           NSP_SearchFault
**  Description:    Returns list faults for a keyword
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Keyword varchar(50)
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus 
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_SearchFaultAdvanced]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**  Name:           NSP_SearchFaultAdvanced
**  Description:    Returns list of Faults matching parameters
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Headcode varchar(10)
**                  @UnitNumber varchar(10)
**                  @Vehicle varchar(10)
**                  @Location varchar(10)
**                  @FaultMetaID int
**                  @Description varchar(50)
**                  @Type varchar(10)
**                  @CategoryID int
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
)
AS
BEGIN 

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @Type = ''       SET @Type = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , Description   
            , Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    = @Type                         OR @Type IS NULL)
        ORDER BY CreateTime DESC 

END


GO
/****** Object:  StoredProcedure [dbo].[NSP_SearchFaultAdvancedCount]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**  Name:           NSP_SearchFaultAdvancedCount
**  Description:    Returns Fault count with specific parameters
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_SearchFaultAdvancedCount](
    @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
)
AS
BEGIN 

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @Type = ''       SET @Type = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
        
    IF @SearchType = 'Live'
        
        SELECT  COUNT(*)
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    = @Type                         OR @Type IS NULL)
        
    IF @SearchType = 'NoLive'
        
        SELECT  COUNT(*)
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    = @Type                         OR @Type IS NULL)

    IF @SearchType = 'All'
        
        SELECT  COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    = @Type                         OR @Type IS NULL)
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_SearchFaultCount]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**  Name:           NSP_SearchFaultCount
**  Description:    Returns Fault count with specific parameters
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_SearchFaultCount]
    @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN
     IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT COUNT(*)
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE (1 = 1) 
                AND CreateTime BETWEEN @DateFrom AND @DateTo
                AND (
                    HeadCode                LIKE @Keyword 
                    OR  FaultUnitNumber     LIKE @Keyword
                    OR  Category            LIKE @Keyword 
                    OR  Description         LIKE @Keyword
                    OR  LocationCode        LIKE @Keyword 
                    OR  FaultType           LIKE @Keyword
                ) 
        
    IF @SearchType = 'NoLive'
        
        SELECT COUNT(*)
            
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE (1 = 1) 
                AND CreateTime BETWEEN @DateFrom AND @DateTo
                AND (
                    HeadCode                LIKE @Keyword 
                    OR  FaultUnitNumber     LIKE @Keyword
                    OR  Category            LIKE @Keyword 
                    OR  Description         LIKE @Keyword
                    OR  LocationCode        LIKE @Keyword 
                    OR  FaultType           LIKE @Keyword
                ) 
        
    IF @SearchType = 'All'
        
        SELECT COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE (1 = 1) 
                AND CreateTime BETWEEN @DateFrom AND @DateTo
                AND (
                    HeadCode                LIKE @Keyword 
                    OR  FaultUnitNumber     LIKE @Keyword
                    OR  Category            LIKE @Keyword 
                    OR  Description         LIKE @Keyword
                    OR  LocationCode        LIKE @Keyword 
                    OR  FaultType           LIKE @Keyword
                ) 
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_UpdateChannelCategory]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_UpdateChannelCategory
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_UpdateChannelCategory]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @GroupOrder int
	, @Group varchar(100)
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT * FROM Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	UPDATE Channel SET
		ChannelGroupOrder = @GroupOrder
		, ChannelGroupID = (SELECT ID FROM ChannelGroup WHERE Notes =@Group)
		, IsAlwaysDisplayed = 1
	WHERE ID = @ChannelID
END

GO
/****** Object:  StoredProcedure [dbo].[NSP_UpdateFaultCategory]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[NSP_UpdateFaultCategory](
	@ID int
	, @Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRAN
	BEGIN TRY

		UPDATE dbo.FaultCategory SET
			Category = @Category
			, ReportingOnly = @ReportingOnly
		WHERE ID = @ID	
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
END



GO
/****** Object:  StoredProcedure [dbo].[NSP_UpdateFaultMeta]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NSP_UpdateFaultMeta](
    @ID int
	, @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @Url varchar(500)
    , @CategoryID int
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
    BEGIN
        
        SET NOCOUNT ON;

        BEGIN TRAN
        BEGIN TRY

            UPDATE dbo.FaultMeta SET
                FaultCode       = @FaultCode
                , Description   = @Description 
                , Summary       = @Summary
                , Url           = @Url
                , CategoryID    = @CategoryID
                , RecoveryProcessPath   = @RecoveryProcessPath
                , FaultTypeID   = @FaultTypeID
                , GroupID   = @GroupID
            WHERE ID = @ID

            COMMIT TRAN
        
        END TRY
        BEGIN CATCH

            EXEC NSP_RethrowError;
            ROLLBACK TRAN;

        END CATCH

    END

GO
/****** Object:  StoredProcedure [dbo].[NSP_UpdateReportAdhesion]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_UpdateReportAdhesion
**	Description:	Updates ReportAdhesion table
**	Call frequency:	Every 15min
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/10/12 09:24:15 $$
**	$$Revision: 1.2.2.2 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/update_spectrum_db_014.sql,v $$
******************************************************************************/


CREATE PROCEDURE [dbo].[NSP_UpdateReportAdhesion]
AS

	SET NOCOUNT ON;
		
	DECLARE @reportTime datetime = DATEADD(n, -1, SYSDATETIME())
	DECLARE @reportCallFrequency int = 15 -- min

	DELETE [dbo].[ReportAdhesion] 
	WHERE TimeStamp < DATEADD(d, -7, @reportTime)
	
	
	SELECT
		ROW_NUMBER() OVER (PARTITION BY UnitID ORDER BY Timestamp) AS UnitRowNumber
		, ID
		, TimeStamp
		, UnitID
		, Col3
		, Col4
		, ISNULL(Col54, 0)	AS WSP
		, CAST(NULL AS bit) AS WspChange
		, CAST(NULL AS datetime) AS EventEndTimestamp
	INTO #temp
	FROM ChannelValue (NOLOCK)
	WHERE
		TimeStamp BETWEEN DATEADD(n, (-2 * @reportCallFrequency + 1), @reportTime) AND @reportTime
		AND UnitID NOT IN (SELECT unitID FROM dbo.Vehicle v WHERE v.UnitID IN(352, 442)) --VehicleNumber: 77704, 62837
	
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_VehicleRowNumber ON #temp(VehicleID, VehicleRowNumber)
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_TimeStamp ON #temp(VehicleID, TimeStamp) INCLUDE (WSP)

	UPDATE t1 SET
		WspChange = CASE
			WHEN t1.WSP = 1 AND t2.WSP = 0
				THEN 1
			ELSE 0
		END
	FROM #temp t1
	INNER JOIN #temp t2 ON t1.UnitID = t2.UnitID 
		AND t1.UnitRowNumber = t2.UnitRowNumber + 1

	UPDATE t1 SET
		[EventEndTimestamp] = (SELECT MIN(t2.Timestamp) FROM #temp t2 WHERE t1.UnitID = t2.UnitID AND t2.TimeStamp > t1.TimeStamp AND WSP = 0)
	FROM #temp t1
	WHERE WspChange = 1
	
	INSERT INTO [dbo].[ReportAdhesion]
	(	
		[ID]
		, [VehicleID]
		, [TimeStamp]
		, [Value]
		, [EventType]
		, [Latitude]
		, [Longitude]
		, [GCourse]
		, [Duration]
		, [EventEndTimestamp]
	)
	SELECT
		t.[ID]
		, v.[VehicleID]
		, t.[TimeStamp]
		, 1				--[Value]
		, 1				--[EventType] = 1 for WSP
		, t.Col1			--[Latitude]
		, t.Col2			--[Longitude]
		, 0				--[GCourse]
		, DATEDIFF(ms, Timestamp, EventEndTimestamp) / 1000.0 --[Duration]
		, [EventEndTimestamp]
	FROM #temp t
	JOIN dbo.Vehicle v ON v.UnitID = t.UnitID
	WHERE WspChange = 1
		AND ID NOT IN (SELECT ID FROM dbo.ReportAdhesion)


GO
/****** Object:  StoredProcedure [dbo].[NSP_UpdateVehicleDownloadStatus]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_UpdateVehicleDownloadStatus
**	Description:	Analysis all events received from:
**					RMD (EventCategory 24)
**					PL (EventCategory 255)
**					Service updatning SQLiteDB (EventCategory 1001)
**					TMS File Processing interface (EventCategory 1000)
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

CREATE PROCEDURE [dbo].[NSP_UpdateVehicleDownloadStatus]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @vehicleDownloadProgressID int
	DECLARE @eventCategory int
	DECLARE @eventCode int
	DECLARE @vehicleID int
	DECLARE @vehicleDownloadID int
	DECLARE @vehicleDownloadStatus int
	
	WHILE 1=1
	BEGIN
		---------------------------------------------------------------------------------------

		SET @vehicleDownloadProgressID = (SELECT TOP 1 ID FROM VehicleDownloadProgress WHERE IsProcessed = 0 ORDER BY RecordInsert ASC)

		SET @eventCategory = NULL
		SET @eventCode = NULL
		SET @vehicleID = NULL
		SET @vehicleDownloadStatus = NULL
		SET @vehicleDownloadID = NULL
		
		SELECT
			@vehicleID = VehicleID
			, @eventCategory = EventCategory
			, @eventCode = EventCode
		FROM VehicleDownloadProgress
		WHERE ID = @vehicleDownloadProgressID

		IF @vehicleDownloadProgressID IS NULL 
			BREAK
			
		-- Find VehicleDownload.ID for vehicle that is not Completed or Failed
		-- VehicleDownload Status:
		-- 1 - Requested
		-- 2 - In Progress
		-- 0 - Completed
		-- 3 - Failed
		SELECT 
			@vehicleDownloadID = ID 
			, @vehicleDownloadStatus = Status
		FROM VehicleDownload 
		WHERE VehicleID = @vehicleID
			AND Status <> 0 -- Comlpeted
			AND Status <> 3 -- Failed

			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
					
		IF @vehicleDownloadID IS NOT NULL
			-- or Completed download 
			OR (@eventCategory = 1000 AND @eventCode = 0)

		BEGIN
		
			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
			
			-- @eventCategory = 24 - RMD Events
			------------------------------------------
			-- Ignored statuses:
			-- 0 Connected to TMS
			-- 3 Auto download request

			-- Failure statuses:
			-- 1 Connect timed out
			-- 5 Download timed out	Failed (RMD)
			-- 6 Download failed	Failed (RMD)
			-- 7 Got a download command and TMS log not enabled	Failed (RMD)
			-- 8 ATMS Buffer Overrun	Failed (RMD)
			IF @eventCategory = 24 AND @eventCode IN (1,5,6,7,8)
				UPDATE VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID
	
			-- 2 Manual download request	Requested
			-- 4 Download completed ok	Confirmed
			IF @eventCategory = 24 AND @eventCode IN (2, 4)
				UPDATE VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			
			-- @eventCategory = 255 - PL Events
			------------------------------------------
			-- 32 File upload failed: CRC Error Failed (PL)
			-- 33 File upload failed: Timeout	Failed (PL)
			-- 34 File upload failed: Aborted	Failed (PL)
			-- 35 File upload failed: Unknown	Failed (PL)
			IF @eventCategory = 255 AND @eventCode IN (32,33,34,35,36)
				UPDATE VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID

			-- 36 File transferred OK	Transferred
			IF @eventCategory = 255 AND @eventCode IN (36)
				UPDATE VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1000 - File Loader Events
			------------------------------------------
			-- 0 File Imported Successfully (User Requested Download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NOT NULL
				UPDATE VehicleDownload SET
					Status = 0	-- Completed
					, DateTimeCompleted = SYSDATETIME()
				WHERE ID = @vehicleDownloadID

			-- 0 File Imported Successfully (Scheduled download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NULL
			BEGIN
				PRINT 'inserted @vehicleDownloadID:'
			
				SET @vehicleDownloadStatus = -1 --current status -1 as not existing

				INSERT INTO dbo.VehicleDownload
					(VehicleID
					,Status
					,DateTimeRequested
					,DateTimeCompleted
					,IsManual
					,FileType)
				SELECT
					VehicleID			= @vehicleID
					,Status				= 0
					,DateTimeRequested	= SYSDATETIME()
					,DateTimeCompleted	= SYSDATETIME()
					,IsManual			= 0
					,FileType			= 6

				SET @vehicleDownloadID = SCOPE_IDENTITY()
				
				
				PRINT @vehicleDownloadID
				
			END
			
			-- 1 File Imported Failed
			IF @eventCategory = 1000 AND @eventCode IN (1)
				UPDATE VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1001 - Scheduling Service Events
			------------------------------------------
			-- 0 Flag updated in SQLite DB successfully
			IF @eventCategory = 1001 AND @eventCode IN (0)
				UPDATE VehicleDownload SET
					Status = 2 -- In Progressd
				WHERE ID = @vehicleDownloadID

			-- 1 Flag updated in SQLite DB failed
			IF @eventCategory = 1001 AND @eventCode IN (1)
				UPDATE VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID
		END

		
		-- Flag file as processed
		UPDATE VehicleDownloadProgress SET 
			IsProcessed = 1
			, VehicleDownloadID = @vehicleDownloadID
			, StatusUpdated = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN 1
				ELSE 0
			END
			, StatusUpdatedDatetime = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN SYSDATETIME()
				ELSE NULL
			END
		WHERE ID = @vehicleDownloadProgressID
		
	END
END


GO
/****** Object:  StoredProcedure [dbo].[NSP_DataArchiving_MASTER]    Script Date: 15/07/2016 09:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NSP_DataArchiving_MASTER]
(
	@StopDate as date
)
AS
BEGIN

	IF EXISTS (SELECT 1 from $(db_name)_Archive..ChannelValue_Archive) BEGIN
		INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]1 ',0,0,'Failure')
		RAISERROR('ChannelValue_Archive Has data',18,1)
	END

	DECLARE @retbit bit 

	EXEC @retbit = $(db_name).dbo.NSP_DataArchiving @StopDate 

	if @retbit = 1 BEGIN
		INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]2',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchiving',18,1)
	END

	EXEC @retbit = NSP_DataArchivingBackupAndTruncate
	
	if @retbit = 1 BEGIN
		INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]3',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchivingBackupAndTruncate',18,1)
	END

	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]',0,0,'Sucess')

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**	Name:			NSP_InsertChannelValue
**	Description:	
**	Call frequency:	
**	Parameters:		none
**	Return values:	
*******************************************************************************
** 
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/
CREATE PROCEDURE [dbo].[NSP_InsertChannelValue](
	@Timestamp datetime2(3)
	, @UnitID int
	, @UpdateRecord bit = NULL
	, @IsComplete bit = 1,
		@Col1 decimal (9,6) = NULL, @Col2 decimal (9,6) = NULL, @Col3 bit = NULL, @Col4 bit = NULL, @Col5 bit = NULL, @Col6 bit = NULL, @Col7 bit = NULL, 
		@Col8 bit = NULL, @Col9 bit = NULL, @Col10 bit = NULL, @Col11 bit = NULL, @Col12 bit = NULL, @Col13 bit = NULL, @Col14 bit = NULL, @Col15 bit = NULL, 
		@Col16 bit = NULL, @Col17 bit = NULL, @Col18 bit = NULL, @Col19 bit = NULL, @Col20 bit = NULL, @Col21 bit = NULL, @Col22 bit = NULL, @Col23 bit = NULL, 
		@Col24 bit = NULL, @Col25 bit = NULL, @Col26 bit = NULL, @Col27 bit = NULL, @Col28 bit = NULL, @Col29 bit = NULL, @Col30 bit = NULL, @Col31 bit = NULL, 
		@Col32 bit = NULL, @Col33 bit = NULL, @Col34 bit = NULL, @Col35 bit = NULL, @Col36 bit = NULL, @Col37 bit = NULL, @Col38 bit = NULL, @Col39 bit = NULL, 
		@Col40 bit = NULL, @Col41 bit = NULL, @Col42 bit = NULL, @Col43 bit = NULL, @Col44 bit = NULL, @Col45 bit = NULL, @Col46 bit = NULL, @Col47 bit = NULL, 
		@Col48 bit = NULL, @Col49 bit = NULL, @Col50 bit = NULL, @Col51 bit = NULL, @Col52 bit = NULL, @Col53 bit = NULL, @Col54 bit = NULL, @Col55 bit = NULL, 
		@Col56 bit = NULL, @Col57 bit = NULL, @Col58 bit = NULL, @Col59 bit = NULL, @Col60 bit = NULL, @Col61 bit = NULL, @Col62 bit = NULL, @Col63 bit = NULL, 
		@Col64 bit = NULL, @Col65 bit = NULL, @Col66 bit = NULL, @Col67 bit = NULL, @Col68 bit = NULL, @Col69 bit = NULL, @Col70 bit = NULL, @Col71 bit = NULL, 
		@Col72 bit = NULL, @Col73 bit = NULL, @Col74 bit = NULL, @Col75 bit = NULL, @Col76 bit = NULL, @Col77 bit = NULL, @Col78 bit = NULL, @Col79 bit = NULL, 
		@Col80 bit = NULL, @Col81 bit = NULL, @Col82 bit = NULL, @Col83 bit = NULL, @Col84 bit = NULL, @Col85 bit = NULL, @Col86 bit = NULL, @Col87 bit = NULL, 
		@Col88 bit = NULL, @Col89 bit = NULL, @Col90 bit = NULL, @Col91 bit = NULL, @Col92 bit = NULL, @Col93 bit = NULL, @Col94 bit = NULL, @Col95 bit = NULL, 
		@Col96 bit = NULL, @Col97 bit = NULL, @Col98 bit = NULL, @Col99 bit = NULL, @Col100 bit = NULL, @Col101 bit = NULL, @Col102 bit = NULL, @Col103 bit = NULL, 
		@Col104 bit = NULL, @Col105 bit = NULL, @Col106 bit = NULL, @Col107 bit = NULL, @Col108 bit = NULL, @Col109 bit = NULL, @Col110 bit = NULL, @Col111 bit = NULL, 
		@Col112 bit = NULL, @Col113 bit = NULL, @Col114 bit = NULL, @Col115 bit = NULL, @Col116 bit = NULL, @Col117 bit = NULL, @Col118 bit = NULL, @Col119 bit = NULL, 
		@Col120 bit = NULL, @Col121 bit = NULL, @Col122 bit = NULL, @Col123 bit = NULL, @Col124 bit = NULL, @Col125 bit = NULL, @Col126 bit = NULL, @Col127 bit = NULL, 
		@Col128 bit = NULL, @Col129 bit = NULL, @Col130 bit = NULL, @Col131 bit = NULL, @Col132 bit = NULL, @Col133 bit = NULL, @Col134 bit = NULL, @Col135 bit = NULL, 
		@Col136 bit = NULL, @Col137 bit = NULL, @Col138 bit = NULL, @Col139 bit = NULL, @Col140 bit = NULL, @Col141 bit = NULL, @Col142 bit = NULL, @Col143 bit = NULL, 
		@Col144 bit = NULL, @Col145 bit = NULL, @Col146 bit = NULL, @Col147 bit = NULL, @Col148 bit = NULL, @Col149 bit = NULL, @Col150 bit = NULL, @Col151 bit = NULL, 
		@Col152 bit = NULL, @Col153 bit = NULL, @Col154 bit = NULL, @Col155 bit = NULL, @Col156 bit = NULL, @Col157 bit = NULL, @Col158 bit = NULL, @Col159 bit = NULL, 
		@Col160 bit = NULL, @Col161 bit = NULL, @Col162 bit = NULL, @Col163 bit = NULL, @Col164 bit = NULL, @Col165 bit = NULL, @Col166 bit = NULL, @Col167 bit = NULL, 
		@Col168 bit = NULL, @Col169 bit = NULL, @Col170 bit = NULL, @Col171 bit = NULL, @Col172 bit = NULL, @Col173 bit = NULL, @Col174 bit = NULL, @Col175 bit = NULL, 
		@Col176 bit = NULL, @Col177 bit = NULL, @Col178 bit = NULL, @Col179 bit = NULL, @Col180 bit = NULL, @Col181 bit = NULL, @Col182 bit = NULL, @Col183 bit = NULL, 
		@Col184 bit = NULL, @Col185 bit = NULL, @Col186 bit = NULL, @Col187 bit = NULL, @Col188 bit = NULL, @Col189 bit = NULL, @Col190 bit = NULL, @Col191 bit = NULL, 
		@Col192 bit = NULL, @Col193 bit = NULL, @Col194 bit = NULL, @Col195 bit = NULL, @Col196 bit = NULL, @Col197 bit = NULL, @Col198 bit = NULL, @Col199 bit = NULL, 
		@Col200 bit = NULL, @Col201 bit = NULL, @Col202 bit = NULL, @Col203 bit = NULL, @Col204 bit = NULL, @Col205 bit = NULL, @Col206 bit = NULL, @Col207 bit = NULL, 
		@Col208 bit = NULL, @Col209 bit = NULL, @Col210 bit = NULL, @Col211 bit = NULL, @Col212 bit = NULL, @Col213 bit = NULL, @Col214 bit = NULL, @Col215 bit = NULL, 
		@Col216 bit = NULL, @Col217 bit = NULL, @Col218 bit = NULL, @Col219 bit = NULL, @Col220 bit = NULL, @Col221 bit = NULL, @Col222 bit = NULL, @Col223 bit = NULL, 
		@Col224 bit = NULL, @Col225 bit = NULL, @Col226 bit = NULL, @Col227 bit = NULL, @Col228 bit = NULL, @Col229 bit = NULL, @Col230 bit = NULL, @Col231 bit = NULL, 
		@Col232 bit = NULL, @Col233 bit = NULL, @Col234 bit = NULL, @Col235 bit = NULL, @Col236 bit = NULL, @Col237 bit = NULL, @Col238 bit = NULL, @Col239 bit = NULL, 
		@Col240 bit = NULL, @Col241 bit = NULL, @Col242 bit = NULL, @Col243 bit = NULL, @Col244 bit = NULL, @Col245 bit = NULL, @Col246 bit = NULL, @Col247 bit = NULL, 
		@Col248 bit = NULL, @Col249 bit = NULL, @Col250 bit = NULL, @Col251 bit = NULL, @Col252 bit = NULL, @Col253 bit = NULL, @Col254 bit = NULL, @Col255 bit = NULL, 
		@Col256 bit = NULL, @Col257 bit = NULL, @Col258 bit = NULL, @Col259 bit = NULL, @Col260 bit = NULL, @Col261 bit = NULL, @Col262 bit = NULL, @Col263 bit = NULL, 
		@Col264 bit = NULL, @Col265 bit = NULL, @Col266 bit = NULL, @Col267 bit = NULL, @Col268 bit = NULL, @Col269 bit = NULL, @Col270 bit = NULL, @Col271 bit = NULL, 
		@Col272 bit = NULL, @Col273 bit = NULL, @Col274 bit = NULL, @Col275 bit = NULL, @Col276 bit = NULL, @Col277 bit = NULL, @Col278 bit = NULL, @Col279 bit = NULL, 
		@Col280 bit = NULL, @Col281 bit = NULL, @Col282 bit = NULL, @Col283 bit = NULL, @Col284 bit = NULL, @Col285 bit = NULL, @Col286 bit = NULL, @Col287 bit = NULL, 
		@Col288 bit = NULL, @Col289 bit = NULL, @Col290 bit = NULL, @Col291 bit = NULL, @Col292 bit = NULL, @Col293 bit = NULL, @Col294 bit = NULL, @Col295 bit = NULL, 
		@Col296 bit = NULL, @Col297 bit = NULL, @Col298 bit = NULL, @Col299 bit = NULL, @Col300 bit = NULL, @Col301 bit = NULL, @Col302 bit = NULL, @Col303 bit = NULL, 
		@Col304 bit = NULL, @Col305 bit = NULL, @Col306 bit = NULL, @Col307 bit = NULL, @Col308 bit = NULL, @Col309 bit = NULL, @Col310 bit = NULL, @Col311 bit = NULL, 
		@Col312 bit = NULL, @Col313 bit = NULL, @Col314 bit = NULL, @Col315 bit = NULL, @Col316 bit = NULL, @Col317 bit = NULL, @Col318 bit = NULL, @Col319 bit = NULL, 
		@Col320 bit = NULL, @Col321 bit = NULL, @Col322 bit = NULL, @Col323 bit = NULL, @Col324 bit = NULL, @Col325 bit = NULL, @Col326 bit = NULL, @Col327 bit = NULL, 
		@Col328 bit = NULL, @Col329 bit = NULL, @Col330 bit = NULL, @Col331 bit = NULL, @Col332 bit = NULL, @Col333 bit = NULL, @Col334 bit = NULL, @Col335 bit = NULL, 
		@Col336 bit = NULL, @Col337 bit = NULL, @Col338 bit = NULL, @Col339 bit = NULL, @Col340 bit = NULL, @Col341 bit = NULL, @Col342 bit = NULL, @Col343 bit = NULL, 
		@Col344 bit = NULL, @Col345 bit = NULL, @Col346 bit = NULL, @Col347 bit = NULL, @Col348 bit = NULL, @Col349 bit = NULL, @Col350 bit = NULL, @Col351 bit = NULL, 
		@Col352 bit = NULL, @Col353 bit = NULL, @Col354 bit = NULL, @Col355 bit = NULL, @Col356 bit = NULL, @Col357 bit = NULL, @Col358 bit = NULL, @Col359 bit = NULL, 
		@Col360 bit = NULL, @Col361 bit = NULL, @Col362 bit = NULL, @Col363 bit = NULL, @Col364 bit = NULL, @Col365 bit = NULL, @Col366 bit = NULL, @Col367 bit = NULL, 
		@Col368 bit = NULL, @Col369 bit = NULL, @Col370 bit = NULL, @Col371 bit = NULL, @Col372 bit = NULL, @Col373 bit = NULL, @Col374 bit = NULL, @Col375 bit = NULL, 
		@Col376 bit = NULL, @Col377 bit = NULL, @Col378 bit = NULL, @Col379 bit = NULL, @Col380 bit = NULL, @Col381 bit = NULL, @Col382 bit = NULL, @Col383 bit = NULL, 
		@Col384 bit = NULL, @Col385 bit = NULL, @Col386 bit = NULL, @Col387 bit = NULL, @Col388 bit = NULL, @Col389 bit = NULL, @Col390 bit = NULL, @Col391 bit = NULL, 
		@Col392 bit = NULL, @Col393 bit = NULL, @Col394 bit = NULL, @Col395 bit = NULL, @Col396 bit = NULL, @Col397 bit = NULL, @Col398 bit = NULL, @Col399 bit = NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelValue_ID BIGINT;

	-- insert into ChannelValue
	INSERT INTO dbo.ChannelValue (
		[Timestamp]
		, [UnitID]
		, [UpdateRecord]
		, [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	) 
	SELECT
		@Timestamp
		, @UnitID
		, @UpdateRecord,
			@Col1, @Col2, @Col3, @Col4, @Col5, @Col6, @Col7, @Col8, @Col9, @Col10, @Col11, @Col12, @Col13, @Col14, @Col15, @Col16, @Col17, @Col18, @Col19, @Col20, 
			@Col21, @Col22, @Col23, @Col24, @Col25, @Col26, @Col27, @Col28, @Col29, @Col30, @Col31, @Col32, @Col33, @Col34, @Col35, @Col36, @Col37, @Col38, @Col39, 
			@Col40, @Col41, @Col42, @Col43, @Col44, @Col45, @Col46, @Col47, @Col48, @Col49, @Col50, @Col51, @Col52, @Col53, @Col54, @Col55, @Col56, @Col57, @Col58, 
			@Col59, @Col60, @Col61, @Col62, @Col63, @Col64, @Col65, @Col66, @Col67, @Col68, @Col69, @Col70, @Col71, @Col72, @Col73, @Col74, @Col75, @Col76, @Col77, 
			@Col78, @Col79, @Col80, @Col81, @Col82, @Col83, @Col84, @Col85, @Col86, @Col87, @Col88, @Col89, @Col90, @Col91, @Col92, @Col93, @Col94, @Col95, @Col96, 
			@Col97, @Col98, @Col99, @Col100, @Col101, @Col102, @Col103, @Col104, @Col105, @Col106, @Col107, @Col108, @Col109, @Col110, @Col111, @Col112, @Col113, 
			@Col114, @Col115, @Col116, @Col117, @Col118, @Col119, @Col120, @Col121, @Col122, @Col123, @Col124, @Col125, @Col126, @Col127, @Col128, @Col129, @Col130, 
			@Col131, @Col132, @Col133, @Col134, @Col135, @Col136, @Col137, @Col138, @Col139, @Col140, @Col141, @Col142, @Col143, @Col144, @Col145, @Col146, @Col147, 
			@Col148, @Col149, @Col150, @Col151, @Col152, @Col153, @Col154, @Col155, @Col156, @Col157, @Col158, @Col159, @Col160, @Col161, @Col162, @Col163, @Col164, 
			@Col165, @Col166, @Col167, @Col168, @Col169, @Col170, @Col171, @Col172, @Col173, @Col174, @Col175, @Col176, @Col177, @Col178, @Col179, @Col180, @Col181, 
			@Col182, @Col183, @Col184, @Col185, @Col186, @Col187, @Col188, @Col189, @Col190, @Col191, @Col192, @Col193, @Col194, @Col195, @Col196, @Col197, @Col198, 
			@Col199, @Col200, @Col201, @Col202, @Col203, @Col204, @Col205, @Col206, @Col207, @Col208, @Col209, @Col210, @Col211, @Col212, @Col213, @Col214, @Col215, 
			@Col216, @Col217, @Col218, @Col219, @Col220, @Col221, @Col222, @Col223, @Col224, @Col225, @Col226, @Col227, @Col228, @Col229, @Col230, @Col231, @Col232, 
			@Col233, @Col234, @Col235, @Col236, @Col237, @Col238, @Col239, @Col240, @Col241, @Col242, @Col243, @Col244, @Col245, @Col246, @Col247, @Col248, @Col249, 
			@Col250, @Col251, @Col252, @Col253, @Col254, @Col255, @Col256, @Col257, @Col258, @Col259, @Col260, @Col261, @Col262, @Col263, @Col264, @Col265, @Col266, 
			@Col267, @Col268, @Col269, @Col270, @Col271, @Col272, @Col273, @Col274, @Col275, @Col276, @Col277, @Col278, @Col279, @Col280, @Col281, @Col282, @Col283, 
			@Col284, @Col285, @Col286, @Col287, @Col288, @Col289, @Col290, @Col291, @Col292, @Col293, @Col294, @Col295, @Col296, @Col297, @Col298, @Col299, @Col300, 
			@Col301, @Col302, @Col303, @Col304, @Col305, @Col306, @Col307, @Col308, @Col309, @Col310, @Col311, @Col312, @Col313, @Col314, @Col315, @Col316, @Col317, 
			@Col318, @Col319, @Col320, @Col321, @Col322, @Col323, @Col324, @Col325, @Col326, @Col327, @Col328, @Col329, @Col330, @Col331, @Col332, @Col333, @Col334, 
			@Col335, @Col336, @Col337, @Col338, @Col339, @Col340, @Col341, @Col342, @Col343, @Col344, @Col345, @Col346, @Col347, @Col348, @Col349, @Col350, @Col351, 
			@Col352, @Col353, @Col354, @Col355, @Col356, @Col357, @Col358, @Col359, @Col360, @Col361, @Col362, @Col363, @Col364, @Col365, @Col366, @Col367, @Col368, 
			@Col369, @Col370, @Col371, @Col372, @Col373, @Col374, @Col375, @Col376, @Col377, @Col378, @Col379, @Col380, @Col381, @Col382, @Col383, @Col384, @Col385, 
			@Col386, @Col387, @Col388, @Col389, @Col390, @Col391, @Col392, @Col393, @Col394, @Col395, @Col396, @Col397, @Col398, @Col399

		SELECT @ChannelValue_ID = SCOPE_IDENTITY();
	
	
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,005
           ,'update_spectrum_db_005.sql'
           ,'1.0.01')
GO