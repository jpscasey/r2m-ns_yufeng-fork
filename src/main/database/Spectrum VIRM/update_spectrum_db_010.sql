SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 010
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO
------------------------------------------------------------
-- update script
------------------------------------------------------------

-------------------------------------------------------------------------------
RAISERROR ('-- alter procedure dbo.NSP_GetFaultChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 

	EXEC ('CREATE PROCEDURE [dbo].[NSP_GetFaultChannelValueByID] AS BEGIN RETURN(1) END;')
GO


ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
	@ID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM ChannelValue), 0) - 1;
	ELSE
		SET @ID = (
		SELECT TOP 1 (ChannelValue.ID - 1) 
		FROM ChannelValue WITH(NOLOCK)
		WHERE ChannelValue.ID > @ID
		ORDER BY ChannelValue.ID ASC
		)
		
	DECLARE @dataSetSize int = 5000
	
	SELECT 
		cv.ID
		, UnitID		= cv.UnitID
		, VehicleNumber	= v.VehicleNumber
		, VehicleType	= v.Type
		, VehicleTypeID	= v.VehicleTypeID
		, Headcode		= fs.Headcode
		, UnitID		= v.UnitID 
		, UnitNumber	= v.UnitNumber
		, UnitType		= v.UnitType
		, FleetCode		= v.FleetCode
		, ServiceStatus	= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, TimeStamp			= CONVERT(datetime, TimeStamp)
		, LocationID	= l.ID
		, Tiploc		= l.Tiploc
		, LocationName	= l.LocationName ,

		-- Channel Data	
		  Col1,Col2,Col3,Col4,Col5,Col6,Col7,Col8,Col9,Col10,Col11,Col12,Col13,Col14,Col15,Col16,Col17,Col18,Col19,Col20,Col21,Col22,Col23,Col24,Col25,
		  Col26,Col27,Col28,Col29,Col30,Col31,Col32,Col33,Col34,Col35,Col36,Col37,Col38,Col39,Col40,Col41,Col42,Col43,Col44,Col45,Col46,Col47,Col48,Col49,
		  Col50,Col51,Col52,Col53,Col54,Col55,Col56,Col57,Col58,Col59,Col60,Col61,Col62,Col63,Col64,Col65,Col66,Col67,Col68,Col69,Col70,Col71,Col72,Col73,
		  Col74,Col75,Col76,Col77,Col78,Col79,Col80,Col81,Col82,Col83,Col84,Col85,Col86,Col87,Col88,Col89,Col90,Col91,Col92,Col93,Col94,Col95,Col96,Col97
	FROM ChannelValue cv WITH(INDEX(PK_ChannelValue))
	INNER JOIN VW_Vehicle v ON cv.UnitID = v.UnitID
	LEFT JOIN FleetStatusHistory fs ON
		v.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
	LEFT JOIN Location l ON l.ID = (
		SELECT TOP 1 LocationID 
		FROM LocationArea 
		WHERE col3 BETWEEN MinLatitude AND MaxLatitude
			AND col4 BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
	ORDER BY ID

END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,010
           ,'update_spectrum_db_010.sql'
           ,'1.0.01')
GO
