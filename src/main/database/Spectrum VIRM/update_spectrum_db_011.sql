SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 011
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
-- SB 2.12.07 - Fault Configuration - Store username information when a fault configuration is saved
------------------------------------------------------------
--------------------------------------------
-- ADD NEW FIELD Username TO FaultMeta
--------------------------------------------
RAISERROR ('-- Add new field Username to FaultMeta', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM SYS.columns WHERE NAME='Username' AND OBJECT_ID = OBJECT_ID('dbo.FaultMeta') )
	ALTER TABLE dbo.FaultMeta ADD Username VARCHAR(3700) COLLATE Latin1_General_CI_AS	NULL
GO

--------------------------------------------
-- UPDATING PROCEDURE NSP_InsertFaultMeta
--------------------------------------------
RAISERROR ('-- alter procedure dbo.NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 
    EXEC ('CREATE PROCEDURE [dbo].[NSP_InsertFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROC [dbo].[NSP_InsertFaultMeta](
    @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
BEGIN

    SET NOCOUNT ON;
    
    INSERT dbo.FaultMeta(
        Username
        , FaultCode
        , Description
        , Summary
		, AdditionalInfo
        , Url
        , CategoryID
        , RecoveryProcessPath
        , FaultTypeID
        , GroupID
    )
    SELECT 
        Username      = @Username
        , FaultCode      = @FaultCode
        , Description   = @Description 
        , Summary       = @Summary
		, AdditionalInfo = @AdditionalInfo
        , Url           = @Url
        , CategoryID    = @CategoryID
        , RecoveryProcessPath   = @RecoveryProcessPath
        , FaultTypeID   = @FaultTypeID
        , GroupID       = @GroupID
        
    SELECT SCOPE_IDENTITY() AS InsertedID

END
GO

--------------------------------------------
-- UPDATING PROCEDURE NSP_UpdateFaultMeta
--------------------------------------------
RAISERROR ('-- alter procedure dbo.NSP_UpdateFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 
    EXEC ('CREATE PROCEDURE [dbo].[NSP_UpdateFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta](
    @ID int
    , @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
    BEGIN
        
        SET NOCOUNT ON;

        BEGIN TRAN
        BEGIN TRY

            UPDATE dbo.FaultMeta SET
                Username        = @Username
                , FaultCode     = @FaultCode
                , Description   = @Description 
                , Summary       = @Summary
                , AdditionalInfo = @AdditionalInfo
                , Url           = @Url
                , CategoryID    = @CategoryID
                , RecoveryProcessPath   = @RecoveryProcessPath
                , FaultTypeID   = @FaultTypeID
                , GroupID       = @GroupID
            WHERE ID = @ID

            COMMIT TRAN
        
        END TRY
        BEGIN CATCH

            EXEC NSP_RethrowError;
            ROLLBACK TRAN;

        END CATCH

    END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,011
           ,'update_spectrum_db_011.sql'
           ,'1.0.01')
GO
