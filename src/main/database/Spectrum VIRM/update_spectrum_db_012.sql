SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 012
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
-- Bug 27243 - SB 2.12.08 - Add field LastUpdateTime to RuleEngineCurrentId table and update it with the procedure
------------------------------------------------------------
--------------------------------------------
-- ADD NEW FIELD LastUpdateTime TO RuleEngineCurrentId
--------------------------------------------
RAISERROR ('-- Add new field LastUpdateTime to RuleEngineCurrentId', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM SYS.columns WHERE NAME='LastUpdateTime' AND OBJECT_ID = OBJECT_ID('dbo.RuleEngineCurrentId') )
    ALTER TABLE dbo.RuleEngineCurrentId ADD [LastUpdateTime] DATETIME2(3)
GO
--------------------------------------------
-- UPDATE PROCEDURE NSP_RuleEngineUpdateCurrentIds
--------------------------------------------
RAISERROR ('-- Updating procedure dbo.NSP_RuleEngineUpdateCurrentIds', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RuleEngineUpdateCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_RuleEngineUpdateCurrentIds AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_RuleEngineUpdateCurrentIds
**	Description:	Returns data to fault engine
**	Call frequency:	Every 5s
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_RuleEngineUpdateCurrentIds]
	@ConfigurationName varchar(100),
	@Value bigint = NULL

AS
BEGIN

    DECLARE @LastUpdateTime datetime;
    SET @LastUpdateTime = GETDATE()

	;MERGE RuleEngineCurrentId AS r
    USING (

    	SELECT Position = '1', ConfigurationName = @ConfigurationName, Value = @Value, LastUpdateTime = @LastUpdateTime

    ) AS nr
    ON r.Position = nr.Position
    	AND r.ConfigurationName = nr.ConfigurationName
    WHEN MATCHED THEN
    UPDATE SET
    	Value = nr.Value,
        LastUpdateTime = nr.LastUpdateTime
    WHEN NOT MATCHED THEN
    INSERT (
    	ConfigurationName
    	, Position
    	, Value
        , LastUpdateTime)
    VALUES(
    	nr.ConfigurationName
    	, nr.Position
    	, nr.Value
        , nr.LastUpdateTime);

END

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,012
           ,'update_spectrum_db_012.sql'
           ,'1.0.01')
GO
