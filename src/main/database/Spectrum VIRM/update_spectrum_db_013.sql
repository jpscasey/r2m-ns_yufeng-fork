SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 013
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
-- Bug 27244 - SB 2.12.08 - Event Analysis - Stored procedures need to support lists of items
------------------------------------------------------------
--------------------------------------------
-- UPDATE PROCEDURE NSP_EaSearchFaultMeta
--------------------------------------------
RAISERROR ('--Updating procedure NSP_EaSearchFaultMeta', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_EaSearchFaultMeta AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_EaSearchFaultMeta
**  Description:
**  Call frequency: Called from Ops Analysis
**  Parameters:
**  Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/
ALTER PROCEDURE [dbo].[NSP_EaSearchFaultMeta]
(
    @FaultString varchar(50)
    , @FaultCategoryID varchar(500)
    , @FaultTypeID varchar(50)
)
AS
BEGIN

    DECLARE @FaultCategoryTable TABLE (FaultCategoryID varchar(10))
    INSERT INTO @FaultCategoryTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultCategoryID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeId varchar(10))
    INSERT INTO @FaultTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultTypeId,',')

    IF @FaultCategoryID = '' SET @FaultCategoryID = NULL
    IF @FaultTypeId = '' SET @FaultTypeId = NULL


	SELECT
        FaultID     = fm.ID
        , FaultDesc = fm.Description
        , FaultCode
    FROM FaultMeta fm
    INNER JOIN FaultType ft ON ft.ID = fm.FaultTypeID
    INNER JOIN FaultCategory fc ON fc.ID = fm.CategoryID
    WHERE
        ft.ID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeId IS NULL
        AND fc.ID IN (SELECT * FROM @FaultCategoryTable) OR @FaultCategoryId IS NULL
        AND
        (
            Description LIKE '%' +  @FaultString + '%'
            OR
            FaultCode LIKE '%' +  @FaultString + '%'
        )

END
GO

--------------------------------------------
-- UPDATE PROCEDURE NSP_EaReport
--------------------------------------------
RAISERROR ('--Updating procedure NSP_EaReport', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaReport' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_EaReport AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_EaReport
**  Description:
**  Call frequency: Called from Event Analysis
**  Parameters:
**  Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaReport]
(
    @FleetCode varchar(10)
    , @DateFrom datetime
    , @DateTo datetime
    -- additional parameters: need to be in alphabetical order
    , @CategoryID varchar(100)
    , @FaultMetaID varchar(max)
    , @FaultTypeID varchar(50)
    , @Headcode varchar(10)
    , @LocationID varchar(max)
    , @UnitID varchar(10)
    , @VehicleID varchar(max)
    , @VehicleType varchar(10)
)
AS
BEGIN

    DECLARE @FaultCategoryTable TABLE (CategoryID varchar(10))
    INSERT INTO @FaultCategoryTable
    SELECT * from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultMetaTable TABLE (FaultMetaID varchar(10))
    INSERT INTO @FaultMetaTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultMetaID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeID varchar(10))
    INSERT INTO @FaultTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@FaultTypeID,',')

    DECLARE @HeadcodeTable TABLE (Headcode varchar(10))
    INSERT INTO @HeadcodeTable
    SELECT * from [dbo].SplitStrings_CTE(@Headcode,',')

    DECLARE @LocationTable TABLE (LocationID varchar(10))
    INSERT INTO @LocationTable
    SELECT * from [dbo].SplitStrings_CTE(@LocationID,',')

    DECLARE @UnitIDTable TABLE (UnitID varchar(10))
    INSERT INTO @UnitIDTable
    SELECT * from [dbo].SplitStrings_CTE(@UnitID,',')

    DECLARE @VehicleIDTable TABLE (VehicleID varchar(100))
    INSERT INTO @VehicleIDTable
    SELECT * from [dbo].SplitStrings_CTE(@VehicleID,',')

    DECLARE @VehicleTypeTable TABLE (VehicleType varchar(100))
    INSERT INTO @VehicleTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@VehicleType,',')

    IF @CategoryID = '' SET @CategoryID = NULL
    IF @FaultMetaID = '' SET @FaultMetaID = NULL
    IF @FaultTypeID = '' SET @FaultTypeID = NULL
    IF @Headcode = '' SET @Headcode = NULL
    IF @LocationID = '' SET @LocationID = NULL
    IF @VehicleID = '' SET @VehicleID = NULL
    IF @UnitID = '' SET @UnitID = NULL
    IF @VehicleType = '' SET @VehicleType = NULL

    DECLARE @VehicleList TABLE (ID int)
    DECLARE @FaultMetaList TABLE (ID int)

    INSERT INTO @VehicleList
    SELECT ID
    FROM Vehicle
    WHERE (ID IN (SELECT * FROM @VehicleIDTable) OR @VehicleID IS NULL OR (@VehicleID = '-1' AND ID IS NULL))
        AND (Type IN (SELECT * FROM @VehicleTypeTable) OR @VehicleType IS NULL OR (@VehicleType = '-1' AND Type IS NULL))
        AND (UnitID  IN (SELECT * FROM @UnitIDTable) OR @UnitID IS NULL OR (@UnitID = '-1' AND Type IS NULL))

    INSERT INTO @FaultMetaList
    SELECT ID
    FROM FaultMeta
    WHERE (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultTypeID = '-1' AND FaultTypeID IS NULL))
        AND (ID IN (SELECT * FROM @FaultMetaTable) OR @FaultMetaID IS NULL OR (@FaultMetaID = '-1' AND ID IS NULL))
        AND (CategoryID IN (SELECT * FROM @FaultCategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))

    SELECT
        FaultID = f.ID
        , CreateTime
        , EndTime
        , IsCurrent

        , HeadCode
        , SetCode
        --, FleetCode
        --, UnitNumber
        , UnitID    = v.UnitID
        , FaultUnitID
        , FaultVehicleNumber = v.VehicleNumber

        , FaultMetaID
        , FaultCode
        , Description

        , FaultTypeID
        , FaultType
        , FaultTypeColor
        , Priority

        , CategoryID
        , Category

        , Latitude
        , Longitude
        , LocationID
        , LocationCode

    FROM VW_IX_Fault f (NOEXPAND)
    INNER JOIN Vehicle v ON v.UnitID = FaultUnitID
    LEFT JOIN Location l ON l.ID = LocationID
    WHERE f.FaultUnitID IN (SELECT ID FROM @VehicleList)
        AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
        AND f.CreateTime BETWEEN @DateFrom AND @DateTo
        AND (LocationID = @LocationID OR @LocationID IS NULL OR (@LocationID = -1 AND LocationID IS NULL))
        AND (HeadCode = @Headcode OR @Headcode IS NULL OR (@Headcode = '-1' AND HeadCode IS NULL))
        --AND (FleetCode = @FleetCode OR @FleetCode IS NULL)

END

GO

--------------------------------------------
-- ALTER PROCEDURE NSP_EaSearchVehicle
--------------------------------------------
RAISERROR ('-- Updating procedure dbo.NSP_EaSearchVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_EaSearchVehicle AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_EaSearchVehicle
**  Description:
**  Call frequency: Called from Ops Analysis
**  Parameters:
**  Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaSearchVehicle]
(
    @VehicleType varchar(50)
    , @VehicleString varchar(50)
    , @UnitID varchar(max) = NULL
)
AS
BEGIN

	DECLARE @VehicleTypeTable TABLE (VehicleType varchar(100))
    INSERT INTO @VehicleTypeTable
    SELECT * from [dbo].SplitStrings_CTE(@VehicleType,',')

    DECLARE @UnitIdTable TABLE (UnitID varchar(100))
    INSERT INTO @UnitIdTable
    SELECT * from [dbo].SplitStrings_CTE(@UnitID,',')

    SELECT
        FleetCode = f.Code
        , VehicleID   = v.ID
        , VehicleNumber = v.VehicleNumber
        , VehicleType = v.Type
    FROM Vehicle v
    JOIN Unit u
    ON u.ID = v.UnitID
    JOIN Fleet f
    ON f.ID = u.FleetID
    WHERE VehicleNumber LIKE '%' +  @VehicleString + '%'
        AND (Type IN (Select * from @VehicleTypeTable) OR @VehicleType IS NULL)
        AND (UnitID IN (Select * from @UnitIdTable) OR @UnitID IS NULL)
END

GO

--------------------------------------------
-- CREATE FUNCTION  PROCEDURE SplitStrings_CTE
--------------------------------------------
RAISERROR ('-- create function dbo.SplitStrings_CTE', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'SplitStrings_CTE' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE FUNCTION dbo.SplitStrings_CTE () RETURNS @Items TABLE (Item NVARCHAR(4000)) AS BEGIN RETURN END;')
GO
ALTER FUNCTION dbo.SplitStrings_CTE
(
   @List       NVARCHAR(MAX),
   @Delimiter  NVARCHAR(255)
)
RETURNS @Items TABLE (Item NVARCHAR(4000))
WITH SCHEMABINDING
AS
BEGIN
   DECLARE @ll INT = LEN(@List) + 1, @ld INT = LEN(@Delimiter);

   WITH a AS
   (
       SELECT
           [start] = 1,
           [end]   = COALESCE(NULLIF(CHARINDEX(@Delimiter,
                       @List, 1), 0), @ll),
           [value] = SUBSTRING(@List, 1,
                     COALESCE(NULLIF(CHARINDEX(@Delimiter,
                       @List, 1), 0), @ll) - 1)
       UNION ALL
       SELECT
           [start] = CONVERT(INT, [end]) + @ld,
           [end]   = COALESCE(NULLIF(CHARINDEX(@Delimiter,
                       @List, [end] + @ld), 0), @ll),
           [value] = SUBSTRING(@List, [end] + @ld,
                     COALESCE(NULLIF(CHARINDEX(@Delimiter,
                       @List, [end] + @ld), 0), @ll)-[end]-@ld)
       FROM a
       WHERE [end] < @ll
   )
   INSERT @Items SELECT [value]
   FROM a
   WHERE LEN([value]) > 0
   OPTION (MAXRECURSION 0);

   RETURN;
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,013
           ,'update_spectrum_db_013.sql'
           ,'1.0.01')
GO
