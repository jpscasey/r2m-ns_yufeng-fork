SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 022
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
-- schema changes to add HasRecovery to FaultMeta and Fault searches/views
IF COL_LENGTH('FaultMeta', 'HasRecovery') IS NULL
BEGIN
	ALTER TABLE FaultMeta ADD HasRecovery bit NOT NULL DEFAULT 0
END
GO

ALTER VIEW [dbo].[VW_IX_FaultLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
	FROM dbo.Fault f
	INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
	INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
	INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
	INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
	INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
	WHERE IsCurrent = 1


GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultLive_ID] ON [dbo].[VW_IX_FaultLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultLive_CreateTime] ON [dbo].[VW_IX_FaultLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER VIEW [dbo].[VW_FaultLive]
AS
    SELECT 
            ID    
          , CreateTime 
          , HeadCode 
          , SetCode 
          , FaultCode 
          , LocationID 
          , IsCurrent 
          , EndTime 
          , RecoveryID 
          , Latitude 
          , Longitude 
          , FaultMetaID 
          , FaultUnitID 
          , FaultUnitNumber 
          , PrimaryUnitID 
          , SecondaryUnitID 
          , TertiaryUnitID 
          , IsDelayed 
          , Description 
          , Summary 
          , CategoryID 
          , Category 
          , ReportingOnly 
          , FaultTypeID 
          , FaultType 
          , FaultTypeColor 
          , IsAcknowledged 
          , AcknowledgedTime 
          , Priority 
          , RecoveryStatus
		  , FleetCode 
		  , AdditionalInfo
		  , HasRecovery
    FROM dbo.VW_IX_FaultLive (NOEXPAND)     

GO

ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 	


GO

/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 11/01/2017 13:33:46 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 11/01/2017 13:33:54 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 11/01/2017 13:34:03 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/******************************************************************************
**	Name:			VW_FaultHistory
**	Description:	View returns list of faults for Fault History screen
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/	

ALTER VIEW [dbo].[VW_FaultHistory]
AS

	SELECT 
		f.ID
		, f.FaultMetaID
		, fv.UnitNumber			AS UnitNumber
		, f.CreateTime			AS CreateTime
		, f.IsCurrent			AS IsCurrent
		, f.RecoveryID			AS RecoveryID
		, fv.VehicleNumber		AS FaultVehicle
		, f.EndTime				AS EndTime
		, f.Headcode			AS HeadCode
		, l.Tiploc				AS LocationCode
		, f.Latitude			AS Latitude
		, f.Longitude			AS Longitude
		, f.FaultCode			AS FaultCode
		, f.FaultType
		, f.FaultTypeColor
		, f.Description			AS FaultDescription
		, f.Summary				AS FaultSummary
		, f.Category			AS FaultCategory
		, fv.FleetID			AS FleetID
		, fv.FleetCode			AS FleetCode
		, f.AdditionalInfo		AS AdditionalInfo
		, f.HasRecovery			AS HasRecovery

	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN VW_Vehicle fv ON fv.UnitID = f.FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID

GO

ALTER VIEW [dbo].[VW_IX_FaultNotLive] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 
		WHERE IsCurrent = 0


GO

/****** Object:  Index [IX_CL_U_VW_IX_FaultNotLive_ID]    Script Date: 11/01/2017 13:41:08 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultNotLive_ID] ON [dbo].[VW_IX_FaultNotLive]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_FaultNotLive_CreateTime]    Script Date: 11/01/2017 13:41:17 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultNotLive_CreateTime] ON [dbo].[VW_IX_FaultNotLive]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER VIEW [dbo].[VW_FaultNotLive]
AS
    SELECT 
    ID
      , CreateTime 
      , HeadCode 
      , SetCode 
      , FaultCode 
      , LocationID 
      , IsCurrent 
      , EndTime 
      , RecoveryID 
      , Latitude 
      , Longitude 
      , FaultMetaID 
      , FaultUnitID 
      , FaultUnitNumber 
      , PrimaryUnitID 
      , SecondaryUnitID 
      , TertiaryUnitID 
      , IsDelayed 
      , Description 
      , Summary 
      , CategoryID 
      , Category 
      , ReportingOnly 
      , FaultTypeID 
      , FaultType 
      , FaultTypeColor 
      , IsAcknowledged 
      , AcknowledgedTime 
      , Priority 
      , RecoveryStatus
	  , FleetCode 
	  , HasRecovery
        FROM dbo.VW_IX_FaultNotLive (NOEXPAND)


GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_SearchFault
**  Description:    Returns list faults for a keyword
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Keyword varchar(50)
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 

END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_SearchFaultAdvanced
**  Description:    Returns list of Faults matching parameters
**  Call frequency: Called from Event History screen
**  Parameters:     @DateFrom datetime
**                  @DateTo datetime
**                  @SearchType varchar(10) --options: Live, NoLive, All 
**                  @Headcode varchar(10)
**                  @UnitNumber varchar(10)
**                  @Vehicle varchar(10)
**                  @Location varchar(10)
**                  @FaultMetaID int
**                  @Description varchar(50)
**                  @Type varchar(10)
**                  @CategoryID int
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
	, @HasRecovery bit = NULL
)
AS
BEGIN 

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @Type = ''       SET @Type = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
			AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
			AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
			, FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
		LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID    = @Type                         OR @Type IS NULL)
			AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
        ORDER BY CreateTime DESC 

END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvancedCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvancedCount AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_SearchFaultAdvancedCount
**  Description:    Returns Fault count with specific parameters
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvancedCount](
    @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
	, @HasRecovery bit = NULL
)
AS
BEGIN 

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @Type = ''       SET @Type = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
        
    IF @SearchType = 'Live'
        
        SELECT  COUNT(*)
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    = @Type                         OR @Type IS NULL)
				AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
        
    IF @SearchType = 'NoLive'
        
        SELECT  COUNT(*)
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    = @Type                         OR @Type IS NULL)
				AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)

    IF @SearchType = 'All'
        
        SELECT  COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    = @Type                         OR @Type IS NULL)
			AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
END

GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLive AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultLive
**	Description:	Returns list of all Current Faults
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
AS

	SET NOCOUNT ON;
	
    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
		, FleetCode
		, HasRecovery
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnit AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultLiveUnit
**	Description:	Returns list of all Current Faults for a unit
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
	@UnitID int
)
AS

	SET NOCOUNT ON;

	SELECT
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
		, HasRecovery
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLive AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLive
**	Description:	Returns list of N recent Faults
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
	@N int
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
)
AS
	SET NOCOUNT ON;
	
 	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
		, HasRecovery
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
		AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
		AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO


IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLiveUnit AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLiveUnit
**	Description:	Returns list of N recent Faults for set
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**					@UnitID
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
	@N int
	, @UnitID int
)
AS
	SET NOCOUNT ON;

	SELECT TOP (@N)
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
		, HasRecovery
	FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
	ORDER BY CreateTime DESC

GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_GetFaultDetail
**  Description:    Returns details of the Fault
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
			, CreateTime    
			, HeadCode  
			, SetCode   
			, f.FaultCode   
			, LocationCode  
			, IsCurrent 
			, EndTime   
			, RecoveryID    
			, Latitude  
			, Longitude    
			, f.FaultMetaID   
			--, UnitNumber   
		    , FleetCode  
			, FaultUnitID    
            , FaultUnitNumber
			, IsDelayed 
			, f.Description   
			, f.Summary   
			, FaultType
			, FaultTypeColor
			, Category
			, f.CategoryID
			, ReportingOnly
			, '' as TransmissionStatus
			, fme.E2MFaultCode
			, fme.E2MDescription
			, fme.E2MPriorityCode
			, fme.E2MPriority
            , fm.Url
			, f.HasRecovery
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        WHERE f.ID = @FaultID

END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultMeta AS BEGIN RETURN(1) END;')
GO

ALTER PROC [dbo].[NSP_InsertFaultMeta](
    @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
	, @HasRecovery bit
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
BEGIN

    SET NOCOUNT ON;
    
    INSERT dbo.FaultMeta(
        Username
        , FaultCode
        , Description
        , Summary
		, AdditionalInfo
        , Url
        , CategoryID
		, HasRecovery
        , RecoveryProcessPath
        , FaultTypeID
        , GroupID
    )
    SELECT 
        Username      = @Username
        , FaultCode      = @FaultCode
        , Description   = @Description 
        , Summary       = @Summary
		, AdditionalInfo = @AdditionalInfo
        , Url           = @Url
        , CategoryID    = @CategoryID
		, HasRecovery	= @HasRecovery
        , RecoveryProcessPath   = @RecoveryProcessPath
        , FaultTypeID   = @FaultTypeID
        , GroupID       = @GroupID
        
    SELECT SCOPE_IDENTITY() AS InsertedID

END

GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateFaultMeta AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta](
    @ID int
    , @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
	, @HasRecovery bit
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
)
AS
    BEGIN
        
        SET NOCOUNT ON;

        BEGIN TRAN
        BEGIN TRY

            UPDATE dbo.FaultMeta SET
                Username        = @Username
                , FaultCode     = @FaultCode
                , Description   = @Description 
                , Summary       = @Summary
                , AdditionalInfo = @AdditionalInfo
                , Url           = @Url
                , CategoryID    = @CategoryID
				, HasRecovery	= @HasRecovery
                , RecoveryProcessPath   = @RecoveryProcessPath
                , FaultTypeID   = @FaultTypeID
                , GroupID       = @GroupID
            WHERE ID = @ID

            COMMIT TRAN
        
        END TRY
        BEGIN CATCH

            EXEC NSP_RethrowError;
            ROLLBACK TRAN;

        END CATCH

    END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,022
           ,'update_spectrum_db_022.sql'
           ,'1.1.02')
GO