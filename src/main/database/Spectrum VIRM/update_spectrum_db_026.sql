SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 026
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultLiveUnitAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLiveUnitAcknowledged] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitAcknowledged](
	@UnitID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
		, HasRecovery
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
		AND IsAcknowledged = 1
	ORDER BY CreateTime DESC
	
END

GO

------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultLiveUnitNotAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitNotAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLiveUnitNotAcknowledged] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitNotAcknowledged](
	@UnitID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber		= up.UnitNumber
        , SecondaryUnitNumber	= us.UnitNumber
        , TertiaryUnitNumber	= ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
		, RecoveryStatus
		, FleetCode
		, HasRecovery
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location ON f.LocationID = Location.ID
   	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE FaultUnitID = @UnitID
	    AND ReportingOnly = 0
		AND IsAcknowledged = 0
	ORDER BY CreateTime DESC
	
END

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,026
           ,'update_spectrum_db_026.sql'
           ,'1.1.01')
GO