SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 028
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- UPDATE PROCEDURE NSP_GetFaultDetail
--------------------------------------------
RAISERROR ('--Updating procedure NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**  Name:           NSP_GetFaultDetail
**  Description:    Returns details of the Fault
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , f.HasRecovery
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        WHERE f.ID = @FaultID

END
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,028
           ,'update_spectrum_db_028.sql'
           ,'1.1.02')
GO