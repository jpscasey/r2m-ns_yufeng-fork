SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 029
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- Create table ChannelDoorValue
--------------------------------------------
RAISERROR ('--Create table ChannelDoorValue', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ChannelDoorValue')
    DROP TABLE [dbo].[ChannelDoorValue]
GO

CREATE TABLE [dbo].[ChannelDoorValue](
	[ID] [bigint] NOT NULL,
	[UpdateRecord] [bit] NOT NULL,
	[UnitID] [int] NOT NULL,
	[RecordInsert] [datetime2](3) NOT NULL,
	[TimeStamp] [datetime2](3) NOT NULL,
	col3000	bit,
	col3001	bit,
	col3002	bit,
	col3003	bit,
	col3004	bit,
	col3005	bit,
	col3006	bit,
	col3007	bit,
	col3008	bit,
	col3009	bit,
	col3010	bit,
	col3011	bit,
	col3012	bit,
	col3013	bit,
	col3014	bit,
	col3015	bit,
	col3016	bit,
	col3017	bit,
	col3018	bit,
	col3019	bit,
	col3020	bit,
	col3021	bit,
	col3022	bit,
	col3023	bit,
	col3024	bit,
	col3025	bit,
	col3026	bit,
	col3027	bit,
	col3028	bit,
	col3029	bit,
	col3030	bit,
	col3031	bit,
	col3032	bit,
	col3033	bit,
	col3034	bit,
	col3035	bit,
	col3036	bit,
	col3037	bit,
	col3038	bit,
	col3039	bit,
	col3040	bit,
	col3041	bit,
	col3042	bit,
	col3043	bit,
	col3044	bit,
	col3045	bit,
	col3046	bit,
	col3047	bit,
	col3048	bit,
	col3049	bit,
	col3050	bit,
	col3051	bit,
	col3052	bit,
	col3053	bit,
	col3054	bit,
	col3055	bit,
	col3056	bit,
	col3057	bit,
	col3058	bit,
	col3059	bit,
	col3060	bit,
	col3061	bit,
	col3062	bit,
	col3063	bit,
	col3064	bit,
	col3065	bit,
	col3066	bit,
	col3067	bit,
	col3068	bit,
	col3069	bit,
	col3070	bit,
	col3071	bit,
	col3072	bit,
	col3073	bit,
	col3074	bit,
	col3075	bit,
	col3076	bit,
	col3077	bit,
	col3078	bit,
	col3079	bit,
	col3080	bit,
	col3081	bit,
	col3082	bit,
	col3083	bit,
	col3084	bit,
	col3085	bit,
	col3086	bit,
	col3087	bit,
	col3088	bit,
	col3089	bit,
	col3090	bit,
	col3091	bit,
	col3092	bit,
	col3093	bit,
	col3094	bit,
	col3095	bit,
	col3096	bit,
	col3097	bit,
	col3098	bit,
	col3099	bit,
	col3100	bit,
	col3101	bit,
	col3102	bit,
	col3103	bit,
	col3104	bit,
	col3105	bit,
	col3106	bit,
	col3107	bit,
	col3108	bit,
	col3109	bit,
	col3110	bit,
	col3111	bit,
	col3112	bit,
	col3113	bit,
	col3114	bit,
	col3115	bit,
	col3116	bit,
	col3117	bit,
	col3118	bit,
	col3119	bit,
	col3120	bit,
	col3121	bit,
	col3122	bit,
	col3123	bit,
	col3124	bit,
	col3125	bit,
	col3126	bit,
	col3127	bit,
	col3128	bit,
	col3129	bit,
	col3130	bit,
	col3131	bit,
	col3132	bit,
	col3133	bit,
	col3134	bit,
	col3135	bit,
	col3136	bit,
	col3137	bit,
	col3138	bit,
	col3139	bit,
	col3140	bit,
	col3141	bit,
	col3142	bit,
	col3143	bit,
	col3144	bit,
	col3145	bit,
	col3146	bit,
	col3147	bit,
	col3148	bit,
	col3149	bit,
	col3150	bit,
	col3151	bit,
	col3152	bit,
	col3153	bit,
	col3154	bit,
	col3155	bit,
	col3156	bit,
	col3157	bit,
	col3158	bit,
	col3159	bit,
	col3160	bit,
	col3161	bit,
	col3162	bit,
	col3163	bit,
	col3164	bit,
	col3165	bit,
	col3166	bit,
	col3167	bit,
	col3168	bit,
	col3169	bit,
	col3170	bit,
	col3171	bit,
	col3172	bit,
	col3173	bit,
	col3174	bit,
	col3175	bit,
	col3176	bit,
	col3177	bit,
	col3178	bit,
	col3179	bit,
	col3180	bit,
	col3181	bit,
	col3182	bit,
	col3183	bit,
	col3184	bit,
	col3185	bit,
	col3186	bit,
	col3187	bit,
	col3188	bit,
	col3189	bit,
	col3190	bit,
	col3191	bit,
	col3192	bit,
	col3193	bit,
	col3194	bit,
	col3195	bit,
	col3196	bit,
	col3197	bit,
	col3198	bit,
	col3199	bit,
	col3200	bit,
	col3201	bit,
	col3202	bit,
	col3203	bit,
	col3204	bit,
	col3205	bit,
	col3206	bit,
	col3207	bit,
	col3208	bit,
	col3209	bit,
	col3210	bit,
	col3211	bit,
	col3212	bit,
	col3213	bit,
	col3214	bit,
	col3215	bit,
	col3216	bit,
	col3217	bit,
	col3218	bit,
	col3219	bit,
	col3220	bit,
	col3221	bit,
	col3222	bit,
	col3223	bit,
	col3224	bit,
	col3225	bit,
	col3226	bit,
	col3227	bit,
	col3228	bit,
	col3229	bit,
	col3230	bit,
	col3231	bit,
	col3232	bit,
	col3233	bit,
	col3234	bit,
	col3235	bit,
	col3236	bit,
	col3237	bit,
	col3238	bit,
	col3239	bit,
	col3240	bit,
	col3241	bit,
	col3242	bit,
	col3243	bit,
	col3244	bit,
	col3245	bit,
	col3246	bit,
	col3247	bit,
	col3248	bit,
	col3249	bit,
	col3250	bit,
	col3251	bit,
	col3252	bit,
	col3253	bit,
	col3254	bit,
	col3255	bit,
	col3256	bit,
	col3257	bit,
	col3258	bit,
	col3259	bit,
	col3260	bit,
	col3261	bit,
	col3262	bit,
	col3263	bit,
	col3264	bit,
	col3265	bit,
	col3266	bit,
	col3267	bit,
	col3268	bit,
	col3269	bit,
	col3270	bit,
	col3271	bit,
	col3272	bit,
	col3273	bit,
	col3274	bit,
	col3275	bit,
	col3276	bit,
	col3277	bit,
	col3278	bit,
	col3279	bit,
	col3280	bit,
	col3281	bit,
	col3282	bit,
	col3283	bit,
	col3284	bit,
	col3285	bit,
	col3286	bit,
	col3287	bit,
	col3288	bit,
	col3289	bit,
	col3290	bit,
	col3291	bit,
	col3292	bit,
	col3293	bit,
	col3294	bit,
	col3295	bit,
	col3296	bit,
	col3297	bit,
	col3298	bit,
	col3299	bit,
	col3300	bit,
	col3301	bit,
	col3302	bit,
	col3303	bit,
	col3304	bit,
	col3305	bit,
	col3306	bit,
	col3307	bit,
	col3308	bit,
	col3309	bit,
	col3310	bit,
	col3311	bit,
	col3312	bit,
	col3313	bit,
	col3314	bit,
	col3315	bit,
	col3316	bit,
	col3317	bit,
	col3318	bit,
	col3319	bit,
	col3320	bit,
	col3321	bit,
	col3322	bit,
	col3323	bit,
	col3324	bit,
	col3325	bit,
	col3326	bit,
	col3327	bit,
	col3328	bit,
	col3329	bit,
	col3330	bit,
	col3331	bit,
	col3332	bit,
	col3333	bit,
	col3334	bit,
	col3335	bit,
	col3336	bit,
	col3337	bit,
	col3338	bit,
	col3339	bit,
	col3340	bit,
	col3341	bit,
	col3342	bit,
	col3343	bit,
	col3344	bit,
	col3345	bit,
	col3346	bit,
	col3347	bit,
	col3348	bit,
	col3349	bit,
	col3350	bit,
	col3351	bit,
	col3352	bit,
	col3353	bit,
	col3354	bit,
	col3355	bit,
	col3356	bit,
	col3357	bit,
	col3358	bit,
	col3359	bit,
	col3360	bit,
	col3361	bit,
	col3362	bit,
	col3363	bit,
	col3364	bit,
	col3365	bit,
	col3366	bit,
	col3367	bit,
	col3368	bit,
	col3369	bit,
	col3370	bit,
	col3371	bit,
	col3372	bit,
	col3373	bit,
	col3374	bit,
	col3375	bit,
	col3376	bit,
	col3377	bit,
	col3378	bit,
	col3379	bit,
	col3380	bit,
	col3381	bit,
	col3382	bit,
	col3383	bit,
	col3384	bit,
	col3385	bit,
	col3386	bit,
	col3387	bit,
	col3388	bit,
	col3389	bit,
	col3390	bit,
	col3391	bit,
	col3392	bit,
	col3393	bit,
	col3394	bit,
	col3395	bit,
	col3396	bit,
	col3397	bit,
	col3398	bit,
	col3399	bit,
	col3400	bit,
	col3401	bit,
	col3402	bit,
	col3403	bit,
	col3404	bit,
	col3405	bit,
	col3406	bit,
	col3407	bit,
	col3408	bit,
	col3409	bit,
	col3410	bit,
	col3411	bit,
	col3412	bit,
	col3413	bit,
	col3414	bit,
	col3415	bit,
	col3416	bit,
	col3417	bit,
	col3418	bit,
	col3419	bit,
	col3420	bit,
	col3421	bit,
	col3422	bit,
	col3423	bit,
	col3424	bit,
	col3425	bit,
	col3426	bit,
	col3427	bit,
	col3428	bit,
	col3429	bit,
	col3430	bit,
	col3431	bit,
	col3432	bit,
	col3433	bit,
	col3434	bit,
	col3435	bit,
	col3436	bit,
	col3437	bit,
	col3438	bit,
	col3439	bit,
	col3440	bit,
	col3441	bit,
	col3442	bit,
	col3443	bit,
	col3444	bit,
	col3445	bit,
	col3446	bit,
	col3447	bit,
	col3448	bit,
	col3449	bit,
	col3450	bit,
	col3451	bit,
	col3452	bit,
	col3453	bit,
	col3454	bit,
	col3455	bit,
	col3456	bit,
	col3457	bit,
	col3458	bit,
	col3459	bit,
	col3460	bit,
	col3461	bit,
	col3462	bit,
	col3463	bit,
	col3464	bit,
	col3465	bit,
	col3466	bit,
	col3467	bit,
	col3468	bit,
	col3469	bit,
	col3470	bit,
	col3471	bit,
	col3472	bit,
	col3473	bit,
	col3474	bit,
	col3475	bit,
	col3476	bit,
	col3477	bit,
	col3478	bit,
	col3479	bit,
	col3480	bit,
	col3481	bit,
	col3482	bit,
	col3483	bit,
	col3484	bit,
	col3485	bit,
	col3486	bit,
	col3487	bit,
	col3488	bit,
	col3489	bit,
	col3490	bit,
	col3491	bit,
	col3492	bit,
	col3493	bit,
	col3494	bit,
	col3495	bit,
	col3496	bit,
	col3497	bit,
	col3498	bit,
	col3499	bit,
	col3500	bit,
	col3501	bit,
	col3502	bit,
	col3503	bit,
	col3504	bit,
	col3505	bit,
	col3506	bit,
	col3507	bit,
	col3508	bit,
	col3509	bit,
	col3510	bit,
	col3511	bit,
	col3512	bit,
	col3513	bit,
	col3514	bit,
	col3515	bit,
	col3516	bit,
	col3517	bit,
	col3518	bit,
	col3519	bit,
 CONSTRAINT [PK_ChannelDoorValue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ChannelDoorValue] ADD  CONSTRAINT [DF_ChannelDoorValue_UpdatesRecord]  DEFAULT ((0)) FOR [UpdateRecord]
GO

ALTER TABLE [dbo].[ChannelDoorValue] ADD  CONSTRAINT [DF_ChannelDoorValue_RecordInsert]  DEFAULT (sysdatetime()) FOR [RecordInsert]
GO

ALTER TABLE [dbo].[ChannelDoorValue]  WITH CHECK ADD  CONSTRAINT [FK_Unit_ChannelDoorValue] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Unit] ([ID])
GO

ALTER TABLE [dbo].[ChannelDoorValue] CHECK CONSTRAINT [FK_Unit_ChannelDoorValue]
GO


--------------------------------------------
-- Create table EventChannelValue
--------------------------------------------
RAISERROR ('--Create table EventChannelValue', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EventChannelValue')
    DROP TABLE [dbo].EventChannelValue
GO

CREATE TABLE [dbo].[EventChannelValue](
	[ID] [bigint] NOT NULL,
	[UnitID] [int] NOT NULL,
	[RecordInsert] [datetime2](3) NOT NULL,
	[TimeStamp] [datetime2](3) NOT NULL,
	[TimeStampEndTime] [datetime2](3) NULL,
	[ChannelID] [int] NOT NULL,
	[Value] bit NOT NULL
	CONSTRAINT [PK_EventChannelValue] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[EventChannelValue] ADD  CONSTRAINT [DF_EventChannelValue_RecordInsert]  DEFAULT (sysdatetime()) FOR [RecordInsert]
GO

ALTER TABLE [dbo].[EventChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_Unit_EventChannelValue] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Unit] ([ID])
GO

ALTER TABLE [dbo].[EventChannelValue] CHECK CONSTRAINT [FK_Unit_EventChannelValue]
GO

ALTER TABLE [dbo].[EventChannelValue]  WITH CHECK ADD  CONSTRAINT [FK_Channel_EventChannelValue] FOREIGN KEY([ChannelID])
REFERENCES [dbo].[Channel] ([ID])
GO

ALTER TABLE [dbo].[EventChannelValue] CHECK CONSTRAINT [FK_Channel_EventChannelValue]
GO

-- fields to store the exact vehicle associated with a measurement or event - For SLT 51 or 50 means for the whole unit
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Channel' and COLUMN_NAME='VehicleIn4Car')
	ALTER TABLE dbo.Channel ADD VehicleIn4Car tinyint

-- fields to store the exact vehicle associated with a measurement or event - For SLT 51 or 50 means for the whole unit
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Channel' and COLUMN_NAME='VehicleIn6Car')
	ALTER TABLE dbo.Channel ADD VehicleIn6Car tinyint

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Channel' and COLUMN_NAME='VehicleNumberIf4Vehicles')
	ALTER TABLE dbo.Channel DROP COLUMN VehicleNumberIf4Vehicles
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Channel' and COLUMN_NAME='VehicleNumberIf6Vehicles')
	ALTER TABLE dbo.Channel DROP COLUMN VehicleNumberIf6Vehicles
GO

--------------------------------------------
-- Create table ChannelCategory
--------------------------------------------
RAISERROR ('--Create table ChannelCategory', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ChannelCategory')
BEGIN
	
	IF EXISTS (SELECT * FROM SYS.foreign_keys WHERE NAME = 'FK_ChannelCategory_Channel' )
		ALTER TABLE [dbo].[Channel]  DROP  CONSTRAINT [FK_ChannelCategory_Channel]

    DROP TABLE [dbo].ChannelCategory
END
GO

CREATE TABLE [dbo].[ChannelCategory](
	[ID] tinyint IDENTITY NOT NULL,
	[ChannelCategoryName] varchar(20) NOT NULL,
	[Notes] varchar(50), 	
	[IsVisible] bit NOT NULL
	CONSTRAINT [PK_ChannelCategory] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE UNIQUE INDEX UX_ChannelCategory_Name on ChannelCategory (ChannelCategoryName)
GO


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Channel' and COLUMN_NAME='ChannelCategoryId')
BEGIN
	ALTER TABLE dbo.Channel ADD ChannelCategoryId tinyint

	ALTER TABLE [dbo].[Channel]  WITH CHECK ADD  CONSTRAINT [FK_ChannelCategory_Channel] FOREIGN KEY(ChannelCategoryId)
	REFERENCES [dbo].[ChannelCategory] ([ID])

	ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_ChannelCategory_Channel]
END

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,029
           ,'update_spectrum_db_029.sql'
           ,'1.1.01')
GO