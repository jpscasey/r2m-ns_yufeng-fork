SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 034
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
--------------------------------------------
-- ALTER TABLE Channel alter Column HardwareType
--------------------------------------------
RAISERROR ('--ALTER TABLE Channel alter Column HardwareType', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'IX_U_Channel_Header_HardwareType_VehicleID')
	ALTER TABLE Channel DROP CONSTRAINT [IX_U_Channel_Header_HardwareType_VehicleID]

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'IX_U_Channel_Name_HardwareType_VehicleID')
	ALTER TABLE Channel DROP CONSTRAINT IX_U_Channel_Name_HardwareType_VehicleID

IF EXISTS (SELECT * FROM sys.default_constraints  WHERE NAME = 'DF_Channel_HardwareType') -- will not be recreated
	ALTER TABLE Channel DROP CONSTRAINT DF_Channel_HardwareType

ALTER TABLE dbo.Channel alter Column HardwareType varchar(20)

ALTER TABLE dbo.Channel ADD CONSTRAINT [IX_U_Channel_Header_HardwareType_VehicleID] UNIQUE NONCLUSTERED 
(
	[Header] ASC,
	[HardwareType] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]

ALTER TABLE dbo.Channel ADD CONSTRAINT [IX_U_Channel_Name_HardwareType_VehicleID] UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[HardwareType] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,034
           ,'update_spectrum_db_034.sql'
           ,'1.1.02')
GO