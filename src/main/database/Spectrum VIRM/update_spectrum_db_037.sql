SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 037
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('-- update NSP_HistoricFleetSummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricFleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricFleetSummary] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_HistoricFleetSummary](
    @DateTimeEnd datetime
)
AS
BEGIN

    SET NOCOUNT ON;
        DECLARE @interval int = 3600 -- value in seconds 
    
    IF @DateTimeEnd IS NULL 
        SET @DateTimeEnd = SYSDATETIME()
    
    DECLARE @DateTimeStart datetime = DATEADD(s, - @interval, @DateTimeEnd);

    -- Get Latest ChannelValue timestamp for each of the vehicles transmitting
    DECLARE @LatestChannelValue TABLE(
        UnitID int
        , LastTimeStamp datetime2(3)
    )
    
    INSERT @LatestChannelValue(
        UnitID
        , LastTimeStamp
    )
    SELECT 
        UnitID      = VW_Vehicle.UnitID
        , LastTimeStamp = (
            SELECT TOP 1 TimeStamp
            FROM ChannelValue
            WHERE 
                Timestamp >= @DateTimeStart
                AND Timestamp <= @DateTimeEnd
                AND UnitID =    VW_Vehicle.UnitID 
            ORDER BY TimeStamp DESC
        )
    FROM VW_Vehicle 

    SELECT 
        v.UnitID
        , v.UnitNumber
        , v.UnitType
        , fs.FleetFormationID
        , fs.UnitPosition
        , Diagram               = fs.Diagram
        , Headcode              = fs.Headcode
        , VehicleID             = v.ID
        , VehicleNumber         = v.VehicleNumber
        , VehiclePosition       = v.VehicleOrder
        , VehicleType           = v.Type
        , Location              = (
            SELECT TOP 1 
                LocationCode --CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
                AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        ,TimeStamp              = CAST(cv.[TimeStamp] AS datetime)
        ,cv.[Col1]
        ,cv.[Col2]
        ,cv.[Col3]
        ,cv.[Col4]
        ,cv.[Col6]
        ,cv.[Col30]
        ,FleetCode
    FROM VW_Vehicle v 

    INNER JOIN @LatestChannelValue lcv 
        ON v.ID = lcv.UnitID

    INNER JOIN ChannelValue cv WITH (NOLOCK) 
        ON cv.TimeStamp = lcv.LastTimeStamp 
        AND cv.UnitID = lcv.UnitID

    LEFT JOIN FleetStatusHistory fs ON
        v.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 

    LEFT JOIN Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
    
END
GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,037
           ,'update_spectrum_db_037.sql'
           ,'1.1.02')
GO