SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 038
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_FleetSummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummary AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_FleetSummary]
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Get totals for Vehicle based faults
	DECLARE @currentVehicleFault TABLE(
		VehicleID int NULL
		, FaultNumber int NULL
		, WarningNumber int NULL
		, RecoveryNumber int NULL
		, NotAcknowledgedNumber int NULL
	)
	
	INSERT INTO @currentVehicleFault
	SELECT 
		Vehicle.ID
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
		, SUM(CASE HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
		, SUM(CASE IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
	FROM VW_IX_FaultLive WITH (NOEXPAND)
	JOIN dbo.Vehicle ON UnitID = FaultUnitID
	WHERE FaultUnitID IS NOT NULL
		AND Category <> 'Test'
		AND ReportingOnly = 0
	GROUP BY Vehicle.ID

	-- return data
	SELECT 
		fs.UnitID
		, fs.UnitNumber
		, fs.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, fs.Headcode
		, fs.Diagram
		, fs.Loading
		, fs.VehicleID
		, fs.VehicleNumber
		, fs.VehicleType
		, fs.VehicleTypeID
		, fs.VehiclePosition
		, fs.FleetCode
		, Location = (
		SELECT TOP 1 
			LocationCode --CRS
		FROM Location
		INNER JOIN LocationArea ON LocationID = Location.ID
		WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
			AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
		ORDER BY Priority
		)
		, FaultNumber		= ISNULL(vf.FaultNumber,0)
		, WarningNumber		= ISNULL(vf.WarningNumber,0)
		, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
		, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
		, VehicleList
		, ServiceStatus		= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = (
					SELECT TOP 1 LocationID
					FROM LocationArea
					WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
						AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
						AND LocationArea.Type = 'C'
					ORDER BY Priority
				)
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		-- temporary solution for datetime and JTDS driver 
		,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp,
-- Channel Data 
		[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
		[Col400],
		[Col401],
		[Col402],
		[Col403],
		[Col404],
		[Col405],
		[Col406],
		[Col407],
		[Col408],
		[Col409],
		[Col410],
		[Col411],
		[Col412],
		[Col413],
		[Col414],
		[Col415],
		[Col416],
		[Col417],
		[Col418],
		[Col419],
		[Col420],
		[Col421],
		[Col422],
		[Col423],
		[Col424],
		[Col425],
		[Col426],
		[Col427],
		[Col428],
		[Col429],
		[Col430],
		[Col431],
		[Col432],
		[Col433],
		[Col434],
		[Col435],
		[Col436],
		[Col437],
		[Col438],
		[Col439],
		[Col440],
		[Col441],
		[Col442],
		[Col443],
		[Col444],
		[Col445],
		[Col446],
		[Col447],
		[Col448],
		[Col449],
		[Col450],
		[Col451],
		[Col452],
		[Col453],
		[Col454],
		[Col455],
		[Col456],
		[Col457],
		[Col458],
		[Col459],
		[Col460],
		[Col461],
		[Col462],
		[Col463],
		[Col464],
		[Col465],
		[Col466],
		[Col467],
		[Col468],
		[Col469],
		[Col470],
		[Col471],
		[Col472],
		[Col473],
		[Col474],
		[Col475],
		[Col476],
		[Col477],
		[Col478],
		[Col479],
		[Col480],
		[Col481],
		[Col482],
		[Col483],
		[Col484],
		[Col485],
		[Col486],
		[Col487],
		[Col488],
		[Col489],
		[Col490],
		[Col491],
		[Col492],
		[Col493],
		[Col494],
		[Col495],
		[Col496],
		[Col497],
		[Col498],
		[Col499],
		[Col500],
		[Col501],
		[Col502],
		[Col503],
		[Col504],
		[Col505],
		[Col506],
		[Col507],
		[Col508],
		[Col509],
		[Col510],
		[Col511],
		[Col512],
		[Col513],
		[Col514],
		[Col515],
		[Col516],
		[Col517],
		[Col518],
		[Col519],
		[Col520],
		[Col521],
		[Col522],
		[Col523],
		[Col524],
		[Col525],
		[Col526],
		[Col527],
		[Col528],
		[Col529],
		[Col530],
		[Col531],
		[Col532],
		[Col533],
		[Col534],
		[Col535],
		[Col536],
		[Col537],
		[Col538],
		[Col539],
		[Col540],
		[Col541],
		[Col542],
		[Col543],
		[Col544],
		[Col545],
		[Col546],
		[Col547],
		[Col548],
		[Col549],
		[Col550],
		[Col551],
		[Col552],
		[Col553],
		[Col554],
		[Col555],
		[Col556],
		[Col557],
		[Col558],
		[Col559],
		[Col560],
		[Col561],
		[Col562],
		[Col563],
		[Col564],
		[Col565],
		[Col566],
		[Col567],
		[Col568],
		[Col569],
		[Col570],
		[Col571],
		[Col572],
		[Col573],
		[Col574],
		[Col575],
		[Col576],
		[Col577],
		[Col578],
		[Col579],
		[Col580],
		[Col581],
		[Col582],
		[Col583],
		[Col584],
		[Col585],
		[Col586],
		[Col587],
		[Col588],
		[Col589],
		[Col590],
		[Col591],
		[Col592],
		[Col593],
		[Col594],
		[Col595],
		[Col596],
		[Col597],
		[Col598],
		[Col599],
		[Col600],
		[Col601],
		[Col602],
		[Col603],
		[Col604],
		[Col605],
		[Col606],
		[Col607],
		[Col608],
		[Col609],
		[Col610],
		[Col611],
		[Col612],
		[Col613],
		[Col614],
		[Col615],
		[Col616],
		[Col617],
		[Col618],
		[Col619],
		[Col620],
		[Col621],
		[Col622],
		[Col623],
		[Col624],
		[Col625],
		[Col626],
		[Col627],
		[Col628],
		[Col629],
		[Col630],
		[Col631],
		[Col632],
		[Col633],
		[Col634],
		[Col635],
		[Col636],
		[Col637],
		[Col638],
		[Col639],
		[Col640],
		[Col641],
		[Col642],
		[Col643],
		[Col644],
		[Col645],
		[Col646],
		[Col647],
		[Col648],
		[Col649],
		[Col650],
		[Col651],
		[Col652],
		[Col653],
		[Col654],
		[Col655],
		[Col656],
		[Col657],
		[Col658],
		[Col659],
		[Col660],
		[Col661],
		[Col662],
		[Col663],
		[Col5000],
		[Col5001],
		[Col5002],
		[Col5003],
		[Col5004],
		[Col5005],
		[Col5006],
		[Col5007],
		[Col5008],
		[Col5009],
		[Col5010],
		[Col5011],
		[Col5012],
		[Col5013],
		[Col5014],
		[Col5015],
		[Col5016],
		[Col5017],
		[Col5018],
		[Col5019],
		[Col5020],
		[Col5021],
		[Col5022],
		[Col5023],
		[Col5024],
		[Col5025],
		[Col5026],
		[Col5027],
		[Col5028],
		[Col5029],
		[Col5030],
		[Col5031],
		[Col5032],
		[Col5033],
		[Col5034],
		[Col5035],
		[Col5036],
		[Col5037],
		[Col5038],
		[Col5039],
		[Col5040],
		[Col5041],
		[Col5042],
		[Col5043],
		[Col5044],
		[Col5045],
		[Col5046],
		[Col5047],
		[Col5048],
		[Col5049],
		[Col5050],
		[Col5051],
		[Col5052],
		[Col5053],
		[Col5054],
		[Col5055],
		[Col5056],
		[Col5057],
		[Col5058],
		[Col5059],
		[Col5060],
		[Col5061],
		[Col5062],
		[Col5063],
		[Col5064],
		[Col5065],
		[Col5066],
		[Col5067],
		[Col5068],
		[Col5069],
		[Col5070],
		[Col5071],
		[Col5072],
		[Col5073],
		[Col5074],
		[Col5075],
		[Col5076],
		[Col5077],
		[Col5078],
		[Col5079],
		[Col5080],
		[Col5081],
		[Col5082],
		[Col5083],
		[Col5084],
		[Col5085],
		[Col5086],
		[Col5087],
		[Col5088],
		[Col5089],
		[Col5090],
		[Col5091],
		[Col5092],
		[Col5093],
		[Col5094],
		[Col5095],
		[Col5096],
		[Col5097],
		[Col5098],
		[Col5099],
		[Col5100],
		[Col5101],
		[Col5102],
		[Col5103],
		[Col5104],
		[Col5105],
		[Col5106],
		[Col5107],
		[Col5108],
		[Col5109],
		[Col5110],
		[Col5111],
		[Col5112],
		[Col5113],
		[Col5114],
		[Col5115],
		[Col5116],
		[Col5117],
		[Col5118],
		[Col5119],
		[Col5120],
		[Col5121],
		[Col5122],
		[Col5123],
		[Col5124],
		[Col5125],
		[Col5126],
		[Col5127],
		[Col5128],
		[Col5129],
		[Col5130],
		[Col5131],
		[Col5132],
		[Col5133],
		[Col5134],
		[Col5135],
		[Col5136],
		[Col5137],
		[Col5138],
		[Col5139],
		[Col5140],
		[Col5141],
		[Col5142],
		[Col5143],
		[Col5144],
		[Col5145],
		[Col5146],
		[Col5147],
		[Col5148],
		[Col5149],
		[Col5150],
		[Col5151],
		[Col5152],
		[Col5153],
		[Col5154],
		[Col5155],
		[Col5156],
		[Col5157],
		[Col5158],
		[Col5159],
		[Col5160],
		[Col5161],
		[Col5162],
		[Col5163],
		[Col5164],
		[Col5165],
		[Col5166],
		[Col5167],
		[Col5168],
		[Col5169],
		[Col5170],
		[Col5171],
		[Col5172],
		[Col5173],
		[Col5174],
		[Col5175],
		[Col5176],
		[Col5177],
		[Col5178],
		[Col5179],
		[Col5180],
		[Col5181],
		[Col5182],
		[Col5183],
		[Col5184],
		[Col5185],
		[Col5186],
		[Col5187],
		[Col5188],
		[Col5189],
		[Col5190],
		[Col5191],
		[Col5192],
		[Col5193],
		[Col5194],
		[Col5195],
		[Col5196],
		[Col5197],
		[Col5198],
		[Col5199],
		[Col5200],
		[Col5201],
		[Col5202],
		[Col5203],
		[Col5204],
		[Col5205],
		[Col5206],
		[Col5207],
		[Col5208],
		[Col5209],
		[Col5210],
		[Col5211],
		[Col5212],
		[Col5213],
		[Col5214],
		[Col5215],
		[Col5216],
		[Col5217],
		[Col5218],
		[Col5219],
		[Col5220],
		[Col5221],
		[Col5222],
		[Col5223],
		[Col5224],
		[Col5225],
		[Col5226],
		[Col5227],
		[Col5228],
		[Col5229],
		[Col5230],
		[Col5231],
		[Col5232],
		[Col5233],
		[Col5234],
		[Col5235],
		[Col5236],
		[Col5237],
		[Col5238],
		[Col5239],
		[Col5240],
		[Col5241],
		[Col5242],
		[Col5243],
		[Col5244],
		[Col5245],
		[Col5246],
		[Col5247],
		[Col5248],
		[Col5249],
		[Col5250],
		[Col5251],
		[Col5252],
		[Col5253],
		[Col5254],
		[Col5255],
		[Col5256],
		[Col5257],
		[Col5258],
		[Col5259],
		[Col5260],
		[Col5261],
		[Col5262],
		[Col5263],
		[Col5264],
		[Col5265],
		[Col5266],
		[Col5267],
		[Col5268],
		[Col5269],
		[Col5270],
		[Col5271],
		[Col5272],
		[Col5273],
		[Col5274],
		[Col5275],
		[Col5276],
		[Col5277],
		[Col5278],
		[Col5279],
		[Col5280],
		[Col5281],
		[Col5282],
		[Col5283],
		[Col5284],
		[Col5285],
		[Col5286],
		[Col5287],
		[Col5288],
		[Col5289],
		[Col5290],
		[Col5291],
		[Col5292],
		[Col5293],
		[Col5294],
		[Col5295],
		[Col5296],
		[Col5297],
		[Col5298],
		[Col5299],
		[Col5300],
		[Col5301],
		[Col5302],
		[Col5303],
		[Col5304],
		[Col5305],
		[Col5306],
		[Col5307],
		[Col5308],
		[Col5309],
		[Col5310],
		[Col5311],
		[Col5312],
		[Col5313],
		[Col5314],
		[Col5315],
		[Col5316],
		[Col5317],
		[Col5318],
		[Col5319],
		[Col5320],
		[Col5321],
		[Col5322],
		[Col5323],
		[Col5324],
		[Col5325],
		[Col5326],
		[Col5327],
		[Col5328],
		[Col5329],
		[Col5330],
		[Col5331],
		[Col5332],
		[Col5333],
		[Col5334],
		[Col5335],
		[Col5336],
		[Col5337],
		[Col5338],
		[Col5339],
		[Col5340],
		[Col5341],
		[Col5342],
		[Col5343],
		[Col5344],
		[Col5345],
		[Col5346],
		[Col5347],
		[Col5348],
		[Col5349],
		[Col5350],
		[Col5351],
		[Col5352],
		[Col5353],
		[Col5354],
		[Col5355],
		[Col5356],
		[Col5357],
		[Col5358],
		[Col5359],
		[Col5360],
		[Col5361],
		[Col5362],
		[Col5363],
		[Col5364],
		[Col5365],
		[Col5366],
		[Col5367],
		[Col5368],
		[Col5369],
		[Col5370],
		[Col5371],
		[Col5372],
		[Col5373],
		[Col5374],
		[Col5375],
		[Col5376],
		[Col5377],
		[Col5378],
		[Col5379],
		[Col5380],
		[Col5381],
		[Col5382],
		[Col5383],
		[Col5384],
		[Col5385],
		[Col5386],
		[Col5387],
		[Col5388],
		[Col5389],
		[Col5390],
		[Col5391],
		[Col5392],
		[Col5393],
		[Col5394],
		[Col5395],
		[Col5396],
		[Col5397],
		[Col5398],
		[Col5399],
		[Col5400],
		[Col5401],
		[Col5402],
		[Col5403],
		[Col5404],
		[Col5405],
		[Col5406],
		[Col5407],
		[Col5408],
		[Col5409],
		[Col5410],
		[Col5411],
		[Col5412],
		[Col5413],
		[Col5414],
		[Col5415],
		[Col5416],
		[Col5417],
		[Col5418],
		[Col5419],
		[Col5420],
		[Col5421],
		[Col5422],
		[Col5423],
		[Col5424],
		[Col5425],
		[Col5426],
		[Col5427],
		[Col5428],
		[Col5429],
		[Col5430],
		[Col5431],
		[Col5432],
		[Col5433],
		[Col5434],
		[Col5435],
		[Col5436],
		[Col5437],
		[Col5438],
		[Col5439],
		[Col5440],
		[Col5441],
		[Col5442],
		[Col5443],
		[Col5444],
		[Col5445],
		[Col5446],
		[Col5447],
		[Col5448],
		[Col5449],
		[Col5450],
		[Col5451],
		[Col5452],
		[Col5453],
		[Col5454],
		[Col5455],
		[Col5456],
		[Col5457],
		[Col5458],
		[Col5459],
		[Col5460],
		[Col5461],
		[Col5462],
		[Col5463],
		[Col5464],
		[Col5465],
		[Col5466],
		[Col5467],
		[Col5468],
		[Col5469],
		[Col5470],
		[Col5471],
		[Col5472],
		[Col5473],
		[Col5474],
		[Col5475],
		[Col5476],
		[Col5477],
		[Col5478],
		[Col5479],
		[Col5480],
		[Col5481],
		[Col5482],
		[Col5483],
		[Col5484],
		[Col5485],
		[Col5486],
		[Col5487],
		[Col5488],
		[Col5489],
		[Col5490],
		[Col5491],
		[Col5492],
		[Col5493],
		[Col5494],
		[Col5495],
		[Col5496],
		[Col5497],
		[Col5498],
		[Col5499],
		[Col5500],
		[Col5501],
		[Col5502],
		[Col5503],
		[Col5504],
		[Col5505],
		[Col5506],
		[Col5507],
		[Col5508],
		[Col5509],
		[Col5510],
		[Col5511],
		[Col5512],
		[Col5513],
		[Col5514],
		[Col5515],
		[Col5516],
		[Col5517],
		[Col5518],
		[Col5519],
		[Col5520],
		[Col5521],
		[Col5522],
		[Col5523],
		[Col5524],
		[Col5525],
		[Col5526],
		[Col5527],
		[Col5528],
		[Col5529],
		[Col5530],
		[Col5531],
		[Col5532],
		[Col5533],
		[Col5534],
		[Col5535],
		[Col5536],
		[Col5537],
		[Col5538],
		[Col5539],
		[Col5540],
		[Col5541],
		[Col5542],
		[Col5543],
		[Col5544],
		[Col5545],
		[Col5546],
		[Col5547],
		[Col5548],
		[Col5549],
		[Col5550],
		[Col5551],
		[Col5552],
		[Col5553],
		[Col5554],
		[Col5555],
		[Col5556],
		[Col5557],
		[Col5558],
		[Col5559],
		[Col5560],
		[Col5561],
		[Col5562],
		[Col5563],
		[Col5564],
		[Col5565],
		[Col5566],
		[Col5567],
		[Col5568],
		[Col5569],
		[Col5570],
		[Col5571],
		[Col5572],
		[Col5573],
		[Col5574],
		[Col5575],
		[Col5576],
		[Col5577],
		[Col5578],
		[Col5579],
		[Col5580],
		[Col5581],
		[Col5582],
		[Col5583],
		[Col5584],
		[Col5585],
		[Col5586],
		[Col5587],
		[Col5588],
		[Col5589],
		[Col5590],
		[Col5591],
		[Col5592],
		[Col5593],
		[Col5594],
		[Col5595],
		[Col5596],
		[Col5597],
		[Col5598],
		[Col5599],
		[Col5600],
		[Col5601],
		[Col5602],
		[Col5603],
		[Col5604],
		[Col5605],
		[Col5606],
		[Col5607],
		[Col5608],
		[Col5609],
		[Col5610],
		[Col5611],
		[Col5612],
		[Col5613],
		[Col5614],
		[Col5615],
		[Col5616],
		[Col5617],
		[Col5618],
		[Col5619],
		[Col5620],
		[Col5621],
		[Col5622],
		[Col5623],
		[Col5624],
		[Col5625],
		[Col5626],
		[Col5627],
		[Col5628],
		[Col5629],
		[Col5630],
		[Col5631],
		[Col5632],
		[Col5633],
		[Col5634],
		[Col5635],
		[Col5636],
		[Col5637],
		[Col5638],
		[Col5639],
		[Col5640],
		[Col5641],
		[Col5642],
		[Col5643],
		[Col5644],
		[Col5645],
		[Col5646],
		[Col5647],
		[Col5648],
		[Col5649],
		[Col5650],
		[Col5651],
		[Col5652],
		[Col5653],
		[Col5654],
		[Col5655],
		[Col5656],
		[Col5657],
		[Col5658],
		[Col5659],
		[Col5660],
		[Col5661],
		[Col5662],
		[Col5663],
		[Col5664],
		[Col5665],
		[Col5666],
		[Col5667],
		[Col5668],
		[Col5669],
		[Col5670],
		[Col5671],
		[Col5672],
		[Col5673],
		[Col5674],
		[Col5675],
		[Col5676],
		[Col5677],
		[Col5678],
		[Col5679],
		[Col5680],
		[Col5681],
		[Col5682],
		[Col5683],
		[Col5684],
		[Col5685],
		[Col5686],
		[Col5687],
		[Col5688],
		[Col5689],
		[Col5690],
		[Col5691],
		[Col5692],
		[Col5693],
		[Col5694],
		[Col5695],
		[Col5696],
		[Col5697],
		[Col5698],
		[Col5699],
		[Col5700],
		[Col5701],
		[Col5702],
		[Col5703],
		[Col5704],
		[Col5705],
		[Col5706],
		[Col5707],
		[Col5708],
		[Col5709],
		[Col5710],
		[Col5711],
		[Col5712],
		[Col5713],
		[Col5714],
		[Col5715],
		[Col5716],
		[Col5717],
		[Col5718],
		[Col5719],
		[Col5720],
		[Col5721],
		[Col5722],
		[Col5723],
		[Col5724],
		[Col5725],
		[Col5726],
		[Col5727],
		[Col5728],
		[Col5729],
		[Col5730],
		[Col5731],
		[Col5732],
		[Col5733],
		[Col5734],
		[Col5735],
		[Col5736],
		[Col5737],
		[Col5738],
		[Col5739],
		[Col5740],
		[Col5741],
		[Col5742],
		[Col5743],
		[Col5744],
		[Col5745],
		[Col5746],
		[Col5747],
		[Col5748],
		[Col5749],
		[Col5750],
		[Col5751],
		[Col5752],
		[Col5753],
		[Col5754],
		[Col5755],
		[Col5756],
		[Col5757],
		[Col5758],
		[Col5759],
		[Col5760],
		[Col5761],
		[Col5762],
		[Col5763],
		[Col5764],
		[Col5765],
		[Col5766],
		[Col5767],
		[Col5768],
		[Col5769],
		[Col5770],
		[Col5771],
		[Col5772],
		[Col5773],
		[Col5774],
		[Col5775],
		[Col5776],
		[Col5777],
		[Col5778],
		[Col5779],
		[Col5780],
		[Col5781],
		[Col5782],
		[Col5783],
		[Col5784],
		[Col5785],
		[Col5786],
		[Col5787],
		[Col5788],
		[Col5789],
		[Col5790],
		[Col5791],
		[Col5792],
		[Col5793],
		[Col5794],
		[Col5795],
		[Col5796],
		[Col5797],
		[Col5798],
		[Col5799],
		[Col5800],
		[Col5801],
		[Col5802],
		[Col5803],
		[Col5804],
		[Col5805],
		[Col5806],
		[Col5807],
		[Col5808],
		[Col5809],
		[Col5810],
		[Col5811],
		[Col5812],
		[Col5813],
		[Col5814],
		[Col5815],
		[Col5816],
		[Col5817],
		[Col5818],
		[Col5819],
		[Col5820],
		[Col5821],
		[Col5822],
		[Col5823],
		[Col5824],
		[Col5825],
		[Col5826],
		[Col5827],
		[Col5828],
		[Col5829],
		[Col5830],
		[Col5831],
		[Col5832],
		[Col5833],
		[Col5834],
		[Col5835],
		[Col5836],
		[Col5837],
		[Col5838],
		[Col5839],
		[Col5840],
		[Col5841],
		[Col5842],
		[Col5843],
		[Col5844],
		[Col5845],
		[Col5846],
		[Col5847],
		[Col5848],
		[Col5849],
		[Col5850],
		[Col5851],
		[Col5852],
		[Col5853],
		[Col5854],
		[Col5855],
		[Col5856],
		[Col5857],
		[Col5858],
		[Col5859],
		[Col5860],
		[Col5861],
		[Col5862],
		[Col5863],
		[Col5864],
		[Col5865],
		[Col5866],
		[Col5867],
		[Col5868],
		[Col5869],
		[Col5870],
		[Col5871],
		[Col5872],
		[Col5873],
		[Col5874],
		[Col5875],
		[Col5876],
		[Col5877],
		[Col5878],
		[Col5879],
		[Col5880],
		[Col5881],
		[Col5882],
		[Col5883],
		[Col5884],
		[Col5885],
		[Col5886],
		[Col5887],
		[Col5888],
		[Col5889],
		[Col5890],
		[Col5891],
		[Col5892],
		[Col5893],
		[Col5894],
		[Col5895],
		[Col5896],
		[Col5897],
		[Col5898],
		[Col5899],
		[Col5900],
		[Col5901],
		[Col5902],
		[Col5903],
		[Col5904],
		[Col5905],
		[Col5906],
		[Col5907],
		[Col5908],
		[Col5909],
		[Col5910],
		[Col5911],
		[Col5912],
		[Col5913],
		[Col5914],
		[Col5915],
		[Col5916],
		[Col5917],
		[Col5918],
		[Col5919],
		[Col5920],
		[Col5921],
		[Col5922],
		[Col5923],
		[Col5924],
		[Col5925],
		[Col5926],
		[Col5927],
		[Col5928],
		[Col5929],
		[Col5930],
		[Col5931],
		[Col5932],
		[Col5933],
		[Col5934],
		[Col5935],
		[Col5936],
		[Col5937],
		[Col5938],
		[Col5939],
		[Col5940],
		[Col5941],
		[Col5942],
		[Col5943],
		[Col5944],
		[Col5945],
		[Col5946],
		[Col5947],
		[Col5948],
		[Col5949],
		[Col5950],
		[Col5951],
		[Col5952],
		[Col5953],
		[Col5954],
		[Col5955],
		[Col5956],
		[Col5957],
		[Col5958],
		[Col5959],
		[Col5960],
		[Col5961],
		[Col5962],
		[Col5963],
		[Col5964],
		[Col5965],
		[Col5966],
		[Col5967],
		[Col5968],
		[Col5969],
		[Col5970],
		[Col5971],
		[Col5972],
		[Col5973],
		[Col5974],
		[Col5975],
		[Col5976],
		[Col5977],
		[Col5978],
		[Col5979],
		[Col5980],
		[Col5981],
		[Col5982],
		[Col5983],
		[Col5984],
		[Col5985],
		[Col5986],
		[Col5987],
		[Col5988],
		[Col5989],
		[Col5990],
		[Col5991],
		[Col5992],
		[Col5993],
		[Col5994],
		[Col5995],
		[Col5996],
		[Col5997],
		[Col5998],
		[Col5999]
	FROM VW_FleetStatus fs WITH (NOLOCK)
    INNER JOIN VW_UnitLastChannelValueTimestamp vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
    LEFT JOIN ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
	LEFT JOIN EventChannelLatestValue eclv WITH (NOLOCK) ON fs.UnitID = eclv.UnitID
	LEFT JOIN @currentVehicleFault vf ON fs.VehicleID = vf.VehicleID

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_HistoricChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelValue AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_HistoricChannelValue
**  Description:    Returns latest record for each of the vehicles for UnitID
**                  in parameter. Stored procedure searches for last record in 
**                  last 24h before timestamp passed as parameter 
**  Call frequency: Every 5s 
**  Parameters:     @DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**                      to return last live record for unit
**                  @UnitID int 
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricChannelValue]
    @DateTimeStart datetime
    , @UnitID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @UnitID IS NULL
    BEGIN
        PRINT 'Unit Number not in a database'
        RETURN -1 
    END


    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, GETDATE())

    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        FaultUnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
    )
        
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
    FROM VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID  = @UnitID
        AND Category <> 'Test'
        AND ReportingOnly = 0
    GROUP BY FaultUnitID

    ;WITH CteLatestRecords 
    AS
    (
        SELECT 
            Vehicle.ID AS VehicleID
            , (
                SELECT TOP 1 TimeStamp
                FROM ChannelValue
                WHERE UnitID =    Vehicle.UnitID  /*TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
                    AND UnitID =    Vehicle.UnitID */
                ORDER BY TimeStamp DESC
            ) AS MaxChannelValueTimeStamp
        FROM Vehicle
        WHERE UnitID = @UnitID
    )   

    SELECT 
        UnitNumber  = v.UnitNumber
        , UnitID    = v.UnitID
        , Headcode  = fs.Headcode
        , Diagram   = fs.Diagram
        , VehicleID     = v.ID
        , VehicleType   = v.Type
        , VehicleTypeID = v.VehicleTypeID
        , VehicleNumber = v.VehicleNumber
        , FaultNumber   = ISNULL(vf.FaultNumber,0)
        , WarningNumber = ISNULL(vf.WarningNumber,0)
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE col3 BETWEEN MinLatitude AND MaxLatitude
                AND col4 BETWEEN MinLongitude AND MaxLongitude
                AND LocationArea.Type = 'C'
            ORDER BY Priority
        )
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,fs.FleetCode
        ,TimeStamp      = CAST(cv.[TimeStamp] AS datetime),

        -- Channel Data 
            [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
            [Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
            [Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
            [Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
            [Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
            [Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
            [Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
            [Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
            [Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
            [Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
            [Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
            [Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
            [Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
            [Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
            [Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
            [Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
            [Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
            [Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
            [Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
            [Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
            [Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
            [Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
            [Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
            [Col400],
            [Col401],
            [Col402],
            [Col403],
            [Col404],
            [Col405],
            [Col406],
            [Col407],
            [Col408],
            [Col409],
            [Col410],
            [Col411],
            [Col412],
            [Col413],
            [Col414],
            [Col415],
            [Col416],
            [Col417],
            [Col418],
            [Col419],
            [Col420],
            [Col421],
            [Col422],
            [Col423],
            [Col424],
            [Col425],
            [Col426],
            [Col427],
            [Col428],
            [Col429],
            [Col430],
            [Col431],
            [Col432],
            [Col433],
            [Col434],
            [Col435],
            [Col436],
            [Col437],
            [Col438],
            [Col439],
            [Col440],
            [Col441],
            [Col442],
            [Col443],
            [Col444],
            [Col445],
            [Col446],
            [Col447],
            [Col448],
            [Col449],
            [Col450],
            [Col451],
            [Col452],
            [Col453],
            [Col454],
            [Col455],
            [Col456],
            [Col457],
            [Col458],
            [Col459],
            [Col460],
            [Col461],
            [Col462],
            [Col463],
            [Col464],
            [Col465],
            [Col466],
            [Col467],
            [Col468],
            [Col469],
            [Col470],
            [Col471],
            [Col472],
            [Col473],
            [Col474],
            [Col475],
            [Col476],
            [Col477],
            [Col478],
            [Col479],
            [Col480],
            [Col481],
            [Col482],
            [Col483],
            [Col484],
            [Col485],
            [Col486],
            [Col487],
            [Col488],
            [Col489],
            [Col490],
            [Col491],
            [Col492],
            [Col493],
            [Col494],
            [Col495],
            [Col496],
            [Col497],
            [Col498],
            [Col499],
            [Col500],
            [Col501],
            [Col502],
            [Col503],
            [Col504],
            [Col505],
            [Col506],
            [Col507],
            [Col508],
            [Col509],
            [Col510],
            [Col511],
            [Col512],
            [Col513],
            [Col514],
            [Col515],
            [Col516],
            [Col517],
            [Col518],
            [Col519],
            [Col520],
            [Col521],
            [Col522],
            [Col523],
            [Col524],
            [Col525],
            [Col526],
            [Col527],
            [Col528],
            [Col529],
            [Col530],
            [Col531],
            [Col532],
            [Col533],
            [Col534],
            [Col535],
            [Col536],
            [Col537],
            [Col538],
            [Col539],
            [Col540],
            [Col541],
            [Col542],
            [Col543],
            [Col544],
            [Col545],
            [Col546],
            [Col547],
            [Col548],
            [Col549],
            [Col550],
            [Col551],
            [Col552],
            [Col553],
            [Col554],
            [Col555],
            [Col556],
            [Col557],
            [Col558],
            [Col559],
            [Col560],
            [Col561],
            [Col562],
            [Col563],
            [Col564],
            [Col565],
            [Col566],
            [Col567],
            [Col568],
            [Col569],
            [Col570],
            [Col571],
            [Col572],
            [Col573],
            [Col574],
            [Col575],
            [Col576],
            [Col577],
            [Col578],
            [Col579],
            [Col580],
            [Col581],
            [Col582],
            [Col583],
            [Col584],
            [Col585],
            [Col586],
            [Col587],
            [Col588],
            [Col589],
            [Col590],
            [Col591],
            [Col592],
            [Col593],
            [Col594],
            [Col595],
            [Col596],
            [Col597],
            [Col598],
            [Col599],
            [Col600],
            [Col601],
            [Col602],
            [Col603],
            [Col604],
            [Col605],
            [Col606],
            [Col607],
            [Col608],
            [Col609],
            [Col610],
            [Col611],
            [Col612],
            [Col613],
            [Col614],
            [Col615],
            [Col616],
            [Col617],
            [Col618],
            [Col619],
            [Col620],
            [Col621],
            [Col622],
            [Col623],
            [Col624],
            [Col625],
            [Col626],
            [Col627],
            [Col628],
            [Col629],
            [Col630],
            [Col631],
            [Col632],
            [Col633],
            [Col634],
            [Col635],
            [Col636],
            [Col637],
            [Col638],
            [Col639],
            [Col640],
            [Col641],
            [Col642],
            [Col643],
            [Col644],
            [Col645],
            [Col646],
            [Col647],
            [Col648],
            [Col649],
            [Col650],
            [Col651],
            [Col652],
            [Col653],
            [Col654],
            [Col655],
            [Col656],
            [Col657],
            [Col658],
            [Col659],
            [Col660],
            [Col661],
            [Col662],
            [Col663]
    FROM VW_Vehicle v 
    INNER JOIN CteLatestRecords     ON v.ID = CteLatestRecords.VehicleID
    LEFT JOIN ChannelValue cv       ON v.UnitID = cv.UnitID  AND CteLatestRecords.MaxChannelValueTimeStamp = cv.TimeStamp
    LEFT JOIN @currentVehicleFault vf ON v.UnitID = vf.FaultUnitID
    LEFT JOIN VW_FleetStatus fs     ON v.ID = fs.VehicleID

END

GO

------------------------------------------------------------
RAISERROR ('-- Create NSP_HistoricEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelValue AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_HistoricEventChannelValue
**  Description:    Returns historic event channel data
**  Call frequency: Every 5s for each vehicle shown on the unit channel data screen
**  Parameters:     @timestamp the timestamp or null to get the latest data
**                  @unitId the unit ID
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelValue] 
    @timestamp DATETIME2(3)
    ,@unitId INT
AS

BEGIN

SET NOCOUNT ON;
DECLARE  @timestampInternal DATETIME2(3)

	IF @timestamp IS NULL
		SET @timestampInternal = DATEADD(hh, 1, GETDATE())
	ELSE 
		SET @timestampInternal = @timestamp

	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.ChannelID
		FROM EventChannelValue ecv 
		WHERE ecv.UnitID = @unitId
			AND ecv.TimeStamp BETWEEN DATEADD(DAY, -1, SYSDATETIME()) AND @timestampInternal
		GROUP BY ecv.ChannelID
		)
	SELECT ecv.TimeStamp
	    ,ecv.UnitID
		,ecv.ChannelID
		,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN EventChannelValue ecv 
		ON ecvl.TimeStamp = ecv.TimeStamp
		AND ecvl.ChannelID = ecv.ChannelID
        AND ecv.UnitID = @unitId
END
GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_FleetSummaryDrilldown', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummaryDrilldown' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummaryDrilldown AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_FleetSummaryDrilldown
**	Description:	Returns live channel data for the application
**	Call frequency:	When user click on grouped cell on Fleet Summary
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
	@UnitIdList varchar(30)
	, @SelectColList varchar(4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @sql varchar(max)
	
	SET @sql = '
SELECT DISTINCT
	cv.UnitID
	, ' + @SelectColList + '
	, fs.UnitPosition
FROM VW_FleetStatus fs
INNER JOIN VW_UnitLastChannelValueTimestamp vlc ON vlc.UnitID = fs.UnitID
LEFT JOIN ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
LEFT JOIN EventChannelLatestValue eclv WITH (NOLOCK) ON vlc.UnitID = eclv.UnitID
WHERE fs.UnitID in (' + @UnitIdList + ')
ORDER BY 
	fs.UnitPosition'

	EXEC (@sql)
	
END

GO

------------------------------------------------------------
RAISERROR ('-- Create NSP_HistoricEventChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelData AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_HistoricEventChannelData
**  Description:    Returns historic event channel data
**  Call frequency: Every 5s for each vehicle shown on the data plots screen
**  Parameters:     @DateTimeEnd end timestamp
**                  @Interval amount of data to return in seconds
**                  @VehicleID the vehicle ID
**                  @SelectColList channel IDs to return separated by commas
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelData]
    @DateTimeEnd DATETIME
    ,@Interval INT -- number of seconds
    ,@UnitID INT
    ,@SelectColList VARCHAR(4000)
AS

BEGIN
    SET NOCOUNT ON;

    DECLARE @sql varchar(max)

    IF @DateTimeEnd IS NULL
        SET @DateTimeEnd = SYSDATETIME()

    DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)

    DECLARE @vehicleTypeID int = (SELECT VehicleTypeID FROM VW_Vehicle WHERE ID = @UnitID)

    SET @sql = '
;WITH EventChannelValueLatest
AS (
	SELECT MAX(ecv.TimeStamp) AS TimeStamp
		,ecv.ChannelID
	FROM EventChannelValue ecv
	WHERE ecv.UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
		AND ecv.ChannelID IN (' + @SelectColList + ')
		AND ecv.TimeStamp BETWEEN DATEADD(DAY, - 1, ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''') AND ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + '''
	GROUP BY ecv.ChannelID
	)
SELECT ecv.TimeStamp
,ecv.ChannelID
,ecv.Value
,ecv.UnitID
FROM EventChannelValueLatest ecvl
JOIN EventChannelValue ecv
	ON ecv.UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
	AND ecv.ChannelID = ecvl.ChannelID
	AND ecv.TimeStamp = ecvl.TimeStamp
UNION ALL
SELECT
    TimeStamp = CAST(TimeStamp AS DATETIME)
    ,ChannelID
    ,Value
	,UnitID
FROM EventChannelValue
WHERE
    UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
    AND ChannelID IN (' + @SelectColList + ')' + '
    AND Timestamp BETWEEN ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(VARCHAR(23), @DateTimeEnd, 121)  + ''''

    EXEC (@sql)
END
GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelData AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultChannelData
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelData]
(
	@FaultID int
	, @UnitID int
)
AS
BEGIN

		SELECT
			cv.[ID]
			,cv.[FaultID]
			,cv.[UpdateRecord]
			,cv.[RecordInsert]
			,cv.[TimeStamp]
			,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
            [Col400],
            [Col401],
            [Col402],
            [Col403],
            [Col404],
            [Col405],
            [Col406],
            [Col407],
            [Col408],
            [Col409],
            [Col410],
            [Col411],
            [Col412],
            [Col413],
            [Col414],
            [Col415],
            [Col416],
            [Col417],
            [Col418],
            [Col419],
            [Col420],
            [Col421],
            [Col422],
            [Col423],
            [Col424],
            [Col425],
            [Col426],
            [Col427],
            [Col428],
            [Col429],
            [Col430],
            [Col431],
            [Col432],
            [Col433],
            [Col434],
            [Col435],
            [Col436],
            [Col437],
            [Col438],
            [Col439],
            [Col440],
            [Col441],
            [Col442],
            [Col443],
            [Col444],
            [Col445],
            [Col446],
            [Col447],
            [Col448],
            [Col449],
            [Col450],
            [Col451],
            [Col452],
            [Col453],
            [Col454],
            [Col455],
            [Col456],
            [Col457],
            [Col458],
            [Col459],
            [Col460],
            [Col461],
            [Col462],
            [Col463],
            [Col464],
            [Col465],
            [Col466],
            [Col467],
            [Col468],
            [Col469],
            [Col470],
            [Col471],
            [Col472],
            [Col473],
            [Col474],
            [Col475],
            [Col476],
            [Col477],
            [Col478],
            [Col479],
            [Col480],
            [Col481],
            [Col482],
            [Col483],
            [Col484],
            [Col485],
            [Col486],
            [Col487],
            [Col488],
            [Col489],
            [Col490],
            [Col491],
            [Col492],
            [Col493],
            [Col494],
            [Col495],
            [Col496],
            [Col497],
            [Col498],
            [Col499],
            [Col500],
            [Col501],
            [Col502],
            [Col503],
            [Col504],
            [Col505],
            [Col506],
            [Col507],
            [Col508],
            [Col509],
            [Col510],
            [Col511],
            [Col512],
            [Col513],
            [Col514],
            [Col515],
            [Col516],
            [Col517],
            [Col518],
            [Col519],
            [Col520],
            [Col521],
            [Col522],
            [Col523],
            [Col524],
            [Col525],
            [Col526],
            [Col527],
            [Col528],
            [Col529],
            [Col530],
            [Col531],
            [Col532],
            [Col533],
            [Col534],
            [Col535],
            [Col536],
            [Col537],
            [Col538],
            [Col539],
            [Col540],
            [Col541],
            [Col542],
            [Col543],
            [Col544],
            [Col545],
            [Col546],
            [Col547],
            [Col548],
            [Col549],
            [Col550],
            [Col551],
            [Col552],
            [Col553],
            [Col554],
            [Col555],
            [Col556],
            [Col557],
            [Col558],
            [Col559],
            [Col560],
            [Col561],
            [Col562],
            [Col563],
            [Col564],
            [Col565],
            [Col566],
            [Col567],
            [Col568],
            [Col569],
            [Col570],
            [Col571],
            [Col572],
            [Col573],
            [Col574],
            [Col575],
            [Col576],
            [Col577],
            [Col578],
            [Col579],
            [Col580],
            [Col581],
            [Col582],
            [Col583],
            [Col584],
            [Col585],
            [Col586],
            [Col587],
            [Col588],
            [Col589],
            [Col590],
            [Col591],
            [Col592],
            [Col593],
            [Col594],
            [Col595],
            [Col596],
            [Col597],
            [Col598],
            [Col599],
            [Col600],
            [Col601],
            [Col602],
            [Col603],
            [Col604],
            [Col605],
            [Col606],
            [Col607],
            [Col608],
            [Col609],
            [Col610],
            [Col611],
            [Col612],
            [Col613],
            [Col614],
            [Col615],
            [Col616],
            [Col617],
            [Col618],
            [Col619],
            [Col620],
            [Col621],
            [Col622],
            [Col623],
            [Col624],
            [Col625],
            [Col626],
            [Col627],
            [Col628],
            [Col629],
            [Col630],
            [Col631],
            [Col632],
            [Col633],
            [Col634],
            [Col635],
            [Col636],
            [Col637],
            [Col638],
            [Col639],
            [Col640],
            [Col641],
            [Col642],
            [Col643],
            [Col644],
            [Col645],
            [Col646],
            [Col647],
            [Col648],
            [Col649],
            [Col650],
            [Col651],
            [Col652],
            [Col653],
            [Col654],
            [Col655],
            [Col656],
            [Col657],
            [Col658],
            [Col659],
            [Col660],
            [Col661],
            [Col662],
            [Col663]
		FROM dbo.FaultChannelValue cv
		WHERE cv.FaultID = @FaultID
			AND cv.UnitID = @UnitID

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultEventChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelData AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_GetFaultEventChannelData
**  Description:    Returns event based channel data for event detail.
**                  Used together with NSP_GetFaultChannelData
**  Call frequency: Called by event detail service
**  Parameters:     @FaultID int
**                  @UnitID int
**  Return values:  FaultEventChannelValue.UnitID
**                  FaultEventChannelValue.TimeStamp
**                  FaultEventChannelValue.ChannelID
**                  FaultEventChannelValue.Value
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelData]
    @FaultID int
    ,@UnitID int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT
        UnitID
        ,TimeStamp
        ,ChannelID
        ,Value
    FROM FaultEventChannelValue
    WHERE FaultID = @FaultID AND UnitID = @UnitID
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,038
           ,'update_spectrum_db_038.sql'
           ,'1.1.02')
GO