SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 048
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO
RAISERROR ('-- update NSP_CalculateActualRun', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CalculateActualRun' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CalculateActualRun] AS BEGIN RETURN(1) END;')
GO
/****** Object: StoredProcedure [dbo].[NSP_CalculateActualRun]	Script Date: 03/02/2017 12:01:49 ******/

/******************************************************************************
**	Name:			NSP_CalculateActualRun
**	Description:	SP runs Actual Run analysis for Ops Analysis
**	Call frequency:	Once a day. Run by SQL Agent at 3AM
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/04/26 07:39:20 $$
**	$$Revision: 1.3 $$
**	$$Source: /home/cvs/spectrum/eastcoast/src/main/database/update_spectrum_db_068.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-03-28		RT		Changing isoation level, adding TRY/CATCH block	
**	2012-04-25		RT		Fix detection of headcodes running over midnight					
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_CalculateActualRun]
AS
BEGIN	

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @permanentJourneyID int
	DECLARE @actualJourneyID int
	DECLARE @headcode varchar(10)
	DECLARE @dateValue date
	DECLARE @msg varchar(200)
	DECLARE @counter int
	
	-- Flag rows to be processed:
	-- This are unprocessed rows in last 30 days
	UPDATE JourneyDate SET
		RunActualRunAnalysis = 1
	FROM JourneyDate
	INNER JOIN Journey ON Journey.ID = JourneyDate.ActualJourneyID
	WHERE RunActualRunAnalysis IS NULL
		AND DateValue > DATEADD(D, -30, GETDATE())
		AND 
		(CASE 
			WHEN StartTime < EndTime	-- headcode not running over midnight
				THEN CAST(DateValue AS datetime) + CAST(EndTime AS DATETIME)
			ELSE DATEADD(d, 1, CAST(DateValue AS datetime) + CAST(EndTime AS DATETIME))
		END < DATEADD(hh, -2, GETDATE()))


	WHILE 1=1
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM JourneyDate WHERE RunActualRunAnalysis = 1)
			BREAK
		----------------------------------------------

		SELECT TOP 1
			@permanentJourneyID = PermanentJourneyID
			, @actualJourneyID = ActualJourneyID
			, @headcode = Headcode
			, @dateValue = DateValue
		FROM JourneyDate
		INNER JOIN Journey ON Journey.ID = PermanentJourneyID
		WHERE RunActualRunAnalysis = 1
		ORDER BY DateValue, Headcode
		
		SET @counter = ISNULL(@counter, 0) + 1
		SET @msg = CAST(SYSDATETIME() AS varchar(30)) + ': ' + CAST(@counter AS CHAR(10)) + ': ' + ISNULL(@headcode, 'NULL') + ': EXEC NSP_CalculateActualRunPerHeadcode ' + CAST(@actualJourneyID AS varchar(10)) + ', ''' + CAST(@dateValue AS varchar(10)) + ''''
		RAISERROR(@msg, 10, 1) WITH NOWAIT
		
		-- Run Actual Run Analysis Per Headcode 
		BEGIN TRY
		
			UPDATE JourneyDate SET 
				RunActualRunAnalysis = 0
				, TimestampAnalysisStarted = GETDATE()
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			EXEC NSP_CalculateActualRunPerHeadcode @actualJourneyID, @dateValue
			
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = GETDATE()
				, AnalysedSuccessfully = 1
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			PRINT 'Processed'
			
		END TRY
		BEGIN CATCH
		
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = GETDATE()
				, AnalysedSuccessfully = 0
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue

			SET @msg = '*** ERROR WHEN PROCESSING ' + ISNULL(@headcode, 'NULL') + ' ***'
			RAISERROR(@msg, 10, 1) WITH NOWAIT

		END CATCH
		
	END

END

GO
RAISERROR ('-- update NSP_CalculateActualRunPerHeadcode', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CalculateActualRunPerHeadcode' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CalculateActualRunPerHeadcode] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_CalculateActualRunPerHeadcode] AS BEGIN RETURN(1) END;

GO
RAISERROR ('-- update NSP_CifImportGetNextFilename', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CifImportGetNextFilename' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CifImportGetNextFilename] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_CifImportGetNextFilename
**	Description:	Generates name of CIF file that supposet to be processed 
**					next
**	Call frequency:	Run from SSIS Package for CIF Timetable Interface.
**	Parameters:		@FileNameInitial - name of first file to be porcessed 
**	Return values:	@FileNameNext - name of the next file to be processed
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-04-11		RT		creation on database
*******************************************************************************/
ALTER PROCEDURE [dbo].[NSP_CifImportGetNextFilename]
(
	@FileNameInitial varchar(20)
	, @FileNameNext varchar(20) OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @fileLast varchar(20)

	-- get latest successfully processed file (without .cif extention)
	SET @fileLast = (
		SELECT TOP 1 LEFT(OriginalFileName, CHARINDEX('.', OriginalFileName) - 1) 
		FROM LoadCifFile
		WHERE Status = 'Processed'
		ORDER BY FileID DESC)

	IF @FileNameInitial IS NOT NULL
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			LEFT(@fileLast, LEN(@fileLast) -1 )
			+ CASE RIGHT(@fileLast, 1)
				WHEN 'Z' THEN 'A'
				ELSE CHAR(ASCII(RIGHT(@fileLast, 1)) + 1)
			END
	END
	ELSE
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			CASE 
				WHEN CHARINDEX('.', @FileNameInitial) > 1 
					THEN LEFT(@FileNameInitial, CHARINDEX('.', @FileNameInitial) - 1) 
				ELSE @FileNameInitial
			END
	END

	-- convert generated filename to upper-cases
	SET @FileNameNext = UPPER(@FileNameNext)
	
END -- stored procedure
GO
RAISERROR ('-- update NSP_CifImportParseData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CifImportParseData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CifImportParseData] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_CifImportParseData]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @sqlQuery varchar(max)
	--------------
	-- HD Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineHD') IS NOT NULL
		DROP TABLE ##CifLineHD

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'HD' AND Name <> 'Spare'
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineHD
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''HD''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BS Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBS') IS NOT NULL
		DROP TABLE ##CifLineBS

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BS' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBS
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BS''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BX Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBX') IS NOT NULL
		DROP TABLE ##CifLineBX

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BX' AND Name NOT IN('Spare', 'Reserved field')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBX
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BX''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LO Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLO') IS NOT NULL
		DROP TABLE ##CifLineLO

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LO' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLO
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LO''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- LI Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLI') IS NOT NULL
		DROP TABLE ##CifLineLI

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LI' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLI
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LI''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- CR Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineCR') IS NOT NULL
		DROP TABLE ##CifLineCR

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'CR' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineCR
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''CR''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LT Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLT') IS NOT NULL
		DROP TABLE ##CifLineLT

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LT' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLT
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LT''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--Add indexes
	CREATE NONCLUSTERED INDEX IX_LI ON [dbo].[##CifLineLI] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LO ON [dbo].[##CifLineLO] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LT ON [dbo].[##CifLineLT] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BS ON [dbo].[##CifLineBS] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BX ON [dbo].[##CifLineBX] ([FileLineNumber])

END
GO

RAISERROR ('-- update NSP_CifImportPopulateSectionData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CifImportPopulateSectionData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CifImportPopulateSectionData] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_CifImportPopulateSectionData
**	Description:	Populate Section and Segment data based on Section Points 
**					loaded from CIF file
**	Call frequency:	Run for each new timetable
**	Parameters:		@JourneyID 
**	Return values:	@FileNameNext - name of the next file to be processed
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:			Author:	Description:
**	2012-04-11		RT		creation on database
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_CifImportPopulateSectionData]
	@JourneyID int
AS
BEGIN
	SET NOCOUNT ON;

	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)

	INSERT INTO [Segment]
		([JourneyID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime]
		,[IdealRunTime])
	SELECT 
		[JourneyID]
		, SegmentID
		, (SELECT StartLocationID FROM CteJourtneySegment cjs WHERE cjs.ID = MIN(CteJourtneySegment.ID)) -- StartSectionID
		, (SELECT EndLocationID FROM CteJourtneySegment cjs WHERE cjs.ID = MAX(CteJourtneySegment.ID)) -- EndSectionID
		, MIN([StartTime])		AS [StartTime]
		, MAX([EndTime])		AS [EndTime]
		, SUM(ISNULL([RecoveryTime], 0))	AS [RecoveryTime]
		, NULL					AS [IdealRunTime]
	FROM CteJourtneySegment
	GROUP BY 
		[JourneyID]
		, SegmentID


	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)
	INSERT INTO [Section]
		([JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime])
	SELECT
		[JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,ISNULL([RecoveryTime], 0)
	FROM CteJourtneySegment
	
END
GO

RAISERROR ('-- update NSP_CifImportProcessFile', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_CifImportProcessFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_CifImportProcessFile] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_CifImportPopulateSectionData
**	Description:	Process a Cif Timetable file, after it is loaded into 
**					staging table by SSIS package
**	Call frequency:	Run for each new timetable
**	Parameters:		@FileID - FileID from LoadCifFile
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_CifImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	SET DATEFORMAT mdy;
	SET datefirst 7;
	-- operaor code - only records with this code will be processed:
	-- SR	- Scotrail
	-- GR	- Eastcoast
	-- SW	- SWT
	DECLARE @atocCode varchar(10) = 'SR'
	
	------------------------------------------------------------------------------
	-- Check file status, porcess only if status is imported
	------------------------------------------------------------------------------
	IF @FileID IS NOT NULL AND (SELECT Status FROM LoadCifFile WHERE FileID = @FileID) <> 'Imported'
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Error - File status different than Imported' WHERE FileID = @FileID
		RAISERROR('Error - File status different than Imported', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Processing' WHERE FileID = @FileID
	END

	------------------------------------------------------------------------------
	-- Parse file data into a set or temporary tables, one for each row type 
	------------------------------------------------------------------------------
	DECLARE @spResult int 
	EXEC @spResult = NSP_CifImportParseData @FileID

	IF @spResult <> 0
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Error - Error when parsing file' WHERE FileID = @FileID
		PRINT 'Error - Error when parsing file'
		RAISERROR('Error - Error when parsing file', 16, 1)
		RETURN
	END

	------------------------------------------------------------------------------
	-- Reset Status column
	------------------------------------------------------------------------------
	UPDATE dbo.LoadCifFileData SET 
		Status = NULL
	WHERE FileID = @FileID

	------------------------------------------------------------------------------
	-- Check this is correct file, not 1 month old
	------------------------------------------------------------------------------
	DECLARE @fileType char(1) -- F - Full, U - update
	DECLARE @dateOfExtract char(6)

	SELECT 
		@fileType = [Bleed-off/UpdateInd]
		, @dateOfExtract = DateofExtract
	FROM ##CifLineHD

/*
	IF DATEDIFF(d, CAST('20'+ SUBSTRING(@dateOfExtract, 5,2) + '-' + SUBSTRING(@dateOfExtract, 3,2) + '-' + SUBSTRING(@dateOfExtract, 1,2) AS date), GETDATE()) > 20 
	BEGIN
		UPDATE LoadCifFile SET Status = 'Error - File older than 20 days' WHERE FileID = @FileID
		PRINT 'Error - File older than 20 days'
		RAISERROR('Error - File older than 20 days', 16, 1)
		RETURN
	END
*/

	------------------------------------------------------------------------------
	-- Delete all records from today going forward (for Full extract only)
	------------------------------------------------------------------------------
	IF @fileType = 'F' 
	BEGIN
		DELETE FROM dbo.JourneyDate
		WHERE DateValue >= CAST(GETDATE() AS date)
	END		
	
	------------------------------------------------------------------------------
	-- Start import:
	------------------------------------------------------------------------------
	DECLARE @bsLine int
	
	IF OBJECT_ID('tempdb..#tempJourney') IS NOT NULL
		DROP TABLE #tempJourney

	CREATE TABLE #tempJourney 
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[TrainUID] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
		[Headcode] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartLocationID] [int] NULL,
		[EndLocationID] [int] NULL,
		[StartTime] [time](0) NULL,
		[EndTime] [time](0) NULL,
		[ValidFrom] [datetime] NOT NULL,
		[ValidTo] [datetime] NOT NULL,
		[DaysRun] [char](7) COLLATE Latin1_General_CI_AS NOT NULL,
		[StpType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	IF OBJECT_ID('tempdb..#tempSectionPoint') IS NOT NULL
		DROP TABLE #tempSectionPoint

	CREATE TABLE #tempSectionPoint
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LocationID] [int] NOT NULL,
		[LocationSuffix] [tinyint] NOT NULL,
		[ScheduledArrival] [time](0) NULL,
		[ScheduledDeparture] [time](0) NULL,
		[ScheduledPass] [time](0) NULL,
		[PublicArrival] [time](0) NULL,
		[PublicDeparture] [time](0) NULL,
		[RecoveryTime] [int] NOT NULL,
		[SectionPointType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	-- loop on BS records
	WHILE 1=1
	BEGIN
		SET @bsLine = (SELECT MIN(FileLineNumber) FROM ##CifLineBS WHERE FileLineNumber > ISNULL(@bsLine, 0))
		IF @bsLine IS NULL 
			BREAK
		-----------------------------------------------------------------------
		DECLARE @bsTransactionType char(1) = (SELECT TransactionType FROM ##CifLineBS WHERE FileLineNumber = @bsLine)

		DECLARE @headcode varchar(10) = NULL
		DECLARE @dateRunsFrom date = NULL
		DECLARE @dateRunsTo date = NULL
		DECLARE @daysRun char(7) = NULL
		DECLARE @bankHolidayRunning char(1) = NULL
		DECLARE @journeyID int = NULL
		DECLARE @stpType char(1) = NULL
		DECLARE @trainUID char(6) = NULL
		DECLARE @printOut varchar(200) = NULL
		DECLARE @logEntry varchar(200) = NULL
		DECLARE @existingJourneyID int = NULL
		DECLARE @useExistingJourneyID bit = NULL

		SELECT 
			@headcode = TrainIdentity
			, @dateRunsFrom = DateRunsFrom
			, @dateRunsTo = DateRunsTo
			, @daysRun = DaysRun
			, @bankHolidayRunning = BankHolidayRunning
			, @stpType = STPIndicator
			, @trainUID = TrainUID
		FROM ##CifLineBS 
		WHERE FileLineNumber = @bsLine

		SET @printOut = CAST(@bsLine AS char(6))
			 + ' | ' + CAST(@trainUID AS char(8))
			 + ' | ' + CAST(@headcode as char(8))
			 + ' | ' + CAST(@dateRunsFrom as char(12))
			 + ' | ' + CAST(@dateRunsTo as char(10))
			 + ' | ' + CAST(@daysRun as char(7))
			 + ' | ' + CAST(@bankHolidayRunning as char(9))
			 + ' | ' + CAST(@bsTransactionType as char(7))
			 + ' | ' + CAST(@stpType as char(7))
			 + ' | '
		
		IF @bsTransactionType NOT IN ('N') -- Delete
		BEGIN
			SET @logEntry = @printOut + 'Not processed - Transaction Type not supported'
			UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
		END

		IF @bsTransactionType IN ('N') -- New
		BEGIN 

			-- C - Stp cancellation of Permanent Schedule
			IF @stpType = 'C' 
			BEGIN
				
				UPDATE dbo.JourneyDate SET
					ActualJourneyID = NULL
				FROM JourneyDate
				INNER JOIN dbo.Journey ON Journey.ID = PermanentJourneyID
				WHERE TrainUID = @trainUID
					AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))

				SET @logEntry = @printOut + 'Stp cancellation of Permanent Schedule. Updated: ' + STR(@@ROWCOUNT)

				UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
						
			END
			
			-- N - New Stp Schedule (not an overlay)
			IF @stpType = 'N'
			BEGIN
				SET @logEntry = @printOut + 'No Action: New Stp Schedule (not an overlay)'

				UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
				
			END

			-- P - New Pemanenet
			-- O - New Overlay Schedule
			IF @stpType IN ('P', 'O')
			BEGIN
			
				-- Check if Headcode is for required ATOC Operator
				IF (SELECT ATOCCode FROM ##CifLineBX WHERE FileLineNumber = @bsLine + 1) <> @atocCode
				BEGIN
					SET @logEntry = @printOut + 'Headcode for another operator'
					UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
					CONTINUE
				END 
				
				TRUNCATE TABLE #tempJourney
				TRUNCATE TABLE #tempSectionPoint

				INSERT INTO #tempJourney
					([TrainUID]
					,[Headcode]
					,[ValidFrom]
					,[ValidTo]
					,[DaysRun]
					,[StpType])
				SELECT
					TrainUID		--[TrainUID]
					,TrainIdentity	--[Headcode]
					,DateRunsFrom	--[ValidFrom]
					,DateRunsTo		--[ValidTo]
					,DaysRun		--[DaysRun]
					,STPIndicator	--[StpType]
				FROM ##CifLineBS 
				WHERE FileLineNumber = @bsLine


					-- loop on all lines for a Headcode (Journey)
					DECLARE @lineNumber int = @bsLine
					
					WHILE 1 = 1
					BEGIN
					
						SET @lineNumber = @lineNumber + 1
						DECLARE @lineType char(2) = (SELECT LEFT(Data, 2) FROM dbo.LoadCifFileData WHERE FileID = @FileID AND FileLineNumber = @lineNumber)
						
						IF @lineType IN ('ZZ','BS') -- ZZ - end of file, BS - next headcode
						BREAK
						-----------------------------------------
						
						IF @lineType NOT IN ('LO', 'LU', 'LI', 'CR', 'LT') -- we are interested only in these lines
						CONTINUE
						-----------------------------------------
						
						DECLARE @tiploc varchar(8)
						DECLARE @locationID int
						
						-- Origin Location
						IF @lineType = 'LO'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLO WHERE FileLineNumber = @lineNumber)

							EXEC dbo.NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								,0			--[LocationSuffix] 
								,NULL		--[ScheduledArrival]
								,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
								,NULL		--[ScheduledPass]
								,NULL		--[PublicArrival]
								,dbo.FN_ConvertCifTime([PublicDeparture])	--[PublicDeparture]
									,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)	--[RecoveryTime]
								,'B'			--[SectionPointType]
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
							
							UPDATE #tempJourney SET
								StartLocationID = @locationID
								, StartTime = dbo.FN_ConvertCifTime([ScheduledDeparture]) 
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
			 
						END
						
						-- Intermediate Location
						IF @lineType = 'LI'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLI WHERE FileLineNumber = @lineNumber)

							EXEC dbo.NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT

							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								, CASE 
									WHEN ISNUMERIC(LocationSuffix) = 1 
										THEN LocationSuffix
									ELSE 1
								END
								,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
								,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
								,dbo.FN_ConvertCifTime([ScheduledPass])		--[ScheduledPass]
								,CASE 
									WHEN [PublicArrival] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicArrival])
								END			--[PublicArrival]
								,CASE 
									WHEN [PublicDeparture] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicDeparture])
								END			--[PublicDeparture]
								,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)
								--[RecoveryTime]
								,CASE 
									WHEN LEN([ScheduledPass]) > 0 THEN 'T'	--Timing Point
									ELSE 'S'	--Station
								END			--[SectionPointType]
							FROM ##CifLineLI
							WHERE FileLineNumber = @lineNumber

						END

						-- Terminating Location
						IF @lineType = 'LT'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLT WHERE FileLineNumber = @lineNumber)

							EXEC NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								,0			--[LocationSuffix] 
								,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
								,NULL		--[ScheduledDeparture]
								,NULL		--[ScheduledPass]
								,dbo.FN_ConvertCifTime([PublicArrival])		--[PublicArrival]
								,NULL		--[PublicDeparture]
								,0			--[RecoveryTime]
								,'E'			--[SectionPointType]
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber

							UPDATE #tempJourney SET
								EndLocationID = @locationID
								, EndTime = dbo.FN_ConvertCifTime([ScheduledArrival])
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber
								
							BREAK -- We dont need any more records for this headcode
						END			
						
					END --WHILE -- loop on all lines for a Headcode (Journey)

					-- Check if journey is already in database
					SET @existingJourneyID = (
						SELECT ID
						FROM Journey 
						WHERE 
							[Headcode] = @headcode
							AND [ValidFrom] = @dateRunsFrom
							AND [ValidTo] = @dateRunsTo
							AND [TrainUID] = @trainUID
							AND [DaysRun] = @daysRun
							AND [StpType] = @stpType
					)
					
					-- if exists check if section points are the same
					IF @existingJourneyID IS NOT NULL 
					BEGIN

							
						-- delete JourneyDate records if not a full extract
						IF @fileType <> 'F'
						BEGIN
							
							DELETE dbo.JourneyDate
							WHERE PermanentJourneyID = @existingJourneyID
								AND DateValue >= CAST(GETDATE() AS date)
							
						END	
						
							
						IF NOT EXISTS 
						(
							SELECT 1 
							FROM
							(
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
								FROM dbo.[SectionPoint]
								WHERE [JourneyID] = @existingJourneyID
								
								UNION ALL
								
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]				
								FROM #tempSectionPoint

							) tmp
							GROUP BY 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
							HAVING COUNT(*) = 1
						)
						-- Jounreys in CIF and in DB are identincal:
						BEGIN
							SET @useExistingJourneyID = 1
							
							-- update @journeyID with existing ID
							SET @journeyID = @existingJourneyID
						
							UPDATE dbo.Journey SET 
								LastFileID = @FileID
							WHERE ID = @journeyID
							
						END
						ELSE
						-- Jounreys in CIF and in DB are different:
						BEGIN
							SET @useExistingJourneyID = 0

							-- close existing Journey
							UPDATE dbo.Journey SET 
								[ValidTo] = GETDATE()
							WHERE ID = @existingJourneyID
							
						END
						
					END
					ELSE
					BEGIN
						SET @useExistingJourneyID = 0
					END --IF @existingJourneyID IS NOT NULL 

					IF @useExistingJourneyID = 0
					BEGIN
						-- insert Journey read from Cif file
					
						-- Populate Journey table
						INSERT INTO dbo.[Journey]
							([Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,[FileID]
							,[LineNumber]
							,[HeadcodeDesc]
							,[LastFileID])
						SELECT
							[Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,@FileID
							,@bsLine
							,@headcode + ' (' + [dbo].[FN_GetDaysRunDescription](DaysRun) + ')'
							,@FileID
						FROM #tempJourney

						SET @journeyID = @@IDENTITY

						-- Populate SectionPoint table
						INSERT INTO dbo.[SectionPoint]
							([JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType])
						SELECT
							@journeyID --[JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType]
						FROM #tempSectionPoint
						
						-- Populate Segment and Section 
						EXEC dbo.NSP_CifImportPopulateSectionData @journeyID

					END -- ELSE - IF @useExistingJourneyID = 1

					-- Populate HeadcodeID
					IF @stpType = 'P'
					BEGIN

						-- for existing JourneyID populate only dates from today forward
						INSERT INTO dbo.JourneyDate
							([PermanentJourneyID]
							,[DateValue]
							,[ActualJourneyID])
						SELECT
							@journeyID
							,DateValue
							,@journeyID
						FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun)
						WHERE DateValue >= CAST(GETDATE() AS date) OR @useExistingJourneyID = 0

						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Perm Sched, Existing JourneyID:' 
								ELSE 'Perm Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
						
					END
					ELSE -- @stpType = '0'
					BEGIN
						
						-- for existing JourneyID populate only dates from today forward
						UPDATE dbo.JourneyDate SET
							[ActualJourneyID] = @journeyID
						FROM dbo.JourneyDate
						INNER JOIN dbo.Journey ON Journey.ID = PermanentJourneyID
						WHERE TrainUID = @trainUID
							AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))
							AND dbo.FN_CompareJorney(PermanentJourneyID, @journeyID) = 0
							AND (DateValue >= CAST(GETDATE() AS date) OR @useExistingJourneyID = 0)
					
						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Over Sched, Existing JourneyID:' 
								ELSE 'Over Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days updated: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
							
					END
					
			END -- IF @stpType IN ('P', 'O') 

		END -- IF @bsTransactionType IN ('N')
		
	END
	
	-- File was fully processed
	UPDATE dbo.LoadCifFile SET Status = 'Processed' WHERE FileID = @FileID

END -- end stored procedure
GO

RAISERROR ('-- update NSP_DataArchiving', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DataArchiving' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DataArchiving] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DataArchiving]
(
	@StopDate as date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @outputChannelValue TABLE(
		ID bigint);
	
	DECLARE @outputFault TABLE(
		ID bigint);

	
	DECLARE 
		 @batch int = 10000
		,@loopmax int = 10000
		,@archived int = 0
		,@stop datetime2 
		,@tempStop datetime2
		,@start datetime2 
		,@id int
		,@d datetime2
		,@t datetime2 = SYSDATETIME()
		,@cnt int 
		,@loop int = 0
		,@unitid int = 0
		,@msg varchar(1000)
		,@StartTime datetime2 = GETDATE();


	WHILE 1 = 1
	BEGIN

		SET @start = (SELECT MIN(timestamp)	FROM dbo.ChannelValue)

		SELECT
			@stop = @StopDate 
			, @t = SYSDATETIME()
			, @archived = 0
			, @tempStop = DATEADD(N, 5, @start)

		IF @tempStop > @stop
		BEGIN

			INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,'Data Archiving Finished')
			BREAK;

		END
	


	--	SET @msg = 'Start: ' + convert(varchar, @start, 121)+ '; Stop: ' +convert(varchar, @stop, 121) + '; TempStop: ' + convert(varchar, @tempStop, 121)

	--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
	--		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,@msg)
	
		

		---------------------------------------------------------------------------
		-- Fault and FaultChannelValue tables
		---------------------------------------------------------------------------
		BEGIN TRAN

		BEGIN TRY

			SET @archived = 0
			DELETE @outputFault

			INSERT NedTrain_Spectrum_Archive.dbo.Fault_Archive
			OUTPUT INSERTED.ID
				INTO @outputFault
			SELECT TOP (@batch / 10) * FROM dbo.Fault 
			WHERE CreateTime <= DATEADD(n, 5, @tempStop)
				AND CreateTime <= @stop
			ORDER BY 
				CreateTime ASC
				, ID ASC;
		
			SET @archived = @@ROWCOUNT;

			IF @archived > 0
			BEGIN

				INSERT NedTrain_Spectrum_Archive.dbo.FaultChannelValue_Archive
				SELECT *
				FROM dbo.FaultChannelValue fcv 
				WHERE FaultID IN (SELECT ID FROM @outputFault)

				DELETE dbo.FaultChannelValue
				WHERE FaultID IN (SELECT ID FROM @outputFault)
			
				DELETE dbo.Fault
				WHERE ID IN (SELECT ID FROM @outputFault)
					AND CreateTime <= DATEADD(n, 5, @tempStop)
			
			END
		
			COMMIT;
		
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK;
			EXEC dbo.nsp_RethrowError;
			RETURN 1
		END CATCH

		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',@archived,@loop,'Faults and FaultChannelValues: ' + convert(varchar(10), @archived) 
			+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms')
	



		---------------------------------------------------------------------------
		-- ChannelValue table
		---------------------------------------------------------------------------
		BEGIN TRAN

		BEGIN TRY
	
			DELETE @outputChannelValue
		
			INSERT NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive
			OUTPUT INSERTED.ID
				INTO @outputChannelValue
			SELECT TOP(@batch) * 
			FROM dbo.ChannelValue 
			WHERE TimeStamp <= @tempStop
			ORDER BY TimeStamp asc;
		
			DELETE
			FROM dbo.ChannelValue
			WHERE ID IN (SELECT ID FROM @outputChannelValue)
		
			SELECT @archived = @@ROWCOUNT;
		
			COMMIT;
		
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK;
			EXEC dbo.nsp_RethrowError;
			RETURN 1
		END CATCH

			INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',@archived,@loop,'ChannelValue:				 ' + convert(varchar(10), @archived)
			+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms')


		WHILE @@TRANCOUNT > 0
			ROLLBACK;


	--	SET @msg = 'Iteration: ' + convert(varchar, @loop)
	--		+ ';		Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, getdate())) + ' ms'
	--		+ ';		Open Transactions: ' + convert(varchar(10), @@trancount);

	--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
	--		VALUES(@StartTime , GETDATE(),'NSP_DataArchiving',0,@loop,@msg)
		------------------------------------------------------------------------------
		SET @loop = @loop + 1
		IF @loop > @loopmax
			BREAK

		WAITFOR DELAY '00:00:02'

	END

END

GO

RAISERROR ('-- update NSP_DataArchiving_MASTER', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DataArchiving_MASTER' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DataArchiving_MASTER] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DataArchiving_MASTER]
(
	@StopDate as date
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 from NedTrain_Spectrum_Archive..ChannelValue_Archive) BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]1 ',0,0,'Failure')
		RAISERROR('ChannelValue_Archive Has data',18,1)
	END

	DECLARE @retbit bit 

	EXEC @retbit = dbo.NSP_DataArchiving @StopDate 

	if @retbit = 1 BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]2',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchiving',18,1)
	END

	EXEC @retbit = dbo.NSP_DataArchivingBackupAndTruncate
	
	if @retbit = 1 BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]3',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchivingBackupAndTruncate',18,1)
	END

	INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(GETDATE() , GETDATE(),'[NSP_DataArchiving_MASTER]',0,0,'Sucess')

END
GO

RAISERROR ('-- update NSP_DataArchivingBackupAndTruncate', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DataArchivingBackupAndTruncate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DataArchivingBackupAndTruncate] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DataArchivingBackupAndTruncate]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @faultStart datetime	
	DECLARE @faultStop datetime	

	BEGIN TRY

		SELECT @faultStart = Min(timestamp) FROM NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive
		SELECT @faultStop = Max(timestamp) FROM NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive

		DECLARE @backupString nvarchar(100) = 'D:\!Brian\NedTrain_Spectrum_Archive_' + isnull(CONVERT(varchar(8), @faultStop, 112),'') + 'at_'+CONVERT(varchar(8), GETDATE(), 112)+'.bak'

		DECLARE @msg varchar(1000) = 'Backing up database to ' + @backupString
		RAISERROR(@msg, 10, 1) WITH NOWAIT

		BACKUP DATABASE [NedTrain_Spectrum] TO DISK = @backupString
			WITH COMPRESSION, CHECKSUM, STATS = 10;

	
		RESTORE VERIFYONLY FROM DISK = @backupString
	END TRY
	BEGIN CATCH
		RETURN 1
	END CATCH
	

	TRUNCATE TABLE [NedTrain_Spectrum]..ChannelValue_Archive
	TRUNCATE TABLE [NedTrain_Spectrum]..FaultChannelValue_Archive
	TRUNCATE TABLE [NedTrain_Spectrum]..Fault_Archive

	RETURN 0
END
GO

RAISERROR ('-- update NSP_DbaDataArchiving', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DbaDataArchiving' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DbaDataArchiving] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DbaDataArchiving]
AS
BEGIN

	SET NOCOUNT ON; 

	RETURN

	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE ChannelValue', 10, 1) WITH NOWAIT;
	SET NOCOUNT ON;
	DECLARE @msg varchar(150)
	DECLARE @timestamp datetime2(3) = (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM dbo.ChannelValue (NOLOCK)) --'2012-04-01 00:00:00'
	DECLARE @maxTimestamp datetime2(3) = DATEADD(D, -7, SYSDATETIME())
	DECLARE @t datetime2(3)
	DECLARE @backupFile varchar(1000) 
	DECLARE @interval int = 30
	DECLARE @interval_OLD int = 1440 -- interval to be used for data older than 1 month - ScotRail can insert a spurious record from a LONG time ago, screwing up the time to complete the archiving
	DECLARE @sql varchar(1000)

	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM dbo.ChannelValue 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(ms, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
		SET @timestamp = DATEADD(n, @interval, @timestamp);
				
		WAITFOR DELAY '00:00:01'
				
	END
	-------------------------------------------------------------------------------
	-- VehicleEvent
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE VehicleEvent', 10, 1) WITH NOWAIT;
	SET @t				= NULL
	SET @msg			= NULL
	SET @timestamp		= (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM dbo.VehicleEvent (NOLOCK)) --'2012-04-01 00:00:00'
	SET @maxTimestamp	= DATEADD(D, -7, SYSDATETIME())
	SET @interval		= 30 --mins
	
	
	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM dbo.VehicleEvent 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
	
		IF @timestamp < DATEADD(month,-1,SYSDATETIME())
		BEGIN
			SET @timestamp = DATEADD(n, @interval_OLD, @timestamp)
		END
		ELSE
		BEGIN
			SET @timestamp = DATEADD(n, @interval, @timestamp);
		END
				
		WAITFOR DELAY '00:00:01'
				
	END


	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE Fault', 10, 1) WITH NOWAIT;

	DECLARE @chunk int	= 1000
	DECLARE @rowCount int
	DECLARE @rowCountFaultChannelValue int
	SET @t				= NULL
	SET @msg			= NULL
	
	DECLARE @FaultToDelete TABLE(
		ID int
	)


	WHILE 1=1
	BEGIN
		SET @t = SYSDATETIME()
		
		INSERT INTO @FaultToDelete(ID)
		SELECT TOP (@chunk) ID
		FROM dbo.Fault 
		WHERE CreateTime < DATEADD(d, -7, SYSDATETIME());
	
		DELETE dbo.FaultChannelValue
		WHERE FaultID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCountFaultChannelValue = @@ROWCOUNT
			
		DELETE dbo.Fault
		WHERE ID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCount = @@ROWCOUNT 

		IF @rowCount = 0 
			BREAK

		SET @msg = 
			'Current Time: ' 
			+ CONVERT(char(23), SYSDATETIME(), 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @rowCount), 10), 'NULL')
			+ '/'
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @rowCountFaultChannelValue), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
			
		WAITFOR DELAY '00:00:01'
	END

END -- stored procedure
GO

RAISERROR ('-- update NSP_DeleteFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DeleteFaultCategory] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DeleteFaultCategory](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY

		DELETE dbo.FaultCategory 
		WHERE ID = @ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
	
END
GO

RAISERROR ('-- update NSP_DeleteFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DeleteFaultMeta] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DeleteFaultMeta](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY

		DELETE dbo.FaultMeta 
		WHERE ID = @ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		IF ERROR_NUMBER() = 547 -- FOREIGN KEY ERROR
			BEGIN
				RAISERROR ('ERR_FAULT_EXIST', 14, 1) WITH NOWAIT
			END
		ELSE
			BEGIN
				EXEC dbo.NSP_RethrowError;
			END
			
		ROLLBACK TRAN;
		
	END CATCH
	
END
GO

RAISERROR ('-- update NSP_DeleteVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_DeleteVehicle] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_DeleteVehicle]
(
	@VehicleNumber varchar(16)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UnitID INT 

	SELECT @UnitID = UnitID FROM dbo.Vehicle WHERE VehicleNumber = @VehicleNumber
	
	BEGIN TRAN
	BEGIN TRY
		
	DELETE FROM dbo.ChannelValue where UnitId = @UnitID
	DELETE FROM dbo.FaultChannelValue WHERE FaultID in (SELECT ID FROM dbo.Fault WHERE FaultUnitID = @UnitID)
	DELETE FROM dbo.Fault WHERE FaultUnitID = @UnitID

	DELETE FROM dbo.Vehicle WHERE VehicleNumber = @VehicleNumber

	DELETE FROM dbo.FleetStatus WHERE unitID = @UnitID

	DELETE FROM dbo.FleetStatusHistory WHERE unitID = @UnitID

	DELETE FROM dbo.FleetStatusStaging WHERE UnitID = @UnitID

	DELETE FROM dbo.FleetFormation WHERE UnitNumberList = @VehicleNumber

	DELETE FROM dbo.Unit WHERE ID = @UnitID

	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH

END
GO

RAISERROR ('-- update NSP_EaFaultCategories', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaFaultCategories' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaFaultCategories] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_EaFaultCategories
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaFaultCategories]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT
		FaultCategory = ID
		, FaultCategoryName = Category
	FROM dbo.FaultCategory	
		
END
GO

RAISERROR ('-- update NSP_EaFaultTypes', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaFaultTypes' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaFaultTypes] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_EaFaultTypes
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaFaultTypes]
AS
BEGIN
	
	SELECT 
		FaultType		= ID
		, FaultTypeName	= Name
		, FaultColor	= DisplayColor
	FROM dbo.FaultType
		
		
		
END
GO

RAISERROR ('-- update NSP_EaReport', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaReport' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaReport] AS BEGIN RETURN(1) END;')
GO

/*
Try me:
exec [dbo].[NSP_EaReport]	 @FleetCode = null
	, @DateFrom = '20170127'
	, @DateTo = '20170201'
	-- additional parameters: need to be in alphabetical order
	, @CategoryID = null
	, @FaultMetaID = '3552'
	, @FaultTypeID = null
	, @LocationID = null
	, @UnitID = ''
	, @VehicleID = null

*/
ALTER PROCEDURE [dbo].[NSP_EaReport]
(
	@FleetCode varchar(10)
	, @DateFrom datetime
	, @DateTo datetime
	-- additional parameters: need to be in alphabetical order
	, @CategoryID varchar(100)
	, @FaultMetaID varchar(max)
	, @FaultTypeID varchar(50)
	, @LocationID varchar(max)
	, @UnitID varchar(max)
	, @VehicleID varchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FaultCategoryTable TABLE (CategoryID int)
	INSERT INTO @FaultCategoryTable
	SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@CategoryID,',')

	DECLARE @FaultMetaTable TABLE (FaultMetaID int)
	INSERT INTO @FaultMetaTable
	SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@FaultMetaID,',')

	DECLARE @FaultTypeTable TABLE (FaultTypeID int)
	INSERT INTO @FaultTypeTable
	SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@FaultTypeID,',')

	DECLARE @LocationTable TABLE (LocationID int)
	INSERT INTO @LocationTable
	SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@LocationID,',')

	DECLARE @UnitIDTable TABLE (UnitID int)
	INSERT INTO @UnitIDTable
	SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@UnitID,',')

	IF @CategoryID = '' SET @CategoryID = NULL
	IF @FaultMetaID = '' SET @FaultMetaID = NULL
	IF @FaultTypeID = '' SET @FaultTypeID = NULL
	IF @LocationID = '' SET @LocationID = NULL
	IF @UnitID = '' SET @UnitID = NULL

	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultTypeID = '-1' AND FaultTypeID IS NULL))
		AND (ID IN (SELECT * FROM @FaultMetaTable) OR @FaultMetaID IS NULL OR (@FaultMetaID = '-1' AND ID IS NULL))
		AND (CategoryID IN (SELECT * FROM @FaultCategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))

	SELECT
		FaultID = f.ID
		, f.CreateTime
		, f.EndTime
		, f.IsCurrent

		, f.HeadCode
		, f.SetCode
		, f.FleetCode
		, f.FaultUnitNumber AS UnitNumber
		, UnitID	= f.FaultUnitID
		, f.FaultUnitID
		, FaultVehicleNumber = f.FaultUnitID -- unit level, for now
		, FaultVehicleID = NULL

		, f.FaultMetaID
		, f.FaultCode
		, f.Description

		, f.FaultTypeID
		, f.FaultType
		, f.FaultTypeColor
		, f.Priority

		, f.CategoryID
		, f.Category

		, f.Latitude
		, f.Longitude
		, f.LocationID
		, l.LocationCode
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@UnitID is null OR (f.FaultUnitID in (SELECT UnitID FROM @UnitIDTable)))
		AND (@LocationID IS NULL OR LocationID = @LocationID OR (@LocationID = -1 AND LocationID IS NULL))
		AND (@FleetCode IS NULL OR f.FleetCode = @FleetCode)

END
GO

RAISERROR ('-- update NSP_EaSearchFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaSearchFaultMeta] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_EaSearchFaultMeta
** Description:
** Call frequency: Called from Ops Analysis
** Parameters:
** Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/
ALTER PROCEDURE [dbo].[NSP_EaSearchFaultMeta]
(
	@FaultString varchar(50)
	, @FaultCategoryID varchar(500)
	, @FaultTypeID varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FaultCategoryTable TABLE (FaultCategoryID int)
	INSERT INTO @FaultCategoryTable
	SELECT convert(int, item) from [dbo].SplitStrings_CTE(@FaultCategoryID,',')

	DECLARE @FaultTypeTable TABLE (FaultTypeId tinyint)
	INSERT INTO @FaultTypeTable
	SELECT convert(tinyint, item) from [dbo].SplitStrings_CTE(@FaultTypeId,',')

	IF @FaultCategoryID = '' SET @FaultCategoryID = NULL
	IF @FaultTypeId = '' SET @FaultTypeId = NULL

	SELECT
		FaultID	 = fm.ID
		, FaultDesc = fm.Description
		, FaultCode
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	INNER JOIN dbo.FaultCategory fc ON fc.ID = fm.CategoryID
	WHERE
		(@FaultTypeId IS NULL OR ft.ID IN (SELECT FaultTypeId FROM @FaultTypeTable))
		AND (@FaultCategoryId IS NULL OR fc.ID IN (SELECT FaultCategoryID FROM @FaultCategoryTable))
		AND
		(
			Description LIKE '%' + @FaultString + '%'
			OR
			FaultCode LIKE '%' + @FaultString + '%'
		)

END
GO

RAISERROR ('-- update NSP_EaSearchLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaSearchLocation] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_EaSearchLocation
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaSearchLocation]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
		, LocationCode
	FROM dbo.Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationCode LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END
GO

RAISERROR ('-- update NSP_EaSearchUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaSearchUnit] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_EaSearchUnit
** Description:	
** Call frequency: Called from Ops Analysis 
** Parameters:	 
** Return values: 
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaSearchUnit]
(
	 @UnitString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		FleetCode = f.Code
		, UnitID = u.ID
		, UnitNumber = u.UnitNumber
	FROM dbo.Unit u
	JOIN dbo.Fleet f
	ON f.ID = u.FleetID
	WHERE UnitNumber LIKE '%' + @UnitString + '%' 
		
END
GO

RAISERROR ('-- update NSP_EaSearchVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaSearchVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaSearchVehicle] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
** Name:			NSP_EaSearchVehicle
** Description:
** Call frequency: Called from Ops Analysis
** Parameters:
** Return values:
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaSearchVehicle]
(
	@VehicleType varchar(50)
	, @VehicleString varchar(50)
	, @UnitID varchar(max) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleTypeTable TABLE (VehicleType varchar(50))
	INSERT INTO @VehicleTypeTable
	SELECT * from [dbo].SplitStrings_CTE(@VehicleType,',')

	DECLARE @UnitIdTable TABLE (UnitID int)
	INSERT INTO @UnitIdTable
	SELECT convert(int, item) from [dbo].SplitStrings_CTE(@UnitID,',')

	SELECT
		FleetCode = f.Code
		, VehicleID	= v.ID
		, VehicleNumber = v.VehicleNumber
		, VehicleType = v.Type
	FROM Vehicle v
	JOIN Unit u ON u.ID = v.UnitID
	JOIN Fleet f ON f.ID = u.FleetID
	WHERE VehicleNumber LIKE '%' + @VehicleString + '%'
		AND (@VehicleType IS NULL OR v.Type IN (Select VehicleType from @VehicleTypeTable))
		AND (@UnitID IS NULL OR v.UnitID IN (Select UnitID from @UnitIdTable))
END
GO

RAISERROR ('-- update NSP_EaVehicleTypes', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaVehicleTypes' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_EaVehicleTypes] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_EaVehicleTypes
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_EaVehicleTypes]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT
		VehicleType		= Type
	FROM dbo.Vehicle
	
END
GO

RAISERROR ('-- update NSP_FaGetFaultList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetFaultList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetFaultList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetFaultList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetFaultList]
(
	@FaultTypeID int
	, @FaultString varchar(50)
)
AS
BEGIN


	SET NOCOUNT ON;
	SELECT 
		FaultID		= fm.ID
		, FaultDesc	= fm.Description
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND
		(
			Description LIKE '%' + @FaultString + '%'
			OR
			FaultCode LIKE '%' + @FaultString + '%'
		)


END
GO

RAISERROR ('-- update NSP_FaGetFaultTypeList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetFaultTypeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetFaultTypeList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetFaultTypeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetFaultTypeList]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		FaultType		= ID
		, FaultTypeName	= Name
	FROM dbo.FaultType
		
		
		
END
GO

RAISERROR ('-- update NSP_FaGetLocationList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetLocationList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetLocationList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetLocationList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetLocationList]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
		, LocationCode
	FROM dbo.Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationCode LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END
GO

RAISERROR ('-- update NSP_FaGetUnitList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetUnitList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetUnitList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetUnitList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetUnitList]
(
	 @UnitString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		UnitID	= ID
		, UnitNumber
	FROM dbo.Unit
	WHERE UnitNumber LIKE '%' + @UnitString + '%' 
		
END
GO

RAISERROR ('-- update NSP_FaGetVehicleList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetVehicleList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetVehicleList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetVehicleList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetVehicleList]
(
	@VehicleType varchar(50)
	, @VehicleString varchar(50)
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		VehicleID	= ID
		, VehicleNumber
	FROM dbo.Vehicle
	WHERE VehicleNumber LIKE '%' + @VehicleString + '%' 
		AND (Type = @VehicleType OR @VehicleType IS NULL)
		AND (UnitID = @UnitID OR @UnitID IS NULL)
		
END


GO

RAISERROR ('-- update NSP_FaGetVehicleTypeList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaGetVehicleTypeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaGetVehicleTypeList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaGetVehicleTypeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaGetVehicleTypeList]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		VehicleType		= Type
	FROM dbo.Vehicle
	
END


GO

RAISERROR ('-- update NSP_FaReportFaultByDate', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultByDate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultByDate] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByDate
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByDate]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID )
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)	

	SELECT 
		CAST(CAST(CreateTime AS DATE) AS DATETIME) CreateTime
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.unitid = faultunitid
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		CAST(CAST(CreateTime AS DATE) AS DATETIME)
	ORDER BY CAST(CAST(CreateTime AS DATE) AS DATETIME) ASC

		

END

GO

RAISERROR ('-- update NSP_FaReportFaultByFaultCode', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultByFaultCode' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultByFaultCode] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByFaultCode
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByFaultCode]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)


	SELECT
		f.FaultMetaID
		, f.FaultCode
		, f.Description
		, f.Category
		, Type = f.FaultType
		, EventCount			= COUNT(*)
		, TotalFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime))) / COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		f.FaultMetaID
		, f.FaultCode
		, f.Description
		, f.Category
		, f.FaultType
	ORDER BY EventCount DESC
		

END
GO

RAISERROR ('-- update NSP_FaReportFaultByLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultByLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultByLocation] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByLocation
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByLocation]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
		LocationID				= ISNULL(f.LocationID, -1) 
		, LocationName			= ISNULL(l.LocationName, 'N/A')
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
		, Lat					= ISNULL(Lat,0)
		, Lng					= ISNULL(Lng,0)
		, DisplayColor			= FaultTypeColor
		, Priority
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		f.LocationID
		, l.LocationName
		, f.FaultTypeColor
		, f.Priority
		, l.Lat
		, l.Lng
	ORDER BY EventCount DESC

END
GO

RAISERROR ('-- update NSP_FaReportFaultByUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultByUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultByUnit] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByUnit
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByUnit]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT fm.ID
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND	fm.ID = ISNULL(@FaultMetaID, fm.ID)	

	SELECT 
		UnitID					= v.UnitID 
		, UnitNumber			= v.UnitNumber
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.VW_Vehicle v ON v.UnitID = f.FaultUnitID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (f.LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		v.UnitID 
		, v.UnitNumber	
	ORDER BY EventCount DESC
		

END
GO

RAISERROR ('-- update NSP_FaReportFaultByVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultByVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultByVehicle] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultByVehicle
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByVehicle]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT 
		VehicleID				= v.ID 
		, VehicleNumber
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		v.ID	
		, VehicleNumber
	ORDER BY EventCount DESC
		

END
GO

RAISERROR ('-- update NSP_FaReportFaultList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaReportFaultList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FaReportFaultList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_FaReportFaultList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FaReportFaultList]
(
	@DateFrom datetime
	, @DateTo datetime
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF @LocationID = -1 SET @LocationID = NULL

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND 
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitNumber	
			, v.ID as FaultVehicleID	
			, FaultVehicleNumber = v.VehicleNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType
			, f.FaultTypeColor
			, f.Priority
			, f.Category
			, f.ReportingOnly
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR f.LocationID = @LocationID)
	ORDER BY CreateTime DESC
		
END
GO

RAISERROR ('-- update NSP_FleetLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FleetLocation] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_FleetLocation]
AS
/******************************************************************************
** Name:			NSP_FleetLocation
** Description:	Returns live channel data for the application
** Call frequency: Every 5s
** Parameters:	 None
** Return values: 0 if successful, else an error is raised
*******************************************************************************/
BEGIN
	SET NOCOUNT ON;
	
	-- Get totals for Vehicle based faults
	DECLARE @currentVehicleFault TABLE(
		UnitID int NULL
		, FaultNumber int NULL
		, WarningNumber int NULL
	)
	
	INSERT INTO @currentVehicleFault
	SELECT 
		FaultUnitID
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
	FROM dbo.VW_IX_FaultLive WITH (NOEXPAND)
	WHERE FaultUnitID IS NOT NULL
	GROUP BY FaultUnitID

	-- return data
	SELECT 
		fs.UnitID
		, fs.UnitNumber
		, fs.UnitType
		, fs.FleetFormationID
		, fs.SetCode
		, fs.UnitPosition
		, fs.Headcode
		, fs.Diagram
		, fs.Loading
		, fs.VehicleID
		, fs.VehicleNumber
		, fs.VehicleType
		, fs.VehicleTypeID
		, fs.VehiclePosition
		, Location	 = (
			SELECT TOP 1 
				LocationCode -- CRS
			FROM dbo.Location
			INNER JOIN dbo.LocationArea ON LocationID = Location.ID
			WHERE 
				(Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
					AND Col2 BETWEEN MinLongitude AND MaxLongitude)
			ORDER BY Priority
		)
		, FaultNumber			= ISNULL(vf.FaultNumber,0)
		, WarningNumber	 = ISNULL(vf.WarningNumber,0)
		, VehicleList
		, ServiceStatus		 = CASE 
			WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
						SELECT TOP 1 LocationID
						FROM dbo.LocationArea
						WHERE 
							(Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
								AND Col2 BETWEEN MinLongitude AND MaxLongitude)
						ORDER BY Priority
					)
					THEN 'Ready for Service'
				WHEN fs.Headcode IS NOT NULL
					THEN 'In Service'
				ELSE 'Out of Service'
			END
		,cv.UpdateRecord
		,cv.RecordInsert
		-- temporary solution for datetime and JTDS driver 
		,CASE WHEN isnull(CAST(cve.[TimeStamp] AS datetime),'01-Jan-1970') > isnull(CAST(cv.[TimeStamp] AS datetime),'01-Jan-1970') THEN CAST(cve.[TimeStamp] AS datetime) ELSE CAST(cv.[TimeStamp] AS datetime) END AS 'TimeStamp'
		,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
		, ServiceStatusEKE	 = CASE 
			WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
					SELECT TOP 1 LocationID
					FROM dbo.LocationArea
					WHERE 
						(Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
							AND Col2 BETWEEN MinLongitude AND MaxLongitude)
					ORDER BY Priority
				)
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,UpdateRecordEKE = cve.[UpdateRecord]
		,RecordInsertEKE = cve.[RecordInsert]
	
	FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
	INNER JOIN dbo.VW_VehicleLastChannelValueTimestamp vlc WITH (NOLOCK) ON vlc.VehicleID = fs.VehicleID
	LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.VehicleID = cv.VehicleID
	LEFT JOIN dbo.ChannelValueEKE cve WITH (NOLOCK) ON cve.TimeStamp = vlc.LastEKETimeStamp AND vlc.VehicleID = cve.VehicleID
	LEFT JOIN @currentVehicleFault vf ON fs.UnitID = vf.UnitID
END
GO

RAISERROR ('-- update NSP_FleetSummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FleetSummary] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_FleetSummary]
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Get totals for Vehicle based faults
	DECLARE @currentVehicleFault TABLE(
		VehicleID int NULL
		, FaultNumber int NULL
		, WarningNumber int NULL
		, RecoveryNumber int NULL
		, NotAcknowledgedNumber int NULL
	)
	
	INSERT INTO @currentVehicleFault
	SELECT 
		Vehicle.ID
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
		, SUM(CASE HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
		, SUM(CASE IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
	FROM dbo.VW_IX_FaultLive WITH (NOEXPAND)
	JOIN dbo.Vehicle ON UnitID = FaultUnitID
	WHERE FaultUnitID IS NOT NULL
		AND Category <> 'Test'
		AND ReportingOnly = 0
	GROUP BY Vehicle.ID

	-- return data
	SELECT 
		fs.UnitID
		, fs.UnitNumber
		, fs.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, fs.Headcode
		, fs.Diagram
		, fs.Loading
		, fs.VehicleID
		, fs.VehicleNumber
		, fs.VehicleType
		, fs.VehicleTypeID
		, fs.VehiclePosition
		, fs.FleetCode
		, Location = (
			SELECT TOP 1 
				LocationCode --CRS
			FROM dbo.Location
			INNER JOIN dbo.LocationArea ON LocationID = Location.ID
			WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
				AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		)
		, FaultNumber		= ISNULL(vf.FaultNumber,0)
		, WarningNumber		= ISNULL(vf.WarningNumber,0)
		, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
		, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
		, VehicleList
		, ServiceStatus		= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = (
					SELECT TOP 1 LocationID
					FROM LocationArea
					WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
						AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
						AND LocationArea.Type = 'C'
					ORDER BY Priority
				)
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		-- temporary solution for datetime and JTDS driver 
		,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp,
-- Channel Data 
		[Col1], [Col2], [Col3], [Col4]
		--, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
		--[Col400],
		--[Col401],
		--[Col402],
		--[Col403],
		--[Col404],
		--[Col405],
		--[Col406],
		--[Col407],
		--[Col408],
		--[Col409],
		--[Col410],
		--[Col411],
		--[Col412],
		--[Col413],
		--[Col414],
		--[Col415],
		--[Col416],
		--[Col417],
		--[Col418],
		--[Col419],
		--[Col420],
		--[Col421],
		--[Col422],
		--[Col423],
		--[Col424],
		--[Col425],
		--[Col426],
		--[Col427],
		--[Col428],
		--[Col429],
		--[Col430],
		--[Col431],
		--[Col432],
		--[Col433],
		--[Col434],
		--[Col435],
		--[Col436],
		--[Col437],
		--[Col438],
		--[Col439],
		--[Col440],
		--[Col441],
		--[Col442],
		--[Col443],
		--[Col444],
		--[Col445],
		--[Col446],
		--[Col447],
		--[Col448],
		--[Col449],
		--[Col450],
		--[Col451],
		--[Col452],
		--[Col453],
		--[Col454],
		--[Col455],
		--[Col456],
		--[Col457],
		--[Col458],
		--[Col459],
		--[Col460],
		--[Col461],
		--[Col462],
		--[Col463],
		--[Col464],
		--[Col465],
		--[Col466],
		--[Col467],
		--[Col468],
		--[Col469],
		--[Col470],
		--[Col471],
		--[Col472],
		--[Col473],
		--[Col474],
		--[Col475],
		--[Col476],
		--[Col477],
		--[Col478],
		--[Col479],
		--[Col480],
		--[Col481],
		--[Col482],
		--[Col483],
		--[Col484],
		--[Col485],
		--[Col486],
		--[Col487],
		--[Col488],
		--[Col489],
		--[Col490],
		--[Col491],
		--[Col492],
		--[Col493],
		--[Col494],
		--[Col495],
		--[Col496],
		--[Col497],
		--[Col498],
		--[Col499],
		--[Col500],
		--[Col501],
		--[Col502],
		--[Col503],
		--[Col504],
		--[Col505],
		--[Col506],
		--[Col507],
		--[Col508],
		--[Col509],
		--[Col510],
		--[Col511],
		--[Col512],
		--[Col513],
		--[Col514],
		--[Col515],
		--[Col516],
		--[Col517],
		--[Col518],
		--[Col519],
		--[Col520],
		--[Col521],
		--[Col522],
		--[Col523],
		--[Col524],
		--[Col525],
		--[Col526],
		--[Col527],
		--[Col528],
		--[Col529],
		--[Col530],
		--[Col531],
		--[Col532],
		--[Col533],
		--[Col534],
		--[Col535],
		--[Col536],
		--[Col537],
		--[Col538],
		--[Col539],
		--[Col540],
		--[Col541],
		--[Col542],
		--[Col543],
		--[Col544],
		--[Col545],
		--[Col546],
		--[Col547],
		--[Col548],
		--[Col549],
		--[Col550],
		--[Col551],
		--[Col552],
		--[Col553],
		--[Col554],
		--[Col555],
		--[Col556],
		--[Col557],
		--[Col558],
		--[Col559],
		--[Col560],
		--[Col561],
		--[Col562],
		--[Col563],
		--[Col564],
		--[Col565],
		--[Col566],
		--[Col567],
		--[Col568],
		--[Col569],
		--[Col570],
		--[Col571],
		--[Col572],
		--[Col573],
		--[Col574],
		--[Col575],
		--[Col576],
		--[Col577],
		--[Col578],
		--[Col579],
		--[Col580],
		--[Col581],
		--[Col582],
		--[Col583],
		--[Col584],
		--[Col585],
		--[Col586],
		--[Col587],
		--[Col588],
		--[Col589],
		--[Col590],
		--[Col591],
		--[Col592],
		--[Col593],
		--[Col594],
		--[Col595],
		--[Col596],
		--[Col597],
		--[Col598],
		--[Col599],
		--[Col600],
		--[Col601],
		--[Col602],
		--[Col603],
		--[Col604],
		--[Col605],
		--[Col606],
		--[Col607],
		--[Col608],
		--[Col609],
		--[Col610],
		--[Col611],
		--[Col612],
		--[Col613],
		--[Col614],
		--[Col615],
		--[Col616],
		--[Col617],
		--[Col618],
		--[Col619],
		--[Col620],
		--[Col621],
		--[Col622],
		--[Col623],
		--[Col624],
		--[Col625],
		--[Col626],
		--[Col627],
		--[Col628],
		--[Col629],
		--[Col630],
		--[Col631],
		--[Col632],
		--[Col633],
		--[Col634],
		--[Col635],
		--[Col636],
		--[Col637],
		--[Col638],
		--[Col639],
		--[Col640],
		--[Col641],
		--[Col642],
		--[Col643],
		--[Col644],
		--[Col645],
		--[Col646],
		--[Col647],
		--[Col648],
		--[Col649],
		--[Col650],
		--[Col651],
		--[Col652],
		--[Col653],
		--[Col654],
		--[Col655],
		--[Col656],
		--[Col657],
		--[Col658],
		--[Col659],
		--[Col660],
		--[Col661],
		--[Col662],
		--[Col663],
		--[Col5000],
		--[Col5001],
		--[Col5002],
		--[Col5003],
		--[Col5004],
		--[Col5005],
		--[Col5006],
		--[Col5007],
		--[Col5008],
		--[Col5009],
		--[Col5010],
		--[Col5011],
		--[Col5012],
		--[Col5013],
		--[Col5014],
		--[Col5015],
		--[Col5016],
		--[Col5017],
		--[Col5018],
		--[Col5019],
		--[Col5020],
		--[Col5021],
		--[Col5022],
		--[Col5023],
		--[Col5024],
		--[Col5025],
		--[Col5026],
		--[Col5027],
		--[Col5028],
		--[Col5029],
		--[Col5030],
		--[Col5031],
		--[Col5032],
		--[Col5033],
		--[Col5034],
		--[Col5035],
		--[Col5036],
		--[Col5037],
		--[Col5038],
		--[Col5039],
		--[Col5040],
		--[Col5041],
		--[Col5042],
		--[Col5043],
		--[Col5044],
		--[Col5045],
		--[Col5046],
		--[Col5047],
		--[Col5048],
		--[Col5049],
		--[Col5050],
		--[Col5051],
		--[Col5052],
		--[Col5053],
		--[Col5054],
		--[Col5055],
		--[Col5056],
		--[Col5057],
		--[Col5058],
		--[Col5059],
		--[Col5060],
		--[Col5061],
		--[Col5062],
		--[Col5063],
		--[Col5064],
		--[Col5065],
		--[Col5066],
		--[Col5067],
		--[Col5068],
		--[Col5069],
		--[Col5070],
		--[Col5071],
		--[Col5072],
		--[Col5073],
		--[Col5074],
		--[Col5075],
		--[Col5076],
		--[Col5077],
		--[Col5078],
		--[Col5079],
		--[Col5080],
		--[Col5081],
		--[Col5082],
		--[Col5083],
		--[Col5084],
		--[Col5085],
		--[Col5086],
		--[Col5087],
		--[Col5088],
		--[Col5089],
		--[Col5090],
		--[Col5091],
		--[Col5092],
		--[Col5093],
		--[Col5094],
		--[Col5095],
		--[Col5096],
		--[Col5097],
		--[Col5098],
		--[Col5099],
		--[Col5100],
		--[Col5101],
		--[Col5102],
		--[Col5103],
		--[Col5104],
		--[Col5105],
		--[Col5106],
		--[Col5107],
		--[Col5108],
		--[Col5109],
		--[Col5110],
		--[Col5111],
		--[Col5112],
		--[Col5113],
		--[Col5114],
		--[Col5115],
		--[Col5116],
		--[Col5117],
		--[Col5118],
		--[Col5119],
		--[Col5120],
		--[Col5121],
		--[Col5122],
		--[Col5123],
		--[Col5124],
		--[Col5125],
		--[Col5126],
		--[Col5127],
		--[Col5128],
		--[Col5129],
		--[Col5130],
		--[Col5131],
		--[Col5132],
		--[Col5133],
		--[Col5134],
		--[Col5135],
		--[Col5136],
		--[Col5137],
		--[Col5138],
		--[Col5139],
		--[Col5140],
		--[Col5141],
		--[Col5142],
		--[Col5143],
		--[Col5144],
		--[Col5145],
		--[Col5146],
		--[Col5147],
		--[Col5148],
		--[Col5149],
		--[Col5150],
		--[Col5151],
		--[Col5152],
		--[Col5153],
		--[Col5154],
		--[Col5155],
		--[Col5156],
		--[Col5157],
		--[Col5158],
		--[Col5159],
		--[Col5160],
		--[Col5161],
		--[Col5162],
		--[Col5163],
		--[Col5164],
		--[Col5165],
		--[Col5166],
		--[Col5167],
		--[Col5168],
		--[Col5169],
		--[Col5170],
		--[Col5171],
		--[Col5172],
		--[Col5173],
		--[Col5174],
		--[Col5175],
		--[Col5176],
		--[Col5177],
		--[Col5178],
		--[Col5179],
		--[Col5180],
		--[Col5181],
		--[Col5182],
		--[Col5183],
		--[Col5184],
		--[Col5185],
		--[Col5186],
		--[Col5187],
		--[Col5188],
		--[Col5189],
		--[Col5190],
		--[Col5191],
		--[Col5192],
		--[Col5193],
		--[Col5194],
		--[Col5195],
		--[Col5196],
		--[Col5197],
		--[Col5198],
		--[Col5199],
		--[Col5200],
		--[Col5201],
		--[Col5202],
		--[Col5203],
		--[Col5204],
		--[Col5205],
		--[Col5206],
		--[Col5207],
		--[Col5208],
		--[Col5209],
		--[Col5210],
		--[Col5211],
		--[Col5212],
		--[Col5213],
		--[Col5214],
		--[Col5215],
		--[Col5216],
		--[Col5217],
		--[Col5218],
		--[Col5219],
		--[Col5220],
		--[Col5221],
		--[Col5222],
		--[Col5223],
		--[Col5224],
		--[Col5225],
		--[Col5226],
		--[Col5227],
		--[Col5228],
		--[Col5229],
		--[Col5230],
		--[Col5231],
		--[Col5232],
		--[Col5233],
		--[Col5234],
		--[Col5235],
		--[Col5236],
		--[Col5237],
		--[Col5238],
		--[Col5239],
		--[Col5240],
		--[Col5241],
		--[Col5242],
		--[Col5243],
		--[Col5244],
		--[Col5245],
		--[Col5246],
		--[Col5247],
		--[Col5248],
		--[Col5249],
		--[Col5250],
		--[Col5251],
		--[Col5252],
		--[Col5253],
		--[Col5254],
		--[Col5255],
		--[Col5256],
		--[Col5257],
		--[Col5258],
		--[Col5259],
		--[Col5260],
		--[Col5261],
		--[Col5262],
		--[Col5263],
		--[Col5264],
		--[Col5265],
		--[Col5266],
		--[Col5267],
		--[Col5268],
		--[Col5269],
		--[Col5270],
		--[Col5271],
		--[Col5272],
		--[Col5273],
		--[Col5274],
		--[Col5275],
		--[Col5276],
		--[Col5277],
		--[Col5278],
		--[Col5279],
		--[Col5280],
		--[Col5281],
		--[Col5282],
		--[Col5283],
		--[Col5284],
		--[Col5285],
		--[Col5286],
		--[Col5287],
		--[Col5288],
		--[Col5289],
		--[Col5290],
		--[Col5291],
		--[Col5292],
		--[Col5293],
		--[Col5294],
		--[Col5295],
		--[Col5296],
		--[Col5297],
		--[Col5298],
		--[Col5299],
		--[Col5300],
		--[Col5301],
		--[Col5302],
		--[Col5303],
		--[Col5304],
		--[Col5305],
		--[Col5306],
		--[Col5307],
		--[Col5308],
		--[Col5309],
		--[Col5310],
		--[Col5311],
		--[Col5312],
		--[Col5313],
		--[Col5314],
		--[Col5315],
		--[Col5316],
		--[Col5317],
		--[Col5318],
		--[Col5319],
		--[Col5320],
		--[Col5321],
		--[Col5322],
		--[Col5323],
		--[Col5324],
		--[Col5325],
		--[Col5326],
		--[Col5327],
		--[Col5328],
		--[Col5329],
		--[Col5330],
		--[Col5331],
		--[Col5332],
		--[Col5333],
		--[Col5334],
		--[Col5335],
		--[Col5336],
		--[Col5337],
		--[Col5338],
		--[Col5339],
		--[Col5340],
		--[Col5341],
		--[Col5342],
		--[Col5343],
		--[Col5344],
		--[Col5345],
		--[Col5346],
		--[Col5347],
		--[Col5348],
		--[Col5349],
		--[Col5350],
		--[Col5351],
		--[Col5352],
		--[Col5353],
		--[Col5354],
		--[Col5355],
		--[Col5356],
		--[Col5357],
		--[Col5358],
		--[Col5359],
		--[Col5360],
		--[Col5361],
		--[Col5362],
		--[Col5363],
		--[Col5364],
		--[Col5365],
		--[Col5366],
		--[Col5367],
		--[Col5368],
		--[Col5369],
		--[Col5370],
		--[Col5371],
		--[Col5372],
		--[Col5373],
		--[Col5374],
		--[Col5375],
		--[Col5376],
		--[Col5377],
		--[Col5378],
		--[Col5379],
		--[Col5380],
		--[Col5381],
		--[Col5382],
		--[Col5383],
		--[Col5384],
		--[Col5385],
		--[Col5386],
		--[Col5387],
		--[Col5388],
		--[Col5389],
		--[Col5390],
		--[Col5391],
		--[Col5392],
		--[Col5393],
		--[Col5394],
		--[Col5395],
		--[Col5396],
		--[Col5397],
		--[Col5398],
		--[Col5399],
		--[Col5400],
		--[Col5401],
		--[Col5402],
		--[Col5403],
		--[Col5404],
		--[Col5405],
		--[Col5406],
		--[Col5407],
		--[Col5408],
		--[Col5409],
		--[Col5410],
		--[Col5411],
		--[Col5412],
		--[Col5413],
		--[Col5414],
		--[Col5415],
		--[Col5416],
		--[Col5417],
		--[Col5418],
		--[Col5419],
		--[Col5420],
		--[Col5421],
		--[Col5422],
		--[Col5423],
		--[Col5424],
		--[Col5425],
		--[Col5426],
		--[Col5427],
		--[Col5428],
		--[Col5429],
		--[Col5430],
		--[Col5431],
		--[Col5432],
		--[Col5433],
		--[Col5434],
		--[Col5435],
		--[Col5436],
		--[Col5437],
		--[Col5438],
		--[Col5439],
		--[Col5440],
		--[Col5441],
		--[Col5442],
		--[Col5443],
		--[Col5444],
		--[Col5445],
		--[Col5446],
		--[Col5447],
		--[Col5448],
		--[Col5449],
		--[Col5450],
		--[Col5451],
		--[Col5452],
		--[Col5453],
		--[Col5454],
		--[Col5455],
		--[Col5456],
		--[Col5457],
		--[Col5458],
		--[Col5459],
		--[Col5460],
		--[Col5461],
		--[Col5462],
		--[Col5463],
		--[Col5464],
		--[Col5465],
		--[Col5466],
		--[Col5467],
		--[Col5468],
		--[Col5469],
		--[Col5470],
		--[Col5471],
		--[Col5472],
		--[Col5473],
		--[Col5474],
		--[Col5475],
		--[Col5476],
		--[Col5477],
		--[Col5478],
		--[Col5479],
		--[Col5480],
		--[Col5481],
		--[Col5482],
		--[Col5483],
		--[Col5484],
		--[Col5485],
		--[Col5486],
		--[Col5487],
		--[Col5488],
		--[Col5489],
		--[Col5490],
		--[Col5491],
		--[Col5492],
		--[Col5493],
		--[Col5494],
		--[Col5495],
		--[Col5496],
		--[Col5497],
		--[Col5498],
		--[Col5499],
		--[Col5500],
		--[Col5501],
		--[Col5502],
		--[Col5503],
		--[Col5504],
		--[Col5505],
		--[Col5506],
		--[Col5507],
		--[Col5508],
		--[Col5509],
		--[Col5510],
		--[Col5511],
		--[Col5512],
		--[Col5513],
		--[Col5514],
		--[Col5515],
		--[Col5516],
		--[Col5517],
		--[Col5518],
		--[Col5519],
		--[Col5520],
		--[Col5521],
		--[Col5522],
		--[Col5523],
		--[Col5524],
		--[Col5525],
		--[Col5526],
		--[Col5527],
		--[Col5528],
		--[Col5529],
		--[Col5530],
		--[Col5531],
		--[Col5532],
		--[Col5533],
		--[Col5534],
		--[Col5535],
		--[Col5536],
		--[Col5537],
		--[Col5538],
		--[Col5539],
		--[Col5540],
		--[Col5541],
		--[Col5542],
		--[Col5543],
		--[Col5544],
		--[Col5545],
		--[Col5546],
		--[Col5547],
		--[Col5548],
		--[Col5549],
		--[Col5550],
		--[Col5551],
		--[Col5552],
		--[Col5553],
		--[Col5554],
		--[Col5555],
		--[Col5556],
		--[Col5557],
		--[Col5558],
		--[Col5559],
		--[Col5560],
		--[Col5561],
		--[Col5562],
		--[Col5563],
		--[Col5564],
		--[Col5565],
		--[Col5566],
		--[Col5567],
		--[Col5568],
		--[Col5569],
		--[Col5570],
		--[Col5571],
		--[Col5572],
		--[Col5573],
		--[Col5574],
		--[Col5575],
		--[Col5576],
		--[Col5577],
		--[Col5578],
		--[Col5579],
		--[Col5580],
		--[Col5581],
		--[Col5582],
		--[Col5583],
		--[Col5584],
		--[Col5585],
		--[Col5586],
		--[Col5587],
		--[Col5588],
		--[Col5589],
		--[Col5590],
		--[Col5591],
		--[Col5592],
		--[Col5593],
		--[Col5594],
		--[Col5595],
		--[Col5596],
		--[Col5597],
		--[Col5598],
		--[Col5599],
		--[Col5600],
		--[Col5601],
		--[Col5602],
		--[Col5603],
		--[Col5604],
		--[Col5605],
		--[Col5606],
		--[Col5607],
		--[Col5608],
		--[Col5609],
		--[Col5610],
		--[Col5611],
		--[Col5612],
		--[Col5613],
		--[Col5614],
		--[Col5615],
		--[Col5616],
		--[Col5617],
		--[Col5618],
		--[Col5619],
		--[Col5620],
		--[Col5621],
		--[Col5622],
		--[Col5623],
		--[Col5624],
		--[Col5625],
		--[Col5626],
		--[Col5627],
		--[Col5628],
		--[Col5629],
		--[Col5630],
		--[Col5631],
		--[Col5632],
		--[Col5633],
		--[Col5634],
		--[Col5635],
		--[Col5636],
		--[Col5637],
		--[Col5638],
		--[Col5639],
		--[Col5640],
		--[Col5641],
		--[Col5642],
		--[Col5643],
		--[Col5644],
		--[Col5645],
		--[Col5646],
		--[Col5647],
		--[Col5648],
		--[Col5649],
		--[Col5650],
		--[Col5651],
		--[Col5652],
		--[Col5653],
		--[Col5654],
		--[Col5655],
		--[Col5656],
		--[Col5657],
		--[Col5658],
		--[Col5659],
		--[Col5660],
		--[Col5661],
		--[Col5662],
		--[Col5663],
		--[Col5664],
		--[Col5665],
		--[Col5666],
		--[Col5667],
		--[Col5668],
		--[Col5669],
		--[Col5670],
		--[Col5671],
		--[Col5672],
		--[Col5673],
		--[Col5674],
		--[Col5675],
		--[Col5676],
		--[Col5677],
		--[Col5678],
		--[Col5679],
		--[Col5680],
		--[Col5681],
		--[Col5682],
		--[Col5683],
		--[Col5684],
		--[Col5685],
		--[Col5686],
		--[Col5687],
		--[Col5688],
		--[Col5689],
		--[Col5690],
		--[Col5691],
		--[Col5692],
		--[Col5693],
		--[Col5694],
		--[Col5695],
		--[Col5696],
		--[Col5697],
		--[Col5698],
		--[Col5699],
		--[Col5700],
		--[Col5701],
		--[Col5702],
		--[Col5703],
		--[Col5704],
		--[Col5705],
		--[Col5706],
		--[Col5707],
		--[Col5708],
		--[Col5709],
		--[Col5710],
		--[Col5711],
		--[Col5712],
		--[Col5713],
		--[Col5714],
		--[Col5715],
		--[Col5716],
		--[Col5717],
		--[Col5718],
		--[Col5719],
		--[Col5720],
		--[Col5721],
		--[Col5722],
		--[Col5723],
		--[Col5724],
		--[Col5725],
		--[Col5726],
		--[Col5727],
		--[Col5728],
		--[Col5729],
		--[Col5730],
		--[Col5731],
		--[Col5732],
		--[Col5733],
		--[Col5734],
		--[Col5735],
		--[Col5736],
		--[Col5737],
		--[Col5738],
		--[Col5739],
		--[Col5740],
		--[Col5741],
		--[Col5742],
		--[Col5743],
		--[Col5744],
		--[Col5745],
		--[Col5746],
		--[Col5747],
		--[Col5748],
		--[Col5749],
		--[Col5750],
		--[Col5751],
		--[Col5752],
		--[Col5753],
		--[Col5754],
		--[Col5755],
		--[Col5756],
		--[Col5757],
		--[Col5758],
		--[Col5759],
		--[Col5760],
		--[Col5761],
		--[Col5762],
		--[Col5763],
		--[Col5764],
		--[Col5765],
		--[Col5766],
		--[Col5767],
		--[Col5768],
		--[Col5769],
		--[Col5770],
		--[Col5771],
		--[Col5772],
		--[Col5773],
		--[Col5774],
		--[Col5775],
		--[Col5776],
		--[Col5777],
		--[Col5778],
		--[Col5779],
		--[Col5780],
		--[Col5781],
		--[Col5782],
		--[Col5783],
		--[Col5784],
		--[Col5785],
		--[Col5786],
		--[Col5787],
		--[Col5788],
		--[Col5789],
		--[Col5790],
		--[Col5791],
		--[Col5792],
		--[Col5793],
		--[Col5794],
		--[Col5795],
		--[Col5796],
		--[Col5797],
		--[Col5798],
		--[Col5799],
		--[Col5800],
		--[Col5801],
		--[Col5802],
		--[Col5803],
		--[Col5804],
		--[Col5805],
		--[Col5806],
		--[Col5807],
		--[Col5808],
		--[Col5809],
		--[Col5810],
		--[Col5811],
		--[Col5812],
		--[Col5813],
		--[Col5814],
		--[Col5815],
		--[Col5816],
		--[Col5817],
		--[Col5818],
		--[Col5819],
		--[Col5820],
		--[Col5821],
		--[Col5822],
		--[Col5823],
		--[Col5824],
		--[Col5825],
		--[Col5826],
		--[Col5827],
		--[Col5828],
		--[Col5829],
		--[Col5830],
		--[Col5831],
		--[Col5832],
		--[Col5833],
		--[Col5834],
		--[Col5835],
		--[Col5836],
		--[Col5837],
		--[Col5838],
		--[Col5839],
		--[Col5840],
		--[Col5841],
		--[Col5842],
		--[Col5843],
		--[Col5844],
		--[Col5845],
		--[Col5846],
		--[Col5847],
		--[Col5848],
		--[Col5849],
		--[Col5850],
		--[Col5851],
		--[Col5852],
		--[Col5853],
		--[Col5854],
		--[Col5855],
		--[Col5856],
		--[Col5857],
		--[Col5858],
		--[Col5859],
		--[Col5860],
		--[Col5861],
		--[Col5862],
		--[Col5863],
		--[Col5864],
		--[Col5865],
		--[Col5866],
		--[Col5867],
		--[Col5868],
		--[Col5869],
		--[Col5870],
		--[Col5871],
		--[Col5872],
		--[Col5873],
		--[Col5874],
		--[Col5875],
		--[Col5876],
		--[Col5877],
		--[Col5878],
		--[Col5879],
		--[Col5880],
		--[Col5881],
		--[Col5882],
		--[Col5883],
		--[Col5884],
		--[Col5885],
		--[Col5886],
		--[Col5887],
		--[Col5888],
		--[Col5889],
		--[Col5890],
		--[Col5891],
		--[Col5892],
		--[Col5893],
		--[Col5894],
		--[Col5895],
		--[Col5896],
		--[Col5897],
		--[Col5898],
		--[Col5899],
		--[Col5900],
		--[Col5901],
		--[Col5902],
		--[Col5903],
		--[Col5904],
		--[Col5905],
		--[Col5906],
		--[Col5907],
		--[Col5908],
		--[Col5909],
		--[Col5910],
		--[Col5911],
		--[Col5912],
		--[Col5913],
		--[Col5914],
		--[Col5915],
		--[Col5916],
		--[Col5917],
		--[Col5918],
		--[Col5919],
		--[Col5920],
		--[Col5921],
		--[Col5922],
		--[Col5923],
		--[Col5924],
		--[Col5925],
		--[Col5926],
		--[Col5927],
		--[Col5928],
		--[Col5929],
		--[Col5930],
		--[Col5931],
		--[Col5932],
		--[Col5933],
		--[Col5934],
		--[Col5935],
		--[Col5936],
		--[Col5937],
		--[Col5938],
		--[Col5939],
		--[Col5940],
		--[Col5941],
		--[Col5942],
		--[Col5943],
		--[Col5944],
		--[Col5945],
		--[Col5946],
		--[Col5947],
		--[Col5948],
		--[Col5949],
		--[Col5950],
		--[Col5951],
		--[Col5952],
		--[Col5953],
		--[Col5954],
		--[Col5955],
		--[Col5956],
		--[Col5957],
		--[Col5958],
		--[Col5959],
		--[Col5960],
		--[Col5961],
		--[Col5962],
		--[Col5963],
		--[Col5964],
		--[Col5965],
		--[Col5966],
		--[Col5967],
		--[Col5968],
		--[Col5969],
		--[Col5970],
		--[Col5971],
		--[Col5972],
		--[Col5973],
		--[Col5974],
		--[Col5975],
		--[Col5976],
		--[Col5977],
		--[Col5978],
		--[Col5979],
		--[Col5980],
		--[Col5981],
		--[Col5982],
		--[Col5983],
		--[Col5984],
		--[Col5985],
		--[Col5986],
		--[Col5987],
		--[Col5988],
		--[Col5989],
		--[Col5990],
		--[Col5991],
		--[Col5992],
		--[Col5993],
		--[Col5994],
		--[Col5995],
		--[Col5996],
		--[Col5997],
		--[Col5998],
		--[Col5999]
	FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
	INNER JOIN dbo.VW_UnitLastChannelValueTimestamp vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
	LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
	LEFT JOIN dbo.EventChannelLatestValue eclv WITH (NOLOCK) ON fs.UnitID = eclv.UnitID
	LEFT JOIN @currentVehicleFault vf ON fs.VehicleID = vf.VehicleID

END
GO
RAISERROR ('-- update NSP_FleetSummaryDrilldown', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummaryDrilldown' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_FleetSummaryDrilldown] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_FleetSummaryDrilldown
**	Description:	Returns live channel data for the application
**	Call frequency:	When user click on grouped cell on Fleet Summary
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
	@UnitIdList varchar(30)
	, @SelectColList varchar(4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @sql varchar(max)
	
	SET @sql = '
SELECT DISTINCT
	cv.UnitID
	, ' + @SelectColList + '
	, fs.UnitPosition
FROM dbo.VW_FleetStatus fs
INNER JOIN dbo.VW_UnitLastChannelValueTimestamp vlc ON vlc.UnitID = fs.UnitID
LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
LEFT JOIN dbo.EventChannelLatestValue eclv WITH (NOLOCK) ON vlc.UnitID = eclv.UnitID
WHERE fs.UnitID in (' + @UnitIdList + ')
ORDER BY 
	fs.UnitPosition'

	EXEC (@sql)
	
END
GO
RAISERROR ('-- update NSP_Genius_CreateNewFile', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_CreateNewFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_CreateNewFile] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_Genius_CreateNewFile]
	 @file varchar(255)
	,@fileTimeStamp datetime
	,@fileId int out
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	creates a new entry for a new import file in the Genius 
**	Interface tables	
**	Return values: returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-22		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
		-- insert filename
		insert dbo.GeniusInterfaceFile (Name ,FileTimeStamp)
		values (@file,@fileTimeStamp) ;
	
		set @fileId = scope_identity();	
	
	end try
	begin catch
		exec dbo.NSP_RethrowError;
		return -1;
	end catch	
	
end
GO
RAISERROR ('-- update NSP_Genius_ImportDataFromStaging', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_ImportDataFromStaging' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_ImportDataFromStaging] AS BEGIN RETURN(1) END;')
GO



ALTER PROCEDURE [dbo].[NSP_Genius_ImportDataFromStaging]
	 @fileId int
	,@cutoffTime time = '03:00'	 
as 
/******************************************************************************
**	Name: NSP_Genius_ImportDataFromStaging
**	Description:	imports the data from the staging table into the Spectrum
**	data tables
**	
**	Return values: returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History 
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-25		HeZ	creation on database
** 2012-03-14	HeZ	adapted for real life data from Genius
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	DECLARE @unitNumber varchar(10) = (
		SELECT TOP 1 UnitNumber 
		FROM dbo.GeniusStaging 
		WHERE GeniusInterfaceFileID = @fileId 
		AND UnitNumber IS NOT NULL
	)
	DECLARE @unitID int = (SELECT ID FROM dbo.Unit WHERE UnitNumber = @unitNumber)
	DECLARE @logText varchar(2000);
	DECLARE @rowNumber int
	--begin tran

	begin try

--debug
--PRINT ISNULL(@unitId, '-5000')

	BEGIN TRAN
	DELETE dbo.FleetStatusStaging WHERE UnitID = @unitId;
	
	WITH CteGeniusStaging AS
	(
	select distinct
	 gs.DiagramNumber	as DiagramNumber
	,@UnitID	as UnitId
	,gs.HeadCode	as HeadCode
	,gs.TrainStartLocation	as TrainStartLocation
	,glts.ID	as TrainStartLocationId
	,gs.TrainStartTime	as TrainStartTime
	,te.LegEndLocation	as TrainEndLocation
	,glte.ID	as TrainEndLocationId
	,te.LegEndTime	as TrainEndTime
	,gs.LegStartLocation	as LegStartLocation
	,glls.ID	as LegStartLocationId
	,gs.LegStartTime	as LegStartTime
	,gs.LegEndLocation	as LegEndLocation
	,glle.ID	as LegEndLocationId
	,gs.LegEndTime	as LegEndTime
	,gs.LegLength	as LegLength
	,gs.CoupledUnits	as CoupledUnits	
	,isnull(gs.UnitPosition, 1)	as UnitPosition
	,gs.VehicleFormation	as VehicleFormation
	,/*d.DiagramDate*/ NULL	as DiagramDate
	,row_number() over 
	(partition by gs.DiagramNumber 
	 order by gs.LegStartTime asc)	as SequenceNumber
	from
	dbo.GeniusStaging gs
	inner hash join (
	select HeadCode, DiagramNumber, LegEndLocation, LegEndTime
	from dbo.GeniusStaging gs2
	where LegEndTime = (select max(LegEndTime) from dbo.GeniusStaging gs3
	where gs2.DiagramNumber = gs3.DiagramNumber
	and gs2.HeadCode = gs3.HeadCode
	and gs3.GeniusInterfaceFileID = @fileId)
	and gs2.GeniusInterfaceFileID = @fileId) as te
	on gs.DiagramNumber = te.DiagramNumber and gs.HeadCode = te.HeadCode
	left outer join dbo.Location glts on glts.Tiploc = gs.TrainStartLocation
	left outer join dbo.Location glte on glte.Tiploc = gs.TrainEndLocation
	left outer join dbo.Location glls on glls.Tiploc = gs.LegStartLocation
	left outer join dbo.Location glle on glle.Tiploc = gs.LegEndLocation
	where 
	gs.GeniusInterfaceFileID = @fileId
	)
	INSERT INTO [dbo].[FleetStatusStaging]
		([UnitId]
		,[DiagramNumber]
		,[HeadCode]
		,[TrainStartLocationId]
		,[TrainStartLocationTiploc]
		,[TrainStartTime]
		,[TrainEndLocationId]
		,[TrainEndLocationTiploc]
		,[TrainEndTime]
		,[StartLocationId]
		,[StartLocationTiploc]
		,[StartTime]
		,[EndLocationId]
		,[EndLocationTipLoc]
		,[EndTime]
		,[Length]
		,[CoupledUnits]
		,[UnitPosition]
		,[VehicleFormation]
		,[SequenceNumber])
	 SELECT
	[UnitId]
		,DiagramNumber
		,[HeadCode]
				
		,[TrainStartLocationId]
		,[TrainStartLocation]
		,dbo.FN_ConvertBstToGmt([TrainStartTime])
				
		,[TrainEndLocationId]
		,[TrainEndLocation]
		,dbo.FN_ConvertBstToGmt([TrainEndTime])
				
		,[LegStartLocationId]
		,[LegStartLocation]
		,dbo.FN_ConvertBstToGmt([LegStartTime])
				
		,[LegEndLocationId]
		,[LegEndLocation]
		,dbo.FN_ConvertBstToGmt([LegEndTime])
				
		,[LegLength]
		,[CoupledUnits]
		,[UnitPosition]
		,[VehicleFormation]
		,[SequenceNumber]
	FROM CteGeniusStaging
	
	SET @rowNumber = @@ROWCOUNT

	COMMIT


	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileId, @status = 'File fully imported.'
	select @logText = convert(varchar, @rowNumber) + ' Rows successfully imported.'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;
	--commit;
	
	end try
	begin catch
	--if @@trancount > 0 rollback;
		SELECT @logText = ERROR_MESSAGE();	
	exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
RAISERROR ('-- update NSP_Genius_ImportDataToFleetSummaryStaging', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_ImportDataToFleetSummaryStaging' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_ImportDataToFleetSummaryStaging] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_Genius_ImportDataToFleetSummaryStaging]
AS
BEGIN

	SET NOCOUNT ON;
	--------------------------------------------------------------------------------
	-- Ignore files for units not existing in Spectrum DB
	--------------------------------------------------------------------------------

	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - UnitNumber does not exist in Unit table'
	WHERE ProcessingStatus = 'Data loaded from file'
	AND	dbo.FN_GeniusGetUnitFromFilename(Name) NOT IN (SELECT UnitNumber FROM Unit)

	--------------------------------------------------------------------------------
	-- Ignore obsolete files
	--------------------------------------------------------------------------------
	;WITH CteFilesToProcess
	AS
	(
	SELECT 
	ID
	, TimeGenerated	= dbo.FN_GeniusGetTimestampFromFilename(Name)
	, Unit	= dbo.FN_GeniusGetUnitFromFilename(Name)
	, RowNumberPerUnit	= ROW_NUMBER() OVER (PARTITION BY dbo.FN_GeniusGetUnitFromFilename(Name) ORDER BY dbo.FN_GeniusGetTimestampFromFilename(Name) DESC)
	FROM dbo.GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	)
	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - Oboslete data for unit'
	WHERE ID IN (
	SELECT ID 
	FROM CteFilesToProcess
	WHERE RowNumberPerUnit <> 1
	)


	--------------------------------------------------------------------------------
	-- Validate and Copy to FleetSummaryStaging
	--------------------------------------------------------------------------------
	DECLARE @FileID int
	WHILE 1 = 1
	BEGIN
	SET @FileID = (
	SELECT MIN(ID)
	FROM GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	AND ID > ISNULL(@FileID, 0)
	)
	IF @FileID IS NULL BREAK
	-----------------------------------------------------------

	-- Truncate GeniusStaging table
	TRUNCATE TABLE dbo.GeniusStaging
	
	PRINT '--------------------------------------------------------------------------------'
	
	PRINT 'NSP_Genius_ValidateAndCopy FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ValidateAndCopy @FileID
	
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Loaded to Staging Table'
	BEGIN
	PRINT 'NSP_Genius_ImportDataFromStaging FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ImportDataFromStaging @FileID
	END
	
	-- If File status has not changed rais error
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Data loaded from file'
	BEGIN
	
	EXEC dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Error - Unknown error in NSP_Genius_ImportDataToFleetSummaryStaging'
	END
	END

END
GO
RAISERROR ('-- update NSP_Genius_SetFileId', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_SetFileId' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_SetFileId] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_Genius_SetFileId]
	 @fileid int
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**	status to 'loaded'	
**	Return values: returns -1 if an error occurred
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-21		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
	-- set file id for loaded records
	update
	dbo.GeniusInterfaceData
	set
	GeniusInterfaceFileId = @fileid
	where
	GeniusInterfaceFileId is null
	;
	
	-- update file processing status
	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileid, @status = 'Data loaded from file'

	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
RAISERROR ('-- update NSP_Genius_UpdateFileStatus', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_UpdateFileStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_UpdateFileStatus] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_Genius_UpdateFileStatus]
	 @fileId int = null
	,@fileName varchar(255) = null
	,@status varchar(255)
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**					status to 'loaded'	
**	Return values: returns -1 if an error occurred
*******************************************************************************
**	Parameters:
**	Input
**	-----------
**	 @fileId		id of the imported file
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:				Author:		Description:
**	2011-09-21			HeZ			creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
		-- either filename or file id must be provided
		if (@fileId is null and @fileName is null)
		begin
			raiserror(N'Either file name or file Id must be provided!', 11, 1);
			return -1;
		end
		
		-- update the status
		update
			dbo.GeniusInterfaceFile
		set
			ProcessingStatus = @status
		where
			ID = isnull(@fileId, ID)
			and Name = isnull(@fileName, Name)
		;
		
	end try
	begin catch
		exec dbo.NSP_RethrowError;
		return -1;
	end catch	
		
	return 0;
end

GO
RAISERROR ('-- update NSP_Genius_ValidateAndCopy', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_ValidateAndCopy' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_ValidateAndCopy] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_Genius_ValidateAndCopy]
	 @FileId int
AS
/******************************************************************************
**	Name:	NSP_Genius_ValidateAndCopy
**	Description:	Checks the imported data against various rules
**	if all checks complete, data is copied to staging table	
**	Return values: Returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************/	

begin
	SET NOCOUNT ON;
	
	DECLARE @unitNo varchar(16);
	DECLARE @logText varchar(500);
	
	begin try
	-- check 1: only one unit per file
	if (select count(distinct UnitNumber) 
	from dbo.GeniusInterfaceData
	where GeniusInterfaceFileId = @fileId) <> 1
	begin
	raiserror(N'File contains more than one Unit Number!', 11, 1);
	exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File contains more than one Unit Number!';	
	end
	
	-- check 2: is unit on this side of the fleet?
	select top(1)
	@unitNo = UnitNumber
	from
	dbo.GeniusInterfaceData
	where 
	GeniusInterfaceFileId = @fileId
	;
	
	if not exists (select 1 from dbo.Unit where UnitNumber = @unitNo)
	begin
		-- unit doesn't exist on this side
		exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. Unit Number not in database.'
		exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File closed. Unit Number not in database.';	
		raiserror(N'File closed. Unit Number not in database.', 11, 1);
		--return 0;	
	end
	
	select @logText = 'Importing data for Unit: ' + @unitNo;
	
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;	

	-- check 3: only deallocations may have no Headcode
	if exists ( 
	select 1 
	from dbo.GeniusInterfaceData 
	where GeniusInterfaceFileId = @fileId
	and HeadCode is null
	and dbo.fn_CheckZero(DiagramId) <> 0)
	begin
	-- Headcode missing
	exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. headcode missing.'
	exec NSP_Genius_WriteLog @fileId, 2, 'For at least one Leg the headcode is missing.';	
	raiserror(N'For at least one Leg the headcode is missing.', 11, 1);
	end

	
	BEGIN TRAN
	
	DELETE FROM dbo.GeniusStaging
	WHERE UnitNumber = @unitNo
	
	INSERT INTO dbo.GeniusStaging
	(GeniusInterfaceFileID
	,DiagramNumber
	,UnitNumber
	,HeadCode
	,TrainStartLocation
	,TrainStartTime
	,TrainEndLocation
	,TrainEndTime
	,LegStartLocation
	,LegStartTime
	,LegEndLocation
	,LegEndTime
	,LegLength
	,VehicleFormation
	,CoupledUnits
	,UnitPosition
	,SequenceNumber)
	SELECT 
	 GeniusInterfaceFileId
	,isnull(DiagramId, 'NA-' + UnitNumber)
	,UnitNumber
	,isnull(HeadCode, 'NA-'+UnitNumber)
	,TrainStartLocation
	,convert(datetime, TrainStartTime, 103)
	,(	
	SELECT TOP 1 LegEndLocation
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndLocation
	,(	
	SELECT TOP 1 convert(datetime, LegEndTime, 103)
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndTime
	,LegStartLocation
	,convert(datetime, LegStartTime, 103)
	,LegEndLocation
	,convert(datetime, LegEndTime, 103)
	,convert(decimal(5,2), isnull(LegLength, 0))
	,VehicleFormation
	,CoupledUnits
	,convert(tinyint, UnitPosition)
	,convert(int, isnull(SequenceNumber, 0))
	FROM 
	dbo.GeniusInterfaceData gid1
	WHERE 
	GeniusInterfaceFileId = @fileId
	;
	IF @@ERROR <> 0
	ROLLBACK
	ELSE 
	COMMIT TRAN
	
	
	exec dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Loaded to Staging Table'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, 'Loaded to Staging Table';	
	
	end try
	begin catch
		SELECT @logText = ERROR_MESSAGE();
		exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end

GO
RAISERROR ('-- update NSP_Genius_WriteLog', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_Genius_WriteLog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_Genius_WriteLog] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_Genius_WriteLog]
	 @fileId int 
	,@logType tinyint = 1
	,@logText varchar(500)
as 
/******************************************************************************
**	Name: NSP_Genius_WriteLog
**	Description:	
**	Return values: returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-10-28		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
	-- write log entry
	insert
	dbo.GeniusLog
	(GeniusInterfaceFileId
	,LogType
	,LogText)
	values
	(@fileId
	,@logType
	,@logText)
	;
	
	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end


GO
RAISERROR ('-- update NSP_GeniusImportProcessFile', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GeniusImportProcessFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GeniusImportProcessFile] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GeniusImportProcessFile
**	Description:	Updates GeniusStaging table with data from the file
**	Call frequency:	Every time new Genius file is received
**	Parameters:		@FileID - ID from LoadGeniusFile
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/07/06 13:41:13 $$
**	$$Revision: 1.2.2.1 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/Attic/update_spectrum_db_009.sql,v $$
******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GeniusImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET DATEFORMAT Mdy

	IF (SELECT COUNT(1) FROM LoadGeniusData WHERE LoadGeniusFileID = @FileID) > 0 
	BEGIN

		DECLARE @bstToGmtTimeOffset smallint
		SET @bstToGmtTimeOffset = - dbo.FN_GetGmtToBstTimeDiff(GETDATE())
	
		BEGIN TRAN
		
		TRUNCATE TABLE [GeniusStaging]

		INSERT INTO dbo.GeniusStaging
			([GeniusInterfaceFileID]
			,[DiagramNumber]
			,Headcode
			,[TrainStartLocation]
			,[TrainEndLocation]
			,[TrainStartTime]
			,[TrainEndTime]
			,[UnitNumber]
			,[UnitPosition])
		SELECT 
			@FileID																--LoadGeniusFileID
			, REPLACE(REPLACE(Diagram, CHAR(10), ''), CHAR(13), '')				--Diagram
			, LEFT(REPLACE(REPLACE(Headcode, CHAR(10), ''), CHAR(13), ''), 4)	--Headcode
			, RTRIM(REPLACE(REPLACE(LocationStart, CHAR(10), ''), CHAR(13), ''))--LocationStart
			, RTRIM(REPLACE(REPLACE(LocationEnd, CHAR(10), ''), CHAR(13), ''))	--LocationEnd
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeStart)					--TimeStart
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeEnd)					--TimeEnd
			, REPLACE(REPLACE(REPLACE(UnitNumber, CHAR(10), ''), CHAR(13), ''),'455','5') --UnitNumber
			, CASE 
				WHEN ISNUMERIC(UnitPosition) = 1
					THEN UnitPosition
				ELSE NULL
			END														--Position
		FROM LoadGeniusData 
		WHERE LoadGeniusFileID = @FileID
			AND Headcode IS NOT NULL
			AND Headcode <> ''
			
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK
			UPDATE LoadGeniusFile SET 
				Status = 'ERROR - Insert to LoadGeniusData failed'
			WHERE ID = @FileID
		END
		ELSE
			COMMIT
			
	END

END


GO
RAISERROR ('-- update NSP_GeniusUpdateFleetStatus', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GeniusUpdateFleetStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GeniusUpdateFleetStatus] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GeniusUpdateFleetStatus
**	Description:	Updates FleetStatus table with current Headcode details
**	Call frequency:	Every 60s
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/10/02 13:58:48 $$
**	$$Revision: 1.2.2.1 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/Attic/update_spectrum_db_013.sql,v $$
******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GeniusUpdateFleetStatus] 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @currentTimestamp datetime		= DATEADD(ms, - DATEPART(ms, GETDATE()), GETDATE()) --timestamp with ms = 000
	DECLARE @gmtToBstTimeOffset smallint	= 0		-- FCC stores channel data with timestamps including DLS -- dbo.FN_GetGmtToBstTimeDiff(GETUTCDATE())
	DECLARE @headcodeBeforeDeparture int	= 20	-- number of minutes before departure time that Headcode is displayed
	DECLARE @headcodeAfterArrival int		= 60	-- number of minutes after arrival time that Headcode is displayed (unless new headcode is on
	DECLARE @unitID int
	DECLARE @fleetFormationID int
	

	IF OBJECT_ID('tempdb.dbo.#FleetConfig') IS NOT NULL
		DROP TABLE #FleetConfig
		
	;WITH CteHeadcode
	AS
	(
		-- Current headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 1 AS Priority
		FROM FleetStatusStaging
		WHERE @currentTimestamp >= TrainStartTime 
			AND @currentTimestamp < TrainEndTime

		UNION ALL
		
		-- Following headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 2 AS Priority
		FROM FleetStatusStaging
		WHERE 
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN DATEADD(n, - @headcodeBeforeDeparture, TrainStartTime) AND TrainEndTime

		UNION ALL

		-- Finished headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 3 AS Priority
		FROM FleetStatusStaging
		WHERE
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN TrainStartTime AND DATEADD(n, @headcodeAfterArrival, TrainEndTime)
	)
	SELECT 
		ID AS UnitID	
		, UnitNumber
		, CASE
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1 ORDER BY TrainStartTime DESC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2 ORDER BY TrainStartTime ASC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3 ORDER BY TrainEndTime DESC)
		END HeadcodeID
	INTO #FleetConfig
	FROM Unit

	WHILE 1 = 1 
	BEGIN
		
		SET @unitID = (SELECT MIN(UnitID) FROM #FleetConfig WHERE UnitID > ISNULL(@unitID, 0))
		IF @unitID IS NULL BREAK
		------------------------------------------------------------------------------------------------------
		
		SET @fleetFormationID = NULL
		
		BEGIN TRY
		
			--Insert formation if does not exist
			SET @fleetFormationID = (
				SELECT ID
				FROM dbo.FleetFormation ff
				WHERE EXISTS 
					(SELECT 1
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
						AND TrainStartTime = ff.ValidFrom
						AND TrainEndTime = ff.ValidTo
						AND CoupledUnits = ff.UnitNumberList
					) 
				)
		
			IF @fleetFormationID IS NULL
			BEGIN
			
				INSERT INTO dbo.FleetFormation(
					UnitNumberList
					, UnitIDList
					, ValidFrom
					, ValidTo)
				SELECT 
					CoupledUnits
					, ''
					, TrainStartTime
					, TrainEndTime
				FROM #FleetConfig fc
				LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID 
				WHERE fc.UnitID = @unitID
					AND TrainStartTime IS NOT NULL
					AND TrainEndTime IS NOT NULL
					AND CoupledUnits IS NOT NULL

				IF @@ROWCOUNT > 0
					SET @fleetFormationID = (SELECT MAX(ID) FROM FleetFormation)
				
			END

			-- Check if unit status to be updated
			IF EXISTS (
				SELECT 1
				FROM dbo.FleetStatus
				LEFT JOIN (
					SELECT 
						UnitID				= fc.UnitID
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				) AS CteFleetConfig ON FleetStatus.UnitID = CteFleetConfig.UnitID
			WHERE ISNULL(FleetStatus.Diagram, 'NULL') <> ISNULL(HeadcodeDiagram, 'NULL')
				AND FleetStatus.UnitID = @unitID
				AND FleetStatus.ValidTo IS NULL	
)
			
			BEGIN

				-- 1. Close Headcodes that are completed
				UPDATE FleetStatus SET
					ValidTo = @currentTimestamp
				WHERE UnitID = @unitID
					AND ValidTo IS NULL

				-- 2. Add new headcodes
				;WITH CteFleetConfig
				AS
				(
					SELECT 
						UnitID				= fc.UnitID
						, UnitNumber		= fc.UnitNumber
						, Diagram			= DiagramNumber
						, Headcode			= LEFT(Headcode, 4)
						, LocationStart		= TrainStartLocationTiploc
						, LocationEnd		= TrainEndLocationTiploc
						, LocationIdStart	= TrainStartLocationId
						, LocationIdEnd		= TrainEndLocationId
						, TimeStart			= TrainStartTime
						, TimeEnd			= TrainEndTime
						, Position			= UnitPosition
						, CoupledUnits		= REPLACE(CoupledUnits, ';',',')
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				)
				INSERT INTO dbo.FleetStatus(
					UnitID
					, ValidFrom
					, ValidTo
					, Setcode
					, Diagram
					, Headcode
					, IsValid
					, Loading
					, ConfigDate
					, UnitList
					, LocationIDHeadcodeStart
					, LocationIDHeadcodeEnd
					, UnitPosition
					, FleetFormationID
				)
				SELECT
					UnitID
					, ValidFrom		= @currentTimestamp
					, ValidTo		= NULL
					, Setcode		= NULL
					, Diagram		= CteFleetConfig.HeadcodeDiagram
					, Headcode		= CteFleetConfig.Headcode
					, IsValid		= 1
					, Loading		= NULL
					, ConfigDate	= SYSDATETIME()
					, UnitList		= CoupledUnits
					, LocationIDHeadcodeStart	= LocationIdStart
					, LocationIDHeadcodeEnd		= LocationIdEnd
					, UnitPosition				= ISNULL(Position, 1)
					, FleetFormationID			= @fleetFormationID
				FROM CteFleetConfig
				WHERE UnitID = @unitID
				
			END
			
		END TRY
		BEGIN CATCH
			PRINT 'Error: Unknown error when updating headcode'
			PRINT 'UnitID			' + CAST(@unitID AS varchar(10))
			PRINT 'CurrentTimestamp ' + CONVERT(char(20), @currentTimestamp, 121)
		END CATCH


	END -- WHILE

END -- stored procedure


GO
RAISERROR ('-- update NSP_GetCurrentFaults', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFaults' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetCurrentFaults] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaults]
	@N int
	, @isLive		 bit		 = NULL	--Live
	, @isAcknowledged bit		 = NULL	--Acknowledged
	, @FleetCodes	 varchar(100) = NULL	--Fleet
	, @FaultTypeIDs	varchar(100) = NULL	--Severity
	, @CategoryIDs	varchar(100) = NULL	--Category
	, @HasRecoveryOnly	bit	 = NULL	--IAS
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FleetTable TABLE (ID varchar(100))
	INSERT INTO @FleetTable
	SELECT convert(int, item) from dbo.SplitStrings_CTE(@FleetCodes,',')

	DECLARE @TypeTable TABLE (ID int)
	INSERT INTO @TypeTable
	SELECT convert(int, item) from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

	DECLARE @CategoryTable TABLE (ID int)
	INSERT INTO @CategoryTable
	SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')

	IF @FleetCodes	= '' SET @FleetCodes	= NULL
	IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
	IF @CategoryIDs = '' SET @CategoryIDs = NULL

	SELECT TOP (@N)
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber	 = up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
		, f.IsAcknowledged
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location l ON f.LocationID = l.ID
		LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
		LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
		LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE 
			(@isLive		 IS NULL OR			f.IsCurrent	 = @isLive)
		AND (@isAcknowledged IS NULL OR			f.IsAcknowledged = @isAcknowledged)
		AND (@FaultTypeIDs	IS NULL OR			f.FaultTypeID	IN (SELECT ID FROM @TypeTable))
		AND (@CategoryIDs	IS NULL OR			f.CategoryID	 IN (SELECT ID FROM @CategoryTable))
		AND (@FleetCodes	 IS NULL OR			f.FleetCode	 IN (SELECT ID FROM @FleetTable))
		AND (@HasRecoveryOnly	IS NULL OR		f.HasRecovery	= @HasRecoveryOnly						OR @HasRecoveryOnly = 0)
		AND ReportingOnly = 0
	ORDER BY 
		CreateTime DESC

END 


GO
RAISERROR ('-- update NSP_GetFaultChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelData] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultChannelData
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelData]
(
	@FaultID int
	, @UnitID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		cv.[ID]
		,cv.[FaultID]
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		,cv.[TimeStamp]
		,[Col1], [Col2], [Col3], [Col4]
		--, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
	--	[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
	--	[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
	--	[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
	--	[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
	--	[Col97], [Col98], [Col99], [Col100],
	--	[Col101],[Col102],[Col103],[Col104],[Col105],[Col106],[Col107],[Col108],[Col109],[Col110],
	--	[Col111],[Col112],[Col113],[Col114],[Col115],[Col116],[Col117],[Col118],[Col119],[Col120],
	--	[Col121],[Col122],[Col123],[Col124],[Col125],[Col126],[Col127],[Col128],[Col129],[Col130],
	--	[Col131],[Col132],[Col133],[Col134],[Col135],[Col136],[Col137],[Col138],[Col139],[Col140],
	--	[Col141],[Col142],[Col143],[Col144],[Col145],[Col146],[Col147],[Col148],[Col149],[Col150],
	--	[Col151],[Col152],[Col153],[Col154],[Col155],[Col156],[Col157],[Col158],[Col159],[Col160],
	--	[Col161],[Col162],[Col163],[Col164],[Col165],[Col166],[Col167],[Col168],[Col169],[Col170],
	--	[Col171],[Col172],[Col173],[Col174],[Col175],[Col176],[Col177],[Col178],[Col179],[Col180],
	--	[Col181],[Col182],[Col183],[Col184],[Col185],[Col186],[Col187],[Col188],[Col189],[Col190],
	--	[Col191],[Col192],[Col193],[Col194],[Col195],[Col196],[Col197],[Col198],[Col199],[Col200],
	--[Col201],[Col202],[Col203],[Col204],[Col205],[Col206],[Col207],[Col208],[Col209],[Col210],
	--[Col211],[Col212],[Col213],[Col214],[Col215],[Col216],[Col217],[Col218],[Col219],[Col220],
	--[Col221],[Col222],[Col223],[Col224],[Col225],[Col226],[Col227],[Col228],[Col229],[Col230],
	--[Col231],[Col232],[Col233],[Col234],[Col235],[Col236],[Col237],[Col238],[Col239],[Col240],
	--[Col241],[Col242],[Col243],[Col244],[Col245],[Col246],[Col247],[Col248],[Col249],[Col250],
	--[Col251],[Col252],[Col253],[Col254],[Col255],[Col256],[Col257],[Col258],[Col259],[Col260],
	--[Col261],[Col262],[Col263],[Col264],[Col265],[Col266],[Col267],[Col268],[Col269],[Col270],
	--[Col271],[Col272],[Col273],[Col274],[Col275],[Col276],[Col277],[Col278],[Col279],[Col280],
	--[Col281],[Col282],[Col283],[Col284],[Col285],[Col286],[Col287],[Col288],[Col289],[Col290],
	--[Col291],[Col292],[Col293],[Col294],[Col295],[Col296],[Col297],[Col298],[Col299],[Col300],
	--	[Col301],[Col302],[Col303],[Col304],[Col305],[Col306],[Col307],[Col308],[Col309],[Col310],
	--	[Col311],[Col312],[Col313],[Col314],[Col315],[Col316],[Col317],[Col318],[Col319],[Col320],
	--	[Col321],[Col322],[Col323],[Col324],[Col325],[Col326],[Col327],[Col328],[Col329],[Col330],
	--	[Col331],[Col332],[Col333],[Col334],[Col335],[Col336],[Col337],[Col338],[Col339],[Col340],
	--	[Col341],[Col342],[Col343],[Col344],[Col345],[Col346],[Col347],[Col348],[Col349],[Col350],
	--	[Col351],[Col352],[Col353],[Col354],[Col355],[Col356],[Col357],[Col358],[Col359],[Col360],
	--	[Col361],[Col362],[Col363],[Col364],[Col365],[Col366],[Col367],[Col368],[Col369],[Col370],
	--	[Col371],[Col372],[Col373],[Col374],[Col375],[Col376],[Col377],[Col378],[Col379],[Col380],
	--	[Col381],[Col382],[Col383],[Col384],[Col385],[Col386],[Col387],[Col388],[Col389],[Col390],
	--	[Col391],[Col392],[Col393],[Col394],[Col395],[Col396],[Col397],[Col398],[Col399],[Col400],
	--[Col401],[Col402],[Col403],[Col404],[Col405],[Col406],[Col407],[Col408],[Col409],[Col410],
	--[Col411],[Col412],[Col413],[Col414],[Col415],[Col416],[Col417],[Col418],[Col419],[Col420],
	--[Col421],[Col422],[Col423],[Col424],[Col425],[Col426],[Col427],[Col428],[Col429],[Col430],
	--[Col431],[Col432],[Col433],[Col434],[Col435],[Col436],[Col437],[Col438],[Col439],[Col440],
	--[Col441],[Col442],[Col443],[Col444],[Col445],[Col446],[Col447],[Col448],[Col449],[Col450],
	--[Col451],[Col452],[Col453],[Col454],[Col455],[Col456],[Col457],[Col458],[Col459],[Col460],
	--[Col461],[Col462],[Col463],[Col464],[Col465],[Col466],[Col467],[Col468],[Col469],[Col470],
	--[Col471],[Col472],[Col473],[Col474],[Col475],[Col476],[Col477],[Col478],[Col479],[Col480],
	--[Col481],[Col482],[Col483],[Col484],[Col485],[Col486],[Col487],[Col488],[Col489],[Col490],
	--[Col491],[Col492],[Col493],[Col494],[Col495],[Col496],[Col497],[Col498],[Col499],[Col500],
	--	[Col501],[Col502],[Col503],[Col504],[Col505],[Col506],[Col507],[Col508],[Col509],[Col510],
	--	[Col511],[Col512],[Col513],[Col514],[Col515],[Col516],[Col517],[Col518],[Col519],[Col520],
	--	[Col521],[Col522],[Col523],[Col524],[Col525],[Col526],[Col527],[Col528],[Col529],[Col530],
	--	[Col531],[Col532],[Col533],[Col534],[Col535],[Col536],[Col537],[Col538],[Col539],[Col540],
	--	[Col541],[Col542],[Col543],[Col544],[Col545],[Col546],[Col547],[Col548],[Col549],[Col550],
	--	[Col551],[Col552],[Col553],[Col554],[Col555],[Col556],[Col557],[Col558],[Col559],[Col560],
	--	[Col561],[Col562],[Col563],[Col564],[Col565],[Col566],[Col567],[Col568],[Col569],[Col570],
	--	[Col571],[Col572],[Col573],[Col574],[Col575],[Col576],[Col577],[Col578],[Col579],[Col580],
	--	[Col581],[Col582],[Col583],[Col584],[Col585],[Col586],[Col587],[Col588],[Col589],[Col590],
	--	[Col591],[Col592],[Col593],[Col594],[Col595],[Col596],[Col597],[Col598],[Col599],[Col600],
	--[Col601],[Col602],[Col603],[Col604],[Col605],[Col606],[Col607],[Col608],[Col609],[Col610],
	--[Col611],[Col612],[Col613],[Col614],[Col615],[Col616],[Col617],[Col618],[Col619],[Col620],
	--[Col621],[Col622],[Col623],[Col624],[Col625],[Col626],[Col627],[Col628],[Col629],[Col630],
	--[Col631],[Col632],[Col633],[Col634],[Col635],[Col636],[Col637],[Col638],[Col639],[Col640],
	--[Col641],[Col642],[Col643],[Col644],[Col645],[Col646],[Col647],[Col648],[Col649],[Col650],
	--[Col651],[Col652],[Col653],[Col654],[Col655],[Col656],[Col657],[Col658],[Col659],[Col660],
	--[Col661],[Col662],[Col663]
	FROM dbo.FaultChannelValue cv
	WHERE cv.FaultID = @FaultID
		AND cv.UnitID = @UnitID

END



GO
RAISERROR ('-- update NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelValue] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_GetFaultChannelValue
** Description:	Returns data to fault engine
** Call frequency: Every 5s 
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue]
	@DatetimeStart datetime
	, @DatetimeEnd datetime
	, @UnitID int = NULL
	, @UpdateRecord bit = NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		cv.UnitID
		, Headcode = CAST(NULL AS varchar(7))
		, u.UnitNumber
		, u.UnitType
		, ServiceStatus = CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, [TimeStamp]			= CONVERT(datetime, cv.TimeStamp)
		, l.ID AS LocationID
		, l.Tiploc AS Tiploc
		, l.LocationName AS LocationName
		,
		-- Channel Values
		[Col1],[Col2],[Col3],[Col4]
		--,[Col5],[Col6],[Col7],[Col8],[Col9],[Col10],
		--[Col11],[Col12],[Col13],[Col14],[Col15],[Col16],[Col17],[Col18],[Col19],[Col20],
		--[Col21],[Col22],[Col23],[Col24],[Col25],[Col26],[Col27],[Col28],[Col29],[Col30],
		--[Col31],[Col32],[Col33],[Col34],[Col35],[Col36],[Col37],[Col38],[Col39],[Col40],
		--[Col41],[Col42],[Col43],[Col44],[Col45],[Col46],[Col47],[Col48],[Col49],[Col50],
		--[Col51],[Col52],[Col53],[Col54],[Col55],[Col56],[Col57],[Col58],[Col59],[Col60],
		--[Col61],[Col62],[Col63],[Col64],[Col65],[Col66],[Col67],[Col68],[Col69],[Col70],
		--[Col71],[Col72],[Col73],[Col74],[Col75],[Col76],[Col77],[Col78],[Col79],[Col80],
		--[Col81],[Col82],[Col83],[Col84],[Col85],[Col86],[Col87],[Col88],[Col89],[Col90],
		--[Col91],[Col92],[Col93],[Col94],[Col95],[Col96],[Col97],[Col98],[Col99],[Col100],
		--	[Col101],[Col102],[Col103],[Col104],[Col105],[Col106],[Col107],[Col108],[Col109],[Col110],
		--	[Col111],[Col112],[Col113],[Col114],[Col115],[Col116],[Col117],[Col118],[Col119],[Col120],
		--	[Col121],[Col122],[Col123],[Col124],[Col125],[Col126],[Col127],[Col128],[Col129],[Col130],
		--	[Col131],[Col132],[Col133],[Col134],[Col135],[Col136],[Col137],[Col138],[Col139],[Col140],
		--	[Col141],[Col142],[Col143],[Col144],[Col145],[Col146],[Col147],[Col148],[Col149],[Col150],
		--	[Col151],[Col152],[Col153],[Col154],[Col155],[Col156],[Col157],[Col158],[Col159],[Col160],
		--	[Col161],[Col162],[Col163],[Col164],[Col165],[Col166],[Col167],[Col168],[Col169],[Col170],
		--	[Col171],[Col172],[Col173],[Col174],[Col175],[Col176],[Col177],[Col178],[Col179],[Col180],
		--	[Col181],[Col182],[Col183],[Col184],[Col185],[Col186],[Col187],[Col188],[Col189],[Col190],
		--	[Col191],[Col192],[Col193],[Col194],[Col195],[Col196],[Col197],[Col198],[Col199],[Col200],
		--[Col201],[Col202],[Col203],[Col204],[Col205],[Col206],[Col207],[Col208],[Col209],[Col210],
		--[Col211],[Col212],[Col213],[Col214],[Col215],[Col216],[Col217],[Col218],[Col219],[Col220],
		--[Col221],[Col222],[Col223],[Col224],[Col225],[Col226],[Col227],[Col228],[Col229],[Col230],
		--[Col231],[Col232],[Col233],[Col234],[Col235],[Col236],[Col237],[Col238],[Col239],[Col240],
		--[Col241],[Col242],[Col243],[Col244],[Col245],[Col246],[Col247],[Col248],[Col249],[Col250],
		--[Col251],[Col252],[Col253],[Col254],[Col255],[Col256],[Col257],[Col258],[Col259],[Col260],
		--[Col261],[Col262],[Col263],[Col264],[Col265],[Col266],[Col267],[Col268],[Col269],[Col270],
		--[Col271],[Col272],[Col273],[Col274],[Col275],[Col276],[Col277],[Col278],[Col279],[Col280],
		--[Col281],[Col282],[Col283],[Col284],[Col285],[Col286],[Col287],[Col288],[Col289],[Col290],
		--[Col291],[Col292],[Col293],[Col294],[Col295],[Col296],[Col297],[Col298],[Col299],[Col300],
		--	[Col301],[Col302],[Col303],[Col304],[Col305],[Col306],[Col307],[Col308],[Col309],[Col310],
		--	[Col311],[Col312],[Col313],[Col314],[Col315],[Col316],[Col317],[Col318],[Col319],[Col320],
		--	[Col321],[Col322],[Col323],[Col324],[Col325],[Col326],[Col327],[Col328],[Col329],[Col330],
		--	[Col331],[Col332],[Col333],[Col334],[Col335],[Col336],[Col337],[Col338],[Col339],[Col340],
		--	[Col341],[Col342],[Col343],[Col344],[Col345],[Col346],[Col347],[Col348],[Col349],[Col350],
		--	[Col351],[Col352],[Col353],[Col354],[Col355],[Col356],[Col357],[Col358],[Col359],[Col360],
		--	[Col361],[Col362],[Col363],[Col364],[Col365],[Col366],[Col367],[Col368],[Col369],[Col370],
		--	[Col371],[Col372],[Col373],[Col374],[Col375],[Col376],[Col377],[Col378],[Col379],[Col380],
		--	[Col381],[Col382],[Col383],[Col384],[Col385],[Col386],[Col387],[Col388],[Col389],[Col390],
		--	[Col391],[Col392],[Col393],[Col394],[Col395],[Col396],[Col397],[Col398],[Col399],[Col400],
		--[Col401],[Col402],[Col403],[Col404],[Col405],[Col406],[Col407],[Col408],[Col409],[Col410],
		--[Col411],[Col412],[Col413],[Col414],[Col415],[Col416],[Col417],[Col418],[Col419],[Col420],
		--[Col421],[Col422],[Col423],[Col424],[Col425],[Col426],[Col427],[Col428],[Col429],[Col430],
		--[Col431],[Col432],[Col433],[Col434],[Col435],[Col436],[Col437],[Col438],[Col439],[Col440],
		--[Col441],[Col442],[Col443],[Col444],[Col445],[Col446],[Col447],[Col448],[Col449],[Col450],
		--[Col451],[Col452],[Col453],[Col454],[Col455],[Col456],[Col457],[Col458],[Col459],[Col460],
		--[Col461],[Col462],[Col463],[Col464],[Col465],[Col466],[Col467],[Col468],[Col469],[Col470],
		--[Col471],[Col472],[Col473],[Col474],[Col475],[Col476],[Col477],[Col478],[Col479],[Col480],
		--[Col481],[Col482],[Col483],[Col484],[Col485],[Col486],[Col487],[Col488],[Col489],[Col490],
		--[Col491],[Col492],[Col493],[Col494],[Col495],[Col496],[Col497],[Col498],[Col499],[Col500],
		--	[Col501],[Col502],[Col503],[Col504],[Col505],[Col506],[Col507],[Col508],[Col509],[Col510],
		--	[Col511],[Col512],[Col513],[Col514],[Col515],[Col516],[Col517],[Col518],[Col519],[Col520],
		--	[Col521],[Col522],[Col523],[Col524],[Col525],[Col526],[Col527],[Col528],[Col529],[Col530],
		--	[Col531],[Col532],[Col533],[Col534],[Col535],[Col536],[Col537],[Col538],[Col539],[Col540],
		--	[Col541],[Col542],[Col543],[Col544],[Col545],[Col546],[Col547],[Col548],[Col549],[Col550],
		--	[Col551],[Col552],[Col553],[Col554],[Col555],[Col556],[Col557],[Col558],[Col559],[Col560],
		--	[Col561],[Col562],[Col563],[Col564],[Col565],[Col566],[Col567],[Col568],[Col569],[Col570],
		--	[Col571],[Col572],[Col573],[Col574],[Col575],[Col576],[Col577],[Col578],[Col579],[Col580],
		--	[Col581],[Col582],[Col583],[Col584],[Col585],[Col586],[Col587],[Col588],[Col589],[Col590],
		--	[Col591],[Col592],[Col593],[Col594],[Col595],[Col596],[Col597],[Col598],[Col599],[Col600],
		--[Col601],[Col602],[Col603],[Col604],[Col605],[Col606],[Col607],[Col608],[Col609],[Col610],
		--[Col611],[Col612],[Col613],[Col614],[Col615],[Col616],[Col617],[Col618],[Col619],[Col620],
		--[Col621],[Col622],[Col623],[Col624],[Col625],[Col626],[Col627],[Col628],[Col629],[Col630],
		--[Col631],[Col632],[Col633],[Col634],[Col635],[Col636],[Col637],[Col638],[Col639],[Col640],
		--[Col641],[Col642],[Col643],[Col644],[Col645],[Col646],[Col647],[Col648],[Col649],[Col650],
		--[Col651],[Col652],[Col653],[Col654],[Col655],[Col656],[Col657],[Col658],[Col659],[Col660],
		--[Col661],[Col662],[Col663]
	FROM dbo.ChannelValue cv WITH (NOLOCK)
	INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
	LEFT JOIN dbo.FleetStatus fs ON
		cv.TimeStamp > fs.ValidFrom 
		AND cv.TimeStamp <= ISNULL(fs.ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
		AND cv.UnitID = fs.UnitID
	LEFT JOIN dbo.Location l ON l.ID = (
										SELECT TOP 1 LocationID 
										FROM dbo.LocationArea 
										WHERE col1 BETWEEN MinLatitude AND MaxLatitude
											AND col2 BETWEEN MinLongitude AND MaxLongitude
										ORDER BY Priority
									)
	WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
		AND (@UnitID IS NULL OR cv.UnitID = @UnitID)
		AND (@UpdateRecord IS NULL OR cv.UpdateRecord = @UpdateRecord)
	ORDER BY [TimeStamp]

END




GO
RAISERROR ('-- update NSP_GetFaultChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelValueByID] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
	@ID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.ChannelValue), 0) - 1;
	ELSE
		SET @ID = (
			SELECT TOP 1 (ID - 1) 
			FROM dbo.ChannelValue WITH(NOLOCK)
			WHERE ID > @ID
			ORDER BY ID ASC
		)
		
	DECLARE @dataSetSize int = 5000
	
	SELECT 
		cv.ID
		, UnitID		= u.ID
		, UnitNumber	= u.UnitNumber
		, UnitType	 = u.UnitType
		, Headcode	 = fs.Headcode
		, FleetCode	 = fl.Code
		, ServiceStatus = CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, TimeStamp		 = CONVERT(datetime, cv.TimeStamp)
		, LocationID	= l.ID
		, Tiploc		= l.Tiploc
		, LocationName = l.LocationName ,

		-- Channel Values
		[Col1],[Col2],[Col3],[Col4]
		--,[Col5],[Col6],[Col7],[Col8],[Col9],[Col10],
		--[Col11],[Col12],[Col13],[Col14],[Col15],[Col16],[Col17],[Col18],[Col19],[Col20],
		--[Col21],[Col22],[Col23],[Col24],[Col25],[Col26],[Col27],[Col28],[Col29],[Col30],
		--[Col31],[Col32],[Col33],[Col34],[Col35],[Col36],[Col37],[Col38],[Col39],[Col40],
		--[Col41],[Col42],[Col43],[Col44],[Col45],[Col46],[Col47],[Col48],[Col49],[Col50],
		--[Col51],[Col52],[Col53],[Col54],[Col55],[Col56],[Col57],[Col58],[Col59],[Col60],
		--[Col61],[Col62],[Col63],[Col64],[Col65],[Col66],[Col67],[Col68],[Col69],[Col70],
		--[Col71],[Col72],[Col73],[Col74],[Col75],[Col76],[Col77],[Col78],[Col79],[Col80],
		--[Col81],[Col82],[Col83],[Col84],[Col85],[Col86],[Col87],[Col88],[Col89],[Col90],
		--[Col91],[Col92],[Col93],[Col94],[Col95],[Col96],[Col97],[Col98],[Col99],[Col100],
		--	[Col101],[Col102],[Col103],[Col104],[Col105],[Col106],[Col107],[Col108],[Col109],[Col110],
		--	[Col111],[Col112],[Col113],[Col114],[Col115],[Col116],[Col117],[Col118],[Col119],[Col120],
		--	[Col121],[Col122],[Col123],[Col124],[Col125],[Col126],[Col127],[Col128],[Col129],[Col130],
		--	[Col131],[Col132],[Col133],[Col134],[Col135],[Col136],[Col137],[Col138],[Col139],[Col140],
		--	[Col141],[Col142],[Col143],[Col144],[Col145],[Col146],[Col147],[Col148],[Col149],[Col150],
		--	[Col151],[Col152],[Col153],[Col154],[Col155],[Col156],[Col157],[Col158],[Col159],[Col160],
		--	[Col161],[Col162],[Col163],[Col164],[Col165],[Col166],[Col167],[Col168],[Col169],[Col170],
		--	[Col171],[Col172],[Col173],[Col174],[Col175],[Col176],[Col177],[Col178],[Col179],[Col180],
		--	[Col181],[Col182],[Col183],[Col184],[Col185],[Col186],[Col187],[Col188],[Col189],[Col190],
		--	[Col191],[Col192],[Col193],[Col194],[Col195],[Col196],[Col197],[Col198],[Col199],[Col200],
		--[Col201],[Col202],[Col203],[Col204],[Col205],[Col206],[Col207],[Col208],[Col209],[Col210],
		--[Col211],[Col212],[Col213],[Col214],[Col215],[Col216],[Col217],[Col218],[Col219],[Col220],
		--[Col221],[Col222],[Col223],[Col224],[Col225],[Col226],[Col227],[Col228],[Col229],[Col230],
		--[Col231],[Col232],[Col233],[Col234],[Col235],[Col236],[Col237],[Col238],[Col239],[Col240],
		--[Col241],[Col242],[Col243],[Col244],[Col245],[Col246],[Col247],[Col248],[Col249],[Col250],
		--[Col251],[Col252],[Col253],[Col254],[Col255],[Col256],[Col257],[Col258],[Col259],[Col260],
		--[Col261],[Col262],[Col263],[Col264],[Col265],[Col266],[Col267],[Col268],[Col269],[Col270],
		--[Col271],[Col272],[Col273],[Col274],[Col275],[Col276],[Col277],[Col278],[Col279],[Col280],
		--[Col281],[Col282],[Col283],[Col284],[Col285],[Col286],[Col287],[Col288],[Col289],[Col290],
		--[Col291],[Col292],[Col293],[Col294],[Col295],[Col296],[Col297],[Col298],[Col299],[Col300],
		--	[Col301],[Col302],[Col303],[Col304],[Col305],[Col306],[Col307],[Col308],[Col309],[Col310],
		--	[Col311],[Col312],[Col313],[Col314],[Col315],[Col316],[Col317],[Col318],[Col319],[Col320],
		--	[Col321],[Col322],[Col323],[Col324],[Col325],[Col326],[Col327],[Col328],[Col329],[Col330],
		--	[Col331],[Col332],[Col333],[Col334],[Col335],[Col336],[Col337],[Col338],[Col339],[Col340],
		--	[Col341],[Col342],[Col343],[Col344],[Col345],[Col346],[Col347],[Col348],[Col349],[Col350],
		--	[Col351],[Col352],[Col353],[Col354],[Col355],[Col356],[Col357],[Col358],[Col359],[Col360],
		--	[Col361],[Col362],[Col363],[Col364],[Col365],[Col366],[Col367],[Col368],[Col369],[Col370],
		--	[Col371],[Col372],[Col373],[Col374],[Col375],[Col376],[Col377],[Col378],[Col379],[Col380],
		--	[Col381],[Col382],[Col383],[Col384],[Col385],[Col386],[Col387],[Col388],[Col389],[Col390],
		--	[Col391],[Col392],[Col393],[Col394],[Col395],[Col396],[Col397],[Col398],[Col399],[Col400],
		--[Col401],[Col402],[Col403],[Col404],[Col405],[Col406],[Col407],[Col408],[Col409],[Col410],
		--[Col411],[Col412],[Col413],[Col414],[Col415],[Col416],[Col417],[Col418],[Col419],[Col420],
		--[Col421],[Col422],[Col423],[Col424],[Col425],[Col426],[Col427],[Col428],[Col429],[Col430],
		--[Col431],[Col432],[Col433],[Col434],[Col435],[Col436],[Col437],[Col438],[Col439],[Col440],
		--[Col441],[Col442],[Col443],[Col444],[Col445],[Col446],[Col447],[Col448],[Col449],[Col450],
		--[Col451],[Col452],[Col453],[Col454],[Col455],[Col456],[Col457],[Col458],[Col459],[Col460],
		--[Col461],[Col462],[Col463],[Col464],[Col465],[Col466],[Col467],[Col468],[Col469],[Col470],
		--[Col471],[Col472],[Col473],[Col474],[Col475],[Col476],[Col477],[Col478],[Col479],[Col480],
		--[Col481],[Col482],[Col483],[Col484],[Col485],[Col486],[Col487],[Col488],[Col489],[Col490],
		--[Col491],[Col492],[Col493],[Col494],[Col495],[Col496],[Col497],[Col498],[Col499],[Col500],
		--	[Col501],[Col502],[Col503],[Col504],[Col505],[Col506],[Col507],[Col508],[Col509],[Col510],
		--	[Col511],[Col512],[Col513],[Col514],[Col515],[Col516],[Col517],[Col518],[Col519],[Col520],
		--	[Col521],[Col522],[Col523],[Col524],[Col525],[Col526],[Col527],[Col528],[Col529],[Col530],
		--	[Col531],[Col532],[Col533],[Col534],[Col535],[Col536],[Col537],[Col538],[Col539],[Col540],
		--	[Col541],[Col542],[Col543],[Col544],[Col545],[Col546],[Col547],[Col548],[Col549],[Col550],
		--	[Col551],[Col552],[Col553],[Col554],[Col555],[Col556],[Col557],[Col558],[Col559],[Col560],
		--	[Col561],[Col562],[Col563],[Col564],[Col565],[Col566],[Col567],[Col568],[Col569],[Col570],
		--	[Col571],[Col572],[Col573],[Col574],[Col575],[Col576],[Col577],[Col578],[Col579],[Col580],
		--	[Col581],[Col582],[Col583],[Col584],[Col585],[Col586],[Col587],[Col588],[Col589],[Col590],
		--	[Col591],[Col592],[Col593],[Col594],[Col595],[Col596],[Col597],[Col598],[Col599],[Col600],
		--[Col601],[Col602],[Col603],[Col604],[Col605],[Col606],[Col607],[Col608],[Col609],[Col610],
		--[Col611],[Col612],[Col613],[Col614],[Col615],[Col616],[Col617],[Col618],[Col619],[Col620],
		--[Col621],[Col622],[Col623],[Col624],[Col625],[Col626],[Col627],[Col628],[Col629],[Col630],
		--[Col631],[Col632],[Col633],[Col634],[Col635],[Col636],[Col637],[Col638],[Col639],[Col640],
		--[Col641],[Col642],[Col643],[Col644],[Col645],[Col646],[Col647],[Col648],[Col649],[Col650],
		--[Col651],[Col652],[Col653],[Col654],[Col655],[Col656],[Col657],[Col658],[Col659],[Col660],
		--[Col661],[Col662],[Col663]

	FROM dbo.ChannelValue cv
	INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
	LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
	LEFT JOIN dbo.FleetStatusHistory fs ON
		cv.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
	LEFT JOIN dbo.Location l ON l.ID = (
		SELECT TOP 1 LocationID 
		FROM dbo.LocationArea 
		WHERE col3 BETWEEN MinLatitude AND MaxLatitude
			AND col4 BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
	ORDER BY ID

END

GO
RAISERROR ('-- update NSP_GetFaultCount', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultCount] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultCount
**	Description:	Returns fault count after specific date
**	Call frequency:	Every 5s 
**	Parameters:		@timestamp
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultCount](
	@timestamp datetime
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(*) AS FaultCount
	FROM dbo.VW_IX_Fault (NOEXPAND)
	WHERE CreateTime >= @timestamp
		AND ReportingOnly = 0

END

GO
RAISERROR ('-- update NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultDetail] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_GetFaultDetail
** Description:	Returns details of the Fault
** Call frequency: Every 5s 
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
	@FaultID int
)
AS
BEGIN

	SET NOCOUNT ON;
	SELECT
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude	
		, f.FaultMetaID	
		--, UnitNumber	
		, f.FleetCode 
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.IsAcknowledged
		, '' as TransmissionStatus
		, fme.E2MFaultCode
		, fme.E2MDescription
		, fme.E2MPriorityCode
		, fme.E2MPriority
		, fm.Url
		, f.HasRecovery
	FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
	LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
	WHERE f.ID = @FaultID

END


GO
RAISERROR ('-- update NSP_GetFaultEventChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelData] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_GetFaultEventChannelData
** Description:	Returns event based channel data for event detail.
**				 Used together with NSP_GetFaultChannelData
** Call frequency: Called by event detail service
** Parameters:	 @FaultID int
**				 @UnitID int
** Return values: FaultEventChannelValue.UnitID
**				 FaultEventChannelValue.TimeStamp
**				 FaultEventChannelValue.ChannelID
**				 FaultEventChannelValue.Value
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelData]
	@FaultID int
	,@UnitID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UnitID
		,TimeStamp
		,ChannelID
		,Value
	FROM dbo.FaultEventChannelValue
	WHERE FaultID = @FaultID AND UnitID = @UnitID
END

GO
/****** Object: StoredProcedure [dbo].[NSP_GetFaultEventChannelValue]	Script Date: 03/02/2017 12:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValue]
	@DatetimeStart DATETIME
	,@DatetimeEnd DATETIME
	,@UnitId INT = NULL
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT ecv.ID
	,ecv.UnitID
	,ecv.ChannelID
	,ecv.TimeStamp
	,ecv.Value
	FROM dbo.EventChannelValue ecv
	WHERE ecv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
	AND (@UnitId IS NULL OR ecv.UnitID = @UnitId)
END


GO
RAISERROR ('-- update NSP_GetFaultEventChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueByID] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueByID]
	@ID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.EventChannelValue), 0) - 1
	ELSE
		SET @ID = (
			SELECT TOP 1 (EventChannelValue.ID - 1) 
			FROM dbo.EventChannelValue WITH(NOLOCK)
			WHERE EventChannelValue.ID > @ID
			ORDER BY EventChannelValue.ID ASC
		)

	SELECT ecv.ID
		,ecv.UnitID
		,ecv.ChannelID
		,ecv.TimeStamp
		,ecv.Value
	FROM dbo.EventChannelValue ecv
	WHERE ecv.ID BETWEEN (@ID + 1) AND (@ID + 10000)
END


GO
RAISERROR ('-- update NSP_GetFaultEventChannelValueLatest', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueLatest] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatest]
	@DatetimeStart DATETIME
	,@UnitID INT = NULL
AS
BEGIN	
	SET NOCOUNT ON;

	WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.UnitID
			,ecv.ChannelID
		FROM dbo.EventChannelValue ecv
		WHERE ecv.TimeStamp <= @DatetimeStart
			AND ecv.TimeStamp >= DATEADD(DAY, - 1, @DatetimeStart)
			AND ecv.UnitID = @UnitID OR @UnitID IS NULL
		GROUP BY ecv.UnitID
			,ecv.ChannelID
		)
	SELECT ecv.ID
	,ecv.UnitID
	,ecv.ChannelID
	,ecv.TimeStamp
	,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN dbo.EventChannelValue ecv
		ON ecvl.UnitID = ecv.UnitID
		AND ecvl.ChannelID = ecv.ChannelID
		AND ecvl.TimeStamp = ecv.TimeStamp
		
END


GO
RAISERROR ('-- update NSP_GetFaultEventChannelValueLatestByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueLatestByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueLatestByID] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatestByID]
	@ID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.EventChannelValue), 0) - 1
	ELSE
		SET @ID = (
			SELECT TOP 1 (EventChannelValue.ID - 1) 
			FROM dbo.EventChannelValue WITH(NOLOCK)
			WHERE EventChannelValue.ID > @ID
			ORDER BY EventChannelValue.ID ASC
		)
	
	
	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.UnitID
			,ecv.ChannelID
		FROM dbo.EventChannelValue ecv
		WHERE ecv.ID <= @ID
			AND ecv.TimeStamp >= DATEADD(DAY, - 1, SYSDATETIME())
		GROUP BY ecv.UnitID
			,ecv.ChannelID
	)
	SELECT ecv.ID
	,ecv.UnitID
	,ecv.ChannelID
	,ecv.TimeStamp
	,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN dbo.EventChannelValue ecv
		ON ecvl.UnitID = ecv.UnitID
		AND ecvl.ChannelID = ecv.ChannelID
		AND ecvl.TimeStamp = ecv.TimeStamp
END


GO
RAISERROR ('-- update NSP_GetFaultEventChannelValueLatestSince', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueLatestSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueLatestSince] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatestSince]
	@ID int,
	@MaxTimeDifferenceMinutes int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TimestampBelongingToNextRecord datetime2 = NULL

	SELECT @TimestampBelongingToNextRecord = Timestamp
	FROM [dbo].[EventChannelValue]
	WHERE ID = (
			SELECT MIN(ID) FROM [dbo].[EventChannelValue] WHERE ID > @ID
	)

	SELECT 
		t1.ID, 
		t1.UnitID, 
		t1.ChannelID, 
		t1.TimeStamp, 
		t1.Value
	FROM
		[dbo].[EventChannelValue] t1 
		INNER JOIN (
			SELECT 
				Max(ID) ID, 
				UnitID, 
				ChannelID
			FROM [dbo].[EventChannelValue] 
			WHERE 
				@TimestampBelongingToNextRecord IS NOT NULL
				AND ID <= @ID 
				AND DATEADD(mi, -@MaxTimeDifferenceMinutes, @TimestampBelongingToNextRecord) <= Timestamp
			GROUP BY 
				UnitID, ChannelID
		) AS t2 
		ON 
			t1.UnitID = t2.UnitID
			AND t1.ChannelID = t2.ChannelID
			AND t1.ID = t2.ID
	WHERE 
		@TimestampBelongingToNextRecord IS NOT NULL
		AND t1.ID <= @ID
		AND DATEADD(mi, -@MaxTimeDifferenceMinutes, @TimestampBelongingToNextRecord) <= t1.Timestamp
END


GO
RAISERROR ('-- update NSP_GetFaultFormation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultFormation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultFormation] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_GetFaultFormation
**	Description:	Returns datails of the Fault
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultFormation]
(
	@FaultID int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @primaryUnitID int
	DECLARE @secondaryUnitID int
	DECLARE @tertiaryUnitID int
	DECLARE @faultUnitID int
	
	SELECT 
		@primaryUnitID		= PrimaryUnitID
		, @secondaryUnitID	= SecondaryUnitID
		, @tertiaryUnitID	= TertiaryUnitID 
		, @faultUnitID		= FaultUnitID
	FROM dbo.Fault
	WHERE ID = @FaultID
	
	;WITH CteUnit AS
	(
		SELECT 
			UnitID		= @primaryUnitID
			, UnitOrder	= 1
		UNION 
		SELECT 
			UnitID		= @secondaryUnitID
			, UnitOrder	= 2
		UNION 
		SELECT 
			UnitID		= @tertiaryUnitID
			, UnitOrder	= 3
		
	)
	SELECT 
		UnitID			= u.ID 
		, UnitNumber
		, UnitOrder
		, IsFaultUnit	= CASE u.ID
			WHEN @faultUnitID THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END
	FROM CteUnit
	INNER JOIN dbo.Unit u ON u.ID = CteUnit.UnitID
	
END



GO
RAISERROR ('-- update NSP_GetFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLive] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_GetFaultLive
**	Description:	Returns list of all Current Faults
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
	@N int --Limit
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
AS

	SET NOCOUNT ON;
	
	SELECT TOP (@N)
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber	 = up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
		AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
		AND f.ReportingOnly = 0
	ORDER BY f.CreateTime DESC

GO
RAISERROR ('-- update NSP_GetFaultLiveCount', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLiveCount] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_GetFaultLiveCount
**	Description:	Returns live fault count
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveCount]
AS
BEGIN
	SET NOCOUNT ON

	SELECT COUNT(*) AS FaultCount
	FROM dbo.VW_IX_FaultLive (NOEXPAND)
	WHERE ReportingOnly = 0

END


GO
/****** Object: StoredProcedure [dbo].[NSP_GetFaultLiveUnit]	Script Date: 03/02/2017 12:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
**	Name:			NSP_GetFaultLiveUnit
**	Description:	Returns list of all Current Faults for a unit
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
	@UnitID int
)
AS

	SET NOCOUNT ON;

	SELECT
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultTypeID 
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultUnitID = @UnitID
		AND f.ReportingOnly = 0
	ORDER BY f.CreateTime DESC


GO
RAISERROR ('-- update NSP_GetFaultLiveUnitAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLiveUnitAcknowledged] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitAcknowledged](
	@UnitID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultTypeID 
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultUnitID = @UnitID
		AND f.ReportingOnly = 0
		AND f.IsAcknowledged = 1
	ORDER BY f.CreateTime DESC
	
END

GO
RAISERROR ('-- update NSP_GetFaultLiveUnitNotAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitNotAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultLiveUnitNotAcknowledged] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitNotAcknowledged](
	@UnitID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultTypeID 
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultUnitID = @UnitID
		AND f.ReportingOnly = 0
		AND f.IsAcknowledged = 0
	ORDER BY f.CreateTime DESC
	
END



GO
RAISERROR ('-- update NSP_GetFaultNotLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultNotLive] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultNotLive
**	Description:	Returns list of N recent Faults
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
	@N int
	, @FaultTypeID int	= NULL	--Severity
	, @CategoryID int	= NULL	--Category
)
AS
	SET NOCOUNT ON;
	
 	SELECT TOP (@N)
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
		AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
		AND ReportingOnly = 0
	ORDER BY CreateTime DESC


GO
RAISERROR ('-- update NSP_GetFaultNotLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultNotLiveUnit] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
**	Name:			NSP_GetFaultNotLiveUnit
**	Description:	Returns list of N recent Faults for set
**	Call frequency:	Every 5s 
**	Parameters:		@N - number of records to return
**					@UnitID
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
	@N int
	, @UnitID int
)
AS
	SET NOCOUNT ON;

	SELECT TOP (@N)
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary	
		, f.FaultTypeID 
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
		, f.HasRecovery
	FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.FaultUnitID = @UnitID
		AND f.ReportingOnly = 0
	ORDER BY f.CreateTime DESC

GO
RAISERROR ('-- update NSP_GetFaultRetractedFlag', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultRetractedFlag' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultRetractedFlag] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
**	Name:			NSP_GetFaultRetractedFlag
**	Description:	Returns 1 if there was a fault retracted since the timestamp
**					passed as parameter, otherwise 0
**	Call frequency:	Every 5s for each user
**	Parameters:		@timestamp
**	Return values:	0 if successful, else an error is raised
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultRetractedFlag](
	@timestamp datetime
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(
		SELECT top 1 1
		FROM dbo.VW_IX_Fault (NOEXPAND)
		WHERE (EndTime >= @timestamp)
	)
		SELECT 1 AS Flag
	ELSE
		SELECT 0 AS Flag
	
END

GO
RAISERROR ('-- update NSP_GetFaultSince', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultSince] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
**	Name:			NSP_GetFaultSince
**	Description:	Returns list of all Faults created after the specified time argument
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultSince]
	@since datetime
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		f.ID	
		, f.CreateTime	
		, f.HeadCode 
		, f.SetCode	
		, f.FaultCode	
		, l.LocationCode 
		, f.IsCurrent 
		, f.EndTime	
		, f.RecoveryID	
		, f.Latitude 
		, f.Longitude 
		, f.FaultMetaID	
		, f.FaultUnitID	
		, f.FaultUnitNumber
		, PrimaryUnitNumber		= up.UnitNumber
		, SecondaryUnitNumber	= us.UnitNumber
		, TertiaryUnitNumber	= ut.UnitNumber
		, f.IsDelayed 
		, f.Description	
		, f.Summary 
		, f.FaultType
		, f.FaultTypeColor
		, f.Category
		, f.CategoryID
		, f.ReportingOnly
		, f.RecoveryStatus
		, f.FleetCode
	FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
	LEFT JOIN dbo.Location l ON f.LocationID = l.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE f.CreateTime > @since
		AND f.ReportingOnly = 0
	ORDER BY f.CreateTime DESC

END

GO
RAISERROR ('-- update NSP_GetFaultTmsData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultTmsData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultTmsData] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_GetFaultTmsData
**	Description:	Returns TMS data for the FaultID
**	Call frequency:	Once for each fault detail request
**	Parameters:		
**	Return values:	0 if successful, else an error is raised:
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultTmsData]
	@FaultID int
AS
BEGIN

	SET NOCOUNT ON;
		
	SELECT
		tf.VehicleNumber
		, tf.FaultDateTime
		, tf.CarNo
		, tf.FaultNo
		, tfl.Description
		, tfl.SwRef
		, tfc.Value	AS CategoryName
		, tff.Value	AS FunctionName
		, cablookup.Value AS Cab
		, mcslookup.Value AS MCS
		, dirlookup.Value AS Direction
		, vltlookup.Value AS Voltage
		, spdlookup.Value AS SpeedSet
		, bctlookup.Value AS BrkCont
		, ebklookup.Value AS EmmerBrk
		, stdlookup.Value AS Stabled
		, mlplookup.Value AS MLP
		, bprlookup.Value AS BrkPr
		, batlookup.Value AS Battery
		, ldslookup.Value AS Loadshed
		, tshlookup.Value AS TracShoreSup
		, ashlookup.Value AS AuxShoreSup
		, bmclookup.Value AS CarBMCoupled
		, bjclookup.Value AS CarBJCoupled
		, tmclookup.Value AS TMCC1
		, tf.Speed
	FROM dbo.TmsFault tf
	LEFT JOIN dbo.TmsFaultLookup tfl ON tf.FaultNo = tfl.FaultNo
	LEFT JOIN dbo.TmsLookup AS tfc ON tfl.TmsCategoryID = tfc.ValueID AND tfc.Type = 'tmsFaultCategory'
	LEFT JOIN dbo.TmsLookup AS tff ON tfl.TmsFunctionID = tff.ValueID AND tff.Type = 'tmsFaultFunction'
	LEFT JOIN dbo.TmsLookup AS cablookup ON tf.CapId = cablookup.ValueId AND cablookup.Type = 'cab'
	LEFT JOIN dbo.TmsLookup AS mcslookup ON tf.McsId = mcslookup.ValueId AND mcslookup.Type = 'mcs'
	LEFT JOIN dbo.TmsLookup AS dirlookup ON tf.DirectionId = dirlookup.ValueId AND dirlookup.Type = 'direction'
	LEFT JOIN dbo.TmsLookup AS vltlookup ON tf.VoltageId = vltlookup.ValueId AND vltlookup.Type = 'voltage'
	LEFT JOIN dbo.TmsLookup AS spdlookup ON tf.SpeedSetId = spdlookup.ValueId AND spdlookup.Type = 'speedSet'
	LEFT JOIN dbo.TmsLookup AS bctlookup ON tf.BrakeContId = bctlookup.ValueId AND bctlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS ebklookup ON tf.EmerBrakeId = ebklookup.ValueId AND ebklookup.Type = 'emergencyBrake'
	LEFT JOIN dbo.TmsLookup AS stdlookup ON tf.StabledId = stdlookup.ValueId AND stdlookup.Type = 'stabled'
	LEFT JOIN dbo.TmsLookup AS mlplookup ON tf.MlpId = mlplookup.ValueId AND mlplookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS bprlookup ON tf.BrakePrId = bprlookup.ValueId AND bprlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS batlookup ON tf.BatteryId = batlookup.ValueId AND batlookup.Type = 'battery'
	LEFT JOIN dbo.TmsLookup AS ldslookup ON tf.LoadshedId = ldslookup.ValueId AND ldslookup.Type = 'loadshed'
	LEFT JOIN dbo.TmsLookup AS tshlookup ON tf.TracShoreSupId = tshlookup.ValueId AND tshlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS ashlookup ON tf.AuxShoreSupId = ashlookup.ValueId AND ashlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS bmclookup ON tf.CarBmCoupledId = bmclookup.ValueId AND bmclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS bjclookup ON tf.CarBjCoupledId = bjclookup.ValueId AND bjclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS tmclookup ON tf.Tmcc1Id = tmclookup.ValueId AND tmclookup.Type = 'tmcc1'
	WHERE tf.FaultID = @FaultID
	
END


GO
RAISERROR ('-- update NSP_GetLastChannelDataForVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetLastChannelDataForVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetLastChannelDataForVehicle] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
** Name:			NSP_GetLastChannelDataForVehicle
** Description:	Returns latest recod for the vehicles for VehicleID in
**				 paramter. Stored procedure searches for latest record in
**				 last 24h before timestamp passed as paramter 
** Call frequency: Every 5s from Cabview screen 
** Parameters:	 @DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**				 to return last live record for unit
**				 @VehicleID int 
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetLastChannelDataForVehicle]
	@DateTimeStart datetime
	, @VehicleID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @setcode varchar(50)
	DECLARE @headcode varchar(50)
	
	IF @DateTimeStart IS NULL
		SET @DateTimeStart = DATEADD(hh, 1, GETDATE())
				
	;WITH CteLatestRecords 
	AS
	(
		SELECT 
			VehicleID	= Vehicle.ID
			, UnitID	= Vehicle.UnitID
			, LastTimeStamp = (
				SELECT TOP 1 TimeStamp
				FROM dbo.ChannelValue
				WHERE TimeStamp BETWEEN DATEADD(d, -7, @DateTimeStart) AND @DateTimeStart
					AND ChannelValue.UnitID = Vehicle.UnitID 
				ORDER BY TimeStamp DESC
			)
		FROM dbo.VW_Vehicle AS Vehicle
		WHERE Vehicle.ID = @VehicleID
	)	

	SELECT 
		UnitNumber
		, Headcode	= CONVERT(varchar(10), NULL)
		, Diagram 	= CONVERT(varchar(10), NULL)
		, Vehicle.ID AS VehicleID
		, Vehicle.Type AS VehicleType
		, Vehicle.VehicleTypeID AS VehicleTypeID
		, Vehicle.VehicleNumber AS VehicleNumber
		, (
			SELECT TOP 1 
				LocationCode -- CRS
			FROM dbo.Location
			INNER JOIN dbo.LocationArea ON LocationID = Location.ID
			WHERE col3 BETWEEN MinLatitude AND MaxLatitude
				AND col4 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) AS Location
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp	 

		,[Col1], [Col2], [Col3], [Col4]
		--, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM dbo.VW_Vehicle Vehicle 
	INNER JOIN CteLatestRecords	 ON Vehicle.UnitID = CteLatestRecords.UnitID
	LEFT JOIN dbo.ChannelValue cv		ON Vehicle.UnitID = cv.UnitID AND CteLatestRecords.LastTimeStamp = cv.TimeStamp

			
END

GO
RAISERROR ('-- update NSP_GetOrInsertLocationIdForTiploc', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetOrInsertLocationIdForTiploc' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetOrInsertLocationIdForTiploc] AS BEGIN RETURN(1) END;')
GO

-- =============================================
-- Author:		Radek Toma
-- Create date: 2011-07-31
-- Description:	Stored procedure returns Location.ID for Tiploc. 
--	If Tiploc doesnt exist, new record is created.
-- =============================================
ALTER PROCEDURE [dbo].[NSP_GetOrInsertLocationIdForTiploc]
	@Tiploc varchar(20)
	, @LocationID int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @LocationID = (SELECT TOP 1 ID FROM dbo.Location WHERE Tiploc = @tiploc)
	 					
	IF @LocationID IS NULL --insert tiploc if not exists in database
	BEGIN
		INSERT INTO dbo.Location (LocationCode, LocationName, Tiploc, Type)
		SELECT @tiploc, @tiploc, @tiploc, 'U'

		SET @locationID = @@IDENTITY
	END

END

GO
RAISERROR ('-- update NSP_GetStockSiblings', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetStockSiblings' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetStockSiblings] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_GetStockSiblings
**	Description:	Given a Vehicle ID, returns Vehicle IDs with the same
**					parent (Unit)
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetStockSiblings]
(
	@VehicleID int
)
AS
BEGIN
	SELECT 
		ID			= Vehicle.ID 
		, StockNumber		= VehicleNumber	
		, StockOrder		= VehicleOrder
	FROM dbo.Vehicle
	WHERE UnitID = (
		SELECT UnitID
		FROM dbo.Vehicle
		WHERE ID = @VehicleID
	)
END

GO
RAISERROR ('-- update NSP_HistoricChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricChannelData] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelData]
	@DateTimeEnd datetime
	, @Interval int -- number of seconds
	, @UnitID int
	, @SelectColList varchar(4000)
	, @N int = NULL --record number
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql varchar(max)

	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)
	
	SET @sql = '
SELECT TOP (' + CAST(ISNULL(@N, 30000) AS varchar(10)) + ')
	TimeStamp = CAST(cv.Timestamp AS datetime)
	, UnitID = ' + CAST(@UnitID AS varchar(10)) + '
	, ' + @SelectColList + '
FROM dbo.ChannelValue cv
WHERE 
	cv.UnitID = ' + CAST(@UnitID AS varchar(10)) + '
	AND cv.Timestamp BETWEEN ''' + CONVERT(varchar(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(varchar(23), @DateTimeEnd, 121) + '''
ORDER BY 1 ASC'

	EXEC (@sql)
	
END


GO
RAISERROR ('-- update NSP_HistoricChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricChannelValue] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_HistoricChannelValue
** Description:	Returns latest record for each of the vehicles for UnitID
**				 in parameter. Stored procedure searches for last record in 
**				 last 24h before timestamp passed as parameter 
** Call frequency: Every 5s 
** Parameters:	 @DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**					 to return last live record for unit
**				 @UnitID int 
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricChannelValue]
	@DateTimeStart datetime
	, @UnitID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @setcode varchar(50)
	DECLARE @headcode varchar(50)
	
	IF @UnitID IS NULL
	BEGIN
		PRINT 'Unit Number not in a database'
		RETURN -1 
	END


	IF @DateTimeStart IS NULL
		SET @DateTimeStart = DATEADD(hh, 1, GETDATE())

	-- Get totals for Vehicle based faults
	DECLARE @currentVehicleFault TABLE(
		FaultUnitID int NULL
		, FaultNumber int NULL
		, WarningNumber int NULL
	)
		
	INSERT INTO @currentVehicleFault
	SELECT 
		FaultUnitID
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
	FROM dbo.VW_IX_FaultLive WITH (NOEXPAND)
	WHERE FaultUnitID = @UnitID
		AND ReportingOnly = 0
	GROUP BY FaultUnitID

	;WITH CteLatestRecords 
	AS
	(
		SELECT 
			Vehicle.ID AS VehicleID
			, (
				SELECT TOP 1 TimeStamp
				FROM dbo.ChannelValue
				WHERE UnitID =	Vehicle.UnitID /*TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
					AND UnitID =	Vehicle.UnitID */
				ORDER BY TimeStamp DESC
			) AS MaxChannelValueTimeStamp
		FROM dbo.Vehicle
		WHERE UnitID = @UnitID
	)	

	SELECT 
		UnitNumber = v.UnitNumber
		, UnitID	= v.UnitID
		, Headcode = fs.Headcode
		, Diagram	= fs.Diagram
		, VehicleID	 = v.ID
		, VehicleType	= v.Type
		, VehicleTypeID = v.VehicleTypeID
		, VehicleNumber = v.VehicleNumber
		, FaultNumber	= ISNULL(vf.FaultNumber,0)
		, WarningNumber = ISNULL(vf.WarningNumber,0)
		, Location	 = (
			SELECT TOP 1 
				LocationCode -- CRS
			FROM dbo.Location
			INNER JOIN dbo.LocationArea ON LocationID = Location.ID
			WHERE col3 BETWEEN MinLatitude AND MaxLatitude
				AND col4 BETWEEN MinLongitude AND MaxLongitude
				AND LocationArea.Type = 'C'
			ORDER BY Priority
		)
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		,fs.FleetCode
		,TimeStamp	 = CAST(cv.[TimeStamp] AS datetime),

		-- Channel Data 
			[Col1], [Col2], [Col3], [Col4]
			--, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			--[Col400],
			--[Col401],
			--[Col402],
			--[Col403],
			--[Col404],
			--[Col405],
			--[Col406],
			--[Col407],
			--[Col408],
			--[Col409],
			--[Col410],
			--[Col411],
			--[Col412],
			--[Col413],
			--[Col414],
			--[Col415],
			--[Col416],
			--[Col417],
			--[Col418],
			--[Col419],
			--[Col420],
			--[Col421],
			--[Col422],
			--[Col423],
			--[Col424],
			--[Col425],
			--[Col426],
			--[Col427],
			--[Col428],
			--[Col429],
			--[Col430],
			--[Col431],
			--[Col432],
			--[Col433],
			--[Col434],
			--[Col435],
			--[Col436],
			--[Col437],
			--[Col438],
			--[Col439],
			--[Col440],
			--[Col441],
			--[Col442],
			--[Col443],
			--[Col444],
			--[Col445],
			--[Col446],
			--[Col447],
			--[Col448],
			--[Col449],
			--[Col450],
			--[Col451],
			--[Col452],
			--[Col453],
			--[Col454],
			--[Col455],
			--[Col456],
			--[Col457],
			--[Col458],
			--[Col459],
			--[Col460],
			--[Col461],
			--[Col462],
			--[Col463],
			--[Col464],
			--[Col465],
			--[Col466],
			--[Col467],
			--[Col468],
			--[Col469],
			--[Col470],
			--[Col471],
			--[Col472],
			--[Col473],
			--[Col474],
			--[Col475],
			--[Col476],
			--[Col477],
			--[Col478],
			--[Col479],
			--[Col480],
			--[Col481],
			--[Col482],
			--[Col483],
			--[Col484],
			--[Col485],
			--[Col486],
			--[Col487],
			--[Col488],
			--[Col489],
			--[Col490],
			--[Col491],
			--[Col492],
			--[Col493],
			--[Col494],
			--[Col495],
			--[Col496],
			--[Col497],
			--[Col498],
			--[Col499],
			--[Col500],
			--[Col501],
			--[Col502],
			--[Col503],
			--[Col504],
			--[Col505],
			--[Col506],
			--[Col507],
			--[Col508],
			--[Col509],
			--[Col510],
			--[Col511],
			--[Col512],
			--[Col513],
			--[Col514],
			--[Col515],
			--[Col516],
			--[Col517],
			--[Col518],
			--[Col519],
			--[Col520],
			--[Col521],
			--[Col522],
			--[Col523],
			--[Col524],
			--[Col525],
			--[Col526],
			--[Col527],
			--[Col528],
			--[Col529],
			--[Col530],
			--[Col531],
			--[Col532],
			--[Col533],
			--[Col534],
			--[Col535],
			--[Col536],
			--[Col537],
			--[Col538],
			--[Col539],
			--[Col540],
			--[Col541],
			--[Col542],
			--[Col543],
			--[Col544],
			--[Col545],
			--[Col546],
			--[Col547],
			--[Col548],
			--[Col549],
			--[Col550],
			--[Col551],
			--[Col552],
			--[Col553],
			--[Col554],
			--[Col555],
			--[Col556],
			--[Col557],
			--[Col558],
			--[Col559],
			--[Col560],
			--[Col561],
			--[Col562],
			--[Col563],
			--[Col564],
			--[Col565],
			--[Col566],
			--[Col567],
			--[Col568],
			--[Col569],
			--[Col570],
			--[Col571],
			--[Col572],
			--[Col573],
			--[Col574],
			--[Col575],
			--[Col576],
			--[Col577],
			--[Col578],
			--[Col579],
			--[Col580],
			--[Col581],
			--[Col582],
			--[Col583],
			--[Col584],
			--[Col585],
			--[Col586],
			--[Col587],
			--[Col588],
			--[Col589],
			--[Col590],
			--[Col591],
			--[Col592],
			--[Col593],
			--[Col594],
			--[Col595],
			--[Col596],
			--[Col597],
			--[Col598],
			--[Col599],
			--[Col600],
			--[Col601],
			--[Col602],
			--[Col603],
			--[Col604],
			--[Col605],
			--[Col606],
			--[Col607],
			--[Col608],
			--[Col609],
			--[Col610],
			--[Col611],
			--[Col612],
			--[Col613],
			--[Col614],
			--[Col615],
			--[Col616],
			--[Col617],
			--[Col618],
			--[Col619],
			--[Col620],
			--[Col621],
			--[Col622],
			--[Col623],
			--[Col624],
			--[Col625],
			--[Col626],
			--[Col627],
			--[Col628],
			--[Col629],
			--[Col630],
			--[Col631],
			--[Col632],
			--[Col633],
			--[Col634],
			--[Col635],
			--[Col636],
			--[Col637],
			--[Col638],
			--[Col639],
			--[Col640],
			--[Col641],
			--[Col642],
			--[Col643],
			--[Col644],
			--[Col645],
			--[Col646],
			--[Col647],
			--[Col648],
			--[Col649],
			--[Col650],
			--[Col651],
			--[Col652],
			--[Col653],
			--[Col654],
			--[Col655],
			--[Col656],
			--[Col657],
			--[Col658],
			--[Col659],
			--[Col660],
			--[Col661],
			--[Col662],
			--[Col663]
	FROM dbo.VW_Vehicle v 
	INNER JOIN CteLatestRecords	 ON v.ID = CteLatestRecords.VehicleID
	LEFT JOIN dbo.ChannelValue cv		ON v.UnitID = cv.UnitID AND CteLatestRecords.MaxChannelValueTimeStamp = cv.TimeStamp
	LEFT JOIN @currentVehicleFault vf ON v.UnitID = vf.FaultUnitID
	LEFT JOIN dbo.VW_FleetStatus fs	 ON v.ID = fs.VehicleID

END


GO
RAISERROR ('-- update NSP_HistoricEventChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricEventChannelData] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_HistoricEventChannelData
** Description:	Returns historic event channel data
** Call frequency: Every 5s for each vehicle shown on the data plots screen
** Parameters:	 @DateTimeEnd end timestamp
**				 @Interval amount of data to return in seconds
**				 @VehicleID the vehicle ID
**				 @SelectColList channel IDs to return separated by commas
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelData]
	@DateTimeEnd DATETIME
	,@Interval INT -- number of seconds
	,@UnitID INT
	,@SelectColList VARCHAR(4000)
AS

BEGIN
	SET NOCOUNT ON;

	DECLARE @sql varchar(max)

	IF @DateTimeEnd IS NULL
		SET @DateTimeEnd = SYSDATETIME()

	DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)

	DECLARE @vehicleTypeID int = (SELECT VehicleTypeID FROM dbo.VW_Vehicle WHERE ID = @UnitID)

	SET @sql = '
;WITH EventChannelValueLatest
AS (
	SELECT MAX(ecv.TimeStamp) AS TimeStamp
		,ecv.ChannelID
	FROM dbo.EventChannelValue ecv
	WHERE ecv.UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
		AND ecv.ChannelID IN (' + @SelectColList + ')
		AND ecv.TimeStamp BETWEEN DATEADD(DAY, - 1, ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''') AND ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + '''
	GROUP BY ecv.ChannelID
	)
SELECT ecv.TimeStamp
,ecv.ChannelID
,ecv.Value
,ecv.UnitID
FROM EventChannelValueLatest ecvl
JOIN dbo.EventChannelValue ecv
	ON ecv.UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
	AND ecv.ChannelID = ecvl.ChannelID
	AND ecv.TimeStamp = ecvl.TimeStamp
UNION ALL
SELECT
	TimeStamp = CAST(TimeStamp AS DATETIME)
	,ChannelID
	,Value
	,UnitID
FROM dbo.EventChannelValue
WHERE
	UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
	AND ChannelID IN (' + @SelectColList + ')' + '
	AND Timestamp BETWEEN ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(VARCHAR(23), @DateTimeEnd, 121) + ''''

	EXEC (@sql)
END

GO
RAISERROR ('-- update NSP_HistoricEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricEventChannelValue] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_HistoricEventChannelValue
** Description:	Returns historic event channel data
** Call frequency: Every 5s for each vehicle shown on the unit channel data screen
** Parameters:	 @timestamp the timestamp or null to get the latest data
**				 @unitId the unit ID
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelValue] 
	@timestamp DATETIME2(3)
	,@unitId INT
AS

BEGIN

SET NOCOUNT ON;
DECLARE @timestampInternal DATETIME2(3)

	IF @timestamp IS NULL
		SET @timestampInternal = DATEADD(hh, 1, GETDATE())
	ELSE 
		SET @timestampInternal = @timestamp

	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.ChannelID
		FROM dbo.EventChannelValue ecv 
		WHERE ecv.UnitID = @unitId
			AND ecv.TimeStamp BETWEEN DATEADD(DAY, -1, SYSDATETIME()) AND @timestampInternal
		GROUP BY ecv.ChannelID
		)
	SELECT ecv.TimeStamp
		,ecv.UnitID
		,ecv.ChannelID
		,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN dbo.EventChannelValue ecv 
		ON ecvl.TimeStamp = ecv.TimeStamp
		AND ecvl.ChannelID = ecv.ChannelID
		AND ecv.UnitID = @unitId
END

GO
RAISERROR ('-- update NSP_HistoricFleetSummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricFleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_HistoricFleetSummary] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_HistoricFleetSummary](
	@DateTimeEnd datetime
)
AS
BEGIN

	SET NOCOUNT ON;
		DECLARE @interval int = 3600 -- value in seconds 
	
	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @interval, @DateTimeEnd);

	-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
	DECLARE @LatestChannelValue TABLE(
		UnitID int
		, LastTimeStamp datetime2(3)
	)
	
	INSERT @LatestChannelValue(
		UnitID
		, LastTimeStamp
	)
	SELECT 
		UnitID	 = VW_Vehicle.UnitID
		, LastTimeStamp = (
			SELECT TOP 1 TimeStamp
			FROM dbo.ChannelValue
			WHERE 
				Timestamp >= @DateTimeStart
				AND Timestamp <= @DateTimeEnd
				AND UnitID =	VW_Vehicle.UnitID 
			ORDER BY TimeStamp DESC
		)
	FROM dbo.VW_Vehicle 

	SELECT 
		v.UnitID
		, v.UnitNumber
		, v.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, Diagram				= fs.Diagram
		, Headcode			 = fs.Headcode
		, VehicleID			 = v.ID
		, VehicleNumber		 = v.VehicleNumber
		, VehiclePosition		= v.VehicleOrder
		, VehicleType			= v.Type
		, Location			 = (
			SELECT TOP 1 
				LocationCode --CRS
			FROM dbo.Location
			INNER JOIN dbo.LocationArea ON LocationID = Location.ID
			WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
				AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
		, ServiceStatus = CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,TimeStamp			 = CAST(cv.[TimeStamp] AS datetime)
		,cv.[Col1]
		,cv.[Col2]
		,cv.[Col3]
		,cv.[Col4]
		--,cv.[Col6]
		--,cv.[Col30]
		,v.FleetCode
	FROM dbo.VW_Vehicle v 
	INNER JOIN @LatestChannelValue lcv ON v.UnitID = lcv.UnitID
	INNER JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = lcv.LastTimeStamp AND cv.UnitID = lcv.UnitID
	LEFT JOIN dbo.FleetStatusHistory fs ON v.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
	LEFT JOIN dbo.Location l ON l.ID = (
		SELECT TOP 1 LocationID 
		FROM dbo.LocationArea 
		WHERE col1 BETWEEN MinLatitude AND MaxLatitude
			AND col2 BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	
END

GO
RAISERROR ('-- update NSP_InsertChannelLimitAnalog', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelLimitAnalog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertChannelLimitAnalog] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_InsertChannelLimitAnalog
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/
ALTER PROCEDURE [dbo].[NSP_InsertChannelLimitAnalog]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @ErrorLowMin varchar(10)
	, @ErrorLowMax varchar(10)
	, @WarningLowMin varchar(10)
	, @WarningLowMax varchar(10)
	, @NormalMin varchar(10)
	, @NormalMax varchar(10)
	, @WarningHighMin varchar(10)
	, @WarningHighMax varchar(10)
	, @ErrorHighMin varchar(10)
	, @ErrorHighMax varchar(10)
	, @InvalidLowMax varchar(10) = NULL
	, @InvalidHighMin varchar(10) = NULL
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM dbo.Channel WHERE ID = @ChannelID)


--	PRINT '** ' + CAST(@ChannelID AS varchar(10)) + ' - ' + ISNULL(@ChannelName, 'NULL') + ' details:'

	-- ERROR LOW
	IF ISNUMERIC(@ErrorLowMin) = 1 OR ISNUMERIC(@ErrorLowMax) = 1 
	BEGIN
		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7
			, @ChannelName + ' - FAULT LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMin) = 1 THEN @ErrorLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMax) = 1 THEN @ErrorLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Warning LOW
	IF ISNUMERIC(@WarningLowMin) = 1 OR ISNUMERIC(@WarningLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningLowMin) = 1 THEN @WarningLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningLowMax) = 1 THEN @WarningLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Normal
	IF ISNUMERIC(@NormalMin) = 1 OR ISNUMERIC(@NormalMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@NormalMin) = 1 THEN @NormalMin
			END 
			, CASE	
				WHEN ISNUMERIC(@NormalMax) = 1 THEN @NormalMax
			END 
			, 1
			, 1
			
	END
	
	-- Warning HIGH
	IF ISNUMERIC(@WarningHighMin) = 1 OR ISNUMERIC(@WarningHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningHighMin) = 1 THEN @WarningHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningHighMax) = 1 THEN @WarningHighMax
			END 
			, 0
			, 1
			
	END

	
	-- ERROR HIGH
	IF ISNUMERIC(@ErrorHighMin) = 1 OR ISNUMERIC(@ErrorHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMin) = 1 THEN @ErrorHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMax) = 1 THEN @ErrorHighMax
			END 
			, 0
			, 1
			
	END


	-- INVALID High
	IF ISNUMERIC(@InvalidHighMin) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@InvalidHighMin) = 1 THEN @InvalidHighMin
			END 
			, NULL
			, 0
			, 0
			
	END
	
	-- INVALID Low
	IF ISNUMERIC(@InvalidLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, NULL
			, CASE	
				WHEN ISNUMERIC(@InvalidLowMax) = 1 THEN @InvalidLowMax
			END 
			, 0
			, 0
			
	END

END

GO
RAISERROR ('-- update NSP_InsertChannelLimitDigital', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelLimitDigital' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertChannelLimitDigital] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_InsertChannelLimitDigital
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_InsertChannelLimitDigital]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @Red varchar(1)
	, @Orange varchar(1)
	, @Green varchar(1)
	, @Blue varchar(1)
	, @Yellow varchar(1)
	, @Grey varchar(1)
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM dbo.Channel WHERE ID = @ChannelID)
	
	-- @Red - FAULT
	IF ISNUMERIC(@Red) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Red, @Red, 1, 1
			
	END

	-- @Orange - WARNING
	IF ISNUMERIC(@Orange) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Orange, @Orange, 1, 1
			
	END
	
	-- @Green - NORMAL
	IF ISNUMERIC(@Green) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1 --Fault
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Green, @Green, 1, 1
			
	END

	-- @Blue -- OFF
	IF ISNUMERIC(@Blue) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 3
			, @ChannelName + ' - OFF' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Blue, @Blue, 1, 1
			
	END

	-- @Yellow -- ON
	IF ISNUMERIC(@Yellow) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 2
			, @ChannelName + ' - ON' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Yellow, @Yellow, 1, 1
			
	END
		
END

GO
RAISERROR ('-- update NSP_InsertChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertChannelValue] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
		@UpdateRecord bit = null,
		@col1 decimal(9,6) = null,
		@col2 decimal(9,6) = null,
		@col3 smallint = null,
		@col4 smallint = null,
		@col5 bit = null,
		@col6 bit = null,
		@col7 bit = null,
		@col8 bit = null,
		@col9 bit = null,
		@col10 bit = null,
		@col11 bit = null,
		@col12 bit = null,
		@col13 bit = null,
		@col14 bit = null,
		@col15 bit = null,
		@col16 bit = null,
		@col17 bit = null,
		@col18 bit = null,
		@col19 bit = null,
		@col20 bit = null,
		@col21 bit = null,
		@col22 bit = null,
		@col23 bit = null,
		@col24 bit = null,
		@col25 bit = null,
		@col26 bit = null,
		@col27 bit = null,
		@col28 bit = null,
		@col29 bit = null,
		@col30 bit = null,
		@col31 bit = null,
		@col32 bit = null,
		@col33 bit = null,
		@col34 bit = null,
		@col35 bit = null,
		@col36 bit = null,
		@col37 bit = null,
		@col38 bit = null,
		@col39 bit = null,
		@col40 bit = null,
		@col41 bit = null,
		@col42 bit = null,
		@col43 bit = null,
		@col44 bit = null,
		@col45 bit = null,
		@col46 bit = null,
		@col47 bit = null,
		@col48 bit = null,
		@col49 bit = null,
		@col50 bit = null,
		@col51 bit = null,
		@col52 bit = null,
		@col53 bit = null,
		@col54 bit = null,
		@col55 bit = null,
		@col56 bit = null,
		@col57 bit = null,
		@col58 bit = null,
		@col59 bit = null,
		@col60 bit = null,
		@col61 smallint = null,
		@col62 smallint = null,
		@col63 smallint = null,
		@col64 smallint = null,
		@col65 smallint = null,
		@col66 bit = null,
		@col67 bit = null,
		@col68 bit = null,
		@col69 bit = null,
		@col70 bit = null,
		@col71 bit = null,
		@col72 bit = null,
		@col73 bit = null,
		@col74 bit = null,
		@col75 bit = null,
		@col76 bit = null,
		@col77 bit = null,
		@col78 bit = null,
		@col79 bit = null,
		@col80 bit = null,
		@col81 bit = null,
		@col82 smallint = null,
		@col83 tinyint = null,
		@col84 tinyint = null,
		@col85 smallint = null,
		@col86 smallint = null,
		@col87 smallint = null,
		@col88 smallint = null,
		@col89 smallint = null,
		@col90 smallint = null,
		@col91 smallint = null,
		@col92 smallint = null,
		@col93 bit = null,
		@col94 bit = null,
		@col95 bit = null,
		@col96 bit = null,
		@col97 bit = null,
		@col98 bit = null,
		@col99 bit = null,
		@col100 bit = null,
		@col101 bit = null,
		@col102 bit = null,
		@col103 bit = null,
		@col104 bit = null,
		@col105 bit = null,
		@col106 bit = null,
		@col107 bit = null,
		@col108 bit = null,
		@col109 smallint = null,
		@col110 tinyint = null,
		@col111 tinyint = null,
		@col112 smallint = null,
		@col113 smallint = null,
		@col114 smallint = null,
		@col115 tinyint = null,
		@col116 tinyint = null,
		@col117 bit = null,
		@col118 bit = null,
		@col119 bit = null,
		@col120 tinyint = null,
		@col121 tinyint = null,
		@col122 bit = null,
		@col123 bit = null,
		@col124 bit = null,
		@col125 bit = null,
		@col126 bit = null,
		@col127 bit = null,
		@col128 bit = null,
		@col129 bit = null,
		@col130 bit = null,
		@col131 bit = null,
		@col132 bit = null,
		@col133 bit = null,
		@col134 bit = null,
		@col135 bit = null,
		@col136 bit = null,
		@col137 bit = null,
		@col138 bit = null,
		@col139 bit = null,
		@col140 bit = null,
		@col141 bit = null,
		@col142 bit = null,
		@col143 bit = null,
		@col144 bit = null,
		@col145 bit = null,
		@col146 bit = null,
		@col147 bit = null,
		@col148 bit = null,
		@col149 bit = null,
		@col150 bit = null,
		@col151 bit = null,
		@col152 bit = null,
		@col153 bit = null,
		@col154 bit = null,
		@col155 bit = null,
		@col156 bit = null,
		@col157 bit = null,
		@col158 bit = null,
		@col159 bit = null,
		@col160 bit = null,
		@col161 bit = null,
		@col162 bit = null,
		@col163 bit = null,
		@col164 bit = null,
		@col165 bit = null,
		@col166 bit = null,
		@col167 bit = null,
		@col168 bit = null,
		@col169 tinyint = null,
		@col170 tinyint = null,
		@col171 tinyint = null,
		@col172 tinyint = null,
		@col173 tinyint = null,
		@col174 bit = null,
		@col175 bit = null,
		@col176 bit = null,
		@col177 bit = null,
		@col178 bit = null,
		@col179 bit = null,
		@col180 bit = null,
		@col181 bit = null,
		@col182 bit = null,
		@col183 bit = null,
		@col184 bit = null,
		@col185 bit = null,
		@col186 bit = null,
		@col187 bit = null,
		@col188 bit = null,
		@col189 bit = null,
		@col190 bit = null,
		@col191 bit = null,
		@col192 bit = null,
		@col193 bit = null,
		@col194 bit = null,
		@col195 bit = null,
		@col196 bit = null,
		@col197 bit = null,
		@col198 bit = null,
		@col199 bit = null,
		@col200 bit = null,
		@col201 bit = null,
		@col202 bit = null,
		@col203 bit = null,
		@col204 bit = null,
		@col205 bit = null,
		@col206 bit = null,
		@col207 bit = null,
		@col208 bit = null,
		@col209 bit = null,
		@col210 bit = null,
		@col211 bit = null,
		@col212 bit = null,
		@col213 bit = null,
		@col214 bit = null,
		@col215 bit = null,
		@col216 bit = null,
		@col217 bit = null,
		@col218 bit = null,
		@col219 bit = null,
		@col220 bit = null,
		@col221 bit = null,
		@col222 bit = null,
		@col223 bit = null,
		@col224 bit = null,
		@col225 bit = null,
		@col226 bit = null,
		@col227 bit = null,
		@col228 bit = null,
		@col229 bit = null,
		@col230 bit = null,
		@col231 bit = null,
		@col232 bit = null,
		@col233 bit = null,
		@col234 bit = null,
		@col235 bit = null,
		@col236 bit = null,
		@col237 bit = null,
		@col238 bit = null,
		@col239 bit = null,
		@col240 bit = null,
		@col241 bit = null,
		@col242 bit = null,
		@col243 bit = null,
		@col244 bit = null,
		@col245 bit = null,
		@col246 bit = null,
		@col247 bit = null,
		@col248 bit = null,
		@col249 bit = null,
		@col250 bit = null,
		@col251 bit = null,
		@col252 smallint = null,
		@col253 smallint = null,
		@col254 smallint = null,
		@col255 smallint = null,
		@col256 bit = null,
		@col257 bit = null,
		@col258 bit = null,
		@col259 bit = null,
		@col260 bit = null,
		@col261 bit = null,
		@col262 bit = null,
		@col263 bit = null,
		@col264 bit = null,
		@col265 bit = null,
		@col266 bit = null,
		@col267 bit = null,
		@col268 bit = null,
		@col269 bit = null,
		@col270 bit = null,
		@col271 bit = null,
		@col272 bit = null,
		@col273 bit = null,
		@col274 bit = null,
		@col275 bit = null,
		@col276 bit = null,
		@col277 bit = null,
		@col278 bit = null,
		@col279 bit = null,
		@col280 bit = null,
		@col281 bit = null,
		@col282 bit = null,
		@col283 bit = null,
		@col284 bit = null,
		@col285 bit = null,
		@col286 bit = null,
		@col287 bit = null,
		@col288 bit = null,
		@col289 bit = null,
		@col290 bit = null,
		@col291 bit = null,
		@col292 bit = null,
		@col293 bit = null,
		@col294 bit = null,
		@col295 bit = null,
		@col296 bit = null,
		@col297 smallint = null,
		@col298 smallint = null,
		@col299 smallint = null,
		@col300 smallint = null,
		@col301 smallint = null,
		@col302 smallint = null,
		@col303 smallint = null,
		@col304 smallint = null,
		@col305 smallint = null,
		@col306 smallint = null,
		@col307 smallint = null,
		@col308 smallint = null,
		@col309 smallint = null,
		@col310 smallint = null,
		@col311 smallint = null,
		@col312 smallint = null,
		@col313 smallint = null,
		@col314 smallint = null,
		@col315 smallint = null,
		@col316 smallint = null,
		@col317 smallint = null,
		@col318 smallint = null,
		@col319 smallint = null,
		@col320 smallint = null,
		@col321 smallint = null,
		@col322 smallint = null,
		@col323 smallint = null,
		@col324 smallint = null,
		@col325 smallint = null,
		@col326 smallint = null,
		@col327 smallint = null,
		@col328 smallint = null,
		@col329 smallint = null,
		@col330 smallint = null,
		@col331 smallint = null,
		@col332 smallint = null,
		@col333 smallint = null,
		@col334 smallint = null,
		@col335 smallint = null,
		@col336 smallint = null,
		@col337 smallint = null,
		@col338 bit = null,
		@col339 bit = null,
		@col340 bit = null,
		@col341 bit = null,
		@col342 bit = null,
		@col343 bit = null,
		@col344 bit = null,
		@col345 bit = null,
		@col346 bit = null,
		@col347 bit = null,
		@col348 bit = null,
		@col349 bit = null,
		@col350 bit = null,
		@col351 bit = null,
		@col352 bit = null,
		@col353 bit = null,
		@col354 bit = null,
		@col355 bit = null,
		@col356 bit = null,
		@col357 bit = null,
		@col358 bit = null,
		@col359 bit = null,
		@col360 bit = null,
		@col361 bit = null,
		@col362 tinyint = null,
		@col363 smallint = null,
		@col364 tinyint = null,
		@col365 tinyint = null,
		@col366 tinyint = null,
		@col367 tinyint = null,
		@col368 smallint = null,
		@col369 smallint = null,
		@col370 smallint = null,
		@col371 smallint = null,
		@col372 smallint = null,
		@col373 smallint = null,
		@col374 smallint = null,
		@col375 smallint = null,
		@col376 smallint = null,
		@col377 smallint = null,
		@col378 smallint = null,
		@col379 smallint = null,
		@col380 smallint = null,
		@col381 smallint = null,
		@col382 smallint = null,
		@col383 smallint = null,
		@col384 smallint = null,
		@col385 smallint = null,
		@col386 smallint = null,
		@col387 smallint = null,
		@col388 smallint = null,
		@col389 smallint = null,
		@col390 smallint = null,
		@col391 smallint = null,
		@col392 smallint = null,
		@col393 smallint = null,
		@col394 smallint = null,
		@col395 smallint = null,
		@col396 smallint = null,
		@col397 smallint = null,
		@col398 smallint = null,
		@col399 smallint = null,
		@col400 smallint = null,
		@col401 smallint = null,
		@col402 smallint = null,
		@col403 smallint = null,
		@col404 smallint = null,
		@col405 smallint = null,
		@col406 smallint = null,
		@col407 smallint = null,
		@col408 smallint = null,
		@col409 smallint = null,
		@col410 smallint = null,
		@col411 smallint = null,
		@col412 smallint = null,
		@col413 smallint = null,
		@col414 smallint = null,
		@col415 smallint = null,
		@col416 smallint = null,
		@col417 smallint = null,
		@col418 smallint = null,
		@col419 smallint = null,
		@col420 smallint = null,
		@col421 smallint = null,
		@col422 smallint = null,
		@col423 smallint = null,
		@col424 smallint = null,
		@col425 smallint = null,
		@col426 smallint = null,
		@col427 smallint = null,
		@col428 smallint = null,
		@col429 smallint = null,
		@col430 smallint = null,
		@col431 smallint = null,
		@col432 smallint = null,
		@col433 smallint = null,
		@col434 smallint = null,
		@col435 smallint = null,
		@col436 smallint = null,
		@col437 smallint = null,
		@col438 smallint = null,
		@col439 smallint = null,
		@col440 smallint = null,
		@col441 smallint = null,
		@col442 smallint = null,
		@col443 smallint = null,
		@col444 smallint = null,
		@col445 smallint = null,
		@col446 smallint = null,
		@col447 smallint = null,
		@col448 smallint = null,
		@col449 smallint = null,
		@col450 tinyint = null,
		@col451 tinyint = null,
		@col452 tinyint = null,
		@col453 tinyint = null,
		@col454 tinyint = null,
		@col455 tinyint = null,
		@col456 tinyint = null,
		@col457 tinyint = null,
		@col458 tinyint = null,
		@col459 tinyint = null,
		@col460 tinyint = null,
		@col461 smallint = null,
		@col462 tinyint = null,
		@col463 smallint = null,
		@col464 smallint = null,
		@col465 smallint = null,
		@col466 smallint = null,
		@col467 bit = null,
		@col468 bit = null,
		@col469 bit = null,
		@col470 bit = null,
		@col471 bit = null,
		@col472 bit = null,
		@col473 bit = null,
		@col474 bit = null,
		@col475 bit = null,
		@col476 bit = null,
		@col477 bit = null,
		@col478 bit = null,
		@col479 bit = null,
		@col480 bit = null,
		@col481 bit = null,
		@col482 bit = null,
		@col483 bit = null,
		@col484 bit = null,
		@col485 bit = null,
		@col486 bit = null,
		@col487 bit = null,
		@col488 bit = null,
		@col489 bit = null,
		@col490 bit = null,
		@col491 bit = null,
		@col492 bit = null,
		@col493 bit = null,
		@col494 bit = null,
		@col495 bit = null,
		@col496 bit = null,
		@col497 bit = null,
		@col498 bit = null,
		@col499 bit = null,
		@col500 bit = null,
		@col501 bit = null,
		@col502 bit = null,
		@col503 bit = null,
		@col504 bit = null,
		@col505 bit = null,
		@col506 bit = null,
		@col507 bit = null,
		@col508 bit = null,
		@col509 bit = null,
		@col510 bit = null,
		@col511 bit = null,
		@col512 bit = null,
		@col513 bit = null,
		@col514 bit = null,
		@col515 bit = null,
		@col516 bit = null,
		@col517 bit = null,
		@col518 bit = null,
		@col519 bit = null,
		@col520 bit = null,
		@col521 bit = null,
		@col522 bit = null,
		@col523 bit = null,
		@col524 bit = null,
		@col525 bit = null,
		@col526 bit = null,
		@col527 bit = null,
		@col528 bit = null,
		@col529 bit = null,
		@col530 bit = null,
		@col531 bit = null,
		@col532 bit = null,
		@col533 bit = null,
		@col534 bit = null,
		@col535 bit = null,
		@col536 bit = null,
		@col537 bit = null,
		@col538 bit = null,
		@col539 bit = null,
		@col540 bit = null,
		@col541 bit = null,
		@col542 bit = null,
		@col543 bit = null,
		@col544 bit = null,
		@col545 tinyint = null,
		@col546 tinyint = null,
		@col547 tinyint = null,
		@col548 tinyint = null,
		@col549 tinyint = null,
		@col550 tinyint = null,
		@col551 smallint = null,
		@col552 smallint = null,
		@col553 tinyint = null,
		@col554 tinyint = null,
		@col555 tinyint = null,
		@col556 tinyint = null,
		@col557 tinyint = null,
		@col558 tinyint = null,
		@col559 tinyint = null,
		@col560 tinyint = null,
		@col561 tinyint = null,
		@col562 bit = null,
		@col563 bit = null,
		@col564 smallint = null,
		@col565 smallint = null,
		@col566 bit = null,
		@col567 tinyint = null,
		@col568 smallint = null,
		@col569 smallint = null,
		@col570 tinyint = null,
		@col571 tinyint = null,
		@col572 smallint = null,
		@col573 tinyint = null,
		@col574 bit = null,
		@col575 bit = null,
		@col576 bit = null,
		@col577 bit = null,
		@col578 bit = null,
		@col579 bit = null,
		@col580 smallint = null,
		@col581 smallint = null,
		@col582 smallint = null,
		@col583 smallint = null,
		@col584 smallint = null,
		@col585 smallint = null,
		@col586 smallint = null,
		@col587 smallint = null,
		@col588 bit = null,
		@col589 bit = null,
		@col590 tinyint = null,
		@col591 tinyint = null,
		@col592 bit = null,
		@col593 bit = null,
		@col594 tinyint = null,
		@col595 bit = null,
		@col596 bit = null,
		@col597 tinyint = null,
		@col598 tinyint = null,
		@col599 smallint = null,
		@col600 tinyint = null,
		@col601 tinyint = null,
		@col602 tinyint = null,
		@col603 bit = null,
		@col604 tinyint = null,
		@col605 tinyint = null,
		@col606 tinyint = null,
		@col607 tinyint = null,
		@col608 tinyint = null,
		@col609 tinyint = null,
		@col610 tinyint = null,
		@col611 tinyint = null,
		@col612 tinyint = null,
		@col613 tinyint = null,
		@col614 tinyint = null,
		@col615 tinyint = null,
		@col616 tinyint = null,
		@col617 tinyint = null,
		@col618 tinyint = null,
		@col619 tinyint = null,
		@col620 tinyint = null,
		@col621 tinyint = null,
		@col622 tinyint = null,
		@col623 tinyint = null,
		@col624 tinyint = null,
		@col625 tinyint = null,
		@col626 tinyint = null,
		@col627 tinyint = null,
		@col628 tinyint = null,
		@col629 tinyint = null,
		@col630 tinyint = null,
		@col631 tinyint = null,
		@col632 tinyint = null,
		@col633 tinyint = null,
		@col634 tinyint = null,
		@col635 tinyint = null,
		@col636 tinyint = null,
		@col637 tinyint = null,
		@col638 tinyint = null,
		@col639 tinyint = null,
		@col640 tinyint = null,
		@col641 tinyint = null,
		@col642 tinyint = null,
		@col643 tinyint = null,
		@col644 tinyint = null,
		@col645 tinyint = null,
		@col646 bit = null,
		@col647 bit = null,
		@col648 smallint = null,
		@col649 smallint = null,
		@col650 smallint = null,
		@col651 bit = null,
		@col652 smallint = null,
		@col653 bit = null,
		@col654 bit = null,
		@col655 smallint = null,
		@col656 smallint = null,
		@col657 smallint = null,
		@col658 bit = null,
		@col659 smallint = null,
		@col660 tinyint = null,
		@col661 bit = null,
		@col662 bit = null,
		@col663 bit = null
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [dbo].[ChannelValue]
			([UnitID]
			,[RecordInsert]
			,[TimeStamp]
			,[UpdateRecord]
			,[col1]			,[col2]
			,[col3]			,[col4]
			--,[col5]			,[col6]
			--,[col7]			,[col8]
			--,[col9]			,[col10]
			--,[col11]			,[col12]
			--,[col13]			,[col14]
			--,[col15]			,[col16]
			--,[col17]			,[col18]
			--,[col19]			,[col20]
			--,[col21]			,[col22]
			--,[col23]			,[col24]
			--,[col25]			,[col26]
			--,[col27]			,[col28]
			--,[col29]			,[col30]
			--,[col31]			,[col32]
			--,[col33]			,[col34]
			--,[col35]			,[col36]
			--,[col37]			,[col38]
			--,[col39]			,[col40]
			--,[col41]			,[col42]
			--,[col43]			,[col44]
			--,[col45]			,[col46]
			--,[col47]			,[col48]
			--,[col49]			,[col50]
			--,[col51]			,[col52]
			--,[col53]			,[col54]
			--,[col55]			,[col56]
			--,[col57]			,[col58]
			--,[col59]			,[col60]
			--,[col61]			,[col62]
			--,[col63]			,[col64]
			--,[col65]			,[col66]
			--,[col67]			,[col68]
			--,[col69]			,[col70]
			--,[col71]			,[col72]
			--,[col73]			,[col74]
			--,[col75]			,[col76]
			--,[col77]			,[col78]
			--,[col79]			,[col80]
			--,[col81]			,[col82]
			--,[col83]			,[col84]
			--,[col85]			,[col86]
			--,[col87]			,[col88]
			--,[col89]			,[col90]
			--,[col91]			,[col92]
			--,[col93]			,[col94]
			--,[col95]			,[col96]
			--,[col97]			,[col98]
			--,[col99]			,[col100]
			--,[col101]
			--,[col102]
			--,[col103]
			--,[col104]
			--,[col105]
			--,[col106]
			--,[col107]
			--,[col108]
			--,[col109]
			--,[col110]
			--,[col111]
			--,[col112]
			--,[col113]
			--,[col114]
			--,[col115]
			--,[col116]
			--,[col117]
			--,[col118]
			--,[col119]
			--,[col120]
			--,[col121]
			--,[col122]
			--,[col123]
			--,[col124]
			--,[col125]
			--,[col126]
			--,[col127]
			--,[col128]
			--,[col129]
			--,[col130]
			--,[col131]
			--,[col132]
			--,[col133]
			--,[col134]
			--,[col135]
			--,[col136]
			--,[col137]
			--,[col138]
			--,[col139]
			--,[col140]
			--,[col141]
			--,[col142]
			--,[col143]
			--,[col144]
			--,[col145]
			--,[col146]
			--,[col147]
			--,[col148]
			--,[col149]
			--,[col150]
			--,[col151]
			--,[col152]
			--,[col153]
			--,[col154]
			--,[col155]
			--,[col156]
			--,[col157]
			--,[col158]
			--,[col159]
			--,[col160]
			--,[col161]
			--,[col162]
			--,[col163]
			--,[col164]
			--,[col165]
			--,[col166]
			--,[col167]
			--,[col168]
			--,[col169]
			--,[col170]
			--,[col171]
			--,[col172]
			--,[col173]
			--,[col174]
			--,[col175]
			--,[col176]
			--,[col177]
			--,[col178]
			--,[col179]
			--,[col180]
			--,[col181]
			--,[col182]
			--,[col183]
			--,[col184]
			--,[col185]
			--,[col186]
			--,[col187]
			--,[col188]
			--,[col189]
			--,[col190]
			--,[col191]
			--,[col192]
			--,[col193]
			--,[col194]
			--,[col195]
			--,[col196]
			--,[col197]
			--,[col198]
			--,[col199]
			--,[col200]
			--,[col201]
			--,[col202]
			--,[col203]
			--,[col204]
			--,[col205]
			--,[col206]
			--,[col207]
			--,[col208]
			--,[col209]
			--,[col210]
			--,[col211]
			--,[col212]
			--,[col213]
			--,[col214]
			--,[col215]
			--,[col216]
			--,[col217]
			--,[col218]
			--,[col219]
			--,[col220]
			--,[col221]
			--,[col222]
			--,[col223]
			--,[col224]
			--,[col225]
			--,[col226]
			--,[col227]
			--,[col228]
			--,[col229]
			--,[col230]
			--,[col231]
			--,[col232]
			--,[col233]
			--,[col234]
			--,[col235]
			--,[col236]
			--,[col237]
			--,[col238]
			--,[col239]
			--,[col240]
			--,[col241]
			--,[col242]
			--,[col243]
			--,[col244]
			--,[col245]
			--,[col246]
			--,[col247]
			--,[col248]
			--,[col249]
			--,[col250]
			--,[col251]
			--,[col252]
			--,[col253]
			--,[col254]
			--,[col255]
			--,[col256]
			--,[col257]
			--,[col258]
			--,[col259]
			--,[col260]
			--,[col261]
			--,[col262]
			--,[col263]
			--,[col264]
			--,[col265]
			--,[col266]
			--,[col267]
			--,[col268]
			--,[col269]
			--,[col270]
			--,[col271]
			--,[col272]
			--,[col273]
			--,[col274]
			--,[col275]
			--,[col276]
			--,[col277]
			--,[col278]
			--,[col279]
			--,[col280]
			--,[col281]
			--,[col282]
			--,[col283]
			--,[col284]
			--,[col285]
			--,[col286]
			--,[col287]
			--,[col288]
			--,[col289]
			--,[col290]
			--,[col291]
			--,[col292]
			--,[col293]
			--,[col294]
			--,[col295]
			--,[col296]
			--,[col297]
			--,[col298]
			--,[col299]
			--,[col300]
			--,[col301]
			--,[col302]
			--,[col303]
			--,[col304]
			--,[col305]
			--,[col306]
			--,[col307]
			--,[col308]
			--,[col309]
			--,[col310]
			--,[col311]
			--,[col312]
			--,[col313]
			--,[col314]
			--,[col315]
			--,[col316]
			--,[col317]
			--,[col318]
			--,[col319]
			--,[col320]
			--,[col321]
			--,[col322]
			--,[col323]
			--,[col324]
			--,[col325]
			--,[col326]
			--,[col327]
			--,[col328]
			--,[col329]
			--,[col330]
			--,[col331]
			--,[col332]
			--,[col333]
			--,[col334]
			--,[col335]
			--,[col336]
			--,[col337]
			--,[col338]
			--,[col339]
			--,[col340]
			--,[col341]
			--,[col342]
			--,[col343]
			--,[col344]
			--,[col345]
			--,[col346]
			--,[col347]
			--,[col348]
			--,[col349]
			--,[col350]
			--,[col351]
			--,[col352]
			--,[col353]
			--,[col354]
			--,[col355]
			--,[col356]
			--,[col357]
			--,[col358]
			--,[col359]
			--,[col360]
			--,[col361]
			--,[col362]
			--,[col363]
			--,[col364]
			--,[col365]
			--,[col366]
			--,[col367]
			--,[col368]
			--,[col369]
			--,[col370]
			--,[col371]
			--,[col372]
			--,[col373]
			--,[col374]
			--,[col375]
			--,[col376]
			--,[col377]
			--,[col378]
			--,[col379]
			--,[col380]
			--,[col381]
			--,[col382]
			--,[col383]
			--,[col384]
			--,[col385]
			--,[col386]
			--,[col387]
			--,[col388]
			--,[col389]
			--,[col390]
			--,[col391]
			--,[col392]
			--,[col393]
			--,[col394]
			--,[col395]
			--,[col396]
			--,[col397]
			--,[col398]
			--,[col399]
			--,[col400]
			--,[col401]
			--,[col402]
			--,[col403]
			--,[col404]
			--,[col405]
			--,[col406]
			--,[col407]
			--,[col408]
			--,[col409]
			--,[col410]
			--,[col411]
			--,[col412]
			--,[col413]
			--,[col414]
			--,[col415]
			--,[col416]
			--,[col417]
			--,[col418]
			--,[col419]
			--,[col420]
			--,[col421]
			--,[col422]
			--,[col423]
			--,[col424]
			--,[col425]
			--,[col426]
			--,[col427]
			--,[col428]
			--,[col429]
			--,[col430]
			--,[col431]
			--,[col432]
			--,[col433]
			--,[col434]
			--,[col435]
			--,[col436]
			--,[col437]
			--,[col438]
			--,[col439]
			--,[col440]
			--,[col441]
			--,[col442]
			--,[col443]
			--,[col444]
			--,[col445]
			--,[col446]
			--,[col447]
			--,[col448]
			--,[col449]
			--,[col450]
			--,[col451]
			--,[col452]
			--,[col453]
			--,[col454]
			--,[col455]
			--,[col456]
			--,[col457]
			--,[col458]
			--,[col459]
			--,[col460]
			--,[col461]
			--,[col462]
			--,[col463]
			--,[col464]
			--,[col465]
			--,[col466]
			--,[col467]
			--,[col468]
			--,[col469]
			--,[col470]
			--,[col471]
			--,[col472]
			--,[col473]
			--,[col474]
			--,[col475]
			--,[col476]
			--,[col477]
			--,[col478]
			--,[col479]
			--,[col480]
			--,[col481]
			--,[col482]
			--,[col483]
			--,[col484]
			--,[col485]
			--,[col486]
			--,[col487]
			--,[col488]
			--,[col489]
			--,[col490]
			--,[col491]
			--,[col492]
			--,[col493]
			--,[col494]
			--,[col495]
			--,[col496]
			--,[col497]
			--,[col498]
			--,[col499]
			--,[col500]
			--,[col501]
			--,[col502]
			--,[col503]
			--,[col504]
			--,[col505]
			--,[col506]
			--,[col507]
			--,[col508]
			--,[col509]
			--,[col510]
			--,[col511]
			--,[col512]
			--,[col513]
			--,[col514]
			--,[col515]
			--,[col516]
			--,[col517]
			--,[col518]
			--,[col519]
			--,[col520]
			--,[col521]
			--,[col522]
			--,[col523]
			--,[col524]
			--,[col525]
			--,[col526]
			--,[col527]
			--,[col528]
			--,[col529]
			--,[col530]
			--,[col531]
			--,[col532]
			--,[col533]
			--,[col534]
			--,[col535]
			--,[col536]
			--,[col537]
			--,[col538]
			--,[col539]
			--,[col540]
			--,[col541]
			--,[col542]
			--,[col543]
			--,[col544]
			--,[col545]
			--,[col546]
			--,[col547]
			--,[col548]
			--,[col549]
			--,[col550]
			--,[col551]
			--,[col552]
			--,[col553]
			--,[col554]
			--,[col555]
			--,[col556]
			--,[col557]
			--,[col558]
			--,[col559]
			--,[col560]
			--,[col561]
			--,[col562]
			--,[col563]
			--,[col564]
			--,[col565]
			--,[col566]
			--,[col567]
			--,[col568]
			--,[col569]
			--,[col570]
			--,[col571]
			--,[col572]
			--,[col573]
			--,[col574]
			--,[col575]
			--,[col576]
			--,[col577]
			--,[col578]
			--,[col579]
			--,[col580]
			--,[col581]
			--,[col582]
			--,[col583]
			--,[col584]
			--,[col585]
			--,[col586]
			--,[col587]
			--,[col588]
			--,[col589]
			--,[col590]
			--,[col591]
			--,[col592]
			--,[col593]
			--,[col594]
			--,[col595]
			--,[col596]
			--,[col597]
			--,[col598]
			--,[col599]
			--,[col600]
			--,[col601]
			--,[col602]
			--,[col603]
			--,[col604]
			--,[col605]
			--,[col606]
			--,[col607]
			--,[col608]
			--,[col609]
			--,[col610]
			--,[col611]
			--,[col612]
			--,[col613]
			--,[col614]
			--,[col615]
			--,[col616]
			--,[col617]
			--,[col618]
			--,[col619]
			--,[col620]
			--,[col621]
			--,[col622]
			--,[col623]
			--,[col624]
			--,[col625]
			--,[col626]
			--,[col627]
			--,[col628]
			--,[col629]
			--,[col630]
			--,[col631]
			--,[col632]
			--,[col633]
			--,[col634]
			--,[col635]
			--,[col636]
			--,[col637]
			--,[col638]
			--,[col639]
			--,[col640]
			--,[col641]
			--,[col642]
			--,[col643]
			--,[col644]
			--,[col645]
			--,[col646]
			--,[col647]
			--,[col648]
			--,[col649]
			--,[col650]
			--,[col651]
			--,[col652]
			--,[col653]
			--,[col654]
			--,[col655]
			--,[col656]
			--,[col657]
			--,[col658]
			--,[col659]
			--,[col660]
			--,[col661]
			--,[col662]
			--,[col663]
			)
 	SELECT
		@UnitID, 
		getdate(),
		@Timestamp,
		@UpdateRecord,
		@col1,
		@col2,
		@col3 ,
		@col4 --,
		--@col5 ,
		--@col6 ,
		--@col7 ,
		--@col8 ,
		--@col9 ,
		--@col10 ,
		--@col11 ,
		--@col12 ,
		--@col13 ,
		--@col14 ,
		--@col15 ,
		--@col16 ,
		--@col17 ,
		--@col18 ,
		--@col19 ,
		--@col20 ,
		--@col21 ,
		--@col22 ,
		--@col23 ,
		--@col24 ,
		--@col25 ,
		--@col26 ,
		--@col27 ,
		--@col28 ,
		--@col29 ,
		--@col30 ,
		--@col31 ,
		--@col32 ,
		--@col33 ,
		--@col34 ,
		--@col35 ,
		--@col36 ,
		--@col37 ,
		--@col38 ,
		--@col39 ,
		--@col40 ,
		--@col41 ,
		--@col42 ,
		--@col43 ,
		--@col44 ,
		--@col45 ,
		--@col46 ,
		--@col47 ,
		--@col48 ,
		--@col49 ,
		--@col50 ,
		--@col51 ,
		--@col52 ,
		--@col53 ,
		--@col54 ,
		--@col55 ,
		--@col56 ,
		--@col57 ,
		--@col58 ,
		--@col59 ,
		--@col60 ,
		--@col61 ,
		--@col62 ,
		--@col63 ,
		--@col64 ,
		--@col65 ,
		--@col66 ,
		--@col67 ,
		--@col68 ,
		--@col69 ,
		--@col70 ,
		--@col71 ,
		--@col72 ,
		--@col73 ,
		--@col74 ,
		--@col75 ,
		--@col76 ,
		--@col77 ,
		--@col78 ,
		--@col79 ,
		--@col80 ,
		--@col81 ,
		--@col82 ,
		--@col83 ,
		--@col84 ,
		--@col85 ,
		--@col86 ,
		--@col87 ,
		--@col88 ,
		--@col89 ,
		--@col90 ,
		--@col91 ,
		--@col92 ,
		--@col93 ,
		--@col94 ,
		--@col95 ,
		--@col96 ,
		--@col97 ,
		--@col98 ,
		--@col99 ,
		--@col100 ,
		--@col101 ,
		--@col102 ,
		--@col103 ,
		--@col104 ,
		--@col105 ,
		--@col106 ,
		--@col107 ,
		--@col108 ,
		--@col109 ,
		--@col110 ,
		--@col111 ,
		--@col112 ,
		--@col113 ,
		--@col114 ,
		--@col115 ,
		--@col116 ,
		--@col117 ,
		--@col118 ,
		--@col119 ,
		--@col120 ,
		--@col121 ,
		--@col122 ,
		--@col123 ,
		--@col124 ,
		--@col125 ,
		--@col126 ,
		--@col127 ,
		--@col128 ,
		--@col129 ,
		--@col130 ,
		--@col131 ,
		--@col132 ,
		--@col133 ,
		--@col134 ,
		--@col135 ,
		--@col136 ,
		--@col137 ,
		--@col138 ,
		--@col139 ,
		--@col140 ,
		--@col141 ,
		--@col142 ,
		--@col143 ,
		--@col144 ,
		--@col145 ,
		--@col146 ,
		--@col147 ,
		--@col148 ,
		--@col149 ,
		--@col150 ,
		--@col151 ,
		--@col152 ,
		--@col153 ,
		--@col154 ,
		--@col155 ,
		--@col156 ,
		--@col157 ,
		--@col158 ,
		--@col159 ,
		--@col160 ,
		--@col161 ,
		--@col162 ,
		--@col163 ,
		--@col164 ,
		--@col165 ,
		--@col166 ,
		--@col167 ,
		--@col168 ,
		--@col169 ,
		--@col170 ,
		--@col171 ,
		--@col172 ,
		--@col173 ,
		--@col174 ,
		--@col175 ,
		--@col176 ,
		--@col177 ,
		--@col178 ,
		--@col179 ,
		--@col180 ,
		--@col181 ,
		--@col182 ,
		--@col183 ,
		--@col184 ,
		--@col185 ,
		--@col186 ,
		--@col187 ,
		--@col188 ,
		--@col189 ,
		--@col190 ,
		--@col191 ,
		--@col192 ,
		--@col193 ,
		--@col194 ,
		--@col195 ,
		--@col196 ,
		--@col197 ,
		--@col198 ,
		--@col199 ,
		--@col200 ,
		--@col201 ,
		--@col202 ,
		--@col203 ,
		--@col204 ,
		--@col205 ,
		--@col206 ,
		--@col207 ,
		--@col208 ,
		--@col209 ,
		--@col210 ,
		--@col211 ,
		--@col212 ,
		--@col213 ,
		--@col214 ,
		--@col215 ,
		--@col216 ,
		--@col217 ,
		--@col218 ,
		--@col219 ,
		--@col220 ,
		--@col221 ,
		--@col222 ,
		--@col223 ,
		--@col224 ,
		--@col225 ,
		--@col226 ,
		--@col227 ,
		--@col228 ,
		--@col229 ,
		--@col230 ,
		--@col231 ,
		--@col232 ,
		--@col233 ,
		--@col234 ,
		--@col235 ,
		--@col236 ,
		--@col237 ,
		--@col238 ,
		--@col239 ,
		--@col240 ,
		--@col241 ,
		--@col242 ,
		--@col243 ,
		--@col244 ,
		--@col245 ,
		--@col246 ,
		--@col247 ,
		--@col248 ,
		--@col249 ,
		--@col250 ,
		--@col251 ,
		--@col252 ,
		--@col253 ,
		--@col254 ,
		--@col255 ,
		--@col256 ,
		--@col257 ,
		--@col258 ,
		--@col259 ,
		--@col260 ,
		--@col261 ,
		--@col262 ,
		--@col263 ,
		--@col264 ,
		--@col265 ,
		--@col266 ,
		--@col267 ,
		--@col268 ,
		--@col269 ,
		--@col270 ,
		--@col271 ,
		--@col272 ,
		--@col273 ,
		--@col274 ,
		--@col275 ,
		--@col276 ,
		--@col277 ,
		--@col278 ,
		--@col279 ,
		--@col280 ,
		--@col281 ,
		--@col282 ,
		--@col283 ,
		--@col284 ,
		--@col285 ,
		--@col286 ,
		--@col287 ,
		--@col288 ,
		--@col289 ,
		--@col290 ,
		--@col291 ,
		--@col292 ,
		--@col293 ,
		--@col294 ,
		--@col295 ,
		--@col296 ,
		--@col297 ,
		--@col298 ,
		--@col299 ,
		--@col300 ,
		--@col301 ,
		--@col302 ,
		--@col303 ,
		--@col304 ,
		--@col305 ,
		--@col306 ,
		--@col307 ,
		--@col308 ,
		--@col309 ,
		--@col310 ,
		--@col311 ,
		--@col312 ,
		--@col313 ,
		--@col314 ,
		--@col315 ,
		--@col316 ,
		--@col317 ,
		--@col318 ,
		--@col319 ,
		--@col320 ,
		--@col321 ,
		--@col322 ,
		--@col323 ,
		--@col324 ,
		--@col325 ,
		--@col326 ,
		--@col327 ,
		--@col328 ,
		--@col329 ,
		--@col330 ,
		--@col331 ,
		--@col332 ,
		--@col333 ,
		--@col334 ,
		--@col335 ,
		--@col336 ,
		--@col337 ,
		--@col338 ,
		--@col339 ,
		--@col340 ,
		--@col341 ,
		--@col342 ,
		--@col343 ,
		--@col344 ,
		--@col345 ,
		--@col346 ,
		--@col347 ,
		--@col348 ,
		--@col349 ,
		--@col350 ,
		--@col351 ,
		--@col352 ,
		--@col353 ,
		--@col354 ,
		--@col355 ,
		--@col356 ,
		--@col357 ,
		--@col358 ,
		--@col359 ,
		--@col360 ,
		--@col361 ,
		--@col362 ,
		--@col363 ,
		--@col364 ,
		--@col365 ,
		--@col366 ,
		--@col367 ,
		--@col368 ,
		--@col369 ,
		--@col370 ,
		--@col371 ,
		--@col372 ,
		--@col373 ,
		--@col374 ,
		--@col375 ,
		--@col376 ,
		--@col377 ,
		--@col378 ,
		--@col379 ,
		--@col380 ,
		--@col381 ,
		--@col382 ,
		--@col383 ,
		--@col384 ,
		--@col385 ,
		--@col386 ,
		--@col387 ,
		--@col388 ,
		--@col389 ,
		--@col390 ,
		--@col391 ,
		--@col392 ,
		--@col393 ,
		--@col394 ,
		--@col395 ,
		--@col396 ,
		--@col397 ,
		--@col398 ,
		--@col399 ,
		--@col400 ,
		--@col401 ,
		--@col402 ,
		--@col403 ,
		--@col404 ,
		--@col405 ,
		--@col406 ,
		--@col407 ,
		--@col408 ,
		--@col409 ,
		--@col410 ,
		--@col411 ,
		--@col412 ,
		--@col413 ,
		--@col414 ,
		--@col415 ,
		--@col416 ,
		--@col417 ,
		--@col418 ,
		--@col419 ,
		--@col420 ,
		--@col421 ,
		--@col422 ,
		--@col423 ,
		--@col424 ,
		--@col425 ,
		--@col426 ,
		--@col427 ,
		--@col428 ,
		--@col429 ,
		--@col430 ,
		--@col431 ,
		--@col432 ,
		--@col433 ,
		--@col434 ,
		--@col435 ,
		--@col436 ,
		--@col437 ,
		--@col438 ,
		--@col439 ,
		--@col440 ,
		--@col441 ,
		--@col442 ,
		--@col443 ,
		--@col444 ,
		--@col445 ,
		--@col446 ,
		--@col447 ,
		--@col448 ,
		--@col449 ,
		--@col450 ,
		--@col451 ,
		--@col452 ,
		--@col453 ,
		--@col454 ,
		--@col455 ,
		--@col456 ,
		--@col457 ,
		--@col458 ,
		--@col459 ,
		--@col460 ,
		--@col461 ,
		--@col462 ,
		--@col463 ,
		--@col464 ,
		--@col465 ,
		--@col466 ,
		--@col467 ,
		--@col468 ,
		--@col469 ,
		--@col470 ,
		--@col471 ,
		--@col472 ,
		--@col473 ,
		--@col474 ,
		--@col475 ,
		--@col476 ,
		--@col477 ,
		--@col478 ,
		--@col479 ,
		--@col480 ,
		--@col481 ,
		--@col482 ,
		--@col483 ,
		--@col484 ,
		--@col485 ,
		--@col486 ,
		--@col487 ,
		--@col488 ,
		--@col489 ,
		--@col490 ,
		--@col491 ,
		--@col492 ,
		--@col493 ,
		--@col494 ,
		--@col495 ,
		--@col496 ,
		--@col497 ,
		--@col498 ,
		--@col499 ,
		--@col500 ,
		--@col501 ,
		--@col502 ,
		--@col503 ,
		--@col504 ,
		--@col505 ,
		--@col506 ,
		--@col507 ,
		--@col508 ,
		--@col509 ,
		--@col510 ,
		--@col511 ,
		--@col512 ,
		--@col513 ,
		--@col514 ,
		--@col515 ,
		--@col516 ,
		--@col517 ,
		--@col518 ,
		--@col519 ,
		--@col520 ,
		--@col521 ,
		--@col522 ,
		--@col523 ,
		--@col524 ,
		--@col525 ,
		--@col526 ,
		--@col527 ,
		--@col528 ,
		--@col529 ,
		--@col530 ,
		--@col531 ,
		--@col532 ,
		--@col533 ,
		--@col534 ,
		--@col535 ,
		--@col536 ,
		--@col537 ,
		--@col538 ,
		--@col539 ,
		--@col540 ,
		--@col541 ,
		--@col542 ,
		--@col543 ,
		--@col544 ,
		--@col545 ,
		--@col546 ,
		--@col547 ,
		--@col548 ,
		--@col549 ,
		--@col550 ,
		--@col551 ,
		--@col552 ,
		--@col553 ,
		--@col554 ,
		--@col555 ,
		--@col556 ,
		--@col557 ,
		--@col558 ,
		--@col559 ,
		--@col560 ,
		--@col561 ,
		--@col562 ,
		--@col563 ,
		--@col564 ,
		--@col565 ,
		--@col566 ,
		--@col567 ,
		--@col568 ,
		--@col569 ,
		--@col570 ,
		--@col571 ,
		--@col572 ,
		--@col573 ,
		--@col574 ,
		--@col575 ,
		--@col576 ,
		--@col577 ,
		--@col578 ,
		--@col579 ,
		--@col580 ,
		--@col581 ,
		--@col582 ,
		--@col583 ,
		--@col584 ,
		--@col585 ,
		--@col586 ,
		--@col587 ,
		--@col588 ,
		--@col589 ,
		--@col590 ,
		--@col591 ,
		--@col592 ,
		--@col593 ,
		--@col594 ,
		--@col595 ,
		--@col596 ,
		--@col597 ,
		--@col598 ,
		--@col599 ,
		--@col600 ,
		--@col601 ,
		--@col602 ,
		--@col603 ,
		--@col604 ,
		--@col605 ,
		--@col606 ,
		--@col607 ,
		--@col608 ,
		--@col609 ,
		--@col610 ,
		--@col611 ,
		--@col612 ,
		--@col613 ,
		--@col614 ,
		--@col615 ,
		--@col616 ,
		--@col617 ,
		--@col618 ,
		--@col619 ,
		--@col620 ,
		--@col621 ,
		--@col622 ,
		--@col623 ,
		--@col624 ,
		--@col625 ,
		--@col626 ,
		--@col627 ,
		--@col628 ,
		--@col629 ,
		--@col630 ,
		--@col631 ,
		--@col632 ,
		--@col633 ,
		--@col634 ,
		--@col635 ,
		--@col636 ,
		--@col637 ,
		--@col638 ,
		--@col639 ,
		--@col640 ,
		--@col641 ,
		--@col642 ,
		--@col643 ,
		--@col644 ,
		--@col645 ,
		--@col646 ,
		--@col647 ,
		--@col648 ,
		--@col649 ,
		--@col650 ,
		--@col651 ,
		--@col652 ,
		--@col653 ,
		--@col654 ,
		--@col655 ,
		--@col656 ,
		--@col657 ,
		--@col658 ,
		--@col659 ,
		--@col660 ,
		--@col661 ,
		--@col662 ,
		--@col663 
END
GO
RAISERROR ('-- update NSP_InsertEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertEventChannelValue] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_InsertEventChannelValue] (
	@Timestamp DATETIME2(3)
	, @TimestampEndTime DATETIME2(3) = NULL
	, @UnitId INT
	, @ChannelId INT
	, @Value bit
	)
AS
/******************************************************************************
** Name:			NSP_InsertEventChannelValue
** Description:	Inserts a new event-based channel value for the SLT fleet
** Call frequency: Called by the telemetry service when the event happens. 
**				 The event is considered started if a start time (timeStamp) only is given.
**				 The event is considered finished when a end date is given.
**				 
** Parameters:	 @timestamp the timestamp when the event happens
**				 @timestamp the timestamp when the event happens
**				 @UnitId the Unit ID
**				 @ChannelId the channel ID
******************************************************************************/
BEGIN
	DECLARE @SqlStr varchar(max)

	-- Insert a record into EventChannelValue
	INSERT INTO dbo.EventChannelValue (TimeStamp, TimestampEndTime, UnitID, ChannelId, Value)
	VALUES (@timestamp, @TimestampEndTime, @UnitId, @ChannelId, @Value)

	IF not @TimestampEndTime is null
		SET @timestamp = @TimestampEndTime-- for the latest value I'm expect that TimestampEndTime will always have the latest datetime

	-- Update EventChannelLatestValue
	IF EXISTS (SELECT top 1 1 FROM dbo.EventChannelLatestValue WHERE UnitID = @UnitId)
		SET @SqlStr = '
			UPDATE dbo.EventChannelLatestValue 
			SET TimeLastUpdated = ''' + CAST(@timestamp AS VARCHAR(25)) + ''',
				col' + CAST(@ChannelId AS VARCHAR(5)) + ' = ' + CAST(@value AS VARCHAR(5)) + ' 
			WHERE UnitID = ' + CAST(@UnitId AS VARCHAR(5)) + '
		'
	ELSE
		SET @SqlStr = '
			INSERT INTO dbo.EventChannelLatestValue(TimeLastUpdated, UnitID, col' + CAST(@ChannelId AS VARCHAR(5)) + ') 
			VALUES (''' + CAST(@timestamp AS VARCHAR(25)) + ''', ' + CAST(@UnitId AS VARCHAR(5)) + ', ' + CAST(@value AS VARCHAR(5)) + ')
		'
	EXEC(@SqlStr)
END


GO
RAISERROR ('-- update NSP_InsertFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertFault] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_InsertFault]
	@FaultCode varchar(100)
	, @TimeCreate datetime
	, @TimeEnd datetime = NULL
	, @FaultUnitID int
	, @Context varchar(50)
	, @IsDelayed bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @rowcount int
	DECLARE @faultMetaID int
	DECLARE @faultMetaRecovery varchar(100) 
	DECLARE @faultID int 
	DECLARE @faultLocationID int 
	DECLARE @faultLat decimal(9,6)
	DECLARE @faultLng decimal(9,6)

	IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
	BEGIN
		SELECT TOP 1
			@faultMetaID = ID 
			, @faultMetaRecovery = RecoveryProcessPath
		FROM dbo.FaultMeta 
		WHERE FaultCode = @FaultCode
	END
	ELSE
	BEGIN
		RAISERROR ('FaultCode does not exist',1,1)
		RETURN -4
	END

	-- check if fault is already inserted
	IF EXISTS (
		SELECT 1 
		FROM dbo.Fault 
		WHERE CreateTime = @TimeCreate 
			AND FaultMetaID = @faultMetaID
			AND FaultUnitID = @FaultUnitID
	)
	BEGIN
		RAISERROR ('Duplicate Fault',1,1)
		RETURN -1
	END	 
	
	-- insert into Fault table

	-- get DECLARE @faultLocationID int 
	SELECT TOP 1
		@faultLat = Col1
		, @faultLng = Col2
		, @faultLocationID = l.ID
		FROM dbo.ChannelValue
		LEFT JOIN dbo.Location l ON l.ID = (
			SELECT TOP 1 LocationID 
			FROM dbo.LocationArea 
			WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
				AND Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
		WHERE UnitID = @FaultUnitID
			AND TimeStamp <= @TimeCreate
			AND TimeStamp > DATEADD(s, -30, @TimeCreate)
		ORDER BY TimeStamp DESC
	
	BEGIN TRAN

		DECLARE @headcode varchar(10)
		DECLARE @setcode varchar(10)			
			
		SELECT 
			@headcode = Headcode
			, @setcode = Setcode
		FROM dbo.FleetStatus
		WHERE 
			UnitID = @FaultUnitID
			AND @TimeCreate >= ValidFrom 
			AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- in case there is a time difference between app and db server
			
			
		;WITH CteNonEmptyRecord AS
		(
			SELECT 1 AS ID
		)
		INSERT INTO dbo.[Fault]
			([CreateTime]
			,[HeadCode]
			,[SetCode]
			,[LocationID]
			,[IsCurrent]
			,[EndTime]
			,[Latitude]
			,[Longitude]
			,[FaultMetaID]
			,[PrimaryUnitID]
			,[FaultUnitID]
			,[IsDelayed]
			,[RecoveryStatus])
		SELECT
			@TimeCreate		 --[CreateTime]
			,@headcode
			,@setCode			--[SetCode]
			,@faultLocationID	--[LocationID]
			,1				 --[IsCurrent]
			,@TimeEnd			--[EndTime]
			,@faultLat		 --[Latitude]
			,@faultLng		 --[Longitude]
			,@faultMetaID		--[FaultMetaID]
			,@FaultUnitID		--PrimaryUnitID
			,@FaultUnitID
			,ISNULL(@IsDelayed, 0)
			,RecoveryStatus = 
				CASE
					WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
					ELSE NULL
				END

		SELECT
			@rowcount = @@ROWCOUNT
			, @faultID = @@IDENTITY
		
	IF @rowcount <> 1
	BEGIN
		ROLLBACK
		RAISERROR ('Error when inserting into Fault table',1,1)
		RETURN -2
	END 
	ELSE
	BEGIN
		COMMIT
	END
	
	--insert Channel data
	INSERT INTO dbo.[FaultChannelValue]
	(
			[ID]
		,[FaultID]
		,[FaultUnit]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4]
		 --, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			--[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			--[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			--[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			--[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			--[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			--[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			--[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			--[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			--[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			--[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			--[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			--[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			--[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			--[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			--[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			--[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663]
		)
		SELECT
		[ID]
		,@faultID
		,CASE UnitID
			WHEN @FaultUnitID THEN 1
			ELSE 0
		END -- [FaultVehicle]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4]
		-- , [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
		--[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
		--[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
		--[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
		--[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
		--[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
		--[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
		--[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
		--[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
		--[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
		--[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
		--[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
		--[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
		--[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
		--[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
		--[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
		--[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663]

	FROM dbo.ChannelValue 
	WHERE UnitID = @FaultUnitID
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


	-- returning faultID
	RETURN @faultID
	
END


GO
RAISERROR ('-- update NSP_InsertFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertFaultCategory] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultCategory](
	@Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	
	DECLARE @ID int = ISNULL((
		SELECT MAX(ID) + 1
		FROM dbo.FaultCategory		
	), 1)
	
	IF @ID IS NULL
		RAISERROR('Error when looking for next ID', 13, 1); 
		
	BEGIN TRAN
	BEGIN TRY 
		
		SET IDENTITY_INSERT dbo.FaultCategory ON;
		
		INSERT dbo.FaultCategory(
			ID
			, Category
			, ReportingOnly
		)
		SELECT 
			@ID
			, @Category
			, @ReportingOnly;

		SET IDENTITY_INSERT dbo.FaultCategory OFF;

		COMMIT TRAN

		SELECT @ID AS InsertedID
			
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
		
END
GO
RAISERROR ('-- update NSP_InsertFaultInterface', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultInterface' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertFaultInterface] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_InsertFaultInterface
**	Description:	Creates a fault. Inserts data into Fault and 
**					FaultChannelValue tables
**	Call frequency:	Called by fault engine
**	Parameters:		@FaultCode varchar(100)
**					@TimeCreate datetime
**					@TimeEnd datetime = NULL
**					@IsDelayed bit = 0
**	Return values:	ID of Fault inserted, else
**					-1	Duplicate Fault
**					-2	Error when inserting into Fault table
**					-3	There is a Live fault overlapping with the fault
**					-4	FaultCode does not exist
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_InsertFaultInterface]
	@FaultCode varchar(100)
	, @TimeCreate datetime
	, @TimeEnd datetime = NULL
	, @FaultUnitNumber varchar(16)
	, @IsDelayed bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @rowcount int
	DECLARE @faultMetaID int
	DECLARE @faultMetaRecovery varchar(100) 
	DECLARE @faultID int 
	DECLARE @faultLocationID int 
	DECLARE @faultLat decimal(9,6)
	DECLARE @faultLng decimal(9,6)

	DECLARE @FaultUnitID int
	IF (SELECT COUNT(*) FROM dbo.Unit WHERE UnitNumber = @FaultUnitNumber) = 1
	BEGIN
		SELECT
			@FaultUnitID = ID 
		FROM dbo.Unit 
		WHERE UnitNumber = @FaultUnitNumber
	END
	ELSE
	BEGIN
		RAISERROR ('UnitNumber %s does not exist',1,1, @FaultUnitNumber)
		RETURN -5
	END


	IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
	BEGIN
		SELECT
			@faultMetaID = ID 
			, @faultMetaRecovery = RecoveryProcessPath
		FROM dbo.FaultMeta 
		WHERE FaultCode = @FaultCode
	END
	ELSE
	BEGIN
		RAISERROR ('FaultCode does not exist',1,1)
		RETURN -4
	END

	-- check if fault is already inserted
	IF EXISTS (
		SELECT TOP 1 1 
		FROM dbo.Fault  
		WHERE CreateTime = @TimeCreate 
			AND FaultMetaID = @faultMetaID
			AND FaultUnitID = @FaultUnitID
	)
	BEGIN
		RAISERROR ('Duplicate Fault',1,1)
		RETURN -1
	END	 
	
	-- insert into Fault table

	-- get DECLARE @faultLocationID int 
	SELECT TOP 1
		@faultLat = Col1
		, @faultLng = Col2
		, @faultLocationID = l.ID
		FROM dbo.ChannelValue
		LEFT JOIN dbo.Location l ON l.ID = (
			SELECT TOP 1 LocationID 
			FROM dbo.LocationArea 
			WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
				AND Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
		WHERE UnitID = @FaultUnitID
			AND TimeStamp <= @TimeCreate
			AND TimeStamp > DATEADD(s, -30, @TimeCreate)
		ORDER BY TimeStamp DESC
	
	BEGIN TRAN

		DECLARE @headcode varchar(10)
		DECLARE @setcode varchar(10)			
			
		SELECT 
			@headcode = Headcode
			, @setcode = Setcode
		FROM dbo.FleetStatus
		WHERE 
			UnitID = @FaultUnitID
			AND @TimeCreate >= ValidFrom 
			AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- in case there is a time difference between app and db server
			
			
		;WITH CteNonEmptyRecord AS
		(
			SELECT 1 AS ID
		)
		INSERT INTO dbo.[Fault]
			([CreateTime]
			,[HeadCode]
			,[SetCode]
			,[LocationID]
			,[IsCurrent]
			,[EndTime]
			,[Latitude]
			,[Longitude]
			,[FaultMetaID]
			,[PrimaryUnitID]
			,[FaultUnitID]
			,[IsDelayed]
			,[RecoveryStatus])
		SELECT
			@TimeCreate		 --[CreateTime]
			,@headcode
			,@setCode			--[SetCode]
			,@faultLocationID	--[LocationID]
			,1				 --[IsCurrent]
			,@TimeEnd			--[EndTime]
			,@faultLat		 --[Latitude]
			,@faultLng		 --[Longitude]
			,@faultMetaID		--[FaultMetaID]
			,@FaultUnitID		--PrimaryUnitID
			,@FaultUnitID
			,ISNULL(@IsDelayed, 0)
			,RecoveryStatus = CASE
				WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
				ELSE NULL
			END

		SELECT
			@rowcount = @@ROWCOUNT
			, @faultID = @@IDENTITY
		
	IF @rowcount <> 1
	BEGIN
		ROLLBACK
		RAISERROR ('Error when inserting into Fault table',1,1)
		RETURN -2
	END 
	ELSE
	BEGIN
		COMMIT
	END
	
	--insert Channel data
	INSERT INTO dbo.[FaultChannelValue]
	(
			[ID]
		,[FaultID]
		,[FaultUnit]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4]
		 --, [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	)
	SELECT
		[ID]
		,@faultID
		,CASE UnitID
			WHEN @FaultUnitID THEN 1
			ELSE 0
		END -- [FaultVehicle]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4]
		-- , [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		--[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		--[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		--[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		--[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		--[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		--[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		--[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		--[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		--[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		--[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		--[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		--[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		--[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		--[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		--[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		--[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		--[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		--[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		--[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		--[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		--[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		--[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM dbo.ChannelValue 
	WHERE UnitID = @FaultUnitID
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


	-- returning faultID
	RETURN @faultID
	
END

GO
RAISERROR ('-- update NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
	@Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT dbo.FaultMeta(
		Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
	)
	SELECT 
		Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID
		
	SELECT SCOPE_IDENTITY() AS InsertedID

END

GO
RAISERROR ('-- update NSP_InsertLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertLocation] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_InsertLocation]
	@Tiploc varchar(20)
	, @LocationName varchar(100)
	, @LocationCode varchar(10)
	, @Lat decimal(9,6) = NULL
	, @Lng decimal(9,6) = NULL
	, @IsDepot bit = NULL
	, @IsStation bit = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM dbo.Location WHERE Tiploc = @Tiploc)
		UPDATE dbo.Location SET
			[LocationCode] = @LocationCode
			,[LocationName] = @LocationName
			,[Tiploc] = @Tiploc
			,[Lat] = @Lat
			,[Lng] = @Lng
			,[Type] = CASE
				WHEN @IsDepot = 1 THEN 'D'
				WHEN @IsStation = 1 THEN 'S'
				ELSE 'T'
			END
		WHERE Tiploc = @Tiploc
		
	ELSE
		INSERT INTO [dbo].[Location]
			([LocationCode]
			,[LocationName]
			,[Tiploc]
			,[Lat]
			,[Lng]
			,[Type])
		SELECT
			@LocationCode
			,@LocationName
			,@Tiploc
			,@Lat
			,@Lng
			,CASE
				WHEN @IsDepot = 1 THEN 'D'
				WHEN @IsStation = 1 THEN 'S'
				ELSE 'T'
			END 

END
GO
RAISERROR ('-- update NSP_InsertLocationArea', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertLocationArea' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertLocationArea] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_InsertLocationArea]
(
	@Tiploc varchar(20)
	, @MinLat decimal(9,6)
	, @MaxLat decimal(9,6)
	, @MinLng decimal(9,6)
	, @MaxLng decimal(9,6)
	, @Priority int
	, @Type char(1)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[LocationArea]
		([LocationID]
		,[MinLatitude]
		,[MaxLatitude]
		,[MinLongitude]
		,[MaxLongitude]
		,[Priority]
		,[Type])
	SELECT
		(SELECT ID FROM dbo.Location WHERE Tiploc = @Tiploc)
		, @MinLat
		, @MaxLat
		, @MinLng
		, @MaxLng
		, @Priority
		, @Type

END


GO
RAISERROR ('-- update NSP_InsertTmsLog', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertTmsLog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_InsertTmsLog] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_InsertTmsLog
**	Description:	Inserts TMS Log record read by interface
**	Call frequency:	Once for every line in imported TMS Log
**	Parameters:		
**	Return values:	0 if successful, else an error is raised:
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/
ALTER PROCEDURE [dbo].[NSP_InsertTmsLog]
	@VehicleNo varchar(10)
	, @FaultDate date
	, @FaultTime time
	, @CarNo varchar(10)
	, @FaultNoMostSignificantByte int
	, @FaultNoLessSignificantByte int
	, @TsnCplMostSignificantByte int
	, @TsnCplLessSignificantByte int
	, @CabByte int
	, @McsByte int
	, @DirectionByte int
	, @Voltage bit
	, @SpeedSet bit
	, @BrakeCont bit
	, @EmerBrakeIsolated bit
	, @EmerBrakeState bit
	, @Stabled bit
	, @Mlp bit
	, @BrakePr bit
	, @Battery bit
	, @Loadshed bit
	, @TractShoreSup bit
	, @AuxShoreSup bit
	, @CarBmCoupled bit
	, @CarBjCoupled bit
	, @Tmcc1 bit
	, @Speed int
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @vehicleNumber varchar(10) = REPLACE(@VehicleNo, 'v','');
	DECLARE @faultNo int		= @FaultNoMostSignificantByte + @FaultNoLessSignificantByte;
	DECLARE @tsnCpl int			= @TsnCplMostSignificantByte + @TsnCplLessSignificantByte;
	DECLARE @cab int			= @CabByte / 16;
	DECLARE @mcs int			= (@McsByte % 16) / 4;
	DECLARE @direction int		= @DirectionByte % 4;
	DECLARE @emerBrake int;
	DECLARE @faultMetaID int;
	DECLARE @faultUnitID int;
	DECLARE @faultID int;
	DECLARE @createTime datetime2(3) = CONVERT(varchar(10), @faultDate, 121) + ' ' + CONVERT(varchar(13), @faultTime);
	
	IF(@EmerBrakeIsolated = 1)
		SET @emerBrake = 2;
	ELSE
		SET @emerBrake = @EmerBrakeState;

	-- Add record in TmsFaultLookup, if corresponding FaultNo does not exist
	IF NOT EXISTS (SELECT 1 FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo) 
	BEGIN

		INSERT INTO [dbo].[TmsFaultLookup]
			([FaultNo]
			,[Description]
			,[FixingInstr]
			,[SwRef]
			,[TmsCategoryID]
			,[TmsFunctionID])
		SELECT
			@faultNo
			,'Unknown FaultNo: ' + CONVERT(varchar(100), @faultNo)
			,'?'
			,'?'
			,0
			,0

	END
	
	-- get @faultMetaID
	SET @faultMetaID = (
		SELECT ID 
		FROM dbo.FaultMeta
		WHERE FaultCode = 'F_TMS_' + CONVERT(varchar(10), @faultNo)
	)

	IF @faultMetaID IS NULL
	BEGIN

		INSERT INTO dbo.FaultMeta(
			FaultCode	
			, Description	
			, Summary	
			, CategoryID	
			, FaultTypeID
			, RecoveryProcessPath)
		SELECT
			'F_TMS_' + CONVERT(varchar(10), @faultNo)
			, (SELECT top 1 Description FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT top 1 Description FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT top 1 ID FROM dbo.FaultCategory WHERE Category = 'TMS')
			, (SELECT top 1 ID FROM dbo.FaultType WHERE Name = 'Fault')
			, NULL
		
		SET @faultMetaID = SCOPE_IDENTITY()

	END
	
	-- get @faultVehicleID -- first vehicle on the unit
	SET @faultUnitID = (
		SELECT UnitID 
		FROM dbo.Vehicle 
		WHERE VehicleNumber = @vehicleNumber
	)
	
	IF @faultUnitID IS NOT NULL
		AND @faultMetaID IS NOT NULL
		AND @createTime IS NOT NULL
		AND NOT EXISTS (
			SELECT top 1 1
			FROM dbo.Fault
			WHERE FaultUnitID = @faultUnitID
				AND FaultMetaID = @faultMetaID
				AND CreateTime = CONVERT(datetime, @createTime)
			)
	BEGIN
		
		INSERT INTO dbo.[Fault] (
			CreateTime
			,HeadCode
			,SetCode
			,LocationID
			,IsCurrent
			,EndTime
			,Latitude
			,Longitude
			,FaultMetaID
			,FaultUnitID
			,IsDelayed)
		SELECT
			CreateTime	= @createTime
			,HeadCode	= NULL
			,SetCode	= NULL
			,LocationID	= NULL
			,IsCurrent	= 0
			,EndTime	= DATEADD(n, 1, @createTime)
			,Latitude	= NULL
			,Longitude	= NULL
			,FaultMetaID	= @faultMetaID
			,FaultUnitID	= @faultUnitID
			,IsDelayed		= 0
			
		SET @faultID = SCOPE_IDENTITY()
		
	END
	
	INSERT INTO dbo.TmsFault (
		FaultID
		, VehicleNumber
		, FaultDateTime
		, CarNo
		, FaultNo
		, TsnCplId
		, CapId
		, McsId
		, DirectionId
		, VoltageId
		, SpeedSetId
		, BrakeContId
		, EmerBrakeId
		, StabledId
		, MlpId
		, BrakePrId
		, BatteryId
		, LoadshedId
		, TracShoreSupId
		, AuxShoreSupId
		, CarBmCoupledId
		, CarBjCoupledId
		, Tmcc1Id
		, Speed)
	VALUES (
		@faultID
		, @vehicleNumber
		, @createTime
		, @CarNo
		, @faultNo
		, @tsnCpl
		, @cab
		, @mcs
		, @direction
		, @voltage
		, @speedSet
		, @brakeCont
		, @emerBrake
		, @stabled
		, @mlp
		, @brakePr
		, @battery
		, @loadshed
		, @tractShoreSup
		, @auxShoreSup
		, @carBmCoupled
		, @carBjCoupled
		, @tmcc1
		, @speed
		);

END


GO
RAISERROR ('-- update NSP_OaArrivalDeparture', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaArrivalDeparture' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaArrivalDeparture] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaArrivalDeparture
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaArrivalDeparture]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	DECLARE @journeyNumber int
	DECLARE @extendedDwellLimit int	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int 	-- in seconds
	DECLARE @lateDepartureLimit int -- in seconds
	
	-- Read Dwell definition limits 
	SET @extendedDwellLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_extendeddwell')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_extendeddwell')
		ELSE NULL
	END	
	
	SET @earlyArrivalLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_earlyarrival')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_earlyarrival')
		ELSE NULL
	END	
	
	SET @lateDepartureLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_latedeparture')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_latedeparture')
		ELSE NULL
	END
	
	-- If not defined set up default
	IF @extendedDwellLimit IS NULL 
		SET @extendedDwellLimit = 60
		
	IF @earlyArrivalLimit IS NULL 
		SET @earlyArrivalLimit = 60
		
	IF @lateDepartureLimit IS NULL 
		SET @lateDepartureLimit = 60
	
	SET @EndDate = DATEADD(d, 1, @EndDate)

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM dbo.VW_TrainPassageValid tp 		
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
	)
		
	---------------------------------------------------------------------------
	-- header information
	---------------------------------------------------------------------------
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM dbo.Journey 
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all section points for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CteSectionPoint') IS NOT NULL
		DROP TABLE #CteSectionPoint
	
	SELECT
		SectionPointID
		, TrainPassageID
		, ActualArrivalTime		= DATEDIFF(s, '00:00:00.000', CAST(ActualArrivalDatetime AS time))
		, ActualDepartureTime	= DATEDIFF(s, '00:00:00.000', CAST(ActualDepartureDatetime AS time))
		, ActualArrivalDatetime
		, ActualDepartureDatetime
		, StartDatetime			= CAST(StartDatetime AS date)
		, SectionPointType
	INTO #CteSectionPoint
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSectionPoint ON TrainPassageID = tp.ID
	INNER JOIN dbo.SectionPoint ON SectionPoint.ID = SectionPointID AND tp.JourneyID = SectionPoint.JourneyID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate


	---------------------------------------------------------------------------
	-- to avoid divide by 0 problem in next query use NULL value for @journeyNumber if it equals 0
	---------------------------------------------------------------------------
	IF @journeyNumber = 0 
		SET @journeyNumber = NULL

	---------------------------------------------------------------------------
	-- Main report dataset
	-- display Arrival Date for (S - Station, E - End location)
	-- display Departure Date for (S - Station, B - Start location, T - Timing Points)
	---------------------------------------------------------------------------
	SELECT
		Loc						= l.Tiploc 
		, BookedArrive			= ScheduledArrival 
		, BookedDeparture		= ISNULL(ScheduledDeparture, ScheduledPass)
		, RecoveryTime

		, ActualMinArrive		= CASE 
			WHEN SectionPointType IN ('S', 'E')			
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMinDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID)
			END 
		
		, ActualMaxArrive		= CASE
			WHEN SectionPointType IN ('S', 'E') 
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMaxDeparture	= CASE 
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMeanArrive		= CASE
			WHEN SectionPointType IN ('S', 'E')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, ActualMeanDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, BookedDwell			= dbo.FN_ConvertSecondToTime(DATEDIFF(s, ScheduledArrival, ScheduledDeparture))
		, MinDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MIN(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
		, MaxDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MAX(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
	
		-- Percentages 
		, [% Extended Dwell] = CASE 
			WHEN SectionPointType IN ('S') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) >= DATEDIFF(s, ScheduledArrival, ScheduledDeparture) + @extendedDwellLimit
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Early Arrival] = CASE 
			WHEN SectionPointType IN ('S', 'E') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Late Departure] = CASE 
				WHEN SectionPointType IN ('S', 'B') 
					THEN CAST(
						(SELECT COUNT(1)
						FROM #CteSectionPoint
						WHERE SectionPointID = SectionPoint.ID
							AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
						) / (@journeyNumber * 0.01)
					AS decimal(9, 2))
			END
		
		-- Dates for Min/Max passages
		, MinArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime ASC) END
		, MaxArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime DESC) END
		, MinDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime ASC) END
		, MaxDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime DESC) END
		, MinDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) ASC) END
		, MaxDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) DESC) END

		, IsStation			= CASE 
				WHEN SectionPointType IN ('B', 'S', 'E')
					THEN CAST(1 AS bit)
				ELSE CAST(0 AS bit)
			END
	FROM dbo.SectionPoint
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SectionPoint.ID

END


GO
RAISERROR ('-- update NSP_OaDelayDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaDelayDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaDelayDetail] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaDelayDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaDelayDetail]
(
	@TrainPassageID int
	, @SegmentID int
	, @SectionID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	IF @SectionID <> 0
		-- select data for Section
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM dbo.TrainPassageSection
		INNER JOIN dbo.TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN dbo.DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SectionID = @SectionID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	 
	ELSE
		-- select data for Segment
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM dbo.TrainPassageSection
		INNER JOIN dbo.TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN dbo.DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SegmentID = @SegmentID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	
END


GO
RAISERROR ('-- update NSP_OaDelaySummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaDelaySummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaDelaySummary] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaDelaySummary
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaDelaySummary]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @ServiceGroupID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT 
		DelayReasonID
		, TotalDuration		= CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
	GROUP BY 
		DelayReasonID
		, Name
		, ChartColor	
	ORDER BY TotalDuration DESC
	
END


GO
RAISERROR ('-- update NSP_OaDelaySummaryDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaDelaySummaryDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaDelaySummaryDetail] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaDelaySummaryDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/


ALTER PROCEDURE [dbo].[NSP_OaDelaySummaryDetail]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @DelayReasonID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
		JourneyID			= tp.JourneyID
		, TrainPassageID	= tp.ID
		, Headcode 
		, DelayDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, DelayName			= dr.Name
		, TiplocStart		= sl.Tiploc
		, TiplocEnd			= el.Tiploc
		, JourneyDate		= CAST(tps.ActualStartDatetime AS date)
		, LeadingVehicle	= v.VehicleNumber
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.Location sl ON s.StartLocationID = sl.ID
	INNER JOIN dbo.Location el ON s.EndLocationID = el.ID
	INNER JOIN dbo.TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	LEFT JOIN dbo.Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
		AND DelayReasonID = @DelayReasonID
	ORDER BY DelayDuration DESC
		, tps.ActualStartDatetime
		, Headcode

END

GO
RAISERROR ('-- update NSP_OaGetHeadcodeList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaGetHeadcodeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaGetHeadcodeList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaGetHeadcodeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaGetHeadcodeList]
(
	@HeadcodeString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @currentDate date = GETDATE()

	SELECT
		JourneyID		= Journey.ID
		, Headcode
		, HeadcodeDesc	= HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, ValidFrom
		, ValidTo
		, Period		= 
			CASE
				WHEN @currentDate BETWEEN ValidFrom AND ValidTo 
					THEN 'Current'
				ELSE 'Previous'
			END
	FROM dbo.Journey
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Headcode LIKE '%' + @HeadcodeString + '%'
		AND ValidTo > DATEADD(YYYY, -1, GETDATE())
		AND StpType = 'P'
	ORDER BY 
		ValidTo DESC
		, ValidFrom ASC

END

GO
RAISERROR ('-- update NSP_OaGetLocationList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaGetLocationList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaGetLocationList] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaGetLocationList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaGetLocationList]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
	FROM dbo.Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END


GO
RAISERROR ('-- update NSP_OaGetSegmentList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaGetSegmentList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaGetSegmentList] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaGetSegmentList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaGetSegmentList]
(
	@Keyword varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT 
		StartLocationID
		, EndLocationID
		, SegmentName		= sl.Tiploc + ' - ' + el.Tiploc
		, SegmentLongName	= sl.LocationName + ' - ' + el.LocationName
	FROM dbo.Segment s
	INNER JOIN dbo.Location sl ON s.StartLocationID = sl.ID
	INNER JOIN dbo.Location el ON s.EndLocationID = el.ID
	WHERE 
		sl.Tiploc + ' - ' + el.Tiploc LIKE '%' + @Keyword + '%'
		OR sl.LocationName + ' - ' + el.LocationName LIKE '%' + @Keyword + '%'
	
END


GO
RAISERROR ('-- update NSP_OaIndividualJourney', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaIndividualJourney' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaIndividualJourney] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaIndividualJourney
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaIndividualJourney]
(
	@TrainPassageID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	DECLARE @JourneyID int = (SELECT JourneyID FROM TrainPassage WHERE ID = @TrainPassageID)

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------

	SELECT 
		CASE StpType
			WHEN 'O' THEN HeadcodeDesc + ' STP'
			ELSE HeadcodeDesc
		END AS Headcode
		, StartDatetime	
		, ISNULL(va.VehicleNumber, '') + ' - ' + ISNULL(vb.VehicleNumber, '') AS VehicleNumber
		, AnalyseDatetime
	FROM dbo.TrainPassage tp 
	INNER JOIN dbo.Journey ON Journey.ID = JourneyID
	LEFT JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	LEFT JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE tp.ID = @TrainPassageID
	
	---------------------------------------------------------------------------
	-- Load section data into a temporary table
	---------------------------------------------------------------------------
	;WITH CteTrainPassageSection
	AS
	(
		SELECT
			TrainPassageSectionID	= ID 
			, TrainPassageSection.SectionID
			, ActualStartDatetime
			, ActualEndDatetime
			, ActualStartTime		= CAST(ActualStartDatetime AS Time(0)) 
			, ActualEndTime			= CAST(ActualEndDatetime AS Time(0))
			, ActualTime			= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
			, TotalAttributed		= (SELECT CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) FROM dbo.TrainPassageSectionDelayReason WHERE TrainPassageSectionID = TrainPassageSection.ID AND DelayDuration >= 1000)
		FROM dbo.TrainPassageSection
		WHERE TrainPassageID = @TrainPassageID
	)
	SELECT
		SegmentID				= s.SegmentID
		, SectionID				= s.ID
		, ID					= TrainPassageSectionID
		, StartLoc				= StartLocation.Tiploc
		, EndLoc				= EndLocation.Tiploc
		, StartTime				= CAST(s.StartTime AS datetime)
		, EndTime				= CAST(s.EndTime AS datetime)
		, BookedTime			= DATEDIFF(s, CAST(s.StartTime AS datetime), CAST(s.EndTime AS datetime))
		, ActualTime			= ActualTime
		, ActualStartDatetime
		, ActualEndDatetime
		, ScheduleDeviationSection = 
				ActualTime - 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) < - 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) + 86400 
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) > 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) - 86400 
					ELSE DATEDIFF(s, s.StartTime, s.EndTime)
				END
		, BookedArrivalTime		= s.EndTime
		, ActualArrivalTime		= tps.ActualEndTime
		, ScheduleDeviationCumulative = 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) < - 43200 -- -12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) + 86400 -- +24h
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) > 43200 -- 12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) - 86400 -- -24h
					ELSE DATEDIFF(s, s.EndTime, tps.ActualEndTime) 
				END
		, TotalAttributed		= TotalAttributed
		, RecoveryTime			= RecoveryTime
		, OptimalJourneyDate	= CONVERT(date, ir.StartDatetime)
		, OptimalTime			= ir.SectionTime
	INTO #SectionData
	FROM dbo.Section s
	LEFT JOIN dbo.Location AS StartLocation ON StartLocation.ID = s.StartLocationID
	LEFT JOIN dbo.Location AS EndLocation ON EndLocation.ID = s.EndLocationID
	LEFT JOIN CteTrainPassageSection tps ON SectionID = s.ID
	LEFT JOIN dbo.IdealRun ir ON 
		ir.JourneyID = s.JourneyID
		AND ir.SectionID = s.ID
		AND ir.ValidTo IS NULL
	WHERE s.JourneyID = @journeyID
	ORDER BY s.ID	

	---------------------------------------------------------------------------
	-- Main report dataset (calculate segment data, merge with sections, return)
	---------------------------------------------------------------------------
	
	SELECT
		SegmentID	
		, SectionID	
		, ID	
		, StartLoc	
		, EndLoc	
		, BookedTime					= dbo.FN_ConvertSecondToTime(BookedTime)
		, ActualTime					= dbo.FN_ConvertSecondToTime(ActualTime)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(ScheduleDeviationSection)
		, BookedArrivalTime				= CONVERT(char(8), BookedArrivalTime, 108)
		, ActualArrivalTime				= CONVERT(char(8), ActualArrivalTime, 108)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime(ScheduleDeviationCumulative)
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(TotalAttributed)
		, RecoveryTime					= 			
			CASE RecoveryTime
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(RecoveryTime) 
			END
		, OptimalJourneyDate
		, OptimalTime					= dbo.FN_ConvertSecondToTime(OptimalTime)
	FROM #SectionData
	
	UNION ALL
	
	SELECT
		SegmentID	
		, SectionID						= 0
		, ID							= 0
		, StartLoc						= (SELECT TOP 1 StartLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
		, EndLoc						= (SELECT TOP 1 EndLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, BookedTime					= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ActualTime = dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
			- DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, BookedArrivalTime				= (SELECT TOP 1 CONVERT(char(8), BookedArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ActualArrivalTime				= (SELECT TOP 1 CONVERT(char(8), ActualArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime((SELECT TOP 1 ScheduleDeviationCumulative FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC))
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(SUM(TotalAttributed))
		, RecoveryTime					= 			
			CASE SUM(RecoveryTime)
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(SUM(RecoveryTime)) 
			END
		-- THIS IS NOT POPULATED: need more feedback how should it be calculated
		, OptimalJourneyDate			= NULL --MIN(OptimalJourneyDate)	
		, OptimalTime					= dbo.FN_ConvertSecondToTime(SUM(OptimalTime))
	FROM #SectionData sd1
	GROUP BY 		
		SegmentID
	ORDER BY 		
		SegmentID
		, SectionID


	---------------------------------------------------------------------------
	-- Return Delay Breakdown	
	---------------------------------------------------------------------------
	SELECT
		SegmentID			= tps.SegmentID 
		, SectionID			= tps.SectionID
		, LocationStart		= sl.Tiploc
		, LocationEnd		= el.Tiploc
		, TrainPassageSectionID
		, DelayReasonID
		, TotalDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM dbo.TrainPassageSectionDelayReason tpsdr
	INNER JOIN dbo.TrainPassageSection tps ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.DelayReason dr ON DelayReasonID = dr.ID
	LEFT JOIN dbo.Location sl ON sl.ID = s.StartLocationID
	LEFT JOIN dbo.Location el ON el.ID = s.EndLocationID
		WHERE TrainPassageID = @TrainPassageID
		AND DelayDuration >= 1000

END


GO
RAISERROR ('-- update NSP_OaJourneyProfile', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaJourneyProfile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaJourneyProfile] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaJourneyProfile
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaJourneyProfile] 
(
	@TrainPassageID int
	, @SectionID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Date datetime = (SELECT StartDatetime FROM dbo.TrainPassage WHERE ID = @TrainPassageID)
	DECLARE @JourneyID datetime = (SELECT JourneyID FROM dbo.TrainPassage WHERE ID = @TrainPassageID)

	DECLARE @gmtToBstTimeOffset smallint = dbo.FN_GetGmtToBstTimeDiff(CONVERT(varchar(10), @Date, 121) + ' 04:00:00.000')
	DECLARE @bstToGmtTimeOffset smallint = - @gmtToBstTimeOffset

	-- Sections - veritical markers are based on this dataset
	IF OBJECT_ID('tempdb..#Section') IS NOT NULL
		DROP TABLE #Section

	SELECT
		SectionID	= Section.ID
		, IsStation	= 
			CASE
				WHEN SectionPoint.SectionPointType IN ('B', 'S', 'E') 
					THEN 1
				ELSE 0
			END
		, StartLoc	= StartLocation.Tiploc 
		, EndLoc	= EndLocation.Tiploc
		-- if distance cannot be calulcated base on LocationSection table, will be calculated in straight line:
		, Distance	= ISNULL(Distance, dbo.FN_CalculateDistance (StartLocation.Lat, StartLocation.Lng, EndLocation.Lat, EndLocation.Lng, 0))
	INTO #Section
	FROM dbo.Section
	INNER JOIN dbo.SectionPoint ON SectionPoint.ID = Section.ID AND SectionPoint.JourneyID = Section.JourneyID
	LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE Section.JourneyID = @JourneyID
	ORDER BY Section.ID

	SELECT 
		SectionID
		, IsStation
		, StartLoc 
		, EndLoc
		, Distance
	FROM #Section
		

	-- Ideal Run data
	IF OBJECT_ID('tempdb..#IdealRunData') IS NOT NULL
		DROP TABLE #IdealRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col6 AS CIRSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #IdealRunData
	FROM dbo.Section
	INNER JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.IdealRun ON IdealRun.SectionID = Section.ID AND IdealRun.JourneyID = Section.JourneyID
	LEFT JOIN dbo.IdealRunChannelValue ON IdealRun.ID = IdealRunChannelValue.IdealRunID --ON VehicleID = LeadingVehicleID AND TimeStamp BETWEEN StartDatetime AND DATEADD(s, SectionTime, StartDatetime)
	WHERE Section.JourneyID = @JourneyID 
		AND IdealRun.ValidTo IS NULL
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)

	;WITH CteIdealRunData AS
	(
		SELECT
			SectionID	
			, SectionIDRecordID	
			, CIRSpeed	
			, col3	
			, col4	
			, StartLocationLat	
			, StartLocationLng
			, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
		FROM #IdealRunData 
		WHERE SectionIDRecordID = 1
		
		UNION ALL 
		
		SELECT
			ird.SectionID	
			, ird.SectionIDRecordID	
			, ird.CIRSpeed	
			, ird.col3	
			, ird.col4	
			, ird.StartLocationLat	
			, ird.StartLocationLng
			, CteIdealRunData.SectionDistance + dbo.FN_CalculateDistance (CteIdealRunData.col3, CteIdealRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
		FROM #IdealRunData ird
		INNER JOIN CteIdealRunData ON ird.SectionIDRecordID = CteIdealRunData.SectionIDRecordID + 1 AND ird.SectionID = CteIdealRunData.SectionID
	)
	SELECT 
		SectionID
		, CIRSpeed
		, SectionDistance
	FROM CteIdealRunData
	ORDER BY SectionID
		, SectionIDRecordID
	OPTION (MAXRECURSION 20000)
	
	--Actual Run Data
	--DECLARE @trainPassageID int = (SELECT TOP 1 ID FROM VW_TrainPassageValid WHERE JourneyID = @JourneyID AND CAST(StartDatetime AS date) = @Date)
	
	IF OBJECT_ID('tempdb..#ActualRunData') IS NOT NULL
	DROP TABLE #ActualRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col4 AS ActualSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #ActualRunData
	FROM dbo.Section
	INNER JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	INNER JOIN dbo.TrainPassageSection ON Section.JourneyID = TrainPassageSection.JourneyID AND Section.ID = SectionID
	LEFT JOIN dbo.VW_TrainPassageValid AS TrainPassage ON TrainPassage.ID = TrainPassageID
	LEFT JOIN dbo.Vehicle v ON v.ID = LeadingVehicleID
	LEFT JOIN dbo.ChannelValue cv ON cv.UnitID = v.UnitID AND TimeStamp BETWEEN DATEADD(hh, @bstToGmtTimeOffset, ActualStartDatetime) AND DATEADD(hh, @bstToGmtTimeOffset, ActualEndDatetime)

	WHERE TrainPassageID = @trainPassageID
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)
	
		
	;WITH CteActualRunData AS
		(
			SELECT
				SectionID	
				, SectionIDRecordID	
				, ActualSpeed	
				, col3	
				, col4	
				, StartLocationLat	
				, StartLocationLng
				, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
			FROM #ActualRunData 
			WHERE SectionIDRecordID = 1
			
			UNION ALL 
			
			SELECT
				ird.SectionID	
				, ird.SectionIDRecordID	
				, ird.ActualSpeed	
				, ird.col3	
				, ird.col4	
				, ird.StartLocationLat	
				, ird.StartLocationLng
				, CteActualRunData.SectionDistance + dbo.FN_CalculateDistance (CteActualRunData.col3, CteActualRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
			FROM #ActualRunData ird
			INNER JOIN CteActualRunData ON ird.SectionIDRecordID = CteActualRunData.SectionIDRecordID + 1 AND ird.SectionID = CteActualRunData.SectionID
		)
		SELECT 
			SectionID
			, ActualSpeed
			, SectionDistance
		FROM CteActualRunData
		ORDER BY SectionID
			, SectionIDRecordID
		OPTION (MAXRECURSION 20000)
					
END



GO
RAISERROR ('-- update NSP_OaJourneyTime', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaJourneyTime' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaJourneyTime] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_OaJourneyTime
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaJourneyTime]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @journeyNumber int
	
	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM dbo.VW_TrainPassageValid tp	
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	)
	

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------
	
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM dbo.Journey 
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all segments for @JourneyID in selected time ranges
	---------------------------------------------------------------------------	
	IF OBJECT_ID('tempdb..#TempSegment') IS NOT NULL
		DROP TABLE #TempSegment
		
	SELECT
		SectionID		= 0 --This is segment level data SecionID = 0 to allow grouping results
		, SegmentID
		, TrainPassageID
		, ActualTime	= SUM(DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)) 
		, StartDate		= CAST(StartDatetime AS date)
	INTO #TempSegment
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND TrainPassageSection.ActualStartDatetime IS NOT NULL
		AND TrainPassageSection.ActualEndDatetime IS NOT NULL
	GROUP BY
		SegmentID
		, TrainPassageID
		, StartDatetime
	-- to exclude segments not having Actual Times for all sections
	HAVING COUNT(*) = (
		SELECT COUNT(*) 
		FROM dbo.Section 
		WHERE JourneyID = @JourneyID 
			AND Section.SegmentID = TrainPassageSection.SegmentID
		)

	---------------------------------------------------------------------------
	-- create temporary table with all sections for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#TempSection') IS NOT NULL
		DROP TABLE #TempSection

	SELECT
		SectionID			= TrainPassageSection.SectionID
		, SegmentID
		, TrainPassageID
		, ActualTime		= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
		, StartDate			= CAST(tp.StartDatetime AS date)
	INTO #TempSection
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate

	
	---------------------------------------------------------------------------
	-- Data to be displayed in OpsAnalysis table
	---------------------------------------------------------------------------

	SELECT
		SegmentID			= Segment.ID
		, SectionID			= 0
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime) 
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
	FROM dbo.Segment
	LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID

	UNION ALL
	
	SELECT
		SegmentID
		, SectionID			= Section.ID
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime)
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime DESC)
	FROM dbo.Section
	LEFT JOIN dbo.Location AS StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location AS EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SegmentID, SectionID

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------
	;WITH CteChartData AS 
	(
		SELECT
			StartLoc		= StartLocation.Tiploc
			, EndLoc		= EndLocation.Tiploc 
			, Booked		=
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
						THEN DATEDIFF(s, StartTime, EndTime) + 86400 
					WHEN DATEDIFF(s, StartTime, EndTime) > 43200
						THEN DATEDIFF(s, StartTime, EndTime) - 86400 
					ELSE DATEDIFF(s, StartTime, EndTime)
				END
			, RecoveryTime 
			, IdealRunTime
			, ActualMin		= (SELECT MIN(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMax		= (SELECT MAX(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMean	= (SELECT AVG(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMinDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
			, ActualMaxDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
			, SegmentID		= Segment.ID
		FROM dbo.Segment
		LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
		LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
		WHERE JourneyID = @JourneyID
	)
	SELECT 
		StartLoc
		, EndLoc
		, OptimalDiff		= IdealRunTime - Booked
		, ActualMinDiff		= ActualMin - Booked
		, ActualMaxDiff		= ActualMax - Booked
		, ActualMeanDiff	= ActualMean - Booked
		, ActualMinDate
		, ActualMaxDate
	FROM CteChartData
	ORDER BY SegmentID ASC

END




GO
RAISERROR ('-- update NSP_OaJourneyTimeList', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaJourneyTimeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaJourneyTimeList] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaJourneyTimeList
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaJourneyTimeList]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @actualJourneyID int
	

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @actualJourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------

	SELECT
		TrainPassageID			= tp.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		--Scotrail:
		, SetFormation			= (SELECT UnitNumber FROM VW_Vehicle WHERE VW_Vehicle.ID = EndAVehicleID) + ' (' + ISNULL(va.VehicleNumber, '') + '/' + ISNULL(vb.VehicleNumber, '') + ')'
		--ECML:
		--, SetFormation			= ISNULL(tp.Setcode, '') + ' / ' + ISNULL(va.VehicleNumber, '') + ' / ' + ISNULL(vb.VehicleNumber, '') 
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) --/ 60 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) --/ 60
		, IsIncludedInAnalysis 
	FROM dbo.TrainPassage tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.JourneyID IN (@JourneyID, @actualJourneyID)
		AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	GROUP BY 
		tp.ID
		, CAST(tp.StartDatetime AS date)
		, EndAVehicleID
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		, IsIncludedInAnalysis 
	ORDER BY DateStart DESC
			
END




GO
RAISERROR ('-- update NSP_OaRunningTime', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaRunningTime' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaRunningTime] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaRunningTime
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaRunningTime]
(
	@DateStart date
	, @DateEnd date
	, @LocationIdStart int
	, @LocationIdEnd int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
		JourneyID				= s.JourneyID
		, JourneySegmenID		= s.ID
		, Headcode				= j.Headcode 
		, HeadcodeDescription	= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram				= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + jsl.Tiploc
			+ ' - ' + jel.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.EndTime, 108), ':', ''), 4) + 10000, 4)
		, NoOfPassages		= COUNT(*)
		, BookedTime		= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime))))
		, RecoveryTime		= dbo.FN_ConvertSecondToTime((
				SELECT SUM(RecoveryTime)
				FROM Section sc
				WHERE sc.SegmentID = s.ID
					AND sc.JourneyID = s.JourneyID
			)
		)

		-- OptimalTime - calculation should be simplified:
		, OptimalTime		= MIN(OptimalTrainPassage.OptimalTime) -- There is only 1 record so MIN/MAX grouping function doesnt matter
		, ActualMin			= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMax			= dbo.FN_ConvertSecondToTime(MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMean		= dbo.FN_ConvertSecondToTime(AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
	
		, TrainPassageIdOptimal				= MIN(OptimalTrainPassage.TrainPassageID)	-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMin			= MIN(MinTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMax			= MIN(MaxTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		
	FROM dbo.Segment s 
	INNER JOIN dbo.Journey j ON j.ID = s.JourneyID
	INNER JOIN dbo.TrainPassageSegment tps ON 
		s.JourneyID = tps.JourneyID
		AND s.ID = tps.SegmentID
	INNER JOIN dbo.Location jsl ON jsl.ID = j.StartLocationID
	LEFT JOIN dbo.Location jel ON jel.ID = j.EndLocationID
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM dbo.TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) ASC
	) AS MinTrainPassage
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM dbo.TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) DESC
	) AS MaxTrainPassage
	OUTER APPLY (
		SELECT TOP 1 
			TrainPassageID
			, OptimalTime = dbo.FN_ConvertSecondToTime(DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd))
		FROM dbo.TrainPassageSegment tps1
		INNER JOIN dbo.Segment s1 ON
			s1.JourneyID = tps1.JourneyID
			AND s1.ID = tps1.SegmentID
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			ABS(
				DATEDIFF(S, s1.StartTime, s1.EndTime) -- booked time
				- DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd)
			) ASC
	) AS OptimalTrainPassage
	
	WHERE 
		s.StartLocationID = @LocationIdStart
		AND s.EndLocationID = @LocationIdEnd
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
	GROUP BY 
		s.ID
		, s.JourneyID
		, j.Headcode 
		, j.HeadcodeDesc
		, j.StartTime
		, j.EndTime
		, jsl.Tiploc
		, jel.Tiploc

END




GO
RAISERROR ('-- update NSP_OaRunningTimeDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaRunningTimeDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaRunningTimeDetail] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaRunningTimeDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaRunningTimeDetail]
(
	@DateStart date
	, @DateEnd date
	, @JourneyID int
	, @SectionID int
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		StartLocation		= sl.Tiploc
		, EndLocation		= el.Tiploc
		, BookedStartTime	= s.StartTime
		, BookedEndTime		= s.EndTime
		, BookedTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime)))
		, TrainPassageDate 	= CONVERT(date, tps.ActualDatetimeStart)
		, TrainPassageWeekDay 	= LEFT(DATENAME(W, tps.ActualDatetimeStart), 3)
		, ActualStartTime	= CONVERT(time(0),tps.ActualDatetimeStart)
		, ActualEndTime		= CONVERT(time(0),tps.ActualDatetimeEnd)
		, ActualTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, tps.ActualDatetimeStart, tps.ActualDatetimeEnd)))
		, LeadingVehicle	= v.VehicleNumber
		, AverageSpeed		= tps.AverageSpeed
		, TotalEnergy		= tps.EnergyRealMove + tps.EnergyReactiveStop
		
	FROM dbo.TrainPassageSegment tps
	INNER JOIN dbo.Segment s ON 
		s.ID = tps.SegmentID
		AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.Location sl ON sl.ID = s.StartLocationID
	INNER JOIN dbo.Location el ON el.ID = s.EndLocationID
	LEFT JOIN dbo.Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE tps.JourneyID = @JourneyID
		AND tps.SegmentID = @SectionID
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
		
	ORDER BY tps.ActualDatetimeStart
	
END



GO
RAISERROR ('-- update NSP_OaTimetableByHeadcode', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaTimetableByHeadcode' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaTimetableByHeadcode] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByHeadcode
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/15 12:51:28 $$
**	$$Revision: 1.1.2.1 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/Attic/update_spectrum_db_011.sql,v $
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaTimetableByHeadcode]
(
	@StartDate datetime
	, @EndDate datetime
	, @N int = NULL
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	-- number of records returned
	IF @N IS NULL OR @N < 0
		SET @N = 1000

	
	SELECT 
		TrainPassageID		= tp.ID								
		, Headcode			= tp.Headcode
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber 
		, Setcode			= tp.Setcode
		, DelayOnArrival	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) / 60. 
		, DelayOnDeparture	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0)))) / 60.
	INTO #DelayAtDestination
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180) 
	
	SELECT
		TrainPassageID			= tp.ID
		, Headcode				= tp.Headcode
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR (tp.ID IN (select dad.TrainPassageID from #DelayAtDestination dad ))) 
	GROUP BY 
		tp.ID
		, tp.Headcode
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode

	
	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival) 
			, MaxDelayOnArrival = MAX(DelayOnArrival) 
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
	SELECT TOP (@N)
		Headcode
		, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(SectionPointTypeNumber_S)
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B))
			END
		
	

	FROM #DelayAtStation
	GROUP BY
		Headcode		
	ORDER BY 4 DESC 

END



GO
RAISERROR ('-- update NSP_OaTimetableByHeadcodeDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaTimetableByHeadcodeDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaTimetableByHeadcodeDetail] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByHeadcodeDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:	
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaTimetableByHeadcodeDetail]
(
	@StartDate datetime
	, @EndDate datetime
	, @Headcode varchar(4)
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	
	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds
	
	SELECT 
		ValidID		= tp.ID
	INTO #ValidTrainPassages
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)

	SELECT
		TrainPassageID		= tp.ID
		, JourneyID			= tp.JourneyID
		, Headcode			= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram			= dbo.FN_GetDiagramByHeadcodeAndDate(tp.Headcode, tp.StartDatetime, tp.StartDatetime)
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber
		, tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60.
		, MaxDelayOnArrival		= MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, DelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(
			CASE
				WHEN SectionPointType = 'E'
					THEN DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))
			END) / 60.)
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.Journey j ON j.ID = tp.JourneyID
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND tp.Headcode = @Headcode
		AND tp.id IN (SELECT vtp.ValidID from #ValidTrainPassages vtp)
	GROUP BY 
		tp.ID
		, tp.JourneyID
		, j.StartTime
		, tp.Headcode
		, j.HeadcodeDesc
		, tp.StartDatetime	--CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		
	SELECT
		ds.Headcode
		, ds.Diagram
		, TrainPassageID
		, JourneyID
		, ds.DateStart
		, DestinationDelay		= DelayOnArrival 
		, AvgStationDelay		= AvgDelayOnArrival
		, MaxStationDelay		= MaxDelayOnArrival
		, ExtendedDwellsPerc	= CASE
			WHEN SectionPointTypeNumber_S > 0
				THEN 100.0 * ExtendedDwellsNumber / SectionPointTypeNumber_S
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * EarlyArrivalsNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_E)
			END
		, LateDeparturesPerc	= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * LateDeparturesNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_B)
			END
	FROM #DelayAtStation AS ds
	ORDER BY ds.DateStart ASC

END



GO
RAISERROR ('-- update NSP_OaTimetableByLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaTimetableByLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaTimetableByLocation] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_OaTimetableByLocation]
(
	@StartDate datetime
	, @EndDate datetime
	, @N int = NULL
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	-- number of records returned
	IF @N IS NULL OR @N < 0
		SET @N = 1000

	SELECT
		TrainPassageID			= tp.ID
		, Location				= l.Tiploc
		, LocationName			= l.LocationName
		, LocationID			= l.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	INNER JOIN dbo.Location l ON sp.LocationID = l.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180) 
	GROUP BY 
		tp.ID
		, l.Tiploc
		, l.ID
		, l.LocationName
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode


--SELECT * FROM #DelayAtStation WHERE Location = 'KNGX'
	
/*	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival) 
			, MaxDelayOnArrival = MAX(DelayOnArrival) 
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
*/	SELECT TOP (@N)
		Location
		, LocationID
		, LocationName
--		, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
--		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
--		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(ISNULL(SectionPointTypeNumber_S, 0))
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)))
			END
		
	FROM #DelayAtStation
	GROUP BY
		Location		
		, LocationID
		, LocationName			
	ORDER BY 4 DESC 

END

GO
RAISERROR ('-- update NSP_OaTimetableByLocationDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_OaTimetableByLocationDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_OaTimetableByLocationDetail] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_OaTimetableByLocationDetail
**	Description:	
**	Call frequency:	Called from Ops Analysis 
**	Parameters:		
**	Return values:
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_OaTimetableByLocationDetail]
(
	@StartDate datetime
	, @EndDate datetime
	, @LocationID int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	SELECT
		TrainPassageID			= tp.ID
		, Location				= l.Tiploc
		, LocationID			= l.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	INNER JOIN dbo.Location l ON sp.LocationID = l.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND l.ID = @LocationID
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)
	GROUP BY 
		tp.ID
		, l.Tiploc
		, l.ID
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode


--SELECT * FROM #DelayAtStation WHERE Location = 'KNGX'
	
/*	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival) 
			, MaxDelayOnArrival = MAX(DelayOnArrival) 
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
*/	SELECT
		Location
		, LocationID
		, DateStart
--, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
--		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
--		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(ISNULL(SectionPointTypeNumber_S, 0))
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)))
			END
		
	FROM #DelayAtStation
	GROUP BY
		Location		
		, LocationID
		, DateStart
	ORDER BY DateStart ASC 

END



GO
RAISERROR ('-- update NSP_RenameVehicle', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RenameVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RenameVehicle] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_RenameVehicle]
(
	@OldVehicleNumber varchar(16)
	,@NewVehicleNumber varchar(16)
)
AS
BEGIN

	SET NOCOUNT ON;
	IF EXISTS ( SELECT Top 1 1 FROM dbo.Vehicle WHERE VehicleNumber =@NewVehicleNumber) RETURN 0 

	UPDATE dbo.Vehicle SET VehicleNumber=@NewVehicleNumber WHERE VehicleNumber=@OldVehicleNumber
	UPDATE dbo.Unit SET UnitNumber=@NewVehicleNumber WHERE UnitNumber=@OldVehicleNumber
	UPDATE dbo.FleetFormation SET UnitNumberList=@NewVehicleNumber WHERE UnitNumberList=@OldVehicleNumber

END

GO
RAISERROR ('-- update NSP_RethrowError', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RethrowError' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RethrowError] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_RethrowError]
as
/******************************************************************************
** Name: usp_RethrowError
** Description: Re raise the original value with MessageNr: 50000 (default)
**		 
**	 Example: ....
**				begin catch
**				 if xact_state() <> 0 rollback transaction
**				 EXEC usp_RethrowError;
**				end catch
**		 
**		 with seterror : Sets the @@ERROR and ERROR_NUMBER values to 50000, 
**		 regardless of the severity level
**
**	 Called by: every catch block if it's necessary	
** 
** Return values: 
*******************************************************************************
** Change History
*******************************************************************************
** Date:			Author:	 Description:
** 2012-02-01	 HeZ		 creation on database
*******************************************************************************/	
begin

	SET NOCOUNT ON;
	-- Return if there is no error information to retrieve.
	IF ERROR_NUMBER() IS NULL
		RETURN;

	DECLARE 
		@ErrorMessage	NVARCHAR(4000),
		@ErrorNumber	 INT,
		@ErrorSeverity	INT,
		@ErrorState	 INT,
		@ErrorLine		INT,
		@ErrorProcedure NVARCHAR(200);

	-- Assign variables to error-handling functions that 
	-- capture information for RAISERROR.
	SELECT 
		@ErrorNumber =	 ERROR_NUMBER(),
		@ErrorSeverity =	ERROR_SEVERITY(),
		@ErrorState =		ERROR_STATE(),
		@ErrorLine =		ERROR_LINE(),
		@ErrorProcedure =	ISNULL(ERROR_PROCEDURE(), '-');

	-- Build the message string that will contain original
	-- error information.
	SELECT @ErrorMessage = 
		N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
			'Message: '+ ERROR_MESSAGE();

	-- Raise an error: msg_str parameter of RAISERROR will contain
	-- the original error information.
	RAISERROR 
		(
		@ErrorMessage, 
		@ErrorSeverity, 
		1,				
		@ErrorNumber,	-- parameter: original error number.
		@ErrorSeverity, -- parameter: original error severity.
		@ErrorState,	 -- parameter: original error state.
		@ErrorProcedure, -- parameter: original error procedure name.
		@ErrorLine		-- parameter: original error line number.
		) with seterror;

END

GO
RAISERROR ('-- update NSP_RuleEngineGetCurrentIds', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RuleEngineGetCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RuleEngineGetCurrentIds] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
** Name:			NSP_RuleEngineGetCurrentIds
** Description:	Returns data to fault engine
** Call frequency: Every 5s 
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_RuleEngineGetCurrentIds]
	@ConfigurationName varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Value FROM dbo.RuleEngineCurrentId WHERE ConfigurationName = @ConfigurationName ORDER BY Position asc

END

GO
RAISERROR ('-- update NSP_RuleEngineUpdateCurrentIds', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RuleEngineUpdateCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RuleEngineUpdateCurrentIds] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
** Name:			NSP_RuleEngineUpdateCurrentIds
** Description:	Returns data to fault engine
** Call frequency: Every 5s
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_RuleEngineUpdateCurrentIds]
	@ConfigurationName varchar(100),
	@Value1 bigint = NULL,
	@Value2 bigint = NULL

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LastUpdateTime datetime;
	SET @LastUpdateTime = GETDATE()

	;MERGE RuleEngineCurrentId AS r
	USING (

		SELECT Position = '1', ConfigurationName = @ConfigurationName, Value = @Value1, LastUpdateTime = @LastUpdateTime
		UNION
		SELECT Position = '2', ConfigurationName = @ConfigurationName, Value = @Value2, LastUpdateTime = @LastUpdateTime

	) AS nr
	ON r.Position = nr.Position
		AND r.ConfigurationName = nr.ConfigurationName
	WHEN MATCHED THEN
	UPDATE SET
		Value = nr.Value,
		LastUpdateTime = nr.LastUpdateTime
	WHEN NOT MATCHED THEN
	INSERT (
		ConfigurationName
		, Position
		, Value
		, LastUpdateTime)
	VALUES(
		nr.ConfigurationName
		, nr.Position
		, nr.Value
		, nr.LastUpdateTime);

END

GO
RAISERROR ('-- update NSP_SaveChannelDefinition', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SaveChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SaveChannelDefinition] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_SaveChannelDefinition
** Description:	Saves a channel definition
** Call frequency: Called from Channel Definition module
** Parameters:	 @ChannelDefinitionDoc xml
** Return values: 
*******************************************************************************/
		
ALTER PROCEDURE [dbo].[NSP_SaveChannelDefinition]
	@ChannelDefinitionParam varchar(max)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @channelDoc xml = N''+@ChannelDefinitionParam;

	--read the channel definition details
	DECLARE @channelId int;
	DECLARE @channelGroupID int;
	DECLARE @channelRef varchar(10);
	DECLARE @channelUnitOfMeasure varchar(10);
	DECLARE @channelGroupOrder int;
	DECLARE @channelAlwaysDisplayed bit;
	DECLARE @previousOder int;

	SELECT
		@channelId = t.c.value(N'@id', N'int'),
		@channelGroupId = t.c.value(N'@channelgroupid', N'int'),
		@channelRef = t.c.value(N'@ref', N'varchar(10)'),
		@channelUnitOfMeasure = t.c.value(N'@unitofmeasure', N'varchar(10)'),
		@channelGroupOrder = t.c.value(N'@channelgrouporder', N'int'),
		@channelAlwaysDisplayed = t.c.value(N'@alwaysdisplayed', N'bit')
	FROM @channelDoc.nodes('/channel') t(c);
	
	-- Get previous order 
	SELECT 
		@previousOder = ChannelGroupOrder 
	FROM dbo.Channel 
	WHERE ID = @channelId	
	
	IF @channelRef = ''
		SET @channelRef = NULL;

	IF @channelUnitOfMeasure = ''
		SET @channelUnitOfMeasure = NULL;

	IF @channelGroupOrder = ''
		SET @channelGroupOrder = NULL;

	BEGIN TRANSACTION;
		--move the channel group order 1 position ahead
		IF NOT @previousOder = @channelGroupOrder
			UPDATE
				dbo.[Channel]
			SET
				[ChannelGroupOrder] = [ChannelGroupOrder] + 1
			WHERE
				[ChannelGroupID] = @channelGroupId
				AND [ChannelGroupOrder] >= @channelGroupOrder;

		--update channel definition
		UPDATE
			dbo.[Channel]
		SET
			[ChannelGroupID] = @channelGroupId,
			[Ref] = @channelRef,
			[UOM] = @channelUnitOfMeasure,
			[ChannelGroupOrder] = @channelGroupOrder,
			[IsAlwaysDisplayed] = @channelAlwaysDisplayed
		WHERE
			[ID] = @channelId;

		--remove all rule/validations for the channel
		DELETE
			FROM dbo.[ChannelRuleValidation]
			WHERE
				ChannelRuleID IN (SELECT ID FROM dbo.[ChannelRule] WHERE ChannelID = @channelId);

		DELETE
			FROM [ChannelRule]
			WHERE ChannelID = @channelId;

		--process rules
		DECLARE @ruleIndex int; 
		DECLARE @rulesCount int;
		DECLARE @ruleXml xml;

		SELECT 
			@ruleIndex = 1,
			@rulesCount = @channelDoc.value('count(/channel/rule)','int');

		WHILE @ruleIndex <= @rulesCount BEGIN
			DECLARE @ruleName varchar(100);
			DECLARE @ruleStatusId int;
			DECLARE @ruleActive bit;
			DECLARE @ruleId int;

			SELECT
				@ruleXml = @channelDoc.query('/channel/rule[position()=sql:variable("@ruleIndex")]')

			SELECT
				@ruleName = t.c.value(N'@name', N'varchar(100)'),
				@ruleStatusId = t.c.value(N'@statusid', N'int'),
				@ruleActive = t.c.value(N'@active', N'bit')
			FROM @ruleXml.nodes('/rule') t(c);

			--insert the rule
			INSERT INTO dbo.ChannelRule (ChannelID, ChannelStatusID, Name, Active)
			VALUES (@channelId, @ruleStatusId, @ruleName, @ruleActive);

			SET @ruleId = SCOPE_IDENTITY();
			
			--process validations
			DECLARE @validationIndex int; 
			DECLARE @validationsCount int;
			DECLARE @validationXml xml;
			
			SELECT 
				@validationIndex = 1,
				@validationsCount = @ruleXml.value('count(/rule/validation)','int');

			WHILE @validationIndex <= @validationsCount BEGIN
				DECLARE @validationChannelId int;
				DECLARE @validationMinValue float;
				DECLARE @validationMaxValue float;
				DECLARE @validationMinInc bit;
				DECLARE @validationMaxInc bit;
				DECLARE @validationMinStringValue varchar(10);
				DECLARE @validationMaxStringValue varchar(10);
				
				SELECT
					@validationXml = @ruleXml.query('/rule/validation[position()=sql:variable("@validationIndex")]')

				SELECT
					@validationChannelId = t.c.value(N'@channelid', N'int'),
					@validationMinStringValue = t.c.value(N'@minvalue', N'varchar(10)'),
					@validationMaxStringValue = t.c.value(N'@maxvalue', N'varchar(10)'),
					@validationMinValue = t.c.value(N'@minvalue', N'float'),
					@validationMaxValue = t.c.value(N'@maxvalue', N'float'),
					@validationMinInc = t.c.value(N'@mininclusive', N'bit'),
					@validationMaxInc = t.c.value(N'@maxinclusive', N'bit')
				FROM @validationXml.nodes('/validation') t(c);

				IF @validationMinStringValue = ''
					SET @validationMinValue = NULL
				ELSE 
					SET @validationMinValue = CONVERT(float, @validationMinStringValue)

				IF @validationMaxStringValue = ''
					SET @validationMaxValue = NULL
				ELSE 
					SET @validationMaxValue = CONVERT(float, @validationMaxStringValue)
		
				--insert validation
				INSERT INTO dbo.ChannelRuleValidation (ChannelRuleID, ChannelID, MinValue, MaxValue, MinInclusive, MaxInclusive)
					VALUES (@ruleId, @validationChannelId, @validationMinValue, @validationMaxValue, @validationMinInc, @validationMaxInc);
				
				SELECT @validationIndex = @validationIndex + 1;
			END
			
			SELECT @ruleIndex = @ruleIndex + 1;
		END

		--update configuration to reload the channel definitions and the validation rules
		UPDATE
			dbo.[Configuration]
		SET
			[PropertyValue] = 1
		WHERE
			[PropertyName] = 'spectrum.refresh.channelconfigurations'
			OR
			[PropertyName] = 'spectrum.refresh.validationrules';
			
	COMMIT TRANSACTION;
END

GO
RAISERROR ('-- update NSP_SaveChannelDefinition', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchChannelDefinition] AS BEGIN RETURN(1) END;')
GO


/******************************************************************************
** Name:			[NSP_SearchChannelDefinition]
** Description:	Returns list channel definition for a keyword
** Call frequency: Called from Channel Definition module
** Parameters:	 @ChannelId int
**				 @Keyword varchar(50)
** Return values: 
*******************************************************************************/
		
ALTER PROCEDURE [dbo].[NSP_SearchChannelDefinition]
	@ChannelId int = NULL,
	@Keyword varchar(50) = NULL
AS
BEGIN

	SET NOCOUNT ON;
	IF @ChannelId IS NULL
	BEGIN
		IF @Keyword IS NULL OR LEN(@Keyword) = 0
			SET @Keyword = '%'
		ELSE
			SET @Keyword = '%' + @Keyword + '%'
	END

	SELECT
			vcd.ID
			, vcd.VehicleID
			, vcd.ColumnID
			, vcd.ChannelGroupID
			, vcd.Name
			, vcd.Description
			, vcd.ChannelGroupOrder
			, vcd.Type
			, vcd.UOM
			, vcd.HardwareType
			, vcd.StorageMethod
			, vcd.Ref
			, vcd.MinValue
			, vcd.MaxValue
			, vcd.Comment
			, vcd.VisibleOnFaultOnly
			, vcd.IsAlwaysDisplayed
			, vcd.IsLookup
			, vcd.DefaultValue
			, vcd.IsDisplayedAsDifference
			, vcd.RelatedChannelID
			, vcd.RelatedChannelName
			, vcd.DataType
	FROM dbo.VW_ChannelDefinition as vcd
	INNER JOIN dbo.ChannelGroup as cgd ON vcd.ChannelGroupID = cgd.ID
	WHERE
		(@ChannelId IS NULL
			AND (vcd.ID LIKE @Keyword
				OR cgd.Notes LIKE @Keyword
				OR vcd.Description LIKE @Keyword
				OR vcd.Name LIKE @Keyword
				OR vcd.Ref LIKE @Keyword
				OR vcd.Type LIKE @Keyword
				OR vcd.UOM LIKE @Keyword))
		OR
		(@ChannelId IS NOT NULL AND vcd.ID = @ChannelId)
	ORDER BY vcd.ID
END


GO
RAISERROR ('-- update NSP_SearchFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchFault] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_SearchFault
** Description:	Returns list faults for a keyword
** Call frequency: Called from Event History screen
** Parameters:	 @DateFrom datetime
**				 @DateTo datetime
**				 @SearchType varchar(10) --options: Live, NoLive, All 
**				 @Keyword varchar(50)
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFault]
	@N int --Limit
	, @DateFrom datetime
	, @DateTo datetime
	, @SearchType varchar(10) --options: Live, NoLive, All 
	, @Keyword varchar(50)
AS
BEGIN 

	SET NOCOUNT ON;
	IF @Keyword IS NULL OR LEN(@Keyword) = 0
		SET @Keyword = '%'
	ELSE
		SET @Keyword = '%' + @Keyword + '%'

	IF @SearchType = 'Live'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.RecoveryStatus
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, f.ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = f.LocationID
		WHERE (1 = 1) 
			AND f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (
				f.HeadCode				LIKE @Keyword 
				OR f.FaultUnitNumber	 LIKE @Keyword 
				OR up.UnitNumber		LIKE @Keyword 
				OR us.UnitNumber		LIKE @Keyword 
				OR ut.UnitNumber		LIKE @Keyword 
				OR f.Category			LIKE @Keyword 
				OR f.Description		LIKE @Keyword
				OR l.LocationCode		LIKE @Keyword 
				OR f.FaultType			LIKE @Keyword
			) 
		ORDER BY f.CreateTime DESC 
		
	IF @SearchType = 'NoLive'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID 
			, f.RecoveryStatus 
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, f.ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = f.LocationID
		WHERE (1 = 1) 
			AND f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (
				f.HeadCode				LIKE @Keyword 
				OR FaultUnitNumber	 LIKE @Keyword 
				OR up.UnitNumber		LIKE @Keyword 
				OR us.UnitNumber		LIKE @Keyword 
				OR ut.UnitNumber		LIKE @Keyword 
				OR f.Category			LIKE @Keyword 
				OR f.Description		 LIKE @Keyword
				OR l.LocationCode		LIKE @Keyword 
				OR f.FaultType			LIKE @Keyword
			) 
		ORDER BY f.CreateTime DESC 
		
	IF @SearchType = 'All'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.RecoveryStatus
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary 
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, f.ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = f.LocationID
		WHERE (1 = 1) 
			AND f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (
				f.HeadCode				LIKE @Keyword 
				OR f.FaultUnitNumber	 LIKE @Keyword 
				OR up.UnitNumber		LIKE @Keyword 
				OR us.UnitNumber		LIKE @Keyword 
				OR ut.UnitNumber		LIKE @Keyword 
				OR f.Category			LIKE @Keyword 
				OR f.Description		 LIKE @Keyword
				OR l.LocationCode		LIKE @Keyword 
				OR f.FaultType			LIKE @Keyword
			) 
		ORDER BY f.CreateTime DESC 

END



GO
RAISERROR ('-- update NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchFaultAdvanced] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_SearchFaultAdvanced
** Description:	Returns list of Faults matching parameters
** Call frequency: Called from Event History screen
** Parameters:	 @DateFrom datetime
**				 @DateTo datetime
**				 @SearchType varchar(10) --options: Live, NoLive, All 
**				 @Headcode varchar(10)
**				 @UnitNumber varchar(10)
**				 @Vehicle varchar(10)
**				 @Location varchar(10)
**				 @FaultMetaID int
**				 @Description varchar(50)
**				 @Type varchar(10)
**				 @CategoryID int
** Return values: 0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
** $$Author$$
** $$Date$$
** $$Revision$$
** $$Source$$
*******************************************************************************/
/*
Try me:
Exec [dbo].[NSP_SearchFaultAdvanced] @N=10, @DateFrom='20170101', @DateTo='20170401', @SearchType='', @Description=null, @FaultMetaID=0, @Type=null, @CategoryID=0
*/
ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
	@N int --limit
	, @DateFrom datetime
	, @DateTo datetime
	, @SearchType varchar(10) --options: Live, NoLive, All 
	, @FaultMetaID int
	, @Type varchar(10)
	, @CategoryID int
	, @Description varchar(50)
	, @Headcode varchar(10) = NULL
	, @UnitNumber varchar(10) = NULL
	, @Vehicle varchar(10) = NULL
	, @Location varchar(10) = NULL
	, @HasRecovery bit = NULL
)
AS
BEGIN 

	SET NOCOUNT ON;

	IF @FaultMetaID = 0 SET @FaultMetaID = NULL
	IF @Type = ''		SET @Type = NULL
	IF @CategoryID = 0 SET @CategoryID = NULL
	IF @Headcode = ''	SET @Headcode = NULL
	IF @UnitNumber = '' SET @UnitNumber = NULL
	IF @Vehicle = ''	SET @Vehicle = NULL
	IF @Location = ''	SET @Location = NULL
	
		
	IF @SearchType = 'Live'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.RecoveryStatus
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, f.ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = f.LocationID
		WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR f.HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR f.FaultUnitNumber		LIKE '%' + @UnitNumber + '%')
--		 Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
			AND (@Vehicle IS NULL OR f.SecondaryUnitID		LIKE '%' + @Vehicle + '%')
			AND (@CategoryID IS NULL OR f.CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR f.Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR f.FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)
		ORDER BY f.CreateTime DESC 
		
	IF @SearchType = 'NoLive'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.RecoveryStatus
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber 
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = LocationID
		--LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID
		WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR f.HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR f.FaultUnitNumber		LIKE '%' + @UnitNumber + '%')
--		 Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
			AND (@Vehicle IS NULL OR f.SecondaryUnitID		LIKE '%' + @Vehicle + '%')
			AND (@CategoryID IS NULL OR f.CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR f.Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR f.FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)
		ORDER BY f.CreateTime DESC 
		
	IF @SearchType = 'All'
		
		SELECT TOP (@N)
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.RecoveryStatus
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitID	
			, f.FaultUnitNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary 
			, f.FaultType 
			, f.FaultTypeColor
			, f.Category
			, f.CategoryID
			, f.ReportingOnly
			, PrimaryUnitNumber	 = up.UnitNumber
			, SecondaryUnitNumber	= us.UnitNumber
			, TertiaryUnitNumber	= ut.UnitNumber 
			, f.RecoveryStatus
			, f.FleetCode 
			, f.AdditionalInfo
			, f.HasRecovery
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
		LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
		LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
		LEFT JOIN dbo.Location l ON l.ID = LocationID
		LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID
		WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR f.HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR f.FaultUnitNumber		LIKE '%' + @UnitNumber + '%')
--		 Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
			AND (@Vehicle IS NULL OR f.SecondaryUnitID		LIKE '%' + @Vehicle + '%')
			AND (@CategoryID IS NULL OR f.CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR f.Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR f.FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)
		ORDER BY f.CreateTime DESC 

END
GO
RAISERROR ('-- update NSP_SearchFaultAdvancedCount', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvancedCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchFaultAdvancedCount] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
** Name:			NSP_SearchFaultAdvancedCount
** Description:	Returns Fault count with specific parameters
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvancedCount](
	@DateFrom datetime
	, @DateTo datetime
	, @SearchType varchar(10) --options: Live, NoLive, All 
	, @FaultMetaID int
	, @Type varchar(10)
	, @CategoryID int
	, @Description varchar(50)
	, @Headcode varchar(10) = NULL
	, @UnitNumber varchar(10) = NULL
	, @Vehicle varchar(10) = NULL
	, @Location varchar(10) = NULL
	, @HasRecovery bit = NULL
)
AS
BEGIN 

	SET NOCOUNT ON;

	IF @FaultMetaID = 0 SET @FaultMetaID = NULL
	IF @Type = ''		SET @Type = NULL
	IF @CategoryID = 0 SET @CategoryID = NULL
	IF @Headcode = ''	SET @Headcode = NULL
	IF @UnitNumber = '' SET @UnitNumber = NULL
	IF @Vehicle = ''	SET @Vehicle = NULL
	IF @Location = ''	SET @Location = NULL
		
	IF @SearchType = 'Live'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
		LEFT JOIN dbo.Location l ON l.ID = LocationID
		WHERE CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR FaultUnitNumber	 LIKE '%' + @UnitNumber + '%')
			AND (@CategoryID IS NULL OR CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL  OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)
		
	IF @SearchType = 'NoLive'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
		LEFT JOIN dbo.Location l ON l.ID = LocationID
		WHERE CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR FaultUnitNumber	 LIKE '%' + @UnitNumber + '%')
			AND (@CategoryID IS NULL OR CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL  OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)

	IF @SearchType = 'All'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		LEFT JOIN dbo.Location l ON l.ID = LocationID
		WHERE CreateTime BETWEEN @DateFrom AND @DateTo
			AND (@Headcode IS NULL OR HeadCode		LIKE '%' + @Headcode + '%')
			AND (@UnitNumber IS NULL OR FaultUnitNumber	 LIKE '%' + @UnitNumber + '%')
			AND (@CategoryID IS NULL OR CategoryID	 = @CategoryID)
			AND (@Description IS NULL OR Description	LIKE '%' + @Description + '%')
			AND (@Location IS NULL OR l.LocationCode	LIKE '%' + @Location + '%')
			AND (@FaultMetaID IS NULL  OR f.FaultMetaID	= @FaultMetaID)
			AND (@Type IS NULL OR FaultTypeID	= @Type)
			AND (@HasRecovery IS NULL OR f.HasRecovery	= @HasRecovery)
END

GO
RAISERROR ('-- update NSP_SearchFaultCount', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchFaultCount] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
** Name:			NSP_SearchFaultCount
** Description:	Returns Fault count with specific parameters
** Call frequency: Every 5s 
** Parameters:	 none
** Return values: 0 if successful, else an error is raised
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_SearchFaultCount]
	@DateFrom datetime
	, @DateTo datetime
	, @SearchType varchar(10) --options: Live, NoLive, All 
	, @Keyword varchar(50)
AS
BEGIN

	SET NOCOUNT ON;
	 IF @Keyword IS NULL OR LEN(@Keyword) = 0
		SET @Keyword = '%'
	ELSE
		SET @Keyword = '%' + @Keyword + '%'

	IF @SearchType = 'Live'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				) 
		
	IF @SearchType = 'NoLive'
		
		SELECT COUNT(*)
			
		FROM dbo.VW_IX_FaultNotLive f WITH (NOEXPAND)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				) 
		
	IF @SearchType = 'All'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				) 
END


GO
RAISERROR ('-- update NSP_SimulateSLTChannelValues', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SimulateSLTChannelValues' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SimulateSLTChannelValues] AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_SimulateSLTChannelValues]
AS
BEGIN

	SET NOCOUNT ON;

	-- will select a randon location from table
	-- will select a randon number of randon channels and call proc to insert channelvalue
	DECLARE @Lower int
	DECLARE @Upper int
	DECLARE @UnitId int 
	DECLARE @lat decimal (9,6)
	DECLARE @lng decimal (9,6)
	DECLARE @EventValue int
	DECLARE @LocationIdRnd int
	DECLARE @ChannelEventIDRnd int
	DECLARE @SpeedRnd int
	DECLARE @TimestampEndTime datetime2(3)
	DECLARE @Timestamp datetime2(3)
	DECLARE @NumberOfChannelsRnd int
	DECLARE @StringSql nvarchar(max)

	SELECT TOP 1 @UnitId = UnitId, @Timestamp = Timestamp
	FROM EventChannelValue
	ORDER BY TimeStamp desc

	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd) + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitID) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql
END

GO
RAISERROR ('-- update NSP_SimulateSLTClosingEvent', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SimulateSLTClosingEvent' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_SimulateSLTClosingEvent] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SimulateSLTClosingEvent]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UnitId int
	DECLARE @ChannelId int
	DECLARE @TimeStamp datetime2
	DECLARE @TimestampEndTime datetime2 = getdate()

	SELECT	TOP 1 @UnitId = UnitID,
			@ChannelId = ChannelId,
			@TimeStamp = TimeStamp
	FROM dbo.EventChannelValue
	WHERE TimeStampEndTime is null

	IF @@ROWCOUNT > 0
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitId, @ChannelId = @ChannelId, @Value = 0
END

GO

/****** Object: StoredProcedure [dbo].[NSP_SimulateSLTEvents]	Script Date: 03/02/2017 12:01:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* Not need in DB nd Causes Errors
ALTER PROCEDURE [dbo].[NSP_SimulateSLTEvents]
AS
BEGIN

	DECLARE @Lower int
	DECLARE @Upper int
	DECLARE @UnitIdRnd int 
	DECLARE @EventHasEndTimeRnd bit
	DECLARE @ChannelEventIDRnd int
	DECLARE @TimestampEndTime datetime2(3)
	DECLARE @Timestamp datetime2(3)

	DECLARE @lat decimal (9,6)
	DECLARE @lng decimal (9,6)
	DECLARE @EventValue int
	DECLARE @LocationIdRnd int
	DECLARE @SpeedRnd int
	DECLARE @NumberOfChannelsRnd int
	DECLARE @StringSql nvarchar(max)

	-- 4 cars
	SET @Lower = 2401
	SET @Upper = 2469
	SELECT @UnitIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 5000
	SET @Upper = 5999
	SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 0
	SET @Upper = 1
	SELECT @EventHasEndTimeRnd = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

	SET @Lower = 2
	SET @Upper = 10
	SELECT @Timestamp = DateAdd(ss,-FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)), getdate())
	SELECT @TimestampEndTime = @Timestamp

	If @EventHasEndTimeRnd = 1
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIdRnd, @Value = 0
	Else 
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = null, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIDRnd, @Value = 1


	-- will select a randon location from table
	-- will select a randon number of randon channels and call proc to insert channelvalue

	-------- insert channelvalues
	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd) + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitIdRnd) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql

	-- insert event for 6 cars
	SET @Lower = 2601
	SET @Upper = 2662
	SELECT @UnitIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 5000
	SET @Upper = 5999
	SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	SET @Lower = 0
	SET @Upper = 1
	SELECT @EventHasEndTimeRnd = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

	SET @Lower = 2
	SET @Upper = 10
	SELECT @Timestamp = DateAdd(ss,-FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)), getdate())
	SELECT @TimestampEndTime = @Timestamp

	If @EventHasEndTimeRnd = 1
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIdRnd, @Value = 0
	Else 
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = null, @UnitId = @UnitIdRnd, @ChannelId = @ChannelEventIDRnd, @Value = 1

	-------- insert channelvalues
	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd) + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitIdRnd) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql
END
*/
GO
RAISERROR ('-- update NSP_UpdateChannelCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateChannelCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_UpdateChannelCategory] AS BEGIN RETURN(1) END;')
GO
/******************************************************************************
**	Name:			NSP_UpdateChannelCategory
**	Description:	Update Channel table
**	Call frequency:	Once on channel definition load
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_UpdateChannelCategory]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @GroupOrder int
	, @Group varchar(100)
)
AS 
BEGIN

	SET NOCOUNT ON;

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT top 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT top 1 ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	UPDATE dbo.Channel SET
		ChannelGroupOrder = @GroupOrder
		, ChannelGroupID = (SELECT top 1 ID FROM dbo.ChannelGroup WHERE Notes =@Group)
		, IsAlwaysDisplayed = 1
	WHERE ID = @ChannelID
END


GO
RAISERROR ('-- update NSP_UpdateFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_UpdateFaultCategory] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultCategory](
	@ID int
	, @Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRAN
	BEGIN TRY

		UPDATE dbo.FaultCategory SET
			Category = @Category
			, ReportingOnly = @ReportingOnly
		WHERE ID = @ID	
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
END
GO
RAISERROR ('-- update NSP_UpdateFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_UpdateFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta](
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
)
AS
	BEGIN
		
		SET NOCOUNT ON;

		BEGIN TRAN
		BEGIN TRY

			UPDATE dbo.FaultMeta SET
				Username		= @Username
				, FaultCode	 = @FaultCode
				, Description	= @Description 
				, Summary		= @Summary
				, AdditionalInfo = @AdditionalInfo
				, Url			= @Url
				, CategoryID	= @CategoryID
				, HasRecovery	= @HasRecovery
				, RecoveryProcessPath	= @RecoveryProcessPath
				, FaultTypeID	= @FaultTypeID
				, GroupID		= @GroupID
			WHERE ID = @ID

			COMMIT TRAN
		
		END TRY
		BEGIN CATCH

			EXEC dbo.NSP_RethrowError;
			ROLLBACK TRAN;

		END CATCH

	END


GO
RAISERROR ('-- update NSP_UpdateReportAdhesion', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateReportAdhesion' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_UpdateReportAdhesion] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_UpdateReportAdhesion
**	Description:	Updates ReportAdhesion table
**	Call frequency:	Every 15min
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/10/12 09:24:15 $$
**	$$Revision: 1.2.2.2 $$
**	$$Source: /home/cvs/spectrum/swt/src/main/database/update_spectrum_db_014.sql,v $$
******************************************************************************/


ALTER PROCEDURE [dbo].[NSP_UpdateReportAdhesion]
AS

	SET NOCOUNT ON;
		
	DECLARE @reportTime datetime = DATEADD(n, -1, SYSDATETIME())
	DECLARE @reportCallFrequency int = 15 -- min

	DELETE [dbo].[ReportAdhesion] 
	WHERE TimeStamp < DATEADD(d, -7, @reportTime)
	
	
	SELECT
		ROW_NUMBER() OVER (PARTITION BY UnitID ORDER BY Timestamp) AS UnitRowNumber
		, ID
		, TimeStamp
		, UnitID
		, Col3
		, Col4
		--, ISNULL(Col54, 0)	AS WSP
		, CAST(NULL AS bit) AS WspChange
		, CAST(NULL AS datetime) AS EventEndTimestamp
	INTO #temp
	FROM dbo.ChannelValue (NOLOCK)
	WHERE
		TimeStamp BETWEEN DATEADD(n, (-2 * @reportCallFrequency + 1), @reportTime) AND @reportTime
		AND UnitID NOT IN (SELECT unitID FROM dbo.Vehicle v WHERE v.UnitID IN(352, 442)) --VehicleNumber: 77704, 62837
	
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_VehicleRowNumber ON #temp(VehicleID, VehicleRowNumber)
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_TimeStamp ON #temp(VehicleID, TimeStamp) INCLUDE (WSP)

	UPDATE t1 SET
		WspChange = CASE
			WHEN t1.WSP = 1 AND t2.WSP = 0
				THEN 1
			ELSE 0
		END
	FROM #temp t1
	INNER JOIN #temp t2 ON t1.UnitID = t2.UnitID 
		AND t1.UnitRowNumber = t2.UnitRowNumber + 1

	UPDATE t1 SET
		[EventEndTimestamp] = (SELECT MIN(t2.Timestamp) FROM #temp t2 WHERE t1.UnitID = t2.UnitID AND t2.TimeStamp > t1.TimeStamp AND WSP = 0)
	FROM #temp t1
	WHERE WspChange = 1
	
	INSERT INTO [dbo].[ReportAdhesion]
	(	
		[ID]
		, [VehicleID]
		, [TimeStamp]
		, [Value]
		, [EventType]
		, [Latitude]
		, [Longitude]
		, [GCourse]
		, [Duration]
		, [EventEndTimestamp]
	)
	SELECT
		t.[ID]
		, v.[VehicleID]
		, t.[TimeStamp]
		, 1				--[Value]
		, 1				--[EventType] = 1 for WSP
		, t.Col1			--[Latitude]
		, t.Col2			--[Longitude]
		, 0				--[GCourse]
		, DATEDIFF(ms, Timestamp, EventEndTimestamp) / 1000.0 --[Duration]
		, [EventEndTimestamp]
	FROM #temp t
	JOIN dbo.Vehicle v ON v.UnitID = t.UnitID
	WHERE WspChange = 1
		AND ID NOT IN (SELECT ID FROM dbo.ReportAdhesion)



GO
RAISERROR ('-- update NSP_UpdateVehicleDownloadStatus', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateVehicleDownloadStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_UpdateVehicleDownloadStatus] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_UpdateVehicleDownloadStatus
**	Description:	Analysis all events received from:
**					RMD (EventCategory 24)
**					PL (EventCategory 255)
**					Service updatning SQLiteDB (EventCategory 1001)
**					TMS File Processing interface (EventCategory 1000)
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_UpdateVehicleDownloadStatus]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @vehicleDownloadProgressID int
	DECLARE @eventCategory int
	DECLARE @eventCode int
	DECLARE @vehicleID int
	DECLARE @vehicleDownloadID int
	DECLARE @vehicleDownloadStatus int
	
	WHILE 1=1
	BEGIN
		---------------------------------------------------------------------------------------

		SET @vehicleDownloadProgressID = (SELECT TOP 1 ID FROM VehicleDownloadProgress WHERE IsProcessed = 0 ORDER BY RecordInsert ASC)

		SET @eventCategory = NULL
		SET @eventCode = NULL
		SET @vehicleID = NULL
		SET @vehicleDownloadStatus = NULL
		SET @vehicleDownloadID = NULL
		
		SELECT
			@vehicleID = VehicleID
			, @eventCategory = EventCategory
			, @eventCode = EventCode
		FROM dbo.VehicleDownloadProgress
		WHERE ID = @vehicleDownloadProgressID

		IF @vehicleDownloadProgressID IS NULL 
			BREAK
			
		-- Find VehicleDownload.ID for vehicle that is not Completed or Failed
		-- VehicleDownload Status:
		-- 1 - Requested
		-- 2 - In Progress
		-- 0 - Completed
		-- 3 - Failed
		SELECT 
			@vehicleDownloadID = ID 
			, @vehicleDownloadStatus = Status
		FROM dbo.VehicleDownload 
		WHERE VehicleID = @vehicleID
			AND Status <> 0 -- Comlpeted
			AND Status <> 3 -- Failed

			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
					
		IF @vehicleDownloadID IS NOT NULL
			-- or Completed download 
			OR (@eventCategory = 1000 AND @eventCode = 0)

		BEGIN
		
			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
			
			-- @eventCategory = 24 - RMD Events
			------------------------------------------
			-- Ignored statuses:
			-- 0 Connected to TMS
			-- 3 Auto download request

			-- Failure statuses:
			-- 1 Connect timed out
			-- 5 Download timed out	Failed (RMD)
			-- 6 Download failed	Failed (RMD)
			-- 7 Got a download command and TMS log not enabled	Failed (RMD)
			-- 8 ATMS Buffer Overrun	Failed (RMD)
			IF @eventCategory = 24 AND @eventCode IN (1,5,6,7,8)
				UPDATE dbo.VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID
	
			-- 2 Manual download request	Requested
			-- 4 Download completed ok	Confirmed
			IF @eventCategory = 24 AND @eventCode IN (2, 4)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			
			-- @eventCategory = 255 - PL Events
			------------------------------------------
			-- 32 File upload failed: CRC Error Failed (PL)
			-- 33 File upload failed: Timeout	Failed (PL)
			-- 34 File upload failed: Aborted	Failed (PL)
			-- 35 File upload failed: Unknown	Failed (PL)
			IF @eventCategory = 255 AND @eventCode IN (32,33,34,35,36)
				UPDATE dbo.VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID

			-- 36 File transferred OK	Transferred
			IF @eventCategory = 255 AND @eventCode IN (36)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1000 - File Loader Events
			------------------------------------------
			-- 0 File Imported Successfully (User Requested Download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NOT NULL
				UPDATE dbo.VehicleDownload SET
					Status = 0	-- Completed
					, DateTimeCompleted = SYSDATETIME()
				WHERE ID = @vehicleDownloadID

			-- 0 File Imported Successfully (Scheduled download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NULL
			BEGIN
				PRINT 'inserted @vehicleDownloadID:'
			
				SET @vehicleDownloadStatus = -1 --current status -1 as not existing

				INSERT INTO dbo.VehicleDownload
					(VehicleID
					,Status
					,DateTimeRequested
					,DateTimeCompleted
					,IsManual
					,FileType)
				SELECT
					VehicleID			= @vehicleID
					,Status				= 0
					,DateTimeRequested	= SYSDATETIME()
					,DateTimeCompleted	= SYSDATETIME()
					,IsManual			= 0
					,FileType			= 6

				SET @vehicleDownloadID = SCOPE_IDENTITY()
				
				
				PRINT @vehicleDownloadID
				
			END
			
			-- 1 File Imported Failed
			IF @eventCategory = 1000 AND @eventCode IN (1)
				UPDATE dbo.VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1001 - Scheduling Service Events
			------------------------------------------
			-- 0 Flag updated in SQLite DB successfully
			IF @eventCategory = 1001 AND @eventCode IN (0)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progressd
				WHERE ID = @vehicleDownloadID

			-- 1 Flag updated in SQLite DB failed
			IF @eventCategory = 1001 AND @eventCode IN (1)
				UPDATE dbo.VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID
		END

		
		-- Flag file as processed
		UPDATE dbo.VehicleDownloadProgress SET 
			IsProcessed = 1
			, VehicleDownloadID = @vehicleDownloadID
			, StatusUpdated = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN 1
				ELSE 0
			END
			, StatusUpdatedDatetime = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN SYSDATETIME()
				ELSE NULL
			END
		WHERE ID = @vehicleDownloadProgressID
		
	END
END

GO
RAISERROR ('-- update NSP_GetFaultEventChannelValueLatest', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueLatest] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatest]
    @DatetimeStart DATETIME
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH EventChannelValueLatest
    AS (
        SELECT MAX(ecv.TimeStamp) AS TimeStamp
            ,ecv.UnitID
            ,ecv.ChannelID
        FROM dbo.EventChannelValue ecv
        WHERE ecv.TimeStamp <= @DatetimeStart
            AND ecv.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND ecv.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY ecv.UnitID
            ,ecv.ChannelID
        )
    SELECT ecv.ID
    ,ecv.UnitID
    ,ecv.ChannelID
    ,ecv.TimeStamp
    ,ecv.Value
    FROM EventChannelValueLatest ecvl
    JOIN dbo.EventChannelValue ecv
        ON ecvl.UnitID = ecv.UnitID
        AND ecvl.ChannelID = ecv.ChannelID
        AND ecvl.TimeStamp = ecv.TimeStamp
        
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,048
           ,'update_spectrum_db_048.sql'
           ,'1.2.01')
GO