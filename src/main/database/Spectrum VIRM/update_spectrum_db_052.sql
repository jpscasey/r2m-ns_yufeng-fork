SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 052
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('--Create table ExternalReference', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ExternalReference')
    DROP TABLE [dbo].ExternalReference
GO

CREATE TABLE ExternalReference
( ID Int IDENTITY (1,1) NOT NULL
, ExternalCode varchar(10) NOT NULL
, SystemName varchar(50) NULL
, timestamp datetime2
, CONSTRAINT [PK_ExternalReference] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

RAISERROR ('--Create table ExternalReferenceField', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ExternalReferenceField')
    DROP TABLE [dbo].ExternalReferenceField
GO

CREATE TABLE ExternalReferenceField
( ID Int IDENTITY (1,1) NOT NULL
, ExternalFaultID INT NOT NULL
, Field varchar(20) NOT NULL
, Value varchar(50) NULL
, CONSTRAINT [PK_ExternalReferenceField] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ExternalReferenceField]  WITH CHECK ADD  CONSTRAINT [FK_ExternalReference_ExternalReferenceField] FOREIGN KEY([ExternalFaultID])
REFERENCES [dbo].[ExternalReference] ([ID])
GO

ALTER TABLE [dbo].[ExternalReferenceField] CHECK CONSTRAINT [FK_ExternalReference_ExternalReferenceField]
GO


RAISERROR ('--Create table Fault2ExternalReferenceLink', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Fault2ExternalReferenceLink')
    DROP TABLE [dbo].Fault2ExternalReferenceLink
GO

CREATE TABLE Fault2ExternalReferenceLink
( ID Int IDENTITY (1,1) NOT NULL
, FaultID INT NOT NULL
, ExternalFaultID INT NOT NULL
, CONSTRAINT [PK_Fault2ExternalReferenceLink] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[Fault2ExternalReferenceLink]  WITH CHECK ADD  CONSTRAINT [FK_ExternalReference_Fault2ExternalReferenceLink] FOREIGN KEY([ExternalFaultID])
REFERENCES [dbo].[ExternalReference] ([ID])
GO

ALTER TABLE [dbo].[Fault2ExternalReferenceLink] CHECK CONSTRAINT [FK_ExternalReference_Fault2ExternalReferenceLink]
GO


ALTER TABLE [dbo].[Fault2ExternalReferenceLink]  WITH CHECK ADD  CONSTRAINT [FK_Fault_Fault2ExternalReferenceLink] FOREIGN KEY([FaultID])
REFERENCES [dbo].[Fault] ([ID])
GO

ALTER TABLE [dbo].[Fault2ExternalReferenceLink] CHECK CONSTRAINT [FK_Fault_Fault2ExternalReferenceLink]
GO 

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,052
           ,'update_spectrum_db_052.sql'
           ,'1.1.01')
GO