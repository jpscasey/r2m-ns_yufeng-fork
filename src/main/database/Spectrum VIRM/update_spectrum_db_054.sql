SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 054
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('--Alter ChannelValue data types', 0, 1) WITH NOWAIT
GO

ALTER TABLE ChannelValue ALTER COLUMN Col4 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col61 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col63 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col65 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col82 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col88 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col90 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col92 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col109 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col252 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col253 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col254 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col255 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col297 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col298 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col299 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col300 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col301 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col302 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col303 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col304 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col305 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col306 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col307 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col308 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col309 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col310 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col311 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col312 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col313 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col314 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col315 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col316 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col317 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col318 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col319 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col320 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col321 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col322 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col323 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col324 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col325 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col326 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col327 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col328 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col329 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col330 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col331 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col332 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col333 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col334 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col335 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col336 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col337 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col363 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col461 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col463 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col464 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col465 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col551 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col552 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col564 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col565 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col568 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col569 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col572 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col580 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col581 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col582 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col583 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col584 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col585 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col586 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col587 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col598 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col599 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col648 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col649 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col650 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col655 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col656 int NULL
ALTER TABLE ChannelValue ALTER COLUMN Col657 int NULL

RAISERROR ('-- update NSP_InsertChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
	    @UpdateRecord bit 
,@Col1 decimal(9,6) = NULL,@Col2 decimal(9,6) = NULL,@Col3 smallint = NULL,@Col4 int = NULL,@Col5 bit = NULL,@Col6 bit = NULL,@Col7 bit = NULL
,@Col8 bit = NULL,@Col9 bit = NULL,@Col10 bit = NULL,@Col11 bit = NULL,@Col12 bit = NULL,@Col13 bit = NULL,@Col14 bit = NULL
,@Col15 bit = NULL,@Col16 bit = NULL,@Col17 bit = NULL,@Col18 bit = NULL,@Col19 bit = NULL,@Col20 bit = NULL,@Col21 bit = NULL
,@Col22 bit = NULL,@Col23 bit = NULL,@Col24 bit = NULL,@Col25 bit = NULL,@Col26 bit = NULL,@Col27 bit = NULL,@Col28 bit = NULL
,@Col29 bit = NULL,@Col30 bit = NULL,@Col31 bit = NULL,@Col32 bit = NULL,@Col33 bit = NULL,@Col34 bit = NULL,@Col35 bit = NULL
,@Col36 bit = NULL,@Col37 bit = NULL,@Col38 bit = NULL,@Col39 bit = NULL,@Col40 bit = NULL,@Col41 bit = NULL,@Col42 bit = NULL
,@Col43 bit = NULL,@Col44 bit = NULL,@Col45 bit = NULL,@Col46 bit = NULL,@Col47 bit = NULL,@Col48 bit = NULL,@Col49 bit = NULL
,@Col50 bit = NULL,@Col51 bit = NULL,@Col52 bit = NULL,@Col53 bit = NULL,@Col54 bit = NULL,@Col55 bit = NULL,@Col56 bit = NULL
,@Col57 bit = NULL,@Col58 bit = NULL,@Col59 bit = NULL,@Col60 bit = NULL,@Col61 int = NULL,@Col62 smallint = NULL,@Col63 int = NULL
,@Col64 smallint = NULL,@Col65 int = NULL,@Col66 bit = NULL,@Col67 bit = NULL,@Col68 bit = NULL,@Col69 bit = NULL,@Col70 bit = NULL
,@Col71 bit = NULL,@Col72 bit = NULL,@Col73 bit = NULL,@Col74 bit = NULL,@Col75 bit = NULL,@Col76 bit = NULL,@Col77 bit = NULL
,@Col78 bit = NULL,@Col79 bit = NULL,@Col80 bit = NULL,@Col81 bit = NULL,@Col82 int = NULL,@Col83 tinyint = NULL,@Col84 tinyint = NULL
,@Col85 smallint = NULL,@Col86 smallint = NULL,@Col87 smallint = NULL,@Col88 int = NULL,@Col89 smallint = NULL,@Col90 int = NULL,@Col91 smallint = NULL
,@Col92 int = NULL,@Col93 bit = NULL,@Col94 bit = NULL,@Col95 bit = NULL,@Col96 bit = NULL,@Col97 bit = NULL,@Col98 bit = NULL
,@Col99 bit = NULL,@Col100 bit = NULL,@Col101 bit = NULL,@Col102 bit = NULL,@Col103 bit = NULL,@Col104 bit = NULL,@Col105 bit = NULL
,@Col106 bit = NULL,@Col107 bit = NULL,@Col108 bit = NULL,@Col109 int = NULL,@Col110 tinyint = NULL,@Col111 tinyint = NULL,@Col112 smallint = NULL
,@Col113 smallint = NULL,@Col114 smallint = NULL,@Col115 tinyint = NULL,@Col116 tinyint = NULL,@Col117 bit = NULL,@Col118 bit = NULL,@Col119 bit = NULL
,@Col120 tinyint = NULL,@Col121 tinyint = NULL,@Col122 bit = NULL,@Col123 bit = NULL,@Col124 bit = NULL,@Col125 bit = NULL,@Col126 bit = NULL
,@Col127 bit = NULL,@Col128 bit = NULL,@Col129 bit = NULL,@Col130 bit = NULL,@Col131 bit = NULL,@Col132 bit = NULL,@Col133 bit = NULL
,@Col134 bit = NULL,@Col135 bit = NULL,@Col136 bit = NULL,@Col137 bit = NULL,@Col138 bit = NULL,@Col139 bit = NULL,@Col140 bit = NULL
,@Col141 bit = NULL,@Col142 bit = NULL,@Col143 bit = NULL,@Col144 bit = NULL,@Col145 bit = NULL,@Col146 bit = NULL,@Col147 bit = NULL
,@Col148 bit = NULL,@Col149 bit = NULL,@Col150 bit = NULL,@Col151 bit = NULL,@Col152 bit = NULL,@Col153 bit = NULL,@Col154 bit = NULL
,@Col155 bit = NULL,@Col156 bit = NULL,@Col157 bit = NULL,@Col158 bit = NULL,@Col159 bit = NULL,@Col160 bit = NULL,@Col161 bit = NULL
,@Col162 bit = NULL,@Col163 bit = NULL,@Col164 bit = NULL,@Col165 bit = NULL,@Col166 bit = NULL,@Col167 bit = NULL,@Col168 bit = NULL
,@Col169 tinyint = NULL,@Col170 tinyint = NULL,@Col171 tinyint = NULL,@Col172 tinyint = NULL,@Col173 tinyint = NULL,@Col174 bit = NULL,@Col175 bit = NULL
,@Col176 bit = NULL,@Col177 bit = NULL,@Col178 bit = NULL,@Col179 bit = NULL,@Col180 bit = NULL,@Col181 bit = NULL,@Col182 bit = NULL
,@Col183 bit = NULL,@Col184 bit = NULL,@Col185 bit = NULL,@Col186 bit = NULL,@Col187 bit = NULL,@Col188 bit = NULL,@Col189 bit = NULL
,@Col190 bit = NULL,@Col191 bit = NULL,@Col192 bit = NULL,@Col193 bit = NULL,@Col194 bit = NULL,@Col195 bit = NULL,@Col196 bit = NULL
,@Col197 bit = NULL,@Col198 bit = NULL,@Col199 bit = NULL,@Col200 bit = NULL,@Col201 bit = NULL,@Col202 bit = NULL,@Col203 bit = NULL
,@Col204 bit = NULL,@Col205 bit = NULL,@Col206 bit = NULL,@Col207 bit = NULL,@Col208 bit = NULL,@Col209 bit = NULL,@Col210 bit = NULL
,@Col211 bit = NULL,@Col212 bit = NULL,@Col213 bit = NULL,@Col214 bit = NULL,@Col215 bit = NULL,@Col216 bit = NULL,@Col217 bit = NULL
,@Col218 bit = NULL,@Col219 bit = NULL,@Col220 bit = NULL,@Col221 bit = NULL,@Col222 bit = NULL,@Col223 bit = NULL,@Col224 bit = NULL
,@Col225 bit = NULL,@Col226 bit = NULL,@Col227 bit = NULL,@Col228 bit = NULL,@Col229 bit = NULL,@Col230 bit = NULL,@Col231 bit = NULL
,@Col232 bit = NULL,@Col233 bit = NULL,@Col234 bit = NULL,@Col235 bit = NULL,@Col236 bit = NULL,@Col237 bit = NULL,@Col238 bit = NULL
,@Col239 bit = NULL,@Col240 bit = NULL,@Col241 bit = NULL,@Col242 bit = NULL,@Col243 bit = NULL,@Col244 bit = NULL,@Col245 bit = NULL
,@Col246 bit = NULL,@Col247 bit = NULL,@Col248 bit = NULL,@Col249 bit = NULL,@Col250 bit = NULL,@Col251 bit = NULL,@Col252 int = NULL
,@Col253 int = NULL,@Col254 int = NULL,@Col255 int = NULL,@Col256 bit = NULL,@Col257 bit = NULL,@Col258 bit = NULL,@Col259 bit = NULL
,@Col260 bit = NULL,@Col261 bit = NULL,@Col262 bit = NULL,@Col263 bit = NULL,@Col264 bit = NULL,@Col265 bit = NULL,@Col266 bit = NULL
,@Col267 bit = NULL,@Col268 bit = NULL,@Col269 bit = NULL,@Col270 bit = NULL,@Col271 bit = NULL,@Col272 bit = NULL,@Col273 bit = NULL
,@Col274 bit = NULL,@Col275 bit = NULL,@Col276 bit = NULL,@Col277 bit = NULL,@Col278 bit = NULL,@Col279 bit = NULL,@Col280 bit = NULL
,@Col281 bit = NULL,@Col282 bit = NULL,@Col283 bit = NULL,@Col284 bit = NULL,@Col285 bit = NULL,@Col286 bit = NULL,@Col287 bit = NULL
,@Col288 bit = NULL,@Col289 bit = NULL,@Col290 bit = NULL,@Col291 bit = NULL,@Col292 bit = NULL,@Col293 bit = NULL,@Col294 bit = NULL
,@Col295 bit = NULL,@Col296 bit = NULL,@Col297 int = NULL,@Col298 int = NULL,@Col299 int = NULL,@Col300 int = NULL,@Col301 int = NULL
,@Col302 int = NULL,@Col303 int = NULL,@Col304 int = NULL,@Col305 int = NULL,@Col306 int = NULL,@Col307 int = NULL,@Col308 int = NULL
,@Col309 int = NULL,@Col310 int = NULL,@Col311 int = NULL,@Col312 int = NULL,@Col313 int = NULL,@Col314 int = NULL,@Col315 int = NULL
,@Col316 int = NULL,@Col317 int = NULL,@Col318 int = NULL,@Col319 int = NULL,@Col320 int = NULL,@Col321 int = NULL,@Col322 int = NULL
,@Col323 int = NULL,@Col324 int = NULL,@Col325 int = NULL,@Col326 int = NULL,@Col327 int = NULL,@Col328 int = NULL,@Col329 int = NULL
,@Col330 int = NULL,@Col331 int = NULL,@Col332 int = NULL,@Col333 int = NULL,@Col334 int = NULL,@Col335 int = NULL,@Col336 int = NULL
,@Col337 int = NULL,@Col338 bit = NULL,@Col339 bit = NULL,@Col340 bit = NULL,@Col341 bit = NULL,@Col342 bit = NULL,@Col343 bit = NULL
,@Col344 bit = NULL,@Col345 bit = NULL,@Col346 bit = NULL,@Col347 bit = NULL,@Col348 bit = NULL,@Col349 bit = NULL,@Col350 bit = NULL
,@Col351 bit = NULL,@Col352 bit = NULL,@Col353 bit = NULL,@Col354 bit = NULL,@Col355 bit = NULL,@Col356 bit = NULL,@Col357 bit = NULL
,@Col358 bit = NULL,@Col359 bit = NULL,@Col360 bit = NULL,@Col361 bit = NULL,@Col362 tinyint = NULL,@Col363 int = NULL,@Col364 tinyint = NULL
,@Col365 tinyint = NULL,@Col366 tinyint = NULL,@Col367 tinyint = NULL,@Col368 smallint = NULL,@Col369 smallint = NULL,@Col370 smallint = NULL,@Col371 smallint = NULL
,@Col372 smallint = NULL,@Col373 smallint = NULL,@Col374 smallint = NULL,@Col375 smallint = NULL,@Col376 smallint = NULL,@Col377 smallint = NULL,@Col378 smallint = NULL
,@Col379 smallint = NULL,@Col380 smallint = NULL,@Col381 smallint = NULL,@Col382 smallint = NULL,@Col383 smallint = NULL,@Col384 smallint = NULL,@Col385 smallint = NULL
,@Col386 smallint = NULL,@Col387 smallint = NULL,@Col388 smallint = NULL,@Col389 smallint = NULL,@Col390 smallint = NULL,@Col391 smallint = NULL,@Col392 smallint = NULL
,@Col393 smallint = NULL,@Col394 smallint = NULL,@Col395 smallint = NULL,@Col396 smallint = NULL,@Col397 smallint = NULL,@Col398 smallint = NULL,@Col399 smallint = NULL
,@Col400 smallint = NULL,@Col401 smallint = NULL,@Col402 smallint = NULL,@Col403 smallint = NULL,@Col404 smallint = NULL,@Col405 smallint = NULL,@Col406 smallint = NULL
,@Col407 smallint = NULL,@Col408 smallint = NULL,@Col409 smallint = NULL,@Col410 smallint = NULL,@Col411 smallint = NULL,@Col412 smallint = NULL,@Col413 smallint = NULL
,@Col414 smallint = NULL,@Col415 smallint = NULL,@Col416 smallint = NULL,@Col417 smallint = NULL,@Col418 smallint = NULL,@Col419 smallint = NULL,@Col420 smallint = NULL
,@Col421 smallint = NULL,@Col422 smallint = NULL,@Col423 smallint = NULL,@Col424 smallint = NULL,@Col425 smallint = NULL,@Col426 smallint = NULL,@Col427 smallint = NULL
,@Col428 smallint = NULL,@Col429 smallint = NULL,@Col430 smallint = NULL,@Col431 smallint = NULL,@Col432 smallint = NULL,@Col433 smallint = NULL,@Col434 smallint = NULL
,@Col435 smallint = NULL,@Col436 smallint = NULL,@Col437 smallint = NULL,@Col438 smallint = NULL,@Col439 smallint = NULL,@Col440 smallint = NULL,@Col441 smallint = NULL
,@Col442 smallint = NULL,@Col443 smallint = NULL,@Col444 smallint = NULL,@Col445 smallint = NULL,@Col446 smallint = NULL,@Col447 smallint = NULL,@Col448 smallint = NULL
,@Col449 smallint = NULL,@Col450 tinyint = NULL,@Col451 tinyint = NULL,@Col452 tinyint = NULL,@Col453 tinyint = NULL,@Col454 tinyint = NULL,@Col455 tinyint = NULL
,@Col456 tinyint = NULL,@Col457 tinyint = NULL,@Col458 tinyint = NULL,@Col459 tinyint = NULL,@Col460 tinyint = NULL,@Col461 int = NULL,@Col462 tinyint = NULL
,@Col463 int = NULL,@Col464 int = NULL,@Col465 int = NULL,@Col466 smallint = NULL,@Col467 bit = NULL,@Col468 bit = NULL,@Col469 bit = NULL
,@Col470 bit = NULL,@Col471 bit = NULL,@Col472 bit = NULL,@Col473 bit = NULL,@Col474 bit = NULL,@Col475 bit = NULL,@Col476 bit = NULL
,@Col477 bit = NULL,@Col478 bit = NULL,@Col479 bit = NULL,@Col480 bit = NULL,@Col481 bit = NULL,@Col482 bit = NULL,@Col483 bit = NULL
,@Col484 bit = NULL,@Col485 bit = NULL,@Col486 bit = NULL,@Col487 bit = NULL,@Col488 bit = NULL,@Col489 bit = NULL,@Col490 bit = NULL
,@Col491 bit = NULL,@Col492 bit = NULL,@Col493 bit = NULL,@Col494 bit = NULL,@Col495 bit = NULL,@Col496 bit = NULL,@Col497 bit = NULL
,@Col498 bit = NULL,@Col499 bit = NULL,@Col500 bit = NULL,@Col501 bit = NULL,@Col502 bit = NULL,@Col503 bit = NULL,@Col504 bit = NULL
,@Col505 bit = NULL,@Col506 bit = NULL,@Col507 bit = NULL,@Col508 bit = NULL,@Col509 bit = NULL,@Col510 bit = NULL,@Col511 bit = NULL
,@Col512 bit = NULL,@Col513 bit = NULL,@Col514 bit = NULL,@Col515 bit = NULL,@Col516 bit = NULL,@Col517 bit = NULL,@Col518 bit = NULL
,@Col519 bit = NULL,@Col520 bit = NULL,@Col521 bit = NULL,@Col522 bit = NULL,@Col523 bit = NULL,@Col524 bit = NULL,@Col525 bit = NULL
,@Col526 bit = NULL,@Col527 bit = NULL,@Col528 bit = NULL,@Col529 bit = NULL,@Col530 bit = NULL,@Col531 bit = NULL,@Col532 bit = NULL
,@Col533 bit = NULL,@Col534 bit = NULL,@Col535 bit = NULL,@Col536 bit = NULL,@Col537 bit = NULL,@Col538 bit = NULL,@Col539 bit = NULL
,@Col540 bit = NULL,@Col541 bit = NULL,@Col542 bit = NULL,@Col543 bit = NULL,@Col544 bit = NULL,@Col545 tinyint = NULL,@Col546 tinyint = NULL
,@Col547 tinyint = NULL,@Col548 tinyint = NULL,@Col549 tinyint = NULL,@Col550 tinyint = NULL,@Col551 int = NULL,@Col552 int = NULL,@Col553 tinyint = NULL
,@Col554 tinyint = NULL,@Col555 tinyint = NULL,@Col556 tinyint = NULL,@Col557 tinyint = NULL,@Col558 tinyint = NULL,@Col559 tinyint = NULL,@Col560 tinyint = NULL
,@Col561 tinyint = NULL,@Col562 bit = NULL,@Col563 bit = NULL,@Col564 int = NULL,@Col565 int = NULL,@Col566 bit = NULL,@Col567 tinyint = NULL
,@Col568 int = NULL,@Col569 int = NULL,@Col570 tinyint = NULL,@Col571 tinyint = NULL,@Col572 int = NULL,@Col573 tinyint = NULL,@Col574 bit = NULL
,@Col575 bit = NULL,@Col576 bit = NULL,@Col577 bit = NULL,@Col578 bit = NULL,@Col579 bit = NULL,@Col580 int = NULL,@Col581 int = NULL
,@Col582 int = NULL,@Col583 int = NULL,@Col584 int = NULL,@Col585 int = NULL,@Col586 int = NULL,@Col587 int = NULL,@Col588 bit = NULL
,@Col589 bit = NULL,@Col590 tinyint = NULL,@Col591 tinyint = NULL,@Col592 bit = NULL,@Col593 bit = NULL,@Col594 tinyint = NULL,@Col595 bit = NULL
,@Col596 bit = NULL,@Col597 tinyint = NULL,@Col598 int = NULL,@Col599 int = NULL,@Col600 tinyint = NULL,@Col601 tinyint = NULL,@Col602 tinyint = NULL
,@Col603 bit = NULL,@Col604 tinyint = NULL,@Col605 tinyint = NULL,@Col606 tinyint = NULL,@Col607 tinyint = NULL,@Col608 tinyint = NULL,@Col609 tinyint = NULL
,@Col610 tinyint = NULL,@Col611 tinyint = NULL,@Col612 tinyint = NULL,@Col613 tinyint = NULL,@Col614 tinyint = NULL,@Col615 tinyint = NULL,@Col616 tinyint = NULL
,@Col617 tinyint = NULL,@Col618 tinyint = NULL,@Col619 tinyint = NULL,@Col620 tinyint = NULL,@Col621 tinyint = NULL,@Col622 tinyint = NULL,@Col623 tinyint = NULL
,@Col624 tinyint = NULL,@Col625 tinyint = NULL,@Col626 tinyint = NULL,@Col627 tinyint = NULL,@Col628 tinyint = NULL,@Col629 tinyint = NULL,@Col630 tinyint = NULL
,@Col631 tinyint = NULL,@Col632 tinyint = NULL,@Col633 tinyint = NULL,@Col634 tinyint = NULL,@Col635 tinyint = NULL,@Col636 tinyint = NULL,@Col637 tinyint = NULL
,@Col638 tinyint = NULL,@Col639 tinyint = NULL,@Col640 tinyint = NULL,@Col641 tinyint = NULL,@Col642 tinyint = NULL,@Col643 tinyint = NULL,@Col644 tinyint = NULL
,@Col645 tinyint = NULL,@Col646 bit = NULL,@Col647 bit = NULL,@Col648 int = NULL,@Col649 int = NULL,@Col650 int = NULL,@Col651 bit = NULL
,@Col652 smallint = NULL,@Col653 bit = NULL,@Col654 bit = NULL,@Col655 int = NULL,@Col656 int = NULL,@Col657 int = NULL,@Col658 bit = NULL
,@Col659 smallint = NULL,@Col660 tinyint = NULL,@Col661 bit = NULL,@Col662 bit = NULL,@Col663 bit = NULL		
,@Col3000 bit = NULL,@Col3001 bit = NULL,@Col3002 bit = NULL,@Col3003 bit = NULL
,@Col3004 bit = NULL,@Col3005 bit = NULL,@Col3006 bit = NULL,@Col3007 bit = NULL,@Col3008 bit = NULL,@Col3009 bit = NULL,@Col3010 bit = NULL
,@Col3011 bit = NULL,@Col3012 bit = NULL,@Col3013 bit = NULL,@Col3014 bit = NULL,@Col3015 bit = NULL,@Col3016 bit = NULL,@Col3017 bit = NULL
,@Col3018 bit = NULL,@Col3019 bit = NULL,@Col3020 bit = NULL,@Col3021 bit = NULL,@Col3022 bit = NULL,@Col3023 bit = NULL,@Col3024 bit = NULL
,@Col3025 bit = NULL,@Col3026 bit = NULL,@Col3027 bit = NULL,@Col3028 bit = NULL,@Col3029 bit = NULL,@Col3030 bit = NULL,@Col3031 bit = NULL
,@Col3032 bit = NULL,@Col3033 bit = NULL,@Col3034 bit = NULL,@Col3035 bit = NULL,@Col3036 bit = NULL,@Col3037 bit = NULL,@Col3038 bit = NULL
,@Col3039 bit = NULL,@Col3040 bit = NULL,@Col3041 bit = NULL,@Col3042 bit = NULL,@Col3043 bit = NULL,@Col3044 bit = NULL,@Col3045 bit = NULL
,@Col3046 bit = NULL,@Col3047 bit = NULL,@Col3048 bit = NULL,@Col3049 bit = NULL,@Col3050 bit = NULL,@Col3051 bit = NULL,@Col3052 bit = NULL
,@Col3053 bit = NULL,@Col3054 bit = NULL,@Col3055 bit = NULL,@Col3056 bit = NULL,@Col3057 bit = NULL,@Col3058 bit = NULL,@Col3059 bit = NULL
,@Col3060 bit = NULL,@Col3061 bit = NULL,@Col3062 bit = NULL,@Col3063 bit = NULL,@Col3064 bit = NULL,@Col3065 bit = NULL,@Col3066 bit = NULL
,@Col3067 bit = NULL,@Col3068 bit = NULL,@Col3069 bit = NULL,@Col3070 bit = NULL,@Col3071 bit = NULL,@Col3072 bit = NULL,@Col3073 bit = NULL
,@Col3074 bit = NULL,@Col3075 bit = NULL,@Col3076 bit = NULL,@Col3077 bit = NULL,@Col3078 bit = NULL,@Col3079 bit = NULL,@Col3080 bit = NULL
,@Col3081 bit = NULL,@Col3082 bit = NULL,@Col3083 bit = NULL,@Col3084 bit = NULL,@Col3085 bit = NULL,@Col3086 bit = NULL,@Col3087 bit = NULL
,@Col3088 bit = NULL,@Col3089 bit = NULL,@Col3090 bit = NULL,@Col3091 bit = NULL,@Col3092 bit = NULL,@Col3093 bit = NULL,@Col3094 bit = NULL
,@Col3095 bit = NULL,@Col3096 bit = NULL,@Col3097 bit = NULL,@Col3098 bit = NULL,@Col3099 bit = NULL,@Col3100 bit = NULL,@Col3101 bit = NULL
,@Col3102 bit = NULL,@Col3103 bit = NULL,@Col3104 bit = NULL,@Col3105 bit = NULL,@Col3106 bit = NULL,@Col3107 bit = NULL,@Col3108 bit = NULL
,@Col3109 bit = NULL,@Col3110 bit = NULL,@Col3111 bit = NULL,@Col3112 bit = NULL,@Col3113 bit = NULL,@Col3114 bit = NULL,@Col3115 bit = NULL
,@Col3116 bit = NULL,@Col3117 bit = NULL,@Col3118 bit = NULL,@Col3119 bit = NULL,@Col3120 bit = NULL,@Col3121 bit = NULL,@Col3122 bit = NULL
,@Col3123 bit = NULL,@Col3124 bit = NULL,@Col3125 bit = NULL,@Col3126 bit = NULL,@Col3127 bit = NULL,@Col3128 bit = NULL,@Col3129 bit = NULL
,@Col3130 bit = NULL,@Col3131 bit = NULL,@Col3132 bit = NULL,@Col3133 bit = NULL,@Col3134 bit = NULL,@Col3135 bit = NULL,@Col3136 bit = NULL
,@Col3137 bit = NULL,@Col3138 bit = NULL,@Col3139 bit = NULL,@Col3140 bit = NULL,@Col3141 bit = NULL,@Col3142 bit = NULL,@Col3143 bit = NULL
,@Col3144 bit = NULL,@Col3145 bit = NULL,@Col3146 bit = NULL,@Col3147 bit = NULL,@Col3148 bit = NULL,@Col3149 bit = NULL,@Col3150 bit = NULL
,@Col3151 bit = NULL,@Col3152 bit = NULL,@Col3153 bit = NULL,@Col3154 bit = NULL,@Col3155 bit = NULL,@Col3156 bit = NULL,@Col3157 bit = NULL
,@Col3158 bit = NULL,@Col3159 bit = NULL,@Col3160 bit = NULL,@Col3161 bit = NULL,@Col3162 bit = NULL,@Col3163 bit = NULL,@Col3164 bit = NULL
,@Col3165 bit = NULL,@Col3166 bit = NULL,@Col3167 bit = NULL,@Col3168 bit = NULL,@Col3169 bit = NULL,@Col3170 bit = NULL,@Col3171 bit = NULL
,@Col3172 bit = NULL,@Col3173 bit = NULL,@Col3174 bit = NULL,@Col3175 bit = NULL,@Col3176 bit = NULL,@Col3177 bit = NULL,@Col3178 bit = NULL
,@Col3179 bit = NULL,@Col3180 bit = NULL,@Col3181 bit = NULL,@Col3182 bit = NULL,@Col3183 bit = NULL,@Col3184 bit = NULL,@Col3185 bit = NULL
,@Col3186 bit = NULL,@Col3187 bit = NULL,@Col3188 bit = NULL,@Col3189 bit = NULL,@Col3190 bit = NULL,@Col3191 bit = NULL,@Col3192 bit = NULL
,@Col3193 bit = NULL,@Col3194 bit = NULL,@Col3195 bit = NULL,@Col3196 bit = NULL,@Col3197 bit = NULL,@Col3198 bit = NULL,@Col3199 bit = NULL
,@Col3200 bit = NULL,@Col3201 bit = NULL,@Col3202 bit = NULL,@Col3203 bit = NULL,@Col3204 bit = NULL,@Col3205 bit = NULL,@Col3206 bit = NULL
,@Col3207 bit = NULL,@Col3208 bit = NULL,@Col3209 bit = NULL,@Col3210 bit = NULL,@Col3211 bit = NULL,@Col3212 bit = NULL,@Col3213 bit = NULL
,@Col3214 bit = NULL,@Col3215 bit = NULL,@Col3216 bit = NULL,@Col3217 bit = NULL,@Col3218 bit = NULL,@Col3219 bit = NULL,@Col3220 bit = NULL
,@Col3221 bit = NULL,@Col3222 bit = NULL,@Col3223 bit = NULL,@Col3224 bit = NULL,@Col3225 bit = NULL,@Col3226 bit = NULL,@Col3227 bit = NULL
,@Col3228 bit = NULL,@Col3229 bit = NULL,@Col3230 bit = NULL,@Col3231 bit = NULL,@Col3232 bit = NULL,@Col3233 bit = NULL,@Col3234 bit = NULL
,@Col3235 bit = NULL,@Col3236 bit = NULL,@Col3237 bit = NULL,@Col3238 bit = NULL,@Col3239 bit = NULL,@Col3240 bit = NULL,@Col3241 bit = NULL
,@Col3242 bit = NULL,@Col3243 bit = NULL,@Col3244 bit = NULL,@Col3245 bit = NULL,@Col3246 bit = NULL,@Col3247 bit = NULL,@Col3248 bit = NULL
,@Col3249 bit = NULL,@Col3250 bit = NULL,@Col3251 bit = NULL,@Col3252 bit = NULL,@Col3253 bit = NULL,@Col3254 bit = NULL,@Col3255 bit = NULL
,@Col3256 bit = NULL,@Col3257 bit = NULL,@Col3258 bit = NULL,@Col3259 bit = NULL,@Col3260 bit = NULL,@Col3261 bit = NULL,@Col3262 bit = NULL
,@Col3263 bit = NULL,@Col3264 bit = NULL,@Col3265 bit = NULL,@Col3266 bit = NULL,@Col3267 bit = NULL,@Col3268 bit = NULL,@Col3269 bit = NULL
,@Col3270 bit = NULL,@Col3271 bit = NULL,@Col3272 bit = NULL,@Col3273 bit = NULL,@Col3274 bit = NULL,@Col3275 bit = NULL,@Col3276 bit = NULL
,@Col3277 bit = NULL,@Col3278 bit = NULL,@Col3279 bit = NULL,@Col3280 bit = NULL,@Col3281 bit = NULL,@Col3282 bit = NULL,@Col3283 bit = NULL
,@Col3284 bit = NULL,@Col3285 bit = NULL,@Col3286 bit = NULL,@Col3287 bit = NULL,@Col3288 bit = NULL,@Col3289 bit = NULL,@Col3290 bit = NULL
,@Col3291 bit = NULL,@Col3292 bit = NULL,@Col3293 bit = NULL,@Col3294 bit = NULL,@Col3295 bit = NULL,@Col3296 bit = NULL,@Col3297 bit = NULL
,@Col3298 bit = NULL,@Col3299 bit = NULL,@Col3300 bit = NULL,@Col3301 bit = NULL,@Col3302 bit = NULL,@Col3303 bit = NULL,@Col3304 bit = NULL
,@Col3305 bit = NULL,@Col3306 bit = NULL,@Col3307 bit = NULL,@Col3308 bit = NULL,@Col3309 bit = NULL,@Col3310 bit = NULL,@Col3311 bit = NULL
,@Col3312 bit = NULL,@Col3313 bit = NULL,@Col3314 bit = NULL,@Col3315 bit = NULL,@Col3316 bit = NULL,@Col3317 bit = NULL,@Col3318 bit = NULL
,@Col3319 bit = NULL,@Col3320 bit = NULL,@Col3321 bit = NULL,@Col3322 bit = NULL,@Col3323 bit = NULL,@Col3324 bit = NULL,@Col3325 bit = NULL
,@Col3326 bit = NULL,@Col3327 bit = NULL,@Col3328 bit = NULL,@Col3329 bit = NULL,@Col3330 bit = NULL,@Col3331 bit = NULL,@Col3332 bit = NULL
,@Col3333 bit = NULL,@Col3334 bit = NULL,@Col3335 bit = NULL,@Col3336 bit = NULL,@Col3337 bit = NULL,@Col3338 bit = NULL,@Col3339 bit = NULL
,@Col3340 bit = NULL,@Col3341 bit = NULL,@Col3342 bit = NULL,@Col3343 bit = NULL,@Col3344 bit = NULL,@Col3345 bit = NULL,@Col3346 bit = NULL
,@Col3347 bit = NULL,@Col3348 bit = NULL,@Col3349 bit = NULL,@Col3350 bit = NULL,@Col3351 bit = NULL,@Col3352 bit = NULL,@Col3353 bit = NULL
,@Col3354 bit = NULL,@Col3355 bit = NULL,@Col3356 bit = NULL,@Col3357 bit = NULL,@Col3358 bit = NULL,@Col3359 bit = NULL,@Col3360 bit = NULL
,@Col3361 bit = NULL,@Col3362 bit = NULL,@Col3363 bit = NULL,@Col3364 bit = NULL,@Col3365 bit = NULL,@Col3366 bit = NULL,@Col3367 bit = NULL
,@Col3368 bit = NULL,@Col3369 bit = NULL,@Col3370 bit = NULL,@Col3371 bit = NULL,@Col3372 bit = NULL,@Col3373 bit = NULL,@Col3374 bit = NULL
,@Col3375 bit = NULL,@Col3376 bit = NULL,@Col3377 bit = NULL,@Col3378 bit = NULL,@Col3379 bit = NULL,@Col3380 bit = NULL,@Col3381 bit = NULL
,@Col3382 bit = NULL,@Col3383 bit = NULL,@Col3384 bit = NULL,@Col3385 bit = NULL,@Col3386 bit = NULL,@Col3387 bit = NULL,@Col3388 bit = NULL
,@Col3389 bit = NULL,@Col3390 bit = NULL,@Col3391 bit = NULL,@Col3392 bit = NULL,@Col3393 bit = NULL,@Col3394 bit = NULL,@Col3395 bit = NULL
,@Col3396 bit = NULL,@Col3397 bit = NULL,@Col3398 bit = NULL,@Col3399 bit = NULL,@Col3400 bit = NULL,@Col3401 bit = NULL,@Col3402 bit = NULL
,@Col3403 bit = NULL,@Col3404 bit = NULL,@Col3405 bit = NULL,@Col3406 bit = NULL,@Col3407 bit = NULL,@Col3408 bit = NULL,@Col3409 bit = NULL
,@Col3410 bit = NULL,@Col3411 bit = NULL,@Col3412 bit = NULL,@Col3413 bit = NULL,@Col3414 bit = NULL,@Col3415 bit = NULL,@Col3416 bit = NULL
,@Col3417 bit = NULL,@Col3418 bit = NULL,@Col3419 bit = NULL,@Col3420 bit = NULL,@Col3421 bit = NULL,@Col3422 bit = NULL,@Col3423 bit = NULL
,@Col3424 bit = NULL,@Col3425 bit = NULL,@Col3426 bit = NULL,@Col3427 bit = NULL,@Col3428 bit = NULL,@Col3429 bit = NULL,@Col3430 bit = NULL
,@Col3431 bit = NULL,@Col3432 bit = NULL,@Col3433 bit = NULL,@Col3434 bit = NULL,@Col3435 bit = NULL,@Col3436 bit = NULL,@Col3437 bit = NULL
,@Col3438 bit = NULL,@Col3439 bit = NULL,@Col3440 bit = NULL,@Col3441 bit = NULL,@Col3442 bit = NULL,@Col3443 bit = NULL,@Col3444 bit = NULL
,@Col3445 bit = NULL,@Col3446 bit = NULL,@Col3447 bit = NULL,@Col3448 bit = NULL,@Col3449 bit = NULL,@Col3450 bit = NULL,@Col3451 bit = NULL
,@Col3452 bit = NULL,@Col3453 bit = NULL,@Col3454 bit = NULL,@Col3455 bit = NULL,@Col3456 bit = NULL,@Col3457 bit = NULL,@Col3458 bit = NULL
,@Col3459 bit = NULL,@Col3460 bit = NULL,@Col3461 bit = NULL,@Col3462 bit = NULL,@Col3463 bit = NULL,@Col3464 bit = NULL,@Col3465 bit = NULL
,@Col3466 bit = NULL,@Col3467 bit = NULL,@Col3468 bit = NULL,@Col3469 bit = NULL,@Col3470 bit = NULL,@Col3471 bit = NULL,@Col3472 bit = NULL
,@Col3473 bit = NULL,@Col3474 bit = NULL,@Col3475 bit = NULL,@Col3476 bit = NULL,@Col3477 bit = NULL,@Col3478 bit = NULL,@Col3479 bit = NULL
,@Col3480 bit = NULL,@Col3481 bit = NULL,@Col3482 bit = NULL,@Col3483 bit = NULL,@Col3484 bit = NULL,@Col3485 bit = NULL,@Col3486 bit = NULL
,@Col3487 bit = NULL,@Col3488 bit = NULL,@Col3489 bit = NULL,@Col3490 bit = NULL,@Col3491 bit = NULL,@Col3492 bit = NULL,@Col3493 bit = NULL
,@Col3494 bit = NULL,@Col3495 bit = NULL,@Col3496 bit = NULL,@Col3497 bit = NULL,@Col3498 bit = NULL,@Col3499 bit = NULL,@Col3500 bit = NULL
,@Col3501 bit = NULL,@Col3502 bit = NULL,@Col3503 bit = NULL,@Col3504 bit = NULL,@Col3505 bit = NULL,@Col3506 bit = NULL,@Col3507 bit = NULL
,@Col3508 bit = NULL,@Col3509 bit = NULL,@Col3510 bit = NULL,@Col3511 bit = NULL,@Col3512 bit = NULL,@Col3513 bit = NULL,@Col3514 bit = NULL
,@Col3515 bit = NULL,@Col3516 bit = NULL,@Col3517 bit = NULL,@Col3518 bit = NULL,@Col3519 bit = NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[col9],[col10],[col11],[col12],[col13],[col14],[col15],[col16]
,[col17],[col18],[col19],[col20],[col21],[col22],[col23],[col24],[col25],[col26],[col27],[col28],[col29],[col30],[col31],[col32]
,[col33],[col34],[col35],[col36],[col37],[col38],[col39],[col40],[col41],[col42],[col43],[col44],[col45],[col46],[col47],[col48]
,[col49],[col50],[col51],[col52],[col53],[col54],[col55],[col56],[col57],[col58],[col59],[col60],[col61],[col62],[col63],[col64]
,[col65],[col66],[col67],[col68],[col69],[col70],[col71],[col72],[col73],[col74],[col75],[col76],[col77],[col78],[col79],[col80]
,[col81],[col82],[col83],[col84],[col85],[col86],[col87],[col88],[col89],[col90],[col91],[col92],[col93],[col94],[col95],[col96]
,[col97],[col98],[col99],[col100],[col101],[col102],[col103],[col104],[col105]
,[col106],[col107],[col108],[col109],[col110],[col111],[col112]
,[col113],[col114],[col115],[col116],[col117],[col118],[col119]
,[col120],[col121],[col122],[col123],[col124],[col125],[col126]
,[col127],[col128],[col129],[col130],[col131],[col132],[col133]
,[col134],[col135],[col136],[col137],[col138],[col139],[col140]
,[col141],[col142],[col143],[col144],[col145],[col146],[col147]
,[col148],[col149],[col150],[col151],[col152],[col153],[col154]
,[col155],[col156],[col157],[col158],[col159],[col160],[col161]
,[col162],[col163],[col164],[col165],[col166],[col167],[col168]
,[col169],[col170],[col171],[col172],[col173],[col174],[col175]
,[col176],[col177],[col178],[col179],[col180],[col181],[col182]
,[col183],[col184],[col185],[col186],[col187],[col188],[col189]
,[col190],[col191],[col192],[col193],[col194],[col195],[col196]
,[col197],[col198],[col199],[col200],[col201],[col202],[col203]
,[col204],[col205],[col206],[col207],[col208],[col209],[col210]
,[col211],[col212],[col213],[col214],[col215],[col216],[col217]
,[col218],[col219],[col220],[col221],[col222],[col223],[col224]
,[col225],[col226],[col227],[col228],[col229],[col230],[col231]
,[col232],[col233],[col234],[col235],[col236],[col237],[col238]
,[col239],[col240],[col241],[col242],[col243],[col244],[col245]
,[col246],[col247],[col248],[col249],[col250],[col251],[col252]
,[col253],[col254],[col255],[col256],[col257],[col258],[col259]
,[col260],[col261],[col262],[col263],[col264],[col265],[col266]
,[col267],[col268],[col269],[col270],[col271],[col272],[col273]
,[col274],[col275],[col276],[col277],[col278],[col279],[col280]
,[col281],[col282],[col283],[col284],[col285],[col286],[col287]
,[col288],[col289],[col290],[col291],[col292],[col293],[col294]
,[col295],[col296],[col297],[col298],[col299],[col300],[col301]
,[col302],[col303],[col304],[col305],[col306],[col307],[col308]
,[col309],[col310],[col311],[col312],[col313],[col314],[col315]
,[col316],[col317],[col318],[col319],[col320],[col321],[col322]
,[col323],[col324],[col325],[col326],[col327],[col328],[col329]
,[col330],[col331],[col332],[col333],[col334],[col335],[col336]
,[col337],[col338],[col339],[col340],[col341],[col342],[col343]
,[col344],[col345],[col346],[col347],[col348],[col349],[col350]
,[col351],[col352],[col353],[col354],[col355],[col356],[col357]
,[col358],[col359],[col360],[col361],[col362],[col363],[col364]
,[col365],[col366],[col367],[col368],[col369],[col370],[col371]
,[col372],[col373],[col374],[col375],[col376],[col377],[col378]
,[col379],[col380],[col381],[col382],[col383],[col384],[col385]
,[col386],[col387],[col388],[col389],[col390],[col391],[col392]
,[col393],[col394],[col395],[col396],[col397],[col398],[col399]
,[col400],[col401],[col402],[col403],[col404],[col405],[col406]
,[col407],[col408],[col409],[col410],[col411],[col412],[col413]
,[col414],[col415],[col416],[col417],[col418],[col419],[col420]
,[col421],[col422],[col423],[col424],[col425],[col426],[col427]
,[col428],[col429],[col430],[col431],[col432],[col433],[col434]
,[col435],[col436],[col437],[col438],[col439],[col440],[col441]
,[col442],[col443],[col444],[col445],[col446],[col447],[col448]
,[col449],[col450],[col451],[col452],[col453],[col454],[col455]
,[col456],[col457],[col458],[col459],[col460],[col461],[col462]
,[col463],[col464],[col465],[col466],[col467],[col468],[col469]
,[col470],[col471],[col472],[col473],[col474],[col475],[col476]
,[col477],[col478],[col479],[col480],[col481],[col482],[col483]
,[col484],[col485],[col486],[col487],[col488],[col489],[col490]
,[col491],[col492],[col493],[col494],[col495],[col496],[col497]
,[col498],[col499],[col500],[col501],[col502],[col503],[col504]
,[col505],[col506],[col507],[col508],[col509],[col510],[col511]
,[col512],[col513],[col514],[col515],[col516],[col517],[col518]
,[col519],[col520],[col521],[col522],[col523],[col524],[col525]
,[col526],[col527],[col528],[col529],[col530],[col531],[col532]
,[col533],[col534],[col535],[col536],[col537],[col538],[col539]
,[col540],[col541],[col542],[col543],[col544],[col545],[col546]
,[col547],[col548],[col549],[col550],[col551],[col552],[col553]
,[col554],[col555],[col556],[col557],[col558],[col559],[col560]
,[col561],[col562],[col563],[col564],[col565],[col566],[col567]
,[col568],[col569],[col570],[col571],[col572],[col573],[col574]
,[col575],[col576],[col577],[col578],[col579],[col580],[col581]
,[col582],[col583],[col584],[col585],[col586],[col587],[col588]
,[col589],[col590],[col591],[col592],[col593],[col594],[col595]
,[col596],[col597],[col598],[col599],[col600],[col601],[col602]
,[col603],[col604],[col605],[col606],[col607],[col608],[col609]
,[col610],[col611],[col612],[col613],[col614],[col615],[col616]
,[col617],[col618],[col619],[col620],[col621],[col622],[col623]
,[col624],[col625],[col626],[col627],[col628],[col629],[col630]
,[col631],[col632],[col633],[col634],[col635],[col636],[col637]
,[col638],[col639],[col640],[col641],[col642],[col643],[col644]
,[col645],[col646],[col647],[col648],[col649],[col650],[col651]
,[col652],[col653],[col654],[col655],[col656],[col657],[col658]
,[col659],[col660],[col661],[col662],[col663]
           )
	OUTPUT inserted.ID INTO @IDs(ID) 
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@Col1,COL1),ISNULL(@Col2,COL2),ISNULL(@Col3,COL3),ISNULL(@Col4,COL4),ISNULL(@Col5,COL5),ISNULL(@Col6,COL6),ISNULL(@Col7,COL7)
,ISNULL(@Col8,COL8),ISNULL(@Col9,COL9),ISNULL(@Col10,COL10),ISNULL(@Col11,COL11),ISNULL(@Col12,COL12),ISNULL(@Col13,COL13),ISNULL(@Col14,COL14)
,ISNULL(@Col15,COL15),ISNULL(@Col16,COL16),ISNULL(@Col17,COL17),ISNULL(@Col18,COL18),ISNULL(@Col19,COL19),ISNULL(@Col20,COL20),ISNULL(@Col21,COL21)
,ISNULL(@Col22,COL22),ISNULL(@Col23,COL23),ISNULL(@Col24,COL24),ISNULL(@Col25,COL25),ISNULL(@Col26,COL26),ISNULL(@Col27,COL27),ISNULL(@Col28,COL28)
,ISNULL(@Col29,COL29),ISNULL(@Col30,COL30),ISNULL(@Col31,COL31),ISNULL(@Col32,COL32),ISNULL(@Col33,COL33),ISNULL(@Col34,COL34),ISNULL(@Col35,COL35)
,ISNULL(@Col36,COL36),ISNULL(@Col37,COL37),ISNULL(@Col38,COL38),ISNULL(@Col39,COL39),ISNULL(@Col40,COL40),ISNULL(@Col41,COL41),ISNULL(@Col42,COL42)
,ISNULL(@Col43,COL43),ISNULL(@Col44,COL44),ISNULL(@Col45,COL45),ISNULL(@Col46,COL46),ISNULL(@Col47,COL47),ISNULL(@Col48,COL48),ISNULL(@Col49,COL49)
,ISNULL(@Col50,COL50),ISNULL(@Col51,COL51),ISNULL(@Col52,COL52),ISNULL(@Col53,COL53),ISNULL(@Col54,COL54),ISNULL(@Col55,COL55),ISNULL(@Col56,COL56)
,ISNULL(@Col57,COL57),ISNULL(@Col58,COL58),ISNULL(@Col59,COL59),ISNULL(@Col60,COL60),ISNULL(@Col61,COL61),ISNULL(@Col62,COL62),ISNULL(@Col63,COL63)
,ISNULL(@Col64,COL64),ISNULL(@Col65,COL65),ISNULL(@Col66,COL66),ISNULL(@Col67,COL67),ISNULL(@Col68,COL68),ISNULL(@Col69,COL69),ISNULL(@Col70,COL70)
,ISNULL(@Col71,COL71),ISNULL(@Col72,COL72),ISNULL(@Col73,COL73),ISNULL(@Col74,COL74),ISNULL(@Col75,COL75),ISNULL(@Col76,COL76),ISNULL(@Col77,COL77)
,ISNULL(@Col78,COL78),ISNULL(@Col79,COL79),ISNULL(@Col80,COL80),ISNULL(@Col81,COL81),ISNULL(@Col82,COL82),ISNULL(@Col83,COL83),ISNULL(@Col84,COL84)
,ISNULL(@Col85,COL85),ISNULL(@Col86,COL86),ISNULL(@Col87,COL87),ISNULL(@Col88,COL88),ISNULL(@Col89,COL89),ISNULL(@Col90,COL90),ISNULL(@Col91,COL91)
,ISNULL(@Col92,COL92),ISNULL(@Col93,COL93),ISNULL(@Col94,COL94),ISNULL(@Col95,COL95),ISNULL(@Col96,COL96),ISNULL(@Col97,COL97),ISNULL(@Col98,COL98)
,ISNULL(@Col99,COL99),ISNULL(@Col100,COL100),ISNULL(@Col101,COL101),ISNULL(@Col102,COL102),ISNULL(@Col103,COL103),ISNULL(@Col104,COL104),ISNULL(@Col105,COL105)
,ISNULL(@Col106,COL106),ISNULL(@Col107,COL107),ISNULL(@Col108,COL108),ISNULL(@Col109,COL109),ISNULL(@Col110,COL110),ISNULL(@Col111,COL111),ISNULL(@Col112,COL112)
,ISNULL(@Col113,COL113),ISNULL(@Col114,COL114),ISNULL(@Col115,COL115),ISNULL(@Col116,COL116),ISNULL(@Col117,COL117),ISNULL(@Col118,COL118),ISNULL(@Col119,COL119)
,ISNULL(@Col120,COL120),ISNULL(@Col121,COL121),ISNULL(@Col122,COL122),ISNULL(@Col123,COL123),ISNULL(@Col124,COL124),ISNULL(@Col125,COL125),ISNULL(@Col126,COL126)
,ISNULL(@Col127,COL127),ISNULL(@Col128,COL128),ISNULL(@Col129,COL129),ISNULL(@Col130,COL130),ISNULL(@Col131,COL131),ISNULL(@Col132,COL132),ISNULL(@Col133,COL133)
,ISNULL(@Col134,COL134),ISNULL(@Col135,COL135),ISNULL(@Col136,COL136),ISNULL(@Col137,COL137),ISNULL(@Col138,COL138),ISNULL(@Col139,COL139),ISNULL(@Col140,COL140)
,ISNULL(@Col141,COL141),ISNULL(@Col142,COL142),ISNULL(@Col143,COL143),ISNULL(@Col144,COL144),ISNULL(@Col145,COL145),ISNULL(@Col146,COL146),ISNULL(@Col147,COL147)
,ISNULL(@Col148,COL148),ISNULL(@Col149,COL149),ISNULL(@Col150,COL150),ISNULL(@Col151,COL151),ISNULL(@Col152,COL152),ISNULL(@Col153,COL153),ISNULL(@Col154,COL154)
,ISNULL(@Col155,COL155),ISNULL(@Col156,COL156),ISNULL(@Col157,COL157),ISNULL(@Col158,COL158),ISNULL(@Col159,COL159),ISNULL(@Col160,COL160),ISNULL(@Col161,COL161)
,ISNULL(@Col162,COL162),ISNULL(@Col163,COL163),ISNULL(@Col164,COL164),ISNULL(@Col165,COL165),ISNULL(@Col166,COL166),ISNULL(@Col167,COL167),ISNULL(@Col168,COL168)
,ISNULL(@Col169,COL169),ISNULL(@Col170,COL170),ISNULL(@Col171,COL171),ISNULL(@Col172,COL172),ISNULL(@Col173,COL173),ISNULL(@Col174,COL174),ISNULL(@Col175,COL175)
,ISNULL(@Col176,COL176),ISNULL(@Col177,COL177),ISNULL(@Col178,COL178),ISNULL(@Col179,COL179),ISNULL(@Col180,COL180),ISNULL(@Col181,COL181),ISNULL(@Col182,COL182)
,ISNULL(@Col183,COL183),ISNULL(@Col184,COL184),ISNULL(@Col185,COL185),ISNULL(@Col186,COL186),ISNULL(@Col187,COL187),ISNULL(@Col188,COL188),ISNULL(@Col189,COL189)
,ISNULL(@Col190,COL190),ISNULL(@Col191,COL191),ISNULL(@Col192,COL192),ISNULL(@Col193,COL193),ISNULL(@Col194,COL194),ISNULL(@Col195,COL195),ISNULL(@Col196,COL196)
,ISNULL(@Col197,COL197),ISNULL(@Col198,COL198),ISNULL(@Col199,COL199),ISNULL(@Col200,COL200),ISNULL(@Col201,COL201),ISNULL(@Col202,COL202),ISNULL(@Col203,COL203)
,ISNULL(@Col204,COL204),ISNULL(@Col205,COL205),ISNULL(@Col206,COL206),ISNULL(@Col207,COL207),ISNULL(@Col208,COL208),ISNULL(@Col209,COL209),ISNULL(@Col210,COL210)
,ISNULL(@Col211,COL211),ISNULL(@Col212,COL212),ISNULL(@Col213,COL213),ISNULL(@Col214,COL214),ISNULL(@Col215,COL215),ISNULL(@Col216,COL216),ISNULL(@Col217,COL217)
,ISNULL(@Col218,COL218),ISNULL(@Col219,COL219),ISNULL(@Col220,COL220),ISNULL(@Col221,COL221),ISNULL(@Col222,COL222),ISNULL(@Col223,COL223),ISNULL(@Col224,COL224)
,ISNULL(@Col225,COL225),ISNULL(@Col226,COL226),ISNULL(@Col227,COL227),ISNULL(@Col228,COL228),ISNULL(@Col229,COL229),ISNULL(@Col230,COL230),ISNULL(@Col231,COL231)
,ISNULL(@Col232,COL232),ISNULL(@Col233,COL233),ISNULL(@Col234,COL234),ISNULL(@Col235,COL235),ISNULL(@Col236,COL236),ISNULL(@Col237,COL237),ISNULL(@Col238,COL238)
,ISNULL(@Col239,COL239),ISNULL(@Col240,COL240),ISNULL(@Col241,COL241),ISNULL(@Col242,COL242),ISNULL(@Col243,COL243),ISNULL(@Col244,COL244),ISNULL(@Col245,COL245)
,ISNULL(@Col246,COL246),ISNULL(@Col247,COL247),ISNULL(@Col248,COL248),ISNULL(@Col249,COL249),ISNULL(@Col250,COL250),ISNULL(@Col251,COL251),ISNULL(@Col252,COL252)
,ISNULL(@Col253,COL253),ISNULL(@Col254,COL254),ISNULL(@Col255,COL255),ISNULL(@Col256,COL256),ISNULL(@Col257,COL257),ISNULL(@Col258,COL258),ISNULL(@Col259,COL259)
,ISNULL(@Col260,COL260),ISNULL(@Col261,COL261),ISNULL(@Col262,COL262),ISNULL(@Col263,COL263),ISNULL(@Col264,COL264),ISNULL(@Col265,COL265),ISNULL(@Col266,COL266)
,ISNULL(@Col267,COL267),ISNULL(@Col268,COL268),ISNULL(@Col269,COL269),ISNULL(@Col270,COL270),ISNULL(@Col271,COL271),ISNULL(@Col272,COL272),ISNULL(@Col273,COL273)
,ISNULL(@Col274,COL274),ISNULL(@Col275,COL275),ISNULL(@Col276,COL276),ISNULL(@Col277,COL277),ISNULL(@Col278,COL278),ISNULL(@Col279,COL279),ISNULL(@Col280,COL280)
,ISNULL(@Col281,COL281),ISNULL(@Col282,COL282),ISNULL(@Col283,COL283),ISNULL(@Col284,COL284),ISNULL(@Col285,COL285),ISNULL(@Col286,COL286),ISNULL(@Col287,COL287)
,ISNULL(@Col288,COL288),ISNULL(@Col289,COL289),ISNULL(@Col290,COL290),ISNULL(@Col291,COL291),ISNULL(@Col292,COL292),ISNULL(@Col293,COL293),ISNULL(@Col294,COL294)
,ISNULL(@Col295,COL295),ISNULL(@Col296,COL296),ISNULL(@Col297,COL297),ISNULL(@Col298,COL298),ISNULL(@Col299,COL299),ISNULL(@Col300,COL300),ISNULL(@Col301,COL301)
,ISNULL(@Col302,COL302),ISNULL(@Col303,COL303),ISNULL(@Col304,COL304),ISNULL(@Col305,COL305),ISNULL(@Col306,COL306),ISNULL(@Col307,COL307),ISNULL(@Col308,COL308)
,ISNULL(@Col309,COL309),ISNULL(@Col310,COL310),ISNULL(@Col311,COL311),ISNULL(@Col312,COL312),ISNULL(@Col313,COL313),ISNULL(@Col314,COL314),ISNULL(@Col315,COL315)
,ISNULL(@Col316,COL316),ISNULL(@Col317,COL317),ISNULL(@Col318,COL318),ISNULL(@Col319,COL319),ISNULL(@Col320,COL320),ISNULL(@Col321,COL321),ISNULL(@Col322,COL322)
,ISNULL(@Col323,COL323),ISNULL(@Col324,COL324),ISNULL(@Col325,COL325),ISNULL(@Col326,COL326),ISNULL(@Col327,COL327),ISNULL(@Col328,COL328),ISNULL(@Col329,COL329)
,ISNULL(@Col330,COL330),ISNULL(@Col331,COL331),ISNULL(@Col332,COL332),ISNULL(@Col333,COL333),ISNULL(@Col334,COL334),ISNULL(@Col335,COL335),ISNULL(@Col336,COL336)
,ISNULL(@Col337,COL337),ISNULL(@Col338,COL338),ISNULL(@Col339,COL339),ISNULL(@Col340,COL340),ISNULL(@Col341,COL341),ISNULL(@Col342,COL342),ISNULL(@Col343,COL343)
,ISNULL(@Col344,COL344),ISNULL(@Col345,COL345),ISNULL(@Col346,COL346),ISNULL(@Col347,COL347),ISNULL(@Col348,COL348),ISNULL(@Col349,COL349),ISNULL(@Col350,COL350)
,ISNULL(@Col351,COL351),ISNULL(@Col352,COL352),ISNULL(@Col353,COL353),ISNULL(@Col354,COL354),ISNULL(@Col355,COL355),ISNULL(@Col356,COL356),ISNULL(@Col357,COL357)
,ISNULL(@Col358,COL358),ISNULL(@Col359,COL359),ISNULL(@Col360,COL360),ISNULL(@Col361,COL361),ISNULL(@Col362,COL362),ISNULL(@Col363,COL363),ISNULL(@Col364,COL364)
,ISNULL(@Col365,COL365),ISNULL(@Col366,COL366),ISNULL(@Col367,COL367),ISNULL(@Col368,COL368),ISNULL(@Col369,COL369),ISNULL(@Col370,COL370),ISNULL(@Col371,COL371)
,ISNULL(@Col372,COL372),ISNULL(@Col373,COL373),ISNULL(@Col374,COL374),ISNULL(@Col375,COL375),ISNULL(@Col376,COL376),ISNULL(@Col377,COL377),ISNULL(@Col378,COL378)
,ISNULL(@Col379,COL379),ISNULL(@Col380,COL380),ISNULL(@Col381,COL381),ISNULL(@Col382,COL382),ISNULL(@Col383,COL383),ISNULL(@Col384,COL384),ISNULL(@Col385,COL385)
,ISNULL(@Col386,COL386),ISNULL(@Col387,COL387),ISNULL(@Col388,COL388),ISNULL(@Col389,COL389),ISNULL(@Col390,COL390),ISNULL(@Col391,COL391),ISNULL(@Col392,COL392)
,ISNULL(@Col393,COL393),ISNULL(@Col394,COL394),ISNULL(@Col395,COL395),ISNULL(@Col396,COL396),ISNULL(@Col397,COL397),ISNULL(@Col398,COL398),ISNULL(@Col399,COL399)
,ISNULL(@Col400,COL400),ISNULL(@Col401,COL401),ISNULL(@Col402,COL402),ISNULL(@Col403,COL403),ISNULL(@Col404,COL404),ISNULL(@Col405,COL405),ISNULL(@Col406,COL406)
,ISNULL(@Col407,COL407),ISNULL(@Col408,COL408),ISNULL(@Col409,COL409),ISNULL(@Col410,COL410),ISNULL(@Col411,COL411),ISNULL(@Col412,COL412),ISNULL(@Col413,COL413)
,ISNULL(@Col414,COL414),ISNULL(@Col415,COL415),ISNULL(@Col416,COL416),ISNULL(@Col417,COL417),ISNULL(@Col418,COL418),ISNULL(@Col419,COL419),ISNULL(@Col420,COL420)
,ISNULL(@Col421,COL421),ISNULL(@Col422,COL422),ISNULL(@Col423,COL423),ISNULL(@Col424,COL424),ISNULL(@Col425,COL425),ISNULL(@Col426,COL426),ISNULL(@Col427,COL427)
,ISNULL(@Col428,COL428),ISNULL(@Col429,COL429),ISNULL(@Col430,COL430),ISNULL(@Col431,COL431),ISNULL(@Col432,COL432),ISNULL(@Col433,COL433),ISNULL(@Col434,COL434)
,ISNULL(@Col435,COL435),ISNULL(@Col436,COL436),ISNULL(@Col437,COL437),ISNULL(@Col438,COL438),ISNULL(@Col439,COL439),ISNULL(@Col440,COL440),ISNULL(@Col441,COL441)
,ISNULL(@Col442,COL442),ISNULL(@Col443,COL443),ISNULL(@Col444,COL444),ISNULL(@Col445,COL445),ISNULL(@Col446,COL446),ISNULL(@Col447,COL447),ISNULL(@Col448,COL448)
,ISNULL(@Col449,COL449),ISNULL(@Col450,COL450),ISNULL(@Col451,COL451),ISNULL(@Col452,COL452),ISNULL(@Col453,COL453),ISNULL(@Col454,COL454),ISNULL(@Col455,COL455)
,ISNULL(@Col456,COL456),ISNULL(@Col457,COL457),ISNULL(@Col458,COL458),ISNULL(@Col459,COL459),ISNULL(@Col460,COL460),ISNULL(@Col461,COL461),ISNULL(@Col462,COL462)
,ISNULL(@Col463,COL463),ISNULL(@Col464,COL464),ISNULL(@Col465,COL465),ISNULL(@Col466,COL466),ISNULL(@Col467,COL467),ISNULL(@Col468,COL468),ISNULL(@Col469,COL469)
,ISNULL(@Col470,COL470),ISNULL(@Col471,COL471),ISNULL(@Col472,COL472),ISNULL(@Col473,COL473),ISNULL(@Col474,COL474),ISNULL(@Col475,COL475),ISNULL(@Col476,COL476)
,ISNULL(@Col477,COL477),ISNULL(@Col478,COL478),ISNULL(@Col479,COL479),ISNULL(@Col480,COL480),ISNULL(@Col481,COL481),ISNULL(@Col482,COL482),ISNULL(@Col483,COL483)
,ISNULL(@Col484,COL484),ISNULL(@Col485,COL485),ISNULL(@Col486,COL486),ISNULL(@Col487,COL487),ISNULL(@Col488,COL488),ISNULL(@Col489,COL489),ISNULL(@Col490,COL490)
,ISNULL(@Col491,COL491),ISNULL(@Col492,COL492),ISNULL(@Col493,COL493),ISNULL(@Col494,COL494),ISNULL(@Col495,COL495),ISNULL(@Col496,COL496),ISNULL(@Col497,COL497)
,ISNULL(@Col498,COL498),ISNULL(@Col499,COL499),ISNULL(@Col500,COL500),ISNULL(@Col501,COL501),ISNULL(@Col502,COL502),ISNULL(@Col503,COL503),ISNULL(@Col504,COL504)
,ISNULL(@Col505,COL505),ISNULL(@Col506,COL506),ISNULL(@Col507,COL507),ISNULL(@Col508,COL508),ISNULL(@Col509,COL509),ISNULL(@Col510,COL510),ISNULL(@Col511,COL511)
,ISNULL(@Col512,COL512),ISNULL(@Col513,COL513),ISNULL(@Col514,COL514),ISNULL(@Col515,COL515),ISNULL(@Col516,COL516),ISNULL(@Col517,COL517),ISNULL(@Col518,COL518)
,ISNULL(@Col519,COL519),ISNULL(@Col520,COL520),ISNULL(@Col521,COL521),ISNULL(@Col522,COL522),ISNULL(@Col523,COL523),ISNULL(@Col524,COL524),ISNULL(@Col525,COL525)
,ISNULL(@Col526,COL526),ISNULL(@Col527,COL527),ISNULL(@Col528,COL528),ISNULL(@Col529,COL529),ISNULL(@Col530,COL530),ISNULL(@Col531,COL531),ISNULL(@Col532,COL532)
,ISNULL(@Col533,COL533),ISNULL(@Col534,COL534),ISNULL(@Col535,COL535),ISNULL(@Col536,COL536),ISNULL(@Col537,COL537),ISNULL(@Col538,COL538),ISNULL(@Col539,COL539)
,ISNULL(@Col540,COL540),ISNULL(@Col541,COL541),ISNULL(@Col542,COL542),ISNULL(@Col543,COL543),ISNULL(@Col544,COL544),ISNULL(@Col545,COL545),ISNULL(@Col546,COL546)
,ISNULL(@Col547,COL547),ISNULL(@Col548,COL548),ISNULL(@Col549,COL549),ISNULL(@Col550,COL550),ISNULL(@Col551,COL551),ISNULL(@Col552,COL552),ISNULL(@Col553,COL553)
,ISNULL(@Col554,COL554),ISNULL(@Col555,COL555),ISNULL(@Col556,COL556),ISNULL(@Col557,COL557),ISNULL(@Col558,COL558),ISNULL(@Col559,COL559),ISNULL(@Col560,COL560)
,ISNULL(@Col561,COL561),ISNULL(@Col562,COL562),ISNULL(@Col563,COL563),ISNULL(@Col564,COL564),ISNULL(@Col565,COL565),ISNULL(@Col566,COL566),ISNULL(@Col567,COL567)
,ISNULL(@Col568,COL568),ISNULL(@Col569,COL569),ISNULL(@Col570,COL570),ISNULL(@Col571,COL571),ISNULL(@Col572,COL572),ISNULL(@Col573,COL573),ISNULL(@Col574,COL574)
,ISNULL(@Col575,COL575),ISNULL(@Col576,COL576),ISNULL(@Col577,COL577),ISNULL(@Col578,COL578),ISNULL(@Col579,COL579),ISNULL(@Col580,COL580),ISNULL(@Col581,COL581)
,ISNULL(@Col582,COL582),ISNULL(@Col583,COL583),ISNULL(@Col584,COL584),ISNULL(@Col585,COL585),ISNULL(@Col586,COL586),ISNULL(@Col587,COL587),ISNULL(@Col588,COL588)
,ISNULL(@Col589,COL589),ISNULL(@Col590,COL590),ISNULL(@Col591,COL591),ISNULL(@Col592,COL592),ISNULL(@Col593,COL593),ISNULL(@Col594,COL594),ISNULL(@Col595,COL595)
,ISNULL(@Col596,COL596),ISNULL(@Col597,COL597),ISNULL(@Col598,COL598),ISNULL(@Col599,COL599),ISNULL(@Col600,COL600),ISNULL(@Col601,COL601),ISNULL(@Col602,COL602)
,ISNULL(@Col603,COL603),ISNULL(@Col604,COL604),ISNULL(@Col605,COL605),ISNULL(@Col606,COL606),ISNULL(@Col607,COL607),ISNULL(@Col608,COL608),ISNULL(@Col609,COL609)
,ISNULL(@Col610,COL610),ISNULL(@Col611,COL611),ISNULL(@Col612,COL612),ISNULL(@Col613,COL613),ISNULL(@Col614,COL614),ISNULL(@Col615,COL615),ISNULL(@Col616,COL616)
,ISNULL(@Col617,COL617),ISNULL(@Col618,COL618),ISNULL(@Col619,COL619),ISNULL(@Col620,COL620),ISNULL(@Col621,COL621),ISNULL(@Col622,COL622),ISNULL(@Col623,COL623)
,ISNULL(@Col624,COL624),ISNULL(@Col625,COL625),ISNULL(@Col626,COL626),ISNULL(@Col627,COL627),ISNULL(@Col628,COL628),ISNULL(@Col629,COL629),ISNULL(@Col630,COL630)
,ISNULL(@Col631,COL631),ISNULL(@Col632,COL632),ISNULL(@Col633,COL633),ISNULL(@Col634,COL634),ISNULL(@Col635,COL635),ISNULL(@Col636,COL636),ISNULL(@Col637,COL637)
,ISNULL(@Col638,COL638),ISNULL(@Col639,COL639),ISNULL(@Col640,COL640),ISNULL(@Col641,COL641),ISNULL(@Col642,COL642),ISNULL(@Col643,COL643),ISNULL(@Col644,COL644)
,ISNULL(@Col645,COL645),ISNULL(@Col646,COL646),ISNULL(@Col647,COL647),ISNULL(@Col648,COL648),ISNULL(@Col649,COL649),ISNULL(@Col650,COL650),ISNULL(@Col651,COL651)
,ISNULL(@Col652,COL652),ISNULL(@Col653,COL653),ISNULL(@Col654,COL654),ISNULL(@Col655,COL655),ISNULL(@Col656,COL656),ISNULL(@Col657,COL657),ISNULL(@Col658,COL658)
,ISNULL(@Col659,COL659),ISNULL(@Col660,COL660),ISNULL(@Col661,COL661),ISNULL(@Col662,COL662),ISNULL(@Col663,COL663)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

	SELECT @CVID = ID FROM @IDs

	INSERT INTO dbo.ChannelValueDoor (ID,TimeStamp,UnitID,UpdateRecord
	 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
	)
	SELECT 
	@CVID
	, @Timestamp
	, @UnitID		
	, @UpdateRecord
,ISNULL(@Col3000,COL3000),ISNULL(@Col3001,COL3001),ISNULL(@Col3002,COL3002),ISNULL(@Col3003,COL3003),ISNULL(@Col3004,COL3004),ISNULL(@Col3005,COL3005),ISNULL(@Col3006,COL3006)
,ISNULL(@Col3007,COL3007),ISNULL(@Col3008,COL3008),ISNULL(@Col3009,COL3009),ISNULL(@Col3010,COL3010),ISNULL(@Col3011,COL3011),ISNULL(@Col3012,COL3012),ISNULL(@Col3013,COL3013)
,ISNULL(@Col3014,COL3014),ISNULL(@Col3015,COL3015),ISNULL(@Col3016,COL3016),ISNULL(@Col3017,COL3017),ISNULL(@Col3018,COL3018),ISNULL(@Col3019,COL3019),ISNULL(@Col3020,COL3020)
,ISNULL(@Col3021,COL3021),ISNULL(@Col3022,COL3022),ISNULL(@Col3023,COL3023),ISNULL(@Col3024,COL3024),ISNULL(@Col3025,COL3025),ISNULL(@Col3026,COL3026),ISNULL(@Col3027,COL3027)
,ISNULL(@Col3028,COL3028),ISNULL(@Col3029,COL3029),ISNULL(@Col3030,COL3030),ISNULL(@Col3031,COL3031),ISNULL(@Col3032,COL3032),ISNULL(@Col3033,COL3033),ISNULL(@Col3034,COL3034)
,ISNULL(@Col3035,COL3035),ISNULL(@Col3036,COL3036),ISNULL(@Col3037,COL3037),ISNULL(@Col3038,COL3038),ISNULL(@Col3039,COL3039),ISNULL(@Col3040,COL3040),ISNULL(@Col3041,COL3041)
,ISNULL(@Col3042,COL3042),ISNULL(@Col3043,COL3043),ISNULL(@Col3044,COL3044),ISNULL(@Col3045,COL3045),ISNULL(@Col3046,COL3046),ISNULL(@Col3047,COL3047),ISNULL(@Col3048,COL3048)
,ISNULL(@Col3049,COL3049),ISNULL(@Col3050,COL3050),ISNULL(@Col3051,COL3051),ISNULL(@Col3052,COL3052),ISNULL(@Col3053,COL3053),ISNULL(@Col3054,COL3054),ISNULL(@Col3055,COL3055)
,ISNULL(@Col3056,COL3056),ISNULL(@Col3057,COL3057),ISNULL(@Col3058,COL3058),ISNULL(@Col3059,COL3059),ISNULL(@Col3060,COL3060),ISNULL(@Col3061,COL3061),ISNULL(@Col3062,COL3062)
,ISNULL(@Col3063,COL3063),ISNULL(@Col3064,COL3064),ISNULL(@Col3065,COL3065),ISNULL(@Col3066,COL3066),ISNULL(@Col3067,COL3067),ISNULL(@Col3068,COL3068),ISNULL(@Col3069,COL3069)
,ISNULL(@Col3070,COL3070),ISNULL(@Col3071,COL3071),ISNULL(@Col3072,COL3072),ISNULL(@Col3073,COL3073),ISNULL(@Col3074,COL3074),ISNULL(@Col3075,COL3075),ISNULL(@Col3076,COL3076)
,ISNULL(@Col3077,COL3077),ISNULL(@Col3078,COL3078),ISNULL(@Col3079,COL3079),ISNULL(@Col3080,COL3080),ISNULL(@Col3081,COL3081),ISNULL(@Col3082,COL3082),ISNULL(@Col3083,COL3083)
,ISNULL(@Col3084,COL3084),ISNULL(@Col3085,COL3085),ISNULL(@Col3086,COL3086),ISNULL(@Col3087,COL3087),ISNULL(@Col3088,COL3088),ISNULL(@Col3089,COL3089),ISNULL(@Col3090,COL3090)
,ISNULL(@Col3091,COL3091),ISNULL(@Col3092,COL3092),ISNULL(@Col3093,COL3093),ISNULL(@Col3094,COL3094),ISNULL(@Col3095,COL3095),ISNULL(@Col3096,COL3096),ISNULL(@Col3097,COL3097)
,ISNULL(@Col3098,COL3098),ISNULL(@Col3099,COL3099),ISNULL(@Col3100,COL3100),ISNULL(@Col3101,COL3101),ISNULL(@Col3102,COL3102),ISNULL(@Col3103,COL3103),ISNULL(@Col3104,COL3104)
,ISNULL(@Col3105,COL3105),ISNULL(@Col3106,COL3106),ISNULL(@Col3107,COL3107),ISNULL(@Col3108,COL3108),ISNULL(@Col3109,COL3109),ISNULL(@Col3110,COL3110),ISNULL(@Col3111,COL3111)
,ISNULL(@Col3112,COL3112),ISNULL(@Col3113,COL3113),ISNULL(@Col3114,COL3114),ISNULL(@Col3115,COL3115),ISNULL(@Col3116,COL3116),ISNULL(@Col3117,COL3117),ISNULL(@Col3118,COL3118)
,ISNULL(@Col3119,COL3119),ISNULL(@Col3120,COL3120),ISNULL(@Col3121,COL3121),ISNULL(@Col3122,COL3122),ISNULL(@Col3123,COL3123),ISNULL(@Col3124,COL3124),ISNULL(@Col3125,COL3125)
,ISNULL(@Col3126,COL3126),ISNULL(@Col3127,COL3127),ISNULL(@Col3128,COL3128),ISNULL(@Col3129,COL3129),ISNULL(@Col3130,COL3130),ISNULL(@Col3131,COL3131),ISNULL(@Col3132,COL3132)
,ISNULL(@Col3133,COL3133),ISNULL(@Col3134,COL3134),ISNULL(@Col3135,COL3135),ISNULL(@Col3136,COL3136),ISNULL(@Col3137,COL3137),ISNULL(@Col3138,COL3138),ISNULL(@Col3139,COL3139)
,ISNULL(@Col3140,COL3140),ISNULL(@Col3141,COL3141),ISNULL(@Col3142,COL3142),ISNULL(@Col3143,COL3143),ISNULL(@Col3144,COL3144),ISNULL(@Col3145,COL3145),ISNULL(@Col3146,COL3146)
,ISNULL(@Col3147,COL3147),ISNULL(@Col3148,COL3148),ISNULL(@Col3149,COL3149),ISNULL(@Col3150,COL3150),ISNULL(@Col3151,COL3151),ISNULL(@Col3152,COL3152),ISNULL(@Col3153,COL3153)
,ISNULL(@Col3154,COL3154),ISNULL(@Col3155,COL3155),ISNULL(@Col3156,COL3156),ISNULL(@Col3157,COL3157),ISNULL(@Col3158,COL3158),ISNULL(@Col3159,COL3159),ISNULL(@Col3160,COL3160)
,ISNULL(@Col3161,COL3161),ISNULL(@Col3162,COL3162),ISNULL(@Col3163,COL3163),ISNULL(@Col3164,COL3164),ISNULL(@Col3165,COL3165),ISNULL(@Col3166,COL3166),ISNULL(@Col3167,COL3167)
,ISNULL(@Col3168,COL3168),ISNULL(@Col3169,COL3169),ISNULL(@Col3170,COL3170),ISNULL(@Col3171,COL3171),ISNULL(@Col3172,COL3172),ISNULL(@Col3173,COL3173),ISNULL(@Col3174,COL3174)
,ISNULL(@Col3175,COL3175),ISNULL(@Col3176,COL3176),ISNULL(@Col3177,COL3177),ISNULL(@Col3178,COL3178),ISNULL(@Col3179,COL3179),ISNULL(@Col3180,COL3180),ISNULL(@Col3181,COL3181)
,ISNULL(@Col3182,COL3182),ISNULL(@Col3183,COL3183),ISNULL(@Col3184,COL3184),ISNULL(@Col3185,COL3185),ISNULL(@Col3186,COL3186),ISNULL(@Col3187,COL3187),ISNULL(@Col3188,COL3188)
,ISNULL(@Col3189,COL3189),ISNULL(@Col3190,COL3190),ISNULL(@Col3191,COL3191),ISNULL(@Col3192,COL3192),ISNULL(@Col3193,COL3193),ISNULL(@Col3194,COL3194),ISNULL(@Col3195,COL3195)
,ISNULL(@Col3196,COL3196),ISNULL(@Col3197,COL3197),ISNULL(@Col3198,COL3198),ISNULL(@Col3199,COL3199),ISNULL(@Col3200,COL3200),ISNULL(@Col3201,COL3201),ISNULL(@Col3202,COL3202)
,ISNULL(@Col3203,COL3203),ISNULL(@Col3204,COL3204),ISNULL(@Col3205,COL3205),ISNULL(@Col3206,COL3206),ISNULL(@Col3207,COL3207),ISNULL(@Col3208,COL3208),ISNULL(@Col3209,COL3209)
,ISNULL(@Col3210,COL3210),ISNULL(@Col3211,COL3211),ISNULL(@Col3212,COL3212),ISNULL(@Col3213,COL3213),ISNULL(@Col3214,COL3214),ISNULL(@Col3215,COL3215),ISNULL(@Col3216,COL3216)
,ISNULL(@Col3217,COL3217),ISNULL(@Col3218,COL3218),ISNULL(@Col3219,COL3219),ISNULL(@Col3220,COL3220),ISNULL(@Col3221,COL3221),ISNULL(@Col3222,COL3222),ISNULL(@Col3223,COL3223)
,ISNULL(@Col3224,COL3224),ISNULL(@Col3225,COL3225),ISNULL(@Col3226,COL3226),ISNULL(@Col3227,COL3227),ISNULL(@Col3228,COL3228),ISNULL(@Col3229,COL3229),ISNULL(@Col3230,COL3230)
,ISNULL(@Col3231,COL3231),ISNULL(@Col3232,COL3232),ISNULL(@Col3233,COL3233),ISNULL(@Col3234,COL3234),ISNULL(@Col3235,COL3235),ISNULL(@Col3236,COL3236),ISNULL(@Col3237,COL3237)
,ISNULL(@Col3238,COL3238),ISNULL(@Col3239,COL3239),ISNULL(@Col3240,COL3240),ISNULL(@Col3241,COL3241),ISNULL(@Col3242,COL3242),ISNULL(@Col3243,COL3243),ISNULL(@Col3244,COL3244)
,ISNULL(@Col3245,COL3245),ISNULL(@Col3246,COL3246),ISNULL(@Col3247,COL3247),ISNULL(@Col3248,COL3248),ISNULL(@Col3249,COL3249),ISNULL(@Col3250,COL3250),ISNULL(@Col3251,COL3251)
,ISNULL(@Col3252,COL3252),ISNULL(@Col3253,COL3253),ISNULL(@Col3254,COL3254),ISNULL(@Col3255,COL3255),ISNULL(@Col3256,COL3256),ISNULL(@Col3257,COL3257),ISNULL(@Col3258,COL3258)
,ISNULL(@Col3259,COL3259),ISNULL(@Col3260,COL3260),ISNULL(@Col3261,COL3261),ISNULL(@Col3262,COL3262),ISNULL(@Col3263,COL3263),ISNULL(@Col3264,COL3264),ISNULL(@Col3265,COL3265)
,ISNULL(@Col3266,COL3266),ISNULL(@Col3267,COL3267),ISNULL(@Col3268,COL3268),ISNULL(@Col3269,COL3269),ISNULL(@Col3270,COL3270),ISNULL(@Col3271,COL3271),ISNULL(@Col3272,COL3272)
,ISNULL(@Col3273,COL3273),ISNULL(@Col3274,COL3274),ISNULL(@Col3275,COL3275),ISNULL(@Col3276,COL3276),ISNULL(@Col3277,COL3277),ISNULL(@Col3278,COL3278),ISNULL(@Col3279,COL3279)
,ISNULL(@Col3280,COL3280),ISNULL(@Col3281,COL3281),ISNULL(@Col3282,COL3282),ISNULL(@Col3283,COL3283),ISNULL(@Col3284,COL3284),ISNULL(@Col3285,COL3285),ISNULL(@Col3286,COL3286)
,ISNULL(@Col3287,COL3287),ISNULL(@Col3288,COL3288),ISNULL(@Col3289,COL3289),ISNULL(@Col3290,COL3290),ISNULL(@Col3291,COL3291),ISNULL(@Col3292,COL3292),ISNULL(@Col3293,COL3293)
,ISNULL(@Col3294,COL3294),ISNULL(@Col3295,COL3295),ISNULL(@Col3296,COL3296),ISNULL(@Col3297,COL3297),ISNULL(@Col3298,COL3298),ISNULL(@Col3299,COL3299),ISNULL(@Col3300,COL3300)
,ISNULL(@Col3301,COL3301),ISNULL(@Col3302,COL3302),ISNULL(@Col3303,COL3303),ISNULL(@Col3304,COL3304),ISNULL(@Col3305,COL3305),ISNULL(@Col3306,COL3306),ISNULL(@Col3307,COL3307)
,ISNULL(@Col3308,COL3308),ISNULL(@Col3309,COL3309),ISNULL(@Col3310,COL3310),ISNULL(@Col3311,COL3311),ISNULL(@Col3312,COL3312),ISNULL(@Col3313,COL3313),ISNULL(@Col3314,COL3314)
,ISNULL(@Col3315,COL3315),ISNULL(@Col3316,COL3316),ISNULL(@Col3317,COL3317),ISNULL(@Col3318,COL3318),ISNULL(@Col3319,COL3319),ISNULL(@Col3320,COL3320),ISNULL(@Col3321,COL3321)
,ISNULL(@Col3322,COL3322),ISNULL(@Col3323,COL3323),ISNULL(@Col3324,COL3324),ISNULL(@Col3325,COL3325),ISNULL(@Col3326,COL3326),ISNULL(@Col3327,COL3327),ISNULL(@Col3328,COL3328)
,ISNULL(@Col3329,COL3329),ISNULL(@Col3330,COL3330),ISNULL(@Col3331,COL3331),ISNULL(@Col3332,COL3332),ISNULL(@Col3333,COL3333),ISNULL(@Col3334,COL3334),ISNULL(@Col3335,COL3335)
,ISNULL(@Col3336,COL3336),ISNULL(@Col3337,COL3337),ISNULL(@Col3338,COL3338),ISNULL(@Col3339,COL3339),ISNULL(@Col3340,COL3340),ISNULL(@Col3341,COL3341),ISNULL(@Col3342,COL3342)
,ISNULL(@Col3343,COL3343),ISNULL(@Col3344,COL3344),ISNULL(@Col3345,COL3345),ISNULL(@Col3346,COL3346),ISNULL(@Col3347,COL3347),ISNULL(@Col3348,COL3348),ISNULL(@Col3349,COL3349)
,ISNULL(@Col3350,COL3350),ISNULL(@Col3351,COL3351),ISNULL(@Col3352,COL3352),ISNULL(@Col3353,COL3353),ISNULL(@Col3354,COL3354),ISNULL(@Col3355,COL3355),ISNULL(@Col3356,COL3356)
,ISNULL(@Col3357,COL3357),ISNULL(@Col3358,COL3358),ISNULL(@Col3359,COL3359),ISNULL(@Col3360,COL3360),ISNULL(@Col3361,COL3361),ISNULL(@Col3362,COL3362),ISNULL(@Col3363,COL3363)
,ISNULL(@Col3364,COL3364),ISNULL(@Col3365,COL3365),ISNULL(@Col3366,COL3366),ISNULL(@Col3367,COL3367),ISNULL(@Col3368,COL3368),ISNULL(@Col3369,COL3369),ISNULL(@Col3370,COL3370)
,ISNULL(@Col3371,COL3371),ISNULL(@Col3372,COL3372),ISNULL(@Col3373,COL3373),ISNULL(@Col3374,COL3374),ISNULL(@Col3375,COL3375),ISNULL(@Col3376,COL3376),ISNULL(@Col3377,COL3377)
,ISNULL(@Col3378,COL3378),ISNULL(@Col3379,COL3379),ISNULL(@Col3380,COL3380),ISNULL(@Col3381,COL3381),ISNULL(@Col3382,COL3382),ISNULL(@Col3383,COL3383),ISNULL(@Col3384,COL3384)
,ISNULL(@Col3385,COL3385),ISNULL(@Col3386,COL3386),ISNULL(@Col3387,COL3387),ISNULL(@Col3388,COL3388),ISNULL(@Col3389,COL3389),ISNULL(@Col3390,COL3390),ISNULL(@Col3391,COL3391)
,ISNULL(@Col3392,COL3392),ISNULL(@Col3393,COL3393),ISNULL(@Col3394,COL3394),ISNULL(@Col3395,COL3395),ISNULL(@Col3396,COL3396),ISNULL(@Col3397,COL3397),ISNULL(@Col3398,COL3398)
,ISNULL(@Col3399,COL3399),ISNULL(@Col3400,COL3400),ISNULL(@Col3401,COL3401),ISNULL(@Col3402,COL3402),ISNULL(@Col3403,COL3403),ISNULL(@Col3404,COL3404),ISNULL(@Col3405,COL3405)
,ISNULL(@Col3406,COL3406),ISNULL(@Col3407,COL3407),ISNULL(@Col3408,COL3408),ISNULL(@Col3409,COL3409),ISNULL(@Col3410,COL3410),ISNULL(@Col3411,COL3411),ISNULL(@Col3412,COL3412)
,ISNULL(@Col3413,COL3413),ISNULL(@Col3414,COL3414),ISNULL(@Col3415,COL3415),ISNULL(@Col3416,COL3416),ISNULL(@Col3417,COL3417),ISNULL(@Col3418,COL3418),ISNULL(@Col3419,COL3419)
,ISNULL(@Col3420,COL3420),ISNULL(@Col3421,COL3421),ISNULL(@Col3422,COL3422),ISNULL(@Col3423,COL3423),ISNULL(@Col3424,COL3424),ISNULL(@Col3425,COL3425),ISNULL(@Col3426,COL3426)
,ISNULL(@Col3427,COL3427),ISNULL(@Col3428,COL3428),ISNULL(@Col3429,COL3429),ISNULL(@Col3430,COL3430),ISNULL(@Col3431,COL3431),ISNULL(@Col3432,COL3432),ISNULL(@Col3433,COL3433)
,ISNULL(@Col3434,COL3434),ISNULL(@Col3435,COL3435),ISNULL(@Col3436,COL3436),ISNULL(@Col3437,COL3437),ISNULL(@Col3438,COL3438),ISNULL(@Col3439,COL3439),ISNULL(@Col3440,COL3440)
,ISNULL(@Col3441,COL3441),ISNULL(@Col3442,COL3442),ISNULL(@Col3443,COL3443),ISNULL(@Col3444,COL3444),ISNULL(@Col3445,COL3445),ISNULL(@Col3446,COL3446),ISNULL(@Col3447,COL3447)
,ISNULL(@Col3448,COL3448),ISNULL(@Col3449,COL3449),ISNULL(@Col3450,COL3450),ISNULL(@Col3451,COL3451),ISNULL(@Col3452,COL3452),ISNULL(@Col3453,COL3453),ISNULL(@Col3454,COL3454)
,ISNULL(@Col3455,COL3455),ISNULL(@Col3456,COL3456),ISNULL(@Col3457,COL3457),ISNULL(@Col3458,COL3458),ISNULL(@Col3459,COL3459),ISNULL(@Col3460,COL3460),ISNULL(@Col3461,COL3461)
,ISNULL(@Col3462,COL3462),ISNULL(@Col3463,COL3463),ISNULL(@Col3464,COL3464),ISNULL(@Col3465,COL3465),ISNULL(@Col3466,COL3466),ISNULL(@Col3467,COL3467),ISNULL(@Col3468,COL3468)
,ISNULL(@Col3469,COL3469),ISNULL(@Col3470,COL3470),ISNULL(@Col3471,COL3471),ISNULL(@Col3472,COL3472),ISNULL(@Col3473,COL3473),ISNULL(@Col3474,COL3474),ISNULL(@Col3475,COL3475)
,ISNULL(@Col3476,COL3476),ISNULL(@Col3477,COL3477),ISNULL(@Col3478,COL3478),ISNULL(@Col3479,COL3479),ISNULL(@Col3480,COL3480),ISNULL(@Col3481,COL3481),ISNULL(@Col3482,COL3482)
,ISNULL(@Col3483,COL3483),ISNULL(@Col3484,COL3484),ISNULL(@Col3485,COL3485),ISNULL(@Col3486,COL3486),ISNULL(@Col3487,COL3487),ISNULL(@Col3488,COL3488),ISNULL(@Col3489,COL3489)
,ISNULL(@Col3490,COL3490),ISNULL(@Col3491,COL3491),ISNULL(@Col3492,COL3492),ISNULL(@Col3493,COL3493),ISNULL(@Col3494,COL3494),ISNULL(@Col3495,COL3495),ISNULL(@Col3496,COL3496)
,ISNULL(@Col3497,COL3497),ISNULL(@Col3498,COL3498),ISNULL(@Col3499,COL3499),ISNULL(@Col3500,COL3500),ISNULL(@Col3501,COL3501),ISNULL(@Col3502,COL3502),ISNULL(@Col3503,COL3503)
,ISNULL(@Col3504,COL3504),ISNULL(@Col3505,COL3505),ISNULL(@Col3506,COL3506),ISNULL(@Col3507,COL3507),ISNULL(@Col3508,COL3508),ISNULL(@Col3509,COL3509),ISNULL(@Col3510,COL3510)
,ISNULL(@Col3511,COL3511),ISNULL(@Col3512,COL3512),ISNULL(@Col3513,COL3513),ISNULL(@Col3514,COL3514),ISNULL(@Col3515,COL3515),ISNULL(@Col3516,COL3516),ISNULL(@Col3517,COL3517)
,ISNULL(@Col3518,COL3518),ISNULL(@Col3519,COL3519)
	FROM dbo.ChannelValueDoor WITH (NOLOCK) 
	WHERE UnitID = @UnitID
	ORDER BY 1 DESC
END

GO


RAISERROR ('-- update NSP_InsertEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertEventChannelValue AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_InsertEventChannelValue] (
    @Timestamp DATETIME2(3)
    , @TimestampEndTime DATETIME2(3) = NULL
    , @UnitId INT
    , @ChannelId INT
	, @Value bit
    )
AS
/******************************************************************************
**  Name:           NSP_InsertEventChannelValue
**  Description:    Inserts a new event-based channel value for the SLT fleet
**  Call frequency: Called by the telemetry service when the event happens. 
**                  The event is considered started if a start time (timeStamp) only is given.
**                  The event is considered finished when a end date is given.
**                  
**  Parameters:     @timestamp the timestamp when the event happens
**                  @timestamp the timestamp when the event happens
**                  @UnitId the Unit ID
**                  @ChannelId the channel ID
******************************************************************************/
BEGIN
	DECLARE @SqlStr varchar(max)

	-- Insert a record into EventChannelValue
    INSERT INTO dbo.EventChannelValue (TimeStamp, TimestampEndTime, UnitID, ChannelId, Value)
    VALUES (@timestamp, @TimestampEndTime, @UnitId, @ChannelId, @Value)

	IF not @TimestampEndTime is null
		SET @timestamp = @TimestampEndTime-- for the latest value I'm expect that TimestampEndTime will always have the latest datetime

	-- Update EventChannelLatestValue
    IF EXISTS (SELECT top 1 1 FROM dbo.EventChannelLatestValue WHERE UnitID = @UnitId)
		SET @SqlStr = '
			UPDATE dbo.EventChannelLatestValue 
			SET TimeLastUpdated = ''' + CAST(@timestamp AS VARCHAR(25)) + ''',
				Value = ' + CAST(@value AS VARCHAR(5)) + ' 
			WHERE UnitID = ' + CAST(@UnitId AS VARCHAR(5)) + ' and ChannelID = ' + CAST(@ChannelId AS VARCHAR(5)) 
		
    ELSE
	    SET @SqlStr = '
	        INSERT INTO dbo.EventChannelLatestValue(TimeLastUpdated, UnitID, ChannelID,Value)  
		    VALUES (''' + CAST(@timestamp AS VARCHAR(25)) + ''', ' + CAST(@UnitId AS VARCHAR(5)) + ', ' + CAST(@ChannelId AS VARCHAR(5)) +', ' + CAST(@value AS VARCHAR(5)) + ')
	    '

    EXEC(@SqlStr)
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,054
           ,'update_spectrum_db_054.sql'
           ,'1.2.01')
GO