SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 070
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('--Create table FaultGroup', 0, 1) WITH NOWAIT
GO

IF Exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='FaultGroup')
	DROP TABLE dbo.FaultGroup

CREATE TABLE dbo.FaultGroup(
	ID INT IDENTITY(1,1) NOT NULL,
	Timestamp DATETIME2(3) NOT NULL,
	UserName VARCHAR(255) NOT NULL
	CONSTRAINT [PK_FaultGroup] PRIMARY KEY CLUSTERED (
		[ID] ASC
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------------------------------------
RAISERROR ('--Add column FaultGroupID to table Fault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Fault' AND COLUMN_NAME='FaultGroupID')
BEGIN
	ALTER TABLE [dbo].[Fault] 
		ADD FaultGroupID INT NULL

	ALTER TABLE [dbo].[Fault] WITH CHECK 
		ADD CONSTRAINT [FK_Fault_FaultGroup] 
		FOREIGN KEY([FaultGroupID])
		REFERENCES [dbo].[FaultGroup] ([ID])

	ALTER TABLE [dbo].[Fault] 
		CHECK CONSTRAINT [FK_Fault_FaultGroup]

END 
GO

------------------------------------------------------------
RAISERROR ('--Add column IsGroupLead to table Fault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Fault' AND COLUMN_NAME='IsGroupLead')
BEGIN
	ALTER TABLE [dbo].[Fault] 
		ADD IsGroupLead BIT NULL
END 
GO

------------------------------------------------------------
RAISERROR ('-- create NSP_UpsertFaultGroup', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpsertFaultGroup' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_UpsertFaultGroup AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpsertFaultGroup] (
	@FaultIDs varchar(1000),
	@LeadFaultID int,
	@UserName varchar(255),
	@FaultGroupID int = NULL
)
AS
BEGIN

	IF @FaultGroupID IS NULL
		BEGIN
			INSERT INTO dbo.FaultGroup (Timestamp, UserName) VALUES (SYSDATETIME(), @UserName)

			SELECT @FaultGroupID = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE dbo.FaultGroup SET Timestamp = SYSDATETIME(), UserName = @UserName WHERE ID = @FaultGroupID

			-- Remove all Faults and then we will add in only the ones passed
			UPDATE dbo.Fault SET FaultGroupID = NULL, IsGroupLead = NULL WHERE FaultGroupID = @FaultGroupID
		END

	DECLARE @FaultIDTable TABLE (FaultID int)
	INSERT INTO @FaultIDTable
	SELECT * from dbo.SplitStrings_CTE(@FaultIDs,',')

	IF (SELECT COUNT(*) FROM @FaultIDTable) > 0
	BEGIN
		UPDATE dbo.Fault SET FaultGroupID = @FaultGroupID, IsGroupLead = 0 WHERE ID IN (SELECT FaultID FROM @FaultIDTable)
		UPDATE dbo.Fault SET IsGroupLead = 1 WHERE ID = @LeadFaultID
	END
END
GO

------------------------------------------------------------
RAISERROR ('-- alter VW_IX_Fault view', 0, 1) WITH NOWAIT
GO

ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		, f.FaultGroupID
		, f.IsGroupLead
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 	


GO

/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 11/01/2017 13:33:46 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 11/01/2017 13:33:54 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 11/01/2017 13:34:03 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------------------------
RAISERROR ('-- create NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, Description
		, Summary
		, Category
		, CategoryID
		, CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = xref.ExternalCode
		, IsGroupLead
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
		LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
	WHERE 
		f.FaultGroupID = @FaultGroupID

END
GO

------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , fm.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , RowVersion
			, f.FaultGroupID
			, f.IsGroupLead
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ID = xref.ID
        WHERE f.ID = @FaultID

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,070
           ,'update_spectrum_db_070.sql'
           ,'1.2.03')
GO