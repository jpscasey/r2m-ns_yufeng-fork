SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 072
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- update NSP_HistoricEventChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelData AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelData]
    @DateTimeEnd DATETIME
    ,@Interval INT -- number of seconds
    ,@UnitID INT
    ,@SelectColList VARCHAR(4000)
AS

BEGIN
    SET NOCOUNT ON;

    DECLARE @sql varchar(max)

    IF @DateTimeEnd IS NULL
        SET @DateTimeEnd = SYSDATETIME()

    DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)

    DECLARE @vehicleTypeID int = (SELECT VehicleTypeID FROM VW_Vehicle WHERE ID = @UnitID)

    SET @sql = '
SELECT
    TimeStamp = CAST(TimeStamp AS DATETIME)
    ,ChannelID
    ,Value
	,UnitID
FROM EventChannelValue
WHERE
    UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
    AND ChannelID IN (' + @SelectColList + ')' + '
    AND Timestamp BETWEEN ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(VARCHAR(23), @DateTimeEnd, 121)  + ''''

    EXEC (@sql)
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,072
           ,'update_spectrum_db_072.sql'
           ,'1.2.03')
GO