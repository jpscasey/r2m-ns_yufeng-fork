SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 081
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_FleetSummaryDrilldown', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummaryDrilldown' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummaryDrilldown AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_FleetSummaryDrilldown
**	Description:	Returns live channel data for the application
**	Call frequency:	When user click on grouped cell on Fleet Summary
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
	@UnitIdList varchar(30)
	, @SelectColList varchar(4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @sql varchar(max)
	DECLARE @cols AS NVARCHAR(MAX)
	DECLARE @colNames AS NVARCHAR(MAX)

	select @cols = STUFF((SELECT ',' + QUOTENAME(ChannelID) 
                    from EventChannelLatestValue
					where CHARINDEX(cast(ChannelID as varchar(10)), @SelectColList) >0
                    group by ChannelID
                    order by ChannelID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

	select @colNames = STUFF((SELECT ','+ QUOTENAME(ChannelID)+' as col' + cast(ChannelID as varchar(10))
                    from EventChannelLatestValue
					where CHARINDEX(cast(ChannelID as varchar(10)), @SelectColList) >0
                    group by ChannelID
                    order by ChannelID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
	
	SET @sql = '
SELECT DISTINCT
	cv.UnitID
	, ' + @SelectColList + '
	, fs.UnitPosition
FROM dbo.VW_FleetStatus fs
INNER JOIN dbo.VW_UnitLastChannelValueTimestamp vlc ON vlc.UnitID = fs.UnitID
LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
LEFT JOIN ( SELECT ' + @colNames + ',UnitID from 
             (
                select cast(value as tinyint) as value, ChannelID,UnitID
                from EventChannelLatestValue
				where UnitID in (' + @UnitIdList + ')
            ) x
            pivot 
            (
                MAX(value)
                for ChannelID in (' + @cols + ')
            ) p  
			) eclv ON vlc.UnitID = eclv.UnitID
WHERE fs.UnitID in (' + @UnitIdList + ')
ORDER BY 
	fs.UnitPosition'

	EXEC (@sql)

END


GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,081
           ,'update_spectrum_db_081.sql'
           ,'1.3.01')
GO