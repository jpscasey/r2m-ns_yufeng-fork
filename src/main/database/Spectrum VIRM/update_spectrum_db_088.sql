SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 088
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- update NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultCode varchar(100)
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
    , @UnitType varchar(20) = NULL
    , @HasRecovery bit = NULL
    , @MaximoID varchar(20) = NULL
    , @HasMaximoSR bit = NULL
    
)
AS
BEGIN

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultCode = ''  SET @FaultCode = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    IF @UnitType = ''   SET @UnitType = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable)  OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode    LIKE @FaultCode    OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 

END
GO


GO

RAISERROR ('-- update NSP_EaReport', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_EaReport' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_EaReport AS BEGIN RETURN(1) END;')
GO
/*
Try me:
exec [dbo].[NSP_EaReport]     @FleetCode = null
    , @DateFrom  = '20170127'
    , @DateTo  = '20170201'
    -- additional parameters: need to be in alphabetical order
    , @CategoryID  = null
    , @FaultMetaID  = '3552'
    , @FaultTypeID  = null
    , @LocationID  = null
    , @UnitID  = ''
    , @UnitType  = ''
    , @VehicleID  = null

*/
ALTER PROCEDURE [dbo].[NSP_EaReport]
(
    @FleetCode varchar(10)
    , @DateFrom datetime
    , @DateTo datetime
    -- additional parameters: need to be in alphabetical order
    , @CategoryID varchar(100)
    , @FaultCode varchar(max)
    , @FaultTypeID varchar(50)
    , @LocationID varchar(max)
    , @UnitID varchar(max)
    , @UnitType varchar(20)
    , @VehicleID varchar(max)
)
AS
BEGIN

    DECLARE @FaultCategoryTable TABLE (CategoryID int)
    INSERT INTO @FaultCategoryTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeID int)
    INSERT INTO @FaultTypeTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@FaultTypeID,',')

    DECLARE @LocationTable TABLE (LocationID int)
    INSERT INTO @LocationTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@LocationID,',')

    DECLARE @UnitIDTable TABLE (UnitID int)
    INSERT INTO @UnitIDTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@UnitID,',')
    
    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitType,',')

    IF @CategoryID = '' SET @CategoryID = NULL
    IF @FaultCode   = '' SET @FaultCode = NULL
    IF @FaultTypeID = '' SET @FaultTypeID = NULL
    IF @LocationID = '' SET @LocationID = NULL
    IF @UnitID = '' SET @UnitID = NULL
    IF @UnitType = '' SET @UnitType    = NULL

    DECLARE @FaultMetaList TABLE (ID int)

    INSERT INTO @FaultMetaList
    SELECT ID
    FROM dbo.FaultMeta
    WHERE (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultTypeID = '-1' AND FaultTypeID IS NULL))
        AND (FaultCode LIKE @FaultCode OR @FaultCode IS NULL OR (@FaultCode = '-1' AND ID IS NULL))
        AND (CategoryID IN (SELECT * FROM @FaultCategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))

    SELECT
        FaultID = f.ID
        , f.CreateTime
        , f.EndTime
        , f.IsCurrent

        , f.HeadCode
        , f.SetCode
        , f.FleetCode
        , f.FaultUnitNumber AS UnitNumber
        , UnitID    = f.FaultUnitID
        , f.FaultUnitID
        , FaultVehicleNumber = f.FaultUnitID -- unit level, for now
        , FaultVehicleID = NULL

        , f.FaultMetaID
        , f.FaultCode
        , f.Description

        , f.FaultTypeID
        , f.FaultType
        , f.FaultTypeColor
        , f.Priority

        , f.CategoryID
        , f.Category

        , f.Latitude
        , f.Longitude
        , f.LocationID
        , l.LocationCode
    FROM dbo.VW_IX_Fault f (NOEXPAND)
    LEFT JOIN dbo.Location l ON l.ID = LocationID
    LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
    WHERE f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
        AND f.CreateTime BETWEEN @DateFrom AND @DateTo
        AND (@UnitID is null OR (f.FaultUnitID in (SELECT UnitID FROM @UnitIDTable)))
		AND (@LocationID IS NULL OR LocationID = @LocationID  OR (@LocationID = -1 AND LocationID IS NULL))
        AND (@FleetCode IS NULL OR f.FleetCode = @FleetCode)
        AND (@UnitType IS NULL OR u.UnitType = @UnitType)

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,088
           ,'update_spectrum_db_088.sql'
           ,'1.3.02')
GO