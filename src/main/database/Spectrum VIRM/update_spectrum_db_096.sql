SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 096
SET @scriptType = 'database update'

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Add FaultMetaExtraFields table', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = 'FaultMetaExtraFields')
BEGIN
    CREATE TABLE [dbo].[FaultMetaExtraField](
	    [FaultMetaId] int NOT NULL,
	    [Field] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
	    [Value] [nvarchar](MAX),
	    PRIMARY KEY (FaultMetaId, Field),
	    CONSTRAINT FK_FaultMetaExtraField FOREIGN KEY (FaultMetaId)
	    REFERENCES FaultMeta(ID)
	);
END


GO


RAISERROR ('-- Update NSP_DeleteFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_DeleteFaultMeta AS BEGIN RETURN(1) END;')
GO

ALTER PROC [dbo].[NSP_DeleteFaultMeta](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY
		
		DELETE FROM dbo.FaultMetaExtraField 
		WHERE FaultMetaId = @ID
	
		DELETE dbo.FaultMeta 
		WHERE ID = @ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		IF ERROR_NUMBER() = 547 -- FOREIGN KEY ERROR
    		BEGIN
    			RAISERROR ('ERR_FAULT_EXIST', 14, 1) WITH NOWAIT
    		END
    	ELSE
    		BEGIN
    			EXEC NSP_RethrowError;
    		END
    		
		ROLLBACK TRAN;
		
	END CATCH
	
END



GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,096
           ,'update_spectrum_db_096.sql'
           ,'1.4.02')
GO