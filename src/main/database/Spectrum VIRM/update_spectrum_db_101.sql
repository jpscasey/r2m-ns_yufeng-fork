SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 101
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update NSP_HistoricEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelValue AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_HistoricEventChannelValue
**  Description:    Returns historic event channel data
**  Call frequency: Every 5s for each vehicle shown on the unit channel data screen
**  Parameters:     @timestamp the timestamp or null to get the latest data
**                  @unitId the unit ID
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelValue] 
    @timestamp DATETIME2(3)
    ,@unitId INT
AS

BEGIN

SET NOCOUNT ON;
DECLARE  @timestampInternal DATETIME2(3)

	IF @timestamp IS NULL
		SET @timestampInternal = DATEADD(hh, 1, GETDATE())
	ELSE 
		SET @timestampInternal = @timestamp

	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.ChannelID
		FROM EventChannelValue ecv 
		WHERE ecv.UnitID = @unitId
			AND ecv.TimeStamp BETWEEN DATEADD(DAY, -1, @timestampInternal) AND @timestampInternal
		GROUP BY ecv.ChannelID
		)
	SELECT ecv.TimeStamp
	    ,ecv.UnitID
		,ecv.ChannelID
		,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN EventChannelValue ecv 
		ON ecvl.TimeStamp = ecv.TimeStamp
		AND ecvl.ChannelID = ecv.ChannelID
        AND ecv.UnitID = @unitId
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,101
           ,'update_spectrum_db_101.sql'
           ,'1.5.01')
GO