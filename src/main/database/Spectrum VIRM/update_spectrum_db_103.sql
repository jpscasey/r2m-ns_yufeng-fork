SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 103
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Add ServiceRequest table', 0, 1) WITH NOWAIT
GO

IF Exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='ServiceRequest')
	DROP TABLE dbo.ServiceRequest

CREATE TABLE [dbo].[ServiceRequest] (
	[ID] int IDENTITY(1,1) NOT NULL,
	[FaultID] int NOT NULL,
	[UnitID] int NOT NULL,
	[ExternalCode] varchar(10) UNIQUE,
	[Status] varchar(50),
	[CreateTime] datetime2,
	[LastUpdateTime] datetime2,
	[CreatedByRule] bit
	PRIMARY KEY (ID)
	CONSTRAINT FK_ServiceRequest_Fault FOREIGN KEY (FaultID)
	REFERENCES Fault(ID)
)

GO

ALTER TABLE [dbo].[ServiceRequest]
	ADD CONSTRAINT [FK_ServiceRequest_Unit]
	FOREIGN KEY([UnitID])
	REFERENCES [dbo].[Unit]([ID])

GO

----------------------------------------------------------------------------

RAISERROR ('--add ServiceRequestID column to Fault table', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Fault' AND COLUMN_NAME='ServiceRequestID')
BEGIN
	ALTER TABLE [dbo].[Fault]
	  ADD ServiceRequestID int
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,103
           ,'update_spectrum_db_103.sql'
           ,'1.5.02')
GO