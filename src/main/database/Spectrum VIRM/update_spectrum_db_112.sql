SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 112
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- add more columns to dbo.ServiceRequest, drop FK constraint', 0, 1) WITH NOWAIT
GO

ALTER TABLE [dbo].[ServiceRequest]
DROP CONSTRAINT [FK_ServiceRequest_Fault]

ALTER TABLE [dbo].[ServiceRequest]
ALTER COLUMN [FaultID] int NULL

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='Username')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [Username] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='Description')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [Description] varchar(100)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='ExternalRecord')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [ExternalRecord] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='VkbPriority')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [VkbPriority] int
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='Classification')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [Classification] varchar(100)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='MbnUrgency')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [MbnUrgency] int
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='NotificationParty')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [NotificationParty] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='PositionCode')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [PositionCode] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='QProfileCode')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [QProfileCode] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='NotificationCompany')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [NotificationCompany] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='TrainNumber')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [TrainNumber] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='SafetyDisruptionLevel')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [SafetyDisruptionLevel] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='WorkorderLocationType')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [WorkorderLocationType] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='NotificationSource')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [NotificationSource] varchar(50)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='StatusTimestamp')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [StatusTimestamp] datetime2(7)
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ServiceRequest' AND COLUMN_NAME='DiagnosticalCode')
BEGIN
	ALTER TABLE [dbo].[ServiceRequest]
	  ADD [DiagnosticalCode] varchar(50)
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,112
           ,'update_spectrum_db_112.sql'
           ,'1.6.02')
GO