SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 113
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Add WorkOrder table', 0, 1) WITH NOWAIT
GO

IF Exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='WorkOrder')
	DROP TABLE dbo.WorkOrder

CREATE TABLE [dbo].[WorkOrder] (
	[ID] int IDENTITY(1,1) NOT NULL,
	[UnitID] int NOT NULL,
	[WorkOrderID] varchar(10) UNIQUE,
	[Status] varchar(50),
	[CreateTime] datetime2,
	[ServiceRequestID] int,
	[Description] varchar(100),
	[MaintenanceType] varchar(20),
	[Symptom] varchar(50),
	[MbnUrgency] int,
	[VkbPriority] int,
	[FaultCode] varchar(10),
	[WorkLocation] varchar(50),
	[NotificationSource] varchar(50)
	PRIMARY KEY (ID)
)

GO

ALTER TABLE [dbo].[WorkOrder]
	ADD CONSTRAINT [FK_WorkOrder_Unit]
	FOREIGN KEY([UnitID])
	REFERENCES [dbo].[Unit]([ID])
	
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,113
           ,'update_spectrum_db_113.sql'
           ,'1.6.02')
GO