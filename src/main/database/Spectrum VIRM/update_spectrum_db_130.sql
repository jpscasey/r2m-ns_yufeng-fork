SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 130
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- Update NSP_HistoricFleetSummary', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricFleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_HistoricFleetSummary AS BEGIN RETURN(1) END;')
GO

--TRY ME [dbo].[NSP_HistoricFleetSummary] @DateTimeEnd='20170704 08:00'
ALTER PROCEDURE [dbo].[NSP_HistoricFleetSummary](
	@DateTimeEnd datetime2(3)
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

	DECLARE @interval int = 3600 -- value in seconds 
	DECLARE @DateTimeStart datetime2(3) = DATEADD(s, - @interval, @DateTimeEnd);
	
	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
	CREATE TABLE #LatestChannelValue (
		UnitID int PRIMARY KEY
		, LastTimeStamp datetime2(3)
	)
	
	INSERT #LatestChannelValue( UnitID , LastTimeStamp)
	SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
	FROM dbo.ChannelValue c
	WHERE 
		c.Timestamp >= @DateTimeStart
		AND c.Timestamp <= @DateTimeEnd
	GROUP BY c.UnitID

	SELECT 
		v.UnitID
		, v.UnitNumber
		, v.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, Diagram				= fs.Diagram
		, Headcode			 = fs.Headcode
		, VehicleID			 = v.ID
		, VehicleNumber		 = v.VehicleNumber
		, VehiclePosition		= v.VehicleOrder
		, VehicleType			= v.Type
		, Location			 = l.LocationCode 
		, ServiceStatus = CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,TimeStamp			 = CAST(cv.[TimeStamp] AS datetime)
		,cv.[Col1]
		,cv.[Col2]
		,cv.[Col3]
		,cv.[Col4]
		,cv.[Col302]
		--,cv.[Col6]
		--,cv.[Col30]
		,v.FleetCode
	FROM dbo.VW_Vehicle v 
	INNER JOIN #LatestChannelValue lcv ON v.UnitID = lcv.UnitID
	INNER JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = lcv.LastTimeStamp AND cv.UnitID = lcv.UnitID
	LEFT JOIN dbo.FleetStatusHistory fs ON v.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN fs.ValidFrom AND ISNULL(fs.ValidTo, CONVERT(datetime2(3),DATEADD(d, 1, GETDATE()))) -- adding 1 day in case live data is inserted with 
	LEFT JOIN dbo.Location l ON l.ID = (
		SELECT TOP 1 la.LocationID 
		FROM dbo.LocationArea  la
		WHERE cv.col1 BETWEEN la.MinLatitude AND la.MaxLatitude
			AND cv.col2 BETWEEN la.MinLongitude AND la.MaxLongitude
		ORDER BY la.Priority
	)
	
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,130
           ,'update_spectrum_db_130.sql'
           ,'1.7.02')
GO

