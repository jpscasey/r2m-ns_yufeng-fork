SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 157
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script 
----------------------------------------------------------------------------

RAISERROR ('add new columns to ChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME='Col146')
BEGIN
	ALTER TABLE [dbo].[ChannelValue] 
		ADD Col146 INT NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME='Col147')
BEGIN
	ALTER TABLE [dbo].[ChannelValue] 
		ADD Col147 INT NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME='Col148')
BEGIN
	ALTER TABLE [dbo].[ChannelValue] 
		ADD Col148 INT NULL
END 
GO

----------------------------------------------------------------------------

RAISERROR ('-- add new colums to FaultChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME='Col146')
BEGIN
	ALTER TABLE [dbo].[FaultChannelValue] 
		ADD Col146 INT NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME='Col147')
BEGIN
	ALTER TABLE [dbo].[FaultChannelValue] 
		ADD Col147 INT NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME='Col148')
BEGIN
	ALTER TABLE [dbo].[FaultChannelValue] 
		ADD Col148 INT NULL
END 


GO

----------------------------------------------------------------------------

RAISERROR ('add new mBvk1 channels to NSP_InsertChannelValue_Text', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue_Text' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue_Text AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue_Text](
		@Timestamp datetime2(3),
		@UnitNumber varchar(16),
	    @UpdateRecord bit ,
@MCG_ILatitude decimal (9,6) = NULL ,@MCG_ILongitude decimal (9,6) = NULL ,@36A1_UIT_VERWARMINGONDERIN Bit = NULL ,@36A1_UIT_VERWARMINGBOVENIN Bit = NULL ,@36A1Verweindrin Bit = NULL
 ,@317A1_Vrijgave_Openen Bit = NULL ,@327A1_Vrijgave_Openen Bit = NULL ,@337A1_Vrijgave_Openen Bit = NULL ,@347A1_Vrijgave_Openen Bit = NULL ,@34A1_DS_HL_MG Bit = NULL
 ,@38A3_Compressor_1_IN Bit = NULL ,@38A3_Druk__8_BAR Bit = NULL ,@56A1_UIT_VERWARMINGONDERIN Bit = NULL ,@56A1_UIT_VERWARMINGBOVENIN Bit = NULL ,@56A1Verweindrin Bit = NULL
 ,@517A1_Vrijgave_Openen Bit = NULL ,@527A1_Vrijgave_Openen Bit = NULL ,@537A1_Vrijgave_Openen Bit = NULL ,@547A1_Vrijgave_Openen Bit = NULL ,@54A1_DS_HL_MG Bit = NULL
 ,@64B5_HR_DRUK decimal(5,1) = NULL ,@6Treinleidingdruk decimal(5,1) = NULL ,@66A1_UIT_VERWARMINGONDERIN Bit = NULL ,@66A1_UIT_VERWARMINGBOVENIN Bit = NULL ,@66A1Verweindrin Bit = NULL
 ,@617A1_Vrijgave_Openen Bit = NULL ,@627A1_Vrijgave_Openen Bit = NULL ,@637A1_Vrijgave_Openen Bit = NULL ,@647A1_Vrijgave_Openen Bit = NULL ,@64A1_DS_HL_MG Bit = NULL
 ,@68A3_Compressor_Gevraagd Bit = NULL ,@6TLdrukkleinerals05Bar Bit = NULL ,@68A2_Druk__5_BAR Bit = NULL ,@68A2_Druk__85_BAR Bit = NULL ,@68A2_Druk__95_BAR Bit = NULL
 ,@73A8_MW_UBAT_VOLT Integer = NULL ,@73A2_M_UT Bit = NULL ,@73A2_M_IT_GEM decimal(5,1) = NULL ,@76A1_UIT_VERWARMINGONDERIN Bit = NULL ,@76A1_UIT_VERWARMINGBOVENIN Bit = NULL
 ,@76A1Verweindrin Bit = NULL ,@717A1_Vrijgave_Openen Bit = NULL ,@727A1_Vrijgave_Openen Bit = NULL ,@737A1_Vrijgave_Openen Bit = NULL ,@747A1_Vrijgave_Openen Bit = NULL
 ,@74A1_DS_HL_MG Bit = NULL ,@74A1_SLIP_ABI Bit = NULL ,@72A1_BELADING Bit = NULL ,@72A1_IN_Gevraagd_Koppel Integer = NULL ,@72A1_IN_Vooruit Bit = NULL ,@72A1_IN_Achteruit Bit = NULL
 ,@7Actueelkoppel Integer = NULL ,@72A1_ILACT Bit = NULL ,@72A1_METING_LIJNSPANNING Bit = NULL ,@72A1_Inverter_Frequentie Integer = NULL ,@72A1_Detectie_Wielslip Bit = NULL
 ,@78A4_IN_HS_AANWEZIG Bit = NULL ,@72A1_Tractie_Levert_Koppel Bit = NULL ,@13A8_MW_UBAT_VOLT Integer = NULL ,@13A2_M_UT Bit = NULL ,@13A2_M_IT_GEM decimal(5,1) = NULL
 ,@18A3_Gereed_Continu Bit = NULL ,@18A3_Bediende_Cabine Bit = NULL ,@18A3_Bedrijfstoestand_Gereed Bit = NULL ,@18A3_IN_Dienstvaardig Bit = NULL ,@18A4_ATB_Snelrem Bit = NULL
 ,@18A4_IN_HS_AANWEZIG Bit = NULL ,@16A1_UIT_VERWARMINGONDERIN Bit = NULL ,@16A1_UIT_VERWARMINGBOVENIN Bit = NULL ,@16A1Verwcabinein Bit = NULL ,@117A1_Vrijgave_Openen Bit = NULL
 ,@127A1_Vrijgave_Openen Bit = NULL ,@137A1_Vrijgave_Openen Bit = NULL ,@147A1_Vrijgave_Openen Bit = NULL ,@14A1_DS_HL_MG Bit = NULL ,@14A1_SLIP_ABI Bit = NULL ,@1standremkraan5 Bit = NULL
 ,@1standremkraan4 Bit = NULL ,@1standremkraan3 Bit = NULL ,@1standremkraan2 Bit = NULL ,@1standremkraan1 Bit = NULL ,@1standremkraanT Bit = NULL ,@1standremkraanA Bit = NULL
 ,@14A1_Rem_Noodbedrijf Bit = NULL ,@14A1_ST_SB Bit = NULL ,@1standremkraan7 Bit = NULL ,@1standremkraan6 Bit = NULL ,@14A1Noodremreizigers Bit = NULL ,@14A1_Ingang_Treindraad_17 Bit = NULL
 ,@14A1_Ingang_Treindraad_27 Bit = NULL ,@14A1_Ingang_Treindraad_47 Bit = NULL ,@12A1_BELADING Bit = NULL ,@12A1_IN_Gevraagd_Koppel Integer = NULL ,@12A1_IN_Vooruit Bit = NULL
 ,@12A1_IN_Achteruit Bit = NULL ,@1Actueelkoppel Integer = NULL ,@12A1_ILACT Bit = NULL ,@12A1_METING_LIJNSPANNING Bit = NULL ,@12A1_Inverter_Frequentie Integer = NULL
 ,@12A1_Detectie_Wielslip Bit = NULL ,@18A2_ATB_Treinsnelheid Integer = NULL ,@18A2_ATB_Bewaakte_Snelheid Integer = NULL ,@12A1_Tractie_Levert_Koppel Bit = NULL ,@23A8_MW_UBAT_VOLT Integer = NULL
 ,@23A2_M_UT Bit = NULL ,@23A2_M_IT_GEM decimal(5,1) = NULL ,@28A3_Gereed_Continu Bit = NULL ,@28A3_Bediende_Cabine Bit = NULL ,@28A3_Bedrijfstoestand_Gereed Bit = NULL
 ,@28A3_IN_Dienstvaardig Bit = NULL ,@28A4_ATB_Snelrem Bit = NULL ,@28A4_IN_HS_AANWEZIG Bit = NULL ,@26A1_UIT_VERWARMINGONDERIN Bit = NULL ,@26A1_UIT_VERWARMINGBOVENIN Bit = NULL
 ,@26A1Verwcabinein Bit = NULL ,@217A1_Vrijgave_Openen Bit = NULL ,@227A1_Vrijgave_Openen Bit = NULL ,@237A1_Vrijgave_Openen Bit = NULL ,@247A1_Vrijgave_Openen Bit = NULL ,@24A1_DS_HL_MG Bit = NULL
 ,@24A1_SLIP_ABI Bit = NULL ,@2standremkraan5 Bit = NULL ,@2standremkraan4 Bit = NULL ,@2standremkraan3 Bit = NULL ,@2standremkraan2 Bit = NULL ,@2standremkraan1 Bit = NULL
 ,@2standremkraanT Bit = NULL ,@2standremkraanA Bit = NULL ,@24A1_Rem_Noodbedrijf Bit = NULL ,@24A1_ST_SB Bit = NULL ,@2standremkraan7 Bit = NULL ,@2standremkraan6 Bit = NULL
 ,@24A1Noodremreizigers Bit = NULL ,@22A1_BELADING Bit = NULL ,@22A1_IN_Gevraagd_Koppel Integer = NULL ,@22A1_IN_Vooruit Bit = NULL ,@22A1_IN_Achteruit Bit = NULL ,@2Actueelkoppel Integer = NULL
 ,@22A1_ILACT Bit = NULL ,@22A1_METING_LIJNSPANNING Bit = NULL ,@22A1_Inverter_Frequentie Integer = NULL ,@22A1_Detectie_Wielslip Bit = NULL ,@28A2_ATB_Treinsnelheid Integer = NULL
 ,@28A2_ATB_Bewaakte_Snelheid Integer = NULL ,@22A1_Tractie_Levert_Koppel Bit = NULL ,@DIENSTVAARD BigInt = NULL ,@DIV_KOPPL_1 BigInt = NULL ,@DIV_KOPPL_2 BigInt = NULL ,@DR01_KLEM BigInt = NULL
 ,@DR02_KLEM BigInt = NULL ,@DR03_KLEM BigInt = NULL ,@DR04_KLEM BigInt = NULL ,@DR05_KLEM BigInt = NULL ,@DR06_KLEM BigInt = NULL ,@DR07_KLEM BigInt = NULL ,@DR08_KLEM BigInt = NULL
 ,@DR09_KLEM BigInt = NULL ,@DR10_KLEM BigInt = NULL ,@DR11_KLEM BigInt = NULL ,@DR12_KLEM BigInt = NULL ,@DR13_KLEM BigInt = NULL ,@DR14_KLEM BigInt = NULL ,@DR15_KLEM BigInt = NULL
 ,@DR16_KLEM BigInt = NULL ,@DR17_KLEM BigInt = NULL ,@DR18_KLEM BigInt = NULL ,@DR19_KLEM BigInt = NULL ,@DR20_KLEM BigInt = NULL ,@DR21_KLEM BigInt = NULL ,@DR22_KLEM BigInt = NULL 
 ,@DR23_KLEM BigInt = NULL ,@DR24_KLEM BigInt = NULL ,@DRN_OCS_11 BigInt = NULL ,@DRN_OCS_12 BigInt = NULL ,@DRN_OCS_13 BigInt = NULL ,@DRN_OCS_14 BigInt = NULL ,@DRN_OCS_21 BigInt = NULL
 ,@DRN_OCS_22 BigInt = NULL ,@DRN_OCS_23 BigInt = NULL ,@DRN_OCS_24 BigInt = NULL ,@DRN_OCS_31 BigInt = NULL ,@DRN_OCS_32 BigInt = NULL ,@DRN_OCS_33 BigInt = NULL ,@DRN_OCS_34 BigInt = NULL
 ,@DRN_OCS_51 BigInt = NULL ,@DRN_OCS_52 BigInt = NULL ,@DRN_OCS_53 BigInt = NULL ,@DRN_OCS_54 BigInt = NULL ,@DRN_OCS_61 BigInt = NULL ,@DRN_OCS_62 BigInt = NULL ,@DRN_OCS_63 BigInt = NULL
 ,@DRN_OCS_64 BigInt = NULL ,@DRN_OCS_71 BigInt = NULL ,@DRN_OCS_72 BigInt = NULL ,@DRN_OCS_73 BigInt = NULL ,@DRN_OCS_74 BigInt = NULL ,@DR_KLEM_Z1 BigInt = NULL ,@DR_KLEM_Z2 BigInt = NULL
 ,@GEREED BigInt = NULL ,@GEREEDCONT BigInt = NULL ,@HSP_SA_KM1 BigInt = NULL ,@HSP_SA_KM2 BigInt = NULL ,@HS_AAN_SSUIT BigInt = NULL ,@HS_AFW_SSUIT BigInt = NULL ,@KLM_BUIBOV_1 BigInt = NULL
 ,@KLM_BUIBOV_2 BigInt = NULL ,@KLM_BUIBOV_3 BigInt = NULL ,@KLM_BUIBOV_5 BigInt = NULL ,@KLM_BUIBOV_6 BigInt = NULL ,@KLM_BUIBOV_7 BigInt = NULL ,@KLM_BUIOND_1 BigInt = NULL
 ,@KLM_BUIOND_2 BigInt = NULL ,@KLM_BUIOND_3 BigInt = NULL ,@KLM_BUIOND_5 BigInt = NULL ,@KLM_BUIOND_6 BigInt = NULL ,@KLM_BUIOND_7 BigInt = NULL ,@KLM_CMP11_T BigInt = NULL 
 ,@KLM_CMP12_T BigInt = NULL ,@KLM_CMP21_T BigInt = NULL ,@KLM_CMP22_T BigInt = NULL ,@KLM_CMP31_T BigInt = NULL ,@KLM_CMP32_T BigInt = NULL ,@KLM_CMP51_T BigInt = NULL
 ,@KLM_CMP52_T BigInt = NULL ,@KLM_CMP61_T BigInt = NULL ,@KLM_CMP62_T BigInt = NULL ,@KLM_CMP71_T BigInt = NULL ,@KLM_CMP72_T BigInt = NULL ,@KLM_HS_BOV1 BigInt = NULL
 ,@KLM_HS_BOV2 BigInt = NULL ,@KLM_HS_BOV3 BigInt = NULL ,@KLM_HS_BOV5 BigInt = NULL ,@KLM_HS_BOV6 BigInt = NULL ,@KLM_HS_BOV7 BigInt = NULL ,@KLM_HS_END1 BigInt = NULL
 ,@KLM_HS_END2 BigInt = NULL ,@KLM_HS_END3 BigInt = NULL ,@KLM_HS_END5 BigInt = NULL ,@KLM_HS_END6 BigInt = NULL ,@KLM_HS_END7 BigInt = NULL ,@KLM_HS_OND1 BigInt = NULL
 ,@KLM_HS_OND2 BigInt = NULL ,@KLM_HS_OND3 BigInt = NULL ,@KLM_HS_OND5 BigInt = NULL ,@KLM_HS_OND6 BigInt = NULL ,@KLM_HS_OND7 BigInt = NULL ,@KLM_RELBOV_1 BigInt = NULL
 ,@KLM_RELBOV_2 BigInt = NULL ,@KLM_RELBOV_3 BigInt = NULL ,@KLM_RELBOV_5 BigInt = NULL ,@KLM_RELBOV_6 BigInt = NULL ,@KLM_RELBOV_7 BigInt = NULL ,@KLM_RELOND_1 BigInt = NULL
 ,@KLM_RELOND_2 BigInt = NULL ,@KLM_RELOND_3 BigInt = NULL ,@KLM_RELOND_5 BigInt = NULL ,@KLM_RELOND_6 BigInt = NULL ,@KLM_RELOND_7 BigInt = NULL ,@KLM_VENT1_M BigInt = NULL
 ,@KLM_VENT2_M BigInt = NULL ,@KLM_VENT3_M BigInt = NULL ,@KLM_VENT5_M BigInt = NULL ,@KLM_VENT6_M BigInt = NULL ,@KLM_VENT7_M BigInt = NULL ,@KM_TELLER BigInt = NULL ,@LSP_BAT_1_M BigInt = NULL
 ,@LSP_BAT_2_M BigInt = NULL ,@LSP_BAT_7_M BigInt = NULL ,@LSP_DCM_1_IN BigInt = NULL ,@LSP_DCM_1_M BigInt = NULL ,@LSP_DCM_2_IN BigInt = NULL ,@LSP_DCM_2_M BigInt = NULL
 ,@LSP_DCM_7_IN BigInt = NULL ,@LSP_DCM_7_M BigInt = NULL ,@LS_EN_DIENST BigInt = NULL ,@LS_EN_GEREED BigInt = NULL ,@LS_EN_GER_C BigInt = NULL ,@LVZ_CMP1_IN BigInt = NULL
 ,@LVZ_CMP1_M BigInt = NULL ,@NDREM1_VGR5 BigInt = NULL ,@NDREM2_VGR5 BigInt = NULL ,@REM_H_BED1 BigInt = NULL ,@REM_H_BED2 BigInt = NULL ,@REM_KIN_EP BigInt = NULL ,@REM_KIN_NR BigInt = NULL
 ,@REM_NR_BED1 BigInt = NULL ,@REM_NR_BED2 BigInt = NULL ,@REM_P_BED BigInt = NULL ,@REM_TRDR17_1 BigInt = NULL ,@REM_TRDR17_2 BigInt = NULL ,@REM_TRDR27_1 BigInt = NULL
 ,@REM_TRDR27_2 BigInt = NULL ,@REM_TRDR47_1 BigInt = NULL ,@REM_TRDR47_2 BigInt = NULL ,@REM_VENT17 BigInt = NULL ,@REM_VENT27 BigInt = NULL ,@REM_VENT47 BigInt = NULL
 ,@SNT_DRN_D_3 BigInt = NULL ,@SNT_DRN_D_6 BigInt = NULL ,@SNT_FRST_R_3 BigInt = NULL ,@SNT_FRST_R_5 BigInt = NULL ,@SNT_FRST_R_6 BigInt = NULL ,@SNT_HYG_R_3 BigInt = NULL
 ,@SNT_HYG_R_5 BigInt = NULL ,@SNT_HYG_R_6 BigInt = NULL ,@SNT_REV_SP_3 BigInt = NULL ,@SNT_REV_SP_5 BigInt = NULL ,@SNT_REV_SP_6 BigInt = NULL ,@SNT_SERV_SP3 BigInt = NULL
 ,@SNT_SERV_SP5 BigInt = NULL ,@SNT_SERV_SP6 BigInt = NULL ,@SNT_SPOEL_3 BigInt = NULL ,@SNT_SPOEL_5 BigInt = NULL ,@SNT_SPOEL_6 BigInt = NULL ,@SNT_UR_SP_3 BigInt = NULL
 ,@SNT_UR_SP_6 BigInt = NULL ,@SNT_WAS_KL_3 BigInt = NULL ,@SNT_WAS_KL_5 BigInt = NULL ,@SNT_WAS_KL_6 BigInt = NULL ,@SNT_Y1_OVER3 BigInt = NULL ,@SNT_Y1_OVER5 BigInt = NULL
 ,@SNT_Y1_OVER6 BigInt = NULL ,@SNT_Y2_HYG_3 BigInt = NULL ,@SNT_Y2_HYG_5 BigInt = NULL ,@SNT_Y2_HYG_6 BigInt = NULL ,@SNT_Y3_TRA_3 BigInt = NULL ,@SNT_Y3_TRA_5 BigInt = NULL
 ,@SNT_Y3_TRA_6 BigInt = NULL ,@SNT_Y4_FRST3 BigInt = NULL ,@SNT_Y4_FRST5 BigInt = NULL ,@SNT_Y4_FRST6 BigInt = NULL ,@SNT_Y5_TRA_3 BigInt = NULL ,@SNT_Y5_TRA_5 BigInt = NULL
 ,@SNT_Y5_TRA_6 BigInt = NULL ,@TRC_EN_AAN1 BigInt = NULL ,@TRC_EN_AAN2 BigInt = NULL ,@TRC_EN_AAN7 BigInt = NULL ,@TRC_EN_REC1 BigInt = NULL ,@TRC_EN_REC2 BigInt = NULL
 ,@TRC_EN_REC7 BigInt = NULL ,@TRC_IN_1_M BigInt = NULL ,@TRC_IN_2_M BigInt = NULL ,@TRC_IN_7_M BigInt = NULL ,@TRC_LS_1_IN BigInt = NULL ,@TRC_LS_2_IN BigInt = NULL
 ,@TRC_LS_7_IN BigInt = NULL ,@TRC_VENT_1_M BigInt = NULL ,@TRC_VENT_2_M BigInt = NULL ,@TRC_VENT_7_M BigInt = NULL ,@VERW_EN_B_D BigInt = NULL ,@VERW_EN_B_G BigInt = NULL
 ,@VERW_EN_B_GC BigInt = NULL ,@VERW_EN_E_D BigInt = NULL ,@VERW_EN_E_G BigInt = NULL ,@VERW_EN_E_GC BigInt = NULL ,@VERW_EN_O_D BigInt = NULL ,@VERW_EN_O_G BigInt = NULL
 ,@VERW_EN_O_GC BigInt = NULL ,@1Actueelkoppel_mBvk1 Integer = NULL ,@2Actueelkoppel_mBvk1 Integer = NULL, @7Actueelkoppel_mBvk1 Integer = NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT

DECLARE @Id bigint = null
,@unitID int

SELECT @unitId = ID FROM unit where UnitNumber = @UnitNumber

SELECT @id = ID FROM ChannelValue where TimeStamp = @Timestamp and UnitId = @unitID

IF @id IS NULL
BEGIN


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148]
           )
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@MCG_ILatitude, Col1) ,ISNULL(@MCG_ILongitude, Col2) ,ISNULL(@36A1_UIT_VERWARMINGONDERIN, Col3) ,ISNULL(@36A1_UIT_VERWARMINGBOVENIN, Col4) ,ISNULL(@36A1Verweindrin, Col5) 
,ISNULL(@317A1_Vrijgave_Openen, Col6) ,ISNULL(@327A1_Vrijgave_Openen, Col7) ,ISNULL(@337A1_Vrijgave_Openen, Col8) ,ISNULL(@347A1_Vrijgave_Openen, Col9) ,ISNULL(@34A1_DS_HL_MG, Col10) 
,ISNULL(@38A3_Compressor_1_IN, Col11) ,ISNULL(@38A3_Druk__8_BAR, Col12) ,ISNULL(@56A1_UIT_VERWARMINGONDERIN, Col13) ,ISNULL(@56A1_UIT_VERWARMINGBOVENIN, Col14) ,ISNULL(@56A1Verweindrin, Col15)
 ,ISNULL(@517A1_Vrijgave_Openen, Col16) ,ISNULL(@527A1_Vrijgave_Openen, Col17) ,ISNULL(@537A1_Vrijgave_Openen, Col18) ,ISNULL(@547A1_Vrijgave_Openen, Col19) ,ISNULL(@54A1_DS_HL_MG, Col20) 
 ,ISNULL(@64B5_HR_DRUK, Col21) ,ISNULL(@6Treinleidingdruk, Col22) ,ISNULL(@66A1_UIT_VERWARMINGONDERIN, Col23) ,ISNULL(@66A1_UIT_VERWARMINGBOVENIN, Col24) ,ISNULL(@66A1Verweindrin, Col25) 
 ,ISNULL(@617A1_Vrijgave_Openen, Col26) ,ISNULL(@627A1_Vrijgave_Openen, Col27) ,ISNULL(@637A1_Vrijgave_Openen, Col28) ,ISNULL(@647A1_Vrijgave_Openen, Col29) ,ISNULL(@64A1_DS_HL_MG, Col30) 
 ,ISNULL(@68A3_Compressor_Gevraagd, Col31) ,ISNULL(@6TLdrukkleinerals05Bar, Col32) ,ISNULL(@68A2_Druk__5_BAR, Col33) ,ISNULL(@68A2_Druk__85_BAR, Col34) ,ISNULL(@68A2_Druk__95_BAR, Col35) 
 ,ISNULL(@73A8_MW_UBAT_VOLT, Col36) ,ISNULL(@73A2_M_UT, Col37) ,ISNULL(@73A2_M_IT_GEM, Col38) ,ISNULL(@76A1_UIT_VERWARMINGONDERIN, Col39) ,ISNULL(@76A1_UIT_VERWARMINGBOVENIN, Col40) 
 ,ISNULL(@76A1Verweindrin, Col41) ,ISNULL(@717A1_Vrijgave_Openen, Col42) ,ISNULL(@727A1_Vrijgave_Openen, Col43) ,ISNULL(@737A1_Vrijgave_Openen, Col44) ,ISNULL(@747A1_Vrijgave_Openen, Col45) 
 ,ISNULL(@74A1_DS_HL_MG, Col46) ,ISNULL(@74A1_SLIP_ABI, Col47) ,ISNULL(@72A1_BELADING, Col48) ,ISNULL(@72A1_IN_Gevraagd_Koppel, Col49) ,ISNULL(@72A1_IN_Vooruit, Col50)
 ,ISNULL(@72A1_IN_Achteruit, Col51) ,ISNULL(@7Actueelkoppel, Col52) ,ISNULL(@72A1_ILACT, Col53) ,ISNULL(@72A1_METING_LIJNSPANNING, Col54) ,ISNULL(@72A1_Inverter_Frequentie, Col55)
 ,ISNULL(@72A1_Detectie_Wielslip, Col56) ,ISNULL(@78A4_IN_HS_AANWEZIG, Col57) ,ISNULL(@72A1_Tractie_Levert_Koppel, Col58) ,ISNULL(@13A8_MW_UBAT_VOLT, Col59) ,ISNULL(@13A2_M_UT, Col60) 
 ,ISNULL(@13A2_M_IT_GEM, Col61) ,ISNULL(@18A3_Gereed_Continu, Col62) ,ISNULL(@18A3_Bediende_Cabine, Col63) ,ISNULL(@18A3_Bedrijfstoestand_Gereed, Col64) ,ISNULL(@18A3_IN_Dienstvaardig, Col65) 
 ,ISNULL(@18A4_ATB_Snelrem, Col66) ,ISNULL(@18A4_IN_HS_AANWEZIG, Col67) ,ISNULL(@16A1_UIT_VERWARMINGONDERIN, Col68) ,ISNULL(@16A1_UIT_VERWARMINGBOVENIN, Col69) ,ISNULL(@16A1Verwcabinein, Col70) 
 ,ISNULL(@117A1_Vrijgave_Openen, Col71) ,ISNULL(@127A1_Vrijgave_Openen, Col72) ,ISNULL(@137A1_Vrijgave_Openen, Col73) ,ISNULL(@147A1_Vrijgave_Openen, Col74) ,ISNULL(@14A1_DS_HL_MG, Col75) 
 ,ISNULL(@14A1_SLIP_ABI, Col76) ,ISNULL(@1standremkraan5, Col77) ,ISNULL(@1standremkraan4, Col78) ,ISNULL(@1standremkraan3, Col79) ,ISNULL(@1standremkraan2, Col80) ,ISNULL(@1standremkraan1, Col81)
 ,ISNULL(@1standremkraanT, Col82) ,ISNULL(@1standremkraanA, Col83) ,ISNULL(@14A1_Rem_Noodbedrijf, Col84) ,ISNULL(@14A1_ST_SB, Col85) ,ISNULL(@1standremkraan7, Col86) ,ISNULL(@1standremkraan6, Col87)
 ,ISNULL(@14A1Noodremreizigers, Col88) ,ISNULL(@14A1_Ingang_Treindraad_17, Col89) ,ISNULL(@14A1_Ingang_Treindraad_27, Col90) ,ISNULL(@14A1_Ingang_Treindraad_47, Col91) ,ISNULL(@12A1_BELADING, Col92) 
 ,ISNULL(@12A1_IN_Gevraagd_Koppel, Col93) ,ISNULL(@12A1_IN_Vooruit, Col94) ,ISNULL(@12A1_IN_Achteruit, Col95) ,ISNULL(@1Actueelkoppel, Col96) ,ISNULL(@12A1_ILACT, Col97) 
 ,ISNULL(@12A1_METING_LIJNSPANNING, Col98) ,ISNULL(@12A1_Inverter_Frequentie, Col99) ,ISNULL(@12A1_Detectie_Wielslip, Col100) ,ISNULL(@18A2_ATB_Treinsnelheid, Col101) 
 ,ISNULL(@18A2_ATB_Bewaakte_Snelheid, Col102) ,ISNULL(@12A1_Tractie_Levert_Koppel, Col103) ,ISNULL(@23A8_MW_UBAT_VOLT, Col104) ,ISNULL(@23A2_M_UT, Col105) ,ISNULL(@23A2_M_IT_GEM, Col106) 
 ,ISNULL(@28A3_Gereed_Continu, Col107) ,ISNULL(@28A3_Bediende_Cabine, Col108) ,ISNULL(@28A3_Bedrijfstoestand_Gereed, Col109) ,ISNULL(@28A3_IN_Dienstvaardig, Col110) ,ISNULL(@28A4_ATB_Snelrem, Col111) 
 ,ISNULL(@28A4_IN_HS_AANWEZIG, Col112) ,ISNULL(@26A1_UIT_VERWARMINGONDERIN, Col113) ,ISNULL(@26A1_UIT_VERWARMINGBOVENIN, Col114) ,ISNULL(@26A1Verwcabinein, Col115)
 ,ISNULL(@217A1_Vrijgave_Openen, Col116) ,ISNULL(@227A1_Vrijgave_Openen, Col117) ,ISNULL(@237A1_Vrijgave_Openen, Col118) ,ISNULL(@247A1_Vrijgave_Openen, Col119) ,ISNULL(@24A1_DS_HL_MG, Col120)
 ,ISNULL(@24A1_SLIP_ABI, Col121) ,ISNULL(@2standremkraan5, Col122) ,ISNULL(@2standremkraan4, Col123) ,ISNULL(@2standremkraan3, Col124) ,ISNULL(@2standremkraan2, Col125)
 ,ISNULL(@2standremkraan1, Col126) ,ISNULL(@2standremkraanT, Col127) ,ISNULL(@2standremkraanA, Col128) ,ISNULL(@24A1_Rem_Noodbedrijf, Col129) ,ISNULL(@24A1_ST_SB, Col130)
 ,ISNULL(@2standremkraan7, Col131) ,ISNULL(@2standremkraan6, Col132) ,ISNULL(@24A1Noodremreizigers, Col133) ,ISNULL(@22A1_BELADING, Col134) ,ISNULL(@22A1_IN_Gevraagd_Koppel, Col135)
 ,ISNULL(@22A1_IN_Vooruit, Col136) ,ISNULL(@22A1_IN_Achteruit, Col137) ,ISNULL(@2Actueelkoppel, Col138) ,ISNULL(@22A1_ILACT, Col139) ,ISNULL(@22A1_METING_LIJNSPANNING, Col140) 
 ,ISNULL(@22A1_Inverter_Frequentie, Col141) ,ISNULL(@22A1_Detectie_Wielslip, Col142) ,ISNULL(@28A2_ATB_Treinsnelheid, Col143) ,ISNULL(@28A2_ATB_Bewaakte_Snelheid, Col144) 
 ,ISNULL(@22A1_Tractie_Levert_Koppel, Col145) ,ISNULL(@DIENSTVAARD, Col1001) ,ISNULL(@DIV_KOPPL_1, Col1002) ,ISNULL(@DIV_KOPPL_2, Col1003) ,ISNULL(@DR01_KLEM, Col1004)
 ,ISNULL(@DR02_KLEM, Col1005) ,ISNULL(@DR03_KLEM, Col1006) ,ISNULL(@DR04_KLEM, Col1007) ,ISNULL(@DR05_KLEM, Col1008) ,ISNULL(@DR06_KLEM, Col1009) ,ISNULL(@DR07_KLEM, Col1010)
 ,ISNULL(@DR08_KLEM, Col1011) ,ISNULL(@DR09_KLEM, Col1012) ,ISNULL(@DR10_KLEM, Col1013) ,ISNULL(@DR11_KLEM, Col1014) ,ISNULL(@DR12_KLEM, Col1015) ,ISNULL(@DR13_KLEM, Col1016)
 ,ISNULL(@DR14_KLEM, Col1017) ,ISNULL(@DR15_KLEM, Col1018) ,ISNULL(@DR16_KLEM, Col1019) ,ISNULL(@DR17_KLEM, Col1020) ,ISNULL(@DR18_KLEM, Col1021) ,ISNULL(@DR19_KLEM, Col1022)
 ,ISNULL(@DR20_KLEM, Col1023) ,ISNULL(@DR21_KLEM, Col1024) ,ISNULL(@DR22_KLEM, Col1025) ,ISNULL(@DR23_KLEM, Col1026) ,ISNULL(@DR24_KLEM, Col1027) ,ISNULL(@DRN_OCS_11, Col1028) 
 ,ISNULL(@DRN_OCS_12, Col1029) ,ISNULL(@DRN_OCS_13, Col1030) ,ISNULL(@DRN_OCS_14, Col1031) ,ISNULL(@DRN_OCS_21, Col1032) ,ISNULL(@DRN_OCS_22, Col1033) ,ISNULL(@DRN_OCS_23, Col1034) 
 ,ISNULL(@DRN_OCS_24, Col1035) ,ISNULL(@DRN_OCS_31, Col1036) ,ISNULL(@DRN_OCS_32, Col1037) ,ISNULL(@DRN_OCS_33, Col1038) ,ISNULL(@DRN_OCS_34, Col1039) ,ISNULL(@DRN_OCS_51, Col1040) 
 ,ISNULL(@DRN_OCS_52, Col1041) ,ISNULL(@DRN_OCS_53, Col1042) ,ISNULL(@DRN_OCS_54, Col1043) ,ISNULL(@DRN_OCS_61, Col1044) ,ISNULL(@DRN_OCS_62, Col1045) ,ISNULL(@DRN_OCS_63, Col1046) 
 ,ISNULL(@DRN_OCS_64, Col1047) ,ISNULL(@DRN_OCS_71, Col1048) ,ISNULL(@DRN_OCS_72, Col1049) ,ISNULL(@DRN_OCS_73, Col1050) ,ISNULL(@DRN_OCS_74, Col1051) ,ISNULL(@DR_KLEM_Z1, Col1052)
 ,ISNULL(@DR_KLEM_Z2, Col1053) ,ISNULL(@GEREED, Col1054) ,ISNULL(@GEREEDCONT, Col1055) ,ISNULL(@HSP_SA_KM1, Col1056) ,ISNULL(@HSP_SA_KM2, Col1057) ,ISNULL(@HS_AAN_SSUIT, Col1058)
 ,ISNULL(@HS_AFW_SSUIT, Col1059) ,ISNULL(@KLM_BUIBOV_1, Col1060) ,ISNULL(@KLM_BUIBOV_2, Col1061) ,ISNULL(@KLM_BUIBOV_3, Col1062) ,ISNULL(@KLM_BUIBOV_5, Col1063) ,ISNULL(@KLM_BUIBOV_6, Col1064)
 ,ISNULL(@KLM_BUIBOV_7, Col1065) ,ISNULL(@KLM_BUIOND_1, Col1066) ,ISNULL(@KLM_BUIOND_2, Col1067) ,ISNULL(@KLM_BUIOND_3, Col1068) ,ISNULL(@KLM_BUIOND_5, Col1069) ,ISNULL(@KLM_BUIOND_6, Col1070) 
 ,ISNULL(@KLM_BUIOND_7, Col1071) ,ISNULL(@KLM_CMP11_T, Col1072) ,ISNULL(@KLM_CMP12_T, Col1073) ,ISNULL(@KLM_CMP21_T, Col1074) ,ISNULL(@KLM_CMP22_T, Col1075) ,ISNULL(@KLM_CMP31_T, Col1076) 
 ,ISNULL(@KLM_CMP32_T, Col1077) ,ISNULL(@KLM_CMP51_T, Col1078) ,ISNULL(@KLM_CMP52_T, Col1079) ,ISNULL(@KLM_CMP61_T, Col1080) ,ISNULL(@KLM_CMP62_T, Col1081) ,ISNULL(@KLM_CMP71_T, Col1082)
 ,ISNULL(@KLM_CMP72_T, Col1083) ,ISNULL(@KLM_HS_BOV1, Col1084) ,ISNULL(@KLM_HS_BOV2, Col1085) ,ISNULL(@KLM_HS_BOV3, Col1086) ,ISNULL(@KLM_HS_BOV5, Col1087) ,ISNULL(@KLM_HS_BOV6, Col1088)
 ,ISNULL(@KLM_HS_BOV7, Col1089) ,ISNULL(@KLM_HS_END1, Col1090) ,ISNULL(@KLM_HS_END2, Col1091) ,ISNULL(@KLM_HS_END3, Col1092) ,ISNULL(@KLM_HS_END5, Col1093) ,ISNULL(@KLM_HS_END6, Col1094)
 ,ISNULL(@KLM_HS_END7, Col1095) ,ISNULL(@KLM_HS_OND1, Col1096) ,ISNULL(@KLM_HS_OND2, Col1097) ,ISNULL(@KLM_HS_OND3, Col1098) ,ISNULL(@KLM_HS_OND5, Col1099) ,ISNULL(@KLM_HS_OND6, Col1100) 
 ,ISNULL(@KLM_HS_OND7, Col1101) ,ISNULL(@KLM_RELBOV_1, Col1102) ,ISNULL(@KLM_RELBOV_2, Col1103) ,ISNULL(@KLM_RELBOV_3, Col1104) ,ISNULL(@KLM_RELBOV_5, Col1105) ,ISNULL(@KLM_RELBOV_6, Col1106)
 ,ISNULL(@KLM_RELBOV_7, Col1107) ,ISNULL(@KLM_RELOND_1, Col1108) ,ISNULL(@KLM_RELOND_2, Col1109) ,ISNULL(@KLM_RELOND_3, Col1110) ,ISNULL(@KLM_RELOND_5, Col1111) ,ISNULL(@KLM_RELOND_6, Col1112) 
 ,ISNULL(@KLM_RELOND_7, Col1113) ,ISNULL(@KLM_VENT1_M, Col1114) ,ISNULL(@KLM_VENT2_M, Col1115) ,ISNULL(@KLM_VENT3_M, Col1116) ,ISNULL(@KLM_VENT5_M, Col1117) ,ISNULL(@KLM_VENT6_M, Col1118)
 ,ISNULL(@KLM_VENT7_M, Col1119) ,ISNULL(@KM_TELLER, Col1120) ,ISNULL(@LSP_BAT_1_M, Col1121) ,ISNULL(@LSP_BAT_2_M, Col1122) ,ISNULL(@LSP_BAT_7_M, Col1123) ,ISNULL(@LSP_DCM_1_IN, Col1124) 
 ,ISNULL(@LSP_DCM_1_M, Col1125) ,ISNULL(@LSP_DCM_2_IN, Col1126) ,ISNULL(@LSP_DCM_2_M, Col1127) ,ISNULL(@LSP_DCM_7_IN, Col1128) ,ISNULL(@LSP_DCM_7_M, Col1129) ,ISNULL(@LS_EN_DIENST, Col1130) 
 ,ISNULL(@LS_EN_GEREED, Col1131) ,ISNULL(@LS_EN_GER_C, Col1132) ,ISNULL(@LVZ_CMP1_IN, Col1133) ,ISNULL(@LVZ_CMP1_M, Col1134) ,ISNULL(@NDREM1_VGR5, Col1135) ,ISNULL(@NDREM2_VGR5, Col1136) 
 ,ISNULL(@REM_H_BED1, Col1137) ,ISNULL(@REM_H_BED2, Col1138) ,ISNULL(@REM_KIN_EP, Col1139) ,ISNULL(@REM_KIN_NR, Col1140) ,ISNULL(@REM_NR_BED1, Col1141) ,ISNULL(@REM_NR_BED2, Col1142) 
 ,ISNULL(@REM_P_BED, Col1143) ,ISNULL(@REM_TRDR17_1, Col1144) ,ISNULL(@REM_TRDR17_2, Col1145) ,ISNULL(@REM_TRDR27_1, Col1146) ,ISNULL(@REM_TRDR27_2, Col1147) ,ISNULL(@REM_TRDR47_1, Col1148)
 ,ISNULL(@REM_TRDR47_2, Col1149) ,ISNULL(@REM_VENT17, Col1150) ,ISNULL(@REM_VENT27, Col1151) ,ISNULL(@REM_VENT47, Col1152) ,ISNULL(@SNT_DRN_D_3, Col1153) ,ISNULL(@SNT_DRN_D_6, Col1154) 
 ,ISNULL(@SNT_FRST_R_3, Col1155) ,ISNULL(@SNT_FRST_R_5, Col1156) ,ISNULL(@SNT_FRST_R_6, Col1157) ,ISNULL(@SNT_HYG_R_3, Col1158) ,ISNULL(@SNT_HYG_R_5, Col1159) ,ISNULL(@SNT_HYG_R_6, Col1160) 
 ,ISNULL(@SNT_REV_SP_3, Col1161) ,ISNULL(@SNT_REV_SP_5, Col1162) ,ISNULL(@SNT_REV_SP_6, Col1163) ,ISNULL(@SNT_SERV_SP3, Col1164) ,ISNULL(@SNT_SERV_SP5, Col1165) ,ISNULL(@SNT_SERV_SP6, Col1166)
 ,ISNULL(@SNT_SPOEL_3, Col1167) ,ISNULL(@SNT_SPOEL_5, Col1168) ,ISNULL(@SNT_SPOEL_6, Col1169) ,ISNULL(@SNT_UR_SP_3, Col1170) ,ISNULL(@SNT_UR_SP_6, Col1171) ,ISNULL(@SNT_WAS_KL_3, Col1172) 
 ,ISNULL(@SNT_WAS_KL_5, Col1173) ,ISNULL(@SNT_WAS_KL_6, Col1174) ,ISNULL(@SNT_Y1_OVER3, Col1175) ,ISNULL(@SNT_Y1_OVER5, Col1176) ,ISNULL(@SNT_Y1_OVER6, Col1177) ,ISNULL(@SNT_Y2_HYG_3, Col1178) 
 ,ISNULL(@SNT_Y2_HYG_5, Col1179) ,ISNULL(@SNT_Y2_HYG_6, Col1180) ,ISNULL(@SNT_Y3_TRA_3, Col1181) ,ISNULL(@SNT_Y3_TRA_5, Col1182) ,ISNULL(@SNT_Y3_TRA_6, Col1183) ,ISNULL(@SNT_Y4_FRST3, Col1184) 
 ,ISNULL(@SNT_Y4_FRST5, Col1185) ,ISNULL(@SNT_Y4_FRST6, Col1186) ,ISNULL(@SNT_Y5_TRA_3, Col1187) ,ISNULL(@SNT_Y5_TRA_5, Col1188) ,ISNULL(@SNT_Y5_TRA_6, Col1189) ,ISNULL(@TRC_EN_AAN1, Col1190) 
 ,ISNULL(@TRC_EN_AAN2, Col1191) ,ISNULL(@TRC_EN_AAN7, Col1192) ,ISNULL(@TRC_EN_REC1, Col1193) ,ISNULL(@TRC_EN_REC2, Col1194) ,ISNULL(@TRC_EN_REC7, Col1195) ,ISNULL(@TRC_IN_1_M, Col1196)
 ,ISNULL(@TRC_IN_2_M, Col1197) ,ISNULL(@TRC_IN_7_M, Col1198) ,ISNULL(@TRC_LS_1_IN, Col1199) ,ISNULL(@TRC_LS_2_IN, Col1200) ,ISNULL(@TRC_LS_7_IN, Col1201) ,ISNULL(@TRC_VENT_1_M, Col1202)
 ,ISNULL(@TRC_VENT_2_M, Col1203) ,ISNULL(@TRC_VENT_7_M, Col1204) ,ISNULL(@VERW_EN_B_D, Col1205) ,ISNULL(@VERW_EN_B_G, Col1206) ,ISNULL(@VERW_EN_B_GC, Col1207) ,ISNULL(@VERW_EN_E_D, Col1208) 
 ,ISNULL(@VERW_EN_E_G, Col1209) ,ISNULL(@VERW_EN_E_GC, Col1210) ,ISNULL(@VERW_EN_O_D, Col1211) ,ISNULL(@VERW_EN_O_G, Col1212) ,ISNULL(@VERW_EN_O_GC, Col1213) ,ISNULL(@1Actueelkoppel_mBvk1, Col146) 
 ,ISNULL(@2Actueelkoppel_mBvk1, Col147) ,ISNULL(@7Actueelkoppel_mBvk1, Col148)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

END

END

GO

----------------------------------------------------------------------------

RAISERROR ('-- update NSP_InsertChannelValue with new columns', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
	    @UpdateRecord bit ,
		@Col1 decimal (9,6) = NULL ,@Col2 decimal (9,6) = NULL ,@Col3 Bit = NULL ,@Col4 Bit = NULL ,@Col5 Bit = NULL ,@Col6 Bit = NULL ,@Col7 Bit = NULL ,@Col8 Bit = NULL ,@Col9 Bit = NULL ,@Col10 Bit = NULL ,
@Col11 Bit = NULL ,@Col12 Bit = NULL ,@Col13 Bit = NULL ,@Col14 Bit = NULL ,@Col15 Bit = NULL ,@Col16 Bit = NULL ,@Col17 Bit = NULL ,@Col18 Bit = NULL ,@Col19 Bit = NULL ,@Col20 Bit = NULL ,
@Col21 decimal(5,1) = NULL ,@Col22 decimal(5,1) = NULL ,@Col23 Bit = NULL ,@Col24 Bit = NULL ,@Col25 Bit = NULL ,@Col26 Bit = NULL ,@Col27 Bit = NULL ,@Col28 Bit = NULL ,@Col29 Bit = NULL ,@Col30 Bit = NULL ,
@Col31 Bit = NULL ,@Col32 Bit = NULL ,@Col33 Bit = NULL ,@Col34 Bit = NULL ,@Col35 Bit = NULL ,@Col36 Integer = NULL ,@Col37 Bit = NULL ,@Col38 decimal(5,1) = NULL ,@Col39 Bit = NULL ,@Col40 Bit = NULL ,
@Col41 Bit = NULL ,@Col42 Bit = NULL ,@Col43 Bit = NULL ,@Col44 Bit = NULL ,@Col45 Bit = NULL ,@Col46 Bit = NULL ,@Col47 Bit = NULL ,@Col48 Bit = NULL ,@Col49 Integer = NULL ,@Col50 Bit = NULL ,
@Col51 Bit = NULL ,@Col52 Integer = NULL ,@Col53 Bit = NULL ,@Col54 Bit = NULL ,@Col55 Integer = NULL ,@Col56 Bit = NULL ,@Col57 Bit = NULL ,@Col58 Bit = NULL ,@Col59 Integer = NULL ,@Col60 Bit = NULL ,
@Col61 decimal(5,1) = NULL ,@Col62 Bit = NULL ,@Col63 Bit = NULL ,@Col64 Bit = NULL ,@Col65 Bit = NULL ,@Col66 Bit = NULL ,@Col67 Bit = NULL ,@Col68 Bit = NULL ,@Col69 Bit = NULL ,@Col70 Bit = NULL ,
@Col71 Bit = NULL ,@Col72 Bit = NULL ,@Col73 Bit = NULL ,@Col74 Bit = NULL ,@Col75 Bit = NULL ,@Col76 Bit = NULL ,@Col77 Bit = NULL ,@Col78 Bit = NULL ,@Col79 Bit = NULL ,@Col80 Bit = NULL ,@Col81 Bit = NULL ,
@Col82 Bit = NULL ,@Col83 Bit = NULL ,@Col84 Bit = NULL ,@Col85 Bit = NULL ,@Col86 Bit = NULL ,@Col87 Bit = NULL ,@Col88 Bit = NULL ,@Col89 Bit = NULL ,@Col90 Bit = NULL ,@Col91 Bit = NULL ,@Col92 Bit = NULL ,
@Col93 Integer = NULL ,@Col94 Bit = NULL ,@Col95 Bit = NULL ,@Col96 Integer = NULL ,@Col97 Bit = NULL ,@Col98 Bit = NULL ,@Col99 Integer = NULL ,@Col100 Bit = NULL ,@Col101 Integer = NULL ,
@Col102 Integer = NULL ,@Col103 Bit = NULL ,@Col104 Integer = NULL ,@Col105 Bit = NULL ,@Col106 decimal(5,1) = NULL ,@Col107 Bit = NULL ,@Col108 Bit = NULL ,@Col109 Bit = NULL ,@Col110 Bit = NULL ,
@Col111 Bit = NULL ,@Col112 Bit = NULL ,@Col113 Bit = NULL ,@Col114 Bit = NULL ,@Col115 Bit = NULL ,@Col116 Bit = NULL ,@Col117 Bit = NULL ,@Col118 Bit = NULL ,@Col119 Bit = NULL ,@Col120 Bit = NULL ,
@Col121 Bit = NULL ,@Col122 Bit = NULL ,@Col123 Bit = NULL ,@Col124 Bit = NULL ,@Col125 Bit = NULL ,@Col126 Bit = NULL ,@Col127 Bit = NULL ,@Col128 Bit = NULL ,@Col129 Bit = NULL ,@Col130 Bit = NULL ,
@Col131 Bit = NULL ,@Col132 Bit = NULL ,@Col133 Bit = NULL ,@Col134 Bit = NULL ,@Col135 Integer = NULL ,@Col136 Bit = NULL ,@Col137 Bit = NULL ,@Col138 Integer = NULL ,@Col139 Bit = NULL ,@Col140 Bit = NULL ,
@Col141 Integer = NULL ,@Col142 Bit = NULL ,@Col143 Integer = NULL ,@Col144 Integer = NULL ,@Col145 Bit = NULL ,@Col1001 BigInt = NULL ,@Col1002 BigInt = NULL ,@Col1003 BigInt = NULL ,@Col1004 BigInt = NULL ,
@Col1005 BigInt = NULL ,@Col1006 BigInt = NULL ,@Col1007 BigInt = NULL ,@Col1008 BigInt = NULL ,@Col1009 BigInt = NULL ,@Col1010 BigInt = NULL ,@Col1011 BigInt = NULL ,@Col1012 BigInt = NULL ,
@Col1013 BigInt = NULL ,@Col1014 BigInt = NULL ,@Col1015 BigInt = NULL ,@Col1016 BigInt = NULL ,@Col1017 BigInt = NULL ,@Col1018 BigInt = NULL ,@Col1019 BigInt = NULL ,@Col1020 BigInt = NULL ,
@Col1021 BigInt = NULL ,@Col1022 BigInt = NULL ,@Col1023 BigInt = NULL ,@Col1024 BigInt = NULL ,@Col1025 BigInt = NULL ,@Col1026 BigInt = NULL ,@Col1027 BigInt = NULL ,@Col1028 BigInt = NULL ,
@Col1029 BigInt = NULL ,@Col1030 BigInt = NULL ,@Col1031 BigInt = NULL ,@Col1032 BigInt = NULL ,@Col1033 BigInt = NULL ,@Col1034 BigInt = NULL ,@Col1035 BigInt = NULL ,@Col1036 BigInt = NULL ,
@Col1037 BigInt = NULL ,@Col1038 BigInt = NULL ,@Col1039 BigInt = NULL ,@Col1040 BigInt = NULL ,@Col1041 BigInt = NULL ,@Col1042 BigInt = NULL ,@Col1043 BigInt = NULL ,@Col1044 BigInt = NULL ,
@Col1045 BigInt = NULL ,@Col1046 BigInt = NULL ,@Col1047 BigInt = NULL ,@Col1048 BigInt = NULL ,@Col1049 BigInt = NULL ,@Col1050 BigInt = NULL ,@Col1051 BigInt = NULL ,@Col1052 BigInt = NULL ,
@Col1053 BigInt = NULL ,@Col1054 BigInt = NULL ,@Col1055 BigInt = NULL ,@Col1056 BigInt = NULL ,@Col1057 BigInt = NULL ,@Col1058 BigInt = NULL ,@Col1059 BigInt = NULL ,@Col1060 BigInt = NULL ,
@Col1061 BigInt = NULL ,@Col1062 BigInt = NULL ,@Col1063 BigInt = NULL ,@Col1064 BigInt = NULL ,@Col1065 BigInt = NULL ,@Col1066 BigInt = NULL ,@Col1067 BigInt = NULL ,@Col1068 BigInt = NULL ,
@Col1069 BigInt = NULL ,@Col1070 BigInt = NULL ,@Col1071 BigInt = NULL ,@Col1072 BigInt = NULL ,@Col1073 BigInt = NULL ,@Col1074 BigInt = NULL ,@Col1075 BigInt = NULL ,@Col1076 BigInt = NULL ,
@Col1077 BigInt = NULL ,@Col1078 BigInt = NULL ,@Col1079 BigInt = NULL ,@Col1080 BigInt = NULL ,@Col1081 BigInt = NULL ,@Col1082 BigInt = NULL ,@Col1083 BigInt = NULL ,@Col1084 BigInt = NULL ,
@Col1085 BigInt = NULL ,@Col1086 BigInt = NULL ,@Col1087 BigInt = NULL ,@Col1088 BigInt = NULL ,@Col1089 BigInt = NULL ,@Col1090 BigInt = NULL ,@Col1091 BigInt = NULL ,@Col1092 BigInt = NULL ,
@Col1093 BigInt = NULL ,@Col1094 BigInt = NULL ,@Col1095 BigInt = NULL ,@Col1096 BigInt = NULL ,@Col1097 BigInt = NULL ,@Col1098 BigInt = NULL ,@Col1099 BigInt = NULL ,@Col1100 BigInt = NULL ,
@Col1101 BigInt = NULL ,@Col1102 BigInt = NULL ,@Col1103 BigInt = NULL ,@Col1104 BigInt = NULL ,@Col1105 BigInt = NULL ,@Col1106 BigInt = NULL ,@Col1107 BigInt = NULL ,@Col1108 BigInt = NULL ,
@Col1109 BigInt = NULL ,@Col1110 BigInt = NULL ,@Col1111 BigInt = NULL ,@Col1112 BigInt = NULL ,@Col1113 BigInt = NULL ,@Col1114 BigInt = NULL ,@Col1115 BigInt = NULL ,@Col1116 BigInt = NULL ,
@Col1117 BigInt = NULL ,@Col1118 BigInt = NULL ,@Col1119 BigInt = NULL ,@Col1120 BigInt = NULL ,@Col1121 BigInt = NULL ,@Col1122 BigInt = NULL ,@Col1123 BigInt = NULL ,@Col1124 BigInt = NULL ,
@Col1125 BigInt = NULL ,@Col1126 BigInt = NULL ,@Col1127 BigInt = NULL ,@Col1128 BigInt = NULL ,@Col1129 BigInt = NULL ,@Col1130 BigInt = NULL ,@Col1131 BigInt = NULL ,@Col1132 BigInt = NULL ,
@Col1133 BigInt = NULL ,@Col1134 BigInt = NULL ,@Col1135 BigInt = NULL ,@Col1136 BigInt = NULL ,@Col1137 BigInt = NULL ,@Col1138 BigInt = NULL ,@Col1139 BigInt = NULL ,@Col1140 BigInt = NULL ,
@Col1141 BigInt = NULL ,@Col1142 BigInt = NULL ,@Col1143 BigInt = NULL ,@Col1144 BigInt = NULL ,@Col1145 BigInt = NULL ,@Col1146 BigInt = NULL ,@Col1147 BigInt = NULL ,@Col1148 BigInt = NULL ,
@Col1149 BigInt = NULL ,@Col1150 BigInt = NULL ,@Col1151 BigInt = NULL ,@Col1152 BigInt = NULL ,@Col1153 BigInt = NULL ,@Col1154 BigInt = NULL ,@Col1155 BigInt = NULL ,@Col1156 BigInt = NULL ,
@Col1157 BigInt = NULL ,@Col1158 BigInt = NULL ,@Col1159 BigInt = NULL ,@Col1160 BigInt = NULL ,@Col1161 BigInt = NULL ,@Col1162 BigInt = NULL ,@Col1163 BigInt = NULL ,@Col1164 BigInt = NULL ,
@Col1165 BigInt = NULL ,@Col1166 BigInt = NULL ,@Col1167 BigInt = NULL ,@Col1168 BigInt = NULL ,@Col1169 BigInt = NULL ,@Col1170 BigInt = NULL ,@Col1171 BigInt = NULL ,@Col1172 BigInt = NULL ,
@Col1173 BigInt = NULL ,@Col1174 BigInt = NULL ,@Col1175 BigInt = NULL ,@Col1176 BigInt = NULL ,@Col1177 BigInt = NULL ,@Col1178 BigInt = NULL ,@Col1179 BigInt = NULL ,@Col1180 BigInt = NULL ,
@Col1181 BigInt = NULL ,@Col1182 BigInt = NULL ,@Col1183 BigInt = NULL ,@Col1184 BigInt = NULL ,@Col1185 BigInt = NULL ,@Col1186 BigInt = NULL ,@Col1187 BigInt = NULL ,@Col1188 BigInt = NULL ,
@Col1189 BigInt = NULL ,@Col1190 BigInt = NULL ,@Col1191 BigInt = NULL ,@Col1192 BigInt = NULL ,@Col1193 BigInt = NULL ,@Col1194 BigInt = NULL ,@Col1195 BigInt = NULL ,@Col1196 BigInt = NULL ,
@Col1197 BigInt = NULL ,@Col1198 BigInt = NULL ,@Col1199 BigInt = NULL ,@Col1200 BigInt = NULL ,@Col1201 BigInt = NULL ,@Col1202 BigInt = NULL ,@Col1203 BigInt = NULL ,@Col1204 BigInt = NULL ,
@Col1205 BigInt = NULL ,@Col1206 BigInt = NULL ,@Col1207 BigInt = NULL ,@Col1208 BigInt = NULL ,@Col1209 BigInt = NULL ,@Col1210 BigInt = NULL ,@Col1211 BigInt = NULL ,@Col1212 BigInt = NULL ,
@Col1213 BigInt = NULL ,@Col146 Integer = NULL ,@Col147 Integer = NULL ,@Col148 Integer = NULL
		)
		AS
		BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148]
           )
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@Col1, Col1) ,ISNULL(@Col2, Col2) ,ISNULL(@Col3, Col3) ,ISNULL(@Col4, Col4) ,ISNULL(@Col5, Col5) ,ISNULL(@Col6, Col6) ,ISNULL(@Col7, Col7) ,ISNULL(@Col8, Col8) ,ISNULL(@Col9, Col9) ,ISNULL(@Col10, Col10)
 ,ISNULL(@Col11, Col11) ,ISNULL(@Col12, Col12) ,ISNULL(@Col13, Col13) ,ISNULL(@Col14, Col14) ,ISNULL(@Col15, Col15) ,ISNULL(@Col16, Col16) ,ISNULL(@Col17, Col17) ,ISNULL(@Col18, Col18) ,ISNULL(@Col19, Col19)
 ,ISNULL(@Col20, Col20) ,ISNULL(@Col21, Col21) ,ISNULL(@Col22, Col22) ,ISNULL(@Col23, Col23) ,ISNULL(@Col24, Col24) ,ISNULL(@Col25, Col25) ,ISNULL(@Col26, Col26) ,ISNULL(@Col27, Col27) ,ISNULL(@Col28, Col28)
 ,ISNULL(@Col29, Col29) ,ISNULL(@Col30, Col30) ,ISNULL(@Col31, Col31) ,ISNULL(@Col32, Col32) ,ISNULL(@Col33, Col33) ,ISNULL(@Col34, Col34) ,ISNULL(@Col35, Col35) ,ISNULL(@Col36, Col36) ,ISNULL(@Col37, Col37)
 ,ISNULL(@Col38, Col38) ,ISNULL(@Col39, Col39) ,ISNULL(@Col40, Col40) ,ISNULL(@Col41, Col41) ,ISNULL(@Col42, Col42) ,ISNULL(@Col43, Col43) ,ISNULL(@Col44, Col44) ,ISNULL(@Col45, Col45) ,ISNULL(@Col46, Col46)
 ,ISNULL(@Col47, Col47) ,ISNULL(@Col48, Col48) ,ISNULL(@Col49, Col49) ,ISNULL(@Col50, Col50) ,ISNULL(@Col51, Col51) ,ISNULL(@Col52, Col52) ,ISNULL(@Col53, Col53) ,ISNULL(@Col54, Col54) ,ISNULL(@Col55, Col55)
 ,ISNULL(@Col56, Col56) ,ISNULL(@Col57, Col57) ,ISNULL(@Col58, Col58) ,ISNULL(@Col59, Col59) ,ISNULL(@Col60, Col60) ,ISNULL(@Col61, Col61) ,ISNULL(@Col62, Col62) ,ISNULL(@Col63, Col63) ,ISNULL(@Col64, Col64)
 ,ISNULL(@Col65, Col65) ,ISNULL(@Col66, Col66) ,ISNULL(@Col67, Col67) ,ISNULL(@Col68, Col68) ,ISNULL(@Col69, Col69) ,ISNULL(@Col70, Col70) ,ISNULL(@Col71, Col71) ,ISNULL(@Col72, Col72) ,ISNULL(@Col73, Col73)
 ,ISNULL(@Col74, Col74) ,ISNULL(@Col75, Col75) ,ISNULL(@Col76, Col76) ,ISNULL(@Col77, Col77) ,ISNULL(@Col78, Col78) ,ISNULL(@Col79, Col79) ,ISNULL(@Col80, Col80) ,ISNULL(@Col81, Col81) ,ISNULL(@Col82, Col82)
 ,ISNULL(@Col83, Col83) ,ISNULL(@Col84, Col84) ,ISNULL(@Col85, Col85) ,ISNULL(@Col86, Col86) ,ISNULL(@Col87, Col87) ,ISNULL(@Col88, Col88) ,ISNULL(@Col89, Col89) ,ISNULL(@Col90, Col90) ,ISNULL(@Col91, Col91)
 ,ISNULL(@Col92, Col92) ,ISNULL(@Col93, Col93) ,ISNULL(@Col94, Col94) ,ISNULL(@Col95, Col95) ,ISNULL(@Col96, Col96) ,ISNULL(@Col97, Col97) ,ISNULL(@Col98, Col98) ,ISNULL(@Col99, Col99) ,ISNULL(@Col100, Col100)
 ,ISNULL(@Col101, Col101) ,ISNULL(@Col102, Col102) ,ISNULL(@Col103, Col103) ,ISNULL(@Col104, Col104) ,ISNULL(@Col105, Col105) ,ISNULL(@Col106, Col106) ,ISNULL(@Col107, Col107) ,ISNULL(@Col108, Col108) 
 ,ISNULL(@Col109, Col109) ,ISNULL(@Col110, Col110) ,ISNULL(@Col111, Col111) ,ISNULL(@Col112, Col112) ,ISNULL(@Col113, Col113) ,ISNULL(@Col114, Col114) ,ISNULL(@Col115, Col115) ,ISNULL(@Col116, Col116) 
 ,ISNULL(@Col117, Col117) ,ISNULL(@Col118, Col118) ,ISNULL(@Col119, Col119) ,ISNULL(@Col120, Col120) ,ISNULL(@Col121, Col121) ,ISNULL(@Col122, Col122) ,ISNULL(@Col123, Col123) ,ISNULL(@Col124, Col124) 
 ,ISNULL(@Col125, Col125) ,ISNULL(@Col126, Col126) ,ISNULL(@Col127, Col127) ,ISNULL(@Col128, Col128) ,ISNULL(@Col129, Col129) ,ISNULL(@Col130, Col130) ,ISNULL(@Col131, Col131) ,ISNULL(@Col132, Col132) 
 ,ISNULL(@Col133, Col133) ,ISNULL(@Col134, Col134) ,ISNULL(@Col135, Col135) ,ISNULL(@Col136, Col136) ,ISNULL(@Col137, Col137) ,ISNULL(@Col138, Col138) ,ISNULL(@Col139, Col139) ,ISNULL(@Col140, Col140) 
 ,ISNULL(@Col141, Col141) ,ISNULL(@Col142, Col142) ,ISNULL(@Col143, Col143) ,ISNULL(@Col144, Col144) ,ISNULL(@Col145, Col145) ,ISNULL(@Col1001, Col1001) ,ISNULL(@Col1002, Col1002) ,ISNULL(@Col1003, Col1003) 
 ,ISNULL(@Col1004, Col1004) ,ISNULL(@Col1005, Col1005) ,ISNULL(@Col1006, Col1006) ,ISNULL(@Col1007, Col1007) ,ISNULL(@Col1008, Col1008) ,ISNULL(@Col1009, Col1009) ,ISNULL(@Col1010, Col1010) ,ISNULL(@Col1011, Col1011)
 ,ISNULL(@Col1012, Col1012) ,ISNULL(@Col1013, Col1013) ,ISNULL(@Col1014, Col1014) ,ISNULL(@Col1015, Col1015) ,ISNULL(@Col1016, Col1016) ,ISNULL(@Col1017, Col1017) ,ISNULL(@Col1018, Col1018) ,ISNULL(@Col1019, Col1019)
 ,ISNULL(@Col1020, Col1020) ,ISNULL(@Col1021, Col1021) ,ISNULL(@Col1022, Col1022) ,ISNULL(@Col1023, Col1023) ,ISNULL(@Col1024, Col1024) ,ISNULL(@Col1025, Col1025) ,ISNULL(@Col1026, Col1026) ,ISNULL(@Col1027, Col1027)
 ,ISNULL(@Col1028, Col1028) ,ISNULL(@Col1029, Col1029) ,ISNULL(@Col1030, Col1030) ,ISNULL(@Col1031, Col1031) ,ISNULL(@Col1032, Col1032) ,ISNULL(@Col1033, Col1033) ,ISNULL(@Col1034, Col1034) ,ISNULL(@Col1035, Col1035)
 ,ISNULL(@Col1036, Col1036) ,ISNULL(@Col1037, Col1037) ,ISNULL(@Col1038, Col1038) ,ISNULL(@Col1039, Col1039) ,ISNULL(@Col1040, Col1040) ,ISNULL(@Col1041, Col1041) ,ISNULL(@Col1042, Col1042) ,ISNULL(@Col1043, Col1043) 
 ,ISNULL(@Col1044, Col1044) ,ISNULL(@Col1045, Col1045) ,ISNULL(@Col1046, Col1046) ,ISNULL(@Col1047, Col1047) ,ISNULL(@Col1048, Col1048) ,ISNULL(@Col1049, Col1049) ,ISNULL(@Col1050, Col1050) ,ISNULL(@Col1051, Col1051)
 ,ISNULL(@Col1052, Col1052) ,ISNULL(@Col1053, Col1053) ,ISNULL(@Col1054, Col1054) ,ISNULL(@Col1055, Col1055) ,ISNULL(@Col1056, Col1056) ,ISNULL(@Col1057, Col1057) ,ISNULL(@Col1058, Col1058) ,ISNULL(@Col1059, Col1059)
 ,ISNULL(@Col1060, Col1060) ,ISNULL(@Col1061, Col1061) ,ISNULL(@Col1062, Col1062) ,ISNULL(@Col1063, Col1063) ,ISNULL(@Col1064, Col1064) ,ISNULL(@Col1065, Col1065) ,ISNULL(@Col1066, Col1066) ,ISNULL(@Col1067, Col1067)
 ,ISNULL(@Col1068, Col1068) ,ISNULL(@Col1069, Col1069) ,ISNULL(@Col1070, Col1070) ,ISNULL(@Col1071, Col1071) ,ISNULL(@Col1072, Col1072) ,ISNULL(@Col1073, Col1073) ,ISNULL(@Col1074, Col1074) ,ISNULL(@Col1075, Col1075)
 ,ISNULL(@Col1076, Col1076) ,ISNULL(@Col1077, Col1077) ,ISNULL(@Col1078, Col1078) ,ISNULL(@Col1079, Col1079) ,ISNULL(@Col1080, Col1080) ,ISNULL(@Col1081, Col1081) ,ISNULL(@Col1082, Col1082) ,ISNULL(@Col1083, Col1083)
 ,ISNULL(@Col1084, Col1084) ,ISNULL(@Col1085, Col1085) ,ISNULL(@Col1086, Col1086) ,ISNULL(@Col1087, Col1087) ,ISNULL(@Col1088, Col1088) ,ISNULL(@Col1089, Col1089) ,ISNULL(@Col1090, Col1090) ,ISNULL(@Col1091, Col1091)
 ,ISNULL(@Col1092, Col1092) ,ISNULL(@Col1093, Col1093) ,ISNULL(@Col1094, Col1094) ,ISNULL(@Col1095, Col1095) ,ISNULL(@Col1096, Col1096) ,ISNULL(@Col1097, Col1097) ,ISNULL(@Col1098, Col1098) ,ISNULL(@Col1099, Col1099)
 ,ISNULL(@Col1100, Col1100) ,ISNULL(@Col1101, Col1101) ,ISNULL(@Col1102, Col1102) ,ISNULL(@Col1103, Col1103) ,ISNULL(@Col1104, Col1104) ,ISNULL(@Col1105, Col1105) ,ISNULL(@Col1106, Col1106) ,ISNULL(@Col1107, Col1107)
 ,ISNULL(@Col1108, Col1108) ,ISNULL(@Col1109, Col1109) ,ISNULL(@Col1110, Col1110) ,ISNULL(@Col1111, Col1111) ,ISNULL(@Col1112, Col1112) ,ISNULL(@Col1113, Col1113) ,ISNULL(@Col1114, Col1114) ,ISNULL(@Col1115, Col1115)
 ,ISNULL(@Col1116, Col1116) ,ISNULL(@Col1117, Col1117) ,ISNULL(@Col1118, Col1118) ,ISNULL(@Col1119, Col1119) ,ISNULL(@Col1120, Col1120) ,ISNULL(@Col1121, Col1121) ,ISNULL(@Col1122, Col1122) ,ISNULL(@Col1123, Col1123)
 ,ISNULL(@Col1124, Col1124) ,ISNULL(@Col1125, Col1125) ,ISNULL(@Col1126, Col1126) ,ISNULL(@Col1127, Col1127) ,ISNULL(@Col1128, Col1128) ,ISNULL(@Col1129, Col1129) ,ISNULL(@Col1130, Col1130) ,ISNULL(@Col1131, Col1131)
 ,ISNULL(@Col1132, Col1132) ,ISNULL(@Col1133, Col1133) ,ISNULL(@Col1134, Col1134) ,ISNULL(@Col1135, Col1135) ,ISNULL(@Col1136, Col1136) ,ISNULL(@Col1137, Col1137) ,ISNULL(@Col1138, Col1138) ,ISNULL(@Col1139, Col1139)
 ,ISNULL(@Col1140, Col1140) ,ISNULL(@Col1141, Col1141) ,ISNULL(@Col1142, Col1142) ,ISNULL(@Col1143, Col1143) ,ISNULL(@Col1144, Col1144) ,ISNULL(@Col1145, Col1145) ,ISNULL(@Col1146, Col1146) ,ISNULL(@Col1147, Col1147)
 ,ISNULL(@Col1148, Col1148) ,ISNULL(@Col1149, Col1149) ,ISNULL(@Col1150, Col1150) ,ISNULL(@Col1151, Col1151) ,ISNULL(@Col1152, Col1152) ,ISNULL(@Col1153, Col1153) ,ISNULL(@Col1154, Col1154) ,ISNULL(@Col1155, Col1155)
 ,ISNULL(@Col1156, Col1156) ,ISNULL(@Col1157, Col1157) ,ISNULL(@Col1158, Col1158) ,ISNULL(@Col1159, Col1159) ,ISNULL(@Col1160, Col1160) ,ISNULL(@Col1161, Col1161) ,ISNULL(@Col1162, Col1162) ,ISNULL(@Col1163, Col1163)
 ,ISNULL(@Col1164, Col1164) ,ISNULL(@Col1165, Col1165) ,ISNULL(@Col1166, Col1166) ,ISNULL(@Col1167, Col1167) ,ISNULL(@Col1168, Col1168) ,ISNULL(@Col1169, Col1169) ,ISNULL(@Col1170, Col1170) ,ISNULL(@Col1171, Col1171)
 ,ISNULL(@Col1172, Col1172) ,ISNULL(@Col1173, Col1173) ,ISNULL(@Col1174, Col1174) ,ISNULL(@Col1175, Col1175) ,ISNULL(@Col1176, Col1176) ,ISNULL(@Col1177, Col1177) ,ISNULL(@Col1178, Col1178) ,ISNULL(@Col1179, Col1179)
 ,ISNULL(@Col1180, Col1180) ,ISNULL(@Col1181, Col1181) ,ISNULL(@Col1182, Col1182) ,ISNULL(@Col1183, Col1183) ,ISNULL(@Col1184, Col1184) ,ISNULL(@Col1185, Col1185) ,ISNULL(@Col1186, Col1186) ,ISNULL(@Col1187, Col1187)
 ,ISNULL(@Col1188, Col1188) ,ISNULL(@Col1189, Col1189) ,ISNULL(@Col1190, Col1190) ,ISNULL(@Col1191, Col1191) ,ISNULL(@Col1192, Col1192) ,ISNULL(@Col1193, Col1193) ,ISNULL(@Col1194, Col1194) ,ISNULL(@Col1195, Col1195)
 ,ISNULL(@Col1196, Col1196) ,ISNULL(@Col1197, Col1197) ,ISNULL(@Col1198, Col1198) ,ISNULL(@Col1199, Col1199) ,ISNULL(@Col1200, Col1200) ,ISNULL(@Col1201, Col1201) ,ISNULL(@Col1202, Col1202) ,ISNULL(@Col1203, Col1203)
 ,ISNULL(@Col1204, Col1204) ,ISNULL(@Col1205, Col1205) ,ISNULL(@Col1206, Col1206) ,ISNULL(@Col1207, Col1207) ,ISNULL(@Col1208, Col1208) ,ISNULL(@Col1209, Col1209) ,ISNULL(@Col1210, Col1210) ,ISNULL(@Col1211, Col1211)
 ,ISNULL(@Col1212, Col1212) ,ISNULL(@Col1213, Col1213) ,ISNULL(@Col146, Col146) ,ISNULL(@Col147, Col147) ,ISNULL(@Col148, Col148)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

END

GO

----------------------------------------------------------------------------

RAISERROR ('-- update NSP_InsertFault with new columns', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @RuleName varchar(255)
    , @Context varchar(50)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
    DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
    DECLARE @faultCount int

    IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
            @faultMetaID = ID 
            , @faultMetaRecovery = RecoveryProcessPath
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
        , @faultLocationID = l.ID
        FROM dbo.ChannelValue
        LEFT JOIN dbo.Location l ON l.ID = (
            SELECT TOP 1 LocationID 
            FROM dbo.LocationArea 
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
         
         -- get current count of faults
        SELECT TOP 1 
            @faultCount = FaultCounter 
            FROM FaultCount fc 
            WHERE fc.UnitID = @faultUnitID 
            AND fc.FaultMetaID = @faultMetaID

        -- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
        IF @faultCount IS NULL
        BEGIN
            INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
            VALUES (@faultUnitID, @faultMetaID, 0)

            SET @faultCount = 0
        END
        
        SET @faultCount = @faultCount + 1   
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO dbo.[Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[RuleName]
            ,[IsDelayed]
            ,[CountSinceLastMaint]
            ,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,@RuleName
            ,ISNULL(@IsDelayed, 0)
            ,@faultCount
            ,RecoveryStatus = CASE
                WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
                ELSE NULL
            
            END

        -- increment the fault counter
        UPDATE dbo.[FaultCount]
        SET FaultCounter = @faultCount
        WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
    
    --insert Channel data
    INSERT INTO dbo.[FaultChannelValue]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148])
        SELECT
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148] 

    FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate

    -- returning faultID
    RETURN @faultID
    
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,157
           ,'update_spectrum_db_157.sql'
           ,'1.13.01')
GO