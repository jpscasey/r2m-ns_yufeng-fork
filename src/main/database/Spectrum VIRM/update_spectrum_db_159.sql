SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 159
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- Update NSP_InsertFaultMeta
----------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@ID IS NULL)
	BEGIN
	INSERT dbo.FaultMeta(
		Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
	)
	SELECT 
		Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID
	END
	ELSE
	BEGIN
	SET IDENTITY_INSERT FaultMeta ON

	INSERT dbo.FaultMeta(
		ID	
		, Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
	)
	SELECT 
		ID			= @ID
		, Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID
	
		SET IDENTITY_INSERT FaultMeta OFF
	END

	
SELECT SCOPE_IDENTITY() AS InsertedID


END



GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,159
           ,'update_spectrum_db_159.sql'
           ,null)
GO