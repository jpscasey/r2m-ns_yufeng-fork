SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 162
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- R2M-4945 - EventChannelValue data now shown in Event Details
----------------------------------------------------------------------------

RAISERROR ('-- update NSP_InsertFault with new columns', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @RuleName varchar(255)
    , @Context varchar(50)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
    DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
    DECLARE @faultCount int

    IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
            @faultMetaID = ID 
            , @faultMetaRecovery = RecoveryProcessPath
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
        , @faultLocationID = l.ID
        FROM dbo.ChannelValue
        LEFT JOIN dbo.Location l ON l.ID = (
            SELECT TOP 1 LocationID 
            FROM dbo.LocationArea 
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
         
         -- get current count of faults
        SELECT TOP 1 
            @faultCount = FaultCounter 
            FROM FaultCount fc 
            WHERE fc.UnitID = @faultUnitID 
            AND fc.FaultMetaID = @faultMetaID

        -- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
        IF @faultCount IS NULL
        BEGIN
            INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
            VALUES (@faultUnitID, @faultMetaID, 0)

            SET @faultCount = 0
        END
        
        SET @faultCount = @faultCount + 1   
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO dbo.[Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[RuleName]
            ,[IsDelayed]
            ,[CountSinceLastMaint]
            ,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,@RuleName
            ,ISNULL(@IsDelayed, 0)
            ,@faultCount
            ,RecoveryStatus = CASE
                WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
                ELSE NULL
            
            END

        -- increment the fault counter
        UPDATE dbo.[FaultCount]
        SET FaultCounter = @faultCount
        WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
    
    --insert Channel data
    INSERT INTO dbo.[FaultChannelValue]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148])
        SELECT
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148] 

    FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate

        DECLARE @StopDATE datetime2 = DATEADD(Day ,-1 ,@TimeCreate)

		INSERT INTO dbo.FaultEventChannelValue (ID ,FaultID,UnitID,ChannelID,Value,TimeStamp)
		SELECT ID, @faultID ,@FaultUnitID, ChannelID, Value,timestamp FROM dbo.EventChannelValue where 
		ID in (	SELECT MAX(ID) FROM dbo.EventChannelValue cv where UnitID = @FaultUnitID
						AND TimeStamp BETWEEN @StopDATE 
						AND @TimeCreate GROUP BY ChannelID)

    -- returning faultID
    RETURN @faultID
    
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,162
           ,'update_spectrum_db_162.sql'
           ,null)
GO