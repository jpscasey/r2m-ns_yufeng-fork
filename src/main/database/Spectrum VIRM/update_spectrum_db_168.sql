SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 168
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- Update NSP_InsertFaultMeta
----------------------------------------------------------------------------
RAISERROR ('-- Update NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultMeta AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
	, @FromDate datetime = NULL
	, @ToDate datetime = NULL
	, @ParentID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@ID IS NULL)
	BEGIN
	INSERT dbo.FaultMeta(
		Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
		, FromDate
		, ToDate
		, ParentID
	)
	SELECT 
		Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID
		, FromDate		= @FromDate
		, ToDate		= @ToDate
		, ParentID		= @ParentID
	END
	ELSE
	BEGIN
	SET IDENTITY_INSERT FaultMeta ON

	INSERT dbo.FaultMeta(
		ID	
		, Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
		, FromDate
		, ToDate
		, ParentID
	)
	SELECT 
		ID			= @ID
		, Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID
		, FromDate		= @FromDate
		, ToDate		= @ToDate
		, ParentID		= @ParentID
	
		SET IDENTITY_INSERT FaultMeta OFF
	END

	
SELECT SCOPE_IDENTITY() AS InsertedID


END

GO

----------------------------------------------------------------------------
-- Update NSP_UpdateFaultMeta
----------------------------------------------------------------------------
RAISERROR ('-- Update NSP_UpdateFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_UpdateFaultMeta AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta]( --temporary, might have to add date checks here for rules engine
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
	, @FromDate datetime = NULL
	, @ToDate datetime = NULL
)
AS
	BEGIN
		
		SET NOCOUNT ON;

		BEGIN TRAN
		BEGIN TRY

			UPDATE dbo.FaultMeta SET
				Username		= @Username
				, FaultCode	 = @FaultCode
				, Description	= @Description 
				, Summary		= @Summary
				, AdditionalInfo = @AdditionalInfo
				, Url			= @Url
				, CategoryID	= @CategoryID
				, HasRecovery	= @HasRecovery
				, RecoveryProcessPath	= @RecoveryProcessPath
				, FaultTypeID	= @FaultTypeID
				, GroupID		= @GroupID
				, FromDate		= @FromDate
				, ToDate		= @ToDate
			WHERE ID = @ID

			COMMIT TRAN
		
		END TRY
		BEGIN CATCH

			EXEC dbo.NSP_RethrowError;
			ROLLBACK TRAN;

		END CATCH

	END
	
	GO

	
------------------------------------------------------------
-- INSERT into SchemaChangeLog
------------------------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,168
           ,'update_spectrum_db_168.sql'
           ,null)
GO