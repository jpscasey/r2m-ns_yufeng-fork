:setvar scriptNumber 001

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------



INSERT INTO dbo.HardwareType (ID, TypeName)
VALUES (1, 'OTMR1')

INSERT INTO Fleet (Name, Code) VALUES ('VIRM1', 'VIRM1')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM2', 'VIRM2')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM3', 'VIRM3')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM4', 'VIRM4')
INSERT INTO Fleet (Name, Code) VALUES ('HSTNG', 'HSTNG')


INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('DefaultGroup','DefaultGroup',1)
INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('BrakeGroup','BrakeGroup',1)
INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('PressureGroup','PressureGroup',1)


INSERT INTO dbo.ChannelType ( Name ) 
VALUES ('Digital') ,('Analog'),('Binary'), ('Hexadecimal'), ('Calculated')

/*Channels*/
/*TODO*/

INSERT INTO dbo.FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo)
VALUES('','',GETDATE(),NULL)

INSERT INTO [dbo].[FleetStatus] ([ValidFrom],[ValidTo],[Setcode],[Diagram],[Headcode],[IsValid],[Loading],[ConfigDate],[UnitList],[LocationIDHeadcodeStart],[LocationIDHeadcodeEnd],[UnitID],[UnitPosition],[UnitOrientation],[FleetFormationID])     
SELECT GETDATE(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,ID,1,1,1 FROM Unit



--NCU
SET IDENTITY_INSERT [dbo].[NcuModel] ON 

GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (1, N'NCU3097')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (2, N'NCU3096')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (3, N'NCU3095')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (4, N'NCU3141')
GO
SET IDENTITY_INSERT [dbo].[NcuModel] OFF
GO
SET IDENTITY_INSERT [dbo].[NcuType] ON 

GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (1, N'3097_GA_321_DTC')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (2, N'3095_GA_321_PMS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (3, N'3096_GA_321_ATS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (4, N'3097_GA_321_DTS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (5, N'3097_GA_317_DTOSB')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (6, N'3096_GA_317_TOC')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (7, N'3095_GA_317_PMOS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (8, N'3097_GA_317_DTOSA')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (9, N'3141_VR_SR2')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (10, N'3141_VR_SR2_B')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (11, N'3141_VR_SR2_C')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (12, N'3141_VR_SR2_D')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (13, N'3141_VR_SR2_E')
GO
SET IDENTITY_INSERT [dbo].[NcuType] OFF
GO
SET IDENTITY_INSERT [dbo].[Ncu] ON 

GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (1, 4, 9, N'P47200000 ', N'')
GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (2, 4, 9, N'P47200001 ', NULL)
GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (40, 4, 12, N'P47200002 ', NULL)
GO
SET IDENTITY_INSERT [dbo].[Ncu] OFF
GO



--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,$(scriptNumber)
           ,'update_spectrum_db_load_$(scriptNumber).sql'
           ,'1.0.01')
GO