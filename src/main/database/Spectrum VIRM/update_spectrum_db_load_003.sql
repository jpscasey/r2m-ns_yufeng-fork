SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 003
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
-- Bug 26934 - SB 2.12 - Add tables ChannelVehicleType, RelatedChannel, VehicleType and modify Channel and VW_ChannelDefinition
------------------------------------------------------------
--------------------------------------------
-- CREATE VehicleType Table
--------------------------------------------
RAISERROR ('--Create VehicleType Table', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VehicleType' AND TABLE_SCHEMA = 'dbo') BEGIN

CREATE TABLE VehicleType(
    ID INT IDENTITY(1,1) NOT NULL
    ,Name varchar(50) COLLATE Latin1_General_CI_AS NOT NULL 
    ,CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED 
    (
        ID ASC
    )
)
END
GO

--------------------------------------------
-- CREATE ChannelVehicleType Table
--------------------------------------------
RAISERROR ('--Create ChannelVehicleType Table', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ChannelVehicleType' AND TABLE_SCHEMA = 'dbo') BEGIN
CREATE TABLE ChannelVehicleType(
    VehicleTypeID INT NOT NULL
    ,ChannelID INT NOT NULL
    CONSTRAINT [PK_ChannelVehicleType] PRIMARY KEY CLUSTERED  
    (
        VehicleTypeID,ChannelID ASC
    )
)
END
GO
ALTER TABLE [dbo].[ChannelVehicleType]  WITH CHECK ADD  CONSTRAINT [FK_VehicleType_ChannelVehicleType] FOREIGN KEY(VehicleTypeID)
REFERENCES [dbo].[VehicleType] (ID)
GO

ALTER TABLE [dbo].[ChannelVehicleType] CHECK CONSTRAINT [FK_VehicleType_ChannelVehicleType]
GO
ALTER TABLE [dbo].[ChannelVehicleType]  WITH CHECK ADD  CONSTRAINT [FK_Channel_ChannelVehicleType] FOREIGN KEY(ChannelID)
REFERENCES [dbo].[Channel] ([ID])
GO

ALTER TABLE [dbo].[ChannelVehicleType] CHECK CONSTRAINT [FK_Channel_ChannelVehicleType]
GO

--------------------------------------------
-- CREATE RelatedChannel Table
--------------------------------------------
RAISERROR ('--Create RelatedChannel Table', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'RelatedChannel' AND TABLE_SCHEMA = 'dbo') BEGIN
CREATE TABLE RelatedChannel(
    ID INT IDENTITY(1,1) NOT NULL
    , Name varchar(100) COLLATE Latin1_General_CI_AS NOT NULL
    CONSTRAINT [PK_RelatedChannel] PRIMARY KEY CLUSTERED  
    (
        ID ASC
    )
)
END
GO

--------------------------------------------
-- Add VehicleType_id to Vehicle
--------------------------------------------

RAISERROR ('--Add VehicleTypeID to Vehicle', 0, 1) WITH NOWAIT
GO
ALTER TABLE Vehicle ADD VehicleTypeID int NULL

ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_VehicleType_Vehicle] FOREIGN KEY(VehicleTypeID)
REFERENCES [dbo].[VehicleType] (ID)
GO

ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_VehicleType_Vehicle]
GO

--------------------------------------------
-- Add RelatedChannel_id to Channel
--------------------------------------------
RAISERROR ('--Add RelatedChannelID to Channel', 0, 1) WITH NOWAIT
GO
ALTER TABLE Channel ADD RelatedChannelID int NULL

ALTER TABLE [dbo].[Channel]  WITH CHECK ADD  CONSTRAINT [FK_RelatedChannel_Channel] FOREIGN KEY(RelatedChannelID)
REFERENCES [dbo].[RelatedChannel] (ID)
GO

ALTER TABLE [dbo].[Channel] CHECK CONSTRAINT [FK_RelatedChannel_Channel]
GO

--------------------------------------------
-- ALTER VIEW VW_ChannelDefinition
--------------------------------------------
RAISERROR ('-- view VW_ChannelDefinition', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'VW_ChannelDefinition')
    EXEC ('CREATE VIEW dbo.VW_ChannelDefinition AS Select 1 as Temp;')
GO

ALTER VIEW [dbo].[VW_ChannelDefinition]
AS
    SELECT     
        c.ID
        , c.VehicleID
        , c.EngColumnNo AS ColumnID
        , c.ChannelGroupID
        , c.Header AS Name
        , c.Name AS Description
        , c.ChannelGroupOrder
        , ct.Name AS Type
        , c.UOM
        , c.HardwareType
        , c.StorageMethod
        , c.Ref
        , c.MinValue
        , c.MaxValue
        , c.Comment
        , c.VisibleOnFaultOnly
        , c.IsAlwaysDisplayed
        , c.IsLookup
        , c.DefaultValue
        , c.IsDisplayedAsDifference
        , rc.Name AS RelatedChannelName
        , rc.ID AS RelatedChannelID
    FROM dbo.Channel AS c 
    INNER JOIN dbo.ChannelType AS ct ON c.TypeID = ct.ID
    LEFT JOIN RelatedChannel rc ON c.RelatedChannelID = rc.ID
GO

--------------------------------------------
-- ALTER PROCEDURE NSP_SearchChannelDefinition
--------------------------------------------
RAISERROR ('-- UPDATE NSP_SearchChannelDefinition', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
EXEC ('CREATE PROCEDURE dbo.NSP_SearchChannelDefinition AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           [NSP_SearchChannelDefinition]
**  Description:    Returns list channel definition for a keyword
**  Call frequency: Called from Channel Definition module
**  Parameters:     @ChannelId int
**                  @Keyword varchar(50)
**  Return values:  
*******************************************************************************/
        
ALTER PROCEDURE [dbo].[NSP_SearchChannelDefinition]
    @ChannelId int = NULL,
    @Keyword varchar(50) = NULL
AS
BEGIN
    IF @ChannelId IS NULL
    BEGIN
        IF @Keyword IS NULL OR LEN(@Keyword) = 0
            SET @Keyword = '%'
        ELSE
            SET @Keyword = '%' + @Keyword + '%'
    END

    SELECT
            vcd.ID
            , vcd.VehicleID
            , vcd.ColumnID
            , vcd.ChannelGroupID
            , vcd.Name
            , vcd.Description
            , vcd.ChannelGroupOrder
            , vcd.Type
            , vcd.UOM
            , vcd.HardwareType
            , vcd.StorageMethod
            , vcd.Ref
            , vcd.MinValue
            , vcd.MaxValue
            , vcd.Comment
            , vcd.VisibleOnFaultOnly
            , vcd.IsAlwaysDisplayed
            , vcd.IsLookup
            , vcd.DefaultValue
            , vcd.IsDisplayedAsDifference
            , vcd.RelatedChannelID
            , vcd.RelatedChannelName
    FROM VW_ChannelDefinition as vcd
    INNER JOIN ChannelGroup as cgd ON vcd.ChannelGroupID = cgd.ID
    WHERE
        (@ChannelId IS NULL
            AND (vcd.ID LIKE @Keyword
                OR cgd.Notes LIKE @Keyword
                OR vcd.Description LIKE @Keyword
                OR vcd.Name LIKE @Keyword
                OR vcd.Ref LIKE @Keyword
                OR vcd.Type LIKE @Keyword
                OR vcd.UOM LIKE @Keyword))
        OR
        (@ChannelId IS NOT NULL AND vcd.ID = @ChannelId)
    ORDER BY vcd.ID
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,003
           ,'update_spectrum_db_load_003.sql'
           ,'1.0.01')
GO