SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 007
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--DROP TABLE #FleetFormation
IF OBJECT_ID('tempdb..#FleetFormation') IS NOT NULL
    DROP TABLE #FleetFormation

CREATE TABLE #FleetFormation(
	[ID] INT IDENTITY(1,1),
	[UnitIDList] [varchar](100) NOT NULL,
	[UnitNumberList] [varchar](100) NOT NULL,
	[ValidFrom] [datetime2](3) NOT NULL,
	[ValidTo] [datetime2](3) NULL)

PRINT 'Inserting rows into table: FleetFormation'

INSERT INTO #FleetFormation ([UnitIDList], [UnitNumberList], [ValidFrom], [ValidTo]) 
SELECT CAST(id as varchar(5)) ,UnitNumber,SYSDATETIME(),NULL  FROM Unit

MERGE dbo.[FleetFormation] AS [target]
USING (SELECT * FROM [#FleetFormation] ff) AS [source]
ON ([target].ID = [source].ID)
WHEN MATCHED THEN
    UPDATE SET [target].[UnitIDList] = [source].[UnitIDList]
			 , [target].[UnitNumberList] = [source].[UnitNumberList]
			 , [target].[ValidFrom] = [source].[ValidFrom]
			 , [target].[ValidTo] = [source].[ValidTo]
WHEN NOT MATCHED THEN 
    INSERT ([UnitIDList], [UnitNumberList], [ValidFrom], [ValidTo])
    VALUES ([source].[UnitIDList], [source].[UnitNumberList], [source].[ValidFrom], [source].[ValidTo]);

UPDATE [dbo].[FleetStatus]
SET [FleetFormationID] = [UnitID]

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,007
           ,'update_spectrum_db_load_007.sql'
           ,'1.0.01')
GO