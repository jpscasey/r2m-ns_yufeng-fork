SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 019
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- Load Locations - bridges', 0, 1) WITH NOWAIT
GO

--select * from dbo.Location -- update location set locationCode = upper (locationCode) 
INSERT dbo.Location (LocationCode, LocationName, Tiploc, Lat, Lng, Type)
SELECT upper(code), name , upper(code), latitude, longitude, 'B' as Type --Bridge
FROM (
	SELECT 'Grs' code,'Greuns, brug o/d - bij Leeuwarden' name ,5.83 longitude,53.200634 latitude UNION
	SELECT 'Rskbl','Rijn-Schiekanaalbrug Leiden',4.499167,52.144535 UNION
	SELECT 'Hrmk','Harinxmakanaal, brug o/h van - tussen Leeuwarden en Grouw=Irnsum',5.773942,53.185281 UNION
	SELECT 'Sgbr','Singelgrachtbrug',4.883004,52.385988 UNION
	SELECT 'Skbr','Schinkelbrug - bij Amsterdam Zuid',4.847141,52.337697 UNION
	SELECT 'Spbr','Spaarnebrug - bij Haarlem',4.646421,52.385538 UNION
	SELECT 'Nnvbr','Nauernasche vaartbrug - bij Krommenie=Assendelft',4.77263,52.49323 UNION
	SELECT 'Wdvb','Wildervankkanaalbrug',6.893787,53.160893 UNION
	SELECT 'Zdb','Zaanbrug',4.812838,52.455139 UNION
	SELECT 'Cosb','Coevorder Stadsgracht',6.737005,52.655264 UNION
	SELECT 'Gwt','Galgewater, brug o/h - bij Leiden',4.477576,52.160231 UNION
	SELECT 'Vlk','Vlakebrug o/h Kanaal door Zuid-Beveland',4.006963,51.469468 UNION
	SELECT 'Hvvb','Hoogeveense Vaartbrug',6.847869,52.716639 UNION
	SELECT 'Hdp','Hoendiep, brug o/h - bij Groningen',6.489465,53.212952 UNION
	SELECT 'Smvrt','Smildervaart, brug o/d',6.198308,52.722469 UNION
	SELECT 'Rdp','Reitdiep, brug o/h - bij Groningen',6.542353,53.220954 UNION
	SELECT 'Hrm','Harinxmakanaal, brug o/h van - tussen Leeuwarden en Dronrijp/Mantgum',5.760026,53.191723 UNION
	SELECT 'Dhs','Delfshavense Schie, brug o/d - bij Schiedam',4.437209,51.922127 UNION
	SELECT 'Wmb','Wijmertsbrug - tussen IJlst en Workum',5.606639,53.010027 UNION
	SELECT 'Wwab','Westerwoldse Aa-brug',7.203418,53.185647 UNION
	SELECT 'Dwb','Dubbele Wierickebrug - tussen Woerden en Gouda',4.809498,52.074377 UNION
	SELECT 'Wijb','Wantijbrug - bij Dordrecht',4.735273,51.805416 UNION
	SELECT 'Hlg','Harlingen',5.422193,53.170319 UNION
	SELECT 'Kr','Klifrak, brug o/h - bij Workum',5.465375,52.97881 UNION
	SELECT 'Kgs','Koegras, brug o/h Noordhollands kanaal',4.787662,52.902427 UNION
	SELECT 'Vtbr','Vechtbrug - bij Weesp',5.047862,52.30987 UNION
	SELECT 'Brdl','Brug o/h Deel - tussen Akkrum en Heerenveen',5.858073,53.017235 UNION
	SELECT 'Botbr','Botlekbrug',4.330255,51.871648 UNION
	SELECT 'Btd','Boterdiep',6.60066,53.308526 UNION
	SELECT 'Bol','Brug o/h Kanaal Alkmaar-Kolhorn - bij Broek op Langedijk',4.800677,52.66009 UNION
	SELECT 'IJbz','IJsselbrug Zutphen',6.187882,52.143315 UNION
	SELECT 'Mabr','Markbrug - tussen Zevenbergen en Oudenbosch',4.593254,51.624372 UNION
	SELECT 'Gwb','Gouwebrug - bij Alphen a/d Rijn',4.672419,52.114506 UNION
	SELECT 'Vkbr','Vinkbrug o/d Rijn - bij Leiden',4.462315,52.151833 UNION
	SELECT 'Ods','Oosterdoksdoorvaart',4.910731,52.376862 UNION
	SELECT 'Hgwbr','Hoge Gouwebrug - bij Gouda',4.682624,52.017553 UNION
	SELECT 'Oij','Oude IJssel, brug o/d - bij Doetinchem',6.28731,51.958655 UNION
	SELECT 'Bobr','Boornbrug - bij Akkrum',5.841538,53.050805 UNION
	SELECT 'Pmk','Prinses Margrietkanaal, brug o/h - bij Grouw=Irnsum',5.827755,53.078766 UNION
	SELECT 'Mtbr','Maas brug b/o bij Maastricht',5.696831,50.858716 UNION
	SELECT 'Slua','Sluiskil aansl.',3.836403,51.293819 UNION
	SELECT 'Grbr','Grote Draaibrug o/d Oude Maas - bij Dordrecht',4.651874,51.8109 UNION
	SELECT 'Vdgbr','Vlaardingenbrug',4.34701,51.903735 UNION
	SELECT 'Nhkbr','Noordhollands kanaal, brug o/h - bij Alkmaar',4.746311,52.638629 UNION
	SELECT 'Nhk','Noordhollands kanaal, brug o/h - bij Purmerend',4.946302,52.499242 UNION
	SELECT 'Gn','Groningen',6.55929,53.210577 UNION
	SELECT 'Shb','Suurhoffbrug o/h Hartelkanaal',4.106444,51.937345 UNION
	SELECT 'Hvbr','Havenbrug - bij Maassluis',4.249126,51.918387 UNION
	SELECT 'Abr','Brug o/d Arne',3.65017,51.499806 UNION
	SELECT 'Whe','Where, brug o/d - bij Purmerend',4.957209,52.507163 UNION
	SELECT 'Rvbr','Ringvaartbrug - bij Sassenheim',4.553083,52.223447 UNION
	SELECT 'Rsl','Rensel, brug o/d - tussen Winschoten en Nieuweschans',7.072317,53.145038 UNION
	SELECT 'Mkbr','Merwedekanaalbrug - bij Arkel',4.996298,51.873138 UNION
	SELECT 'Clb','Calandbrug',4.227479,51.901431 UNION
	SELECT 'Bmbr','Baanhoekbrug o/d Beneden-Merwede - bij Sliedrecht',4.741745,51.822562 UNION
	SELECT 'Lgwbr','Lage Gouwebrug - bij Gouda',4.682031,52.018947
) source

LEFT OUTER JOIN dbo.location l on source.code = l.LocationCode
WHERE l.LocationCode is null

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,019
           ,'update_spectrum_db_load_019.sql'
           ,'1.1.02')
GO
