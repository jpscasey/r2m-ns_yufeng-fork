SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 038
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- Update - NS-573
---------------------

RAISERROR ('-- Fixing encoding on some strings with accents', 0, 1) WITH NOWAIT
GO

UPDATE [dbo].[Location] SET LocationName = 'Amsterdam werkplaats' WHERE LocationCode LIKE 'Aswp';
UPDATE [dbo].[Location] SET LocationName = 'OB-Leidschendam' WHERE LocationCode LIKE 'OB-Ldd';
UPDATE [dbo].[Location] SET LocationName = 'Tilburg  <> ’s Hertogenbosch' WHERE LocationCode LIKE 'Tb  Ht';
UPDATE [dbo].[Location] SET LocationName = 'Helmond Brandevoort  <> Helmond ’t Hout' WHERE LocationCode LIKE 'Hmbv  Hmh';
UPDATE [dbo].[Location] SET LocationName = 'Helmond ’t Hout  <> Helmond' WHERE LocationCode LIKE 'Hmh  Hm';
UPDATE [dbo].[Location] SET LocationName = '’s Hertogenbosch <> Vught' WHERE LocationCode LIKE 'Ht  Vg';
UPDATE [dbo].[Location] SET LocationName = 'Zaltbommel  <> ’s Hertogenbosch' WHERE LocationCode LIKE 'Zbm  Ht';
UPDATE [dbo].[Location] SET LocationName = '’s Hertogenbosch  <> ’s Hertogenbosch Oost' WHERE LocationCode LIKE 'Ht  Hto';
UPDATE [dbo].[Location] SET LocationName = '’s Hertogenbosch Oost  <> Rosmalen' WHERE LocationCode LIKE 'Hto  Rs';
UPDATE [dbo].[Location] SET LocationName = 'Wormerveer  <> Koog Zaandijk' WHERE LocationCode LIKE 'Wm  Kzd';
UPDATE [dbo].[Location] SET LocationName = 'Zaanstraat' WHERE LocationCode LIKE 'Znst';
UPDATE [dbo].[Location] SET LocationName = 'Mariënberg  <> Vroomshoop' WHERE LocationCode LIKE 'Mrb  Vhp';
UPDATE [dbo].[Location] SET LocationName = 'Ommen  <> Mariënberg' WHERE LocationCode LIKE 'Omn  Mrb';
UPDATE [dbo].[Location] SET LocationName = 'Mariënberg  <> Hardenberg' WHERE LocationCode LIKE 'Mrb  Hdb';
UPDATE [dbo].[Location] SET LocationName = 'Nunspeet  <> ’t Harde' WHERE LocationCode LIKE 'Ns  Hde';
UPDATE [dbo].[Location] SET LocationName = 'Feanwâlden  <> De Westereen' WHERE LocationCode LIKE 'Fwd  Dwe';
UPDATE [dbo].[Location] SET LocationName = 'Hurdegaryp  <> Feanwâlden' WHERE LocationCode LIKE 'Hdg  Fwd';
UPDATE [dbo].[Location] SET LocationName = 'Hogesnelheidslijn' WHERE LocationCode LIKE 'Hsl';
UPDATE [dbo].[Location] SET LocationName = '’t Harde  <> Wezep' WHERE LocationCode LIKE 'Hde  Wz';

INSERT INTO [dbo].[LocationArea](LocationID, MinLatitude, MaxLatitude, MinLongitude, MaxLongitude, Priority, Type) VALUES ((SELECT ID FROM Location WHERE LocationCode = 'Kbw  Zd'), 52.450735, 52.457051, 4.805175, 4.80694, 100, 'C')
INSERT INTO [dbo].[LocationArea](LocationID, MinLatitude, MaxLatitude, MinLongitude, MaxLongitude, Priority, Type) VALUES ((SELECT ID FROM Location WHERE LocationCode = 'Kzd  Kbw'), 52.460385, 52.467739, 4.805066, 4.805948, 100, 'C')

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,038
           ,'update_spectrum_db_load_038.sql'
           ,'1.8.01')
GO
