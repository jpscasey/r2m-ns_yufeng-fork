SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 046
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update - NS-654
--------------------------------------------

RAISERROR ('-- Add unit and fleet', 0, 1) WITH NOWAIT
GO

DELETE FROM UNIT
DELETE FROM Fleet


DELETE FROM Fleet
INSERT INTO Fleet (Name, Code) VALUES ('VIRM', 'VIRM')

INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8608', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8614', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8615', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8621', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8624', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8628', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8629', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8632', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8633', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8635', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8636', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8637', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8638', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8639', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8640', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8641', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8642', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8644', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8645', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8646', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8647', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8648', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8649', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8651', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8652', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8653', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8654', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8655', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8656', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8657', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8658', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8659', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8660', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8661', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8662', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8663', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8664', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8665', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8666', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8667', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8668', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8670', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8671', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8672', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8674', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8675', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8676', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9401', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9402', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9403', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9404', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9405', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9406', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9407', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9409', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9410', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9411', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9412', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9413', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9416', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9417', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9418', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9419', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9420', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9422', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9423', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9425', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9426', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9427', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9430', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9431', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9434', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9443', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9450', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9469', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9473', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9477', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9478', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9479', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9480', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9481', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8701', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8703', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8705', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8707', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8709', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8711', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8713', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8715', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8717', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8719', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8721', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8723', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9502', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9504', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9506', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9508', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9510', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9512', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9514', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9516', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9518', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9520', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9522', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9524', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9525', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8726', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8727', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8728', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8729', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8730', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8731', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8732', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8733', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8734', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8735', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8736', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8737', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8738', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8739', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8740', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8741', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8742', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8743', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8744', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8745', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8746', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9547', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9548', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9549', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9550', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9551', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9552', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9553', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9554', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9555', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9556', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9557', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9558', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9559', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9560', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9561', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9562', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9563', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9564', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9565', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9566', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9567', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9568', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9569', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9570', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9571', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9572', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9573', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9574', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9575', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9576', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9577', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9578', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9579', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9580', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9581', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9582', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9583', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9584', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9585', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9586', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9587', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9588', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9589', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9590', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9591', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9592', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9593', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9594', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9595', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9596', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9597', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM'))

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,046
           ,'update_spectrum_db_load_046.sql'
           ,'1.9.01')
GO