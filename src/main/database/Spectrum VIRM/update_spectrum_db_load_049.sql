SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 049
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update - NS-705 - populate VIRM db RuleEngineCurrentId table
--------------------------------------------

RAISERROR ('-- populate dbo.RuleEngineCurrentId with VIRM fleet configurations', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM RuleEngineCurrentId WHERE ConfigurationName = 'VIRM' AND Position = 1)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('VIRM', 1, (SELECT top 1 Value FROM RuleEngineCurrentId), GETDATE())
	
IF NOT EXISTS (SELECT * FROM RuleEngineCurrentId WHERE ConfigurationName = 'VIRM' AND Position = 2)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('VIRM', 2, (SELECT top 1 Value FROM RuleEngineCurrentId), GETDATE())

IF NOT EXISTS (SELECT * FROM RuleEngineCurrentId WHERE ConfigurationName = 'VIRMFault' AND Position = 1)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('VIRMFault', 1, null, GETDATE())
	
IF NOT EXISTS (SELECT * FROM RuleEngineCurrentId WHERE ConfigurationName = 'VIRMFault' AND Position = 2)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('VIRMFault', 2, null, GETDATE())

DELETE FROM RuleEngineCurrentId WHERE ConfigurationName = 'SLT'

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,049
           ,'update_spectrum_db_load_049.sql'
           ,'1.9.01')
GO