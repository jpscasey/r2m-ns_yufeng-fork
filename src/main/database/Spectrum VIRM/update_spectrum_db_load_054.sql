SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 054
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- Update - NS-759
---------------------

BEGIN TRAN 

DELETE FROM locationarea 

DELETE FROM location 

SET IDENTITY_INSERT location ON 

INSERT INTO location 
            (id, 
             locationcode, 
             locationname, 
             tiploc, 
             lat, 
             lng, 
             type) 
SELECT id, 
       locationcode, 
       locationname, 
       tiploc, 
       lat, 
       lng, 
       type 
FROM   [$(db_name_slt)].[dbo].[Location]

SET IDENTITY_INSERT location OFF 
SET IDENTITY_INSERT locationarea ON 

INSERT INTO locationarea 
            (id, 
             locationid, 
             minlatitude, 
             maxlatitude, 
             minlongitude, 
             maxlongitude, 
             priority, 
             type) 
SELECT id, 
       locationid, 
       minlatitude, 
       maxlatitude, 
       minlongitude, 
       maxlongitude, 
       priority, 
       type 
FROM   [$(db_name_slt)].[dbo].[LocationArea]

SET IDENTITY_INSERT location OFF 

COMMIT 

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,054
           ,'update_spectrum_db_load_054.sql'
           ,'1.10.01')
GO