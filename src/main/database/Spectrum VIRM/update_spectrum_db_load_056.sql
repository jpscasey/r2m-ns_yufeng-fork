SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 056
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

----------------------------------------------------
-- NS-753 Channels added for viewing in static chart
----------------------------------------------------

-- Table created to emulate AGA structure for static charts

CREATE TABLE [dbo].[ChartVehicleType] (
	ChartId int,
	VehicleType varchar(255)
);

DELETE FROM [dbo].[ChartChannel] WHERE ChartID < 5;
DELETE FROM [dbo].[Chart] WHERE ID < 5;
DELETE FROM [dbo].[ChartVehicleType] WHERE ChartId < 5;

-- Same channels inserted for VIRM-IV and VIRM-VI unit sizes

SET IDENTITY_INSERT [dbo].[Chart] ON;

INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (1, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (2, NULL, 'Quick brake', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (3, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (4, NULL, 'Quick brake', 1, 10, NULL);

SET IDENTITY_INSERT [dbo].[Chart] OFF;

INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (1, 'VIRM-IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (2, 'VIRM-IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (3, 'VIRM-VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (4, 'VIRM-VI');

--------------------------  VIRM Phase 1 Channels  -------------------------------------------

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (1, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (1, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (1, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (1, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (2, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (2, 111, 1.00, 6);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (3, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (3, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (3, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (3, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (4, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (4, 111, 1.00, 6);

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,056
           ,'update_spectrum_db_load_056.sql'
           ,'1.11.01')
GO