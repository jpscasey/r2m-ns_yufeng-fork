SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 057
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

----------------------------------------------------
-- NS-844 Channels added for viewing in static chart
----------------------------------------------------

-- Delete all records of the Unit 8744

RAISERROR ('-- Remove Unit 8744 from [dbo].[EventChannelLatestValue]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[EventChannelLatestValue] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[FaultEventChannelValue]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[FaultEventChannelValue] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[ServiceRequest]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[ServiceRequest] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[ChannelValue]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[ChannelValue] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[EventChannelValue]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[EventChannelValue] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[Fault]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[Fault] WHERE FaultUnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[FaultChannelValue]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[FaultChannelValue] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[FleetStatusStaging]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[FleetStatusStaging] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[Vehicle]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[Vehicle] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[WorkOrder]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[WorkOrder] WHERE UnitID = (SELECT ID FROM Unit WHERE UnitNumber = 8744)

RAISERROR ('-- Remove Unit 8744 from [dbo].[Unit]', 0, 1) WITH NOWAIT
GO
DELETE FROM [dbo].[Unit] WHERE UnitNumber = 8744

-- insert new Unit 8610 in database

RAISERROR ('-- Insert new Unit 8610 into [dbo].[Unit] ', 0, 1) WITH NOWAIT
GO

INSERT INTO [dbo].[Unit]
           ([UnitNumber]
           ,[UnitType]
           ,[IsValid]
           ,[FleetID]
           ,[UnitStatus]
           ,[UnitMaintLoc]
           ,[LastMaintDate])
     VALUES
           (8610
           ,'VIRM-VI'
           ,1
           ,6
           ,NULL
           ,NULL
           ,NULL)
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,057
           ,'update_spectrum_db_load_057.sql'
           ,'1.11.01')
GO