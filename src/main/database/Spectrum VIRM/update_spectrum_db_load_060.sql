SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 060
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------------------------------
-- NS-820 Updating Sub Series for VIRM fleet
--------------------------------------------------------------------

-- Chart, ChartChannel & ChartVehicleType tables restructured due to updating UnitType names

DELETE FROM [dbo].[ChartChannel] WHERE ChartID > 4 AND ChartID < 15;
DELETE FROM [dbo].[Chart] WHERE ID > 4 AND ID < 15;
DELETE FROM [dbo].[ChartVehicleType];


SET IDENTITY_INSERT [dbo].[Chart] ON;

INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (5, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (6, NULL, 'Quick brake', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (7, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (8, NULL, 'Quick brake', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (9, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (10, NULL, 'Quick brake', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (11, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (12, NULL, 'Quick brake', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (13, NULL, 'Speeds', 1, 10, NULL);
INSERT INTO [dbo].[Chart] (ID, UserID, Name, IsSystem, Ticks, UserName) VALUES (14, NULL, 'Quick brake', 1, 10, NULL);

SET IDENTITY_INSERT [dbo].[Chart] OFF;


INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (1, 'VIRM1 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (2, 'VIRM1 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (3, 'VIRM-1M IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (4, 'VIRM-1M IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (5, 'VIRM2/3 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (6, 'VIRM2/3 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (7, 'VIRM4 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (8, 'VIRM4 IV');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (9, 'VIRM1 VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (10, 'VIRM1 VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (11, 'VIRM-1M VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (12, 'VIRM-1M VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (13, 'VIRM2/3 VI');
INSERT INTO [dbo].[ChartVehicleType] (ChartId, VehicleType) VALUES (14, 'VIRM2/3 VI');


INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (5, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (5, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (5, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (5, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (6, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (6, 111, 1.00, 6);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (7, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (7, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (7, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (7, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (8, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (8, 111, 1.00, 6);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (9, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (9, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (9, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (9, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (10, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (10, 111, 1.00, 6);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (11, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (11, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (11, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (11, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (12, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (12, 111, 1.00, 6);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (13, 101, 1.00, 1);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (13, 102, 1.00, 2);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (13, 143, 1.00, 4);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (13, 144, 1.00, 5);

INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (14, 66, 1.00, 3);
INSERT INTO [dbo].[ChartChannel] (ChartID, ChannelID, Scale, Sequence) VALUES (14, 111, 1.00, 6);



-- VIRM-1M IV

UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber > 9400 AND UnitNumber < 9408;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE unitNumber = 9409;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber > 9410 AND UnitNumber < 9414;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber > 9415 AND UnitNumber < 9419;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE unitNumber = 9420;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE unitNumber = 9469;

-- VIRM1 IV

UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9419;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9422;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9423;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber > 9424 AND UnitNumber < 9428;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9430;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9431;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9434;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9443;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9450;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber = 9473;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 IV' WHERE UnitNumber > 9476 AND UnitNumber < 9482;

-- VIRM2/3 IV

UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9504;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9506;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9508;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9510;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9512;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9514;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9516;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9518;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9520;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9522;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9524;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 IV' WHERE UnitNumber = 9525;

-- VIRM4 IV

UPDATE [dbo].[Unit] SET UnitType = 'VIRM4 IV' WHERE UnitNumber > 9546 AND UnitNumber < 9598;

-- VIRM1 VI

UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8608;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8614;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8615;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8621;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8624;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8628;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8629;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8632;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8633;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8635;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber = 8636;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber > 8637 AND UnitNumber < 8643;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber > 8643 AND UnitNumber < 8650;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber > 8650 AND UnitNumber < 8668;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber > 8669 AND UnitNumber < 8673;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM1 VI' WHERE UnitNumber > 8673 AND UnitNumber < 8677;

-- VIRM-1M VI

UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = 8637;

-- VIRM2/3 VI

UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8701;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8703;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8705;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8707;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8709;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8711;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8713;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8715;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8717;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8719;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8721;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber = 8723;
UPDATE [dbo].[Unit] SET UnitType = 'VIRM2/3 VI' WHERE UnitNumber > 8725 AND UnitNumber < 8747;

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,060
           ,'update_spectrum_db_load_060.sql'
           ,'1.11.02')
GO