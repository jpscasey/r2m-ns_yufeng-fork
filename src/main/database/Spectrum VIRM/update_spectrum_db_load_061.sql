SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 061
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------------------------------
-- NS-865 Matching database unit list with that provided by NS
--------------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8608') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8608', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8610') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8610', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8614') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8614', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8615') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8615', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8621') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8621', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8624') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8624', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8628') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8628', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8629') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8629', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8632') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8632', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8633') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8633', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8635') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8635', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8636') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8636', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8637') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8637', 'VIRM-1M VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8638') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8638', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8639') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8639', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8640') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8640', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8641') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8641', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8642') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8642', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8644') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8644', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8645') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8645', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8646') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8646', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8647') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8647', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8648') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8648', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8649') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8649', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8651') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8651', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8652') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8652', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8653') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8653', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8654') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8654', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8655') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8655', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8656') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8656', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8657') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8657', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8658') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8658', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8659') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8659', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8660') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8660', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8661') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8661', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8662') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8662', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8663') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8663', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8664') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8664', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8665') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8665', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8666') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8666', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8667') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8667', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8670') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8670', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8671') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8671', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8672') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8672', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8674') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8674', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8675') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8675', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8676') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8676', 'VIRM1 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8701') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8701', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8702') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8702', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8703') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8703', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8705') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8705', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8707') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8707', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8709') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8709', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8711') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8711', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8713') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8713', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8715') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8715', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8717') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8717', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8719') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8719', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8721') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8721', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8723') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8723', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8726') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8726', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8727') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8727', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8728') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8728', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8729') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8729', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8730') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8730', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8731') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8731', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8732') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8732', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8733') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8733', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8734') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8734', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8735') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8735', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8736') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8736', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8737') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8737', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8738') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8738', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8739') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8739', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8740') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8740', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8741') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8741', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8742') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8742', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8743') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8743', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8745') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8745', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '8746') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('8746', 'VIRM2/3 VI', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9401') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9401', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9402') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9402', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9403') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9403', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9404') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9404', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9405') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9405', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9406') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9406', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9407') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9407', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9409') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9409', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9411') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9411', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9412') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9412', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9413') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9413', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9416') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9416', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9417') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9417', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9418') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9418', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9419') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9419', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9420') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9420', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9422') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9422', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9423') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9423', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9425') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9425', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9426') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9426', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9427') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9427', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9430') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9430', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9431') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9431', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9434') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9434', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9443') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9443', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9450') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9450', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9468') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9468', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9469') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9469', 'VIRM-1M IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9473') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9473', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9477') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9477', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9478') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9478', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9479') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9479', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9480') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9480', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9481') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9481', 'VIRM1 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9504') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9504', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9506') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9506', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9508') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9508', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9510') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9510', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9512') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9512', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9514') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9514', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9516') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9516', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9518') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9518', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9520') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9520', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9522') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9522', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9524') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9524', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9525') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9525', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9544') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9544', 'VIRM2/3 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9547') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9547', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9548') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9548', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9549') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9549', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9550') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9550', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9551') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9551', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9552') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9552', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9553') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9553', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9554') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9554', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9555') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9555', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9556') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9556', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9557') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9557', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9558') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9558', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9559') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9559', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9560') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9560', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9561') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9561', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9562') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9562', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9563') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9563', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9564') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9564', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9565') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9565', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9566') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9566', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9567') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9567', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9568') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9568', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9569') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9569', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9570') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9570', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9571') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9571', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9572') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9572', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9573') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9573', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9574') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9574', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9575') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9575', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9576') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9576', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9577') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9577', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9578') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9578', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9579') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9579', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9580') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9580', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9581') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9581', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9582') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9582', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9583') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9583', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9584') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9584', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9585') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9585', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9586') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9586', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9587') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9587', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9588') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9588', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9589') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9589', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9590') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9590', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9591') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9591', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9592') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9592', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9593') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9593', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9594') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9594', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9595') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9595', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9596') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9596', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);
IF NOT EXISTS (SELECT 1 FROM [dbo].[Unit] WHERE UnitNumber = '9597') INSERT INTO [dbo].[Unit] (UnitNumber, UnitType, IsValid, FleetID, UnitStatus, UnitMaintLoc, LastMaintDate) VALUES ('9597', 'VIRM4 IV', 1, 6, NULL, NULL, NULL);

CREATE TABLE [dbo].[#ActiveUnits] (ID int);

CREATE TABLE [dbo].[#IdsForActiveUnits] (ID int);

INSERT INTO [dbo].[#ActiveUnits] (ID)
SELECT 8608 UNION SELECT 8610 UNION SELECT 8608 UNION SELECT 8610
UNION SELECT 8614 UNION SELECT 8615 UNION SELECT 8621 UNION SELECT 8624
UNION SELECT 8628 UNION SELECT 8629 UNION SELECT 8632 UNION SELECT 8633
UNION SELECT 8635 UNION SELECT 8636 UNION SELECT 8637 UNION SELECT 8638
UNION SELECT 8639 UNION SELECT 8640 UNION SELECT 8641 UNION SELECT 8642
UNION SELECT 8644 UNION SELECT 8645 UNION SELECT 8646 UNION SELECT 8647
UNION SELECT 8648 UNION SELECT 8649 UNION SELECT 8651 UNION SELECT 8652
UNION SELECT 8653 UNION SELECT 8654 UNION SELECT 8655 UNION SELECT 8656
UNION SELECT 8657 UNION SELECT 8658 UNION SELECT 8659 UNION SELECT 8660
UNION SELECT 8661 UNION SELECT 8662 UNION SELECT 8663 UNION SELECT 8664
UNION SELECT 8665 UNION SELECT 8666 UNION SELECT 8667 UNION SELECT 8670
UNION SELECT 8671 UNION SELECT 8672 UNION SELECT 8674 UNION SELECT 8675
UNION SELECT 8676 UNION SELECT 8701 UNION SELECT 8702 UNION SELECT 8703
UNION SELECT 8705 UNION SELECT 8707 UNION SELECT 8709 UNION SELECT 8711
UNION SELECT 8713 UNION SELECT 8715 UNION SELECT 8717 UNION SELECT 8719
UNION SELECT 8721 UNION SELECT 8723 UNION SELECT 8726 UNION SELECT 8727
UNION SELECT 8728 UNION SELECT 8729 UNION SELECT 8730 UNION SELECT 8731
UNION SELECT 8732 UNION SELECT 8733 UNION SELECT 8734 UNION SELECT 8735
UNION SELECT 8736 UNION SELECT 8737 UNION SELECT 8738 UNION SELECT 8739
UNION SELECT 8740 UNION SELECT 8741 UNION SELECT 8742 UNION SELECT 8743
UNION SELECT 8745 UNION SELECT 8746 UNION SELECT 9401 UNION SELECT 9402
UNION SELECT 9403 UNION SELECT 9404 UNION SELECT 9405 UNION SELECT 9406
UNION SELECT 9407 UNION SELECT 9409 UNION SELECT 9411 UNION SELECT 9412
UNION SELECT 9413 UNION SELECT 9416 UNION SELECT 9417 UNION SELECT 9418
UNION SELECT 9419 UNION SELECT 9420 UNION SELECT 9422 UNION SELECT 9423
UNION SELECT 9425 UNION SELECT 9426 UNION SELECT 9427 UNION SELECT 9430
UNION SELECT 9431 UNION SELECT 9434 UNION SELECT 9443 UNION SELECT 9450
UNION SELECT 9468 UNION SELECT 9469 UNION SELECT 9473 UNION SELECT 9477
UNION SELECT 9478 UNION SELECT 9479 UNION SELECT 9480 UNION SELECT 9481
UNION SELECT 9504 UNION SELECT 9506 UNION SELECT 9508 UNION SELECT 9510
UNION SELECT 9512 UNION SELECT 9514 UNION SELECT 9516 UNION SELECT 9518
UNION SELECT 9520 UNION SELECT 9522 UNION SELECT 9524 UNION SELECT 9525
UNION SELECT 9544 UNION SELECT 9547 UNION SELECT 9548 UNION SELECT 9549
UNION SELECT 9550 UNION SELECT 9551 UNION SELECT 9552 UNION SELECT 9553
UNION SELECT 9554 UNION SELECT 9555 UNION SELECT 9556 UNION SELECT 9557
UNION SELECT 9558 UNION SELECT 9559 UNION SELECT 9560 UNION SELECT 9561
UNION SELECT 9562 UNION SELECT 9563 UNION SELECT 9564 UNION SELECT 9565
UNION SELECT 9566 UNION SELECT 9567 UNION SELECT 9568 UNION SELECT 9569
UNION SELECT 9570 UNION SELECT 9571 UNION SELECT 9572 UNION SELECT 9573
UNION SELECT 9574 UNION SELECT 9575 UNION SELECT 9576 UNION SELECT 9577
UNION SELECT 9578 UNION SELECT 9579 UNION SELECT 9580 UNION SELECT 9581
UNION SELECT 9582 UNION SELECT 9583 UNION SELECT 9584 UNION SELECT 9585
UNION SELECT 9586 UNION SELECT 9587 UNION SELECT 9588 UNION SELECT 9589
UNION SELECT 9590 UNION SELECT 9591 UNION SELECT 9592 UNION SELECT 9593
UNION SELECT 9594 UNION SELECT 9595 UNION SELECT 9596 UNION SELECT 9597;

INSERT INTO [dbo].[#IdsForActiveUnits]
SELECT ID FROM [dbo].[Unit]
WHERE UnitNumber = '8608' OR UnitNumber = '8610' OR UnitNumber = '8608' OR UnitNumber = '8610'
OR UnitNumber = '8614' OR UnitNumber = '8615' OR UnitNumber = '8621' OR UnitNumber = '8624'
OR UnitNumber = '8628' OR UnitNumber = '8629' OR UnitNumber = '8632' OR UnitNumber = '8633'
OR UnitNumber = '8635' OR UnitNumber = '8636' OR UnitNumber = '8637' OR UnitNumber = '8638'
OR UnitNumber = '8639' OR UnitNumber = '8640' OR UnitNumber = '8641' OR UnitNumber = '8642'
OR UnitNumber = '8644' OR UnitNumber = '8645' OR UnitNumber = '8646' OR UnitNumber = '8647'
OR UnitNumber = '8648' OR UnitNumber = '8649' OR UnitNumber = '8651' OR UnitNumber = '8652'
OR UnitNumber = '8653' OR UnitNumber = '8654' OR UnitNumber = '8655' OR UnitNumber = '8656'
OR UnitNumber = '8657' OR UnitNumber = '8658' OR UnitNumber = '8659' OR UnitNumber = '8660'
OR UnitNumber = '8661' OR UnitNumber = '8662' OR UnitNumber = '8663' OR UnitNumber = '8664'
OR UnitNumber = '8665' OR UnitNumber = '8666' OR UnitNumber = '8667' OR UnitNumber = '8670'
OR UnitNumber = '8671' OR UnitNumber = '8672' OR UnitNumber = '8674' OR UnitNumber = '8675'
OR UnitNumber = '8676' OR UnitNumber = '8701' OR UnitNumber = '8702' OR UnitNumber = '8703'
OR UnitNumber = '8705' OR UnitNumber = '8707' OR UnitNumber = '8709' OR UnitNumber = '8711'
OR UnitNumber = '8713' OR UnitNumber = '8715' OR UnitNumber = '8717' OR UnitNumber = '8719'
OR UnitNumber = '8721' OR UnitNumber = '8723' OR UnitNumber = '8726' OR UnitNumber = '8727'
OR UnitNumber = '8728' OR UnitNumber = '8729' OR UnitNumber = '8730' OR UnitNumber = '8731'
OR UnitNumber = '8732' OR UnitNumber = '8733' OR UnitNumber = '8734' OR UnitNumber = '8735'
OR UnitNumber = '8736' OR UnitNumber = '8737' OR UnitNumber = '8738' OR UnitNumber = '8739'
OR UnitNumber = '8740' OR UnitNumber = '8741' OR UnitNumber = '8742' OR UnitNumber = '8743'
OR UnitNumber = '8745' OR UnitNumber = '8746' OR UnitNumber = '9401' OR UnitNumber = '9402'
OR UnitNumber = '9403' OR UnitNumber = '9404' OR UnitNumber = '9405' OR UnitNumber = '9406'
OR UnitNumber = '9407' OR UnitNumber = '9409' OR UnitNumber = '9411' OR UnitNumber = '9412'
OR UnitNumber = '9413' OR UnitNumber = '9416' OR UnitNumber = '9417' OR UnitNumber = '9418'
OR UnitNumber = '9419' OR UnitNumber = '9420' OR UnitNumber = '9422' OR UnitNumber = '9423'
OR UnitNumber = '9425' OR UnitNumber = '9426' OR UnitNumber = '9427' OR UnitNumber = '9430'
OR UnitNumber = '9431' OR UnitNumber = '9434' OR UnitNumber = '9443' OR UnitNumber = '9450'
OR UnitNumber = '9468' OR UnitNumber = '9469' OR UnitNumber = '9473' OR UnitNumber = '9477'
OR UnitNumber = '9478' OR UnitNumber = '9479' OR UnitNumber = '9480' OR UnitNumber = '9481'
OR UnitNumber = '9504' OR UnitNumber = '9506' OR UnitNumber = '9508' OR UnitNumber = '9510'
OR UnitNumber = '9512' OR UnitNumber = '9514' OR UnitNumber = '9516' OR UnitNumber = '9518'
OR UnitNumber = '9520' OR UnitNumber = '9522' OR UnitNumber = '9524' OR UnitNumber = '9525'
OR UnitNumber = '9544' OR UnitNumber = '9547' OR UnitNumber = '9548' OR UnitNumber = '9549'
OR UnitNumber = '9550' OR UnitNumber = '9551' OR UnitNumber = '9552' OR UnitNumber = '9553'
OR UnitNumber = '9554' OR UnitNumber = '9555' OR UnitNumber = '9556' OR UnitNumber = '9557'
OR UnitNumber = '9558' OR UnitNumber = '9559' OR UnitNumber = '9560' OR UnitNumber = '9561'
OR UnitNumber = '9562' OR UnitNumber = '9563' OR UnitNumber = '9564' OR UnitNumber = '9565'
OR UnitNumber = '9566' OR UnitNumber = '9567' OR UnitNumber = '9568' OR UnitNumber = '9569'
OR UnitNumber = '9570' OR UnitNumber = '9571' OR UnitNumber = '9572' OR UnitNumber = '9573'
OR UnitNumber = '9574' OR UnitNumber = '9575' OR UnitNumber = '9576' OR UnitNumber = '9577'
OR UnitNumber = '9578' OR UnitNumber = '9579' OR UnitNumber = '9580' OR UnitNumber = '9581'
OR UnitNumber = '9582' OR UnitNumber = '9583' OR UnitNumber = '9584' OR UnitNumber = '9585'
OR UnitNumber = '9586' OR UnitNumber = '9587' OR UnitNumber = '9588' OR UnitNumber = '9589'
OR UnitNumber = '9590' OR UnitNumber = '9591' OR UnitNumber = '9592' OR UnitNumber = '9593'
OR UnitNumber = '9594' OR UnitNumber = '9595' OR UnitNumber = '9596' OR UnitNumber = '9597';


DELETE FROM [dbo].[EventChannelValue]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[FaultChannelValue]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[ChannelValue]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[Fault]
WHERE FaultUnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[Vehicle]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[ServiceRequest]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[WorkOrder]
WHERE UnitID NOT IN (SELECT ID FROM [dbo].[#IdsForActiveUnits]);

DELETE FROM [dbo].[Unit]
WHERE UnitNumber NOT IN (SELECT ID FROM [dbo].[#ActiveUnits]);

DROP TABLE [dbo].[#ActiveUnits];

DROP TABLE [dbo].[#IdsForActiveUnits];

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,061
           ,'update_spectrum_db_load_061.sql'
           ,'1.12.01')
GO