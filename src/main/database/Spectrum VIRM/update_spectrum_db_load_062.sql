SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 062
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update - NS-844 - change unit type field and populate two tables
--------------------------------------------

RAISERROR ('-- Update the UnitType field for unit 8610', 0, 1) WITH NOWAIT
GO

-- update the unit type field

UPDATE Unit
   SET UnitType = 'VIRM1-VI'
 WHERE UnitNumber = 8610
GO

RAISERROR ('-- Populate tables', 0, 1) WITH NOWAIT
GO


-- insert new fleet into FleetFormation and FleetStatus

INSERT INTO FleetFormation(UnitIDList,UnitNumberList,ValidFrom,ValidTo)
SELECT id, UnitNumber,GETDATE(),GETDATE() from Unit where UnitNumber = 8610

INSERT INTO FleetStatus (UnitID,UnitList,ValidFROM, isvalid,UnitPosition,FleetFormationID)
SELECT id, UnitNumber,GETDATE(),1,1,(Select id FROM FleetFormation where UnitIDList = unit.id) from Unit where UnitNumber = 8610
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,062
           ,'update_spectrum_db_load_062.sql'
           ,'1.12.01')
GO