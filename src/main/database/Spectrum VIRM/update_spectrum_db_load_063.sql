SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 063
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update - NS-844 - change unit type field and populate two tables
--------------------------------------------

RAISERROR ('-- Populated Channel Mask for software version 316', 0, 1) WITH NOWAIT
GO

INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGONDERIN'), 1688, 1688, 1689)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGBOVENIN'), 1690, 1690, 1691)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '36A1Verweindrin'), 1724, 1724, 1725)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '317A1_Vrijgave_Openen'), 1806, 1806, 1807)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '327A1_Vrijgave_Openen'), 1886, 1886, 1887)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '337A1_Vrijgave_Openen'), 1966, 1966, 1967)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '347A1_Vrijgave_Openen'), 2046, 2046, 2047)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '34A1_DS_HL_MG'), 2122, 2122, 2123)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '38A3_Compressor_1_IN'), 2220, 2220, 2221)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '38A3_Druk__8_BAR'), 2228, 2228, 2229)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGONDERIN'), 2930, 2930, 2931)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGBOVENIN'), 2932, 2932, 2933)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '56A1Verweindrin'), 2966, 2966, 2967)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '517A1_Vrijgave_Openen'), 3048, 3048, 3049)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '527A1_Vrijgave_Openen'), 3128, 3128, 3129)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '537A1_Vrijgave_Openen'), 3208, 3208, 3209)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '547A1_Vrijgave_Openen'), 3288, 3288, 3289)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '54A1_DS_HL_MG'), 3364, 3364, 3365)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '64B5_HR_DRUK'), 375, 368, 3865)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '6Treinleidingdruk'), 383, 376, 3867)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGONDERIN'), 4110, 4110, 4111)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGBOVENIN'), 4112, 4112, 4113)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '66A1Verweindrin'), 4146, 4146, 4147)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '617A1_Vrijgave_Openen'), 4228, 4228, 4229)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '627A1_Vrijgave_Openen'), 4308, 4308, 4309)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '637A1_Vrijgave_Openen'), 4388, 4388, 4389)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '647A1_Vrijgave_Openen'), 4468, 4468, 4469)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '64A1_DS_HL_MG'), 4544, 4544, 4545)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '68A3_Compressor_Gevraagd'), 4684, 4684, 4685)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '6TLdrukkleinerals05Bar'), 5064, 5064, 5065)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '68A2_Druk__5_BAR'), 5072, 5072, 5073)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '68A2_Druk__85_BAR'), 5074, 5074, 5075)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '68A2_Druk__95_BAR'), 5082, 5082, 5083)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '73A8_MW_UBAT_VOLT'), 751, 744, 5717)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '73A2_M_UT'), 6298, 6298, 6299)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '73A2_M_IT_GEM'), 783, 776, 6301)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGONDERIN'), 6392, 6392, 6393)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGBOVENIN'), 6394, 6394, 6395)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '76A1Verweindrin'), 6428, 6428, 6429)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '717A1_Vrijgave_Openen'), 6510, 6510, 6511)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '727A1_Vrijgave_Openen'), 6590, 6590, 6591)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '737A1_Vrijgave_Openen'), 6670, 6670, 6671)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '747A1_Vrijgave_Openen'), 6750, 6750, 6751)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '74A1_DS_HL_MG'), 6826, 6826, 6827)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '74A1_SLIP_ABI'), 6854, 6854, 6855)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_BELADING'), 6922, 6922, 6923)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_IN_Gevraagd_Koppel'), 791, 784, 6925)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_IN_Vooruit'), 6938, 6938, 6939)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_IN_Achteruit'), 6940, 6940, 6941)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_ILACT'), 7002, 7002, 7003)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_METING_LIJNSPANNING'), 7008, 7008, 7009)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_Inverter_Frequentie'), 807, 800, 7017)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_Detectie_Wielslip'), 7022, 7022, 7023)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '78A4_IN_HS_AANWEZIG'), 7276, 7276, 7277)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '72A1_Tractie_Levert_Koppel'), 7474, 7474, 7475)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '13A8_MW_UBAT_VOLT'), 967, 960, 7589)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '13A2_M_UT'), 8170, 8170, 8171)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '13A2_M_IT_GEM'), 999, 992, 8173)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A3_Gereed_Continu'), 8266, 8266, 8267)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A3_Bediende_Cabine'), 8272, 8272, 8273)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A3_Bedrijfstoestand_Gereed'), 8274, 8274, 8275)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A3_IN_Dienstvaardig'), 8278, 8278, 8279)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A4_ATB_Snelrem'), 8304, 8304, 8305)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A4_IN_HS_AANWEZIG'), 8312, 8312, 8313)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGONDERIN'), 8332, 8332, 8333)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGBOVENIN'), 8334, 8334, 8335)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '16A1Verwcabinein'), 8368, 8368, 8369)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '117A1_Vrijgave_Openen'), 8450, 8450, 8451)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '127A1_Vrijgave_Openen'), 8530, 8530, 8531)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '137A1_Vrijgave_Openen'), 8610, 8610, 8611)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '147A1_Vrijgave_Openen'), 8690, 8690, 8691)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_DS_HL_MG'), 8766, 8766, 8767)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_SLIP_ABI'), 8794, 8794, 8795)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan5'), 8810, 8810, 8811)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan4'), 8812, 8812, 8813)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan3'), 8814, 8814, 8815)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan2'), 8816, 8816, 8817)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan1'), 8818, 8818, 8819)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraanT'), 8822, 8822, 8823)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraanA'), 8824, 8824, 8825)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_Rem_Noodbedrijf'), 8826, 8826, 8827)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_ST_SB'), 8834, 8834, 8835)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan7'), 8838, 8838, 8839)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1standremkraan6'), 8840, 8840, 8841)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1Noodremreizigers'), 8848, 8848, 8849)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_17'), 8852, 8852, 8853)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_27'), 8854, 8854, 8855)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_47'), 8856, 8856, 8857)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_BELADING'), 8878, 8878, 8879)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_IN_Gevraagd_Koppel'), 1007, 1000, 8881)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_IN_Vooruit'), 8894, 8894, 8895)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_IN_Achteruit'), 8896, 8896, 8897)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '1Actueelkoppel'), 1015, 1008, 8957)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_ILACT'), 8958, 8958, 8959)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_METING_LIJNSPANNING'), 8964, 8964, 8965)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_Inverter_Frequentie'), 1023, 1016, 8973)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_Detectie_Wielslip'), 8978, 8978, 8979)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A2_ATB_Treinsnelheid'), 1031, 1024, 9251)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '18A2_ATB_Bewaakte_Snelheid'), 1039, 1032, 9253)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '12A1_Tractie_Levert_Koppel'), 9454, 9454, 9455)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '23A8_MW_UBAT_VOLT'), 1199, 1192, 9585)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '23A2_M_UT'), 10166, 10166, 10167)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '23A2_M_IT_GEM'), 1231, 1224, 10169)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A3_Gereed_Continu'), 10262, 10262, 10263)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A3_Bediende_Cabine'), 10268, 10268, 10269)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A3_Bedrijfstoestand_Gereed'), 10270, 10270, 10271)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A3_IN_Dienstvaardig'), 10274, 10274, 10275)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A4_ATB_Snelrem'), 10300, 10300, 10301)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A4_IN_HS_AANWEZIG'), 10308, 10308, 10309)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGONDERIN'), 10328, 10328, 10329)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGBOVENIN'), 10330, 10330, 10331)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '26A1Verwcabinein'), 10364, 10364, 10365)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '217A1_Vrijgave_Openen'), 10446, 10446, 10447)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '227A1_Vrijgave_Openen'), 10526, 10526, 10527)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '237A1_Vrijgave_Openen'), 10606, 10606, 10607)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '247A1_Vrijgave_Openen'), 10686, 10686, 10687)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '24A1_DS_HL_MG'), 10762, 10762, 10763)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '24A1_SLIP_ABI'), 10790, 10790, 10791)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan5'), 10806, 10806, 10807)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan4'), 10808, 10808, 10809)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan3'), 10810, 10810, 10811)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan2'), 10812, 10812, 10813)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan1'), 10814, 10814, 10815)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraanT'), 10818, 10818, 10819)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraanA'), 10820, 10820, 10821)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '24A1_Rem_Noodbedrijf'), 10822, 10822, 10823)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '24A1_ST_SB'), 10830, 10830, 10831)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan7'), 10834, 10834, 10835)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2standremkraan6'), 10836, 10836, 10837)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '24A1Noodremreizigers'), 10844, 10844, 10845)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_BELADING'), 10874, 10874, 10875)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_IN_Gevraagd_Koppel'), 1239, 1232, 10877)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_IN_Vooruit'), 10890, 10890, 10891)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_IN_Achteruit'), 10892, 10892, 10893)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '2Actueelkoppel'), 1247, 1240, 10953)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_ILACT'), 10954, 10954, 10955)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_METING_LIJNSPANNING'), 10960, 10960, 10961)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_Inverter_Frequentie'), 1255, 1248, 10969)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '22A1_Detectie_Wielslip'), 10974, 10974, 10975)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Treinsnelheid'), 1263, 1256, 11247)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES (316, (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Bewaakte_Snelheid'), 1271, 1264, 11249)

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,063
           ,'update_spectrum_db_load_063.sql'
           ,'1.12.01')
GO