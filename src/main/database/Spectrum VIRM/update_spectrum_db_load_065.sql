SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 065
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update = R2M-4785 - support software version 312, dataset mt
--------------------------------------------

RAISERROR ('-- Insert new channels for mask 312', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT [dbo].[Channel] ON

INSERT INTO Channel (ID, EngColumnNo, Name, Description , REF, DataType, TypeID, ChannelGroupID, MinValue, MaxValue, Comment, UOM, IsAlwaysDisplayed,IsLookup,IsDisplayedAsDifference,VehicleIn4Car,VehicleIn6Car,ChannelCategoryID) VALUES ( 146,146,'1Actueelkoppel_mBvk1','Actuele tractie','A237','int',2,(SELECT ID FROM ChannelGroup where ChannelGroupName ='Traction'),'-12700',12700,'Actuele tractie','Nm',1,1,1,1,1,6)
INSERT INTO Channel (ID, EngColumnNo, Name, Description , REF, DataType, TypeID, ChannelGroupID, MinValue, MaxValue, Comment, UOM, IsAlwaysDisplayed,IsLookup,IsDisplayedAsDifference,VehicleIn4Car,VehicleIn6Car,ChannelCategoryID) VALUES ( 147,147,'2Actueelkoppel_mBvk1','Actuele tractie','A238','int',2,(SELECT ID FROM ChannelGroup where ChannelGroupName ='Traction'),'-12700',12700,'Actuele tractie','Nm',1,1,1,1,1,6)
INSERT INTO Channel (ID, EngColumnNo, Name, Description , REF, DataType, TypeID, ChannelGroupID, MinValue, MaxValue, Comment, UOM, IsAlwaysDisplayed,IsLookup,IsDisplayedAsDifference,VehicleIn4Car,VehicleIn6Car,ChannelCategoryID) VALUES ( 148,148,'7Actueelkoppel_mBvk1','Actuele tractie','A239','int',2,(SELECT ID FROM ChannelGroup where ChannelGroupName ='Traction'),'-12700',12700,'Actuele tractie','Nm',1,1,1,1,1,6)

SET IDENTITY_INSERT [dbo].[Channel] OFF

GO

--------------------------------------------

RAISERROR ('-- change channel data type for 12 channels', 0, 1) WITH NOWAIT
GO

UPDATE Channel SET Datatype = 'int' WHERE Name = '73A2_M_UT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '72A1_BELADING'
UPDATE Channel SET Datatype = 'int' WHERE Name = '72A1_ILACT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '72A1_METING_LIJNSPANNING'
UPDATE Channel SET Datatype = 'int' WHERE Name = '13A2_M_UT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '12A1_BELADING'
UPDATE Channel SET Datatype = 'int' WHERE Name = '12A1_ILACT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '12A1_METING_LIJNSPANNING'
UPDATE Channel SET Datatype = 'int' WHERE Name = '23A2_M_UT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '22A1_BELADING'
UPDATE Channel SET Datatype = 'int' WHERE Name = '22A1_ILACT'
UPDATE Channel SET Datatype = 'int' WHERE Name = '22A1_METING_LIJNSPANNING'

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,065
           ,'update_spectrum_db_load_065.sql'
           ,'1.13.01')
GO