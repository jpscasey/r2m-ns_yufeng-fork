SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 066
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------------------------------------------
-- data load script - add masks for software version 312 and dataset mt
---------------------------------------------------------------------------

RAISERROR ('-- Populated Channel Mask for software version 312', 0, 1) WITH NOWAIT
GO

INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGONDERIN'), 1784, 1784, 1785)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGBOVENIN'), 1786, 1786, 1787)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '36A1Verweindrin'), 1820, 1820, 1821)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '317A1_Vrijgave_Openen'), 1902, 1902, 1903)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '327A1_Vrijgave_Openen'), 1982, 1982, 1983)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '337A1_Vrijgave_Openen'), 2062, 2062, 2063)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '347A1_Vrijgave_Openen'), 2142, 2142, 2143)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '34A1_DS_HL_MG'), 2218, 2218, 2219)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '38A3_Compressor_1_IN'), 2316, 2316, 2317)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '38A3_Druk__8_BAR'), 2324, 2324, 2325)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGONDERIN'), 3026, 3026, 3027)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGBOVENIN'), 3028, 3028, 3029)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '56A1Verweindrin'), 3062, 3062, 3063)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '517A1_Vrijgave_Openen'), 3144, 3144, 3145)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '527A1_Vrijgave_Openen'), 3224, 3224, 3225)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '537A1_Vrijgave_Openen'), 3304, 3304, 3305)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '547A1_Vrijgave_Openen'), 3384, 3384, 3385)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '54A1_DS_HL_MG'), 3460, 3460, 3461)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '64B5_HR_DRUK'), 375, 368, 3961)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '6Treinleidingdruk'), 383, 376, 3963)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGONDERIN'), 4206, 4206, 4207)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGBOVENIN'), 4208, 4208, 4209)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '66A1Verweindrin'), 4242, 4242, 4243)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '617A1_Vrijgave_Openen'), 4324, 4324, 4325)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '627A1_Vrijgave_Openen'), 4404, 4404, 4405)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '637A1_Vrijgave_Openen'), 4484, 4484, 4485)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '647A1_Vrijgave_Openen'), 4564, 4564, 4565)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '64A1_DS_HL_MG'), 4640, 4640, 4641)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '68A3_Compressor_Gevraagd'), 4780, 4780, 4781)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '6TLdrukkleinerals05Bar'), 5160, 5160, 5161)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '68A2_Druk__5_BAR'), 5168, 5168, 5169)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '68A2_Druk__85_BAR'), 5170, 5170, 5171)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '68A2_Druk__95_BAR'), 5178, 5178, 5179)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '73A8_MW_UBAT_VOLT'), 751, 744, 5813)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '73A2_M_UT'), 6394, 6394, 6395)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '73A2_M_IT_GEM'), 783, 776, 6397)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGONDERIN'), 6488, 6488, 6489)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGBOVENIN'), 6490, 6490, 6491)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '76A1Verweindrin'), 6524, 6524, 6525)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '717A1_Vrijgave_Openen'), 6606, 6606, 6607)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '727A1_Vrijgave_Openen'), 6686, 6686, 6687)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '737A1_Vrijgave_Openen'), 6766, 6766, 6767)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '747A1_Vrijgave_Openen'), 6846, 6846, 6847)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '74A1_DS_HL_MG'), 6922, 6922, 6923)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '74A1_SLIP_ABI'), 6950, 6950, 6951)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_BELADING'), 7018, 7018, 7019)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Gevraagd_Koppel'), 791, 784, 7021)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Vooruit'), 7034, 7034, 7035)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Achteruit'), 7036, 7036, 7037)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '7Actueelkoppel_mBvk1'), 799, 792, 7097)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_ILACT'), 7098, 7098, 7099)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_METING_LIJNSPANNING'), 7104, 7104, 7105)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_Inverter_Frequentie'), 807, 800, 7113)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_Detectie_Wielslip'), 7118, 7118, 7119)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '78A4_IN_HS_AANWEZIG'), 7372, 7372, 7373)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '72A1_Tractie_Levert_Koppel'), 7570, 7570, 7571)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '13A8_MW_UBAT_VOLT'), 967, 960, 7685)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '13A2_M_UT'), 8266, 8266, 8267)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '13A2_M_IT_GEM'), 999, 992, 8269)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A3_Gereed_Continu'), 8362, 8362, 8363)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A3_Bediende_Cabine'), 8368, 8368, 8369)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A3_Bedrijfstoestand_Gereed'), 8370, 8370, 8371)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A3_IN_Dienstvaardig'), 8374, 8374, 8375)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A4_ATB_Snelrem'), 8400, 8400, 8401)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A4_IN_HS_AANWEZIG'), 8408, 8408, 8409)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGONDERIN'), 8428, 8428, 8429)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGBOVENIN'), 8430, 8430, 8431)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '16A1Verwcabinein'), 8464, 8464, 8465)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '117A1_Vrijgave_Openen'), 8546, 8546, 8547)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '127A1_Vrijgave_Openen'), 8626, 8626, 8627)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '137A1_Vrijgave_Openen'), 8706, 8706, 8707)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '147A1_Vrijgave_Openen'), 8786, 8786, 8787)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_DS_HL_MG'), 8862, 8862, 8863)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_SLIP_ABI'), 8890, 8890, 8891)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan5'), 8906, 8906, 8907)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan4'), 8908, 8908, 8909)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan3'), 8910, 8910, 8911)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan2'), 8912, 8912, 8913)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan1'), 8914, 8914, 8915)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraanT'), 8918, 8918, 8919)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraanA'), 8920, 8920, 8921)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_Rem_Noodbedrijf'), 8922, 8922, 8923)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_ST_SB'), 8930, 8930, 8931)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan7'), 8934, 8934, 8935)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1standremkraan6'), 8936, 8936, 8937)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1Noodremreizigers'), 8944, 8944, 8945)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_17'), 8948, 8948, 8949)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_27'), 8950, 8950, 8951)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_47'), 8952, 8952, 8953)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_BELADING'), 8974, 8974, 8975)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Gevraagd_Koppel'), 1007, 1000, 8977)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Vooruit'), 8990, 8990, 8991)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Achteruit'), 8992, 8992, 8993)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '1Actueelkoppel_mBvk1'), 1015, 1008, 9053)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_ILACT'), 9054, 9054, 9055)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_METING_LIJNSPANNING'), 9060, 9060, 9061)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_Inverter_Frequentie'), 1023, 1016, 9069)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_Detectie_Wielslip'), 9074, 9074, 9075)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A2_ATB_Treinsnelheid'), 1031, 1024, 9347)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '18A2_ATB_Bewaakte_Snelheid'), 1039, 1032, 9349)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '12A1_Tractie_Levert_Koppel'), 9550, 9550, 9551)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '23A8_MW_UBAT_VOLT'), 1199, 1192, 9681)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '23A2_M_UT'), 10262, 10262, 10263)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '23A2_M_IT_GEM'), 1231, 1224, 10265)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A3_Gereed_Continu'), 10358, 10358, 10359)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A3_Bediende_Cabine'), 10364, 10364, 10365)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A3_Bedrijfstoestand_Gereed'), 10366, 10366, 10367)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A3_IN_Dienstvaardig'), 10370, 10370, 10371)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A4_ATB_Snelrem'), 10396, 10396, 10397)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A4_IN_HS_AANWEZIG'), 10404, 10404, 10405)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGONDERIN'), 10424, 10424, 10425)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGBOVENIN'), 10426, 10426, 10427)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '26A1Verwcabinein'), 10460, 10460, 10461)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '217A1_Vrijgave_Openen'), 10542, 10542, 10543)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '227A1_Vrijgave_Openen'), 10622, 10622, 10623)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '237A1_Vrijgave_Openen'), 10702, 10702, 10703)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '247A1_Vrijgave_Openen'), 10782, 10782, 10783)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '24A1_DS_HL_MG'), 10858, 10858, 10859)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '24A1_SLIP_ABI'), 10886, 10886, 10887)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan5'), 10902, 10902, 10903)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan4'), 10904, 10904, 10905)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan3'), 10906, 10906, 10907)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan2'), 10908, 10908, 10909)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan1'), 10910, 10910, 10911)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraanT'), 10914, 10914, 10915)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraanA'), 10916, 10916, 10917)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '24A1_Rem_Noodbedrijf'), 10918, 10918, 10919)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '24A1_ST_SB'), 10926, 10926, 10927)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan7'), 10930, 10930, 10931)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2standremkraan6'), 10932, 10932, 10933)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '24A1Noodremreizigers'), 10940, 10940, 10941)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_BELADING'), 10970, 10970, 10971)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Gevraagd_Koppel'), 1239, 1232, 10973)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Vooruit'), 10986, 10986, 10987)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Achteruit'), 10988, 10988, 10989)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '2Actueelkoppel_mBvk1'), 1247, 1240, 11049)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_ILACT'), 11050, 11050, 11051)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_METING_LIJNSPANNING'), 11056, 11056, 11057)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_Inverter_Frequentie'), 1255, 1248, 11065)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_Detectie_Wielslip'), 11070, 11070, 11071)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Treinsnelheid'), 1263, 1256, 11343)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Bewaakte_Snelheid'), 1271, 1264, 11345)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('312', (SELECT ID FROM Channel WHERE Name = '22A1_Tractie_Levert_Koppel'), 11544, 11544, 11545)

GO
-----------------------------------------------------------------------------------

RAISERROR ('-- Populated Channel Mask for software version mt', 0, 1) WITH NOWAIT
GO

INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGONDERIN'), 4, 4, 0)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '36A1_UIT_VERWARMINGBOVENIN'), 12, 12, 8)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '36A1Verweindrin'), 20, 20, 16)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '34A1_DS_HL_MG'), 28, 28, 24)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '38A3_Compressor_1_IN'), 36, 36, 32)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGONDERIN'), 44, 44, 40)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '56A1_UIT_VERWARMINGBOVENIN'), 52, 52, 48)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '56A1Verweindrin'), 60, 60, 56)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGONDERIN'), 68, 68, 64)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '66A1_UIT_VERWARMINGBOVENIN'), 76, 76, 72)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '66A1Verweindrin'), 84, 84, 80)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '64A1_DS_HL_MG'), 92, 92, 88)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGONDERIN'), 100, 100, 96)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '76A1_UIT_VERWARMINGBOVENIN'), 108, 108, 104)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '76A1Verweindrin'), 116, 116, 112)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '74A1_SLIP_ABI'), 124, 124, 120)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Vooruit'), 132, 132, 128)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Achteruit'), 140, 140, 136)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_Detectie_Wielslip'), 148, 148, 144)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_Tractie_Levert_Koppel'), 156, 156, 152)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGONDERIN'), 164, 164, 160)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '16A1_UIT_VERWARMINGBOVENIN'), 172, 172, 168)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '16A1Verwcabinein'), 180, 180, 176)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '117A1_Vrijgave_Openen'), 188, 188, 184)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '137A1_Vrijgave_Openen'), 196, 196, 192)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_DS_HL_MG'), 204, 204, 200)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_SLIP_ABI'), 212, 212, 208)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_ST_SB'), 220, 220, 216)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_17'), 228, 228, 224)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_27'), 236, 236, 232)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '14A1_Ingang_Treindraad_47'), 244, 244, 240)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Vooruit'), 252, 252, 248)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Achteruit'), 260, 260, 256)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_Detectie_Wielslip'), 268, 268, 264)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_Tractie_Levert_Koppel'), 276, 276, 272)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGONDERIN'), 284, 284, 280)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '26A1_UIT_VERWARMINGBOVENIN'), 292, 292, 288)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '26A1Verwcabinein'), 300, 300, 296)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '24A1_DS_HL_MG'), 308, 308, 304)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '24A1_SLIP_ABI'), 316, 316, 312)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '24A1_ST_SB'), 324, 324, 320)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Vooruit'), 332, 332, 328)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Achteruit'), 340, 340, 336)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_Detectie_Wielslip'), 348, 348, 344)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_Tractie_Levert_Koppel'), 356, 356, 352)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '64B5_HR_DRUK'), 375, 360, 376)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '6Treinleidingdruk'), 399, 384, 400)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '73A8_MW_UBAT_VOLT'), 423, 408, 424)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '73A2_M_UT'), 447, 432, 448)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '73A2_M_IT_GEM'), 471, 456, 472)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_BELADING'), 495, 480, 496)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_IN_Gevraagd_Koppel'), 519, 504, 520)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '7Actueelkoppel_mBvk1'), 543, 528, 544)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_ILACT'), 567, 552, 568)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_METING_LIJNSPANNING'), 591, 576, 592)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '72A1_Inverter_Frequentie'), 615, 600, 616)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '13A8_MW_UBAT_VOLT'), 639, 624, 640)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '13A2_M_UT'), 663, 648, 664)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '13A2_M_IT_GEM'), 687, 672, 688)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_BELADING'), 711, 696, 712)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_IN_Gevraagd_Koppel'), 735, 720, 736)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '1Actueelkoppel_mBvk1'), 759, 744, 760)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_ILACT'), 783, 768, 784)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_METING_LIJNSPANNING'), 807, 792, 808)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '12A1_Inverter_Frequentie'), 831, 816, 832)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '18A2_ATB_Treinsnelheid'), 855, 840, 856)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '23A8_MW_UBAT_VOLT'), 879, 864, 880)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '23A2_M_UT'), 903, 888, 904)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '23A2_M_IT_GEM'), 927, 912, 928)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_BELADING'), 951, 936, 952)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_IN_Gevraagd_Koppel'), 975, 960, 976)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '2Actueelkoppel_mBvk1'), 999, 984, 1000)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_ILACT'), 1023, 1008, 1024)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_METING_LIJNSPANNING'), 1047, 1032, 1048)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '22A1_Inverter_Frequentie'), 1071, 1056, 1072)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Treinsnelheid'), 1095, 1080, 1096)
INSERT INTO [dbo].[ChannelMask] (MaskName, ChannelID, MostSignificantBit, LeastSignificantBit, ValidBitAddress) VALUES ('mt', (SELECT ID FROM Channel WHERE Name = '28A2_ATB_Bewaakte_Snelheid'), 1119, 1104, 1120)

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,066
           ,'update_spectrum_db_load_066.sql'
           ,'1.13.01')
GO