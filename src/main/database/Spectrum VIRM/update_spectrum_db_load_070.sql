SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 070
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------

DECLARE @UnitId8702 int = (SELECT ID FROM Unit WHERE UnitNumber = 8702);
DECLARE @UnitId9468 int = (SELECT ID FROM Unit WHERE UnitNumber = 9468);
DECLARE @UnitId9544 int = (SELECT ID FROM Unit WHERE UnitNumber = 9544);

IF NOT EXISTS (SELECT 1 FROM FleetFormation WHERE UnitNumberList = '8702')
BEGIN
	INSERT INTO FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo) VALUES 
	(CAST(@UnitId8702 AS varchar(100)) ,'8702', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
END

IF NOT EXISTS (SELECT 1 FROM FleetFormation WHERE UnitNumberList = '9468')
BEGIN
	INSERT INTO FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo) VALUES
	(CAST(@UnitId9468 AS varchar(100)) ,'9468', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
END

IF NOT EXISTS (SELECT 1 FROM FleetFormation WHERE UnitNumberList = '9544')
BEGIN 
	INSERT INTO FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo) VALUES 
	(CAST(@UnitId9544 AS varchar(100)) ,'9544', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
END

IF NOT EXISTS (SELECT 1 FROM FleetStatus WHERE UnitList = '8702')
BEGIN
	INSERT INTO FleetStatus (ValidFrom, IsValid, UnitList, UnitID, UnitPosition, UnitOrientation, FleetFormationID) values 
  	(CURRENT_TIMESTAMP, 1, '8702', @UnitId8702, 1, 1, (SELECT ID FROM FleetFormation WHERE UnitIdList = CAST(@UnitId8702 AS varchar(100))))
END

IF NOT EXISTS (SELECT 1 FROM FleetStatus WHERE UnitList = '9468')
BEGIN
	INSERT INTO FleetStatus (ValidFrom, IsValid, UnitList, UnitID, UnitPosition, UnitOrientation, FleetFormationID) values 
  	(CURRENT_TIMESTAMP, 1, '9468', @UnitId9468, 1, 1, (SELECT ID FROM FleetFormation WHERE UnitIdList = CAST(@UnitId9468 AS varchar(100))))
END

IF NOT EXISTS (SELECT 1 FROM FleetStatus WHERE UnitList = '9544')
BEGIN 
	INSERT INTO FleetStatus (ValidFrom, IsValid, UnitList, UnitID, UnitPosition, UnitOrientation, FleetFormationID) values 
  	(CURRENT_TIMESTAMP, 1, '9544', @UnitId9544, 1, 1, (SELECT ID FROM FleetFormation WHERE UnitIdList = CAST(@UnitId9544 AS varchar(100))))
END
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,070
           ,'update_spectrum_db_load_070.sql'
           ,NULL)
GO