SET ANSI_NULLS ON
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 1
SET @scriptType = 'database update job'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('--Job [- DELETE ChannelValue Older tahn 4 Months', 0, 1) WITH NOWAIT
GO
USE [msdb]
GO

/****** Object:  Job [- DELETE ChannelValue Older tahn 4 Months]    Script Date: 16/02/2017 17:26:44 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
DECLARE @dbname nvarchar(100) = '$(DB_NAME)'
DECLARE @jobname nvarchar(100) = @dbname+' - DELETE ChannelValue Older tahn 4 Months'
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 16/02/2017 17:26:44 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@jobname, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete ChannelValue Older than 4 Months]    Script Date: 16/02/2017 17:26:44 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete ChannelValue Older than 4 Months', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Two step aproach is due to 
ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))#

Not running in SP
*/

DECLARE @date date = dateADD(DAY,-130,sysdatetime())
EXEC NSP_DeleteChannelValue @date 

DECLARE @TopDate date
SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name =''PF_ChannelValue_Date''
order by value

While @date > @TopDate
BEGIN

	ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
			
	SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
	INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
	Where f.name =''PF_ChannelValue_Date''
	order by value

END', 
		@database_name=N'$(DB_NAME)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 2am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170216, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'43513943-dc84-479b-812c-3d537d411c15'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

RAISERROR ('-- Job [- Create ChannelValue(door) Partition] ', 0, 1) WITH NOWAIT
GO

/****** Object:  Job [- Create ChannelValue(door) Partition]    Script Date: 16/02/2017 17:28:18 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
DECLARE @dbname nvarchar(100) = '$(DB_NAME)'
DECLARE @jobname nvarchar(100) = @dbname+' - Create ChannelValue(door) Partition'
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 16/02/2017 17:28:18 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@jobname, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create Partition]    Script Date: 16/02/2017 17:28:18 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Partition', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @D date 

SELECT Top 1  @D = CAST(value as date)  FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name =''PF_ChannelValue_Date''
ORDER BY value DESC


While 1=1
BEGIN
	IF @D > DATEADD(day,7,SYSDATETIME()) BREAK;
	
	ALTER PARTITION SCHEME [PS_ChannelValue_Date]
	NEXT USED [PRIMARY]

	ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()
	SPLIT RANGE (CONVERT(VARCHAR(8), @D, 112))
	
	SET @D = DATEADD(day,1,@D)
END
', 
		@database_name=N'$(DB_NAME)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


RAISERROR ('-- Job [- DELETE EventChannelValue 12Months old] ', 0, 1) WITH NOWAIT
GO

/****** Object:  Job [- DELETE EventChannelValue 12Months old]    Script Date: 16/02/2017 17:29:31 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
DECLARE @dbname nvarchar(100) = '$(DB_NAME)'
DECLARE @jobname nvarchar(100) = @dbname+' - DELETE EventChannelValue 12Months old'
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 16/02/2017 17:29:31 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@dbname, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete EventChannelData Older than 12 months]    Script Date: 16/02/2017 17:29:31 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete EventChannelData Older than 12 months', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Two step aproach is due to 
ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))#

Not running in SP
*/

DECLARE @date date = dateADD(DAY,-375,sysdatetime())
EXEC NSP_DeleteEventChannelValue @date 

DECLARE @TopDate date
SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name =''PF_EventChannelValue_Date''
order by value

While @date > @TopDate
BEGIN

	ALTER PARTITION FUNCTION [PF_EventChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
			
	SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
	INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
	Where f.name =''PF_EventChannelValue_Date''
	order by value

END', 
		@database_name=N'$(DB_NAME)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 2am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170216, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'43513943-dc84-479b-812c-3d537d411c15'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO
  

RAISERROR ('-- Job [-Create EventChannelPartitions] ', 0, 1) WITH NOWAIT
GO

/****** Object:  Job [-Create EventChannelPartitions]    Script Date: 16/02/2017 17:31:05 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
DECLARE @dbname nvarchar(100) = '$(DB_NAME)'
DECLARE @jobname nvarchar(100) = @dbname+' - Create EventChannelPartitions'
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 16/02/2017 17:31:05 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@jobname, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create Event Channel Partitons]    Script Date: 16/02/2017 17:31:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Event Channel Partitons', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @D date 

SELECT Top 1  @D = CAST(value as date)  FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name =''PF_EventChannelValue_Date''
ORDER BY value DESC

While 1=1
BEGIN
	IF @D > DATEADD(day,7,SYSDATETIME()) BREAK;
	
	ALTER PARTITION SCHEME [PS_EventChannelValue_Date]
	NEXT USED [PRIMARY]

	ALTER PARTITION FUNCTION [PF_EventChannelValue_Date]()
	SPLIT RANGE (CONVERT(VARCHAR(8), @D, 112))
	
	SET @D = DATEADD(day,1,@D)
END
', 
		@database_name=N'$(DB_NAME)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 2am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170216, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'43513943-dc84-479b-812c-3d537d411c15'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
USE [$(db_name)]
GO
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update job'
           ,1
           ,'update_spectrum_jobs_db_001.sql'
           ,'1.1.01')
GO