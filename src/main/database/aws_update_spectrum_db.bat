@echo on
@REM setlocal
SET SQLSERVER=sqlserverexfree.cic7e4eytzqz.eu-west-1.rds.amazonaws.com,1433
SET login=sa
for /f "delims=" %%i in ('powershell -file getpwd.ps1') do set passwd=%%i
echo.

@if "%1" == "" GOTO dbdefault ELSE GOTO setdbdata

:setdbdata
@echo setdbdata - %1
@SET DB_NAME=%1 
@GOTO dbstart


:dbdefault
set DB_NAME="EUROMAINT_Spectrum"
set DB_NAME_ADMINISTRATION="SR_Administration"

:dbstart
REM sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i create_spectrum_db.sql -d master% -E
:script001
sqlcmd -S %SQLSERVER% -U %login% -P %passwd% -v db_name=%DB_NAME% -i update_spectrum_db_001.sql -d %DB_NAME%
@if %ERRORLEVEL% GEQ 1 GOTO generalerror
:script002
sqlcmd -S %SQLSERVER% -U %login% -P %passwd% -v db_name=%DB_NAME% -i update_spectrum_db_002.sql -d %DB_NAME%
@if %ERRORLEVEL% GEQ 1 GOTO generalerror
:script003
sqlcmd -S %SQLSERVER% -U %login% -P %passwd% -v db_name=%DB_NAME% -i update_spectrum_db_003.sql -d %DB_NAME%
@if %ERRORLEVEL% GEQ 1 GOTO generalerror
:script004
sqlcmd -S %SQLSERVER% -U %login% -P %passwd% -v db_name=%DB_NAME% -i update_spectrum_db_004.sql -d %DB_NAME%
@if %ERRORLEVEL% GEQ 1 GOTO generalerror
:script005
sqlcmd -S %SQLSERVER% -U %login% -P %passwd% -v db_name=%DB_NAME% -i update_spectrum_db_005.sql -d %DB_NAME%
@if %ERRORLEVEL% GEQ 1 GOTO   generalerror


:CurrentVersion
@pause
@GOTO end

:generalerror
@echo ****************************
@echo **ERROR: Stopped execution**
@echo ****************************
@Pause
@GOTO end

:end