/*
 * If the current content of the administration database needs to be cleaned
 * The ADMINISTRATOR user/role should not be deleted
 */
--DELETE FROM [Permission]

--DELETE FROM [UserRole]
--    WHERE UserID != (SELECT UserID FROM [User] WHERE UPPER(Username) = 'ADMINISTRATOR')
--    OR RoleID != (SELECT RoleID FROM [Role] WHERE UPPER(RoleName) = 'ADMINISTRATOR')

--DELETE FROM [Resource]

--DELETE FROM [Application]

--DELETE FROM [Role]
--    WHERE RoleName <> 'ADMINISTRATOR'

--DELETE FROM [User]
--    WHERE Username <> 'ADMINISTRATOR'
/***************************************************************/

DECLARE @administratorID AS int
DECLARE @spectrumRoleID AS int
DECLARE @powerSpectrumRoleID AS int

/* validates the ADMINISTRATOR user */
IF NOT EXISTS(SELECT * FROM [User] WHERE UPPER([UserName]) = 'ADMINISTRATOR')
	RAISERROR('Administration DB update ERROR: The user ADMINISTRATOR does not exist', 20, 1) WITH LOG

SET @administratorID = (SELECT [UserID] FROM [User] WHERE UPPER([UserName]) = 'ADMINISTRATOR')

-- Only add the SPECTRUM role if it doesnt already exist
IF NOT EXISTS (SELECT * FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM')
BEGIN
    INSERT INTO [Role] ([RoleName], [Description], [Priority], [Enabled], [SystemRole]) VALUES ('SPECTRUM', 'Access to Spectrum Application', 2, 1, 1);
    PRINT 'SPECTRUM role created'
END
ELSE
BEGIN
    PRINT 'SPECTRUM role already exist'
END

SET @spectrumRoleID = (SELECT [RoleID] FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM')

--Only add the Administrator User into the SPECTRUM role if it doesnt already exist
IF NOT EXISTS (SELECT * FROM [UserRole] WHERE [UserID] = @administratorID AND [RoleID] = @spectrumRoleID)
BEGIN
    INSERT into UserRole ([UserID], [RoleID]) VALUES (@administratorID, @spectrumRoleID);
    PRINT 'Administrator user inserted in SPECTRUM role'
END
ELSE
BEGIN
    PRINT 'Administrator User already belongs to SPECTRUM role'
END

-- Only add the SPECTRUM Power User role if it doesnt already exist
IF NOT EXISTS (SELECT * FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM POWER USER')
BEGIN
    INSERT INTO [Role] ([RoleName], [Description], [Priority], [Enabled], [SystemRole]) VALUES ('SPECTRUM Power User', 'Power User of SPECTRUM', 3, 1, 0);
    PRINT 'SPECTRUM Power User role created'
END
ELSE
BEGIN
    PRINT 'SPECTRUM Power User role already exist'
END

SET @powerSpectrumRoleID = (SELECT [RoleID] FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM POWER USER')

--Only add the Administrator User into the SPECTRUM Role if it doesnt already exist
IF NOT EXISTS (SELECT * FROM [UserRole] WHERE [UserID] = @administratorID AND [RoleID] = @powerSpectrumRoleID)
BEGIN
    INSERT into UserRole ([UserID], [RoleID]) VALUES (@administratorID, @powerSpectrumRoleID);
    PRINT 'Administrator user inserted in SPECTRUM Power User role'
END
ELSE
BEGIN
    PRINT 'Administrator User already belongs to SPECTRUM Power User role'
END

-- Only add Spectrum application if it doesnt already exist
IF NOT EXISTS (SELECT * FROM [Application] WHERE UPPER([ApplicationName]) = 'SPECTRUM')
BEGIN
    INSERT INTO [Application] ([ApplicationName],[Description])
	VALUES ('SPECTRUM', 'Spectrum Application')
	
	PRINT 'Spectrum application created'
END
ELSE
BEGIN
	PRINT 'Spectrum application already exist'
END
GO

-- ensure all the changes to Resource table are cascaded to Permission table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Resource_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
	ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Resource_Permission]
GO

ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Permission] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Resource_Permission]
GO

/* gets the ADMINISTRATOR user id */
DECLARE @userID int = (SELECT [UserID] FROM [User] WHERE UPPER([UserName]) = 'ADMINISTRATOR')

/* gets the SPECTRUM application id */
DECLARE @applicationID int = (SELECT [ApplicationID] FROM [Application] WHERE UPPER([ApplicationName]) = 'SPECTRUM')

/* gets the SPECTRUM POWER USER role id */
DECLARE @roleID int = (SELECT [RoleID] FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM POWER USER')

/* update feature resources */
DECLARE @SummaryOfChanges TABLE(
	Change VARCHAR(20)
	, FeatureKey VARCHAR(100)
);

;MERGE Resource AS r 
USING (
	-- Administration:
	SELECT Type = 'F', FeatureKey = 'spectrum.administration', FeatureDescription = 'Administration'
	UNION ALL SELECT 'F', 'spectrum.administration.channeldefinition'	, 'Administration - Channel Definition'
	UNION ALL SELECT 'F', 'spectrum.administration.emailtemplates'		, 'Administration - Email Template Editor'
	UNION ALL SELECT 'F', 'spectrum.administration.faultconfig'			, 'Administration - Fault Configuration Editor'
	UNION ALL SELECT 'F', 'spectrum.administration.faulttest'			, 'Administration - Fault Test'
	UNION ALL SELECT 'F', 'spectrum.administration.packageupload'		, 'Administration - Package Upload'
--	UNION ALL SELECT 'F', 'spectrum.administration.recoverieseditor'	, 'Administration - Recoveries Editor'
	UNION ALL SELECT 'F', 'spectrum.administration.repobackup'			, 'Administration - Repository'
	UNION ALL SELECT 'F', 'spectrum.administration.ruleseditor'			, 'Administration - Fault Rules Editor'
	UNION ALL SELECT 'F', 'spectrum.administration.users'				, 'Administration - Users'

	-- Fleet Summary
	UNION ALL SELECT 'F', 'spectrum.fleetsummary'						, 'Fleet Summary'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.unitnumber'			, 'Fleet Summary (Unit 1)'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.lastupdatetime'		, 'Fleet Summary (Last Update Time)'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.gpscoordinates'		, 'Fleet Summary (GPS Coordinates)'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.speed'					, 'Fleet Summary (Speed)'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.livefaultscount'		, 'Fleet Summary (Live Faults)'
	UNION ALL SELECT 'F', 'spectrum.fleetsummary.livewarningscount'		, 'Fleet Summary (Live Warnings)'

	-- Unit Summary
	UNION ALL SELECT 'F', 'spectrum.unitsummary'						, 'Unit Summary'
	UNION ALL SELECT 'F', 'spectrum.unitsummary.detail'					, 'Unit Summary (Detail)'
	UNION ALL SELECT 'F', 'spectrum.unitsummary.channeldata'			, 'Unit Summary (Channel Data)'
	UNION ALL SELECT 'F', 'spectrum.unitsummary.dataplots'				, 'Unit Summary (Data Plots)'
	UNION ALL SELECT 'F', 'spectrum.unitsummary.location'				, 'Unit Summary (Location)'
	UNION ALL SELECT 'F', 'spectrum.unitsummary.cabview'				, 'Unit Summary (Cab View)'
	UNION ALL SELECT 'F', 'spectrum.event.panel'						, 'Events Panel'
	
	-- Fleet Location
	UNION ALL SELECT 'F', 'spectrum.fleetlocation'						, 'Fleet Location'

	-- Event History
	UNION ALL SELECT 'F', 'spectrum.eventhistory'						, 'Event History'

) AS nr
ON r.FeatureKey = nr.FeatureKey
	AND r.Type = 'F'
WHEN MATCHED THEN 
UPDATE SET
	Type = nr.Type
	, FeatureDescription = nr.FeatureDescription
WHEN NOT MATCHED THEN
INSERT (
	ApplicationID
	, Type
	, FeatureKey
	, FeatureDescription)
VALUES(
	@applicationID
	, nr.Type
	, nr.FeatureKey
	, nr.FeatureDescription)
WHEN NOT MATCHED BY SOURCE AND r.Type = 'F' THEN
DELETE
OUTPUT $action, ISNULL(inserted.FeatureKey, deleted.FeatureKey) 
INTO @SummaryOfChanges;

-- add newly added licences to Power User role
INSERT INTO Permission (
	ResourceID
	, RoleID
	, PermissionType)
SELECT 
	r.ResourceID
	, @roleID
	, 'READ'
FROM @SummaryOfChanges soc
INNER JOIN Resource r ON r.FeatureKey = soc.FeatureKey
WHERE soc.Change = 'INSERT'

 
/* update data resources */
;MERGE Resource AS r 
USING (
	-- Unit:
	SELECT Type = 'D', DataName = 'Unit', DataField = 'spectrum.data.unitnumber'

) AS nr
ON r.DataField = nr.DataField
	AND r.Type = 'D'
WHEN MATCHED THEN 
UPDATE SET
	Type = nr.Type
	, DataName = nr.DataName
WHEN NOT MATCHED THEN
INSERT (
	ApplicationID
	, Type
	, DataName
	, DataField)
VALUES(
	@applicationID
	, nr.Type
	, nr.DataName
	, nr.DataField)
WHEN NOT MATCHED BY SOURCE AND r.Type = 'D' THEN
DELETE
;
