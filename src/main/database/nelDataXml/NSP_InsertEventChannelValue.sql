/****** Object:  StoredProcedure [dbo].[NSP_InsertEventChannelValue]    Script Date: 01/02/2017 16:20:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[NSP_InsertEventChannelValue] (
    @Timestamp DATETIME2(3)
    , @TimestampEndTime DATETIME2(3)  = NULL
    , @UnitId INT
    , @ChannelId INT
	, @Value bit
    )
AS
/******************************************************************************
**  Name:           NSP_InsertEventChannelValue
**  Description:    Inserts a new event-based channel value for the SLT fleet
**  Call frequency: Called by the telemetry service when the event happens. 
**                  The event is considered started if a start time (timeStamp) only is given.
**                  The event is considered finished when a end date is given.
**                  
**  Parameters:     @timestamp the timestamp when the event happens
**                  @timestamp the timestamp when the event happens
**                  @UnitId the Unit ID
**                  @ChannelId the channel ID
******************************************************************************/
BEGIN
	DECLARE @SqlStr varchar(max)
	DECLARE @colRef varchar(10)

	-- Insert a record into EventChannelValue
    INSERT INTO dbo.EventChannelValue (TimeStamp, TimestampEndTime, UnitID, ChannelId, Value)
    VALUES (@timestamp, @TimestampEndTime, @UnitId, @ChannelId, @Value)

	IF not @TimestampEndTime is null
		SET @timestamp = @TimestampEndTime-- for the latest value I'm expect that TimestampEndTime will always have the latest datetime

	SELECT @colRef = ref
	FROM dbo.Channel
	WHERE id = @ChannelId

	-- Update EventChannelLatestValue
    IF EXISTS (SELECT top 1 1 FROM dbo.EventChannelLatestValue WHERE UnitID = @UnitId)
		SET @SqlStr = '
			UPDATE dbo.EventChannelLatestValue 
			SET TimeLastUpdated = ''' + CAST(@timestamp AS VARCHAR(25)) + ''',
				' + @colRef + ' = ' + CAST(@value AS VARCHAR(5)) + ' 
			WHERE UnitID = ' + CAST(@UnitId AS VARCHAR(5)) + '
		'
    ELSE
	    SET @SqlStr = '
	        INSERT INTO dbo.EventChannelLatestValue(TimeLastUpdated, UnitID, ' + @colRef + ')  
		    VALUES (''' + CAST(@timestamp AS VARCHAR(25)) + ''', ' + CAST(@UnitId AS VARCHAR(5)) + ', ' + CAST(@value AS VARCHAR(5)) + ')
	    '
    EXEC(@SqlStr)
END

