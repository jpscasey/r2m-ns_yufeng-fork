SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 001
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO
------------------------------------------------------------
-- update script
------------------------------------------------------------

-------------------------------------------------------------------------------
RAISERROR ('-- Create SchemaChangeLog', 0, 1) WITH NOWAIT


IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = 'SchemaChangeLog')
BEGIN
    CREATE TABLE [dbo].[SchemaChangeLog](
	    [ID] [int] IDENTITY(1,1) NOT NULL,
	    [ScriptType] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
	    [ScriptNumber] [int] NOT NULL,
	    [ScriptName] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
	    [DateApplied] [datetime2](0) NOT NULL,
	    [ApplicationVersion] [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
	CONSTRAINT [PK_SchemaChangeLog] PRIMARY KEY CLUSTERED 
    (
	    [ID] ASC
    )WITH (FILLFACTOR = 100, PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY];
END
ELSE
BEGIN
    TRUNCATE TABLE [SchemaChangeLog]
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints dc WHERE dc.name = 'DF_SchemaChangeLog_DateApplied' )
    ALTER TABLE [dbo].[SchemaChangeLog] ADD  CONSTRAINT [DF_SchemaChangeLog_DateApplied]  DEFAULT (SYSDATETIME()) FOR [DateApplied]
GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database create'
           ,0
           ,'create_spectrum_db.sql'
           ,'1.0.01')
GO



-------------------------------------------------------------------------------
RAISERROR ('-- Drop all stored procedures', 0, 1) WITH NOWAIT

DECLARE @objName varchar(100)

WHILE EXISTS(
	SELECT * 
	FROM INFORMATION_SCHEMA.ROUTINES 
	WHERE ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME NOT LIKE 'sp_MS%'
)
BEGIN
	SET @objName = (
		SELECT TOP 1 ROUTINE_NAME
		FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_TYPE = 'PROCEDURE'
			AND ROUTINE_NAME NOT LIKE 'sp_MS%'
	)
	
	PRINT 'Stored procedure name: ' + ISNULL(@objName,'NULL')
	IF @objName IS NOT NULL
		EXEC('DROP PROCEDURE ' + @objName)

END

-------------------------------------------------------------------------------
RAISERROR ('-- Drop all user defined functions', 0, 1) WITH NOWAIT

WHILE EXISTS(
	SELECT * 
	FROM INFORMATION_SCHEMA.ROUTINES 
	WHERE ROUTINE_TYPE = 'FUNCTION'
)
BEGIN
	SET @objName = (
		SELECT TOP 1 ROUTINE_NAME
		FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_TYPE = 'FUNCTION'
	)
	
	PRINT 'User defined function name: ' + ISNULL(@objName,'NULL')
	IF @objName IS NOT NULL
		EXEC('DROP FUNCTION ' + @objName)

END


-------------------------------------------------------------------------------
RAISERROR ('-- Drop all views', 0, 1) WITH NOWAIT

WHILE EXISTS(
	SELECT * 
	FROM INFORMATION_SCHEMA.VIEWS
)
BEGIN
	SET @objName = (
		SELECT TOP 1 TABLE_NAME
		FROM INFORMATION_SCHEMA.VIEWS 
	)
	
	PRINT 'View name: ' + ISNULL(@objName,'NULL')
	IF @objName IS NOT NULL
		EXEC('DROP VIEW ' + @objName)

END

-------------------------------------------------------------------------------
RAISERROR ('-- Drop all FKs', 0, 1) WITH NOWAIT

WHILE(EXISTS(
	SELECT 1 
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE CONSTRAINT_TYPE='FOREIGN KEY')
)
BEGIN

	DECLARE @sql nvarchar(2000)

	SET @sql = (SELECT TOP 1 
		'ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME + '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']'
	FROM information_schema.table_constraints
	WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'
	)

	IF @sql IS NOT NULL
		EXEC (@sql)
	
END

GO
-------------------------------------------------------------------------------
RAISERROR ('-- Rename tables', 0, 1) WITH NOWAIT

DECLARE @sql NVARCHAR(MAX) = '';

--SELECT * FROM INFORMATION_SCHEMA.TABLES t

SELECT @sql = @sql + 'DROP TABLE ''' + TABLE_NAME + ''+ '''; ' 
FROM INFORMATION_SCHEMA.TABLES 
WHERE INFORMATION_SCHEMA.TABLES.TABLE_NAME LIKE 'old_%'
ORDER BY TABLE_NAME

--SELECT * FROM INFORMATION_SCHEMA.TABLES t

-- generate statements:
SELECT @sql = @sql + 'EXEC sp_rename ''' + TABLE_SCHEMA + '.' + TABLE_NAME + ''', ''old_' + TABLE_NAME + '''; '--, * 
FROM INFORMATION_SCHEMA.TABLES 
WHERE INFORMATION_SCHEMA.TABLES.TABLE_NAME NOT IN ('SchemaChangeLog', 'dtproperties')
    AND INFORMATION_SCHEMA.TABLES.TABLE_NAME NOT LIKE 'old_%'        
ORDER BY TABLE_NAME

--SELECT @sql

EXEC (@sql)
GO

-------------------------------------------------------------------------------
RAISERROR ('-- Rename Primary Keys', 0, 1) WITH NOWAIT

-- generate statements:
DECLARE @sql NVARCHAR(MAX) = '';


SELECT @sql = @sql + 'EXEC SP_Rename ''' + TABLE_SCHEMA + '.' + CONSTRAINT_NAME + ''', ''old_' + CONSTRAINT_NAME + ''';'
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE TABLE_NAME LIKE 'old%' AND CONSTRAINT_NAME NOT LIKE 'old%'
ORDER BY TABLE_NAME

--SELECT @sql

EXEC (@sql)
GO


-------------------------------------------------------------------------------
RAISERROR ('-- Rename Default Constraints ', 0, 1) WITH NOWAIT

-- generate statements:
DECLARE @sql NVARCHAR(MAX) = '';

SELECT @sql = @sql + 'EXEC SP_Rename ''' + OBJECT_SCHEMA_NAME(p.id) + '.' + o.Name + ''', ''old_' + o.Name + ''', ''OBJECT'';'--, * 
FROM sysobjects o
INNER JOIN sysobjects p ON p.ID = o.parent_obj
WHERE o.xtype = 'D' 
	AND p.Name LIKE 'old_%'
	AND o.Name NOT LIKE 'old_%'

--SELECT @sql

EXEC (@sql)
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,001
           ,'update_repository_db_001.sql'
           ,'1.0.01')
GO