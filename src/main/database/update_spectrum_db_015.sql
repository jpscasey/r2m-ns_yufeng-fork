SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 015
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Updating NSP_HistoricChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelData AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelData]
	@DateTimeEnd datetime
	, @Interval int -- number of seconds
	, @UnitID int
	, @SelectColList varchar(4000)
	, @N int = NULL --record number
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql varchar(max)

	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	DECLARE @DateTimeStart datetime = DATEADD(s, - @Interval, @DateTimeEnd)
	
	SET @sql = '
SELECT TOP (' + CAST(ISNULL(@N, 30000) AS varchar(10)) +  ')
	TimeStamp = CAST(cv.Timestamp AS datetime)
	, UnitID = ' + CAST(@UnitID AS varchar(10)) + '
	, ' + @SelectColList + '
FROM ChannelValue cv
WHERE 
	cv.UnitID = ' + CAST(@UnitID AS varchar(10)) + '
	AND cv.Timestamp BETWEEN ''' + CONVERT(varchar(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(varchar(23), @DateTimeEnd, 121)  + '''
ORDER BY 1 ASC'

	EXEC (@sql)
	
END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_HistoricChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_HistoricChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelValue AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_HistoricChannelValue
**	Description:	Returns latest record for each of the vehicles for UnitID
**					in parameter. Stored procedure searches for last record in 
**					last 24h before timestamp passed as parameter 
**	Call frequency:	Every 5s 
**	Parameters:		@DateTimeStart datetime - if NULL set to GETDATE() + 1h 
**						to return last live record for unit
**					@UnitID int 
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_HistoricChannelValue]
	@DateTimeStart datetime
	, @UnitID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @setcode varchar(50)
	DECLARE @headcode varchar(50)
	
	IF @UnitID IS NULL
	BEGIN
		PRINT 'Unit Number not in a database'
		RETURN -1 
	END


	IF @DateTimeStart IS NULL
		SET @DateTimeStart = DATEADD(hh, 1, GETDATE())

	-- Get totals for Vehicle based faults
	DECLARE @currentVehicleFault TABLE(
		FaultUnitID int NULL
		, FaultNumber int NULL
		, WarningNumber int NULL
	)
		
	INSERT INTO @currentVehicleFault
	SELECT 
		FaultUnitID
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
	FROM VW_IX_FaultLive WITH (NOEXPAND)
	WHERE FaultUnitID  = @UnitID
		AND Category <> 'Test'
		AND ReportingOnly = 0
	GROUP BY FaultUnitID

	;WITH CteLatestRecords 
	AS
	(
		SELECT 
			Vehicle.ID AS VehicleID
			, (
				SELECT TOP 1 TimeStamp
				FROM ChannelValue
				WHERE TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
					AND UnitID =	Vehicle.UnitID 
				ORDER BY TimeStamp DESC
			) AS MaxChannelValueTimeStamp
		FROM Vehicle
		WHERE UnitID = @UnitID
	)	

	SELECT 
		UnitNumber	= v.UnitNumber
		, UnitID	= v.UnitID
		, Headcode	= fs.Headcode
		, Diagram	= fs.Diagram
		, VehicleID		= v.ID
		, VehicleType	= v.Type
		, VehicleTypeID	= v.VehicleTypeID
		, VehicleNumber	= v.VehicleNumber
		, FaultNumber	= ISNULL(vf.FaultNumber,0)
		, WarningNumber = ISNULL(vf.WarningNumber,0)
		, Location		= (
			SELECT TOP 1 
				LocationCode -- CRS
			FROM Location
			INNER JOIN LocationArea ON LocationID = Location.ID
			WHERE col3 BETWEEN MinLatitude AND MaxLatitude
				AND col4 BETWEEN MinLongitude AND MaxLongitude
				AND LocationArea.Type = 'C'
			ORDER BY Priority
		)
		,cv.[UpdateRecord]
		,cv.[RecordInsert]
		,fs.FleetCode
		,TimeStamp		= CAST(cv.[TimeStamp] AS datetime),

        -- Channel Data	
		    [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]


	FROM VW_Vehicle v 
	INNER JOIN CteLatestRecords		ON v.ID = CteLatestRecords.VehicleID
	LEFT JOIN ChannelValue cv		ON v.UnitID = cv.UnitID  AND CteLatestRecords.MaxChannelValueTimeStamp = cv.TimeStamp
	LEFT JOIN @currentVehicleFault vf ON v.UnitID = vf.FaultUnitID
	LEFT JOIN VW_FleetStatus fs		ON v.ID = fs.VehicleID

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_FleetSummaryDrilldown', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetSummaryDrilldown' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummaryDrilldown AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_FleetSummaryDrilldown
**	Description:	Returns live channel data for the application
**	Call frequency:	When user click on grouped cell on Fleet Summary
**	Parameters:		None
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
	@UnitIdList varchar(30)
	, @SelectColList varchar(4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @sql varchar(max)
	
	SET @sql = '
SELECT DISTINCT
	cv.UnitID
	, ' + @SelectColList + '
	, fs.UnitPosition
FROM VW_FleetStatus fs
INNER JOIN VW_UnitLastChannelValueTimestamp vlc ON vlc.UnitID = fs.UnitID
LEFT JOIN ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
WHERE fs.UnitID in (' + @UnitIdList + ')
ORDER BY 
	fs.UnitPosition'

	EXEC (@sql)
	
	

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueByID AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
	@ID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM ChannelValue), 0) - 1;
	ELSE
		SET @ID = (
		SELECT TOP 1 (ChannelValue.ID - 1) 
		FROM ChannelValue WITH(NOLOCK)
		WHERE ChannelValue.ID > @ID
		ORDER BY ChannelValue.ID ASC
		)
		
	DECLARE @dataSetSize int = 5000
	
	SELECT 
		cv.ID
		, UnitID		= u.ID
		, UnitNumber	= u.UnitNumber
		, UnitType		= u.UnitType
		, Headcode		= fs.Headcode
		, FleetCode		= fl.Code
		, ServiceStatus	= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, TimeStamp			= CONVERT(datetime, cv.TimeStamp)
		, LocationID	= l.ID
		, Tiploc		= l.Tiploc
		, LocationName	= l.LocationName ,

		-- Channel Data	
		    [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]

	FROM ChannelValue cv WITH(INDEX(PK_ChannelValue))
	INNER JOIN Unit u ON cv.UnitID = u.ID
	LEFT JOIN Fleet fl ON u.FleetID = fl.ID
	LEFT JOIN FleetStatusHistory fs ON
		cv.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
	LEFT JOIN Location l ON l.ID = (
		SELECT TOP 1 LocationID 
		FROM LocationArea 
		WHERE col3 BETWEEN MinLatitude AND MaxLatitude
			AND col4 BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
	ORDER BY ID

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValue AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_GetFaultChannelValue
**	Description:	Returns data to fault engine
**	Call frequency:	Every 5s 
**	Parameters:		none
**	Return values:	0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue]
	@DatetimeStart datetime
	, @DatetimeEnd datetime
	, @UnitID int = NULL
	, @UpdateRecord bit = NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		cv.UnitID
		, Headcode = CAST(NULL AS varchar(7))
		, u.UnitNumber
		, u.UnitType
		, ServiceStatus	= CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = Location.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		, [TimeStamp]			= CONVERT(datetime, cv.TimeStamp)
		, Location.ID AS LocationID
		, Location.Tiploc AS Tiploc
		, Location.LocationName AS LocationName
		,
		-- Channel Data	
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM ChannelValue cv WITH (NOLOCK)
	INNER JOIN Unit u ON cv.UnitID = u.ID
	LEFT JOIN FleetStatus fs ON
		cv.TimeStamp > ValidFrom 
		AND cv.TimeStamp <= ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
		AND cv.UnitID = fs.UnitID
	LEFT JOIN Location ON Location.ID = (
										SELECT TOP 1 LocationID 
										FROM LocationArea 
										WHERE col1 BETWEEN MinLatitude AND MaxLatitude
											AND col2 BETWEEN MinLongitude AND MaxLongitude
										ORDER BY Priority
									)
	WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
		AND (cv.UnitID = @UnitID OR @UnitID IS NULL)
		AND (cv.UpdateRecord = @UpdateRecord OR @UpdateRecord IS NULL)
	ORDER BY TimeStamp

END

GO

------------------------------------------------------------
RAISERROR ('-- Updating NSP_InsertFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**	Name:			NSP_InsertFault
**	Description:	Creates a fault. Inserts data into Fault and 
**					FaultChannelValue tables
**	Call frequency:	Called by fault engine
**	Parameters:		@FaultCode varchar(100)
**					@TimeCreate datetime
**					@TimeEnd datetime = NULL
**					@IsDelayed bit = 0
**	Return values:	ID of Fault inserted, else
**					-1	Duplicate Fault
**					-2	Error when inserting into Fault table
**					-3	There is a Live fault overlapping with the fault
**					-4	FaultCode does not exist
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author$$
**	$$Date$$
**	$$Revision$$
**	$$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
	DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)

    IF (SELECT COUNT(*) FROM FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
			@faultMetaID = ID 
			, @faultMetaRecovery = RecoveryProcessPath
        FROM FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
		, @faultLocationID = l.ID
        FROM ChannelValue
		LEFT JOIN Location l ON l.ID = (
			SELECT TOP 1 LocationID 
			FROM LocationArea 
			WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
				AND Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
            
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO [Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[IsDelayed]
			,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,ISNULL(@IsDelayed, 0)
            ,RecoveryStatus = CASE
				WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
				ELSE NULL
			END

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
	
	--insert Channel data
	INSERT INTO [FaultChannelValue]
	(
	        [ID]
        ,[FaultID]
        ,[FaultUnit]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	)
	SELECT
		[ID]
		,@faultID
		,CASE UnitID
			WHEN @FaultUnitID THEN 1
			ELSE 0
		END -- [FaultVehicle]
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		  ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399]
	FROM ChannelValue 
    WHERE UnitID = @FaultUnitID
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


	-- returning faultID
	RETURN @faultID
	
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,015
           ,'update_spectrum_db_015.sql'
           ,'1.0.01')
GO