SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 016
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Create PerformanceTestData', 0, 1) WITH NOWAIT
GO

CREATE TABLE PerformanceTestData (
    ID int IDENTITY NOT NULL,
    Timestamp datetime2 NOT NULL,
    UnitID int NOT NULL,
    col1 decimal(9,3) NULL,
    col2 decimal(9,3) NULL,
    col3 decimal(9,3) NULL,
    col4 decimal(9,3) NULL,
    col5 decimal(9,3) NULL,
    col6 decimal(9,3) NULL,
    col7 decimal(9,3) NULL,
    col8 decimal(9,3) NULL,
    col9 decimal(9,3) NULL,
    col10 decimal(9,3) NULL,
    col11 decimal(9,3) NULL,
    col12 decimal(9,3) NULL,
    col13 decimal(9,3) NULL,
    col14 decimal(9,3) NULL,
    col15 decimal(9,3) NULL,
    col16 decimal(9,3) NULL,
    col17 decimal(9,3) NULL,
    col18 decimal(9,3) NULL,
    col19 decimal(9,3) NULL,
    col20 decimal(9,3) NULL,
    col21 decimal(9,3) NULL,
    col22 decimal(9,3) NULL,
    col23 decimal(9,3) NULL,
    col24 decimal(9,3) NULL,
    col25 decimal(9,3) NULL,
    col26 decimal(9,3) NULL,
    col27 decimal(9,3) NULL,
    col28 decimal(9,3) NULL,
    col29 decimal(9,3) NULL,
    col30 decimal(9,3) NULL,
    col31 decimal(9,3) NULL,
    col32 decimal(9,3) NULL,
    col33 decimal(9,3) NULL,
    col34 decimal(9,3) NULL,
    col35 decimal(9,3) NULL,
    col36 decimal(9,3) NULL,
    col37 decimal(9,3) NULL,
    col38 decimal(9,3) NULL,
    col39 decimal(9,3) NULL,
    col40 decimal(9,3) NULL,
    col41 decimal(9,3) NULL,
    col42 decimal(9,3) NULL,
    col43 decimal(9,3) NULL,
    col44 decimal(9,3) NULL,
    col45 decimal(9,3) NULL,
    col46 decimal(9,3) NULL,
    col47 decimal(9,3) NULL,
    col48 decimal(9,3) NULL,
    col49 decimal(9,3) NULL,
    col50 decimal(9,3) NULL,
    col51 decimal(9,3) NULL,
    col52 decimal(9,3) NULL,
    col53 decimal(9,3) NULL,
    col54 decimal(9,3) NULL,
    col55 decimal(9,3) NULL,
    col56 decimal(9,3) NULL,
    col57 decimal(9,3) NULL,
    col58 decimal(9,3) NULL,
    col59 decimal(9,3) NULL,
    col60 decimal(9,3) NULL,
    col61 decimal(9,3) NULL,
    col62 decimal(9,3) NULL,
    col63 decimal(9,3) NULL,
    col64 decimal(9,3) NULL,
    col65 decimal(9,3) NULL,
    col66 decimal(9,3) NULL,
    col67 decimal(9,3) NULL,
    col68 decimal(9,3) NULL,
    col69 decimal(9,3) NULL,
    col70 decimal(9,3) NULL,
    col71 decimal(9,3) NULL,
    col72 decimal(9,3) NULL,
    col73 decimal(9,3) NULL,
    col74 decimal(9,3) NULL,
    col75 decimal(9,3) NULL,
    col76 decimal(9,3) NULL,
    col77 decimal(9,3) NULL,
    col78 decimal(9,3) NULL,
    col79 decimal(9,3) NULL,
    col80 decimal(9,3) NULL,
    col81 decimal(9,3) NULL,
    col82 decimal(9,3) NULL,
    col83 decimal(9,3) NULL,
    col84 decimal(9,3) NULL,
    col85 decimal(9,3) NULL,
    col86 decimal(9,3) NULL,
    col87 decimal(9,3) NULL,
    col88 decimal(9,3) NULL,
    col89 decimal(9,3) NULL,
    col90 decimal(9,3) NULL,
    col91 decimal(9,3) NULL,
    col92 decimal(9,3) NULL,
    col93 decimal(9,3) NULL,
    col94 decimal(9,3) NULL,
    col95 decimal(9,3) NULL,
    col96 decimal(9,3) NULL,
    col97 decimal(9,3) NULL,
    col98 decimal(9,3) NULL,
    col99 decimal(9,3) NULL,
    col100 decimal(9,3) NULL
)


GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,016
           ,'update_spectrum_db_016.sql'
           ,'1.0.01')
GO