SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 018
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


------------------------------------------------------------
RAISERROR ('-- Updating NSP_GetPerfTestChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetPerfTestChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.NSP_GetPerfTestChannelValueByID AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetPerfTestChannelValueByID]
(
	@ID bigint
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM PerformanceTestData), 0) - 1;
	ELSE
		SET @ID = (
		SELECT TOP 1 (PerformanceTestData.ID - 1) 
		FROM PerformanceTestData WITH(NOLOCK)
		WHERE PerformanceTestData.ID > @ID
		ORDER BY PerformanceTestData.ID ASC
		)
		
	DECLARE @dataSetSize int = 5000
	
	SELECT 
		cv.ID
		, UnitID		= u.ID
		, UnitNumber	= u.UnitNumber
		, UnitType		= u.UnitType
		, Headcode		= ''
		, FleetCode		= fl.Code
		, ServiceStatus	= ''
		, TimeStamp		= CONVERT(datetime, cv.TimeStamp)
		, LocationID	= ''
		, Tiploc		= ''
		, LocationName	= '' ,

		-- Channel Data	
		    [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100]

	FROM PerformanceTestData cv
	INNER JOIN Unit u ON cv.UnitID = u.ID
	LEFT JOIN Fleet fl ON u.FleetID = fl.ID
	WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
	ORDER BY ID

END

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,018
           ,'update_spectrum_db_018.sql'
           ,'1.0.01')
GO