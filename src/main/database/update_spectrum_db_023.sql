SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 023
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


------------------------------------------------------------
RAISERROR ('-- Creating NSP_GetCurrentFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
	EXEC ('CREATE PROCEDURE dbo.[NSP_GetCurrentFault] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFault]
    @N int
    , @isLive bit
	, @isAcknowledged bit
AS
BEGIN
    SET NOCOUNT ON;

	SELECT TOP (@N)

			f.ID    
			, CreateTime    
			, HeadCode  
			, SetCode   
			, FaultCode   
			, LocationCode  
			, IsCurrent 
			, EndTime   
			, RecoveryID    
			, Latitude  
			, Longitude 
			, FaultMetaID   
			, FaultUnitID    
			, FaultUnitNumber
			, PrimaryUnitNumber     = up.UnitNumber
			, SecondaryUnitNumber   = us.UnitNumber
			, TertiaryUnitNumber    = ut.UnitNumber
			, IsDelayed 
			, Description   
			, Summary   
			, FaultType
			, FaultTypeColor
			, Category
			, CategoryID
			, ReportingOnly
			, RecoveryStatus
			, FleetCode
			, HasRecovery
			, IsAcknowledged
	FROM dbo.VW_IX_Fault f
	LEFT JOIN dbo.Location ON f.LocationID = Location.ID
	LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
	LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
	LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
	WHERE (@isLive IS NULL OR f.IsCurrent = @isLive )
	AND (@isAcknowledged IS NULL OR f.IsAcknowledged = @isAcknowledged)
	ORDER BY CreateTime DESC

END 

GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,023
           ,'update_spectrum_db_023.sql'
           ,'1.1.01')
GO