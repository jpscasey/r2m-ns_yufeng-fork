SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 036
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelValue] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_GetFaultChannelValue
**  Description:    Returns data to fault engine
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue]
    @DatetimeStart datetime
    , @DatetimeEnd datetime
    , @UnitID int = NULL
    , @UpdateRecord bit = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        cv.UnitID
        , Headcode = CAST(NULL AS varchar(7))
        , u.UnitNumber
        , u.UnitType
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = Location.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , [TimeStamp]           = CONVERT(datetime, cv.TimeStamp)
        , Location.ID AS LocationID
        , Location.Tiploc AS Tiploc
        , Location.LocationName AS LocationName
        ,
        -- Channel Values
        [Col1],[Col2],[Col3],[Col4],[Col5],[Col6],[Col7],[Col8],[Col9],[Col10],
        [Col11],[Col12],[Col13],[Col14],[Col15],[Col16],[Col17],[Col18],[Col19],[Col20],
        [Col21],[Col22],[Col23],[Col24],[Col25],[Col26],[Col27],[Col28],[Col29],[Col30],
        [Col31],[Col32],[Col33],[Col34],[Col35],[Col36],[Col37],[Col38],[Col39],[Col40],
        [Col41],[Col42],[Col43],[Col44],[Col45],[Col46],[Col47],[Col48],[Col49],[Col50],
        [Col51],[Col52],[Col53],[Col54],[Col55],[Col56],[Col57],[Col58],[Col59],[Col60],
        [Col61],[Col62],[Col63],[Col64],[Col65],[Col66],[Col67],[Col68],[Col69],[Col70],
        [Col71],[Col72],[Col73],[Col74],[Col75],[Col76],[Col77],[Col78],[Col79],[Col80],
        [Col81],[Col82],[Col83],[Col84],[Col85],[Col86],[Col87],[Col88],[Col89],[Col90],
        [Col91],[Col92],[Col93],[Col94],[Col95],[Col96],[Col97],[Col98],[Col99],[Col100],
            [Col101],[Col102],[Col103],[Col104],[Col105],[Col106],[Col107],[Col108],[Col109],[Col110],
            [Col111],[Col112],[Col113],[Col114],[Col115],[Col116],[Col117],[Col118],[Col119],[Col120],
            [Col121],[Col122],[Col123],[Col124],[Col125],[Col126],[Col127],[Col128],[Col129],[Col130],
            [Col131],[Col132],[Col133],[Col134],[Col135],[Col136],[Col137],[Col138],[Col139],[Col140],
            [Col141],[Col142],[Col143],[Col144],[Col145],[Col146],[Col147],[Col148],[Col149],[Col150],
            [Col151],[Col152],[Col153],[Col154],[Col155],[Col156],[Col157],[Col158],[Col159],[Col160],
            [Col161],[Col162],[Col163],[Col164],[Col165],[Col166],[Col167],[Col168],[Col169],[Col170],
            [Col171],[Col172],[Col173],[Col174],[Col175],[Col176],[Col177],[Col178],[Col179],[Col180],
            [Col181],[Col182],[Col183],[Col184],[Col185],[Col186],[Col187],[Col188],[Col189],[Col190],
            [Col191],[Col192],[Col193],[Col194],[Col195],[Col196],[Col197],[Col198],[Col199],[Col200],
        [Col201],[Col202],[Col203],[Col204],[Col205],[Col206],[Col207],[Col208],[Col209],[Col210],
        [Col211],[Col212],[Col213],[Col214],[Col215],[Col216],[Col217],[Col218],[Col219],[Col220],
        [Col221],[Col222],[Col223],[Col224],[Col225],[Col226],[Col227],[Col228],[Col229],[Col230],
        [Col231],[Col232],[Col233],[Col234],[Col235],[Col236],[Col237],[Col238],[Col239],[Col240],
        [Col241],[Col242],[Col243],[Col244],[Col245],[Col246],[Col247],[Col248],[Col249],[Col250],
        [Col251],[Col252],[Col253],[Col254],[Col255],[Col256],[Col257],[Col258],[Col259],[Col260],
        [Col261],[Col262],[Col263],[Col264],[Col265],[Col266],[Col267],[Col268],[Col269],[Col270],
        [Col271],[Col272],[Col273],[Col274],[Col275],[Col276],[Col277],[Col278],[Col279],[Col280],
        [Col281],[Col282],[Col283],[Col284],[Col285],[Col286],[Col287],[Col288],[Col289],[Col290],
        [Col291],[Col292],[Col293],[Col294],[Col295],[Col296],[Col297],[Col298],[Col299],[Col300],
            [Col301],[Col302],[Col303],[Col304],[Col305],[Col306],[Col307],[Col308],[Col309],[Col310],
            [Col311],[Col312],[Col313],[Col314],[Col315],[Col316],[Col317],[Col318],[Col319],[Col320],
            [Col321],[Col322],[Col323],[Col324],[Col325],[Col326],[Col327],[Col328],[Col329],[Col330],
            [Col331],[Col332],[Col333],[Col334],[Col335],[Col336],[Col337],[Col338],[Col339],[Col340],
            [Col341],[Col342],[Col343],[Col344],[Col345],[Col346],[Col347],[Col348],[Col349],[Col350],
            [Col351],[Col352],[Col353],[Col354],[Col355],[Col356],[Col357],[Col358],[Col359],[Col360],
            [Col361],[Col362],[Col363],[Col364],[Col365],[Col366],[Col367],[Col368],[Col369],[Col370],
            [Col371],[Col372],[Col373],[Col374],[Col375],[Col376],[Col377],[Col378],[Col379],[Col380],
            [Col381],[Col382],[Col383],[Col384],[Col385],[Col386],[Col387],[Col388],[Col389],[Col390],
            [Col391],[Col392],[Col393],[Col394],[Col395],[Col396],[Col397],[Col398],[Col399],[Col400],
        [Col401],[Col402],[Col403],[Col404],[Col405],[Col406],[Col407],[Col408],[Col409],[Col410],
        [Col411],[Col412],[Col413],[Col414],[Col415],[Col416],[Col417],[Col418],[Col419],[Col420],
        [Col421],[Col422],[Col423],[Col424],[Col425],[Col426],[Col427],[Col428],[Col429],[Col430],
        [Col431],[Col432],[Col433],[Col434],[Col435],[Col436],[Col437],[Col438],[Col439],[Col440],
        [Col441],[Col442],[Col443],[Col444],[Col445],[Col446],[Col447],[Col448],[Col449],[Col450],
        [Col451],[Col452],[Col453],[Col454],[Col455],[Col456],[Col457],[Col458],[Col459],[Col460],
        [Col461],[Col462],[Col463],[Col464],[Col465],[Col466],[Col467],[Col468],[Col469],[Col470],
        [Col471],[Col472],[Col473],[Col474],[Col475],[Col476],[Col477],[Col478],[Col479],[Col480],
        [Col481],[Col482],[Col483],[Col484],[Col485],[Col486],[Col487],[Col488],[Col489],[Col490],
        [Col491],[Col492],[Col493],[Col494],[Col495],[Col496],[Col497],[Col498],[Col499],[Col500],
            [Col501],[Col502],[Col503],[Col504],[Col505],[Col506],[Col507],[Col508],[Col509],[Col510],
            [Col511],[Col512],[Col513],[Col514],[Col515],[Col516],[Col517],[Col518],[Col519],[Col520],
            [Col521],[Col522],[Col523],[Col524],[Col525],[Col526],[Col527],[Col528],[Col529],[Col530],
            [Col531],[Col532],[Col533],[Col534],[Col535],[Col536],[Col537],[Col538],[Col539],[Col540],
            [Col541],[Col542],[Col543],[Col544],[Col545],[Col546],[Col547],[Col548],[Col549],[Col550],
            [Col551],[Col552],[Col553],[Col554],[Col555],[Col556],[Col557],[Col558],[Col559],[Col560],
            [Col561],[Col562],[Col563],[Col564],[Col565],[Col566],[Col567],[Col568],[Col569],[Col570],
            [Col571],[Col572],[Col573],[Col574],[Col575],[Col576],[Col577],[Col578],[Col579],[Col580],
            [Col581],[Col582],[Col583],[Col584],[Col585],[Col586],[Col587],[Col588],[Col589],[Col590],
            [Col591],[Col592],[Col593],[Col594],[Col595],[Col596],[Col597],[Col598],[Col599],[Col600],
        [Col601],[Col602],[Col603],[Col604],[Col605],[Col606],[Col607],[Col608],[Col609],[Col610],
        [Col611],[Col612],[Col613],[Col614],[Col615],[Col616],[Col617],[Col618],[Col619],[Col620],
        [Col621],[Col622],[Col623],[Col624],[Col625],[Col626],[Col627],[Col628],[Col629],[Col630],
        [Col631],[Col632],[Col633],[Col634],[Col635],[Col636],[Col637],[Col638],[Col639],[Col640],
        [Col641],[Col642],[Col643],[Col644],[Col645],[Col646],[Col647],[Col648],[Col649],[Col650],
        [Col651],[Col652],[Col653],[Col654],[Col655],[Col656],[Col657],[Col658],[Col659],[Col660],
        [Col661],[Col662],[Col663]
    FROM ChannelValue cv WITH (NOLOCK)
    INNER JOIN Unit u ON cv.UnitID = u.ID
    LEFT JOIN FleetStatus fs ON
        cv.TimeStamp > ValidFrom 
        AND cv.TimeStamp <= ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
        AND cv.UnitID = fs.UnitID
    LEFT JOIN Location ON Location.ID = (
                                        SELECT TOP 1 LocationID 
                                        FROM LocationArea 
                                        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
                                            AND col2 BETWEEN MinLongitude AND MaxLongitude
                                        ORDER BY Priority
                                    )
    WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
        AND (cv.UnitID = @UnitID OR @UnitID IS NULL)
        AND (cv.UpdateRecord = @UpdateRecord OR @UpdateRecord IS NULL)
    ORDER BY TimeStamp

END
GO

RAISERROR ('-- update NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelValue] AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
    @ID bigint
)
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM ChannelValue), 0) - 1;
    ELSE
        SET @ID = (
        SELECT TOP 1 (ChannelValue.ID - 1) 
        FROM ChannelValue WITH(NOLOCK)
        WHERE ChannelValue.ID > @ID
        ORDER BY ChannelValue.ID ASC
        )
        
    DECLARE @dataSetSize int = 5000
    
    SELECT 
        cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime, cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,

        -- Channel Values
        [Col1],[Col2],[Col3],[Col4],[Col5],[Col6],[Col7],[Col8],[Col9],[Col10],
        [Col11],[Col12],[Col13],[Col14],[Col15],[Col16],[Col17],[Col18],[Col19],[Col20],
        [Col21],[Col22],[Col23],[Col24],[Col25],[Col26],[Col27],[Col28],[Col29],[Col30],
        [Col31],[Col32],[Col33],[Col34],[Col35],[Col36],[Col37],[Col38],[Col39],[Col40],
        [Col41],[Col42],[Col43],[Col44],[Col45],[Col46],[Col47],[Col48],[Col49],[Col50],
        [Col51],[Col52],[Col53],[Col54],[Col55],[Col56],[Col57],[Col58],[Col59],[Col60],
        [Col61],[Col62],[Col63],[Col64],[Col65],[Col66],[Col67],[Col68],[Col69],[Col70],
        [Col71],[Col72],[Col73],[Col74],[Col75],[Col76],[Col77],[Col78],[Col79],[Col80],
        [Col81],[Col82],[Col83],[Col84],[Col85],[Col86],[Col87],[Col88],[Col89],[Col90],
        [Col91],[Col92],[Col93],[Col94],[Col95],[Col96],[Col97],[Col98],[Col99],[Col100],
            [Col101],[Col102],[Col103],[Col104],[Col105],[Col106],[Col107],[Col108],[Col109],[Col110],
            [Col111],[Col112],[Col113],[Col114],[Col115],[Col116],[Col117],[Col118],[Col119],[Col120],
            [Col121],[Col122],[Col123],[Col124],[Col125],[Col126],[Col127],[Col128],[Col129],[Col130],
            [Col131],[Col132],[Col133],[Col134],[Col135],[Col136],[Col137],[Col138],[Col139],[Col140],
            [Col141],[Col142],[Col143],[Col144],[Col145],[Col146],[Col147],[Col148],[Col149],[Col150],
            [Col151],[Col152],[Col153],[Col154],[Col155],[Col156],[Col157],[Col158],[Col159],[Col160],
            [Col161],[Col162],[Col163],[Col164],[Col165],[Col166],[Col167],[Col168],[Col169],[Col170],
            [Col171],[Col172],[Col173],[Col174],[Col175],[Col176],[Col177],[Col178],[Col179],[Col180],
            [Col181],[Col182],[Col183],[Col184],[Col185],[Col186],[Col187],[Col188],[Col189],[Col190],
            [Col191],[Col192],[Col193],[Col194],[Col195],[Col196],[Col197],[Col198],[Col199],[Col200],
        [Col201],[Col202],[Col203],[Col204],[Col205],[Col206],[Col207],[Col208],[Col209],[Col210],
        [Col211],[Col212],[Col213],[Col214],[Col215],[Col216],[Col217],[Col218],[Col219],[Col220],
        [Col221],[Col222],[Col223],[Col224],[Col225],[Col226],[Col227],[Col228],[Col229],[Col230],
        [Col231],[Col232],[Col233],[Col234],[Col235],[Col236],[Col237],[Col238],[Col239],[Col240],
        [Col241],[Col242],[Col243],[Col244],[Col245],[Col246],[Col247],[Col248],[Col249],[Col250],
        [Col251],[Col252],[Col253],[Col254],[Col255],[Col256],[Col257],[Col258],[Col259],[Col260],
        [Col261],[Col262],[Col263],[Col264],[Col265],[Col266],[Col267],[Col268],[Col269],[Col270],
        [Col271],[Col272],[Col273],[Col274],[Col275],[Col276],[Col277],[Col278],[Col279],[Col280],
        [Col281],[Col282],[Col283],[Col284],[Col285],[Col286],[Col287],[Col288],[Col289],[Col290],
        [Col291],[Col292],[Col293],[Col294],[Col295],[Col296],[Col297],[Col298],[Col299],[Col300],
            [Col301],[Col302],[Col303],[Col304],[Col305],[Col306],[Col307],[Col308],[Col309],[Col310],
            [Col311],[Col312],[Col313],[Col314],[Col315],[Col316],[Col317],[Col318],[Col319],[Col320],
            [Col321],[Col322],[Col323],[Col324],[Col325],[Col326],[Col327],[Col328],[Col329],[Col330],
            [Col331],[Col332],[Col333],[Col334],[Col335],[Col336],[Col337],[Col338],[Col339],[Col340],
            [Col341],[Col342],[Col343],[Col344],[Col345],[Col346],[Col347],[Col348],[Col349],[Col350],
            [Col351],[Col352],[Col353],[Col354],[Col355],[Col356],[Col357],[Col358],[Col359],[Col360],
            [Col361],[Col362],[Col363],[Col364],[Col365],[Col366],[Col367],[Col368],[Col369],[Col370],
            [Col371],[Col372],[Col373],[Col374],[Col375],[Col376],[Col377],[Col378],[Col379],[Col380],
            [Col381],[Col382],[Col383],[Col384],[Col385],[Col386],[Col387],[Col388],[Col389],[Col390],
            [Col391],[Col392],[Col393],[Col394],[Col395],[Col396],[Col397],[Col398],[Col399],[Col400],
        [Col401],[Col402],[Col403],[Col404],[Col405],[Col406],[Col407],[Col408],[Col409],[Col410],
        [Col411],[Col412],[Col413],[Col414],[Col415],[Col416],[Col417],[Col418],[Col419],[Col420],
        [Col421],[Col422],[Col423],[Col424],[Col425],[Col426],[Col427],[Col428],[Col429],[Col430],
        [Col431],[Col432],[Col433],[Col434],[Col435],[Col436],[Col437],[Col438],[Col439],[Col440],
        [Col441],[Col442],[Col443],[Col444],[Col445],[Col446],[Col447],[Col448],[Col449],[Col450],
        [Col451],[Col452],[Col453],[Col454],[Col455],[Col456],[Col457],[Col458],[Col459],[Col460],
        [Col461],[Col462],[Col463],[Col464],[Col465],[Col466],[Col467],[Col468],[Col469],[Col470],
        [Col471],[Col472],[Col473],[Col474],[Col475],[Col476],[Col477],[Col478],[Col479],[Col480],
        [Col481],[Col482],[Col483],[Col484],[Col485],[Col486],[Col487],[Col488],[Col489],[Col490],
        [Col491],[Col492],[Col493],[Col494],[Col495],[Col496],[Col497],[Col498],[Col499],[Col500],
            [Col501],[Col502],[Col503],[Col504],[Col505],[Col506],[Col507],[Col508],[Col509],[Col510],
            [Col511],[Col512],[Col513],[Col514],[Col515],[Col516],[Col517],[Col518],[Col519],[Col520],
            [Col521],[Col522],[Col523],[Col524],[Col525],[Col526],[Col527],[Col528],[Col529],[Col530],
            [Col531],[Col532],[Col533],[Col534],[Col535],[Col536],[Col537],[Col538],[Col539],[Col540],
            [Col541],[Col542],[Col543],[Col544],[Col545],[Col546],[Col547],[Col548],[Col549],[Col550],
            [Col551],[Col552],[Col553],[Col554],[Col555],[Col556],[Col557],[Col558],[Col559],[Col560],
            [Col561],[Col562],[Col563],[Col564],[Col565],[Col566],[Col567],[Col568],[Col569],[Col570],
            [Col571],[Col572],[Col573],[Col574],[Col575],[Col576],[Col577],[Col578],[Col579],[Col580],
            [Col581],[Col582],[Col583],[Col584],[Col585],[Col586],[Col587],[Col588],[Col589],[Col590],
            [Col591],[Col592],[Col593],[Col594],[Col595],[Col596],[Col597],[Col598],[Col599],[Col600],
        [Col601],[Col602],[Col603],[Col604],[Col605],[Col606],[Col607],[Col608],[Col609],[Col610],
        [Col611],[Col612],[Col613],[Col614],[Col615],[Col616],[Col617],[Col618],[Col619],[Col620],
        [Col621],[Col622],[Col623],[Col624],[Col625],[Col626],[Col627],[Col628],[Col629],[Col630],
        [Col631],[Col632],[Col633],[Col634],[Col635],[Col636],[Col637],[Col638],[Col639],[Col640],
        [Col641],[Col642],[Col643],[Col644],[Col645],[Col646],[Col647],[Col648],[Col649],[Col650],
        [Col651],[Col652],[Col653],[Col654],[Col655],[Col656],[Col657],[Col658],[Col659],[Col660],
        [Col661],[Col662],[Col663]

    FROM ChannelValue cv WITH(INDEX(PK_ChannelValue))
    INNER JOIN Unit u ON cv.UnitID = u.ID
    LEFT JOIN Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM LocationArea 
        WHERE col3 BETWEEN MinLatitude AND MaxLatitude
            AND col4 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
    WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
    ORDER BY ID

END
GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,036
           ,'update_spectrum_db_036.sql'
           ,'1.1.02')
GO