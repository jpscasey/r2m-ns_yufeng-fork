SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 039
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------
RAISERROR ('-- update NSP_GetFaultEventChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValue] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValue]
    @DatetimeStart DATETIME
    ,@DatetimeEnd DATETIME
    ,@UnitId INT = NULL
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT ecv.ID
    ,ecv.UnitID
    ,ecv.ChannelID
    ,ecv.TimeStamp
    ,ecv.Value
    FROM dbo.EventChannelValue ecv
    WHERE ecv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
    AND (ecv.UnitID = @UnitId OR @UnitId IS NULL)
END
GO
--
--
--
RAISERROR ('-- update NSP_GetFaultEventChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultEventChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultEventChannelValueByID] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueByID]
    @ID int
AS
BEGIN
	SET NOCOUNT ON;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM EventChannelValue), 0) - 1
    ELSE
        SET @ID = (
        SELECT TOP 1 (EventChannelValue.ID - 1) 
        FROM dbo.EventChannelValue WITH(NOLOCK)
        WHERE EventChannelValue.ID > @ID
        ORDER BY EventChannelValue.ID ASC
        )

    SELECT ecv.ID
    ,ecv.UnitID
    ,ecv.ChannelID
    ,ecv.TimeStamp
    ,ecv.Value
    FROM dbo.EventChannelValue ecv
    WHERE ecv.ID BETWEEN (@ID + 1) AND (@ID + 10000)
END
GO
--
--
--
RAISERROR ('-- update NSP_RuleEngineGetCurrentIds', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RuleEngineGetCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RuleEngineGetCurrentIds] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_RuleEngineGetCurrentIds
**  Description:    Returns data to fault engine
**  Call frequency: Every 5s 
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_RuleEngineGetCurrentIds]
    @ConfigurationName varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Value FROM dbo.RuleEngineCurrentId WHERE ConfigurationName = @ConfigurationName ORDER BY Position asc

END
GO
--
--
--
RAISERROR ('-- update NSP_RuleEngineUpdateCurrentIds', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_RuleEngineUpdateCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_RuleEngineUpdateCurrentIds] AS BEGIN RETURN(1) END;')
GO

/******************************************************************************
**  Name:           NSP_RuleEngineUpdateCurrentIds
**  Description:    Returns data to fault engine
**  Call frequency: Every 5s
**  Parameters:     none
**  Return values:  0 if successful, else an error is raised
*******************************************************************************
** CVS Properties
*******************************************************************************
**  $$Author$$
**  $$Date$$
**  $$Revision$$
**  $$Source$$
*******************************************************************************/

ALTER PROCEDURE [dbo].[NSP_RuleEngineUpdateCurrentIds]
    @ConfigurationName varchar(100),
    @Value1 bigint = NULL,
    @Value2 bigint = NULL

AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @LastUpdateTime datetime;
    SET @LastUpdateTime = GETDATE()

    ;MERGE RuleEngineCurrentId AS r
    USING (

        SELECT Position = '1', ConfigurationName = @ConfigurationName, Value = @Value1, LastUpdateTime = @LastUpdateTime
        UNION
        SELECT Position = '2', ConfigurationName = @ConfigurationName, Value = @Value2, LastUpdateTime = @LastUpdateTime

    ) AS nr
    ON r.Position = nr.Position
        AND r.ConfigurationName = nr.ConfigurationName
    WHEN MATCHED THEN
    UPDATE SET
        Value = nr.Value,
        LastUpdateTime = nr.LastUpdateTime
    WHEN NOT MATCHED THEN
    INSERT (
        ConfigurationName
        , Position
        , Value
        , LastUpdateTime)
    VALUES(
        nr.ConfigurationName
        , nr.Position
        , nr.Value
        , nr.LastUpdateTime);

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,039
           ,'update_spectrum_db_039.sql'
           ,'1.1.02')
GO