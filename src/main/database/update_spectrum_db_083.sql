SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 083
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


ALTER VIEW [dbo].[VW_IX_FaultLive] WITH SCHEMABINDING
    AS
        SELECT 
        f.ID    
        , f.CreateTime    
        , f.HeadCode  
        , f.SetCode   
        , fm.FaultCode   
        , f.LocationID 
        , f.IsCurrent 
        , f.EndTime   
        , f.RecoveryID    
        , f.Latitude  
        , f.Longitude 
        , f.FaultMetaID   
        , f.FaultUnitID
        , FaultUnitNumber       = uf.UnitNumber 
        , f.PrimaryUnitID
        , f.SecondaryUnitID
        , f.TertiaryUnitID
        , f.IsDelayed 
        , fm.Description   
        , fm.Summary   
        , fm.CategoryID
        , fc.Category
        , fc.ReportingOnly
        , fm.FaultTypeID
        , FaultType         = ft.Name
        , FaultTypeColor    = ft.DisplayColor
        , f.IsAcknowledged  
        , f.AcknowledgedTime
        , ft.Priority
        , f.RecoveryStatus
        , FleetCode         = fl.Code
        , fm.AdditionalInfo
        , fm.HasRecovery
        , f.RowVersion
        , f.FaultGroupID
        , f.IsGroupLead
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm     ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.FaultType ft     ON ft.ID = fm.FaultTypeID
    INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
    INNER JOIN dbo.Unit uf          ON uf.ID = f.FaultUnitID
    INNER JOIN dbo.Fleet fl         ON fl.ID = uf.FleetID 
    WHERE IsCurrent = 1
GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultLive_ID] ON [dbo].[VW_IX_FaultLive]
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultLive_CreateTime] ON [dbo].[VW_IX_FaultLive]
(
    [CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER VIEW [dbo].[VW_IX_FaultNotLive] WITH SCHEMABINDING
    AS
        SELECT 
        f.ID    
        , f.CreateTime    
        , f.HeadCode  
        , f.SetCode   
        , fm.FaultCode   
        , f.LocationID 
        , f.IsCurrent 
        , f.EndTime   
        , f.RecoveryID    
        , f.Latitude  
        , f.Longitude 
        , f.FaultMetaID   
        , f.FaultUnitID
        , FaultUnitNumber       = uf.UnitNumber 
        , f.PrimaryUnitID
        , f.SecondaryUnitID
        , f.TertiaryUnitID
        , f.IsDelayed 
        , fm.Description   
        , fm.Summary   
        , fm.CategoryID
        , fc.Category
        , fc.ReportingOnly
        , fm.FaultTypeID
        , FaultType         = ft.Name
        , FaultTypeColor    = ft.DisplayColor
        , f.IsAcknowledged  
        , f.AcknowledgedTime
        , ft.Priority
        , f.RecoveryStatus
        , FleetCode         = fl.Code
        , fm.AdditionalInfo
        , fm.HasRecovery
        , f.RowVersion
        , f.FaultGroupID
        , f.IsGroupLead
        FROM dbo.Fault f
        INNER JOIN dbo.FaultMeta fm     ON f.FaultMetaID = fm.ID
        INNER JOIN dbo.FaultType ft     ON ft.ID = fm.FaultTypeID
        INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
        INNER JOIN dbo.Unit uf          ON uf.ID = f.FaultUnitID
        INNER JOIN dbo.Fleet fl         ON fl.ID = uf.FleetID 
        WHERE IsCurrent = 0
GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultNotLive_ID] ON [dbo].[VW_IX_FaultNotLive]
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultNotLive_CreateTime] ON [dbo].[VW_IX_FaultNotLive]
(
    [CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 

END
GO

------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO
ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
    , @HasRecovery bit = NULL
    , @MaximoID varchar(20) = NULL
    , @HasMaximoSR bit = NULL
)
AS
BEGIN

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , GroupStatus = CASE 
                WHEN f.FaultGroupID IS NOT NULL
                    AND f.IsGroupLead = 1
                    THEN 'Group Lead'
                WHEN f.FaultGroupID IS NOT NULL
                    THEN 'Grouped'
                ELSE 'Not Grouped'
            END
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 

END
GO

------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpsertFaultGroup' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_UpsertFaultGroup AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpsertFaultGroup] (
    @FaultIDs varchar(1000),
    @LeadFaultID int,
    @UserName varchar(255)
)
AS
BEGIN

    DECLARE @FaultIDTable TABLE (FaultID int)
    INSERT INTO @FaultIDTable
    SELECT * from dbo.SplitStrings_CTE(@FaultIDs,',')

    -- If any lead events are being regrouped, we should delete the old group
    DECLARE @FaultGroupsToDeleteTable TABLE (FaultGroupID int)
    INSERT INTO @FaultGroupsToDeleteTable
    SELECT FaultGroupID FROM Fault 
        WHERE ID IN (SELECT FaultID FROM @FaultIDTable)
        AND IsGroupLead = 1

    UPDATE dbo.Fault SET FaultGroupID = NULL, IsGroupLead = NULL WHERE FaultGroupID IN (SELECT FaultGroupID FROM @FaultGroupsToDeleteTable)
    DELETE FROM dbo.FaultGroup WHERE ID IN (SELECT FaultGroupID FROM @FaultGroupsToDeleteTable)

    -- Create the new group
    DECLARE @FaultGroupID int
    INSERT INTO dbo.FaultGroup (Timestamp, UserName) VALUES (SYSDATETIME(), @UserName)
    SELECT @FaultGroupID = SCOPE_IDENTITY()

    UPDATE dbo.Fault SET FaultGroupID = @FaultGroupID, IsGroupLead = 0 WHERE ID IN (SELECT FaultID FROM @FaultIDTable)
    UPDATE dbo.Fault SET IsGroupLead = 1 WHERE ID = @LeadFaultID
END
GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,083
           ,'update_spectrum_db_083.sql'
           ,'1.3.02')
GO