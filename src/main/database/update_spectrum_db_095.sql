SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 095
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update NSP_GetCurrentFaults', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFaults' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetCurrentFaults AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaults]
    @N int
    , @isLive         bit          = NULL   --Live
    , @isAcknowledged bit          = NULL   --Acknowledged
    , @FleetCodes     varchar(100) = NULL   --Fleet
    , @FaultTypeIDs   varchar(100) = NULL   --Severity
    , @CategoryIDs    varchar(100) = NULL   --Category
    , @HasRecoveryOnly    bit      = NULL   --IAS
    , @UnitTypes   	  varchar(100) = NULL   --UnitType
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @FleetTable TABLE (ID varchar(100))
    INSERT INTO @FleetTable
    SELECT * from dbo.SplitStrings_CTE(@FleetCodes,',')

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

    DECLARE @CategoryTable TABLE (ID int)
    INSERT INTO @CategoryTable
    SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')

    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitTypes,',')

    IF @FleetCodes   = '' SET @FleetCodes   = NULL
    IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
    IF @CategoryIDs  = '' SET @CategoryIDs  = NULL
    IF @UnitTypes    = '' SET @UnitTypes    = NULL

    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , IsAcknowledged
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM 
        dbo.VW_IX_Fault f
        LEFT JOIN dbo.Location ON f.LocationID = Location.ID
        LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
        LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
        LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE 
            (f.IsCurrent      = @isLive                             OR @isLive         IS NULL)
        AND (f.IsAcknowledged = @isAcknowledged                     OR @isAcknowledged IS NULL)
        AND (f.FaultTypeID    IN (SELECT ID FROM @TypeTable)        OR @FaultTypeIDs   IS NULL)
        AND (f.CategoryID     IN (SELECT ID FROM @CategoryTable)    OR @CategoryIDs    IS NULL)
        AND (f.FleetCode      IN (SELECT ID FROM @FleetTable)       OR @FleetCodes     IS NULL)
        AND (f.HasRecovery    = @HasRecoveryOnly                    OR @HasRecoveryOnly    IS NULL
            OR @HasRecoveryOnly = 0)
        AND (u.UnitType    IN (SELECT ID FROM @UnitTypeTable)    OR @UnitTypes     IS NULL)
        AND ReportingOnly = 0
    ORDER BY 
        CreateTime DESC

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
    , @FaultTypeID int  = NULL  --Severity
    , @CategoryID int   = NULL  --Category
AS

    SET NOCOUNT ON;
    
    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetAllFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetAllFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetAllFaultLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetAllFaultLive]
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , null as LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , null AS MaximoId
        , null AS MaximoServiceRequestStatus
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_SearchFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE (1 = 1) 
            AND CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY CreateTime DESC 

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultCode varchar(100)
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
    , @UnitType varchar(20) = NULL
    , @HasRecovery bit = NULL
    , @MaximoID varchar(20) = NULL
    , @HasMaximoSR bit = NULL
    
)
AS
BEGIN

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultCode = ''  SET @FaultCode = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    IF @UnitType = ''   SET @UnitType = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable)  OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode    LIKE @FaultCode    OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (xref.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    xref.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR xref.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
        ORDER BY CreateTime DESC 

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, f.FleetCode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, Description
		, Summary
		, Category
		, CategoryID
		, CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = xref.ExternalCode
		, MaximoServiceRequestStatus = xreffield.Value
		, IsGroupLead
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
		LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
		LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
	WHERE 
		f.FaultGroupID = @FaultGroupID

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultNotLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
    @N int
    , @FaultTypeID int  = NULL  --Severity
    , @CategoryID int   = NULL  --Category
)
AS
    SET NOCOUNT ON;
    
    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
    @UnitID int
)
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultNotLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
    @N int
    , @UnitID int
)
AS
    SET NOCOUNT ON;

    SELECT TOP (@N)
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnitAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnitAcknowledged AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitAcknowledged](
    @UnitID int
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
        AND IsAcknowledged = 1
    ORDER BY CreateTime DESC
    
END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnitNotAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitNotAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnitNotAcknowledged AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitNotAcknowledged](
    @UnitID int
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = xref.ExternalCode
        , MaximoServiceRequestStatus = xreffield.Value
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
    LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
    LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
        AND IsAcknowledged = 0
    ORDER BY CreateTime DESC
    
END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , fm.AdditionalInfo
            , f.HasRecovery
            , MaximoID = xref.ExternalCode
            , MaximoServiceRequestStatus = xreffield.Value
            , RowVersion
			, f.FaultGroupID
			, f.IsGroupLead
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        -- get external references
        LEFT JOIN dbo.Fault2ExternalReferenceLink ftxl ON f.ID = ftxl.FaultID
        LEFT JOIN dbo.ExternalReference xref ON ftxl.ExternalFaultID = xref.ID
        LEFT JOIN dbo.ExternalReferenceField xreffield ON xref.ID = xreffield.ExternalFaultID AND xreffield.Field = 'Status'
        WHERE f.ID = @FaultID

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,095
           ,'update_spectrum_db_095.sql'
           ,'1.4.02')
GO