SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 098
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update NSP_FaultIsCurrentUpdate', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FaultIsCurrentUpdate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo') 	
	EXEC ('CREATE PROCEDURE dbo.NSP_FaultIsCurrentUpdate AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE NSP_FaultIsCurrentUpdate
(
@FaultID int
,@IsCurrent bit
,@Username varchar(255)
,@Version varbinary(8)
,@deactivateTime datetime
)
AS
BEGIN 

DECLARE @rowcount int
	
UPDATE Fault set IsCurrent = @IsCurrent, EndTime = @deactivateTime, LastUpdateTime = GETDATE() where ID = @FaultID AND RowVersion = @Version

SELECT
   @rowcount = @@ROWCOUNT
   
IF @rowcount = 1
INSERT INTO FaultStatusHistory(FaultID,UserName,IsCurrent,timestamp) VALUES(@FaultID,@Username,@IsCurrent,@deactivateTime)


SELECT @rowcount
END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,098
           ,'update_spectrum_db_098.sql'
           ,'1.4.02')
GO