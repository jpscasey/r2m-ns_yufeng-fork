SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 126
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Add CountSinceLastMaint to VW_IX_Fault', 0, 1) WITH NOWAIT
GO

ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode   
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		, f.FaultGroupID
		, f.IsGroupLead
		, f.RuleName
		, f.ServiceRequestID
		, f.CountSinceLastMaint
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID 	



GO

/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 11/01/2017 13:33:46 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 11/01/2017 13:33:54 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 11/01/2017 13:34:03 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------------------------
RAISERROR ('-- Add CountSinceLastMaint to VW_IX_FaultLive', 0, 1) WITH NOWAIT
GO

ALTER VIEW [dbo].[VW_IX_FaultLive] WITH SCHEMABINDING
    AS
        SELECT 
        f.ID    
        , f.CreateTime    
        , f.HeadCode  
        , f.SetCode   
        , fm.FaultCode   
        , f.LocationID 
        , f.IsCurrent 
        , f.EndTime   
        , f.RecoveryID    
        , f.Latitude  
        , f.Longitude 
        , f.FaultMetaID   
        , f.FaultUnitID
        , FaultUnitNumber       = uf.UnitNumber 
        , f.PrimaryUnitID
        , f.SecondaryUnitID
        , f.TertiaryUnitID
        , f.IsDelayed 
        , fm.Description   
        , fm.Summary   
        , fm.CategoryID
        , fc.Category
        , fc.ReportingOnly
        , fm.FaultTypeID
        , FaultType         = ft.Name
        , FaultTypeColor    = ft.DisplayColor
        , f.IsAcknowledged  
        , f.AcknowledgedTime
        , ft.Priority
        , f.RecoveryStatus
        , FleetCode         = fl.Code
        , fm.AdditionalInfo
        , fm.HasRecovery
        , f.RowVersion
        , f.FaultGroupID
        , f.IsGroupLead
        , f.RuleName
		, f.ServiceRequestID
		, f.CountSinceLastMaint
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm     ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.FaultType ft     ON ft.ID = fm.FaultTypeID
    INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
    INNER JOIN dbo.Unit uf          ON uf.ID = f.FaultUnitID
    INNER JOIN dbo.Fleet fl         ON fl.ID = uf.FleetID 
    WHERE IsCurrent = 1
GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultLive_ID] ON [dbo].[VW_IX_FaultLive]
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultLive_CreateTime] ON [dbo].[VW_IX_FaultLive]
(
    [CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------------------------
RAISERROR ('-- Add CountSinceLastMaint to VW_IX_FaultNotLive', 0, 1) WITH NOWAIT
GO

ALTER VIEW [dbo].[VW_IX_FaultNotLive] WITH SCHEMABINDING
    AS
        SELECT 
        f.ID    
        , f.CreateTime    
        , f.HeadCode  
        , f.SetCode   
        , fm.FaultCode   
        , f.LocationID 
        , f.IsCurrent 
        , f.EndTime   
        , f.RecoveryID    
        , f.Latitude  
        , f.Longitude 
        , f.FaultMetaID   
        , f.FaultUnitID
        , FaultUnitNumber       = uf.UnitNumber 
        , f.PrimaryUnitID
        , f.SecondaryUnitID
        , f.TertiaryUnitID
        , f.IsDelayed 
        , fm.Description   
        , fm.Summary   
        , fm.CategoryID
        , fc.Category
        , fc.ReportingOnly
        , fm.FaultTypeID
        , FaultType         = ft.Name
        , FaultTypeColor    = ft.DisplayColor
        , f.IsAcknowledged  
        , f.AcknowledgedTime
        , ft.Priority
        , f.RecoveryStatus
        , FleetCode         = fl.Code
        , fm.AdditionalInfo
        , fm.HasRecovery
        , f.RowVersion
        , f.FaultGroupID
        , f.IsGroupLead
        , f.RuleName
		, f.ServiceRequestID
		, f.CountSinceLastMaint
        FROM dbo.Fault f
        INNER JOIN dbo.FaultMeta fm     ON f.FaultMetaID = fm.ID
        INNER JOIN dbo.FaultType ft     ON ft.ID = fm.FaultTypeID
        INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
        INNER JOIN dbo.Unit uf          ON uf.ID = f.FaultUnitID
        INNER JOIN dbo.Fleet fl         ON fl.ID = uf.FleetID 
        WHERE IsCurrent = 0

GO

CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_FaultNotLive_ID] ON [dbo].[VW_IX_FaultNotLive]
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VW_IX_FaultNotLive_CreateTime] ON [dbo].[VW_IX_FaultNotLive]
(
    [CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

------------------------------------------------------------

RAISERROR ('-- Update NSP_GetCurrentFaults', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetCurrentFaults' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetCurrentFaults AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaults]
    @N int
    , @isLive         bit          = NULL   --Live
    , @isAcknowledged bit          = NULL   --Acknowledged
    , @FleetCodes     varchar(100) = NULL   --Fleet
    , @FaultTypeIDs   varchar(100) = NULL   --Severity
    , @CategoryIDs    varchar(100) = NULL   --Category
    , @HasRecoveryOnly    bit      = NULL   --IAS
    , @UnitTypes   	  varchar(100) = NULL   --UnitType
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @FleetTable TABLE (ID varchar(100))
    INSERT INTO @FleetTable
    SELECT * from dbo.SplitStrings_CTE(@FleetCodes,',')

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

    DECLARE @CategoryTable TABLE (ID int)
    INSERT INTO @CategoryTable
    SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')

    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitTypes,',')

    IF @FleetCodes   = '' SET @FleetCodes   = NULL
    IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
    IF @CategoryIDs  = '' SET @CategoryIDs  = NULL
    IF @UnitTypes    = '' SET @UnitTypes    = NULL

    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , IsAcknowledged
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, CountSinceLastMaint
    FROM 
        dbo.VW_IX_Fault f
        LEFT JOIN dbo.Location ON f.LocationID = Location.ID
        LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
        LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
        LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE 
            (f.IsCurrent      = @isLive                             OR @isLive         IS NULL)
        AND (f.IsAcknowledged = @isAcknowledged                     OR @isAcknowledged IS NULL)
        AND (f.FaultTypeID    IN (SELECT ID FROM @TypeTable)        OR @FaultTypeIDs   IS NULL)
        AND (f.CategoryID     IN (SELECT ID FROM @CategoryTable)    OR @CategoryIDs    IS NULL)
        AND (f.FleetCode      IN (SELECT ID FROM @FleetTable)       OR @FleetCodes     IS NULL)
        AND (f.HasRecovery    = @HasRecoveryOnly                    OR @HasRecoveryOnly    IS NULL
            OR @HasRecoveryOnly = 0)
        AND (u.UnitType    IN (SELECT ID FROM @UnitTypeTable)    OR @UnitTypes     IS NULL)
        AND ReportingOnly = 0
    ORDER BY 
        f.CreateTime DESC

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
    , @FaultTypeID int  = NULL  --Severity
    , @CategoryID int   = NULL  --Category
AS

    SET NOCOUNT ON;
    
    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, CountSinceLastMaint
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetAllFaultLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetAllFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetAllFaultLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetAllFaultLive]
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , null as LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , null AS MaximoId
        , null AS MaximoServiceRequestStatus
		, null AS CreatedByRule
		, CountSinceLastMaint
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    ORDER BY CreateTime DESC

GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_SearchFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'

    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE (1 = 1) 
            AND f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID  
            , RecoveryStatus  
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE (1 = 1) 
            AND f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'All'
        
       SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE (1 = 1) 
            AND f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description         LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            ) 
        ORDER BY f.CreateTime DESC 

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All
    , @FaultCode varchar(100)
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
    , @UnitType varchar(20) = NULL
    , @HasRecovery bit = NULL
    , @MaximoID varchar(20) = NULL
    , @HasMaximoSR bit = NULL
    , @IsAcknowledged bit = NULL
	, @IsCreatedByRule bit = NULL
    
)
AS
BEGIN

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultCode = ''  SET @FaultCode = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    IF @UnitType = ''   SET @UnitType = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable)  OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
					OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
					OR @IsCreatedByRule = 0 AND sr.CreatedByRule IS NULL
				)
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
				OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
				OR @IsCreatedByRule = 0 AND sr.CreatedByRule IS NULL
			)
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
--          Temporary solution. To be changed if vehicle column will be added to VW_IX_Fault
            AND (SecondaryUnitID        LIKE '%' + @Vehicle + '%'   OR @Vehicle IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode    LIKE @FaultCode    OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
					OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
					OR @IsCreatedByRule = 0 AND sr.CreatedByRule = 0
				)
        ORDER BY CreateTime DESC 

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, f.FleetCode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, f.Description
		, Summary
		, Category
		, CategoryID
		, f.CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = sr.ExternalCode
		, MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, IsGroupLead
		, AdditionalInfo
		, IsAcknowledged
		, RowVersion
		, FaultGroupID
		, f.CountSinceLastMaint
	FROM 
		dbo.VW_IX_Fault f
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
	WHERE 
		f.FaultGroupID = @FaultGroupID

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultNotLive', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLive AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLive]
(
    @N int
    , @FaultTypeID int  = NULL  --Severity
    , @CategoryID int   = NULL  --Category
)
AS
    SET NOCOUNT ON;
    
    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, f.CountSinceLastMaint
    FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnit](
    @UnitID int
)
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultNotLiveUnit', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultNotLiveUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultNotLiveUnit AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultNotLiveUnit]
(
    @N int
    , @UnitID int
)
AS
    SET NOCOUNT ON;

    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
    FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC
    
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnitAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnitAcknowledged AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitAcknowledged](
    @UnitID int
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
        AND IsAcknowledged = 1
    ORDER BY f.CreateTime DESC
    
END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultLiveUnitNotAcknowledged', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultLiveUnitNotAcknowledged' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveUnitNotAcknowledged AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveUnitNotAcknowledged](
    @UnitID int
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, f.CountSinceLastMaint
    FROM VW_IX_FaultLive f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE FaultUnitID = @UnitID
        AND ReportingOnly = 0
        AND IsAcknowledged = 0
    ORDER BY f.CreateTime DESC
    
END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultDetail', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , f.FaultTypeID
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , fm.AdditionalInfo
            , f.HasRecovery
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
            , CreatedByRule
            , RowVersion
            , f.FaultGroupID
            , f.IsGroupLead
            , f.RuleName
            , f.RecoveryStatus
			, f.CountSinceLastMaint
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.ID = @FaultID

END
GO

------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultSince', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultSince AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultSince]
    @since datetime
AS
BEGIN

    SET NOCOUNT ON;
    
    
    SET NOCOUNT ON;
    
    SELECT 
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , MaximoID = sr.ExternalCode
		, f.CountSinceLastMaint
    FROM VW_IX_Fault f WITH (NOEXPAND)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
    LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE f.CreateTime > @since
        AND ReportingOnly = 0
    ORDER BY f.CreateTime DESC

END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,126
           ,'update_spectrum_db_126.sql'
           ,'1.7.01')
GO