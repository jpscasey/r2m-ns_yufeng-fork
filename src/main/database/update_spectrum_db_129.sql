SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 129
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

RAISERROR ('-- Update [NSP_SearchFaultAdvanced]', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.[NSP_SearchFaultAdvanced] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime
    , @DateTo datetime
    , @SearchType varchar(10) --options: Live, NoLive, All
    , @FaultCode varchar(100)
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Location varchar(10) = NULL
    , @UnitType varchar(20) = NULL
    , @HasRecovery bit = NULL
    , @MaximoID varchar(20) = NULL
    , @HasMaximoSR bit = NULL
    , @IsAcknowledged bit = NULL
	, @IsCreatedByRule bit = NULL
    
)
AS
BEGIN

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultCode = ''  SET @FaultCode = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    IF @UnitType = ''   SET @UnitType = NULL
    
        
    IF @SearchType = 'Live'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable)  OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode  LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
					OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
					OR @IsCreatedByRule = 0 AND sr.CreatedByRule IS NULL
				)
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'NoLive'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_FaultNotLive f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode      LIKE @FaultCode     OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
				OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
				OR @IsCreatedByRule = 0 AND sr.CreatedByRule IS NULL
			)
        ORDER BY f.CreateTime DESC 
        
    IF @SearchType = 'All'
        
        SELECT TOP (@N)
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary  
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber 
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
			, CreatedByRule
            , f.FaultGroupID
			, f.IsGroupLead
			, f.RowVersion
			, f.CountSinceLastMaint
        FROM VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN Location ON Location.ID = LocationID
        LEFT JOIN FaultMeta fm ON fm.ID = f.FaultMetaID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber        LIKE @UnitNumber    OR @UnitNumber IS NULL)
            AND (f.CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (f.Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (f.FaultCode    LIKE @FaultCode    OR @FaultCode IS NULL)
            AND (f.FaultTypeID  IN (Select ID from @TypeTable) OR @Type IS NULL)
            AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
            AND (f.IsAcknowledged    = @IsAcknowledged                  OR @IsAcknowledged IS NULL)
            AND (u.UnitType    = @UnitType    					OR @UnitType     IS NULL)
            AND (sr.ExternalCode LIKE '%' + @MaximoID + '%'      OR @MaximoID IS NULL)
            AND (
                    sr.ExternalCode IS NOT NULL AND @HasMaximoSR = 1 
                    OR sr.ExternalCode IS NULL AND @HasMaximoSR = 0 
                    OR @HasMaximoSR IS NULL
                )
			AND (@IsCreatedByRule IS NULL 
					OR @IsCreatedByRule = 1 AND sr.CreatedByRule = 1 
					OR @IsCreatedByRule = 0 AND sr.CreatedByRule = 0
				)
        ORDER BY CreateTime DESC 

END


GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,129
           ,'update_spectrum_db_129.sql'
           ,'1.7.02')
GO

