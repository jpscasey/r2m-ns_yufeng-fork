SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 148
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------
RAISERROR ('WARNING!!!!!!!!', 0, 1) WITH NOWAIT
GO
RAISERROR ('SYNONYM THAT POINT AT VIRM have been added here Please Ensure that they are Correctly SET Up', 0, 1) WITH NOWAIT
GO

RAISERROR ('IF Correction need PLEase run NSP_PopulateOtherFaultCategoryMata After to ensure Fault MEta and Category are the same.', 0, 1) WITH NOWAIT

RAISERROR ('WARNING END!!!!!!!!', 0, 1) WITH NOWAIT
GO
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_InsertFaultCategory_VIRM')
CREATE SYNONYM [dbo].[NSP_InsertFaultCategory_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_InsertFaultCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_InsertFaultMeta_VIRM')
CREATE SYNONYM [dbo].[NSP_InsertFaultMeta_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_InsertFaultMeta]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_UpdateFaultCategory_VIRM]')
CREATE SYNONYM [dbo].[NSP_UpdateFaultCategory_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_UpdateFaultCategory]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_UpdateFaultMeta_VIRM]')
CREATE SYNONYM [dbo].[NSP_UpdateFaultMeta_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_UpdateFaultMeta]
GO

RAISERROR ('Update NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_InsertFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
	@Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)

	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint

	, @GroupID int
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @ID int

	BEGIN TRAN
	
	INSERT dbo.FaultMeta(
		Username
		, FaultCode
		, Description
		, Summary
		, AdditionalInfo
		, Url
		, CategoryID
		, HasRecovery
		, RecoveryProcessPath
		, FaultTypeID
		, GroupID
	)
	SELECT 
		Username	 = @Username
		, FaultCode	 = @FaultCode
		, Description	= @Description 
		, Summary		= @Summary
		, AdditionalInfo = @AdditionalInfo
		, Url			= @Url
		, CategoryID	= @CategoryID
		, HasRecovery	= @HasRecovery
		, RecoveryProcessPath	= @RecoveryProcessPath
		, FaultTypeID	= @FaultTypeID
		, GroupID		= @GroupID

		SET @ID = SCOPE_IDENTITY()

		EXEC [NSP_InsertFaultMeta_VIRM] 
		 @ID 
		, @Username
		, @FaultCode
		, @Description 
		, @Summary
		, @AdditionalInfo

		, @Url
		, @CategoryID
		, @HasRecovery
		, @RecoveryProcessPath
		, @FaultTypeID

		, @GroupID

		SELECT @ID AS InsertedID
	COMMiT
END



GO


RAISERROR ('Update NSP_InsertFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_InsertFaultCategory] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultCategory](
	@Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	
	DECLARE @ID int = ISNULL((
		SELECT MAX(ID) + 1
		FROM dbo.FaultCategory		
	), 1)
	
	IF @ID IS NULL
		RAISERROR('Error when looking for next ID', 13, 1); 
		
	BEGIN TRAN
	BEGIN TRY 
		
		SET IDENTITY_INSERT dbo.FaultCategory ON;
		
		INSERT dbo.FaultCategory(
			ID
			, Category
			, ReportingOnly
		)
		SELECT 
			@ID
			, @Category
			, @ReportingOnly;

		SET IDENTITY_INSERT dbo.FaultCategory OFF;

		EXEC [NSP_InsertFaultCategory_VIRM] @ID, @Category, @ReportingOnly

		COMMIT TRAN

		SELECT @ID AS InsertedID
			
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
		
END

GO

RAISERROR ('Update NSP_UpdateFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_UpdateFaultCategory] AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_UpdateFaultCategory](
	@ID int
	, @Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRAN
	BEGIN TRY

		UPDATE dbo.FaultCategory SET
			Category = @Category
			, ReportingOnly = @ReportingOnly
		WHERE ID = @ID	
		
		EXEC NSP_UpdateFaultCategory_VIRM @ID,@Category,@ReportingOnly

		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
END


GO

RAISERROR ('Update NSP_UpdateFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_UpdateFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta](
	@ID int
	, @Username varchar(50)
	, @FaultCode varchar(100)
	, @Description varchar(100)
	, @Summary varchar(1000)
	, @AdditionalInfo varchar(3700)
	, @Url varchar(500)
	, @CategoryID int
	, @HasRecovery bit
	, @RecoveryProcessPath varchar(100)
	, @FaultTypeID tinyint
	, @GroupID int
)
AS
	BEGIN
		
		SET NOCOUNT ON;

		BEGIN TRAN
		BEGIN TRY

			UPDATE dbo.FaultMeta SET
				Username		= @Username
				, FaultCode	 = @FaultCode
				, Description	= @Description 
				, Summary		= @Summary
				, AdditionalInfo = @AdditionalInfo
				, Url			= @Url
				, CategoryID	= @CategoryID
				, HasRecovery	= @HasRecovery
				, RecoveryProcessPath	= @RecoveryProcessPath
				, FaultTypeID	= @FaultTypeID
				, GroupID		= @GroupID
			WHERE ID = @ID

			EXEC NSP_UpdateFaultMeta_VIRM @ID,@Username , @FaultCode , @Description , @Summary , @AdditionalInfo, @Url , @CategoryID , @HasRecovery , @RecoveryProcessPath,@FaultTypeID , @GroupID

			COMMIT TRAN
		
		END TRY
		BEGIN CATCH

			EXEC dbo.NSP_RethrowError;
			ROLLBACK TRAN;

		END CATCH

	END
GO


RAISERROR ('Update NSP_PopulateOtherFaultCategoryMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_PopulateOtherFaultCategoryMeta' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_PopulateOtherFaultCategoryMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PRocedure NSP_PopulateOtherFaultCategoryMeta
AS

BEGIN

DECLARE @ID int =0
DECLARE @sql varchar(max)

	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql = 'EXEC [dbo].[NSP_InsertFaultCategory_VIRM] ' + cast(ID as varchar(10)) +' , '+Category + ', ' +cast(ReportingOnly as varchar(10)) 
			,@ID = ID
		FROM FaultCategory
		where ID > @ID
		ORDER BY ID

		IF @sql is null BREAK;

		BEGIN TRY
			EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

	SET @ID = 0
	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql= 'EXEC [dbo].[NSP_InsertFaultMeta_VIRM] ' + cast(ID as varchar(10)) +' , '
		+ isnull(Username,'null') + ', ' 
		+isnull(FaultCode,'null') + ', ' 
		+isnull(Description,'null') + ', ' 
		+isnull(Summary,'null') + ', ' 
		+isnull(AdditionalInfo,'null') + ', ' 
		+isnull(Url,'null') + ', ' 
		+isnull(cast(CategoryID as varchar(10)),'null') + ', ' 
		+isnull(cast(HasRecovery as varchar(10)),'null') + ', ' 
		+isnull(RecoveryProcessPath,'null') + ', '
		+isnull(cast(+FaultTypeID as varchar(10)),'null') + ', '
		+isnull(cast(GroupID  as varchar(10)),'null')
		,@ID = ID
		FROM FaultMeta
		where ID > @ID
		ORDER BY ID

		IF @sql is null BREAK;

		BEGIN TRY
		EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

END
GO
EXEC NSP_PopulateOtherFaultCategoryMeta
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,148
           ,'update_spectrum_db_148.sql'
           ,'1.9.01')
GO