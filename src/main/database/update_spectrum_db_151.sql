SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 151
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

---------------------------------------
-- Add new ChannelValue columns
---------------------------------------

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col677')
	ALTER TABLE dbo.ChannelValue ADD Col677 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col678')
	ALTER TABLE dbo.ChannelValue ADD Col678 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col679')
	ALTER TABLE dbo.ChannelValue ADD Col679 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col680')
	ALTER TABLE dbo.ChannelValue ADD Col680 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col681')
	ALTER TABLE dbo.ChannelValue ADD Col681 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col682')
	ALTER TABLE dbo.ChannelValue ADD Col682 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col683')
	ALTER TABLE dbo.ChannelValue ADD Col683 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col684')
	ALTER TABLE dbo.ChannelValue ADD Col684 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col685')
	ALTER TABLE dbo.ChannelValue ADD Col685 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col686')
	ALTER TABLE dbo.ChannelValue ADD Col686 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col687')
	ALTER TABLE dbo.ChannelValue ADD Col687 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col688')
	ALTER TABLE dbo.ChannelValue ADD Col688 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col689')
	ALTER TABLE dbo.ChannelValue ADD Col689 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col690')
	ALTER TABLE dbo.ChannelValue ADD Col690 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col691')
	ALTER TABLE dbo.ChannelValue ADD Col691 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col692')
	ALTER TABLE dbo.ChannelValue ADD Col692 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col693')
	ALTER TABLE dbo.ChannelValue ADD Col693 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col694')
	ALTER TABLE dbo.ChannelValue ADD Col694 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col695')
	ALTER TABLE dbo.ChannelValue ADD Col695 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col696')
	ALTER TABLE dbo.ChannelValue ADD Col696 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col697')
	ALTER TABLE dbo.ChannelValue ADD Col697 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col698')
	ALTER TABLE dbo.ChannelValue ADD Col698 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col699')
	ALTER TABLE dbo.ChannelValue ADD Col699 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col700')
	ALTER TABLE dbo.ChannelValue ADD Col700 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col701')
	ALTER TABLE dbo.ChannelValue ADD Col701 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col702')
	ALTER TABLE dbo.ChannelValue ADD Col702 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col703')
	ALTER TABLE dbo.ChannelValue ADD Col703 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col704')
	ALTER TABLE dbo.ChannelValue ADD Col704 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col705')
	ALTER TABLE dbo.ChannelValue ADD Col705 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col706')
	ALTER TABLE dbo.ChannelValue ADD Col706 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col707')
	ALTER TABLE dbo.ChannelValue ADD Col707 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col708')
	ALTER TABLE dbo.ChannelValue ADD Col708 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col709')
	ALTER TABLE dbo.ChannelValue ADD Col709 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col710')
	ALTER TABLE dbo.ChannelValue ADD Col710 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col711')
	ALTER TABLE dbo.ChannelValue ADD Col711 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col712')
	ALTER TABLE dbo.ChannelValue ADD Col712 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col713')
	ALTER TABLE dbo.ChannelValue ADD Col713 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col714')
	ALTER TABLE dbo.ChannelValue ADD Col714 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'ChannelValue' AND COLUMN_NAME = 'Col715')
	ALTER TABLE dbo.ChannelValue ADD Col715 int;
GO

---------------------------------------
-- Add new FaultChannelValue columns
---------------------------------------

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col677')
	ALTER TABLE dbo.FaultChannelValue ADD Col677 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col678')
	ALTER TABLE dbo.FaultChannelValue ADD Col678 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col679')
	ALTER TABLE dbo.FaultChannelValue ADD Col679 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col680')
	ALTER TABLE dbo.FaultChannelValue ADD Col680 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col681')
	ALTER TABLE dbo.FaultChannelValue ADD Col681 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col682')
	ALTER TABLE dbo.FaultChannelValue ADD Col682 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col683')
	ALTER TABLE dbo.FaultChannelValue ADD Col683 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col684')
	ALTER TABLE dbo.FaultChannelValue ADD Col684 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col685')
	ALTER TABLE dbo.FaultChannelValue ADD Col685 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col686')
	ALTER TABLE dbo.FaultChannelValue ADD Col686 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col687')
	ALTER TABLE dbo.FaultChannelValue ADD Col687 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col688')
	ALTER TABLE dbo.FaultChannelValue ADD Col688 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col689')
	ALTER TABLE dbo.FaultChannelValue ADD Col689 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col690')
	ALTER TABLE dbo.FaultChannelValue ADD Col690 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col691')
	ALTER TABLE dbo.FaultChannelValue ADD Col691 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col692')
	ALTER TABLE dbo.FaultChannelValue ADD Col692 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col693')
	ALTER TABLE dbo.FaultChannelValue ADD Col693 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col694')
	ALTER TABLE dbo.FaultChannelValue ADD Col694 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col695')
	ALTER TABLE dbo.FaultChannelValue ADD Col695 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col696')
	ALTER TABLE dbo.FaultChannelValue ADD Col696 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col697')
	ALTER TABLE dbo.FaultChannelValue ADD Col697 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col698')
	ALTER TABLE dbo.FaultChannelValue ADD Col698 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col699')
	ALTER TABLE dbo.FaultChannelValue ADD Col699 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col700')
	ALTER TABLE dbo.FaultChannelValue ADD Col700 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col701')
	ALTER TABLE dbo.FaultChannelValue ADD Col701 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col702')
	ALTER TABLE dbo.FaultChannelValue ADD Col702 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col703')
	ALTER TABLE dbo.FaultChannelValue ADD Col703 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col704')
	ALTER TABLE dbo.FaultChannelValue ADD Col704 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col705')
	ALTER TABLE dbo.FaultChannelValue ADD Col705 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col706')
	ALTER TABLE dbo.FaultChannelValue ADD Col706 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col707')
	ALTER TABLE dbo.FaultChannelValue ADD Col707 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col708')
	ALTER TABLE dbo.FaultChannelValue ADD Col708 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col709')
	ALTER TABLE dbo.FaultChannelValue ADD Col709 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col710')
	ALTER TABLE dbo.FaultChannelValue ADD Col710 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col711')
	ALTER TABLE dbo.FaultChannelValue ADD Col711 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col712')
	ALTER TABLE dbo.FaultChannelValue ADD Col712 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col713')
	ALTER TABLE dbo.FaultChannelValue ADD Col713 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col714')
	ALTER TABLE dbo.FaultChannelValue ADD Col714 int;
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'FaultChannelValue' AND COLUMN_NAME = 'Col715')
	ALTER TABLE dbo.FaultChannelValue ADD Col715 int;
GO

----------------------------------------------------------------
-- Update NSP_InsertChannelValue with new columns
----------------------------------------------------------------

RAISERROR ('-- Update NSP_InsertChannelValue', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
	    @UpdateRecord bit 
		,@Col1 decimal(9,6) = NULL,@Col2 decimal(9,6) = NULL,@Col3 bit = NULL,@Col4 smallint = NULL,@Col5 bit = NULL,@Col6 bit = NULL,@Col7 bit = NULL
		,@Col8 bit = NULL,@Col9 bit = NULL,@Col10 bit = NULL,@Col11 bit = NULL,@Col12 bit = NULL,@Col13 bit = NULL,@Col14 bit = NULL
		,@Col15 bit = NULL,@Col16 bit = NULL,@Col17 bit = NULL,@Col18 bit = NULL,@Col19 bit = NULL,@Col20 bit = NULL,@Col21 bit = NULL
		,@Col22 bit = NULL,@Col23 bit = NULL,@Col24 bit = NULL,@Col25 bit = NULL,@Col26 bit = NULL,@Col27 bit = NULL,@Col28 bit = NULL
		,@Col29 bit = NULL,@Col30 bit = NULL,@Col31 bit = NULL,@Col32 bit = NULL,@Col33 bit = NULL,@Col34 bit = NULL,@Col35 bit = NULL
		,@Col36 bit = NULL,@Col37 bit = NULL,@Col38 bit = NULL,@Col39 bit = NULL,@Col40 bit = NULL,@Col41 bit = NULL,@Col42 bit = NULL
		,@Col43 bit = NULL,@Col44 bit = NULL,@Col45 bit = NULL,@Col46 bit = NULL,@Col47 bit = NULL,@Col48 bit = NULL,@Col49 bit = NULL
		,@Col50 bit = NULL,@Col51 bit = NULL,@Col52 bit = NULL,@Col53 bit = NULL,@Col54 bit = NULL,@Col55 bit = NULL,@Col56 bit = NULL
		,@Col57 bit = NULL,@Col58 bit = NULL,@Col59 bit = NULL,@Col60 bit = NULL,@Col61 decimal(5,1) = NULL,@Col62 decimal(5,1) = NULL,@Col63 decimal(5,1) = NULL
		,@Col64 decimal(5,1) = NULL,@Col65 decimal(5,1) = NULL,@Col66 bit = NULL,@Col67 bit = NULL,@Col68 bit = NULL,@Col69 bit = NULL,@Col70 bit = NULL
		,@Col71 bit = NULL,@Col72 bit = NULL,@Col73 bit = NULL,@Col74 bit = NULL,@Col75 bit = NULL,@Col76 bit = NULL,@Col77 bit = NULL
		,@Col78 bit = NULL,@Col79 bit = NULL,@Col80 bit = NULL,@Col81 bit = NULL,@Col82 smallint = NULL,@Col83 tinyint = NULL,@Col84 tinyint = NULL
		,@Col85 smallint = NULL,@Col86 smallint = NULL,@Col87 smallint = NULL,@Col88 decimal(5,1) = NULL,@Col89 decimal(5,1) = NULL,@Col90 decimal(5,1) = NULL,@Col91 decimal(5,1) = NULL
		,@Col92 decimal(5,1) = NULL,@Col93 bit = NULL,@Col94 bit = NULL,@Col95 bit = NULL,@Col96 bit = NULL,@Col97 bit = NULL,@Col98 bit = NULL
		,@Col99 bit = NULL,@Col100 bit = NULL,@Col101 bit = NULL,@Col102 bit = NULL,@Col103 bit = NULL,@Col104 bit = NULL,@Col105 bit = NULL
		,@Col106 bit = NULL,@Col107 bit = NULL,@Col108 bit = NULL,@Col109 smallint = NULL,@Col110 tinyint = NULL,@Col111 tinyint = NULL,@Col112 smallint = NULL
		,@Col113 smallint = NULL,@Col114 smallint = NULL,@Col115 decimal(5,1) = NULL,@Col116 decimal(5,1) = NULL,@Col117 bit = NULL,@Col118 bit = NULL,@Col119 bit = NULL
		,@Col120 decimal(5,1) = NULL,@Col121 decimal(5,1) = NULL,@Col122 bit = NULL,@Col123 bit = NULL,@Col124 bit = NULL,@Col125 bit = NULL,@Col126 bit = NULL
		,@Col127 bit = NULL,@Col128 bit = NULL,@Col129 bit = NULL,@Col130 bit = NULL,@Col131 bit = NULL,@Col132 bit = NULL,@Col133 bit = NULL
		,@Col134 bit = NULL,@Col135 bit = NULL,@Col136 bit = NULL,@Col137 bit = NULL,@Col138 bit = NULL,@Col139 bit = NULL,@Col140 bit = NULL
		,@Col141 bit = NULL,@Col142 bit = NULL,@Col143 bit = NULL,@Col144 bit = NULL,@Col145 bit = NULL,@Col146 bit = NULL,@Col147 bit = NULL
		,@Col148 bit = NULL,@Col149 bit = NULL,@Col150 bit = NULL,@Col151 bit = NULL,@Col152 bit = NULL,@Col153 bit = NULL,@Col154 bit = NULL
		,@Col155 bit = NULL,@Col156 bit = NULL,@Col157 bit = NULL,@Col158 bit = NULL,@Col159 bit = NULL,@Col160 bit = NULL,@Col161 bit = NULL
		,@Col162 bit = NULL,@Col163 bit = NULL,@Col164 bit = NULL,@Col165 bit = NULL,@Col166 bit = NULL,@Col167 bit = NULL,@Col168 bit = NULL
		,@Col169 bit = NULL,@Col170 bit = NULL,@Col171 bit = NULL,@Col172 bit = NULL,@Col173 tinyint = NULL,@Col174 bit = NULL,@Col175 bit = NULL
		,@Col176 bit = NULL,@Col177 bit = NULL,@Col178 bit = NULL,@Col179 bit = NULL,@Col180 bit = NULL,@Col181 bit = NULL,@Col182 bit = NULL
		,@Col183 bit = NULL,@Col184 bit = NULL,@Col185 bit = NULL,@Col186 bit = NULL,@Col187 bit = NULL,@Col188 bit = NULL,@Col189 bit = NULL
		,@Col190 bit = NULL,@Col191 bit = NULL,@Col192 bit = NULL,@Col193 bit = NULL,@Col194 bit = NULL,@Col195 bit = NULL,@Col196 bit = NULL
		,@Col197 bit = NULL,@Col198 bit = NULL,@Col199 bit = NULL,@Col200 bit = NULL,@Col201 bit = NULL,@Col202 bit = NULL,@Col203 bit = NULL
		,@Col204 bit = NULL,@Col205 bit = NULL,@Col206 bit = NULL,@Col207 bit = NULL,@Col208 bit = NULL,@Col209 bit = NULL,@Col210 bit = NULL
		,@Col211 bit = NULL,@Col212 bit = NULL,@Col213 bit = NULL,@Col214 bit = NULL,@Col215 bit = NULL,@Col216 bit = NULL,@Col217 bit = NULL
		,@Col218 bit = NULL,@Col219 bit = NULL,@Col220 bit = NULL,@Col221 bit = NULL,@Col222 bit = NULL,@Col223 bit = NULL,@Col224 bit = NULL
		,@Col225 bit = NULL,@Col226 bit = NULL,@Col227 bit = NULL,@Col228 bit = NULL,@Col229 bit = NULL,@Col230 bit = NULL,@Col231 bit = NULL
		,@Col232 bit = NULL,@Col233 bit = NULL,@Col234 bit = NULL,@Col235 bit = NULL,@Col236 bit = NULL,@Col237 bit = NULL,@Col238 bit = NULL
		,@Col239 bit = NULL,@Col240 bit = NULL,@Col241 bit = NULL,@Col242 bit = NULL,@Col243 bit = NULL,@Col244 bit = NULL,@Col245 bit = NULL
		,@Col246 bit = NULL,@Col247 bit = NULL,@Col248 bit = NULL,@Col249 bit = NULL,@Col250 bit = NULL,@Col251 bit = NULL,@Col252 tinyint = NULL
		,@Col253 tinyint = NULL,@Col254 tinyint = NULL,@Col255 tinyint = NULL,@Col256 bit = NULL,@Col257 bit = NULL,@Col258 bit = NULL,@Col259 bit = NULL
		,@Col260 bit = NULL,@Col261 bit = NULL,@Col262 bit = NULL,@Col263 bit = NULL,@Col264 bit = NULL,@Col265 bit = NULL,@Col266 bit = NULL
		,@Col267 bit = NULL,@Col268 bit = NULL,@Col269 bit = NULL,@Col270 bit = NULL,@Col271 bit = NULL,@Col272 bit = NULL,@Col273 bit = NULL
		,@Col274 bit = NULL,@Col275 bit = NULL,@Col276 bit = NULL,@Col277 bit = NULL,@Col278 bit = NULL,@Col279 bit = NULL,@Col280 bit = NULL
		,@Col281 bit = NULL,@Col282 bit = NULL,@Col283 bit = NULL,@Col284 bit = NULL,@Col285 bit = NULL,@Col286 bit = NULL,@Col287 bit = NULL
		,@Col288 bit = NULL,@Col289 bit = NULL,@Col290 bit = NULL,@Col291 bit = NULL,@Col292 bit = NULL,@Col293 bit = NULL,@Col294 bit = NULL
		,@Col295 bit = NULL,@Col296 bit = NULL,@Col297 tinyint = NULL,@Col298 tinyint = NULL,@Col299 smallint = NULL,@Col300 smallint = NULL,@Col301 smallint = NULL
		,@Col302 smallint = NULL,@Col303 smallint = NULL,@Col304 smallint = NULL,@Col305 smallint = NULL,@Col306 smallint = NULL,@Col307 smallint = NULL,@Col308 smallint = NULL
		,@Col309 smallint = NULL,@Col310 smallint = NULL,@Col311 smallint = NULL,@Col312 smallint = NULL,@Col313 smallint = NULL,@Col314 smallint = NULL,@Col315 smallint = NULL
		,@Col316 smallint = NULL,@Col317 smallint = NULL,@Col318 smallint = NULL,@Col319 smallint = NULL,@Col320 smallint = NULL,@Col321 smallint = NULL,@Col322 smallint = NULL
		,@Col323 smallint = NULL,@Col324 smallint = NULL,@Col325 smallint = NULL,@Col326 smallint = NULL,@Col327 smallint = NULL,@Col328 smallint = NULL,@Col329 smallint = NULL
		,@Col330 smallint = NULL,@Col331 smallint = NULL,@Col332 smallint = NULL,@Col333 smallint = NULL,@Col334 smallint = NULL,@Col335 smallint = NULL,@Col336 smallint = NULL
		,@Col337 smallint = NULL,@Col338 bit = NULL,@Col339 bit = NULL,@Col340 bit = NULL,@Col341 bit = NULL,@Col342 bit = NULL,@Col343 bit = NULL
		,@Col344 bit = NULL,@Col345 bit = NULL,@Col346 bit = NULL,@Col347 bit = NULL,@Col348 bit = NULL,@Col349 bit = NULL,@Col350 bit = NULL
		,@Col351 bit = NULL,@Col352 bit = NULL,@Col353 bit = NULL,@Col354 bit = NULL,@Col355 bit = NULL,@Col356 bit = NULL,@Col357 bit = NULL
		,@Col358 bit = NULL,@Col359 bit = NULL,@Col360 bit = NULL,@Col361 bit = NULL,@Col362 bit = NULL,@Col363 bit = NULL,@Col364 bit = NULL
		,@Col365 bit = NULL,@Col366 bit = NULL,@Col367 tinyint = NULL,@Col368 smallint = NULL,@Col369 smallint = NULL,@Col370 smallint = NULL,@Col371 smallint = NULL
		,@Col372 smallint = NULL,@Col373 smallint = NULL,@Col374 smallint = NULL,@Col375 smallint = NULL,@Col376 smallint = NULL,@Col377 smallint = NULL,@Col378 smallint = NULL
		,@Col379 smallint = NULL,@Col380 smallint = NULL,@Col381 smallint = NULL,@Col382 smallint = NULL,@Col383 smallint = NULL,@Col384 smallint = NULL,@Col385 smallint = NULL
		,@Col386 smallint = NULL,@Col387 smallint = NULL,@Col388 smallint = NULL,@Col389 smallint = NULL,@Col390 smallint = NULL,@Col391 smallint = NULL,@Col392 smallint = NULL
		,@Col393 smallint = NULL,@Col394 smallint = NULL,@Col395 smallint = NULL,@Col396 smallint = NULL,@Col397 smallint = NULL,@Col398 smallint = NULL,@Col399 smallint = NULL
		,@Col400 smallint = NULL,@Col401 smallint = NULL,@Col402 smallint = NULL,@Col403 smallint = NULL,@Col404 smallint = NULL,@Col405 smallint = NULL,@Col406 smallint = NULL
		,@Col407 smallint = NULL,@Col408 smallint = NULL,@Col409 smallint = NULL,@Col410 smallint = NULL,@Col411 smallint = NULL,@Col412 smallint = NULL,@Col413 smallint = NULL
		,@Col414 smallint = NULL,@Col415 smallint = NULL,@Col416 smallint = NULL,@Col417 smallint = NULL,@Col418 smallint = NULL,@Col419 smallint = NULL,@Col420 smallint = NULL
		,@Col421 smallint = NULL,@Col422 smallint = NULL,@Col423 smallint = NULL,@Col424 smallint = NULL,@Col425 smallint = NULL,@Col426 smallint = NULL,@Col427 smallint = NULL
		,@Col428 smallint = NULL,@Col429 smallint = NULL,@Col430 smallint = NULL,@Col431 smallint = NULL,@Col432 smallint = NULL,@Col433 smallint = NULL,@Col434 smallint = NULL
		,@Col435 smallint = NULL,@Col436 smallint = NULL,@Col437 smallint = NULL,@Col438 smallint = NULL,@Col439 smallint = NULL,@Col440 smallint = NULL,@Col441 smallint = NULL
		,@Col442 smallint = NULL,@Col443 smallint = NULL,@Col444 smallint = NULL,@Col445 smallint = NULL,@Col446 smallint = NULL,@Col447 smallint = NULL,@Col448 smallint = NULL
		,@Col449 smallint = NULL,@Col450 tinyint = NULL,@Col451 tinyint = NULL,@Col452 tinyint = NULL,@Col453 tinyint = NULL,@Col454 tinyint = NULL,@Col455 tinyint = NULL
		,@Col456 tinyint = NULL,@Col457 smallint = NULL,@Col458 tinyint = NULL,@Col459 tinyint = NULL,@Col460 tinyint = NULL,@Col461 smallint = NULL,@Col462 bit = NULL
		,@Col463 smallint = NULL,@Col464 smallint = NULL,@Col465 smallint = NULL,@Col466 smallint = NULL,@Col467 bit = NULL,@Col468 bit = NULL,@Col469 bit = NULL
		,@Col470 bit = NULL,@Col471 bit = NULL,@Col472 bit = NULL,@Col473 bit = NULL,@Col474 bit = NULL,@Col475 bit = NULL,@Col476 bit = NULL
		,@Col477 bit = NULL,@Col478 bit = NULL,@Col479 bit = NULL,@Col480 bit = NULL,@Col481 bit = NULL,@Col482 bit = NULL,@Col483 bit = NULL
		,@Col484 bit = NULL,@Col485 bit = NULL,@Col486 bit = NULL,@Col487 bit = NULL,@Col488 bit = NULL,@Col489 bit = NULL,@Col490 bit = NULL
		,@Col491 bit = NULL,@Col492 bit = NULL,@Col493 bit = NULL,@Col494 bit = NULL,@Col495 bit = NULL,@Col496 bit = NULL,@Col497 bit = NULL
		,@Col498 bit = NULL,@Col499 bit = NULL,@Col500 bit = NULL,@Col501 bit = NULL,@Col502 bit = NULL,@Col503 bit = NULL,@Col504 bit = NULL
		,@Col505 bit = NULL,@Col506 bit = NULL,@Col507 bit = NULL,@Col508 bit = NULL,@Col509 bit = NULL,@Col510 bit = NULL,@Col511 bit = NULL
		,@Col512 bit = NULL,@Col513 bit = NULL,@Col514 bit = NULL,@Col515 bit = NULL,@Col516 bit = NULL,@Col517 bit = NULL,@Col518 bit = NULL
		,@Col519 bit = NULL,@Col520 bit = NULL,@Col521 bit = NULL,@Col522 bit = NULL,@Col523 bit = NULL,@Col524 bit = NULL,@Col525 bit = NULL
		,@Col526 bit = NULL,@Col527 bit = NULL,@Col528 bit = NULL,@Col529 bit = NULL,@Col530 bit = NULL,@Col531 bit = NULL,@Col532 bit = NULL
		,@Col533 bit = NULL,@Col534 bit = NULL,@Col535 bit = NULL,@Col536 bit = NULL,@Col537 bit = NULL,@Col538 bit = NULL,@Col539 bit = NULL
		,@Col540 bit = NULL,@Col541 bit = NULL,@Col542 bit = NULL,@Col543 bit = NULL,@Col544 bit = NULL,@Col545 tinyint = NULL,@Col546 tinyint = NULL
		,@Col547 bit = NULL,@Col548 tinyint = NULL,@Col549 tinyint = NULL,@Col550 tinyint = NULL,@Col551 tinyint = NULL,@Col552 smallint = NULL,@Col553 tinyint = NULL
		,@Col554 smallint = NULL,@Col555 smallint = NULL,@Col556 smallint = NULL,@Col557 tinyint = NULL,@Col558 tinyint = NULL,@Col559 tinyint = NULL,@Col560 tinyint = NULL
		,@Col561 smallint = NULL,@Col562 bit = NULL,@Col563 bit = NULL,@Col564 bit = NULL,@Col565 tinyint = NULL,@Col566 bit = NULL,@Col567 tinyint = NULL
		,@Col568 tinyint = NULL,@Col569 tinyint = NULL,@Col570 bit = NULL,@Col571 bit = NULL,@Col572 tinyint = NULL,@Col573 tinyint = NULL,@Col574 tinyint = NULL
		,@Col575 tinyint = NULL,@Col576 bit = NULL,@Col577 bit = NULL,@Col578 bit = NULL,@Col579 bit = NULL,@Col580 decimal(5,1) = NULL,@Col581 decimal(5,1) = NULL
		,@Col582 decimal(5,1) = NULL,@Col583 decimal(5,1) = NULL,@Col584 decimal(5,1) = NULL,@Col585 decimal(5,1) = NULL,@Col586 decimal(5,1) = NULL,@Col587 decimal(5,1) = NULL,@Col588 bit = NULL
		,@Col589 bit = NULL,@Col590 tinyint = NULL,@Col591 bit = NULL,@Col592 bit = NULL,@Col593 bit = NULL,@Col594 tinyint = NULL,@Col595 bit = NULL
		,@Col596 bit = NULL,@Col597 VARCHAR(15) = NULL,@Col598 smallint = NULL,@Col599 smallint = NULL,@Col600 tinyint = NULL,@Col601 tinyint = NULL,@Col602 tinyint = NULL
		,@Col603 tinyint = NULL,@Col604 tinyint = NULL,@Col605 tinyint = NULL,@Col606 tinyint = NULL,@Col607 tinyint = NULL,@Col608 tinyint = NULL,@Col609 tinyint = NULL
		,@Col610 tinyint = NULL,@Col611 tinyint = NULL,@Col612 tinyint = NULL,@Col613 tinyint = NULL,@Col614 tinyint = NULL,@Col615 tinyint = NULL,@Col616 tinyint = NULL
		,@Col617 tinyint = NULL,@Col618 tinyint = NULL,@Col619 tinyint = NULL,@Col620 tinyint = NULL,@Col621 tinyint = NULL,@Col622 tinyint = NULL,@Col623 tinyint = NULL
		,@Col624 tinyint = NULL,@Col625 tinyint = NULL,@Col626 tinyint = NULL,@Col627 tinyint = NULL,@Col628 tinyint = NULL,@Col629 tinyint = NULL,@Col630 tinyint = NULL
		,@Col631 tinyint = NULL,@Col632 tinyint = NULL,@Col633 tinyint = NULL,@Col634 tinyint = NULL,@Col635 tinyint = NULL,@Col636 tinyint = NULL,@Col637 tinyint = NULL
		,@Col638 tinyint = NULL,@Col639 tinyint = NULL,@Col640 tinyint = NULL,@Col641 tinyint = NULL,@Col642 tinyint = NULL,@Col643 tinyint = NULL,@Col644 tinyint = NULL
		,@Col645 tinyint = NULL,@Col646 bit = NULL,@Col647 bit = NULL,@Col648 decimal(5,1) = NULL,@Col649 decimal(5,1) = NULL,@Col650 decimal(5,1) = NULL,@Col651 bit = NULL
		,@Col652 decimal(5,1) = NULL,@Col653 bit = NULL,@Col654 bit = NULL,@Col655 decimal(5,1) = NULL,@Col656 decimal(5,1) = NULL,@Col657 decimal(5,1) = NULL,@Col658 bit = NULL
		,@Col659 decimal(5,1) = NULL,@Col660 tinyint = NULL,@Col661 bit = NULL,@Col662 bit = NULL,@Col663 bit = NULL	
		,@Col664 int = NULL,@Col665 int = NULL,@Col666 int = NULL,@Col667 int = NULL,@Col668 int = NULL,@Col669 int = NULL,@Col670 int = NULL
		,@Col671 int = NULL,@Col672 int = NULL,@Col673 int = NULL,@Col674 int = NULL,@Col675 int = NULL,@Col676 bit = NULL
		,@Col677 int = NULL,@Col678 int = NULL,@Col679 int = NULL,@Col680 int = NULL,@Col681 int = NULL,@Col682 int = NULL,@Col683 int = NULL
		,@Col684 int = NULL,@Col685 int = NULL,@Col686 int = NULL,@Col687 int = NULL,@Col688 int = NULL,@Col689 int = NULL,@Col690 int = NULL
		,@Col691 int = NULL,@Col692 int = NULL,@Col693 int = NULL,@Col694 int = NULL,@Col695 int = NULL,@Col696 int = NULL,@Col697 int = NULL
		,@Col698 int = NULL,@Col699 int = NULL,@Col700 int = NULL,@Col701 int = NULL,@Col702 int = NULL,@Col703 int = NULL,@Col704 int = NULL
		,@Col705 int = NULL,@Col706 int = NULL,@Col707 int = NULL,@Col708 int = NULL,@Col709 int = NULL,@Col710 int = NULL,@Col711 int = NULL
		,@Col712 int = NULL,@Col713 int = NULL,@Col714 int = NULL,@Col715 int = NULL
		,@Col3000 bit = NULL,@Col3001 bit = NULL,@Col3002 bit = NULL,@Col3003 bit = NULL
		,@Col3004 bit = NULL,@Col3005 bit = NULL,@Col3006 bit = NULL,@Col3007 bit = NULL,@Col3008 bit = NULL,@Col3009 bit = NULL,@Col3010 bit = NULL
		,@Col3011 bit = NULL,@Col3012 bit = NULL,@Col3013 bit = NULL,@Col3014 bit = NULL,@Col3015 bit = NULL,@Col3016 bit = NULL,@Col3017 bit = NULL
		,@Col3018 bit = NULL,@Col3019 bit = NULL,@Col3020 bit = NULL,@Col3021 bit = NULL,@Col3022 bit = NULL,@Col3023 bit = NULL,@Col3024 bit = NULL
		,@Col3025 bit = NULL,@Col3026 bit = NULL,@Col3027 bit = NULL,@Col3028 bit = NULL,@Col3029 bit = NULL,@Col3030 bit = NULL,@Col3031 bit = NULL
		,@Col3032 bit = NULL,@Col3033 bit = NULL,@Col3034 bit = NULL,@Col3035 bit = NULL,@Col3036 bit = NULL,@Col3037 bit = NULL,@Col3038 bit = NULL
		,@Col3039 bit = NULL,@Col3040 bit = NULL,@Col3041 bit = NULL,@Col3042 bit = NULL,@Col3043 bit = NULL,@Col3044 bit = NULL,@Col3045 bit = NULL
		,@Col3046 bit = NULL,@Col3047 bit = NULL,@Col3048 bit = NULL,@Col3049 bit = NULL,@Col3050 bit = NULL,@Col3051 bit = NULL,@Col3052 bit = NULL
		,@Col3053 bit = NULL,@Col3054 bit = NULL,@Col3055 bit = NULL,@Col3056 bit = NULL,@Col3057 bit = NULL,@Col3058 bit = NULL,@Col3059 bit = NULL
		,@Col3060 bit = NULL,@Col3061 bit = NULL,@Col3062 bit = NULL,@Col3063 bit = NULL,@Col3064 bit = NULL,@Col3065 bit = NULL,@Col3066 bit = NULL
		,@Col3067 bit = NULL,@Col3068 bit = NULL,@Col3069 bit = NULL,@Col3070 bit = NULL,@Col3071 bit = NULL,@Col3072 bit = NULL,@Col3073 bit = NULL
		,@Col3074 bit = NULL,@Col3075 bit = NULL,@Col3076 bit = NULL,@Col3077 bit = NULL,@Col3078 bit = NULL,@Col3079 bit = NULL,@Col3080 bit = NULL
		,@Col3081 bit = NULL,@Col3082 bit = NULL,@Col3083 bit = NULL,@Col3084 bit = NULL,@Col3085 bit = NULL,@Col3086 bit = NULL,@Col3087 bit = NULL
		,@Col3088 bit = NULL,@Col3089 bit = NULL,@Col3090 bit = NULL,@Col3091 bit = NULL,@Col3092 bit = NULL,@Col3093 bit = NULL,@Col3094 bit = NULL
		,@Col3095 bit = NULL,@Col3096 bit = NULL,@Col3097 bit = NULL,@Col3098 bit = NULL,@Col3099 bit = NULL,@Col3100 bit = NULL,@Col3101 bit = NULL
		,@Col3102 bit = NULL,@Col3103 bit = NULL,@Col3104 bit = NULL,@Col3105 bit = NULL,@Col3106 bit = NULL,@Col3107 bit = NULL,@Col3108 bit = NULL
		,@Col3109 bit = NULL,@Col3110 bit = NULL,@Col3111 bit = NULL,@Col3112 bit = NULL,@Col3113 bit = NULL,@Col3114 bit = NULL,@Col3115 bit = NULL
		,@Col3116 bit = NULL,@Col3117 bit = NULL,@Col3118 bit = NULL,@Col3119 bit = NULL,@Col3120 bit = NULL,@Col3121 bit = NULL,@Col3122 bit = NULL
		,@Col3123 bit = NULL,@Col3124 bit = NULL,@Col3125 bit = NULL,@Col3126 bit = NULL,@Col3127 bit = NULL,@Col3128 bit = NULL,@Col3129 bit = NULL
		,@Col3130 bit = NULL,@Col3131 bit = NULL,@Col3132 bit = NULL,@Col3133 bit = NULL,@Col3134 bit = NULL,@Col3135 bit = NULL,@Col3136 bit = NULL
		,@Col3137 bit = NULL,@Col3138 bit = NULL,@Col3139 bit = NULL,@Col3140 bit = NULL,@Col3141 bit = NULL,@Col3142 bit = NULL,@Col3143 bit = NULL
		,@Col3144 bit = NULL,@Col3145 bit = NULL,@Col3146 bit = NULL,@Col3147 bit = NULL,@Col3148 bit = NULL,@Col3149 bit = NULL,@Col3150 bit = NULL
		,@Col3151 bit = NULL,@Col3152 bit = NULL,@Col3153 bit = NULL,@Col3154 bit = NULL,@Col3155 bit = NULL,@Col3156 bit = NULL,@Col3157 bit = NULL
		,@Col3158 bit = NULL,@Col3159 bit = NULL,@Col3160 bit = NULL,@Col3161 bit = NULL,@Col3162 bit = NULL,@Col3163 bit = NULL,@Col3164 bit = NULL
		,@Col3165 bit = NULL,@Col3166 bit = NULL,@Col3167 bit = NULL,@Col3168 bit = NULL,@Col3169 bit = NULL,@Col3170 bit = NULL,@Col3171 bit = NULL
		,@Col3172 bit = NULL,@Col3173 bit = NULL,@Col3174 bit = NULL,@Col3175 bit = NULL,@Col3176 bit = NULL,@Col3177 bit = NULL,@Col3178 bit = NULL
		,@Col3179 bit = NULL,@Col3180 bit = NULL,@Col3181 bit = NULL,@Col3182 bit = NULL,@Col3183 bit = NULL,@Col3184 bit = NULL,@Col3185 bit = NULL
		,@Col3186 bit = NULL,@Col3187 bit = NULL,@Col3188 bit = NULL,@Col3189 bit = NULL,@Col3190 bit = NULL,@Col3191 bit = NULL,@Col3192 bit = NULL
		,@Col3193 bit = NULL,@Col3194 bit = NULL,@Col3195 bit = NULL,@Col3196 bit = NULL,@Col3197 bit = NULL,@Col3198 bit = NULL,@Col3199 bit = NULL
		,@Col3200 bit = NULL,@Col3201 bit = NULL,@Col3202 bit = NULL,@Col3203 bit = NULL,@Col3204 bit = NULL,@Col3205 bit = NULL,@Col3206 bit = NULL
		,@Col3207 bit = NULL,@Col3208 bit = NULL,@Col3209 bit = NULL,@Col3210 bit = NULL,@Col3211 bit = NULL,@Col3212 bit = NULL,@Col3213 bit = NULL
		,@Col3214 bit = NULL,@Col3215 bit = NULL,@Col3216 bit = NULL,@Col3217 bit = NULL,@Col3218 bit = NULL,@Col3219 bit = NULL,@Col3220 bit = NULL
		,@Col3221 bit = NULL,@Col3222 bit = NULL,@Col3223 bit = NULL,@Col3224 bit = NULL,@Col3225 bit = NULL,@Col3226 bit = NULL,@Col3227 bit = NULL
		,@Col3228 bit = NULL,@Col3229 bit = NULL,@Col3230 bit = NULL,@Col3231 bit = NULL,@Col3232 bit = NULL,@Col3233 bit = NULL,@Col3234 bit = NULL
		,@Col3235 bit = NULL,@Col3236 bit = NULL,@Col3237 bit = NULL,@Col3238 bit = NULL,@Col3239 bit = NULL,@Col3240 bit = NULL,@Col3241 bit = NULL
		,@Col3242 bit = NULL,@Col3243 bit = NULL,@Col3244 bit = NULL,@Col3245 bit = NULL,@Col3246 bit = NULL,@Col3247 bit = NULL,@Col3248 bit = NULL
		,@Col3249 bit = NULL,@Col3250 bit = NULL,@Col3251 bit = NULL,@Col3252 bit = NULL,@Col3253 bit = NULL,@Col3254 bit = NULL,@Col3255 bit = NULL
		,@Col3256 bit = NULL,@Col3257 bit = NULL,@Col3258 bit = NULL,@Col3259 bit = NULL,@Col3260 bit = NULL,@Col3261 bit = NULL,@Col3262 bit = NULL
		,@Col3263 bit = NULL,@Col3264 bit = NULL,@Col3265 bit = NULL,@Col3266 bit = NULL,@Col3267 bit = NULL,@Col3268 bit = NULL,@Col3269 bit = NULL
		,@Col3270 bit = NULL,@Col3271 bit = NULL,@Col3272 bit = NULL,@Col3273 bit = NULL,@Col3274 bit = NULL,@Col3275 bit = NULL,@Col3276 bit = NULL
		,@Col3277 bit = NULL,@Col3278 bit = NULL,@Col3279 bit = NULL,@Col3280 bit = NULL,@Col3281 bit = NULL,@Col3282 bit = NULL,@Col3283 bit = NULL
		,@Col3284 bit = NULL,@Col3285 bit = NULL,@Col3286 bit = NULL,@Col3287 bit = NULL,@Col3288 bit = NULL,@Col3289 bit = NULL,@Col3290 bit = NULL
		,@Col3291 bit = NULL,@Col3292 bit = NULL,@Col3293 bit = NULL,@Col3294 bit = NULL,@Col3295 bit = NULL,@Col3296 bit = NULL,@Col3297 bit = NULL
		,@Col3298 bit = NULL,@Col3299 bit = NULL,@Col3300 bit = NULL,@Col3301 bit = NULL,@Col3302 bit = NULL,@Col3303 bit = NULL,@Col3304 bit = NULL
		,@Col3305 bit = NULL,@Col3306 bit = NULL,@Col3307 bit = NULL,@Col3308 bit = NULL,@Col3309 bit = NULL,@Col3310 bit = NULL,@Col3311 bit = NULL
		,@Col3312 bit = NULL,@Col3313 bit = NULL,@Col3314 bit = NULL,@Col3315 bit = NULL,@Col3316 bit = NULL,@Col3317 bit = NULL,@Col3318 bit = NULL
		,@Col3319 bit = NULL,@Col3320 bit = NULL,@Col3321 bit = NULL,@Col3322 bit = NULL,@Col3323 bit = NULL,@Col3324 bit = NULL,@Col3325 bit = NULL
		,@Col3326 bit = NULL,@Col3327 bit = NULL,@Col3328 bit = NULL,@Col3329 bit = NULL,@Col3330 bit = NULL,@Col3331 bit = NULL,@Col3332 bit = NULL
		,@Col3333 bit = NULL,@Col3334 bit = NULL,@Col3335 bit = NULL,@Col3336 bit = NULL,@Col3337 bit = NULL,@Col3338 bit = NULL,@Col3339 bit = NULL
		,@Col3340 bit = NULL,@Col3341 bit = NULL,@Col3342 bit = NULL,@Col3343 bit = NULL,@Col3344 bit = NULL,@Col3345 bit = NULL,@Col3346 bit = NULL
		,@Col3347 bit = NULL,@Col3348 bit = NULL,@Col3349 bit = NULL,@Col3350 bit = NULL,@Col3351 bit = NULL,@Col3352 bit = NULL,@Col3353 bit = NULL
		,@Col3354 bit = NULL,@Col3355 bit = NULL,@Col3356 bit = NULL,@Col3357 bit = NULL,@Col3358 bit = NULL,@Col3359 bit = NULL,@Col3360 bit = NULL
		,@Col3361 bit = NULL,@Col3362 bit = NULL,@Col3363 bit = NULL,@Col3364 bit = NULL,@Col3365 bit = NULL,@Col3366 bit = NULL,@Col3367 bit = NULL
		,@Col3368 bit = NULL,@Col3369 bit = NULL,@Col3370 bit = NULL,@Col3371 bit = NULL,@Col3372 bit = NULL,@Col3373 bit = NULL,@Col3374 bit = NULL
		,@Col3375 bit = NULL,@Col3376 bit = NULL,@Col3377 bit = NULL,@Col3378 bit = NULL,@Col3379 bit = NULL,@Col3380 bit = NULL,@Col3381 bit = NULL
		,@Col3382 bit = NULL,@Col3383 bit = NULL,@Col3384 bit = NULL,@Col3385 bit = NULL,@Col3386 bit = NULL,@Col3387 bit = NULL,@Col3388 bit = NULL
		,@Col3389 bit = NULL,@Col3390 bit = NULL,@Col3391 bit = NULL,@Col3392 bit = NULL,@Col3393 bit = NULL,@Col3394 bit = NULL,@Col3395 bit = NULL
		,@Col3396 bit = NULL,@Col3397 bit = NULL,@Col3398 bit = NULL,@Col3399 bit = NULL,@Col3400 bit = NULL,@Col3401 bit = NULL,@Col3402 bit = NULL
		,@Col3403 bit = NULL,@Col3404 bit = NULL,@Col3405 bit = NULL,@Col3406 bit = NULL,@Col3407 bit = NULL,@Col3408 bit = NULL,@Col3409 bit = NULL
		,@Col3410 bit = NULL,@Col3411 bit = NULL,@Col3412 bit = NULL,@Col3413 bit = NULL,@Col3414 bit = NULL,@Col3415 bit = NULL,@Col3416 bit = NULL
		,@Col3417 bit = NULL,@Col3418 bit = NULL,@Col3419 bit = NULL,@Col3420 bit = NULL,@Col3421 bit = NULL,@Col3422 bit = NULL,@Col3423 bit = NULL
		,@Col3424 bit = NULL,@Col3425 bit = NULL,@Col3426 bit = NULL,@Col3427 bit = NULL,@Col3428 bit = NULL,@Col3429 bit = NULL,@Col3430 bit = NULL
		,@Col3431 bit = NULL,@Col3432 bit = NULL,@Col3433 bit = NULL,@Col3434 bit = NULL,@Col3435 bit = NULL,@Col3436 bit = NULL,@Col3437 bit = NULL
		,@Col3438 bit = NULL,@Col3439 bit = NULL,@Col3440 bit = NULL,@Col3441 bit = NULL,@Col3442 bit = NULL,@Col3443 bit = NULL,@Col3444 bit = NULL
		,@Col3445 bit = NULL,@Col3446 bit = NULL,@Col3447 bit = NULL,@Col3448 bit = NULL,@Col3449 bit = NULL,@Col3450 bit = NULL,@Col3451 bit = NULL
		,@Col3452 bit = NULL,@Col3453 bit = NULL,@Col3454 bit = NULL,@Col3455 bit = NULL,@Col3456 bit = NULL,@Col3457 bit = NULL,@Col3458 bit = NULL
		,@Col3459 bit = NULL,@Col3460 bit = NULL,@Col3461 bit = NULL,@Col3462 bit = NULL,@Col3463 bit = NULL,@Col3464 bit = NULL,@Col3465 bit = NULL
		,@Col3466 bit = NULL,@Col3467 bit = NULL,@Col3468 bit = NULL,@Col3469 bit = NULL,@Col3470 bit = NULL,@Col3471 bit = NULL,@Col3472 bit = NULL
		,@Col3473 bit = NULL,@Col3474 bit = NULL,@Col3475 bit = NULL,@Col3476 bit = NULL,@Col3477 bit = NULL,@Col3478 bit = NULL,@Col3479 bit = NULL
		,@Col3480 bit = NULL,@Col3481 bit = NULL,@Col3482 bit = NULL,@Col3483 bit = NULL,@Col3484 bit = NULL,@Col3485 bit = NULL,@Col3486 bit = NULL
		,@Col3487 bit = NULL,@Col3488 bit = NULL,@Col3489 bit = NULL,@Col3490 bit = NULL,@Col3491 bit = NULL,@Col3492 bit = NULL,@Col3493 bit = NULL
		,@Col3494 bit = NULL,@Col3495 bit = NULL,@Col3496 bit = NULL,@Col3497 bit = NULL,@Col3498 bit = NULL,@Col3499 bit = NULL,@Col3500 bit = NULL
		,@Col3501 bit = NULL,@Col3502 bit = NULL,@Col3503 bit = NULL,@Col3504 bit = NULL,@Col3505 bit = NULL,@Col3506 bit = NULL,@Col3507 bit = NULL
		,@Col3508 bit = NULL,@Col3509 bit = NULL,@Col3510 bit = NULL,@Col3511 bit = NULL,@Col3512 bit = NULL,@Col3513 bit = NULL,@Col3514 bit = NULL
		,@Col3515 bit = NULL,@Col3516 bit = NULL,@Col3517 bit = NULL,@Col3518 bit = NULL,@Col3519 bit = NULL
		)
		AS
		BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[col9],[col10],[col11],[col12],[col13],[col14],[col15],[col16]
		,[col17],[col18],[col19],[col20],[col21],[col22],[col23],[col24],[col25],[col26],[col27],[col28],[col29],[col30],[col31],[col32]
		,[col33],[col34],[col35],[col36],[col37],[col38],[col39],[col40],[col41],[col42],[col43],[col44],[col45],[col46],[col47],[col48]
		,[col49],[col50],[col51],[col52],[col53],[col54],[col55],[col56],[col57],[col58],[col59],[col60],[col61],[col62],[col63],[col64]
		,[col65],[col66],[col67],[col68],[col69],[col70],[col71],[col72],[col73],[col74],[col75],[col76],[col77],[col78],[col79],[col80]
		,[col81],[col82],[col83],[col84],[col85],[col86],[col87],[col88],[col89],[col90],[col91],[col92],[col93],[col94],[col95],[col96]
		,[col97],[col98],[col99],[col100],[col101],[col102],[col103],[col104],[col105]
		,[col106],[col107],[col108],[col109],[col110],[col111],[col112]
		,[col113],[col114],[col115],[col116],[col117],[col118],[col119]
		,[col120],[col121],[col122],[col123],[col124],[col125],[col126]
		,[col127],[col128],[col129],[col130],[col131],[col132],[col133]
		,[col134],[col135],[col136],[col137],[col138],[col139],[col140]
		,[col141],[col142],[col143],[col144],[col145],[col146],[col147]
		,[col148],[col149],[col150],[col151],[col152],[col153],[col154]
		,[col155],[col156],[col157],[col158],[col159],[col160],[col161]
		,[col162],[col163],[col164],[col165],[col166],[col167],[col168]
		,[col169],[col170],[col171],[col172],[col173],[col174],[col175]
		,[col176],[col177],[col178],[col179],[col180],[col181],[col182]
		,[col183],[col184],[col185],[col186],[col187],[col188],[col189]
		,[col190],[col191],[col192],[col193],[col194],[col195],[col196]
		,[col197],[col198],[col199],[col200],[col201],[col202],[col203]
		,[col204],[col205],[col206],[col207],[col208],[col209],[col210]
		,[col211],[col212],[col213],[col214],[col215],[col216],[col217]
		,[col218],[col219],[col220],[col221],[col222],[col223],[col224]
		,[col225],[col226],[col227],[col228],[col229],[col230],[col231]
		,[col232],[col233],[col234],[col235],[col236],[col237],[col238]
		,[col239],[col240],[col241],[col242],[col243],[col244],[col245]
		,[col246],[col247],[col248],[col249],[col250],[col251],[col252]
		,[col253],[col254],[col255],[col256],[col257],[col258],[col259]
		,[col260],[col261],[col262],[col263],[col264],[col265],[col266]
		,[col267],[col268],[col269],[col270],[col271],[col272],[col273]
		,[col274],[col275],[col276],[col277],[col278],[col279],[col280]
		,[col281],[col282],[col283],[col284],[col285],[col286],[col287]
		,[col288],[col289],[col290],[col291],[col292],[col293],[col294]
		,[col295],[col296],[col297],[col298],[col299],[col300],[col301]
		,[col302],[col303],[col304],[col305],[col306],[col307],[col308]
		,[col309],[col310],[col311],[col312],[col313],[col314],[col315]
		,[col316],[col317],[col318],[col319],[col320],[col321],[col322]
		,[col323],[col324],[col325],[col326],[col327],[col328],[col329]
		,[col330],[col331],[col332],[col333],[col334],[col335],[col336]
		,[col337],[col338],[col339],[col340],[col341],[col342],[col343]
		,[col344],[col345],[col346],[col347],[col348],[col349],[col350]
		,[col351],[col352],[col353],[col354],[col355],[col356],[col357]
		,[col358],[col359],[col360],[col361],[col362],[col363],[col364]
		,[col365],[col366],[col367],[col368],[col369],[col370],[col371]
		,[col372],[col373],[col374],[col375],[col376],[col377],[col378]
		,[col379],[col380],[col381],[col382],[col383],[col384],[col385]
		,[col386],[col387],[col388],[col389],[col390],[col391],[col392]
		,[col393],[col394],[col395],[col396],[col397],[col398],[col399]
		,[col400],[col401],[col402],[col403],[col404],[col405],[col406]
		,[col407],[col408],[col409],[col410],[col411],[col412],[col413]
		,[col414],[col415],[col416],[col417],[col418],[col419],[col420]
		,[col421],[col422],[col423],[col424],[col425],[col426],[col427]
		,[col428],[col429],[col430],[col431],[col432],[col433],[col434]
		,[col435],[col436],[col437],[col438],[col439],[col440],[col441]
		,[col442],[col443],[col444],[col445],[col446],[col447],[col448]
		,[col449],[col450],[col451],[col452],[col453],[col454],[col455]
		,[col456],[col457],[col458],[col459],[col460],[col461],[col462]
		,[col463],[col464],[col465],[col466],[col467],[col468],[col469]
		,[col470],[col471],[col472],[col473],[col474],[col475],[col476]
		,[col477],[col478],[col479],[col480],[col481],[col482],[col483]
		,[col484],[col485],[col486],[col487],[col488],[col489],[col490]
		,[col491],[col492],[col493],[col494],[col495],[col496],[col497]
		,[col498],[col499],[col500],[col501],[col502],[col503],[col504]
		,[col505],[col506],[col507],[col508],[col509],[col510],[col511]
		,[col512],[col513],[col514],[col515],[col516],[col517],[col518]
		,[col519],[col520],[col521],[col522],[col523],[col524],[col525]
		,[col526],[col527],[col528],[col529],[col530],[col531],[col532]
		,[col533],[col534],[col535],[col536],[col537],[col538],[col539]
		,[col540],[col541],[col542],[col543],[col544],[col545],[col546]
		,[col547],[col548],[col549],[col550],[col551],[col552],[col553]
		,[col554],[col555],[col556],[col557],[col558],[col559],[col560]
		,[col561],[col562],[col563],[col564],[col565],[col566],[col567]
		,[col568],[col569],[col570],[col571],[col572],[col573],[col574]
		,[col575],[col576],[col577],[col578],[col579],[col580],[col581]
		,[col582],[col583],[col584],[col585],[col586],[col587],[col588]
		,[col589],[col590],[col591],[col592],[col593],[col594],[col595]
		,[col596],[col597],[col598],[col599],[col600],[col601],[col602]
		,[col603],[col604],[col605],[col606],[col607],[col608],[col609]
		,[col610],[col611],[col612],[col613],[col614],[col615],[col616]
		,[col617],[col618],[col619],[col620],[col621],[col622],[col623]
		,[col624],[col625],[col626],[col627],[col628],[col629],[col630]
		,[col631],[col632],[col633],[col634],[col635],[col636],[col637]
		,[col638],[col639],[col640],[col641],[col642],[col643],[col644]
		,[col645],[col646],[col647],[col648],[col649],[col650],[col651]
		,[col652],[col653],[col654],[col655],[col656],[col657],[col658]
		,[col659],[col660],[col661],[col662],[col663]
		,[Col664],[Col665],[Col666],[Col667],[Col668],[Col669],[Col670]
		,[Col671],[Col672],[Col673],[Col674],[Col675],[Col676],[Col677]
		,[Col678],[Col679],[Col680],[Col681],[Col682],[Col683],[Col684]
		,[Col685],[Col686],[Col687],[Col688],[Col689],[Col690],[Col691]
		,[Col692],[Col693],[Col694],[Col695],[Col696],[Col697],[Col698]
		,[Col699],[Col700],[Col701],[Col702],[Col703],[Col704],[Col705]
		,[Col706],[Col707],[Col708],[Col709],[Col710],[Col711],[Col712]
		,[Col713],[Col714],[Col715]
           )
	OUTPUT inserted.ID INTO @IDs(ID) 
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@Col1,COL1),ISNULL(@Col2,COL2),ISNULL(@Col3,COL3),ISNULL(@Col4,COL4),ISNULL(@Col5,COL5),ISNULL(@Col6,COL6),ISNULL(@Col7,COL7)
		,ISNULL(@Col8,COL8),ISNULL(@Col9,COL9),ISNULL(@Col10,COL10),ISNULL(@Col11,COL11),ISNULL(@Col12,COL12),ISNULL(@Col13,COL13),ISNULL(@Col14,COL14)
		,ISNULL(@Col15,COL15),ISNULL(@Col16,COL16),ISNULL(@Col17,COL17),ISNULL(@Col18,COL18),ISNULL(@Col19,COL19),ISNULL(@Col20,COL20),ISNULL(@Col21,COL21)
		,ISNULL(@Col22,COL22),ISNULL(@Col23,COL23),ISNULL(@Col24,COL24),ISNULL(@Col25,COL25),ISNULL(@Col26,COL26),ISNULL(@Col27,COL27),ISNULL(@Col28,COL28)
		,ISNULL(@Col29,COL29),ISNULL(@Col30,COL30),ISNULL(@Col31,COL31),ISNULL(@Col32,COL32),ISNULL(@Col33,COL33),ISNULL(@Col34,COL34),ISNULL(@Col35,COL35)
		,ISNULL(@Col36,COL36),ISNULL(@Col37,COL37),ISNULL(@Col38,COL38),ISNULL(@Col39,COL39),ISNULL(@Col40,COL40),ISNULL(@Col41,COL41),ISNULL(@Col42,COL42)
		,ISNULL(@Col43,COL43),ISNULL(@Col44,COL44),ISNULL(@Col45,COL45),ISNULL(@Col46,COL46),ISNULL(@Col47,COL47),ISNULL(@Col48,COL48),ISNULL(@Col49,COL49)
		,ISNULL(@Col50,COL50),ISNULL(@Col51,COL51),ISNULL(@Col52,COL52),ISNULL(@Col53,COL53),ISNULL(@Col54,COL54),ISNULL(@Col55,COL55),ISNULL(@Col56,COL56)
		,ISNULL(@Col57,COL57),ISNULL(@Col58,COL58),ISNULL(@Col59,COL59),ISNULL(@Col60,COL60),ISNULL(@Col61,COL61),ISNULL(@Col62,COL62),ISNULL(@Col63,COL63)
		,ISNULL(@Col64,COL64),ISNULL(@Col65,COL65),ISNULL(@Col66,COL66),ISNULL(@Col67,COL67),ISNULL(@Col68,COL68),ISNULL(@Col69,COL69),ISNULL(@Col70,COL70)
		,ISNULL(@Col71,COL71),ISNULL(@Col72,COL72),ISNULL(@Col73,COL73),ISNULL(@Col74,COL74),ISNULL(@Col75,COL75),ISNULL(@Col76,COL76),ISNULL(@Col77,COL77)
		,ISNULL(@Col78,COL78),ISNULL(@Col79,COL79),ISNULL(@Col80,COL80),ISNULL(@Col81,COL81),ISNULL(@Col82,COL82),ISNULL(@Col83,COL83),ISNULL(@Col84,COL84)
		,ISNULL(@Col85,COL85),ISNULL(@Col86,COL86),ISNULL(@Col87,COL87),ISNULL(@Col88,COL88),ISNULL(@Col89,COL89),ISNULL(@Col90,COL90),ISNULL(@Col91,COL91)
		,ISNULL(@Col92,COL92),ISNULL(@Col93,COL93),ISNULL(@Col94,COL94),ISNULL(@Col95,COL95),ISNULL(@Col96,COL96),ISNULL(@Col97,COL97),ISNULL(@Col98,COL98)
		,ISNULL(@Col99,COL99),ISNULL(@Col100,COL100),ISNULL(@Col101,COL101),ISNULL(@Col102,COL102),ISNULL(@Col103,COL103),ISNULL(@Col104,COL104),ISNULL(@Col105,COL105)
		,ISNULL(@Col106,COL106),ISNULL(@Col107,COL107),ISNULL(@Col108,COL108),ISNULL(@Col109,COL109),ISNULL(@Col110,COL110),ISNULL(@Col111,COL111),ISNULL(@Col112,COL112)
		,ISNULL(@Col113,COL113),ISNULL(@Col114,COL114),ISNULL(@Col115,COL115),ISNULL(@Col116,COL116),ISNULL(@Col117,COL117),ISNULL(@Col118,COL118),ISNULL(@Col119,COL119)
		,ISNULL(@Col120,COL120),ISNULL(@Col121,COL121),ISNULL(@Col122,COL122),ISNULL(@Col123,COL123),ISNULL(@Col124,COL124),ISNULL(@Col125,COL125),ISNULL(@Col126,COL126)
		,ISNULL(@Col127,COL127),ISNULL(@Col128,COL128),ISNULL(@Col129,COL129),ISNULL(@Col130,COL130),ISNULL(@Col131,COL131),ISNULL(@Col132,COL132),ISNULL(@Col133,COL133)
		,ISNULL(@Col134,COL134),ISNULL(@Col135,COL135),ISNULL(@Col136,COL136),ISNULL(@Col137,COL137),ISNULL(@Col138,COL138),ISNULL(@Col139,COL139),ISNULL(@Col140,COL140)
		,ISNULL(@Col141,COL141),ISNULL(@Col142,COL142),ISNULL(@Col143,COL143),ISNULL(@Col144,COL144),ISNULL(@Col145,COL145),ISNULL(@Col146,COL146),ISNULL(@Col147,COL147)
		,ISNULL(@Col148,COL148),ISNULL(@Col149,COL149),ISNULL(@Col150,COL150),ISNULL(@Col151,COL151),ISNULL(@Col152,COL152),ISNULL(@Col153,COL153),ISNULL(@Col154,COL154)
		,ISNULL(@Col155,COL155),ISNULL(@Col156,COL156),ISNULL(@Col157,COL157),ISNULL(@Col158,COL158),ISNULL(@Col159,COL159),ISNULL(@Col160,COL160),ISNULL(@Col161,COL161)
		,ISNULL(@Col162,COL162),ISNULL(@Col163,COL163),ISNULL(@Col164,COL164),ISNULL(@Col165,COL165),ISNULL(@Col166,COL166),ISNULL(@Col167,COL167),ISNULL(@Col168,COL168)
		,ISNULL(@Col169,COL169),ISNULL(@Col170,COL170),ISNULL(@Col171,COL171),ISNULL(@Col172,COL172),ISNULL(@Col173,COL173),ISNULL(@Col174,COL174),ISNULL(@Col175,COL175)
		,ISNULL(@Col176,COL176),ISNULL(@Col177,COL177),ISNULL(@Col178,COL178),ISNULL(@Col179,COL179),ISNULL(@Col180,COL180),ISNULL(@Col181,COL181),ISNULL(@Col182,COL182)
		,ISNULL(@Col183,COL183),ISNULL(@Col184,COL184),ISNULL(@Col185,COL185),ISNULL(@Col186,COL186),ISNULL(@Col187,COL187),ISNULL(@Col188,COL188),ISNULL(@Col189,COL189)
		,ISNULL(@Col190,COL190),ISNULL(@Col191,COL191),ISNULL(@Col192,COL192),ISNULL(@Col193,COL193),ISNULL(@Col194,COL194),ISNULL(@Col195,COL195),ISNULL(@Col196,COL196)
		,ISNULL(@Col197,COL197),ISNULL(@Col198,COL198),ISNULL(@Col199,COL199),ISNULL(@Col200,COL200),ISNULL(@Col201,COL201),ISNULL(@Col202,COL202),ISNULL(@Col203,COL203)
		,ISNULL(@Col204,COL204),ISNULL(@Col205,COL205),ISNULL(@Col206,COL206),ISNULL(@Col207,COL207),ISNULL(@Col208,COL208),ISNULL(@Col209,COL209),ISNULL(@Col210,COL210)
		,ISNULL(@Col211,COL211),ISNULL(@Col212,COL212),ISNULL(@Col213,COL213),ISNULL(@Col214,COL214),ISNULL(@Col215,COL215),ISNULL(@Col216,COL216),ISNULL(@Col217,COL217)
		,ISNULL(@Col218,COL218),ISNULL(@Col219,COL219),ISNULL(@Col220,COL220),ISNULL(@Col221,COL221),ISNULL(@Col222,COL222),ISNULL(@Col223,COL223),ISNULL(@Col224,COL224)
		,ISNULL(@Col225,COL225),ISNULL(@Col226,COL226),ISNULL(@Col227,COL227),ISNULL(@Col228,COL228),ISNULL(@Col229,COL229),ISNULL(@Col230,COL230),ISNULL(@Col231,COL231)
		,ISNULL(@Col232,COL232),ISNULL(@Col233,COL233),ISNULL(@Col234,COL234),ISNULL(@Col235,COL235),ISNULL(@Col236,COL236),ISNULL(@Col237,COL237),ISNULL(@Col238,COL238)
		,ISNULL(@Col239,COL239),ISNULL(@Col240,COL240),ISNULL(@Col241,COL241),ISNULL(@Col242,COL242),ISNULL(@Col243,COL243),ISNULL(@Col244,COL244),ISNULL(@Col245,COL245)
		,ISNULL(@Col246,COL246),ISNULL(@Col247,COL247),ISNULL(@Col248,COL248),ISNULL(@Col249,COL249),ISNULL(@Col250,COL250),ISNULL(@Col251,COL251),ISNULL(@Col252,COL252)
		,ISNULL(@Col253,COL253),ISNULL(@Col254,COL254),ISNULL(@Col255,COL255),ISNULL(@Col256,COL256),ISNULL(@Col257,COL257),ISNULL(@Col258,COL258),ISNULL(@Col259,COL259)
		,ISNULL(@Col260,COL260),ISNULL(@Col261,COL261),ISNULL(@Col262,COL262),ISNULL(@Col263,COL263),ISNULL(@Col264,COL264),ISNULL(@Col265,COL265),ISNULL(@Col266,COL266)
		,ISNULL(@Col267,COL267),ISNULL(@Col268,COL268),ISNULL(@Col269,COL269),ISNULL(@Col270,COL270),ISNULL(@Col271,COL271),ISNULL(@Col272,COL272),ISNULL(@Col273,COL273)
		,ISNULL(@Col274,COL274),ISNULL(@Col275,COL275),ISNULL(@Col276,COL276),ISNULL(@Col277,COL277),ISNULL(@Col278,COL278),ISNULL(@Col279,COL279),ISNULL(@Col280,COL280)
		,ISNULL(@Col281,COL281),ISNULL(@Col282,COL282),ISNULL(@Col283,COL283),ISNULL(@Col284,COL284),ISNULL(@Col285,COL285),ISNULL(@Col286,COL286),ISNULL(@Col287,COL287)
		,ISNULL(@Col288,COL288),ISNULL(@Col289,COL289),ISNULL(@Col290,COL290),ISNULL(@Col291,COL291),ISNULL(@Col292,COL292),ISNULL(@Col293,COL293),ISNULL(@Col294,COL294)
		,ISNULL(@Col295,COL295),ISNULL(@Col296,COL296),ISNULL(@Col297,COL297),ISNULL(@Col298,COL298),ISNULL(@Col299,COL299),ISNULL(@Col300,COL300),ISNULL(@Col301,COL301)
		,ISNULL(@Col302,COL302),ISNULL(@Col303,COL303),ISNULL(@Col304,COL304),ISNULL(@Col305,COL305),ISNULL(@Col306,COL306),ISNULL(@Col307,COL307),ISNULL(@Col308,COL308)
		,ISNULL(@Col309,COL309),ISNULL(@Col310,COL310),ISNULL(@Col311,COL311),ISNULL(@Col312,COL312),ISNULL(@Col313,COL313),ISNULL(@Col314,COL314),ISNULL(@Col315,COL315)
		,ISNULL(@Col316,COL316),ISNULL(@Col317,COL317),ISNULL(@Col318,COL318),ISNULL(@Col319,COL319),ISNULL(@Col320,COL320),ISNULL(@Col321,COL321),ISNULL(@Col322,COL322)
		,ISNULL(@Col323,COL323),ISNULL(@Col324,COL324),ISNULL(@Col325,COL325),ISNULL(@Col326,COL326),ISNULL(@Col327,COL327),ISNULL(@Col328,COL328),ISNULL(@Col329,COL329)
		,ISNULL(@Col330,COL330),ISNULL(@Col331,COL331),ISNULL(@Col332,COL332),ISNULL(@Col333,COL333),ISNULL(@Col334,COL334),ISNULL(@Col335,COL335),ISNULL(@Col336,COL336)
		,ISNULL(@Col337,COL337),ISNULL(@Col338,COL338),ISNULL(@Col339,COL339),ISNULL(@Col340,COL340),ISNULL(@Col341,COL341),ISNULL(@Col342,COL342),ISNULL(@Col343,COL343)
		,ISNULL(@Col344,COL344),ISNULL(@Col345,COL345),ISNULL(@Col346,COL346),ISNULL(@Col347,COL347),ISNULL(@Col348,COL348),ISNULL(@Col349,COL349),ISNULL(@Col350,COL350)
		,ISNULL(@Col351,COL351),ISNULL(@Col352,COL352),ISNULL(@Col353,COL353),ISNULL(@Col354,COL354),ISNULL(@Col355,COL355),ISNULL(@Col356,COL356),ISNULL(@Col357,COL357)
		,ISNULL(@Col358,COL358),ISNULL(@Col359,COL359),ISNULL(@Col360,COL360),ISNULL(@Col361,COL361),ISNULL(@Col362,COL362),ISNULL(@Col363,COL363),ISNULL(@Col364,COL364)
		,ISNULL(@Col365,COL365),ISNULL(@Col366,COL366),ISNULL(@Col367,COL367),ISNULL(@Col368,COL368),ISNULL(@Col369,COL369),ISNULL(@Col370,COL370),ISNULL(@Col371,COL371)
		,ISNULL(@Col372,COL372),ISNULL(@Col373,COL373),ISNULL(@Col374,COL374),ISNULL(@Col375,COL375),ISNULL(@Col376,COL376),ISNULL(@Col377,COL377),ISNULL(@Col378,COL378)
		,ISNULL(@Col379,COL379),ISNULL(@Col380,COL380),ISNULL(@Col381,COL381),ISNULL(@Col382,COL382),ISNULL(@Col383,COL383),ISNULL(@Col384,COL384),ISNULL(@Col385,COL385)
		,ISNULL(@Col386,COL386),ISNULL(@Col387,COL387),ISNULL(@Col388,COL388),ISNULL(@Col389,COL389),ISNULL(@Col390,COL390),ISNULL(@Col391,COL391),ISNULL(@Col392,COL392)
		,ISNULL(@Col393,COL393),ISNULL(@Col394,COL394),ISNULL(@Col395,COL395),ISNULL(@Col396,COL396),ISNULL(@Col397,COL397),ISNULL(@Col398,COL398),ISNULL(@Col399,COL399)
		,ISNULL(@Col400,COL400),ISNULL(@Col401,COL401),ISNULL(@Col402,COL402),ISNULL(@Col403,COL403),ISNULL(@Col404,COL404),ISNULL(@Col405,COL405),ISNULL(@Col406,COL406)
		,ISNULL(@Col407,COL407),ISNULL(@Col408,COL408),ISNULL(@Col409,COL409),ISNULL(@Col410,COL410),ISNULL(@Col411,COL411),ISNULL(@Col412,COL412),ISNULL(@Col413,COL413)
		,ISNULL(@Col414,COL414),ISNULL(@Col415,COL415),ISNULL(@Col416,COL416),ISNULL(@Col417,COL417),ISNULL(@Col418,COL418),ISNULL(@Col419,COL419),ISNULL(@Col420,COL420)
		,ISNULL(@Col421,COL421),ISNULL(@Col422,COL422),ISNULL(@Col423,COL423),ISNULL(@Col424,COL424),ISNULL(@Col425,COL425),ISNULL(@Col426,COL426),ISNULL(@Col427,COL427)
		,ISNULL(@Col428,COL428),ISNULL(@Col429,COL429),ISNULL(@Col430,COL430),ISNULL(@Col431,COL431),ISNULL(@Col432,COL432),ISNULL(@Col433,COL433),ISNULL(@Col434,COL434)
		,ISNULL(@Col435,COL435),ISNULL(@Col436,COL436),ISNULL(@Col437,COL437),ISNULL(@Col438,COL438),ISNULL(@Col439,COL439),ISNULL(@Col440,COL440),ISNULL(@Col441,COL441)
		,ISNULL(@Col442,COL442),ISNULL(@Col443,COL443),ISNULL(@Col444,COL444),ISNULL(@Col445,COL445),ISNULL(@Col446,COL446),ISNULL(@Col447,COL447),ISNULL(@Col448,COL448)
		,ISNULL(@Col449,COL449),ISNULL(@Col450,COL450),ISNULL(@Col451,COL451),ISNULL(@Col452,COL452),ISNULL(@Col453,COL453),ISNULL(@Col454,COL454),ISNULL(@Col455,COL455)
		,ISNULL(@Col456,COL456),ISNULL(@Col457,COL457),ISNULL(@Col458,COL458),ISNULL(@Col459,COL459),ISNULL(@Col460,COL460),ISNULL(@Col461,COL461),ISNULL(@Col462,COL462)
		,ISNULL(@Col463,COL463),ISNULL(@Col464,COL464),ISNULL(@Col465,COL465),ISNULL(@Col466,COL466),ISNULL(@Col467,COL467),ISNULL(@Col468,COL468),ISNULL(@Col469,COL469)
		,ISNULL(@Col470,COL470),ISNULL(@Col471,COL471),ISNULL(@Col472,COL472),ISNULL(@Col473,COL473),ISNULL(@Col474,COL474),ISNULL(@Col475,COL475),ISNULL(@Col476,COL476)
		,ISNULL(@Col477,COL477),ISNULL(@Col478,COL478),ISNULL(@Col479,COL479),ISNULL(@Col480,COL480),ISNULL(@Col481,COL481),ISNULL(@Col482,COL482),ISNULL(@Col483,COL483)
		,ISNULL(@Col484,COL484),ISNULL(@Col485,COL485),ISNULL(@Col486,COL486),ISNULL(@Col487,COL487),ISNULL(@Col488,COL488),ISNULL(@Col489,COL489),ISNULL(@Col490,COL490)
		,ISNULL(@Col491,COL491),ISNULL(@Col492,COL492),ISNULL(@Col493,COL493),ISNULL(@Col494,COL494),ISNULL(@Col495,COL495),ISNULL(@Col496,COL496),ISNULL(@Col497,COL497)
		,ISNULL(@Col498,COL498),ISNULL(@Col499,COL499),ISNULL(@Col500,COL500),ISNULL(@Col501,COL501),ISNULL(@Col502,COL502),ISNULL(@Col503,COL503),ISNULL(@Col504,COL504)
		,ISNULL(@Col505,COL505),ISNULL(@Col506,COL506),ISNULL(@Col507,COL507),ISNULL(@Col508,COL508),ISNULL(@Col509,COL509),ISNULL(@Col510,COL510),ISNULL(@Col511,COL511)
		,ISNULL(@Col512,COL512),ISNULL(@Col513,COL513),ISNULL(@Col514,COL514),ISNULL(@Col515,COL515),ISNULL(@Col516,COL516),ISNULL(@Col517,COL517),ISNULL(@Col518,COL518)
		,ISNULL(@Col519,COL519),ISNULL(@Col520,COL520),ISNULL(@Col521,COL521),ISNULL(@Col522,COL522),ISNULL(@Col523,COL523),ISNULL(@Col524,COL524),ISNULL(@Col525,COL525)
		,ISNULL(@Col526,COL526),ISNULL(@Col527,COL527),ISNULL(@Col528,COL528),ISNULL(@Col529,COL529),ISNULL(@Col530,COL530),ISNULL(@Col531,COL531),ISNULL(@Col532,COL532)
		,ISNULL(@Col533,COL533),ISNULL(@Col534,COL534),ISNULL(@Col535,COL535),ISNULL(@Col536,COL536),ISNULL(@Col537,COL537),ISNULL(@Col538,COL538),ISNULL(@Col539,COL539)
		,ISNULL(@Col540,COL540),ISNULL(@Col541,COL541),ISNULL(@Col542,COL542),ISNULL(@Col543,COL543),ISNULL(@Col544,COL544),ISNULL(@Col545,COL545),ISNULL(@Col546,COL546)
		,ISNULL(@Col547,COL547),ISNULL(@Col548,COL548),ISNULL(@Col549,COL549),ISNULL(@Col550,COL550),ISNULL(@Col551,COL551),ISNULL(@Col552,COL552),ISNULL(@Col553,COL553)
		,ISNULL(@Col554,COL554),ISNULL(@Col555,COL555),ISNULL(@Col556,COL556),ISNULL(@Col557,COL557),ISNULL(@Col558,COL558),ISNULL(@Col559,COL559),ISNULL(@Col560,COL560)
		,ISNULL(@Col561,COL561),ISNULL(@Col562,COL562),ISNULL(@Col563,COL563),ISNULL(@Col564,COL564),ISNULL(@Col565,COL565),ISNULL(@Col566,COL566),ISNULL(@Col567,COL567)
		,ISNULL(@Col568,COL568),ISNULL(@Col569,COL569),ISNULL(@Col570,COL570),ISNULL(@Col571,COL571),ISNULL(@Col572,COL572),ISNULL(@Col573,COL573),ISNULL(@Col574,COL574)
		,ISNULL(@Col575,COL575),ISNULL(@Col576,COL576),ISNULL(@Col577,COL577),ISNULL(@Col578,COL578),ISNULL(@Col579,COL579),ISNULL(@Col580,COL580),ISNULL(@Col581,COL581)
		,ISNULL(@Col582,COL582),ISNULL(@Col583,COL583),ISNULL(@Col584,COL584),ISNULL(@Col585,COL585),ISNULL(@Col586,COL586),ISNULL(@Col587,COL587),ISNULL(@Col588,COL588)
		,ISNULL(@Col589,COL589),ISNULL(@Col590,COL590),ISNULL(@Col591,COL591),ISNULL(@Col592,COL592),ISNULL(@Col593,COL593),ISNULL(@Col594,COL594),ISNULL(@Col595,COL595)
		,ISNULL(@Col596,COL596),ISNULL(@Col597,COL597),ISNULL(@Col598,COL598),ISNULL(@Col599,COL599),ISNULL(@Col600,COL600),ISNULL(@Col601,COL601),ISNULL(@Col602,COL602)
		,ISNULL(@Col603,COL603),ISNULL(@Col604,COL604),ISNULL(@Col605,COL605),ISNULL(@Col606,COL606),ISNULL(@Col607,COL607),ISNULL(@Col608,COL608),ISNULL(@Col609,COL609)
		,ISNULL(@Col610,COL610),ISNULL(@Col611,COL611),ISNULL(@Col612,COL612),ISNULL(@Col613,COL613),ISNULL(@Col614,COL614),ISNULL(@Col615,COL615),ISNULL(@Col616,COL616)
		,ISNULL(@Col617,COL617),ISNULL(@Col618,COL618),ISNULL(@Col619,COL619),ISNULL(@Col620,COL620),ISNULL(@Col621,COL621),ISNULL(@Col622,COL622),ISNULL(@Col623,COL623)
		,ISNULL(@Col624,COL624),ISNULL(@Col625,COL625),ISNULL(@Col626,COL626),ISNULL(@Col627,COL627),ISNULL(@Col628,COL628),ISNULL(@Col629,COL629),ISNULL(@Col630,COL630)
		,ISNULL(@Col631,COL631),ISNULL(@Col632,COL632),ISNULL(@Col633,COL633),ISNULL(@Col634,COL634),ISNULL(@Col635,COL635),ISNULL(@Col636,COL636),ISNULL(@Col637,COL637)
		,ISNULL(@Col638,COL638),ISNULL(@Col639,COL639),ISNULL(@Col640,COL640),ISNULL(@Col641,COL641),ISNULL(@Col642,COL642),ISNULL(@Col643,COL643),ISNULL(@Col644,COL644)
		,ISNULL(@Col645,COL645),ISNULL(@Col646,COL646),ISNULL(@Col647,COL647),ISNULL(@Col648,COL648),ISNULL(@Col649,COL649),ISNULL(@Col650,COL650),ISNULL(@Col651,COL651)
		,ISNULL(@Col652,COL652),ISNULL(@Col653,COL653),ISNULL(@Col654,COL654),ISNULL(@Col655,COL655),ISNULL(@Col656,COL656),ISNULL(@Col657,COL657),ISNULL(@Col658,COL658)
		,ISNULL(@Col659,COL659),ISNULL(@Col660,COL660),ISNULL(@Col661,COL661),ISNULL(@Col662,COL662),ISNULL(@Col663,COL663),ISNULL(@Col664,COL664),ISNULL(@Col665,COL665)
		,ISNULL(@Col666,COL666),ISNULL(@Col667,COL667),ISNULL(@Col668,COL668),ISNULL(@Col669,COL669),ISNULL(@Col670,COL670),ISNULL(@Col671,COL671),ISNULL(@Col672,COL672)
		,ISNULL(@Col673,COL673),ISNULL(@Col674,COL674),ISNULL(@Col675,COL675),ISNULL(@Col676,COL676),ISNULL(@Col677,COL677),ISNULL(@Col678,COL678),ISNULL(@Col679,COL679)
		,ISNULL(@Col680,COL680),ISNULL(@Col681,COL681),ISNULL(@Col682,COL682),ISNULL(@Col683,COL683),ISNULL(@Col684,COL684),ISNULL(@Col685,COL685),ISNULL(@Col686,COL686)
		,ISNULL(@Col687,COL687),ISNULL(@Col688,COL688),ISNULL(@Col689,COL689),ISNULL(@Col690,COL690),ISNULL(@Col691,COL691),ISNULL(@Col692,COL692),ISNULL(@Col693,COL693)
		,ISNULL(@Col694,COL694),ISNULL(@Col695,COL695),ISNULL(@Col696,COL696),ISNULL(@Col697,COL697),ISNULL(@Col698,COL698),ISNULL(@Col699,COL699),ISNULL(@Col700,COL700)
		,ISNULL(@Col701,COL701),ISNULL(@Col702,COL702),ISNULL(@Col703,COL703),ISNULL(@Col704,COL704),ISNULL(@Col705,COL705),ISNULL(@Col706,COL706),ISNULL(@Col707,COL707)
		,ISNULL(@Col708,COL708),ISNULL(@Col709,COL709),ISNULL(@Col710,COL710),ISNULL(@Col711,COL711),ISNULL(@Col712,COL712),ISNULL(@Col713,COL713),ISNULL(@Col714,COL714)
		,ISNULL(@Col715,COL715)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

	SELECT @CVID = ID FROM @IDs

	INSERT INTO dbo.ChannelValueDoor (ID,TimeStamp,UnitID,UpdateRecord
	 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
	 ,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
	,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
	,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
	,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
	,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
	,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
	,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
	,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
	,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
	,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
	,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
	,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
	,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
	,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
	,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
	,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
	,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
	,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
	,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
	,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
	,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
	,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
	,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
	,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
	,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
	,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
	,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
	,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
	,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
	,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
	,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
	,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
	,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
	,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
	,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
	,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
	,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
	,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
	,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
	,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
	,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
	,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
	,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
	,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
	,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
	,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
	,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
	,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
	,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
	,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
	,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
	,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
	,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
	,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
	,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
	,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
	,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
	,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
	,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
	,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
	,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
	,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
	,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
	,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
	,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
	,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
	,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
	,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
	,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
	,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
	,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
	,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
	,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
	,[Col3518],[Col3519]
	)
	SELECT Top 1
	@CVID
	, @Timestamp
	, @UnitID		
	, @UpdateRecord
	,ISNULL(@Col3000,COL3000),ISNULL(@Col3001,COL3001),ISNULL(@Col3002,COL3002),ISNULL(@Col3003,COL3003),ISNULL(@Col3004,COL3004),ISNULL(@Col3005,COL3005),ISNULL(@Col3006,COL3006)
	,ISNULL(@Col3007,COL3007),ISNULL(@Col3008,COL3008),ISNULL(@Col3009,COL3009),ISNULL(@Col3010,COL3010),ISNULL(@Col3011,COL3011),ISNULL(@Col3012,COL3012),ISNULL(@Col3013,COL3013)
	,ISNULL(@Col3014,COL3014),ISNULL(@Col3015,COL3015),ISNULL(@Col3016,COL3016),ISNULL(@Col3017,COL3017),ISNULL(@Col3018,COL3018),ISNULL(@Col3019,COL3019),ISNULL(@Col3020,COL3020)
	,ISNULL(@Col3021,COL3021),ISNULL(@Col3022,COL3022),ISNULL(@Col3023,COL3023),ISNULL(@Col3024,COL3024),ISNULL(@Col3025,COL3025),ISNULL(@Col3026,COL3026),ISNULL(@Col3027,COL3027)
	,ISNULL(@Col3028,COL3028),ISNULL(@Col3029,COL3029),ISNULL(@Col3030,COL3030),ISNULL(@Col3031,COL3031),ISNULL(@Col3032,COL3032),ISNULL(@Col3033,COL3033),ISNULL(@Col3034,COL3034)
	,ISNULL(@Col3035,COL3035),ISNULL(@Col3036,COL3036),ISNULL(@Col3037,COL3037),ISNULL(@Col3038,COL3038),ISNULL(@Col3039,COL3039),ISNULL(@Col3040,COL3040),ISNULL(@Col3041,COL3041)
	,ISNULL(@Col3042,COL3042),ISNULL(@Col3043,COL3043),ISNULL(@Col3044,COL3044),ISNULL(@Col3045,COL3045),ISNULL(@Col3046,COL3046),ISNULL(@Col3047,COL3047),ISNULL(@Col3048,COL3048)
	,ISNULL(@Col3049,COL3049),ISNULL(@Col3050,COL3050),ISNULL(@Col3051,COL3051),ISNULL(@Col3052,COL3052),ISNULL(@Col3053,COL3053),ISNULL(@Col3054,COL3054),ISNULL(@Col3055,COL3055)
	,ISNULL(@Col3056,COL3056),ISNULL(@Col3057,COL3057),ISNULL(@Col3058,COL3058),ISNULL(@Col3059,COL3059),ISNULL(@Col3060,COL3060),ISNULL(@Col3061,COL3061),ISNULL(@Col3062,COL3062)
	,ISNULL(@Col3063,COL3063),ISNULL(@Col3064,COL3064),ISNULL(@Col3065,COL3065),ISNULL(@Col3066,COL3066),ISNULL(@Col3067,COL3067),ISNULL(@Col3068,COL3068),ISNULL(@Col3069,COL3069)
	,ISNULL(@Col3070,COL3070),ISNULL(@Col3071,COL3071),ISNULL(@Col3072,COL3072),ISNULL(@Col3073,COL3073),ISNULL(@Col3074,COL3074),ISNULL(@Col3075,COL3075),ISNULL(@Col3076,COL3076)
	,ISNULL(@Col3077,COL3077),ISNULL(@Col3078,COL3078),ISNULL(@Col3079,COL3079),ISNULL(@Col3080,COL3080),ISNULL(@Col3081,COL3081),ISNULL(@Col3082,COL3082),ISNULL(@Col3083,COL3083)
	,ISNULL(@Col3084,COL3084),ISNULL(@Col3085,COL3085),ISNULL(@Col3086,COL3086),ISNULL(@Col3087,COL3087),ISNULL(@Col3088,COL3088),ISNULL(@Col3089,COL3089),ISNULL(@Col3090,COL3090)
	,ISNULL(@Col3091,COL3091),ISNULL(@Col3092,COL3092),ISNULL(@Col3093,COL3093),ISNULL(@Col3094,COL3094),ISNULL(@Col3095,COL3095),ISNULL(@Col3096,COL3096),ISNULL(@Col3097,COL3097)
	,ISNULL(@Col3098,COL3098),ISNULL(@Col3099,COL3099),ISNULL(@Col3100,COL3100),ISNULL(@Col3101,COL3101),ISNULL(@Col3102,COL3102),ISNULL(@Col3103,COL3103),ISNULL(@Col3104,COL3104)
	,ISNULL(@Col3105,COL3105),ISNULL(@Col3106,COL3106),ISNULL(@Col3107,COL3107),ISNULL(@Col3108,COL3108),ISNULL(@Col3109,COL3109),ISNULL(@Col3110,COL3110),ISNULL(@Col3111,COL3111)
	,ISNULL(@Col3112,COL3112),ISNULL(@Col3113,COL3113),ISNULL(@Col3114,COL3114),ISNULL(@Col3115,COL3115),ISNULL(@Col3116,COL3116),ISNULL(@Col3117,COL3117),ISNULL(@Col3118,COL3118)
	,ISNULL(@Col3119,COL3119),ISNULL(@Col3120,COL3120),ISNULL(@Col3121,COL3121),ISNULL(@Col3122,COL3122),ISNULL(@Col3123,COL3123),ISNULL(@Col3124,COL3124),ISNULL(@Col3125,COL3125)
	,ISNULL(@Col3126,COL3126),ISNULL(@Col3127,COL3127),ISNULL(@Col3128,COL3128),ISNULL(@Col3129,COL3129),ISNULL(@Col3130,COL3130),ISNULL(@Col3131,COL3131),ISNULL(@Col3132,COL3132)
	,ISNULL(@Col3133,COL3133),ISNULL(@Col3134,COL3134),ISNULL(@Col3135,COL3135),ISNULL(@Col3136,COL3136),ISNULL(@Col3137,COL3137),ISNULL(@Col3138,COL3138),ISNULL(@Col3139,COL3139)
	,ISNULL(@Col3140,COL3140),ISNULL(@Col3141,COL3141),ISNULL(@Col3142,COL3142),ISNULL(@Col3143,COL3143),ISNULL(@Col3144,COL3144),ISNULL(@Col3145,COL3145),ISNULL(@Col3146,COL3146)
	,ISNULL(@Col3147,COL3147),ISNULL(@Col3148,COL3148),ISNULL(@Col3149,COL3149),ISNULL(@Col3150,COL3150),ISNULL(@Col3151,COL3151),ISNULL(@Col3152,COL3152),ISNULL(@Col3153,COL3153)
	,ISNULL(@Col3154,COL3154),ISNULL(@Col3155,COL3155),ISNULL(@Col3156,COL3156),ISNULL(@Col3157,COL3157),ISNULL(@Col3158,COL3158),ISNULL(@Col3159,COL3159),ISNULL(@Col3160,COL3160)
	,ISNULL(@Col3161,COL3161),ISNULL(@Col3162,COL3162),ISNULL(@Col3163,COL3163),ISNULL(@Col3164,COL3164),ISNULL(@Col3165,COL3165),ISNULL(@Col3166,COL3166),ISNULL(@Col3167,COL3167)
	,ISNULL(@Col3168,COL3168),ISNULL(@Col3169,COL3169),ISNULL(@Col3170,COL3170),ISNULL(@Col3171,COL3171),ISNULL(@Col3172,COL3172),ISNULL(@Col3173,COL3173),ISNULL(@Col3174,COL3174)
	,ISNULL(@Col3175,COL3175),ISNULL(@Col3176,COL3176),ISNULL(@Col3177,COL3177),ISNULL(@Col3178,COL3178),ISNULL(@Col3179,COL3179),ISNULL(@Col3180,COL3180),ISNULL(@Col3181,COL3181)
	,ISNULL(@Col3182,COL3182),ISNULL(@Col3183,COL3183),ISNULL(@Col3184,COL3184),ISNULL(@Col3185,COL3185),ISNULL(@Col3186,COL3186),ISNULL(@Col3187,COL3187),ISNULL(@Col3188,COL3188)
	,ISNULL(@Col3189,COL3189),ISNULL(@Col3190,COL3190),ISNULL(@Col3191,COL3191),ISNULL(@Col3192,COL3192),ISNULL(@Col3193,COL3193),ISNULL(@Col3194,COL3194),ISNULL(@Col3195,COL3195)
	,ISNULL(@Col3196,COL3196),ISNULL(@Col3197,COL3197),ISNULL(@Col3198,COL3198),ISNULL(@Col3199,COL3199),ISNULL(@Col3200,COL3200),ISNULL(@Col3201,COL3201),ISNULL(@Col3202,COL3202)
	,ISNULL(@Col3203,COL3203),ISNULL(@Col3204,COL3204),ISNULL(@Col3205,COL3205),ISNULL(@Col3206,COL3206),ISNULL(@Col3207,COL3207),ISNULL(@Col3208,COL3208),ISNULL(@Col3209,COL3209)
	,ISNULL(@Col3210,COL3210),ISNULL(@Col3211,COL3211),ISNULL(@Col3212,COL3212),ISNULL(@Col3213,COL3213),ISNULL(@Col3214,COL3214),ISNULL(@Col3215,COL3215),ISNULL(@Col3216,COL3216)
	,ISNULL(@Col3217,COL3217),ISNULL(@Col3218,COL3218),ISNULL(@Col3219,COL3219),ISNULL(@Col3220,COL3220),ISNULL(@Col3221,COL3221),ISNULL(@Col3222,COL3222),ISNULL(@Col3223,COL3223)
	,ISNULL(@Col3224,COL3224),ISNULL(@Col3225,COL3225),ISNULL(@Col3226,COL3226),ISNULL(@Col3227,COL3227),ISNULL(@Col3228,COL3228),ISNULL(@Col3229,COL3229),ISNULL(@Col3230,COL3230)
	,ISNULL(@Col3231,COL3231),ISNULL(@Col3232,COL3232),ISNULL(@Col3233,COL3233),ISNULL(@Col3234,COL3234),ISNULL(@Col3235,COL3235),ISNULL(@Col3236,COL3236),ISNULL(@Col3237,COL3237)
	,ISNULL(@Col3238,COL3238),ISNULL(@Col3239,COL3239),ISNULL(@Col3240,COL3240),ISNULL(@Col3241,COL3241),ISNULL(@Col3242,COL3242),ISNULL(@Col3243,COL3243),ISNULL(@Col3244,COL3244)
	,ISNULL(@Col3245,COL3245),ISNULL(@Col3246,COL3246),ISNULL(@Col3247,COL3247),ISNULL(@Col3248,COL3248),ISNULL(@Col3249,COL3249),ISNULL(@Col3250,COL3250),ISNULL(@Col3251,COL3251)
	,ISNULL(@Col3252,COL3252),ISNULL(@Col3253,COL3253),ISNULL(@Col3254,COL3254),ISNULL(@Col3255,COL3255),ISNULL(@Col3256,COL3256),ISNULL(@Col3257,COL3257),ISNULL(@Col3258,COL3258)
	,ISNULL(@Col3259,COL3259),ISNULL(@Col3260,COL3260),ISNULL(@Col3261,COL3261),ISNULL(@Col3262,COL3262),ISNULL(@Col3263,COL3263),ISNULL(@Col3264,COL3264),ISNULL(@Col3265,COL3265)
	,ISNULL(@Col3266,COL3266),ISNULL(@Col3267,COL3267),ISNULL(@Col3268,COL3268),ISNULL(@Col3269,COL3269),ISNULL(@Col3270,COL3270),ISNULL(@Col3271,COL3271),ISNULL(@Col3272,COL3272)
	,ISNULL(@Col3273,COL3273),ISNULL(@Col3274,COL3274),ISNULL(@Col3275,COL3275),ISNULL(@Col3276,COL3276),ISNULL(@Col3277,COL3277),ISNULL(@Col3278,COL3278),ISNULL(@Col3279,COL3279)
	,ISNULL(@Col3280,COL3280),ISNULL(@Col3281,COL3281),ISNULL(@Col3282,COL3282),ISNULL(@Col3283,COL3283),ISNULL(@Col3284,COL3284),ISNULL(@Col3285,COL3285),ISNULL(@Col3286,COL3286)
	,ISNULL(@Col3287,COL3287),ISNULL(@Col3288,COL3288),ISNULL(@Col3289,COL3289),ISNULL(@Col3290,COL3290),ISNULL(@Col3291,COL3291),ISNULL(@Col3292,COL3292),ISNULL(@Col3293,COL3293)
	,ISNULL(@Col3294,COL3294),ISNULL(@Col3295,COL3295),ISNULL(@Col3296,COL3296),ISNULL(@Col3297,COL3297),ISNULL(@Col3298,COL3298),ISNULL(@Col3299,COL3299),ISNULL(@Col3300,COL3300)
	,ISNULL(@Col3301,COL3301),ISNULL(@Col3302,COL3302),ISNULL(@Col3303,COL3303),ISNULL(@Col3304,COL3304),ISNULL(@Col3305,COL3305),ISNULL(@Col3306,COL3306),ISNULL(@Col3307,COL3307)
	,ISNULL(@Col3308,COL3308),ISNULL(@Col3309,COL3309),ISNULL(@Col3310,COL3310),ISNULL(@Col3311,COL3311),ISNULL(@Col3312,COL3312),ISNULL(@Col3313,COL3313),ISNULL(@Col3314,COL3314)
	,ISNULL(@Col3315,COL3315),ISNULL(@Col3316,COL3316),ISNULL(@Col3317,COL3317),ISNULL(@Col3318,COL3318),ISNULL(@Col3319,COL3319),ISNULL(@Col3320,COL3320),ISNULL(@Col3321,COL3321)
	,ISNULL(@Col3322,COL3322),ISNULL(@Col3323,COL3323),ISNULL(@Col3324,COL3324),ISNULL(@Col3325,COL3325),ISNULL(@Col3326,COL3326),ISNULL(@Col3327,COL3327),ISNULL(@Col3328,COL3328)
	,ISNULL(@Col3329,COL3329),ISNULL(@Col3330,COL3330),ISNULL(@Col3331,COL3331),ISNULL(@Col3332,COL3332),ISNULL(@Col3333,COL3333),ISNULL(@Col3334,COL3334),ISNULL(@Col3335,COL3335)
	,ISNULL(@Col3336,COL3336),ISNULL(@Col3337,COL3337),ISNULL(@Col3338,COL3338),ISNULL(@Col3339,COL3339),ISNULL(@Col3340,COL3340),ISNULL(@Col3341,COL3341),ISNULL(@Col3342,COL3342)
	,ISNULL(@Col3343,COL3343),ISNULL(@Col3344,COL3344),ISNULL(@Col3345,COL3345),ISNULL(@Col3346,COL3346),ISNULL(@Col3347,COL3347),ISNULL(@Col3348,COL3348),ISNULL(@Col3349,COL3349)
	,ISNULL(@Col3350,COL3350),ISNULL(@Col3351,COL3351),ISNULL(@Col3352,COL3352),ISNULL(@Col3353,COL3353),ISNULL(@Col3354,COL3354),ISNULL(@Col3355,COL3355),ISNULL(@Col3356,COL3356)
	,ISNULL(@Col3357,COL3357),ISNULL(@Col3358,COL3358),ISNULL(@Col3359,COL3359),ISNULL(@Col3360,COL3360),ISNULL(@Col3361,COL3361),ISNULL(@Col3362,COL3362),ISNULL(@Col3363,COL3363)
	,ISNULL(@Col3364,COL3364),ISNULL(@Col3365,COL3365),ISNULL(@Col3366,COL3366),ISNULL(@Col3367,COL3367),ISNULL(@Col3368,COL3368),ISNULL(@Col3369,COL3369),ISNULL(@Col3370,COL3370)
	,ISNULL(@Col3371,COL3371),ISNULL(@Col3372,COL3372),ISNULL(@Col3373,COL3373),ISNULL(@Col3374,COL3374),ISNULL(@Col3375,COL3375),ISNULL(@Col3376,COL3376),ISNULL(@Col3377,COL3377)
	,ISNULL(@Col3378,COL3378),ISNULL(@Col3379,COL3379),ISNULL(@Col3380,COL3380),ISNULL(@Col3381,COL3381),ISNULL(@Col3382,COL3382),ISNULL(@Col3383,COL3383),ISNULL(@Col3384,COL3384)
	,ISNULL(@Col3385,COL3385),ISNULL(@Col3386,COL3386),ISNULL(@Col3387,COL3387),ISNULL(@Col3388,COL3388),ISNULL(@Col3389,COL3389),ISNULL(@Col3390,COL3390),ISNULL(@Col3391,COL3391)
	,ISNULL(@Col3392,COL3392),ISNULL(@Col3393,COL3393),ISNULL(@Col3394,COL3394),ISNULL(@Col3395,COL3395),ISNULL(@Col3396,COL3396),ISNULL(@Col3397,COL3397),ISNULL(@Col3398,COL3398)
	,ISNULL(@Col3399,COL3399),ISNULL(@Col3400,COL3400),ISNULL(@Col3401,COL3401),ISNULL(@Col3402,COL3402),ISNULL(@Col3403,COL3403),ISNULL(@Col3404,COL3404),ISNULL(@Col3405,COL3405)
	,ISNULL(@Col3406,COL3406),ISNULL(@Col3407,COL3407),ISNULL(@Col3408,COL3408),ISNULL(@Col3409,COL3409),ISNULL(@Col3410,COL3410),ISNULL(@Col3411,COL3411),ISNULL(@Col3412,COL3412)
	,ISNULL(@Col3413,COL3413),ISNULL(@Col3414,COL3414),ISNULL(@Col3415,COL3415),ISNULL(@Col3416,COL3416),ISNULL(@Col3417,COL3417),ISNULL(@Col3418,COL3418),ISNULL(@Col3419,COL3419)
	,ISNULL(@Col3420,COL3420),ISNULL(@Col3421,COL3421),ISNULL(@Col3422,COL3422),ISNULL(@Col3423,COL3423),ISNULL(@Col3424,COL3424),ISNULL(@Col3425,COL3425),ISNULL(@Col3426,COL3426)
	,ISNULL(@Col3427,COL3427),ISNULL(@Col3428,COL3428),ISNULL(@Col3429,COL3429),ISNULL(@Col3430,COL3430),ISNULL(@Col3431,COL3431),ISNULL(@Col3432,COL3432),ISNULL(@Col3433,COL3433)
	,ISNULL(@Col3434,COL3434),ISNULL(@Col3435,COL3435),ISNULL(@Col3436,COL3436),ISNULL(@Col3437,COL3437),ISNULL(@Col3438,COL3438),ISNULL(@Col3439,COL3439),ISNULL(@Col3440,COL3440)
	,ISNULL(@Col3441,COL3441),ISNULL(@Col3442,COL3442),ISNULL(@Col3443,COL3443),ISNULL(@Col3444,COL3444),ISNULL(@Col3445,COL3445),ISNULL(@Col3446,COL3446),ISNULL(@Col3447,COL3447)
	,ISNULL(@Col3448,COL3448),ISNULL(@Col3449,COL3449),ISNULL(@Col3450,COL3450),ISNULL(@Col3451,COL3451),ISNULL(@Col3452,COL3452),ISNULL(@Col3453,COL3453),ISNULL(@Col3454,COL3454)
	,ISNULL(@Col3455,COL3455),ISNULL(@Col3456,COL3456),ISNULL(@Col3457,COL3457),ISNULL(@Col3458,COL3458),ISNULL(@Col3459,COL3459),ISNULL(@Col3460,COL3460),ISNULL(@Col3461,COL3461)
	,ISNULL(@Col3462,COL3462),ISNULL(@Col3463,COL3463),ISNULL(@Col3464,COL3464),ISNULL(@Col3465,COL3465),ISNULL(@Col3466,COL3466),ISNULL(@Col3467,COL3467),ISNULL(@Col3468,COL3468)
	,ISNULL(@Col3469,COL3469),ISNULL(@Col3470,COL3470),ISNULL(@Col3471,COL3471),ISNULL(@Col3472,COL3472),ISNULL(@Col3473,COL3473),ISNULL(@Col3474,COL3474),ISNULL(@Col3475,COL3475)
	,ISNULL(@Col3476,COL3476),ISNULL(@Col3477,COL3477),ISNULL(@Col3478,COL3478),ISNULL(@Col3479,COL3479),ISNULL(@Col3480,COL3480),ISNULL(@Col3481,COL3481),ISNULL(@Col3482,COL3482)
	,ISNULL(@Col3483,COL3483),ISNULL(@Col3484,COL3484),ISNULL(@Col3485,COL3485),ISNULL(@Col3486,COL3486),ISNULL(@Col3487,COL3487),ISNULL(@Col3488,COL3488),ISNULL(@Col3489,COL3489)
	,ISNULL(@Col3490,COL3490),ISNULL(@Col3491,COL3491),ISNULL(@Col3492,COL3492),ISNULL(@Col3493,COL3493),ISNULL(@Col3494,COL3494),ISNULL(@Col3495,COL3495),ISNULL(@Col3496,COL3496)
	,ISNULL(@Col3497,COL3497),ISNULL(@Col3498,COL3498),ISNULL(@Col3499,COL3499),ISNULL(@Col3500,COL3500),ISNULL(@Col3501,COL3501),ISNULL(@Col3502,COL3502),ISNULL(@Col3503,COL3503)
	,ISNULL(@Col3504,COL3504),ISNULL(@Col3505,COL3505),ISNULL(@Col3506,COL3506),ISNULL(@Col3507,COL3507),ISNULL(@Col3508,COL3508),ISNULL(@Col3509,COL3509),ISNULL(@Col3510,COL3510)
	,ISNULL(@Col3511,COL3511),ISNULL(@Col3512,COL3512),ISNULL(@Col3513,COL3513),ISNULL(@Col3514,COL3514),ISNULL(@Col3515,COL3515),ISNULL(@Col3516,COL3516),ISNULL(@Col3517,COL3517)
	,ISNULL(@Col3518,COL3518),ISNULL(@Col3519,COL3519)
	FROM dbo.ChannelValueDoor WITH (NOLOCK) 
	WHERE UnitID = @UnitID
	ORDER BY 1 DESC
END
GO

------------------------------------------
-- Update NSP_InsertFault with new columns
------------------------------------------

RAISERROR ('-- Update NSP_InsertFault', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
GO


ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitID int
    , @RuleName varchar(255)
    , @Context varchar(50)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
    DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
    DECLARE @faultCount int

    IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
            @faultMetaID = ID 
            , @faultMetaRecovery = RecoveryProcessPath
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
        , @faultLocationID = l.ID
        FROM dbo.ChannelValue
        LEFT JOIN dbo.Location l ON l.ID = (
            SELECT TOP 1 LocationID 
            FROM dbo.LocationArea 
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
         
         -- get current count of faults
        SELECT TOP 1 
            @faultCount = FaultCounter 
            FROM FaultCount fc 
            WHERE fc.UnitID = @faultUnitID 
            AND fc.FaultMetaID = @faultMetaID

        -- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
        IF @faultCount IS NULL
        BEGIN
            INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
            VALUES (@faultUnitID, @faultMetaID, 0)

            SET @faultCount = 0
        END
        
        SET @faultCount = @faultCount + 1   
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO dbo.[Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[RuleName]
            ,[IsDelayed]
            ,[CountSinceLastMaint]
            ,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,@RuleName
            ,ISNULL(@IsDelayed, 0)
            ,@faultCount
            ,RecoveryStatus = CASE
                WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
                ELSE NULL
            
            END

        -- increment the fault counter
        UPDATE dbo.[FaultCount]
        SET FaultCounter = @faultCount
        WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
    
    --insert Channel data
    INSERT INTO dbo.[FaultChannelValue]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
         ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
            [Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
            [Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
            [Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
            [Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
            [Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
            [Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
            [Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
            [Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
            [Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
            [Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
            [Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
            [Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
            [Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
            [Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
            [Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
            [Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
            [Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
            [Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
            [Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
            [Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
            [Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
            [Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
            [Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
            [Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
            [Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
            [Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
            [Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
            [Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],   
            [Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
            [Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
            [Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
            [Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
            [Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
            [Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
            [Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
            [Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
            [Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
            [Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663] ,[Col664], [Col665], [Col666], [Col667],
            [Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
            [Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
            [Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715])
        SELECT
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
          ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
        [Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
        [Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
        [Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
        [Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
        [Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
        [Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
        [Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
        [Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
        [Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
        [Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
        [Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
        [Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
        [Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
        [Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
        [Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
        [Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
        [Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
        [Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
        [Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
        [Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
        [Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
        [Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
        [Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
        [Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
        [Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
        [Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
        [Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
        [Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],   
        [Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
        [Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
        [Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
        [Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
        [Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
        [Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
        [Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
        [Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
        [Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
        [Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
        [Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
        [Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
        [Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]

    FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


    INSERT INTO dbo.[FaultChannelValueDoor]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
         ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
        )
        SELECT
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
         ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
        

    FROM dbo.ChannelValueDoor
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


    -- returning faultID
    RETURN @faultID
    
END
GO

----------------------------------------------------------------------------
-- update NSP_HistoricChannelValue with new columns
----------------------------------------------------------------------------

RAISERROR ('-- update NSP_HistoricChannelValue', 0, 1) WITH NOWAIT
GO

-- update NSP_HistoricChannelValue to contain specific (P1,P2, etc) FaultNumber values
ALTER PROCEDURE [dbo].[NSP_HistoricChannelValue]
    @DateTimeStart datetime
    , @UnitID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @UnitID IS NULL
    BEGIN
        PRINT 'Unit Number not in a database'
        RETURN -1 
    END


    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, GETDATE())

    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        FaultUnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
		, P1FaultNumber int NULL
		, P2FaultNumber int NULL
		, P3FaultNumber int NULL
		, P4FaultNumber int NULL
		, P5FaultNumber int NULL
		, RecoveryNumber int NULL
		, NotAcknowledgedNumber int NULL
    )
        
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
		, SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
		, SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
		, SUM(CASE FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
		, SUM(CASE FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
		, SUM(CASE FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
		, SUM(CASE HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
		, SUM(CASE IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
    FROM dbo.VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID  = @UnitID
        AND Category <> 'Test'
        AND ReportingOnly = 0
    GROUP BY FaultUnitID

    ;WITH CteLatestRecords 
    AS
    (
        SELECT 
            Unit.ID AS UnitID
            , (
                SELECT TOP 1 TimeStamp
                FROM dbo.ChannelValue
                WHERE TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
                    AND UnitID =    Unit.ID 
                ORDER BY TimeStamp DESC
            ) AS MaxChannelValueTimeStamp
        FROM dbo.Unit
        WHERE ID = @UnitID
    )   

    SELECT 
        UnitNumber  = u.UnitNumber
        , UnitID    = u.ID
        , UnitStatus = u.UnitStatus
        , UnitMaintLoc = u.UnitMaintLoc
		, LastMaintDate = CAST(u.[LastMaintDate] AS datetime)
        , Headcode  = fs.Headcode
        , Diagram   = fs.Diagram
        , FaultNumber   = ISNULL(vf.FaultNumber,0)
        , WarningNumber = ISNULL(vf.WarningNumber,0)
		, P1FaultNumber		= ISNULL(vf.P1FaultNumber,0)
		, P2FaultNumber		= ISNULL(vf.P2FaultNumber,0)
		, P3FaultNumber		= ISNULL(vf.P3FaultNumber,0)
		, P4FaultNumber		= ISNULL(vf.P4FaultNumber,0)
		, P5FaultNumber		= ISNULL(vf.P5FaultNumber,0)
		, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
		, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
                AND LocationArea.Type = 'C'
            ORDER BY Priority
        )
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,fs.FleetCode
        ,TimeStamp      = CAST(cv.[TimeStamp] AS datetime)

        -- Channel Data 
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
			,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
			,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
			,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
			,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
			,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
			,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
			,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
			,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
			,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
			,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
			,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
			,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
			,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
			,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
			,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
			,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
			,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
			,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
			,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
			,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
			,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
			,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
			,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
			,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
			,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
			,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
			,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
			,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
			,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
			,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
			,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
			,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
			,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
			,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
			,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
			,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
			,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
			,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
			,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
			,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
			,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
			,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
			,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
			,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
			,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
			,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
			,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
			,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
			,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
			,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
			,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
			,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
			,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
			,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
			,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
			,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
			,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
			,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
			,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
			,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
			,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
			,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
			,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
			,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
			,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
			,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
			,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
			,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
			,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
			,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
			,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
			,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
			,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
			,[Col3518],[Col3519] 
    FROM dbo.Unit u 
    INNER JOIN CteLatestRecords     ON u.ID = CteLatestRecords.unitID
    LEFT JOIN dbo.ChannelValue cv       ON u.ID = cv.UnitID  AND CteLatestRecords.MaxChannelValueTimeStamp = cv.TimeStamp
	LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.ID = cv.ID
    LEFT JOIN @currentVehicleFault vf ON u.ID = vf.FaultUnitID
    LEFT JOIN dbo.VW_FleetStatus fs     ON u.ID = fs.UnitID

END
GO

---------------------------------------------------------------------------------------
-- update NSP_GetFaultChannelValueByID with new columns 
---------------------------------------------------------------------------------------
RAISERROR ('-- Update NSP_GetFaultChannelValueByID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueByID AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelValueByID]
(
    @ID bigint
)
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.ChannelValue), 0) - 1;
    ELSE
        SET @ID = (
        SELECT TOP 1 (ChannelValue.ID - 1) 
        FROM dbo.ChannelValue WITH(NOLOCK)
        WHERE ChannelValue.ID > @ID
        ORDER BY ChannelValue.ID ASC
        )
        
    DECLARE @dataSetSize int = 5000
    
    SELECT 
        cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , UnitStatus    = u.UnitStatus
        , UnitMaintLoc  = u.UnitMaintLoc
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime, cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,

        -- Channel Values
       [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
			,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
			,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
			,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
			,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
			,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
			,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
			,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
			,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
			,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
			,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
			,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
			,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
			,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
			,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
			,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
			,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
			,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
			,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
			,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
			,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
			,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
			,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
			,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
			,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
			,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
			,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
			,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
			,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
			,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
			,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
			,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
			,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
			,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
			,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
			,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
			,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
			,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
			,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
			,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
			,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
			,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
			,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
			,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
			,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
			,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
			,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
			,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
			,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
			,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
			,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
			,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
			,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
			,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
			,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
			,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
			,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
			,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
			,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
			,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
			,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
			,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
			,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
			,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
			,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
			,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
			,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
			,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
			,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
			,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
			,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
			,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
			,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
			,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
			,[Col3518],[Col3519]

    FROM dbo.ChannelValue cv WITH(INDEX(PK_ChannelValue))
	INNER JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.ID = cv.ID
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN dbo.FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, GETDATE())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
    WHERE cv.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
    ORDER BY ID

END
GO

------------------------------------------------------------
-- update NSP_GetFaultChannelData with new columns
------------------------------------------------------------

RAISERROR ('-- update NSP_GetFaultChannelData', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelData AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetFaultChannelData]
(
	@FaultID int
	, @UnitID int
)
AS
BEGIN

		SELECT
			cv.[ID]
			,cv.[FaultID]
			,cv.[UpdateRecord]
			,cv.[RecordInsert]
			,cv.[TimeStamp]
			,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
			,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
			,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
			,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
			,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
			,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
			,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
			,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
			,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
			,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
			,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
			,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
			,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
			,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
			,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
			,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
			,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
			,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
			,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
			,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
			,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
			,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
			,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
			,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
			,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
			,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
			,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
			,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
			,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
			,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
			,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
			,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
			,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
			,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
			,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
			,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
			,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
			,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
			,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
			,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
			,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
			,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
			,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
			,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
			,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
			,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
			,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
			,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
			,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
			,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
			,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
			,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
			,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
			,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
			,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
			,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
			,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
			,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
			,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
			,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
			,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
			,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
			,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
			,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
			,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
			,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
			,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
			,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
			,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
			,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
			,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
			,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
			,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
			,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
			,[Col3518],[Col3519]
		FROM dbo.FaultChannelValue cv
		LEFT JOIN dbo.FaultChannelValueDoor cvd ON cvd.ID = cv.ID
		WHERE cv.FaultID = @FaultID
			AND cv.UnitID = @UnitID

END
GO

------------------------------------------------------------
-- update NSP_GetFaultChannelValue with new columns
------------------------------------------------------------

RAISERROR ('-- ALTER NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetFaultChannelValue] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue]
    @DatetimeStart datetime
    , @DatetimeEnd datetime
    , @UnitID int = NULL
    , @UpdateRecord bit = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        cv.UnitID
        , Headcode = CAST(NULL AS varchar(7))
        , u.UnitNumber
        , u.UnitType
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = Location.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , [TimeStamp]           = CONVERT(datetime, cv.TimeStamp)
        , Location.ID AS LocationID
        , Location.Tiploc AS Tiploc
        , Location.LocationName AS LocationName
        ,
        -- Channel Values
		[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
			,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
			,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
			,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
			,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
			,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
			,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
			,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
			,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
			,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
			,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
			,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
			,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
			,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
			,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
			,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
			,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
			,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
			,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
			,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
			,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
			,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
			,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
			,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
			,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
			,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
			,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
			,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
			,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
			,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
			,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
			,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
			,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
			,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
			,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
			,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
			,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
			,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
			,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
			,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
			,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
			,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
			,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
			,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
			,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
			,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
			,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
			,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
			,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
			,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
			,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
			,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
			,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
			,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
			,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
			,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
			,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
			,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
			,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
			,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
			,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
			,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
			,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
			,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
			,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
			,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
			,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
			,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
			,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
			,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
			,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
			,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
			,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
			,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
			,[Col3518],[Col3519]
    FROM dbo.ChannelValue cv WITH (NOLOCK)
	INNER JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.ID = cv.ID
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.FleetStatus fs ON
        cv.TimeStamp > ValidFrom 
        AND cv.TimeStamp <= ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
        AND cv.UnitID = fs.UnitID
    LEFT JOIN dbo.Location ON Location.ID = (
                                        SELECT TOP 1 LocationID 
                                        FROM dbo.LocationArea 
                                        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
                                            AND col2 BETWEEN MinLongitude AND MaxLongitude
                                        ORDER BY Priority
                                    )
    WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
        AND (cv.UnitID = @UnitID OR @UnitID IS NULL)
        AND (cv.UpdateRecord = @UpdateRecord OR @UpdateRecord IS NULL)
    ORDER BY TimeStamp

END
GO

------------------------------------------------------------
-- update NSP_GetLastChannelDataForVehicle with new columns
------------------------------------------------------------

RAISERROR ('-- ALTER NSP_GetLastChannelDataForVehicle', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_GetLastChannelDataForVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')
    EXEC ('CREATE PROCEDURE dbo.[NSP_GetLastChannelDataForVehicle] AS BEGIN RETURN(1) END;')
GO

ALTER  PROCEDURE [dbo].[NSP_GetLastChannelDataForVehicle]
    @DateTimeStart datetime
    , @VehicleID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, GETDATE())
                
    ;WITH CteLatestRecords 
    AS
    (
		SELECT 
			VehicleID	= Vehicle.ID
			, UnitID	= Vehicle.UnitID
			, LastTimeStamp = (
				SELECT TOP 1 TimeStamp
				FROM dbo.ChannelValue
				WHERE  TimeStamp BETWEEN DATEADD(d, -7, @DateTimeStart) AND @DateTimeStart
					AND ChannelValue.UnitID = Vehicle.UnitID 
				ORDER BY TimeStamp DESC
			)
		FROM VW_Vehicle AS Vehicle
		WHERE Vehicle.ID = @VehicleID
    )   

    SELECT 
        UnitNumber
        , Headcode	= CONVERT(varchar(10), NULL)
		, Diagram 	= CONVERT(varchar(10), NULL)
        , Vehicle.ID AS VehicleID
        , Vehicle.Type AS VehicleType
        , Vehicle.VehicleTypeID AS VehicleTypeID
        , Vehicle.VehicleNumber AS VehicleNumber
        , (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM dbo.Location
            INNER JOIN dbo.LocationArea ON LocationID = Location.ID
            WHERE col3 BETWEEN MinLatitude AND MaxLatitude
                AND col4 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) AS Location
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp      
		,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
			,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
			,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
			,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
			,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
			,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
			,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
			,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
			,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
			,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
			,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
			,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
			,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
			,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
			,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
			,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
			,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
			,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
			,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
			,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
			,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
			,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
			,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
			,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
			,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
			,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
			,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
			,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
			,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
			,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
			,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
			,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
			,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
			,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
			,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
			,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
			,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
			,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
			,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
			,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
			,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
			,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
			,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
			,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
			,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
			,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
			,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
			,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
			,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
			,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
			,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
			,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
			,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
			,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
			,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
			,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
			,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
			,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
			,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
			,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
			,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
			,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
			,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
			,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
			,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
			,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
			,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
			,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
			,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
			,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
			,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
			,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
			,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
			,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
			,[Col3518],[Col3519]
    FROM dbo.VW_Vehicle Vehicle 
    INNER JOIN CteLatestRecords     ON Vehicle.UnitID = CteLatestRecords.UnitID
    LEFT JOIN dbo.ChannelValue cv       ON Vehicle.UnitID = cv.UnitID  AND CteLatestRecords.LastTimeStamp = cv.TimeStamp
	LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.ID = cv.ID

END
GO

----------------------------------------------------------------
-- Update NSP_InsertChannelValue_Text with new columns
----------------------------------------------------------------

RAISERROR ('-- Update NSP_InsertChannelValue_Text', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertChannelValue_Text' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue_Text AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue_Text](
		@Timestamp datetime2(3),
		@UnitNumber varchar(16),
	    @UpdateRecord bit 
,@MCG_ILatitude decimal(9,6) = NULL ,@MCG_ILongitude decimal(9,6) = NULL ,@DIRECTION_CONTROL bit = NULL ,@CURRENT_SPEED smallint = NULL ,@AUX1_CDeratingReq_1_1 bit=NULL ,@AUX1_I3ACContactorOn_1_1 bit = NULL 
,@AUX1_I3ACContactorOn_2_1 bit = NULL ,@AUX1_I3ACContactorOn_2_2 bit = NULL ,@AUX1_I3ACLoadEnabled_1_1 bit = NULL ,@AUX1_I3ACOk_1_1 bit = NULL ,@AUX1_IInputVoltageOk_1_1 bit = NULL 
,@AUX1_IInverterEnabled_1_1 bit = NULL ,@AUX1_ILineContactorOn_1_1 bit = NULL ,@AUX1_IOutputVoltageOk_1_1 bit = NULL ,@AUX1_IPreChargContOn_1_1 bit = NULL ,@AUX1_ITempHeatSinkOk_1_1 bit = NULL 
,@AUX2_CDeratingReq_1_2 bit = NULL ,@AUX2_I3ACContactorOn_1_2 bit = NULL ,@AUX2_I3ACContactorOn_2_1 bit = NULL ,@AUX2_I3ACContactorOn_2_2 bit = NULL ,@AUX2_I3ACLoadEnabled_1_2 bit = NULL 
,@AUX2_I3ACOk_1_2 bit = NULL ,@AUX2_IInputVoltageOk_1_2 bit = NULL ,@AUX2_IInverterEnabled_1_2 bit = NULL ,@AUX2_ILineContactorOn_1_2 bit = NULL ,@AUX2_IOutputVoltageOk_1_2 bit = NULL 
,@AUX2_IPreChargContOn_1_2 bit = NULL ,@AUX2_ITempHeatSinkOk_1_2 bit = NULL ,@AUXY_IBatCont1On bit = NULL ,@AUXY_IBatCont1On_1_1 bit = NULL ,@AUXY_IBatCont1On_1_2 bit = NULL 
,@AUXY_IBatCont1On_2_1 bit = NULL ,@AUXY_IBatCont1On_2_2 bit = NULL ,@AUXY_IBatCont1On_3_1 bit = NULL ,@AUXY_IBatCont1On_3_2 bit = NULL ,@AUXY_IBatCont1On_8_1 bit = NULL 
,@AUXY_IBatCont1On_8_2 bit = NULL ,@AUXY_IBatCont1On_8_3 bit = NULL ,@AUXY_IBatCont1On_8_4 bit = NULL ,@AUXY_IBatCont1On_8_5 bit = NULL ,@AUXY_IBatCont1On_8_6 bit = NULL 
,@AUXY_IBatCont1On_8_7 bit = NULL ,@AUXY_IBatCont1On_8_8 bit = NULL ,@AUXY_IBatCont1On_9 bit = NULL ,@AUXY_IBatCont2On bit = NULL ,@AUXY_IBatCont2On_1_1 bit = NULL 
,@AUXY_IBatCont2On_1_2 bit = NULL ,@AUXY_IBatCont2On_2_1 bit = NULL ,@AUXY_IBatCont2On_2_2 bit = NULL ,@AUXY_IBatCont2On_3_1 bit = NULL ,@AUXY_IBatCont2On_3_2 bit = NULL 
,@AUXY_IBatCont2On_8_1 bit = NULL ,@AUXY_IBatCont2On_8_2 bit = NULL ,@AUXY_IBatCont2On_8_3 bit = NULL ,@AUXY_IBatCont2On_8_4 bit = NULL ,@AUXY_IBatCont2On_8_5 bit = NULL 
,@AUXY_IBatCont2On_8_6 bit = NULL ,@AUXY_IBatCont2On_8_7 bit = NULL ,@AUXY_IBatCont2On_8_8 bit = NULL ,@AUXY_IBatCont2On_9 bit = NULL ,@BCHA1_I110VAuxPowSup_2_1 decimal(5,1) = NULL 
,@BCHA1_IBattCurr_2_1 decimal(5,1) = NULL ,@BCHA1_IBattOutputCurr_2_1 decimal(5,1) = NULL ,@BCHA1_IChargCurrMax_2_1 decimal(5,1) = NULL ,@BCHA1_IDcLinkVoltage_2_1 decimal(5,1) = NULL ,@BCHA1_IExternal400Vac bit = NULL 
,@BCHA1_IExternal400Vac_1_1 bit = NULL ,@BCHA1_IExternal400Vac_1_2 bit = NULL ,@BCHA1_IExternal400Vac_2_1 bit = NULL ,@BCHA1_IExternal400Vac_2_2 bit = NULL ,@BCHA1_IExternal400Vac_3_1 bit = NULL 
,@BCHA1_IExternal400Vac_3_2 bit = NULL ,@BCHA1_IExternal400Vac_8_1 bit = NULL ,@BCHA1_IExternal400Vac_8_2 bit = NULL ,@BCHA1_IExternal400Vac_8_3 bit = NULL ,@BCHA1_IExternal400Vac_8_4 bit = NULL 
,@BCHA1_IExternal400Vac_8_5 bit = NULL ,@BCHA1_IExternal400Vac_8_6 bit = NULL ,@BCHA1_IExternal400Vac_8_7 bit = NULL ,@BCHA1_IExternal400Vac_8_8 bit = NULL ,@BCHA1_IExternal400Vac_9 bit = NULL 
,@BCHA1_IOutputPower_2_1 smallint = NULL ,@BCHA1_ISpecSystNokMod_2_1 tinyint = NULL ,@BCHA1_ISpecSystOkMode_2_1 tinyint = NULL ,@BCHA1_ITempBattSens1_2_1 smallint = NULL ,@BCHA1_ITempBattSens2_2_1 smallint = NULL 
,@BCHA1_ITempHeatSink_2_1 smallint = NULL ,@BCHA2_I110VAuxPowSup_2_2 decimal(5,1) = NULL ,@BCHA2_IBattCurr_2_2 decimal(5,1) = NULL ,@BCHA2_IBattOutputCurr_2_2 decimal(5,1) = NULL ,@BCHA2_IChargCurrMax_2_2 decimal(5,1) = NULL 
,@BCHA2_IDcLinkVoltage_2_2 decimal(5,1) = NULL ,@BCHA2_IExternal400Vac bit = NULL ,@BCHA2_IExternal400Vac_1_1 bit = NULL ,@BCHA2_IExternal400Vac_1_2 bit = NULL ,@BCHA2_IExternal400Vac_2_1 bit = NULL 
,@BCHA2_IExternal400Vac_2_2 bit = NULL ,@BCHA2_IExternal400Vac_3_1 bit = NULL ,@BCHA2_IExternal400Vac_3_2 bit = NULL ,@BCHA2_IExternal400Vac_8_1 bit = NULL ,@BCHA2_IExternal400Vac_8_2 bit = NULL 
,@BCHA2_IExternal400Vac_8_3 bit = NULL ,@BCHA2_IExternal400Vac_8_4 bit = NULL ,@BCHA2_IExternal400Vac_8_5 bit = NULL ,@BCHA2_IExternal400Vac_8_6 bit = NULL ,@BCHA2_IExternal400Vac_8_7 bit = NULL 
,@BCHA2_IExternal400Vac_8_8 bit = NULL ,@BCHA2_IExternal400Vac_9 bit = NULL ,@BCHA2_IOutputPower_2_2 smallint = NULL ,@BCHA2_ISpecSystNokMod_2_2 tinyint = NULL ,@BCHA2_ISpecSystOkMode_2_2 tinyint = NULL 
,@BCHA2_ITempBattSens1_2_2 smallint = NULL ,@BCHA2_ITempBattSens2_2_2 smallint = NULL ,@BCHA2_ITempHeatSink_2_2 smallint = NULL ,@BCU1_IBrPipePressureInt_5_1 decimal(5,1) = NULL ,@BCU1_IMainAirResPipeInt_5_1 decimal(5,1) = NULL 
,@BCU1_IParkBrApplied_5_1 bit = NULL ,@BCU1_IParkBrLocked_5_1 bit = NULL ,@BCU1_IParkBrReleased_5_1 bit = NULL ,@BCU2_IBrPipePressureInt_5_2 decimal(5,1) = NULL ,@BCU2_IMainAirResPipeInt_5_2 decimal(5,1) = NULL 
,@BCU2_IParkBrApplied_5_2 bit = NULL ,@BCU2_IParkBrLocked_5_2 bit = NULL ,@BCU2_IParkBrReleased_5_2 bit = NULL ,@BCUm_IBrTestRun_5_1 bit = NULL ,@BCUm_IBrTestRun_5_2 bit = NULL 
,@BCUx_IBogie1Locked_5_1 bit = NULL ,@BCUx_IBogie1Locked_5_2 bit = NULL ,@BCUx_IBogie2Locked_5_1 bit = NULL ,@BCUx_IBogie2Locked_5_2 bit = NULL ,@BCUx_IBogie3Locked_5_1 bit = NULL 
,@BCUx_IBogie3Locked_5_2 bit = NULL ,@BCUx_IBogie4Locked_5_1 bit = NULL ,@BCUx_IBogie4Locked_5_2 bit = NULL ,@BCUx_IBogie5Locked_5_1 bit = NULL ,@BCUx_IBogie5Locked_5_2 bit = NULL 
,@BCUx_IBogie6Locked_5_1 bit = NULL ,@BCUx_IBogie6Locked_5_2 bit = NULL ,@BCUx_IBogie7Locked_5_1 bit = NULL ,@BCUx_IBogie7Locked_5_2 bit = NULL ,@BCUx_IWspBogie1Slide_3_1 bit = NULL 
,@BCUx_IWspBogie1Slide_3_2 bit = NULL ,@BCUx_IWspBogie1Slide_5_1 bit = NULL ,@BCUx_IWspBogie1Slide_5_2 bit = NULL ,@BCUx_IWspBogie2Slide_3_1 bit = NULL ,@BCUx_IWspBogie2Slide_3_2 bit = NULL 
,@BCUx_IWspBogie2Slide_5_1 bit = NULL ,@BCUx_IWspBogie2Slide_5_2 bit = NULL ,@BCUx_IWspBogie3Slide_3_1 bit = NULL ,@BCUx_IWspBogie3Slide_3_2 bit = NULL ,@BCUx_IWspBogie3Slide_5_1 bit = NULL 
,@BCUx_IWspBogie3Slide_5_2 bit = NULL ,@BCUx_IWspBogie4Slide_3_1 bit = NULL ,@BCUx_IWspBogie4Slide_3_2 bit = NULL ,@BCUx_IWspBogie4Slide_5_1 bit = NULL ,@BCUx_IWspBogie4Slide_5_2 bit = NULL 
,@BCUx_IWspBogie5Slide_3_1 bit = NULL ,@BCUx_IWspBogie5Slide_3_2 bit = NULL ,@BCUx_IWspBogie5Slide_5_1 bit = NULL ,@BCUx_IWspBogie5Slide_5_2 bit = NULL ,@BCUx_IWspBogie6Slide_3_1 bit = NULL 
,@BCUx_IWspBogie6Slide_3_2 bit = NULL ,@BCUx_IWspBogie6Slide_5_1 bit = NULL ,@BCUx_IWspBogie6Slide_5_2 bit = NULL ,@BCUx_IWspBogie7Slide_3_1 bit = NULL ,@BCUx_IWspBogie7Slide_3_2 bit = NULL 
,@BCUx_IWspBogie7Slide_5_1 bit = NULL ,@BCUx_IWspBogie7Slide_5_2 bit = NULL ,@CABIN bit = NULL ,@CABIN_IST bit = NULL ,@CABIN_SOLL bit = NULL 
,@CABIN_STATUS bit = NULL ,@CYCLE_COUNTER tinyint = NULL ,@DBC_CBCU1ModeTowing_5_1 bit = NULL ,@DBC_CBCU1ModeTowing_5_2 bit = NULL ,@DBC_CBCU2ModeTowing_5_1 bit = NULL 
,@DBC_CBCU2ModeTowing_5_2 bit = NULL ,@DBC_Cbrake_M bit = NULL ,@DBC_Cbrake_M_1_1 bit = NULL ,@DBC_Cbrake_M_1_2 bit = NULL ,@DBC_Cbrake_M_2_1 bit = NULL 
,@DBC_Cbrake_M_2_2 bit = NULL ,@DBC_Cbrake_M_3_1 bit = NULL ,@DBC_Cbrake_M_3_2 bit = NULL ,@DBC_Cbrake_M_4_1 bit = NULL ,@DBC_Cbrake_M_4_10 bit = NULL 
,@DBC_Cbrake_M_4_11 bit = NULL ,@DBC_Cbrake_M_4_12 bit = NULL ,@DBC_Cbrake_M_4_13 bit = NULL ,@DBC_Cbrake_M_4_14 bit = NULL ,@DBC_Cbrake_M_4_15 bit = NULL 
,@DBC_Cbrake_M_4_16 bit = NULL ,@DBC_Cbrake_M_4_17 bit = NULL ,@DBC_Cbrake_M_4_18 bit = NULL ,@DBC_Cbrake_M_4_19 bit = NULL ,@DBC_Cbrake_M_4_2 bit = NULL 
,@DBC_Cbrake_M_4_20 bit = NULL ,@DBC_Cbrake_M_4_3 bit = NULL ,@DBC_Cbrake_M_4_4 bit = NULL ,@DBC_Cbrake_M_4_5 bit = NULL ,@DBC_Cbrake_M_4_6 bit = NULL 
,@DBC_Cbrake_M_4_7 bit = NULL ,@DBC_Cbrake_M_4_8 bit = NULL ,@DBC_Cbrake_M_4_9 bit = NULL ,@DBC_Cbrake_M_5_1 bit = NULL ,@DBC_Cbrake_M_5_2 bit = NULL 
,@DBC_Cbrake_M_8_1 bit = NULL ,@DBC_Cbrake_M_8_2 bit = NULL ,@DBC_Cbrake_M_8_3 bit = NULL ,@DBC_Cbrake_M_8_4 bit = NULL ,@DBC_Cbrake_M_8_5 bit = NULL 
,@DBC_Cbrake_M_8_6 bit = NULL ,@DBC_Cbrake_M_8_7 bit = NULL ,@DBC_Cbrake_M_8_8 bit = NULL ,@DBC_CBrakeQuick_M bit = NULL ,@DBC_CBrakeQuick_M_1_1 bit = NULL 
,@DBC_CBrakeQuick_M_1_2 bit = NULL ,@DBC_CBrakeQuick_M_2_1 bit = NULL ,@DBC_CBrakeQuick_M_2_2 bit = NULL ,@DBC_CBrakeQuick_M_3_1 bit = NULL ,@DBC_CBrakeQuick_M_3_2 bit = NULL 
,@DBC_CBrakeQuick_M_4_1 bit = NULL ,@DBC_CBrakeQuick_M_4_10 bit = NULL ,@DBC_CBrakeQuick_M_4_11 bit = NULL ,@DBC_CBrakeQuick_M_4_12 bit = NULL ,@DBC_CBrakeQuick_M_4_13 bit = NULL 
,@DBC_CBrakeQuick_M_4_14 bit = NULL ,@DBC_CBrakeQuick_M_4_15 bit = NULL ,@DBC_CBrakeQuick_M_4_16 bit = NULL ,@DBC_CBrakeQuick_M_4_17 bit = NULL ,@DBC_CBrakeQuick_M_4_18 bit = NULL 
,@DBC_CBrakeQuick_M_4_19 bit = NULL ,@DBC_CBrakeQuick_M_4_2 bit = NULL ,@DBC_CBrakeQuick_M_4_20 bit = NULL ,@DBC_CBrakeQuick_M_4_3 bit = NULL ,@DBC_CBrakeQuick_M_4_4 bit = NULL 
,@DBC_CBrakeQuick_M_4_5 bit = NULL ,@DBC_CBrakeQuick_M_4_6 bit = NULL ,@DBC_CBrakeQuick_M_4_7 bit = NULL ,@DBC_CBrakeQuick_M_4_8 bit = NULL ,@DBC_CBrakeQuick_M_4_9 bit = NULL 
,@DBC_CBrakeQuick_M_5_1 bit = NULL ,@DBC_CBrakeQuick_M_5_2 bit = NULL ,@DBC_CBrakeQuick_M_8_1 bit = NULL ,@DBC_CBrakeQuick_M_8_2 bit = NULL ,@DBC_CBrakeQuick_M_8_3 bit = NULL 
,@DBC_CBrakeQuick_M_8_4 bit = NULL ,@DBC_CBrakeQuick_M_8_5 bit = NULL ,@DBC_CBrakeQuick_M_8_6 bit = NULL ,@DBC_CBrakeQuick_M_8_7 bit = NULL ,@DBC_CBrakeQuick_M_8_8 bit = NULL 
,@DBC_CBrakingEffortInt_3_1 tinyint = NULL ,@DBC_CBrakingEffortInt_3_2 tinyint = NULL ,@DBC_CBrakingEffortInt_5_1 tinyint = NULL ,@DBC_CBrakingEffortInt_5_2 tinyint = NULL ,@DBC_Cdrive_M bit = NULL 
,@DBC_Cdrive_M_1_1 bit = NULL ,@DBC_Cdrive_M_1_2 bit = NULL ,@DBC_Cdrive_M_2_1 bit = NULL ,@DBC_Cdrive_M_2_2 bit = NULL ,@DBC_Cdrive_M_3_1 bit = NULL 
,@DBC_Cdrive_M_3_2 bit = NULL ,@DBC_Cdrive_M_4_1 bit = NULL ,@DBC_Cdrive_M_4_10 bit = NULL ,@DBC_Cdrive_M_4_11 bit = NULL ,@DBC_Cdrive_M_4_12 bit = NULL 
,@DBC_Cdrive_M_4_13 bit = NULL ,@DBC_Cdrive_M_4_14 bit = NULL ,@DBC_Cdrive_M_4_15 bit = NULL ,@DBC_Cdrive_M_4_16 bit = NULL ,@DBC_Cdrive_M_4_17 bit = NULL 
,@DBC_Cdrive_M_4_18 bit = NULL ,@DBC_Cdrive_M_4_19 bit = NULL ,@DBC_Cdrive_M_4_2 bit = NULL ,@DBC_Cdrive_M_4_20 bit = NULL ,@DBC_Cdrive_M_4_3 bit = NULL 
,@DBC_Cdrive_M_4_4 bit = NULL ,@DBC_Cdrive_M_4_5 bit = NULL ,@DBC_Cdrive_M_4_6 bit = NULL ,@DBC_Cdrive_M_4_7 bit = NULL ,@DBC_Cdrive_M_4_8 bit = NULL 
,@DBC_Cdrive_M_4_9 bit = NULL ,@DBC_Cdrive_M_5_1 bit = NULL ,@DBC_Cdrive_M_5_2 bit = NULL ,@DBC_Cdrive_M_8_1 bit = NULL ,@DBC_Cdrive_M_8_2 bit = NULL 
,@DBC_Cdrive_M_8_3 bit = NULL ,@DBC_Cdrive_M_8_4 bit = NULL ,@DBC_Cdrive_M_8_5 bit = NULL ,@DBC_Cdrive_M_8_6 bit = NULL ,@DBC_Cdrive_M_8_7 bit = NULL 
,@DBC_Cdrive_M_8_8 bit = NULL ,@DBC_CModeTowing_M_5_1 bit = NULL ,@DBC_CModeTowing_M_5_2 bit = NULL ,@DBC_CTCU1GrInTracConv1_3_1 bit = NULL ,@DBC_CTCU2GrInTracConv2_3_2 bit = NULL 
,@DBC_CTractiveEffort_3_1 tinyint = NULL ,@DBC_CTractiveEffort_3_2 tinyint = NULL ,@DBC_ITrainSpeedInt smallint = NULL ,@DBC_ITrainSpeedInt_1_1 smallint = NULL ,@DBC_ITrainSpeedInt_1_2 smallint = NULL 
,@DBC_ITrainSpeedInt_15 smallint = NULL ,@DBC_ITrainSpeedInt_2_1 smallint = NULL ,@DBC_ITrainSpeedInt_2_2 smallint = NULL ,@DBC_ITrainSpeedInt_3_1 smallint = NULL ,@DBC_ITrainSpeedInt_3_2 smallint = NULL 
,@DBC_ITrainSpeedInt_4_1 smallint = NULL ,@DBC_ITrainSpeedInt_4_10 smallint = NULL ,@DBC_ITrainSpeedInt_4_11 smallint = NULL ,@DBC_ITrainSpeedInt_4_12 smallint = NULL ,@DBC_ITrainSpeedInt_4_13 smallint = NULL 
,@DBC_ITrainSpeedInt_4_14 smallint = NULL ,@DBC_ITrainSpeedInt_4_15 smallint = NULL ,@DBC_ITrainSpeedInt_4_16 smallint = NULL ,@DBC_ITrainSpeedInt_4_17 smallint = NULL ,@DBC_ITrainSpeedInt_4_18 smallint = NULL 
,@DBC_ITrainSpeedInt_4_19 smallint = NULL ,@DBC_ITrainSpeedInt_4_2 smallint = NULL ,@DBC_ITrainSpeedInt_4_20 smallint = NULL ,@DBC_ITrainSpeedInt_4_3 smallint = NULL ,@DBC_ITrainSpeedInt_4_4 smallint = NULL 
,@DBC_ITrainSpeedInt_4_5 smallint = NULL ,@DBC_ITrainSpeedInt_4_6 smallint = NULL ,@DBC_ITrainSpeedInt_4_7 smallint = NULL ,@DBC_ITrainSpeedInt_4_8 smallint = NULL ,@DBC_ITrainSpeedInt_4_9 smallint = NULL 
,@DBC_ITrainSpeedInt_5_1 smallint = NULL ,@DBC_ITrainSpeedInt_5_2 smallint = NULL ,@DBC_ITrainSpeedInt_8_1 smallint = NULL ,@DBC_ITrainSpeedInt_8_2 smallint = NULL ,@DBC_ITrainSpeedInt_8_3 smallint = NULL 
,@DBC_ITrainSpeedInt_8_4 smallint = NULL ,@DBC_ITrainSpeedInt_8_5 smallint = NULL ,@DBC_ITrainSpeedInt_8_6 smallint = NULL ,@DBC_ITrainSpeedInt_8_7 smallint = NULL ,@DBC_ITrainSpeedInt_8_8 smallint = NULL 
,@DBC_ITrainSpeedInt_9 smallint = NULL ,@DBC_IZrSpdInd_4_1 bit = NULL ,@DBC_IZrSpdInd_4_10 bit = NULL ,@DBC_IZrSpdInd_4_11 bit = NULL ,@DBC_IZrSpdInd_4_12 bit = NULL 
,@DBC_IZrSpdInd_4_13 bit = NULL ,@DBC_IZrSpdInd_4_14 bit = NULL ,@DBC_IZrSpdInd_4_15 bit = NULL ,@DBC_IZrSpdInd_4_16 bit = NULL ,@DBC_IZrSpdInd_4_17 bit = NULL 
,@DBC_IZrSpdInd_4_18 bit = NULL ,@DBC_IZrSpdInd_4_19 bit = NULL ,@DBC_IZrSpdInd_4_2 bit = NULL ,@DBC_IZrSpdInd_4_20 bit = NULL ,@DBC_IZrSpdInd_4_3 bit = NULL 
,@DBC_IZrSpdInd_4_4 bit = NULL ,@DBC_IZrSpdInd_4_5 bit = NULL ,@DBC_IZrSpdInd_4_6 bit = NULL ,@DBC_IZrSpdInd_4_7 bit = NULL ,@DBC_IZrSpdInd_4_8 bit = NULL 
,@DBC_IZrSpdInd_4_9 bit = NULL ,@DBC_PRTcu1GrOut_3_1 bit = NULL ,@DBC_PRTcu2GrOut_3_2 bit = NULL ,@DRIVERBRAKE_APPLIED_SUFF bit = NULL ,@DRIVERBRAKE_OPERATED bit = NULL 
,@EB_STATUS bit = NULL ,@ERROR_CODE_INTERNAL bit = NULL ,@ERRORCODE_1 bit = NULL ,@ERRORCODE_2 bit = NULL ,@ERRORCODE_3 bit = NULL 
,@GW_IGwOrientation tinyint = NULL ,@HAC_ITempOutsideInt smallint = NULL ,@HAC_ITempOutsideInt_1_1 smallint = NULL ,@HAC_ITempOutsideInt_1_2 smallint = NULL ,@HAC_ITempOutsideInt_2_1 smallint = NULL 
,@HAC_ITempOutsideInt_2_2 smallint = NULL ,@HAC_ITempOutsideInt_3_1 smallint = NULL ,@HAC_ITempOutsideInt_3_2 smallint = NULL ,@HAC_ITempOutsideInt_4_1 smallint = NULL ,@HAC_ITempOutsideInt_4_10 smallint = NULL 
,@HAC_ITempOutsideInt_4_11 smallint = NULL ,@HAC_ITempOutsideInt_4_12 smallint = NULL ,@HAC_ITempOutsideInt_4_13 smallint = NULL ,@HAC_ITempOutsideInt_4_14 smallint = NULL ,@HAC_ITempOutsideInt_4_15 smallint = NULL 
,@HAC_ITempOutsideInt_4_16 smallint = NULL ,@HAC_ITempOutsideInt_4_17 smallint = NULL ,@HAC_ITempOutsideInt_4_18 smallint = NULL ,@HAC_ITempOutsideInt_4_19 smallint = NULL ,@HAC_ITempOutsideInt_4_2 smallint = NULL 
,@HAC_ITempOutsideInt_4_20 smallint = NULL ,@HAC_ITempOutsideInt_4_3 smallint = NULL ,@HAC_ITempOutsideInt_4_4 smallint = NULL ,@HAC_ITempOutsideInt_4_5 smallint = NULL ,@HAC_ITempOutsideInt_4_6 smallint = NULL 
,@HAC_ITempOutsideInt_4_7 smallint = NULL ,@HAC_ITempOutsideInt_4_8 smallint = NULL ,@HAC_ITempOutsideInt_4_9 smallint = NULL ,@HAC_ITempOutsideInt_5_1 smallint = NULL ,@HAC_ITempOutsideInt_5_2 smallint = NULL 
,@HAC_ITempOutsideInt_9 smallint = NULL ,@HVAC1_ITempAirMixedInt smallint = NULL ,@HVAC1_ITempDuctAir1Int smallint = NULL ,@HVAC1_ITempDuctAir2Int smallint = NULL ,@HVAC1_ITempInComInt smallint = NULL 
,@HVAC1_ITempOutsideInt smallint = NULL ,@HVAC1_ITempSetpActDuctInt smallint = NULL ,@HVAC1_ITempSetpActInt smallint = NULL ,@HVAC2_ITempAirMixedInt smallint = NULL ,@HVAC2_ITempDuctAir1Int smallint = NULL 
,@HVAC2_ITempDuctAir2Int smallint = NULL ,@HVAC2_ITempInComInt smallint = NULL ,@HVAC2_ITempOutsideInt smallint = NULL ,@HVAC2_ITempSetpActDuctInt smallint = NULL ,@HVAC2_ITempSetpActInt smallint = NULL 
,@HVAC3_ITempAirMixedInt smallint = NULL ,@HVAC3_ITempDuctAir1Int smallint = NULL ,@HVAC3_ITempDuctAir2Int smallint = NULL ,@HVAC3_ITempInComInt smallint = NULL ,@HVAC3_ITempOutsideInt smallint = NULL 
,@HVAC3_ITempSetpActDuctInt smallint = NULL ,@HVAC3_ITempSetpActInt smallint = NULL ,@HVAC4_ITempAirMixedInt smallint = NULL ,@HVAC4_ITempDuctAir1Int smallint = NULL ,@HVAC4_ITempDuctAir2Int smallint = NULL 
,@HVAC4_ITempInComInt smallint = NULL ,@HVAC4_ITempOutsideInt smallint = NULL ,@HVAC4_ITempSetpActDuctInt smallint = NULL ,@HVAC4_ITempSetpActInt smallint = NULL ,@HVAC5_ITempAirMixedInt smallint = NULL 
,@HVAC5_ITempDuctAir1Int smallint = NULL ,@HVAC5_ITempDuctAir2Int smallint = NULL ,@HVAC5_ITempInComInt smallint = NULL ,@HVAC5_ITempOutsideInt smallint = NULL ,@HVAC5_ITempSetpActDuctInt smallint = NULL 
,@HVAC5_ITempSetpActInt smallint = NULL ,@HVAC6_ITempAirMixedInt smallint = NULL ,@HVAC6_ITempDuctAir1Int smallint = NULL ,@HVAC6_ITempDuctAir2Int smallint = NULL ,@HVAC6_ITempInComInt smallint = NULL 
,@HVAC6_ITempOutsideInt smallint = NULL ,@HVAC6_ITempSetpActDuctInt smallint = NULL ,@HVAC6_ITempSetpActInt smallint = NULL ,@HVCC1_ITempDuctAir1Int smallint = NULL ,@HVCC1_ITempInComInt smallint = NULL 
,@HVCC1_ITempOutsideInt smallint = NULL ,@HVCC1_ITempSetpActDuctInt smallint = NULL ,@HVCC1_ITempSetpActInt smallint = NULL ,@HVCC2_ITempDuctAir1Int smallint = NULL ,@HVCC2_ITempInComInt smallint = NULL 
,@HVCC2_ITempOutsideInt smallint = NULL ,@HVCC2_ITempSetpActDuctInt smallint = NULL ,@HVCC2_ITempSetpActInt smallint = NULL ,@INadiCtrlInfo1 tinyint = NULL ,@INadiCtrlInfo2 tinyint = NULL 
,@INadiCtrlInfo3 tinyint = NULL ,@INadiNumberEntries tinyint = NULL ,@INadiTcnAddr1 tinyint = NULL ,@INadiTcnAddr2 tinyint = NULL ,@INadiTcnAddr3 tinyint = NULL 
,@INadiTopoCount smallint = NULL ,@INadiUicAddr1 tinyint = NULL ,@INadiUicAddr2 tinyint = NULL ,@INadiUicAddr3 tinyint = NULL ,@INTERNAL_ERROR_CODE smallint = NULL 
,@INTERNAL_STATE bit = NULL ,@ITrainsetNumber1 smallint = NULL ,@ITrainsetNumber2 smallint = NULL ,@ITrainsetNumber3 smallint = NULL ,@MCG_IGsmSigStrength smallint = NULL 
,@MIO_IContBusBar400On bit = NULL ,@MIO_IContBusBar400On_1_1 bit = NULL ,@MIO_IContBusBar400On_1_2 bit = NULL ,@MIO_IContBusBar400On_2_1 bit = NULL ,@MIO_IContBusBar400On_2_2 bit = NULL 
,@MIO_IContBusBar400On_3_1 bit = NULL ,@MIO_IContBusBar400On_3_2 bit = NULL ,@MIO_IContBusBar400On_8_1 bit = NULL ,@MIO_IContBusBar400On_8_2 bit = NULL ,@MIO_IContBusBar400On_8_3 bit = NULL 
,@MIO_IContBusBar400On_8_4 bit = NULL ,@MIO_IContBusBar400On_8_5 bit = NULL ,@MIO_IContBusBar400On_8_6 bit = NULL ,@MIO_IContBusBar400On_8_7 bit = NULL ,@MIO_IContBusBar400On_8_8 bit = NULL 
,@MIO_IContBusBar400On_9 bit = NULL ,@MIO_IHscbOff bit = NULL ,@MIO_IHscbOff_1_1 bit = NULL ,@MIO_IHscbOff_1_2 bit = NULL ,@MIO_IHscbOff_2_1 bit = NULL 
,@MIO_IHscbOff_2_2 bit = NULL ,@MIO_IHscbOff_3_1 bit = NULL ,@MIO_IHscbOff_3_2 bit = NULL ,@MIO_IHscbOff_8_1 bit = NULL ,@MIO_IHscbOff_8_2 bit = NULL 
,@MIO_IHscbOff_8_3 bit = NULL ,@MIO_IHscbOff_8_4 bit = NULL ,@MIO_IHscbOff_8_5 bit = NULL ,@MIO_IHscbOff_8_6 bit = NULL ,@MIO_IHscbOff_8_7 bit = NULL 
,@MIO_IHscbOff_8_8 bit = NULL ,@MIO_IHscbOn bit = NULL ,@MIO_IHscbOn_1_1 bit = NULL ,@MIO_IHscbOn_1_2 bit = NULL ,@MIO_IHscbOn_2_1 bit = NULL 
,@MIO_IHscbOn_2_2 bit = NULL ,@MIO_IHscbOn_3_1 bit = NULL ,@MIO_IHscbOn_3_2 bit = NULL ,@MIO_IHscbOn_8_1 bit = NULL ,@MIO_IHscbOn_8_2 bit = NULL 
,@MIO_IHscbOn_8_3 bit = NULL ,@MIO_IHscbOn_8_4 bit = NULL ,@MIO_IHscbOn_8_5 bit = NULL ,@MIO_IHscbOn_8_6 bit = NULL ,@MIO_IHscbOn_8_7 bit = NULL 
,@MIO_IHscbOn_8_8 bit = NULL ,@MIO_IPantoPrssSw1On bit = NULL ,@MIO_IPantoPrssSw1On_1_1 bit = NULL ,@MIO_IPantoPrssSw1On_1_2 bit = NULL ,@MIO_IPantoPrssSw1On_2_1 bit = NULL 
,@MIO_IPantoPrssSw1On_2_2 bit = NULL ,@MIO_IPantoPrssSw1On_3_1 bit = NULL ,@MIO_IPantoPrssSw1On_3_2 bit = NULL ,@MIO_IPantoPrssSw1On_8_1 bit = NULL ,@MIO_IPantoPrssSw1On_8_2 bit = NULL 
,@MIO_IPantoPrssSw1On_8_3 bit = NULL ,@MIO_IPantoPrssSw1On_8_4 bit = NULL ,@MIO_IPantoPrssSw1On_8_5 bit = NULL ,@MIO_IPantoPrssSw1On_8_6 bit = NULL ,@MIO_IPantoPrssSw1On_8_7 bit = NULL 
,@MIO_IPantoPrssSw1On_8_8 bit = NULL ,@MIO_IPantoPrssSw1On_9 bit = NULL ,@MIO_IPantoPrssSw2On bit = NULL ,@MIO_IPantoPrssSw2On_1_1 bit = NULL ,@MIO_IPantoPrssSw2On_1_2 bit = NULL 
,@MIO_IPantoPrssSw2On_2_1 bit = NULL ,@MIO_IPantoPrssSw2On_2_2 bit = NULL ,@MIO_IPantoPrssSw2On_3_1 bit = NULL ,@MIO_IPantoPrssSw2On_3_2 bit = NULL ,@MIO_IPantoPrssSw2On_8_1 bit = NULL 
,@MIO_IPantoPrssSw2On_8_2 bit = NULL ,@MIO_IPantoPrssSw2On_8_3 bit = NULL ,@MIO_IPantoPrssSw2On_8_4 bit = NULL ,@MIO_IPantoPrssSw2On_8_5 bit = NULL ,@MIO_IPantoPrssSw2On_8_6 bit = NULL 
,@MIO_IPantoPrssSw2On_8_7 bit = NULL ,@MIO_IPantoPrssSw2On_8_8 bit = NULL ,@MIO_IPantoPrssSw2On_9 bit = NULL ,@MMI_EB_STATUS tinyint = NULL ,@MMI_M_ACTIVE_CABIN tinyint = NULL 
,@MMI_M_LEVEL bit = NULL ,@MMI_M_MODE tinyint = NULL ,@MMI_M_WARNING tinyint = NULL ,@MMI_SB_STATUS tinyint = NULL ,@MMI_V_PERMITTED tinyint = NULL 
,@MMI_V_TRAIN smallint = NULL ,@NID_STM_1 tinyint = NULL ,@NID_STM_2 smallint = NULL ,@NID_STM_3 smallint = NULL ,@NID_STM_4 smallint = NULL 
,@NID_STM_5 tinyint = NULL ,@NID_STM_6 tinyint = NULL ,@NID_STM_7 tinyint = NULL ,@NID_STM_8 tinyint = NULL ,@NID_STM_DA smallint = NULL 
,@ONE_STM_DA bit = NULL ,@ONE_STM_HS bit = NULL ,@PB_ADDR bit = NULL ,@PERMITTED_SPEED tinyint = NULL ,@Q_ODO bit = NULL 
,@REPORTED_DIRECTION tinyint = NULL ,@REPORTED_SPEED tinyint = NULL ,@REPORTED_SPEED_HISTORIC tinyint = NULL ,@ROTATION_DIRECTION_SDU1 bit = NULL ,@ROTATION_DIRECTION_SDU2 bit = NULL 
,@SAP tinyint = NULL ,@SB_STATUS tinyint = NULL ,@SDU_DIRECTION_SDU1 tinyint = NULL ,@SDU_DIRECTION_SDU2 tinyint = NULL ,@SENSOR_ERROR_STATUS1_TACHO1 bit = NULL 
,@SENSOR_ERROR_STATUS2_TACHO2 bit = NULL ,@SENSOR_ERROR_STATUS3_RADAR1 bit = NULL ,@SENSOR_ERROR_STATUS4_RADAR2 bit = NULL ,@SENSOR_SPEED_HISTORIC_RADAR1 decimal(5,1) = NULL ,@SENSOR_SPEED_HISTORIC_RADAR2 decimal(5,1) = NULL 
,@SENSOR_SPEED_HISTORIC_TACHO1 decimal(5,1) = NULL ,@SENSOR_SPEED_HISTORIC_TACHO2 decimal(5,1) = NULL ,@SENSOR_SPEED_RADAR1 decimal(5,1) = NULL ,@SENSOR_SPEED_RADAR2 decimal(5,1) = NULL ,@SENSOR_SPEED_TACHO1 decimal(5,1) = NULL 
,@SENSOR_SPEED_TACHO2 decimal(5,1) = NULL ,@SLIP_SLIDE_ACC_AXLE1 bit = NULL ,@SLIP_SLIDE_ACC_AXLE2 bit = NULL ,@SLIP_SLIDE_AXLE1 tinyint = NULL ,@SLIP_SLIDE_AXLE2 bit = NULL 
,@SLIP_SLIDE_RADAR_DIFF_AXLE1 bit = NULL ,@SLIP_SLIDE_RADAR_DIFF_AXLE2 bit = NULL ,@SLIP_SLIDE_STATUS tinyint = NULL ,@SLIP_SLIDE_TACHO_DIFF_AXLE1 bit = NULL ,@SLIP_SLIDE_TACHO_DIFF_AXLE2 bit = NULL 
,@SOFTWARE_VERSION VARCHAR(15) = NULL ,@SPL_FUNC_ID smallint = NULL ,@SPL_RETURN smallint = NULL ,@STM_STATE tinyint = NULL ,@STM_SUB_STATE tinyint = NULL 
,@SUPERVISION_STATE tinyint = NULL ,@TC_CCorrectOrient_M tinyint = NULL ,@TC_IConsistOrient tinyint = NULL ,@TC_IGwOrient tinyint = NULL ,@TC_INumConsists tinyint = NULL 
,@TC_INumConsists_1_1 tinyint = NULL ,@TC_INumConsists_1_2 tinyint = NULL ,@TC_INumConsists_2_1 tinyint = NULL ,@TC_INumConsists_2_2 tinyint = NULL ,@TC_INumConsists_3_1 tinyint = NULL 
,@TC_INumConsists_3_2 tinyint = NULL ,@TC_INumConsists_4_1 tinyint = NULL ,@TC_INumConsists_4_10 tinyint = NULL ,@TC_INumConsists_4_11 tinyint = NULL ,@TC_INumConsists_4_12 tinyint = NULL 
,@TC_INumConsists_4_13 tinyint = NULL ,@TC_INumConsists_4_14 tinyint = NULL ,@TC_INumConsists_4_15 tinyint = NULL ,@TC_INumConsists_4_16 tinyint = NULL ,@TC_INumConsists_4_17 tinyint = NULL 
,@TC_INumConsists_4_18 tinyint = NULL ,@TC_INumConsists_4_19 tinyint = NULL ,@TC_INumConsists_4_2 tinyint = NULL ,@TC_INumConsists_4_20 tinyint = NULL ,@TC_INumConsists_4_3 tinyint = NULL 
,@TC_INumConsists_4_4 tinyint = NULL ,@TC_INumConsists_4_5 tinyint = NULL ,@TC_INumConsists_4_6 tinyint = NULL ,@TC_INumConsists_4_7 tinyint = NULL ,@TC_INumConsists_4_8 tinyint = NULL 
,@TC_INumConsists_4_9 tinyint = NULL ,@TC_INumConsists_5_1 tinyint = NULL ,@TC_INumConsists_5_2 tinyint = NULL ,@TC_INumConsists_8_1 tinyint = NULL ,@TC_INumConsists_8_2 tinyint = NULL 
,@TC_INumConsists_8_3 tinyint = NULL ,@TC_INumConsists_8_4 tinyint = NULL ,@TC_INumConsists_8_5 tinyint = NULL ,@TC_INumConsists_8_6 tinyint = NULL ,@TC_INumConsists_8_7 tinyint = NULL 
,@TC_INumConsists_8_8 tinyint = NULL ,@TC_INumConsists_9 tinyint = NULL ,@TC_ITclOrient tinyint = NULL ,@TCO_STATUS tinyint = NULL ,@TCU1_IEdSliding_3_1 bit = NULL 
,@TCU1_IEdSliding_5_1 bit = NULL ,@TCU1_ILineVoltage_3_1 decimal(5,1) = NULL ,@TCU1_ILineVoltage_5_1 decimal(5,1) = NULL ,@TCU1_ILineVoltage_5_2 decimal(5,1) = NULL ,@TCU1_IPowerLimit_3_1 bit = NULL 
,@TCU1_ITracBrActualInt_3_1 decimal(5,1) = NULL ,@TCU2_IEdSliding_3_2 bit = NULL ,@TCU2_IEdSliding_5_2 bit = NULL ,@TCU2_ILineVoltage_3_2 decimal(5,1) = NULL ,@TCU2_ILineVoltage_5_1 decimal(5,1) = NULL 
,@TCU2_ILineVoltage_5_2 decimal(5,1) = NULL ,@TCU2_IPowerLimit_3_2 bit = NULL ,@TCU2_ITracBrActualInt_3_2 decimal(5,1) = NULL ,@TRACK_SIGNAL tinyint = NULL ,@DBC_PRTcu1GrOut bit = NULL 
,@DBC_PRTcu2GrOut bit = NULL ,@MPW_PRHscbEnable bit = NULL ,@MPW_IPnt1UpDrvCount int = NULL ,@MPW_IPnt2UpDrvCount int = NULL ,@MPW_IPnt1UpCount int = NULL 
,@MPW_IPnt2UpCount int = NULL ,@MPW_IPnt1KmCount int = NULL ,@MPW_IPnt2KmCount int = NULL ,@DBC_IKmCounter int = NULL ,@AUXY_IAuxAirCOnCnt int = NULL 
,@MPW_IOnHscb int = NULL ,@AUXY_IAirCSwOnCount int = NULL ,@AUXY_IAirCOnCount int = NULL ,@AUXY_IAuxAirCSwOnCnt int = NULL ,@MPW_PRPantoEnable bit = NULL
,@DIA_Reserve1_AUX1 int = NULL ,@DIA_Reserve1_AUX2 int = NULL ,@DIA_Reserve1_BCHA_1 int = NULL ,@DIA_Reserve1_BCHA_2 int = NULL ,@DIA_Reserve1_BCU_1 int = NULL ,@DIA_Reserve1_BCU_2 int = NULL
,@DIA_Reserve1_CCUO int = NULL ,@DIA_Reserve1_DCU_1 int = NULL ,@DIA_Reserve1_DCU_10 int = NULL ,@DIA_Reserve1_DCU_11 int = NULL ,@DIA_Reserve1_DCU_12 int = NULL ,@DIA_Reserve1_DCU_13 int = NULL
,@DIA_Reserve1_DCU_14 int = NULL ,@DIA_Reserve1_DCU_15 int = NULL ,@DIA_Reserve1_DCU_16 int = NULL ,@DIA_Reserve1_DCU_17 int = NULL ,@DIA_Reserve1_DCU_18 int = NULL ,@DIA_Reserve1_DCU_19 int = NULL
,@DIA_Reserve1_DCU_2 int = NULL ,@DIA_Reserve1_DCU_20 int = NULL ,@DIA_Reserve1_DCU_3 int = NULL ,@DIA_Reserve1_DCU_4 int = NULL ,@DIA_Reserve1_DCU_5 int = NULL ,@DIA_Reserve1_DCU_6 int = NULL
,@DIA_Reserve1_DCU_7 int = NULL ,@DIA_Reserve1_DCU_8 int = NULL ,@DIA_Reserve1_DCU_9 int = NULL ,@DIA_Reserve1_HVAC_1 int = NULL ,@DIA_Reserve1_HVAC_2 int = NULL ,@DIA_Reserve1_HVAC_3 int = NULL
,@DIA_Reserve1_HVAC_4 int = NULL ,@DIA_Reserve1_HVAC_5 int = NULL ,@DIA_Reserve1_HVAC_6 int = NULL ,@DIA_Reserve1_HVCC_1 int = NULL ,@DIA_Reserve1_HVCC_2 int = NULL ,@DIA_Reserve1_PIS int = NULL
,@DIA_Reserve1_TCU_1 int = NULL ,@DIA_Reserve1_TCU_2 int = NULL ,@Reserve int = NULL
,@DCU01_CDoorCloseConduc bit = NULL ,@DCU01_IDoorClosedSafe bit = NULL ,@DCU01_IDoorClRelease bit = NULL ,@DCU01_IDoorPbClose bit = NULL ,@DCU01_IDoorPbOpenIn bit = NULL 
,@DCU01_IDoorPbOpenOut bit = NULL ,@DCU01_IDoorReleased bit = NULL ,@DCU01_IFootStepRel bit = NULL ,@DCU01_IForcedClosDoor bit = NULL ,@DCU01_ILeafsStopDoor bit = NULL 
,@DCU01_IObstacleDoor bit = NULL ,@DCU01_IObstacleStep bit = NULL ,@DCU01_IOpenAssistDoor bit = NULL ,@DCU01_IStandstillBack bit = NULL ,@DCU02_CDoorCloseConduc bit = NULL 
,@DCU02_IDoorClosedSafe bit = NULL ,@DCU02_IDoorClRelease bit = NULL ,@DCU02_IDoorPbClose bit = NULL ,@DCU02_IDoorPbOpenIn bit = NULL ,@DCU02_IDoorPbOpenOut bit = NULL 
,@DCU02_IDoorReleased bit = NULL ,@DCU02_IFootStepRel bit = NULL ,@DCU02_IForcedClosDoor bit = NULL ,@DCU02_ILeafsStopDoor bit = NULL ,@DCU02_IObstacleDoor bit = NULL 
,@DCU02_IObstacleStep bit = NULL ,@DCU02_IOpenAssistDoor bit = NULL ,@DCU02_IStandstillBack bit = NULL ,@DCU03_CDoorCloseConduc bit = NULL ,@DCU03_IDoorClosedSafe bit = NULL 
,@DCU03_IDoorClRelease bit = NULL ,@DCU03_IDoorPbClose bit = NULL ,@DCU03_IDoorPbOpenIn bit = NULL ,@DCU03_IDoorPbOpenOut bit = NULL ,@DCU03_IDoorReleased bit = NULL 
,@DCU03_IFootStepRel bit = NULL ,@DCU03_IForcedClosDoor bit = NULL ,@DCU03_ILeafsStopDoor bit = NULL ,@DCU03_IObstacleDoor bit = NULL ,@DCU03_IObstacleStep bit = NULL 
,@DCU03_IOpenAssistDoor bit = NULL ,@DCU03_IStandstillBack bit = NULL ,@DCU04_CDoorCloseConduc bit = NULL ,@DCU04_IDoorClosedSafe bit = NULL ,@DCU04_IDoorClRelease bit = NULL 
,@DCU04_IDoorPbClose bit = NULL ,@DCU04_IDoorPbOpenIn bit = NULL ,@DCU04_IDoorPbOpenOut bit = NULL ,@DCU04_IDoorReleased bit = NULL ,@DCU04_IFootStepRel bit = NULL 
,@DCU04_IForcedClosDoor bit = NULL ,@DCU04_ILeafsStopDoor bit = NULL ,@DCU04_IObstacleDoor bit = NULL ,@DCU04_IObstacleStep bit = NULL ,@DCU04_IOpenAssistDoor bit = NULL 
,@DCU04_IStandstillBack bit = NULL ,@DCU05_CDoorCloseConduc bit = NULL ,@DCU05_IDoorClosedSafe bit = NULL ,@DCU05_IDoorClRelease bit = NULL ,@DCU05_IDoorPbClose bit = NULL 
,@DCU05_IDoorPbOpenIn bit = NULL ,@DCU05_IDoorPbOpenOut bit = NULL ,@DCU05_IDoorReleased bit = NULL ,@DCU05_IFootStepRel bit = NULL ,@DCU05_IForcedClosDoor bit = NULL 
,@DCU05_ILeafsStopDoor bit = NULL ,@DCU05_IObstacleDoor bit = NULL ,@DCU05_IObstacleStep bit = NULL ,@DCU05_IOpenAssistDoor bit = NULL ,@DCU05_IStandstillBack bit = NULL 
,@DCU06_CDoorCloseConduc bit = NULL ,@DCU06_IDoorClosedSafe bit = NULL ,@DCU06_IDoorClRelease bit = NULL ,@DCU06_IDoorPbClose bit = NULL ,@DCU06_IDoorPbOpenIn bit = NULL 
,@DCU06_IDoorPbOpenOut bit = NULL ,@DCU06_IDoorReleased bit = NULL ,@DCU06_IFootStepRel bit = NULL ,@DCU06_IForcedClosDoor bit = NULL ,@DCU06_ILeafsStopDoor bit = NULL 
,@DCU06_IObstacleDoor bit = NULL ,@DCU06_IObstacleStep bit = NULL ,@DCU06_IOpenAssistDoor bit = NULL ,@DCU06_IStandstillBack bit = NULL ,@DCU07_CDoorCloseConduc bit = NULL 
,@DCU07_IDoorClosedSafe bit = NULL ,@DCU07_IDoorClRelease bit = NULL ,@DCU07_IDoorPbClose bit = NULL ,@DCU07_IDoorPbOpenIn bit = NULL ,@DCU07_IDoorPbOpenOut bit = NULL 
,@DCU07_IDoorReleased bit = NULL ,@DCU07_IFootStepRel bit = NULL ,@DCU07_IForcedClosDoor bit = NULL ,@DCU07_ILeafsStopDoor bit = NULL ,@DCU07_IObstacleDoor bit = NULL 
,@DCU07_IObstacleStep bit = NULL ,@DCU07_IOpenAssistDoor bit = NULL ,@DCU07_IStandstillBack bit = NULL ,@DCU08_CDoorCloseConduc bit = NULL ,@DCU08_IDoorClosedSafe bit = NULL 
,@DCU08_IDoorClRelease bit = NULL ,@DCU08_IDoorPbClose bit = NULL ,@DCU08_IDoorPbOpenIn bit = NULL ,@DCU08_IDoorPbOpenOut bit = NULL ,@DCU08_IDoorReleased bit = NULL 
,@DCU08_IFootStepRel bit = NULL ,@DCU08_IForcedClosDoor bit = NULL ,@DCU08_ILeafsStopDoor bit = NULL ,@DCU08_IObstacleDoor bit = NULL ,@DCU08_IObstacleStep bit = NULL 
,@DCU08_IOpenAssistDoor bit = NULL ,@DCU08_IStandstillBack bit = NULL ,@DCU09_CDoorCloseConduc bit = NULL ,@DCU09_IDoorClosedSafe bit = NULL ,@DCU09_IDoorClRelease bit = NULL 
,@DCU09_IDoorPbClose bit = NULL ,@DCU09_IDoorPbOpenIn bit = NULL ,@DCU09_IDoorPbOpenOut bit = NULL ,@DCU09_IDoorReleased bit = NULL ,@DCU09_IFootStepRel bit = NULL 
,@DCU09_IForcedClosDoor bit = NULL ,@DCU09_ILeafsStopDoor bit = NULL ,@DCU09_IObstacleDoor bit = NULL ,@DCU09_IObstacleStep bit = NULL ,@DCU09_IOpenAssistDoor bit = NULL 
,@DCU09_IStandstillBack bit = NULL ,@DCU10_CDoorCloseConduc bit = NULL ,@DCU10_IDoorClosedSafe bit = NULL ,@DCU10_IDoorClRelease bit = NULL ,@DCU10_IDoorPbClose bit = NULL 
,@DCU10_IDoorPbOpenIn bit = NULL ,@DCU10_IDoorPbOpenOut bit = NULL ,@DCU10_IDoorReleased bit = NULL ,@DCU10_IFootStepRel bit = NULL ,@DCU10_IForcedClosDoor bit = NULL 
,@DCU10_ILeafsStopDoor bit = NULL ,@DCU10_IObstacleDoor bit = NULL ,@DCU10_IObstacleStep bit = NULL ,@DCU10_IOpenAssistDoor bit = NULL ,@DCU10_IStandstillBack bit = NULL 
,@DCU11_CDoorCloseConduc bit = NULL ,@DCU11_IDoorClosedSafe bit = NULL ,@DCU11_IDoorClRelease bit = NULL ,@DCU11_IDoorPbClose bit = NULL ,@DCU11_IDoorPbOpenIn bit = NULL 
,@DCU11_IDoorPbOpenOut bit = NULL ,@DCU11_IDoorReleased bit = NULL ,@DCU11_IFootStepRel bit = NULL ,@DCU11_IForcedClosDoor bit = NULL ,@DCU11_ILeafsStopDoor bit = NULL 
,@DCU11_IObstacleDoor bit = NULL ,@DCU11_IObstacleStep bit = NULL ,@DCU11_IOpenAssistDoor bit = NULL ,@DCU11_IStandstillBack bit = NULL ,@DCU12_CDoorCloseConduc bit = NULL 
,@DCU12_IDoorClosedSafe bit = NULL ,@DCU12_IDoorClRelease bit = NULL ,@DCU12_IDoorPbClose bit = NULL ,@DCU12_IDoorPbOpenIn bit = NULL ,@DCU12_IDoorPbOpenOut bit = NULL 
,@DCU12_IDoorReleased bit = NULL ,@DCU12_IFootStepRel bit = NULL ,@DCU12_IForcedClosDoor bit = NULL ,@DCU12_ILeafsStopDoor bit = NULL ,@DCU12_IObstacleDoor bit = NULL 
,@DCU12_IObstacleStep bit = NULL ,@DCU12_IOpenAssistDoor bit = NULL ,@DCU12_IStandstillBack bit = NULL ,@DCU13_CDoorCloseConduc bit = NULL ,@DCU13_IDoorClosedSafe bit = NULL 
,@DCU13_IDoorClRelease bit = NULL ,@DCU13_IDoorPbClose bit = NULL ,@DCU13_IDoorPbOpenIn bit = NULL ,@DCU13_IDoorPbOpenOut bit = NULL ,@DCU13_IDoorReleased bit = NULL 
,@DCU13_IFootStepRel bit = NULL ,@DCU13_IForcedClosDoor bit = NULL ,@DCU13_ILeafsStopDoor bit = NULL ,@DCU13_IObstacleDoor bit = NULL ,@DCU13_IObstacleStep bit = NULL 
,@DCU13_IOpenAssistDoor bit = NULL ,@DCU13_IStandstillBack bit = NULL ,@DCU14_CDoorCloseConduc bit = NULL ,@DCU14_IDoorClosedSafe bit = NULL ,@DCU14_IDoorClRelease bit = NULL 
,@DCU14_IDoorPbClose bit = NULL ,@DCU14_IDoorPbOpenIn bit = NULL ,@DCU14_IDoorPbOpenOut bit = NULL ,@DCU14_IDoorReleased bit = NULL ,@DCU14_IFootStepRel bit = NULL 
,@DCU14_IForcedClosDoor bit = NULL ,@DCU14_ILeafsStopDoor bit = NULL ,@DCU14_IObstacleDoor bit = NULL ,@DCU14_IObstacleStep bit = NULL ,@DCU14_IOpenAssistDoor bit = NULL 
,@DCU14_IStandstillBack bit = NULL ,@DCU15_CDoorCloseConduc bit = NULL ,@DCU15_IDoorClosedSafe bit = NULL ,@DCU15_IDoorClRelease bit = NULL ,@DCU15_IDoorPbClose bit = NULL 
,@DCU15_IDoorPbOpenIn bit = NULL ,@DCU15_IDoorPbOpenOut bit = NULL ,@DCU15_IDoorReleased bit = NULL ,@DCU15_IFootStepRel bit = NULL ,@DCU15_IForcedClosDoor bit = NULL 
,@DCU15_ILeafsStopDoor bit = NULL ,@DCU15_IObstacleDoor bit = NULL ,@DCU15_IObstacleStep bit = NULL ,@DCU15_IOpenAssistDoor bit = NULL ,@DCU15_IStandstillBack bit = NULL 
,@DCU16_CDoorCloseConduc bit = NULL ,@DCU16_IDoorClosedSafe bit = NULL ,@DCU16_IDoorClRelease bit = NULL ,@DCU16_IDoorPbClose bit = NULL ,@DCU16_IDoorPbOpenIn bit = NULL 
,@DCU16_IDoorPbOpenOut bit = NULL ,@DCU16_IDoorReleased bit = NULL ,@DCU16_IFootStepRel bit = NULL ,@DCU16_IForcedClosDoor bit = NULL ,@DCU16_ILeafsStopDoor bit = NULL 
,@DCU16_IObstacleDoor bit = NULL ,@DCU16_IObstacleStep bit = NULL ,@DCU16_IOpenAssistDoor bit = NULL ,@DCU16_IStandstillBack bit = NULL ,@DCU17_CDoorCloseConduc bit = NULL 
,@DCU17_IDoorClosedSafe bit = NULL ,@DCU17_IDoorClRelease bit = NULL ,@DCU17_IDoorPbClose bit = NULL ,@DCU17_IDoorPbOpenIn bit = NULL ,@DCU17_IDoorPbOpenOut bit = NULL 
,@DCU17_IDoorReleased bit = NULL ,@DCU17_IFootStepRel bit = NULL ,@DCU17_IForcedClosDoor bit = NULL ,@DCU17_ILeafsStopDoor bit = NULL ,@DCU17_IObstacleDoor bit = NULL 
,@DCU17_IObstacleStep bit = NULL ,@DCU17_IOpenAssistDoor bit = NULL ,@DCU17_IStandstillBack bit = NULL ,@DCU18_CDoorCloseConduc bit = NULL ,@DCU18_IDoorClosedSafe bit = NULL 
,@DCU18_IDoorClRelease bit = NULL ,@DCU18_IDoorPbClose bit = NULL ,@DCU18_IDoorPbOpenIn bit = NULL ,@DCU18_IDoorPbOpenOut bit = NULL ,@DCU18_IDoorReleased bit = NULL 
,@DCU18_IFootStepRel bit = NULL ,@DCU18_IForcedClosDoor bit = NULL ,@DCU18_ILeafsStopDoor bit = NULL ,@DCU18_IObstacleDoor bit = NULL ,@DCU18_IObstacleStep bit = NULL 
,@DCU18_IOpenAssistDoor bit = NULL ,@DCU18_IStandstillBack bit = NULL ,@DCU19_CDoorCloseConduc bit = NULL ,@DCU19_IDoorClosedSafe bit = NULL ,@DCU19_IDoorClRelease bit = NULL 
,@DCU19_IDoorPbClose bit = NULL ,@DCU19_IDoorPbOpenIn bit = NULL ,@DCU19_IDoorPbOpenOut bit = NULL ,@DCU19_IDoorReleased bit = NULL ,@DCU19_IFootStepRel bit = NULL 
,@DCU19_IForcedClosDoor bit = NULL ,@DCU19_ILeafsStopDoor bit = NULL ,@DCU19_IObstacleDoor bit = NULL ,@DCU19_IObstacleStep bit = NULL ,@DCU19_IOpenAssistDoor bit = NULL 
,@DCU19_IStandstillBack bit = NULL ,@DCU20_CDoorCloseConduc bit = NULL ,@DCU20_IDoorClosedSafe bit = NULL ,@DCU20_IDoorClRelease bit = NULL ,@DCU20_IDoorPbClose bit = NULL 
,@DCU20_IDoorPbOpenIn bit = NULL ,@DCU20_IDoorPbOpenOut bit = NULL ,@DCU20_IDoorReleased bit = NULL ,@DCU20_IFootStepRel bit = NULL ,@DCU20_IForcedClosDoor bit = NULL 
,@DCU20_ILeafsStopDoor bit = NULL ,@DCU20_IObstacleDoor bit = NULL ,@DCU20_IObstacleStep bit = NULL ,@DCU20_IOpenAssistDoor bit = NULL ,@DCU20_IStandstillBack bit = NULL 
,@DRS_CDisDoorRelease_4_1 bit = NULL ,@DRS_CDisDoorRelease_4_10 bit = NULL ,@DRS_CDisDoorRelease_4_11 bit = NULL ,@DRS_CDisDoorRelease_4_12 bit = NULL ,@DRS_CDisDoorRelease_4_13 bit = NULL 
,@DRS_CDisDoorRelease_4_14 bit = NULL ,@DRS_CDisDoorRelease_4_15 bit = NULL ,@DRS_CDisDoorRelease_4_16 bit = NULL ,@DRS_CDisDoorRelease_4_17 bit = NULL ,@DRS_CDisDoorRelease_4_18 bit = NULL 
,@DRS_CDisDoorRelease_4_19 bit = NULL ,@DRS_CDisDoorRelease_4_2 bit = NULL ,@DRS_CDisDoorRelease_4_20 bit = NULL ,@DRS_CDisDoorRelease_4_3 bit = NULL ,@DRS_CDisDoorRelease_4_4 bit = NULL 
,@DRS_CDisDoorRelease_4_5 bit = NULL ,@DRS_CDisDoorRelease_4_6 bit = NULL ,@DRS_CDisDoorRelease_4_7 bit = NULL ,@DRS_CDisDoorRelease_4_8 bit = NULL ,@DRS_CDisDoorRelease_4_9 bit = NULL 
,@DRS_CDoorClosAutDis_4_1 bit = NULL ,@DRS_CDoorClosAutDis_4_10 bit = NULL ,@DRS_CDoorClosAutDis_4_11 bit = NULL ,@DRS_CDoorClosAutDis_4_12 bit = NULL ,@DRS_CDoorClosAutDis_4_13 bit = NULL 
,@DRS_CDoorClosAutDis_4_14 bit = NULL ,@DRS_CDoorClosAutDis_4_15 bit = NULL ,@DRS_CDoorClosAutDis_4_16 bit = NULL ,@DRS_CDoorClosAutDis_4_17 bit = NULL ,@DRS_CDoorClosAutDis_4_18 bit = NULL 
,@DRS_CDoorClosAutDis_4_19 bit = NULL ,@DRS_CDoorClosAutDis_4_2 bit = NULL ,@DRS_CDoorClosAutDis_4_20 bit = NULL ,@DRS_CDoorClosAutDis_4_3 bit = NULL ,@DRS_CDoorClosAutDis_4_4 bit = NULL 
,@DRS_CDoorClosAutDis_4_5 bit = NULL ,@DRS_CDoorClosAutDis_4_6 bit = NULL ,@DRS_CDoorClosAutDis_4_7 bit = NULL ,@DRS_CDoorClosAutDis_4_8 bit = NULL ,@DRS_CDoorClosAutDis_4_9 bit = NULL 
,@DRS_CForcedClosDoor_4_1 bit = NULL ,@DRS_CForcedClosDoor_4_10 bit = NULL ,@DRS_CForcedClosDoor_4_11 bit = NULL ,@DRS_CForcedClosDoor_4_12 bit = NULL ,@DRS_CForcedClosDoor_4_13 bit = NULL 
,@DRS_CForcedClosDoor_4_14 bit = NULL ,@DRS_CForcedClosDoor_4_15 bit = NULL ,@DRS_CForcedClosDoor_4_16 bit = NULL ,@DRS_CForcedClosDoor_4_17 bit = NULL ,@DRS_CForcedClosDoor_4_18 bit = NULL 
,@DRS_CForcedClosDoor_4_19 bit = NULL ,@DRS_CForcedClosDoor_4_2 bit = NULL ,@DRS_CForcedClosDoor_4_20 bit = NULL ,@DRS_CForcedClosDoor_4_3 bit = NULL ,@DRS_CForcedClosDoor_4_4 bit = NULL 
,@DRS_CForcedClosDoor_4_5 bit = NULL ,@DRS_CForcedClosDoor_4_6 bit = NULL ,@DRS_CForcedClosDoor_4_7 bit = NULL ,@DRS_CForcedClosDoor_4_8 bit = NULL ,@DRS_CForcedClosDoor_4_9 bit = NULL 
,@DRS_CTestModeReq_M_4_1 bit = NULL ,@DRS_CTestModeReq_M_4_10 bit = NULL ,@DRS_CTestModeReq_M_4_11 bit = NULL ,@DRS_CTestModeReq_M_4_12 bit = NULL ,@DRS_CTestModeReq_M_4_13 bit = NULL 
,@DRS_CTestModeReq_M_4_14 bit = NULL ,@DRS_CTestModeReq_M_4_15 bit = NULL ,@DRS_CTestModeReq_M_4_16 bit = NULL ,@DRS_CTestModeReq_M_4_17 bit = NULL ,@DRS_CTestModeReq_M_4_18 bit = NULL 
,@DRS_CTestModeReq_M_4_19 bit = NULL ,@DRS_CTestModeReq_M_4_2 bit = NULL ,@DRS_CTestModeReq_M_4_20 bit = NULL ,@DRS_CTestModeReq_M_4_3 bit = NULL ,@DRS_CTestModeReq_M_4_4 bit = NULL 
,@DRS_CTestModeReq_M_4_5 bit = NULL ,@DRS_CTestModeReq_M_4_6 bit = NULL ,@DRS_CTestModeReq_M_4_7 bit = NULL ,@DRS_CTestModeReq_M_4_8 bit = NULL ,@DRS_CTestModeReq_M_4_9 bit = NULL 
,@MIO_I1DoorRelLe_4_1 bit = NULL ,@MIO_I1DoorRelLe_4_10 bit = NULL ,@MIO_I1DoorRelLe_4_11 bit = NULL ,@MIO_I1DoorRelLe_4_12 bit = NULL ,@MIO_I1DoorRelLe_4_13 bit = NULL 
,@MIO_I1DoorRelLe_4_14 bit = NULL ,@MIO_I1DoorRelLe_4_15 bit = NULL ,@MIO_I1DoorRelLe_4_16 bit = NULL ,@MIO_I1DoorRelLe_4_17 bit = NULL ,@MIO_I1DoorRelLe_4_18 bit = NULL 
,@MIO_I1DoorRelLe_4_19 bit = NULL ,@MIO_I1DoorRelLe_4_2 bit = NULL ,@MIO_I1DoorRelLe_4_20 bit = NULL ,@MIO_I1DoorRelLe_4_3 bit = NULL ,@MIO_I1DoorRelLe_4_4 bit = NULL 
,@MIO_I1DoorRelLe_4_5 bit = NULL ,@MIO_I1DoorRelLe_4_6 bit = NULL ,@MIO_I1DoorRelLe_4_7 bit = NULL ,@MIO_I1DoorRelLe_4_8 bit = NULL ,@MIO_I1DoorRelLe_4_9 bit = NULL 
,@MIO_I1DoorRelRi_4_1 bit = NULL ,@MIO_I1DoorRelRi_4_10 bit = NULL ,@MIO_I1DoorRelRi_4_11 bit = NULL ,@MIO_I1DoorRelRi_4_12 bit = NULL ,@MIO_I1DoorRelRi_4_13 bit = NULL 
,@MIO_I1DoorRelRi_4_14 bit = NULL ,@MIO_I1DoorRelRi_4_15 bit = NULL ,@MIO_I1DoorRelRi_4_16 bit = NULL ,@MIO_I1DoorRelRi_4_17 bit = NULL ,@MIO_I1DoorRelRi_4_18 bit = NULL 
,@MIO_I1DoorRelRi_4_19 bit = NULL ,@MIO_I1DoorRelRi_4_2 bit = NULL ,@MIO_I1DoorRelRi_4_20 bit = NULL ,@MIO_I1DoorRelRi_4_3 bit = NULL ,@MIO_I1DoorRelRi_4_4 bit = NULL 
,@MIO_I1DoorRelRi_4_5 bit = NULL ,@MIO_I1DoorRelRi_4_6 bit = NULL ,@MIO_I1DoorRelRi_4_7 bit = NULL ,@MIO_I1DoorRelRi_4_8 bit = NULL ,@MIO_I1DoorRelRi_4_9 bit = NULL 
,@MIO_I2DoorRelLe_4_1 bit = NULL ,@MIO_I2DoorRelLe_4_10 bit = NULL ,@MIO_I2DoorRelLe_4_11 bit = NULL ,@MIO_I2DoorRelLe_4_12 bit = NULL ,@MIO_I2DoorRelLe_4_13 bit = NULL 
,@MIO_I2DoorRelLe_4_14 bit = NULL ,@MIO_I2DoorRelLe_4_15 bit = NULL ,@MIO_I2DoorRelLe_4_16 bit = NULL ,@MIO_I2DoorRelLe_4_17 bit = NULL ,@MIO_I2DoorRelLe_4_18 bit = NULL 
,@MIO_I2DoorRelLe_4_19 bit = NULL ,@MIO_I2DoorRelLe_4_2 bit = NULL ,@MIO_I2DoorRelLe_4_20 bit = NULL ,@MIO_I2DoorRelLe_4_3 bit = NULL ,@MIO_I2DoorRelLe_4_4 bit = NULL 
,@MIO_I2DoorRelLe_4_5 bit = NULL ,@MIO_I2DoorRelLe_4_6 bit = NULL ,@MIO_I2DoorRelLe_4_7 bit = NULL ,@MIO_I2DoorRelLe_4_8 bit = NULL ,@MIO_I2DoorRelLe_4_9 bit = NULL 
,@MIO_I2DoorRelRi_4_1 bit = NULL ,@MIO_I2DoorRelRi_4_10 bit = NULL ,@MIO_I2DoorRelRi_4_11 bit = NULL ,@MIO_I2DoorRelRi_4_12 bit = NULL ,@MIO_I2DoorRelRi_4_13 bit = NULL 
,@MIO_I2DoorRelRi_4_14 bit = NULL ,@MIO_I2DoorRelRi_4_15 bit = NULL ,@MIO_I2DoorRelRi_4_16 bit = NULL ,@MIO_I2DoorRelRi_4_17 bit = NULL ,@MIO_I2DoorRelRi_4_18 bit = NULL 
,@MIO_I2DoorRelRi_4_19 bit = NULL ,@MIO_I2DoorRelRi_4_2 bit = NULL ,@MIO_I2DoorRelRi_4_20 bit = NULL ,@MIO_I2DoorRelRi_4_3 bit = NULL ,@MIO_I2DoorRelRi_4_4 bit = NULL 
,@MIO_I2DoorRelRi_4_5 bit = NULL ,@MIO_I2DoorRelRi_4_6 bit = NULL ,@MIO_I2DoorRelRi_4_7 bit = NULL ,@MIO_I2DoorRelRi_4_8 bit = NULL ,@MIO_I2DoorRelRi_4_9 bit = NULL 
,@MIO_IDoorLoopBypa1_4_1 bit = NULL ,@MIO_IDoorLoopBypa1_4_10 bit = NULL ,@MIO_IDoorLoopBypa1_4_11 bit = NULL ,@MIO_IDoorLoopBypa1_4_12 bit = NULL ,@MIO_IDoorLoopBypa1_4_13 bit = NULL 
,@MIO_IDoorLoopBypa1_4_14 bit = NULL ,@MIO_IDoorLoopBypa1_4_15 bit = NULL ,@MIO_IDoorLoopBypa1_4_16 bit = NULL ,@MIO_IDoorLoopBypa1_4_17 bit = NULL ,@MIO_IDoorLoopBypa1_4_18 bit = NULL 
,@MIO_IDoorLoopBypa1_4_19 bit = NULL ,@MIO_IDoorLoopBypa1_4_2 bit = NULL ,@MIO_IDoorLoopBypa1_4_20 bit = NULL ,@MIO_IDoorLoopBypa1_4_3 bit = NULL ,@MIO_IDoorLoopBypa1_4_4 bit = NULL 
,@MIO_IDoorLoopBypa1_4_5 bit = NULL ,@MIO_IDoorLoopBypa1_4_6 bit = NULL ,@MIO_IDoorLoopBypa1_4_7 bit = NULL ,@MIO_IDoorLoopBypa1_4_8 bit = NULL ,@MIO_IDoorLoopBypa1_4_9 bit = NULL 
,@MIO_IDoorLoopBypa2_4_1 bit = NULL ,@MIO_IDoorLoopBypa2_4_10 bit = NULL ,@MIO_IDoorLoopBypa2_4_11 bit = NULL ,@MIO_IDoorLoopBypa2_4_12 bit = NULL ,@MIO_IDoorLoopBypa2_4_13 bit = NULL 
,@MIO_IDoorLoopBypa2_4_14 bit = NULL ,@MIO_IDoorLoopBypa2_4_15 bit = NULL ,@MIO_IDoorLoopBypa2_4_16 bit = NULL ,@MIO_IDoorLoopBypa2_4_17 bit = NULL ,@MIO_IDoorLoopBypa2_4_18 bit = NULL 
,@MIO_IDoorLoopBypa2_4_19 bit = NULL ,@MIO_IDoorLoopBypa2_4_2 bit = NULL ,@MIO_IDoorLoopBypa2_4_20 bit = NULL ,@MIO_IDoorLoopBypa2_4_3 bit = NULL ,@MIO_IDoorLoopBypa2_4_4 bit = NULL 
,@MIO_IDoorLoopBypa2_4_5 bit = NULL ,@MIO_IDoorLoopBypa2_4_6 bit = NULL ,@MIO_IDoorLoopBypa2_4_7 bit = NULL ,@MIO_IDoorLoopBypa2_4_8 bit = NULL ,@MIO_IDoorLoopBypa2_4_9 bit = NULL 
,@MIO_IDoorLoopCl1_4_1 bit = NULL ,@MIO_IDoorLoopCl1_4_10 bit = NULL ,@MIO_IDoorLoopCl1_4_11 bit = NULL ,@MIO_IDoorLoopCl1_4_12 bit = NULL ,@MIO_IDoorLoopCl1_4_13 bit = NULL 
,@MIO_IDoorLoopCl1_4_14 bit = NULL ,@MIO_IDoorLoopCl1_4_15 bit = NULL ,@MIO_IDoorLoopCl1_4_16 bit = NULL ,@MIO_IDoorLoopCl1_4_17 bit = NULL ,@MIO_IDoorLoopCl1_4_18 bit = NULL 
,@MIO_IDoorLoopCl1_4_19 bit = NULL ,@MIO_IDoorLoopCl1_4_2 bit = NULL ,@MIO_IDoorLoopCl1_4_20 bit = NULL ,@MIO_IDoorLoopCl1_4_3 bit = NULL ,@MIO_IDoorLoopCl1_4_4 bit = NULL 
,@MIO_IDoorLoopCl1_4_5 bit = NULL ,@MIO_IDoorLoopCl1_4_6 bit = NULL ,@MIO_IDoorLoopCl1_4_7 bit = NULL ,@MIO_IDoorLoopCl1_4_8 bit = NULL ,@MIO_IDoorLoopCl1_4_9 bit = NULL 
,@MIO_IDoorLoopCl2_4_1 bit = NULL ,@MIO_IDoorLoopCl2_4_10 bit = NULL ,@MIO_IDoorLoopCl2_4_11 bit = NULL ,@MIO_IDoorLoopCl2_4_12 bit = NULL ,@MIO_IDoorLoopCl2_4_13 bit = NULL 
,@MIO_IDoorLoopCl2_4_14 bit = NULL ,@MIO_IDoorLoopCl2_4_15 bit = NULL ,@MIO_IDoorLoopCl2_4_16 bit = NULL ,@MIO_IDoorLoopCl2_4_17 bit = NULL ,@MIO_IDoorLoopCl2_4_18 bit = NULL 
,@MIO_IDoorLoopCl2_4_19 bit = NULL ,@MIO_IDoorLoopCl2_4_2 bit = NULL ,@MIO_IDoorLoopCl2_4_20 bit = NULL ,@MIO_IDoorLoopCl2_4_3 bit = NULL ,@MIO_IDoorLoopCl2_4_4 bit = NULL 
,@MIO_IDoorLoopCl2_4_5 bit = NULL ,@MIO_IDoorLoopCl2_4_6 bit = NULL ,@MIO_IDoorLoopCl2_4_7 bit = NULL ,@MIO_IDoorLoopCl2_4_8 bit = NULL ,@MIO_IDoorLoopCl2_4_9 bit = NULL 
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT

DECLARE @Id bigint = null
,@unitID int

SELECT @unitId = ID FROM unit where UnitNumber = @UnitNumber

SELECT @id = ID FROM ChannelValue where TimeStamp = @Timestamp and UnitId = @unitID

IF @id IS NULL
BEGIN


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[col9],[col10],[col11],[col12],[col13],[col14],[col15],[col16]
,[col17],[col18],[col19],[col20],[col21],[col22],[col23],[col24],[col25],[col26],[col27],[col28],[col29],[col30],[col31],[col32]
,[col33],[col34],[col35],[col36],[col37],[col38],[col39],[col40],[col41],[col42],[col43],[col44],[col45],[col46],[col47],[col48]
,[col49],[col50],[col51],[col52],[col53],[col54],[col55],[col56],[col57],[col58],[col59],[col60],[col61],[col62],[col63],[col64]
,[col65],[col66],[col67],[col68],[col69],[col70],[col71],[col72],[col73],[col74],[col75],[col76],[col77],[col78],[col79],[col80]
,[col81],[col82],[col83],[col84],[col85],[col86],[col87],[col88],[col89],[col90],[col91],[col92],[col93],[col94],[col95],[col96]
,[col97],[col98],[col99],[col100],[col101],[col102],[col103],[col104],[col105]
,[col106],[col107],[col108],[col109],[col110],[col111],[col112]
,[col113],[col114],[col115],[col116],[col117],[col118],[col119]
,[col120],[col121],[col122],[col123],[col124],[col125],[col126]
,[col127],[col128],[col129],[col130],[col131],[col132],[col133]
,[col134],[col135],[col136],[col137],[col138],[col139],[col140]
,[col141],[col142],[col143],[col144],[col145],[col146],[col147]
,[col148],[col149],[col150],[col151],[col152],[col153],[col154]
,[col155],[col156],[col157],[col158],[col159],[col160],[col161]
,[col162],[col163],[col164],[col165],[col166],[col167],[col168]
,[col169],[col170],[col171],[col172],[col173],[col174],[col175]
,[col176],[col177],[col178],[col179],[col180],[col181],[col182]
,[col183],[col184],[col185],[col186],[col187],[col188],[col189]
,[col190],[col191],[col192],[col193],[col194],[col195],[col196]
,[col197],[col198],[col199],[col200],[col201],[col202],[col203]
,[col204],[col205],[col206],[col207],[col208],[col209],[col210]
,[col211],[col212],[col213],[col214],[col215],[col216],[col217]
,[col218],[col219],[col220],[col221],[col222],[col223],[col224]
,[col225],[col226],[col227],[col228],[col229],[col230],[col231]
,[col232],[col233],[col234],[col235],[col236],[col237],[col238]
,[col239],[col240],[col241],[col242],[col243],[col244],[col245]
,[col246],[col247],[col248],[col249],[col250],[col251],[col252]
,[col253],[col254],[col255],[col256],[col257],[col258],[col259]
,[col260],[col261],[col262],[col263],[col264],[col265],[col266]
,[col267],[col268],[col269],[col270],[col271],[col272],[col273]
,[col274],[col275],[col276],[col277],[col278],[col279],[col280]
,[col281],[col282],[col283],[col284],[col285],[col286],[col287]
,[col288],[col289],[col290],[col291],[col292],[col293],[col294]
,[col295],[col296],[col297],[col298],[col299],[col300],[col301]
,[col302],[col303],[col304],[col305],[col306],[col307],[col308]
,[col309],[col310],[col311],[col312],[col313],[col314],[col315]
,[col316],[col317],[col318],[col319],[col320],[col321],[col322]
,[col323],[col324],[col325],[col326],[col327],[col328],[col329]
,[col330],[col331],[col332],[col333],[col334],[col335],[col336]
,[col337],[col338],[col339],[col340],[col341],[col342],[col343]
,[col344],[col345],[col346],[col347],[col348],[col349],[col350]
,[col351],[col352],[col353],[col354],[col355],[col356],[col357]
,[col358],[col359],[col360],[col361],[col362],[col363],[col364]
,[col365],[col366],[col367],[col368],[col369],[col370],[col371]
,[col372],[col373],[col374],[col375],[col376],[col377],[col378]
,[col379],[col380],[col381],[col382],[col383],[col384],[col385]
,[col386],[col387],[col388],[col389],[col390],[col391],[col392]
,[col393],[col394],[col395],[col396],[col397],[col398],[col399]
,[col400],[col401],[col402],[col403],[col404],[col405],[col406]
,[col407],[col408],[col409],[col410],[col411],[col412],[col413]
,[col414],[col415],[col416],[col417],[col418],[col419],[col420]
,[col421],[col422],[col423],[col424],[col425],[col426],[col427]
,[col428],[col429],[col430],[col431],[col432],[col433],[col434]
,[col435],[col436],[col437],[col438],[col439],[col440],[col441]
,[col442],[col443],[col444],[col445],[col446],[col447],[col448]
,[col449],[col450],[col451],[col452],[col453],[col454],[col455]
,[col456],[col457],[col458],[col459],[col460],[col461],[col462]
,[col463],[col464],[col465],[col466],[col467],[col468],[col469]
,[col470],[col471],[col472],[col473],[col474],[col475],[col476]
,[col477],[col478],[col479],[col480],[col481],[col482],[col483]
,[col484],[col485],[col486],[col487],[col488],[col489],[col490]
,[col491],[col492],[col493],[col494],[col495],[col496],[col497]
,[col498],[col499],[col500],[col501],[col502],[col503],[col504]
,[col505],[col506],[col507],[col508],[col509],[col510],[col511]
,[col512],[col513],[col514],[col515],[col516],[col517],[col518]
,[col519],[col520],[col521],[col522],[col523],[col524],[col525]
,[col526],[col527],[col528],[col529],[col530],[col531],[col532]
,[col533],[col534],[col535],[col536],[col537],[col538],[col539]
,[col540],[col541],[col542],[col543],[col544],[col545],[col546]
,[col547],[col548],[col549],[col550],[col551],[col552],[col553]
,[col554],[col555],[col556],[col557],[col558],[col559],[col560]
,[col561],[col562],[col563],[col564],[col565],[col566],[col567]
,[col568],[col569],[col570],[col571],[col572],[col573],[col574]
,[col575],[col576],[col577],[col578],[col579],[col580],[col581]
,[col582],[col583],[col584],[col585],[col586],[col587],[col588]
,[col589],[col590],[col591],[col592],[col593],[col594],[col595]
,[col596],[col597],[col598],[col599],[col600],[col601],[col602]
,[col603],[col604],[col605],[col606],[col607],[col608],[col609]
,[col610],[col611],[col612],[col613],[col614],[col615],[col616]
,[col617],[col618],[col619],[col620],[col621],[col622],[col623]
,[col624],[col625],[col626],[col627],[col628],[col629],[col630]
,[col631],[col632],[col633],[col634],[col635],[col636],[col637]
,[col638],[col639],[col640],[col641],[col642],[col643],[col644]
,[col645],[col646],[col647],[col648],[col649],[col650],[col651]
,[col652],[col653],[col654],[col655],[col656],[col657],[col658]
,[col659],[col660],[col661],[col662],[col663]
,[Col664],[Col665],[Col666],[Col667],[Col668],[Col669],[Col670]
,[Col671],[Col672],[Col673],[Col674],[Col675],[Col676],[Col677]
,[Col678],[Col679],[Col680],[Col681],[Col682],[Col683],[Col684]
,[Col685],[Col686],[Col687],[Col688],[Col689],[Col690],[Col691]
,[Col692],[Col693],[Col694],[Col695],[Col696],[Col697],[Col698]
,[Col699],[Col700],[Col701],[Col702],[Col703],[Col704],[Col705]
,[Col706],[Col707],[Col708],[Col709],[Col710],[Col711],[Col712]
,[Col713],[Col714],[Col715]
           )
	OUTPUT inserted.ID INTO @IDs(ID) 
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@MCG_ILatitude, col1) ,ISNULL(@MCG_ILongitude, col2) ,ISNULL(@DIRECTION_CONTROL, col3) ,ISNULL(@CURRENT_SPEED, col4),ISNULL(@AUX1_CDeratingReq_1_1,col5),ISNULL(@AUX1_I3ACContactorOn_1_1, col6) 
,ISNULL(@AUX1_I3ACContactorOn_2_1, col7) ,ISNULL(@AUX1_I3ACContactorOn_2_2, col8) ,ISNULL(@AUX1_I3ACLoadEnabled_1_1, col9) ,ISNULL(@AUX1_I3ACOk_1_1, col10) ,ISNULL(@AUX1_IInputVoltageOk_1_1, col11) 
,ISNULL(@AUX1_IInverterEnabled_1_1, col12) ,ISNULL(@AUX1_ILineContactorOn_1_1, col13) ,ISNULL(@AUX1_IOutputVoltageOk_1_1, col14) ,ISNULL(@AUX1_IPreChargContOn_1_1, col15) ,ISNULL(@AUX1_ITempHeatSinkOk_1_1, col16) 
,ISNULL(@AUX2_CDeratingReq_1_2, col17) ,ISNULL(@AUX2_I3ACContactorOn_1_2, col18) ,ISNULL(@AUX2_I3ACContactorOn_2_1, col19) ,ISNULL(@AUX2_I3ACContactorOn_2_2, col20) ,ISNULL(@AUX2_I3ACLoadEnabled_1_2, col21) 
,ISNULL(@AUX2_I3ACOk_1_2, col22) ,ISNULL(@AUX2_IInputVoltageOk_1_2, col23) ,ISNULL(@AUX2_IInverterEnabled_1_2, col24) ,ISNULL(@AUX2_ILineContactorOn_1_2, col25) ,ISNULL(@AUX2_IOutputVoltageOk_1_2, col26) 
,ISNULL(@AUX2_IPreChargContOn_1_2, col27) ,ISNULL(@AUX2_ITempHeatSinkOk_1_2, col28) ,ISNULL(@AUXY_IBatCont1On, col29) ,ISNULL(@AUXY_IBatCont1On_1_1, col30) ,ISNULL(@AUXY_IBatCont1On_1_2, col31) 
,ISNULL(@AUXY_IBatCont1On_2_1, col32) ,ISNULL(@AUXY_IBatCont1On_2_2, col33) ,ISNULL(@AUXY_IBatCont1On_3_1, col34) ,ISNULL(@AUXY_IBatCont1On_3_2, col35) ,ISNULL(@AUXY_IBatCont1On_8_1, col36) 
,ISNULL(@AUXY_IBatCont1On_8_2, col37) ,ISNULL(@AUXY_IBatCont1On_8_3, col38) ,ISNULL(@AUXY_IBatCont1On_8_4, col39) ,ISNULL(@AUXY_IBatCont1On_8_5, col40) ,ISNULL(@AUXY_IBatCont1On_8_6, col41) 
,ISNULL(@AUXY_IBatCont1On_8_7, col42) ,ISNULL(@AUXY_IBatCont1On_8_8, col43) ,ISNULL(@AUXY_IBatCont1On_9, col44) ,ISNULL(@AUXY_IBatCont2On, col45) ,ISNULL(@AUXY_IBatCont2On_1_1, col46) 
,ISNULL(@AUXY_IBatCont2On_1_2, col47) ,ISNULL(@AUXY_IBatCont2On_2_1, col48) ,ISNULL(@AUXY_IBatCont2On_2_2, col49) ,ISNULL(@AUXY_IBatCont2On_3_1, col50) ,ISNULL(@AUXY_IBatCont2On_3_2, col51) 
,ISNULL(@AUXY_IBatCont2On_8_1, col52) ,ISNULL(@AUXY_IBatCont2On_8_2, col53) ,ISNULL(@AUXY_IBatCont2On_8_3, col54) ,ISNULL(@AUXY_IBatCont2On_8_4, col55) ,ISNULL(@AUXY_IBatCont2On_8_5, col56) 
,ISNULL(@AUXY_IBatCont2On_8_6, col57) ,ISNULL(@AUXY_IBatCont2On_8_7, col58) ,ISNULL(@AUXY_IBatCont2On_8_8, col59) ,ISNULL(@AUXY_IBatCont2On_9, col60) ,ISNULL(@BCHA1_I110VAuxPowSup_2_1, col61) 
,ISNULL(@BCHA1_IBattCurr_2_1, col62) ,ISNULL(@BCHA1_IBattOutputCurr_2_1, col63) ,ISNULL(@BCHA1_IChargCurrMax_2_1, col64) ,ISNULL(@BCHA1_IDcLinkVoltage_2_1, col65) ,ISNULL(@BCHA1_IExternal400Vac, col66) 
,ISNULL(@BCHA1_IExternal400Vac_1_1, col67) ,ISNULL(@BCHA1_IExternal400Vac_1_2, col68) ,ISNULL(@BCHA1_IExternal400Vac_2_1, col69) ,ISNULL(@BCHA1_IExternal400Vac_2_2, col70) ,ISNULL(@BCHA1_IExternal400Vac_3_1, col71) 
,ISNULL(@BCHA1_IExternal400Vac_3_2, col72) ,ISNULL(@BCHA1_IExternal400Vac_8_1, col73) ,ISNULL(@BCHA1_IExternal400Vac_8_2, col74) ,ISNULL(@BCHA1_IExternal400Vac_8_3, col75) ,ISNULL(@BCHA1_IExternal400Vac_8_4, col76) 
,ISNULL(@BCHA1_IExternal400Vac_8_5, col77) ,ISNULL(@BCHA1_IExternal400Vac_8_6, col78) ,ISNULL(@BCHA1_IExternal400Vac_8_7, col79) ,ISNULL(@BCHA1_IExternal400Vac_8_8, col80) ,ISNULL(@BCHA1_IExternal400Vac_9, col81) 
,ISNULL(@BCHA1_IOutputPower_2_1, col82) ,ISNULL(@BCHA1_ISpecSystNokMod_2_1, col83) ,ISNULL(@BCHA1_ISpecSystOkMode_2_1, col84) ,ISNULL(@BCHA1_ITempBattSens1_2_1, col85) ,ISNULL(@BCHA1_ITempBattSens2_2_1, col86) 
,ISNULL(@BCHA1_ITempHeatSink_2_1, col87) ,ISNULL(@BCHA2_I110VAuxPowSup_2_2, col88) ,ISNULL(@BCHA2_IBattCurr_2_2, col89) ,ISNULL(@BCHA2_IBattOutputCurr_2_2, col90) ,ISNULL(@BCHA2_IChargCurrMax_2_2, col91) 
,ISNULL(@BCHA2_IDcLinkVoltage_2_2, col92) ,ISNULL(@BCHA2_IExternal400Vac, col93) ,ISNULL(@BCHA2_IExternal400Vac_1_1, col94) ,ISNULL(@BCHA2_IExternal400Vac_1_2, col95) ,ISNULL(@BCHA2_IExternal400Vac_2_1, col96) 
,ISNULL(@BCHA2_IExternal400Vac_2_2, col97) ,ISNULL(@BCHA2_IExternal400Vac_3_1, col98) ,ISNULL(@BCHA2_IExternal400Vac_3_2, col99) ,ISNULL(@BCHA2_IExternal400Vac_8_1, col100) ,ISNULL(@BCHA2_IExternal400Vac_8_2, col101) 
,ISNULL(@BCHA2_IExternal400Vac_8_3, col102) ,ISNULL(@BCHA2_IExternal400Vac_8_4, col103) ,ISNULL(@BCHA2_IExternal400Vac_8_5, col104) ,ISNULL(@BCHA2_IExternal400Vac_8_6, col105) ,ISNULL(@BCHA2_IExternal400Vac_8_7, col106) 
,ISNULL(@BCHA2_IExternal400Vac_8_8, col107) ,ISNULL(@BCHA2_IExternal400Vac_9, col108) ,ISNULL(@BCHA2_IOutputPower_2_2, col109) ,ISNULL(@BCHA2_ISpecSystNokMod_2_2, col110) ,ISNULL(@BCHA2_ISpecSystOkMode_2_2, col111) 
,ISNULL(@BCHA2_ITempBattSens1_2_2, col112) ,ISNULL(@BCHA2_ITempBattSens2_2_2, col113) ,ISNULL(@BCHA2_ITempHeatSink_2_2, col114) ,ISNULL(@BCU1_IBrPipePressureInt_5_1, col115) ,ISNULL(@BCU1_IMainAirResPipeInt_5_1, col116) 
,ISNULL(@BCU1_IParkBrApplied_5_1, col117) ,ISNULL(@BCU1_IParkBrLocked_5_1, col118) ,ISNULL(@BCU1_IParkBrReleased_5_1, col119) ,ISNULL(@BCU2_IBrPipePressureInt_5_2, col120) ,ISNULL(@BCU2_IMainAirResPipeInt_5_2, col121) 
,ISNULL(@BCU2_IParkBrApplied_5_2, col122) ,ISNULL(@BCU2_IParkBrLocked_5_2, col123) ,ISNULL(@BCU2_IParkBrReleased_5_2, col124) ,ISNULL(@BCUm_IBrTestRun_5_1, col125) ,ISNULL(@BCUm_IBrTestRun_5_2, col126) 
,ISNULL(@BCUx_IBogie1Locked_5_1, col127) ,ISNULL(@BCUx_IBogie1Locked_5_2, col128) ,ISNULL(@BCUx_IBogie2Locked_5_1, col129) ,ISNULL(@BCUx_IBogie2Locked_5_2, col130) ,ISNULL(@BCUx_IBogie3Locked_5_1, col131) 
,ISNULL(@BCUx_IBogie3Locked_5_2, col132) ,ISNULL(@BCUx_IBogie4Locked_5_1, col133) ,ISNULL(@BCUx_IBogie4Locked_5_2, col134) ,ISNULL(@BCUx_IBogie5Locked_5_1, col135) ,ISNULL(@BCUx_IBogie5Locked_5_2, col136) 
,ISNULL(@BCUx_IBogie6Locked_5_1, col137) ,ISNULL(@BCUx_IBogie6Locked_5_2, col138) ,ISNULL(@BCUx_IBogie7Locked_5_1, col139) ,ISNULL(@BCUx_IBogie7Locked_5_2, col140) ,ISNULL(@BCUx_IWspBogie1Slide_3_1, col141) 
,ISNULL(@BCUx_IWspBogie1Slide_3_2, col142) ,ISNULL(@BCUx_IWspBogie1Slide_5_1, col143) ,ISNULL(@BCUx_IWspBogie1Slide_5_2, col144) ,ISNULL(@BCUx_IWspBogie2Slide_3_1, col145) ,ISNULL(@BCUx_IWspBogie2Slide_3_2, col146) 
,ISNULL(@BCUx_IWspBogie2Slide_5_1, col147) ,ISNULL(@BCUx_IWspBogie2Slide_5_2, col148) ,ISNULL(@BCUx_IWspBogie3Slide_3_1, col149) ,ISNULL(@BCUx_IWspBogie3Slide_3_2, col150) ,ISNULL(@BCUx_IWspBogie3Slide_5_1, col151) 
,ISNULL(@BCUx_IWspBogie3Slide_5_2, col152) ,ISNULL(@BCUx_IWspBogie4Slide_3_1, col153) ,ISNULL(@BCUx_IWspBogie4Slide_3_2, col154) ,ISNULL(@BCUx_IWspBogie4Slide_5_1, col155) ,ISNULL(@BCUx_IWspBogie4Slide_5_2, col156) 
,ISNULL(@BCUx_IWspBogie5Slide_3_1, col157) ,ISNULL(@BCUx_IWspBogie5Slide_3_2, col158) ,ISNULL(@BCUx_IWspBogie5Slide_5_1, col159) ,ISNULL(@BCUx_IWspBogie5Slide_5_2, col160) ,ISNULL(@BCUx_IWspBogie6Slide_3_1, col161) 
,ISNULL(@BCUx_IWspBogie6Slide_3_2, col162) ,ISNULL(@BCUx_IWspBogie6Slide_5_1, col163) ,ISNULL(@BCUx_IWspBogie6Slide_5_2, col164) ,ISNULL(@BCUx_IWspBogie7Slide_3_1, col165) ,ISNULL(@BCUx_IWspBogie7Slide_3_2, col166) 
,ISNULL(@BCUx_IWspBogie7Slide_5_1, col167) ,ISNULL(@BCUx_IWspBogie7Slide_5_2, col168) ,ISNULL(@CABIN, col169) ,ISNULL(@CABIN_IST, col170) ,ISNULL(@CABIN_SOLL, col171) 
,ISNULL(@CABIN_STATUS, col172) ,ISNULL(@CYCLE_COUNTER, col173) ,ISNULL(@DBC_CBCU1ModeTowing_5_1, col174) ,ISNULL(@DBC_CBCU1ModeTowing_5_2, col175) ,ISNULL(@DBC_CBCU2ModeTowing_5_1, col176) 
,ISNULL(@DBC_CBCU2ModeTowing_5_2, col177) ,ISNULL(@DBC_Cbrake_M, col178) ,ISNULL(@DBC_Cbrake_M_1_1, col179) ,ISNULL(@DBC_Cbrake_M_1_2, col180) ,ISNULL(@DBC_Cbrake_M_2_1, col181) 
,ISNULL(@DBC_Cbrake_M_2_2, col182) ,ISNULL(@DBC_Cbrake_M_3_1, col183) ,ISNULL(@DBC_Cbrake_M_3_2, col184) ,ISNULL(@DBC_Cbrake_M_4_1, col185) ,ISNULL(@DBC_Cbrake_M_4_10, col186) 
,ISNULL(@DBC_Cbrake_M_4_11, col187) ,ISNULL(@DBC_Cbrake_M_4_12, col188) ,ISNULL(@DBC_Cbrake_M_4_13, col189) ,ISNULL(@DBC_Cbrake_M_4_14, col190) ,ISNULL(@DBC_Cbrake_M_4_15, col191) 
,ISNULL(@DBC_Cbrake_M_4_16, col192) ,ISNULL(@DBC_Cbrake_M_4_17, col193) ,ISNULL(@DBC_Cbrake_M_4_18, col194) ,ISNULL(@DBC_Cbrake_M_4_19, col195) ,ISNULL(@DBC_Cbrake_M_4_2, col196) 
,ISNULL(@DBC_Cbrake_M_4_20, col197) ,ISNULL(@DBC_Cbrake_M_4_3, col198) ,ISNULL(@DBC_Cbrake_M_4_4, col199) ,ISNULL(@DBC_Cbrake_M_4_5, col200) ,ISNULL(@DBC_Cbrake_M_4_6, col201) 
,ISNULL(@DBC_Cbrake_M_4_7, col202) ,ISNULL(@DBC_Cbrake_M_4_8, col203) ,ISNULL(@DBC_Cbrake_M_4_9, col204) ,ISNULL(@DBC_Cbrake_M_5_1, col205) ,ISNULL(@DBC_Cbrake_M_5_2, col206) 
,ISNULL(@DBC_Cbrake_M_8_1, col207) ,ISNULL(@DBC_Cbrake_M_8_2, col208) ,ISNULL(@DBC_Cbrake_M_8_3, col209) ,ISNULL(@DBC_Cbrake_M_8_4, col210) ,ISNULL(@DBC_Cbrake_M_8_5, col211) 
,ISNULL(@DBC_Cbrake_M_8_6, col212) ,ISNULL(@DBC_Cbrake_M_8_7, col213) ,ISNULL(@DBC_Cbrake_M_8_8, col214) ,ISNULL(@DBC_CBrakeQuick_M, col215) ,ISNULL(@DBC_CBrakeQuick_M_1_1, col216) 
,ISNULL(@DBC_CBrakeQuick_M_1_2, col217) ,ISNULL(@DBC_CBrakeQuick_M_2_1, col218) ,ISNULL(@DBC_CBrakeQuick_M_2_2, col219) ,ISNULL(@DBC_CBrakeQuick_M_3_1, col220) ,ISNULL(@DBC_CBrakeQuick_M_3_2, col221) 
,ISNULL(@DBC_CBrakeQuick_M_4_1, col222) ,ISNULL(@DBC_CBrakeQuick_M_4_10, col223) ,ISNULL(@DBC_CBrakeQuick_M_4_11, col224) ,ISNULL(@DBC_CBrakeQuick_M_4_12, col225) ,ISNULL(@DBC_CBrakeQuick_M_4_13, col226) 
,ISNULL(@DBC_CBrakeQuick_M_4_14, col227) ,ISNULL(@DBC_CBrakeQuick_M_4_15, col228) ,ISNULL(@DBC_CBrakeQuick_M_4_16, col229) ,ISNULL(@DBC_CBrakeQuick_M_4_17, col230) ,ISNULL(@DBC_CBrakeQuick_M_4_18, col231) 
,ISNULL(@DBC_CBrakeQuick_M_4_19, col232) ,ISNULL(@DBC_CBrakeQuick_M_4_2, col233) ,ISNULL(@DBC_CBrakeQuick_M_4_20, col234) ,ISNULL(@DBC_CBrakeQuick_M_4_3, col235) ,ISNULL(@DBC_CBrakeQuick_M_4_4, col236) 
,ISNULL(@DBC_CBrakeQuick_M_4_5, col237) ,ISNULL(@DBC_CBrakeQuick_M_4_6, col238) ,ISNULL(@DBC_CBrakeQuick_M_4_7, col239) ,ISNULL(@DBC_CBrakeQuick_M_4_8, col240) ,ISNULL(@DBC_CBrakeQuick_M_4_9, col241) 
,ISNULL(@DBC_CBrakeQuick_M_5_1, col242) ,ISNULL(@DBC_CBrakeQuick_M_5_2, col243) ,ISNULL(@DBC_CBrakeQuick_M_8_1, col244) ,ISNULL(@DBC_CBrakeQuick_M_8_2, col245) ,ISNULL(@DBC_CBrakeQuick_M_8_3, col246) 
,ISNULL(@DBC_CBrakeQuick_M_8_4, col247) ,ISNULL(@DBC_CBrakeQuick_M_8_5, col248) ,ISNULL(@DBC_CBrakeQuick_M_8_6, col249) ,ISNULL(@DBC_CBrakeQuick_M_8_7, col250) ,ISNULL(@DBC_CBrakeQuick_M_8_8, col251) 
,ISNULL(@DBC_CBrakingEffortInt_3_1, col252) ,ISNULL(@DBC_CBrakingEffortInt_3_2, col253) ,ISNULL(@DBC_CBrakingEffortInt_5_1, col254) ,ISNULL(@DBC_CBrakingEffortInt_5_2, col255) ,ISNULL(@DBC_Cdrive_M, col256) 
,ISNULL(@DBC_Cdrive_M_1_1, col257) ,ISNULL(@DBC_Cdrive_M_1_2, col258) ,ISNULL(@DBC_Cdrive_M_2_1, col259) ,ISNULL(@DBC_Cdrive_M_2_2, col260) ,ISNULL(@DBC_Cdrive_M_3_1, col261) 
,ISNULL(@DBC_Cdrive_M_3_2, col262) ,ISNULL(@DBC_Cdrive_M_4_1, col263) ,ISNULL(@DBC_Cdrive_M_4_10, col264) ,ISNULL(@DBC_Cdrive_M_4_11, col265) ,ISNULL(@DBC_Cdrive_M_4_12, col266) 
,ISNULL(@DBC_Cdrive_M_4_13, col267) ,ISNULL(@DBC_Cdrive_M_4_14, col268) ,ISNULL(@DBC_Cdrive_M_4_15, col269) ,ISNULL(@DBC_Cdrive_M_4_16, col270) ,ISNULL(@DBC_Cdrive_M_4_17, col271) 
,ISNULL(@DBC_Cdrive_M_4_18, col272) ,ISNULL(@DBC_Cdrive_M_4_19, col273) ,ISNULL(@DBC_Cdrive_M_4_2, col274) ,ISNULL(@DBC_Cdrive_M_4_20, col275) ,ISNULL(@DBC_Cdrive_M_4_3, col276) 
,ISNULL(@DBC_Cdrive_M_4_4, col277) ,ISNULL(@DBC_Cdrive_M_4_5, col278) ,ISNULL(@DBC_Cdrive_M_4_6, col279) ,ISNULL(@DBC_Cdrive_M_4_7, col280) ,ISNULL(@DBC_Cdrive_M_4_8, col281) 
,ISNULL(@DBC_Cdrive_M_4_9, col282) ,ISNULL(@DBC_Cdrive_M_5_1, col283) ,ISNULL(@DBC_Cdrive_M_5_2, col284) ,ISNULL(@DBC_Cdrive_M_8_1, col285) ,ISNULL(@DBC_Cdrive_M_8_2, col286) 
,ISNULL(@DBC_Cdrive_M_8_3, col287) ,ISNULL(@DBC_Cdrive_M_8_4, col288) ,ISNULL(@DBC_Cdrive_M_8_5, col289) ,ISNULL(@DBC_Cdrive_M_8_6, col290) ,ISNULL(@DBC_Cdrive_M_8_7, col291) 
,ISNULL(@DBC_Cdrive_M_8_8, col292) ,ISNULL(@DBC_CModeTowing_M_5_1, col293) ,ISNULL(@DBC_CModeTowing_M_5_2, col294) ,ISNULL(@DBC_CTCU1GrInTracConv1_3_1, col295) ,ISNULL(@DBC_CTCU2GrInTracConv2_3_2, col296) 
,ISNULL(@DBC_CTractiveEffort_3_1, col297) ,ISNULL(@DBC_CTractiveEffort_3_2, col298) ,ISNULL(@DBC_ITrainSpeedInt, col299) ,ISNULL(@DBC_ITrainSpeedInt_1_1, col300) ,ISNULL(@DBC_ITrainSpeedInt_1_2, col301) 
,ISNULL(@DBC_ITrainSpeedInt_15, col302) ,ISNULL(@DBC_ITrainSpeedInt_2_1, col303) ,ISNULL(@DBC_ITrainSpeedInt_2_2, col304) ,ISNULL(@DBC_ITrainSpeedInt_3_1, col305) ,ISNULL(@DBC_ITrainSpeedInt_3_2, col306) 
,ISNULL(@DBC_ITrainSpeedInt_4_1, col307) ,ISNULL(@DBC_ITrainSpeedInt_4_10, col308) ,ISNULL(@DBC_ITrainSpeedInt_4_11, col309) ,ISNULL(@DBC_ITrainSpeedInt_4_12, col310) ,ISNULL(@DBC_ITrainSpeedInt_4_13, col311) 
,ISNULL(@DBC_ITrainSpeedInt_4_14, col312) ,ISNULL(@DBC_ITrainSpeedInt_4_15, col313) ,ISNULL(@DBC_ITrainSpeedInt_4_16, col314) ,ISNULL(@DBC_ITrainSpeedInt_4_17, col315) ,ISNULL(@DBC_ITrainSpeedInt_4_18, col316) 
,ISNULL(@DBC_ITrainSpeedInt_4_19, col317) ,ISNULL(@DBC_ITrainSpeedInt_4_2, col318) ,ISNULL(@DBC_ITrainSpeedInt_4_20, col319) ,ISNULL(@DBC_ITrainSpeedInt_4_3, col320) ,ISNULL(@DBC_ITrainSpeedInt_4_4, col321) 
,ISNULL(@DBC_ITrainSpeedInt_4_5, col322) ,ISNULL(@DBC_ITrainSpeedInt_4_6, col323) ,ISNULL(@DBC_ITrainSpeedInt_4_7, col324) ,ISNULL(@DBC_ITrainSpeedInt_4_8, col325) ,ISNULL(@DBC_ITrainSpeedInt_4_9, col326) 
,ISNULL(@DBC_ITrainSpeedInt_5_1, col327) ,ISNULL(@DBC_ITrainSpeedInt_5_2, col328) ,ISNULL(@DBC_ITrainSpeedInt_8_1, col329) ,ISNULL(@DBC_ITrainSpeedInt_8_2, col330) ,ISNULL(@DBC_ITrainSpeedInt_8_3, col331) 
,ISNULL(@DBC_ITrainSpeedInt_8_4, col332) ,ISNULL(@DBC_ITrainSpeedInt_8_5, col333) ,ISNULL(@DBC_ITrainSpeedInt_8_6, col334) ,ISNULL(@DBC_ITrainSpeedInt_8_7, col335) ,ISNULL(@DBC_ITrainSpeedInt_8_8, col336) 
,ISNULL(@DBC_ITrainSpeedInt_9, col337) ,ISNULL(@DBC_IZrSpdInd_4_1, col338) ,ISNULL(@DBC_IZrSpdInd_4_10, col339) ,ISNULL(@DBC_IZrSpdInd_4_11, col340) ,ISNULL(@DBC_IZrSpdInd_4_12, col341) 
,ISNULL(@DBC_IZrSpdInd_4_13, col342) ,ISNULL(@DBC_IZrSpdInd_4_14, col343) ,ISNULL(@DBC_IZrSpdInd_4_15, col344) ,ISNULL(@DBC_IZrSpdInd_4_16, col345) ,ISNULL(@DBC_IZrSpdInd_4_17, col346) 
,ISNULL(@DBC_IZrSpdInd_4_18, col347) ,ISNULL(@DBC_IZrSpdInd_4_19, col348) ,ISNULL(@DBC_IZrSpdInd_4_2, col349) ,ISNULL(@DBC_IZrSpdInd_4_20, col350) ,ISNULL(@DBC_IZrSpdInd_4_3, col351) 
,ISNULL(@DBC_IZrSpdInd_4_4, col352) ,ISNULL(@DBC_IZrSpdInd_4_5, col353) ,ISNULL(@DBC_IZrSpdInd_4_6, col354) ,ISNULL(@DBC_IZrSpdInd_4_7, col355) ,ISNULL(@DBC_IZrSpdInd_4_8, col356) 
,ISNULL(@DBC_IZrSpdInd_4_9, col357) ,ISNULL(@DBC_PRTcu1GrOut_3_1, col358) ,ISNULL(@DBC_PRTcu2GrOut_3_2, col359) ,ISNULL(@DRIVERBRAKE_APPLIED_SUFF, col360) ,ISNULL(@DRIVERBRAKE_OPERATED, col361) 
,ISNULL(@EB_STATUS, col362) ,ISNULL(@ERROR_CODE_INTERNAL, col363) ,ISNULL(@ERRORCODE_1, col364) ,ISNULL(@ERRORCODE_2, col365) ,ISNULL(@ERRORCODE_3, col366) 
,ISNULL(@GW_IGwOrientation, col367) ,ISNULL(@HAC_ITempOutsideInt, col368) ,ISNULL(@HAC_ITempOutsideInt_1_1, col369) ,ISNULL(@HAC_ITempOutsideInt_1_2, col370) ,ISNULL(@HAC_ITempOutsideInt_2_1, col371) 
,ISNULL(@HAC_ITempOutsideInt_2_2, col372) ,ISNULL(@HAC_ITempOutsideInt_3_1, col373) ,ISNULL(@HAC_ITempOutsideInt_3_2, col374) ,ISNULL(@HAC_ITempOutsideInt_4_1, col375) ,ISNULL(@HAC_ITempOutsideInt_4_10, col376) 
,ISNULL(@HAC_ITempOutsideInt_4_11, col377) ,ISNULL(@HAC_ITempOutsideInt_4_12, col378) ,ISNULL(@HAC_ITempOutsideInt_4_13, col379) ,ISNULL(@HAC_ITempOutsideInt_4_14, col380) ,ISNULL(@HAC_ITempOutsideInt_4_15, col381) 
,ISNULL(@HAC_ITempOutsideInt_4_16, col382) ,ISNULL(@HAC_ITempOutsideInt_4_17, col383) ,ISNULL(@HAC_ITempOutsideInt_4_18, col384) ,ISNULL(@HAC_ITempOutsideInt_4_19, col385) ,ISNULL(@HAC_ITempOutsideInt_4_2, col386) 
,ISNULL(@HAC_ITempOutsideInt_4_20, col387) ,ISNULL(@HAC_ITempOutsideInt_4_3, col388) ,ISNULL(@HAC_ITempOutsideInt_4_4, col389) ,ISNULL(@HAC_ITempOutsideInt_4_5, col390) ,ISNULL(@HAC_ITempOutsideInt_4_6, col391) 
,ISNULL(@HAC_ITempOutsideInt_4_7, col392) ,ISNULL(@HAC_ITempOutsideInt_4_8, col393) ,ISNULL(@HAC_ITempOutsideInt_4_9, col394) ,ISNULL(@HAC_ITempOutsideInt_5_1, col395) ,ISNULL(@HAC_ITempOutsideInt_5_2, col396) 
,ISNULL(@HAC_ITempOutsideInt_9, col397) ,ISNULL(@HVAC1_ITempAirMixedInt, col398) ,ISNULL(@HVAC1_ITempDuctAir1Int, col399) ,ISNULL(@HVAC1_ITempDuctAir2Int, col400) ,ISNULL(@HVAC1_ITempInComInt, col401) 
,ISNULL(@HVAC1_ITempOutsideInt, col402) ,ISNULL(@HVAC1_ITempSetpActDuctInt, col403) ,ISNULL(@HVAC1_ITempSetpActInt, col404) ,ISNULL(@HVAC2_ITempAirMixedInt, col405) ,ISNULL(@HVAC2_ITempDuctAir1Int, col406) 
,ISNULL(@HVAC2_ITempDuctAir2Int, col407) ,ISNULL(@HVAC2_ITempInComInt, col408) ,ISNULL(@HVAC2_ITempOutsideInt, col409) ,ISNULL(@HVAC2_ITempSetpActDuctInt, col410) ,ISNULL(@HVAC2_ITempSetpActInt, col411) 
,ISNULL(@HVAC3_ITempAirMixedInt, col412) ,ISNULL(@HVAC3_ITempDuctAir1Int, col413) ,ISNULL(@HVAC3_ITempDuctAir2Int, col414) ,ISNULL(@HVAC3_ITempInComInt, col415) ,ISNULL(@HVAC3_ITempOutsideInt, col416) 
,ISNULL(@HVAC3_ITempSetpActDuctInt, col417) ,ISNULL(@HVAC3_ITempSetpActInt, col418) ,ISNULL(@HVAC4_ITempAirMixedInt, col419) ,ISNULL(@HVAC4_ITempDuctAir1Int, col420) ,ISNULL(@HVAC4_ITempDuctAir2Int, col421) 
,ISNULL(@HVAC4_ITempInComInt, col422) ,ISNULL(@HVAC4_ITempOutsideInt, col423) ,ISNULL(@HVAC4_ITempSetpActDuctInt, col424) ,ISNULL(@HVAC4_ITempSetpActInt, col425) ,ISNULL(@HVAC5_ITempAirMixedInt, col426) 
,ISNULL(@HVAC5_ITempDuctAir1Int, col427) ,ISNULL(@HVAC5_ITempDuctAir2Int, col428) ,ISNULL(@HVAC5_ITempInComInt, col429) ,ISNULL(@HVAC5_ITempOutsideInt, col430) ,ISNULL(@HVAC5_ITempSetpActDuctInt, col431) 
,ISNULL(@HVAC5_ITempSetpActInt, col432) ,ISNULL(@HVAC6_ITempAirMixedInt, col433) ,ISNULL(@HVAC6_ITempDuctAir1Int, col434) ,ISNULL(@HVAC6_ITempDuctAir2Int, col435) ,ISNULL(@HVAC6_ITempInComInt, col436) 
,ISNULL(@HVAC6_ITempOutsideInt, col437) ,ISNULL(@HVAC6_ITempSetpActDuctInt, col438) ,ISNULL(@HVAC6_ITempSetpActInt, col439) ,ISNULL(@HVCC1_ITempDuctAir1Int, col440) ,ISNULL(@HVCC1_ITempInComInt, col441) 
,ISNULL(@HVCC1_ITempOutsideInt, col442) ,ISNULL(@HVCC1_ITempSetpActDuctInt, col443) ,ISNULL(@HVCC1_ITempSetpActInt, col444) ,ISNULL(@HVCC2_ITempDuctAir1Int, col445) ,ISNULL(@HVCC2_ITempInComInt, col446) 
,ISNULL(@HVCC2_ITempOutsideInt, col447) ,ISNULL(@HVCC2_ITempSetpActDuctInt, col448) ,ISNULL(@HVCC2_ITempSetpActInt, col449) ,ISNULL(@INadiCtrlInfo1, col450) ,ISNULL(@INadiCtrlInfo2, col451) 
,ISNULL(@INadiCtrlInfo3, col452) ,ISNULL(@INadiNumberEntries, col453) ,ISNULL(@INadiTcnAddr1, col454) ,ISNULL(@INadiTcnAddr2, col455) ,ISNULL(@INadiTcnAddr3, col456) 
,ISNULL(@INadiTopoCount, col457) ,ISNULL(@INadiUicAddr1, col458) ,ISNULL(@INadiUicAddr2, col459) ,ISNULL(@INadiUicAddr3, col460) ,ISNULL(@INTERNAL_ERROR_CODE, col461) 
,ISNULL(@INTERNAL_STATE, col462) ,ISNULL(@ITrainsetNumber1, col463) ,ISNULL(@ITrainsetNumber2, col464) ,ISNULL(@ITrainsetNumber3, col465) ,ISNULL(@MCG_IGsmSigStrength, col466) 
,ISNULL(@MIO_IContBusBar400On, col467) ,ISNULL(@MIO_IContBusBar400On_1_1, col468) ,ISNULL(@MIO_IContBusBar400On_1_2, col469) ,ISNULL(@MIO_IContBusBar400On_2_1, col470) ,ISNULL(@MIO_IContBusBar400On_2_2, col471) 
,ISNULL(@MIO_IContBusBar400On_3_1, col472) ,ISNULL(@MIO_IContBusBar400On_3_2, col473) ,ISNULL(@MIO_IContBusBar400On_8_1, col474) ,ISNULL(@MIO_IContBusBar400On_8_2, col475) ,ISNULL(@MIO_IContBusBar400On_8_3, col476) 
,ISNULL(@MIO_IContBusBar400On_8_4, col477) ,ISNULL(@MIO_IContBusBar400On_8_5, col478) ,ISNULL(@MIO_IContBusBar400On_8_6, col479) ,ISNULL(@MIO_IContBusBar400On_8_7, col480) ,ISNULL(@MIO_IContBusBar400On_8_8, col481) 
,ISNULL(@MIO_IContBusBar400On_9, col482) ,ISNULL(@MIO_IHscbOff, col483) ,ISNULL(@MIO_IHscbOff_1_1, col484) ,ISNULL(@MIO_IHscbOff_1_2, col485) ,ISNULL(@MIO_IHscbOff_2_1, col486) 
,ISNULL(@MIO_IHscbOff_2_2, col487) ,ISNULL(@MIO_IHscbOff_3_1, col488) ,ISNULL(@MIO_IHscbOff_3_2, col489) ,ISNULL(@MIO_IHscbOff_8_1, col490) ,ISNULL(@MIO_IHscbOff_8_2, col491) 
,ISNULL(@MIO_IHscbOff_8_3, col492) ,ISNULL(@MIO_IHscbOff_8_4, col493) ,ISNULL(@MIO_IHscbOff_8_5, col494) ,ISNULL(@MIO_IHscbOff_8_6, col495) ,ISNULL(@MIO_IHscbOff_8_7, col496) 
,ISNULL(@MIO_IHscbOff_8_8, col497) ,ISNULL(@MIO_IHscbOn, col498) ,ISNULL(@MIO_IHscbOn_1_1, col499) ,ISNULL(@MIO_IHscbOn_1_2, col500) ,ISNULL(@MIO_IHscbOn_2_1, col501) 
,ISNULL(@MIO_IHscbOn_2_2, col502) ,ISNULL(@MIO_IHscbOn_3_1, col503) ,ISNULL(@MIO_IHscbOn_3_2, col504) ,ISNULL(@MIO_IHscbOn_8_1, col505) ,ISNULL(@MIO_IHscbOn_8_2, col506) 
,ISNULL(@MIO_IHscbOn_8_3, col507) ,ISNULL(@MIO_IHscbOn_8_4, col508) ,ISNULL(@MIO_IHscbOn_8_5, col509) ,ISNULL(@MIO_IHscbOn_8_6, col510) ,ISNULL(@MIO_IHscbOn_8_7, col511) 
,ISNULL(@MIO_IHscbOn_8_8, col512) ,ISNULL(@MIO_IPantoPrssSw1On, col513) ,ISNULL(@MIO_IPantoPrssSw1On_1_1, col514) ,ISNULL(@MIO_IPantoPrssSw1On_1_2, col515) ,ISNULL(@MIO_IPantoPrssSw1On_2_1, col516) 
,ISNULL(@MIO_IPantoPrssSw1On_2_2, col517) ,ISNULL(@MIO_IPantoPrssSw1On_3_1, col518) ,ISNULL(@MIO_IPantoPrssSw1On_3_2, col519) ,ISNULL(@MIO_IPantoPrssSw1On_8_1, col520) ,ISNULL(@MIO_IPantoPrssSw1On_8_2, col521) 
,ISNULL(@MIO_IPantoPrssSw1On_8_3, col522) ,ISNULL(@MIO_IPantoPrssSw1On_8_4, col523) ,ISNULL(@MIO_IPantoPrssSw1On_8_5, col524) ,ISNULL(@MIO_IPantoPrssSw1On_8_6, col525) ,ISNULL(@MIO_IPantoPrssSw1On_8_7, col526) 
,ISNULL(@MIO_IPantoPrssSw1On_8_8, col527) ,ISNULL(@MIO_IPantoPrssSw1On_9, col528) ,ISNULL(@MIO_IPantoPrssSw2On, col529) ,ISNULL(@MIO_IPantoPrssSw2On_1_1, col530) ,ISNULL(@MIO_IPantoPrssSw2On_1_2, col531) 
,ISNULL(@MIO_IPantoPrssSw2On_2_1, col532) ,ISNULL(@MIO_IPantoPrssSw2On_2_2, col533) ,ISNULL(@MIO_IPantoPrssSw2On_3_1, col534) ,ISNULL(@MIO_IPantoPrssSw2On_3_2, col535) ,ISNULL(@MIO_IPantoPrssSw2On_8_1, col536) 
,ISNULL(@MIO_IPantoPrssSw2On_8_2, col537) ,ISNULL(@MIO_IPantoPrssSw2On_8_3, col538) ,ISNULL(@MIO_IPantoPrssSw2On_8_4, col539) ,ISNULL(@MIO_IPantoPrssSw2On_8_5, col540) ,ISNULL(@MIO_IPantoPrssSw2On_8_6, col541) 
,ISNULL(@MIO_IPantoPrssSw2On_8_7, col542) ,ISNULL(@MIO_IPantoPrssSw2On_8_8, col543) ,ISNULL(@MIO_IPantoPrssSw2On_9, col544) ,ISNULL(@MMI_EB_STATUS, col545) ,ISNULL(@MMI_M_ACTIVE_CABIN, col546) 
,ISNULL(@MMI_M_LEVEL, col547) ,ISNULL(@MMI_M_MODE, col548) ,ISNULL(@MMI_M_WARNING, col549) ,ISNULL(@MMI_SB_STATUS, col550) ,ISNULL(@MMI_V_PERMITTED, col551) 
,ISNULL(@MMI_V_TRAIN, col552) ,ISNULL(@NID_STM_1, col553) ,ISNULL(@NID_STM_2, col554) ,ISNULL(@NID_STM_3, col555) ,ISNULL(@NID_STM_4, col556) 
,ISNULL(@NID_STM_5, col557) ,ISNULL(@NID_STM_6, col558) ,ISNULL(@NID_STM_7, col559) ,ISNULL(@NID_STM_8, col560) ,ISNULL(@NID_STM_DA, col561) 
,ISNULL(@ONE_STM_DA, col562) ,ISNULL(@ONE_STM_HS, col563) ,ISNULL(@PB_ADDR, col564) ,ISNULL(@PERMITTED_SPEED, col565) ,ISNULL(@Q_ODO, col566) 
,ISNULL(@REPORTED_DIRECTION, col567) ,ISNULL(@REPORTED_SPEED, col568) ,ISNULL(@REPORTED_SPEED_HISTORIC, col569) ,ISNULL(@ROTATION_DIRECTION_SDU1, col570) ,ISNULL(@ROTATION_DIRECTION_SDU2, col571) 
,ISNULL(@SAP, col572) ,ISNULL(@SB_STATUS, col573) ,ISNULL(@SDU_DIRECTION_SDU1, col574) ,ISNULL(@SDU_DIRECTION_SDU2, col575) ,ISNULL(@SENSOR_ERROR_STATUS1_TACHO1, col576) 
,ISNULL(@SENSOR_ERROR_STATUS2_TACHO2, col577) ,ISNULL(@SENSOR_ERROR_STATUS3_RADAR1, col578) ,ISNULL(@SENSOR_ERROR_STATUS4_RADAR2, col579) ,ISNULL(@SENSOR_SPEED_HISTORIC_RADAR1, col580) ,ISNULL(@SENSOR_SPEED_HISTORIC_RADAR2, col581) 
,ISNULL(@SENSOR_SPEED_HISTORIC_TACHO1, col582) ,ISNULL(@SENSOR_SPEED_HISTORIC_TACHO2, col583) ,ISNULL(@SENSOR_SPEED_RADAR1, col584) ,ISNULL(@SENSOR_SPEED_RADAR2, col585) ,ISNULL(@SENSOR_SPEED_TACHO1, col586) 
,ISNULL(@SENSOR_SPEED_TACHO2, col587) ,ISNULL(@SLIP_SLIDE_ACC_AXLE1, col588) ,ISNULL(@SLIP_SLIDE_ACC_AXLE2, col589) ,ISNULL(@SLIP_SLIDE_AXLE1, col590) ,ISNULL(@SLIP_SLIDE_AXLE2, col591) 
,ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE1, col592) ,ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE2, col593) ,ISNULL(@SLIP_SLIDE_STATUS, col594) ,ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE1, col595) ,ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE2, col596) 
,ISNULL(@SOFTWARE_VERSION, col597) ,ISNULL(@SPL_FUNC_ID, col598) ,ISNULL(@SPL_RETURN, col599) ,ISNULL(@STM_STATE, col600) ,ISNULL(@STM_SUB_STATE, col601) 
,ISNULL(@SUPERVISION_STATE, col602) ,ISNULL(@TC_CCorrectOrient_M, col603) ,ISNULL(@TC_IConsistOrient, col604) ,ISNULL(@TC_IGwOrient, col605) ,ISNULL(@TC_INumConsists, col606) 
,ISNULL(@TC_INumConsists_1_1, col607) ,ISNULL(@TC_INumConsists_1_2, col608) ,ISNULL(@TC_INumConsists_2_1, col609) ,ISNULL(@TC_INumConsists_2_2, col610) ,ISNULL(@TC_INumConsists_3_1, col611) 
,ISNULL(@TC_INumConsists_3_2, col612) ,ISNULL(@TC_INumConsists_4_1, col613) ,ISNULL(@TC_INumConsists_4_10, col614) ,ISNULL(@TC_INumConsists_4_11, col615) ,ISNULL(@TC_INumConsists_4_12, col616) 
,ISNULL(@TC_INumConsists_4_13, col617) ,ISNULL(@TC_INumConsists_4_14, col618) ,ISNULL(@TC_INumConsists_4_15, col619) ,ISNULL(@TC_INumConsists_4_16, col620) ,ISNULL(@TC_INumConsists_4_17, col621) 
,ISNULL(@TC_INumConsists_4_18, col622) ,ISNULL(@TC_INumConsists_4_19, col623) ,ISNULL(@TC_INumConsists_4_2, col624) ,ISNULL(@TC_INumConsists_4_20, col625) ,ISNULL(@TC_INumConsists_4_3, col626) 
,ISNULL(@TC_INumConsists_4_4, col627) ,ISNULL(@TC_INumConsists_4_5, col628) ,ISNULL(@TC_INumConsists_4_6, col629) ,ISNULL(@TC_INumConsists_4_7, col630) ,ISNULL(@TC_INumConsists_4_8, col631) 
,ISNULL(@TC_INumConsists_4_9, col632) ,ISNULL(@TC_INumConsists_5_1, col633) ,ISNULL(@TC_INumConsists_5_2, col634) ,ISNULL(@TC_INumConsists_8_1, col635) ,ISNULL(@TC_INumConsists_8_2, col636) 
,ISNULL(@TC_INumConsists_8_3, col637) ,ISNULL(@TC_INumConsists_8_4, col638) ,ISNULL(@TC_INumConsists_8_5, col639) ,ISNULL(@TC_INumConsists_8_6, col640) ,ISNULL(@TC_INumConsists_8_7, col641) 
,ISNULL(@TC_INumConsists_8_8, col642) ,ISNULL(@TC_INumConsists_9, col643) ,ISNULL(@TC_ITclOrient, col644) ,ISNULL(@TCO_STATUS, col645) ,ISNULL(@TCU1_IEdSliding_3_1, col646) 
,ISNULL(@TCU1_IEdSliding_5_1, col647) ,ISNULL(@TCU1_ILineVoltage_3_1, col648) ,ISNULL(@TCU1_ILineVoltage_5_1, col649) ,ISNULL(@TCU1_ILineVoltage_5_2, col650) ,ISNULL(@TCU1_IPowerLimit_3_1, col651) 
,ISNULL(@TCU1_ITracBrActualInt_3_1, col652) ,ISNULL(@TCU2_IEdSliding_3_2, col653) ,ISNULL(@TCU2_IEdSliding_5_2, col654) ,ISNULL(@TCU2_ILineVoltage_3_2, col655) ,ISNULL(@TCU2_ILineVoltage_5_1, col656) 
,ISNULL(@TCU2_ILineVoltage_5_2, col657) ,ISNULL(@TCU2_IPowerLimit_3_2, col658) ,ISNULL(@TCU2_ITracBrActualInt_3_2, col659) ,ISNULL(@TRACK_SIGNAL, col660) ,ISNULL(@DBC_PRTcu1GrOut, col661) 
,ISNULL(@DBC_PRTcu2GrOut, col662) ,ISNULL(@MPW_PRHscbEnable, col663) ,ISNULL(@MPW_IPnt1UpDrvCount, col664) ,ISNULL(@MPW_IPnt2UpDrvCount, col665) ,ISNULL(@MPW_IPnt1UpCount, col666) 
,ISNULL(@MPW_IPnt2UpCount, col667) ,ISNULL(@MPW_IPnt1KmCount, col668) ,ISNULL(@MPW_IPnt2KmCount, col669) ,ISNULL(@DBC_IKmCounter, col670) ,ISNULL(@AUXY_IAuxAirCOnCnt, col671) 
,ISNULL(@MPW_IOnHscb, col672) ,ISNULL(@AUXY_IAirCSwOnCount, col673) ,ISNULL(@AUXY_IAirCOnCount, col674) ,ISNULL(@AUXY_IAuxAirCSwOnCnt, col675) ,ISNULL(@MPW_PRPantoEnable, col676)
,ISNULL(@DIA_Reserve1_AUX1, col677) ,ISNULL(@DIA_Reserve1_AUX2, col678) ,ISNULL(@DIA_Reserve1_BCHA_1, col679) ,ISNULL(@DIA_Reserve1_BCHA_2, col680) ,ISNULL(@DIA_Reserve1_BCU_1, col681)
,ISNULL(@DIA_Reserve1_BCU_2, col682) ,ISNULL(@DIA_Reserve1_CCUO, col683) ,ISNULL(@DIA_Reserve1_DCU_1, col684) ,ISNULL(@DIA_Reserve1_DCU_10, col685) ,ISNULL(@DIA_Reserve1_DCU_11, col686)
,ISNULL(@DIA_Reserve1_DCU_12, col687) ,ISNULL(@DIA_Reserve1_DCU_13, col688) ,ISNULL(@DIA_Reserve1_DCU_14, col689) ,ISNULL(@DIA_Reserve1_DCU_15, col690) ,ISNULL(@DIA_Reserve1_DCU_16, col691)
,ISNULL(@DIA_Reserve1_DCU_17, col692) ,ISNULL(@DIA_Reserve1_DCU_18, col693) ,ISNULL(@DIA_Reserve1_DCU_19, col694) ,ISNULL(@DIA_Reserve1_DCU_2, col695) ,ISNULL(@DIA_Reserve1_DCU_20, col696)
,ISNULL(@DIA_Reserve1_DCU_3, col697) ,ISNULL(@DIA_Reserve1_DCU_4, col698) ,ISNULL(@DIA_Reserve1_DCU_5, col699) ,ISNULL(@DIA_Reserve1_DCU_6, col700) ,ISNULL(@DIA_Reserve1_DCU_7, col701)
,ISNULL(@DIA_Reserve1_DCU_8, col702) ,ISNULL(@DIA_Reserve1_DCU_9, col703) ,ISNULL(@DIA_Reserve1_HVAC_1, col704) ,ISNULL(@DIA_Reserve1_HVAC_2, col705) ,ISNULL(@DIA_Reserve1_HVAC_3, col706)
,ISNULL(@DIA_Reserve1_HVAC_4, col707) ,ISNULL(@DIA_Reserve1_HVAC_5, col708) ,ISNULL(@DIA_Reserve1_HVAC_6, col709) ,ISNULL(@DIA_Reserve1_HVCC_1, col710) ,ISNULL(@DIA_Reserve1_HVCC_2, col711)
,ISNULL(@DIA_Reserve1_PIS, col712) ,ISNULL(@DIA_Reserve1_TCU_1, col713) ,ISNULL(@DIA_Reserve1_TCU_2, col714) ,ISNULL(@Reserve, col715)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

	SELECT @CVID = ID FROM @IDs

	INSERT INTO dbo.ChannelValueDoor (ID,TimeStamp,UnitID,UpdateRecord
	 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
	)
	SELECT TOP 1
	@CVID
	, @Timestamp
	, @UnitID		
	, @UpdateRecord
	,ISNULL(@DCU01_CDoorCloseConduc, col3000) ,ISNULL(@DCU01_IDoorClosedSafe, col3001) ,ISNULL(@DCU01_IDoorClRelease, col3002) ,ISNULL(@DCU01_IDoorPbClose, col3003) ,ISNULL(@DCU01_IDoorPbOpenIn, col3004) 
,ISNULL(@DCU01_IDoorPbOpenOut, col3005) ,ISNULL(@DCU01_IDoorReleased, col3006) ,ISNULL(@DCU01_IFootStepRel, col3007) ,ISNULL(@DCU01_IForcedClosDoor, col3008) ,ISNULL(@DCU01_ILeafsStopDoor, col3009) 
,ISNULL(@DCU01_IObstacleDoor, col3010) ,ISNULL(@DCU01_IObstacleStep, col3011) ,ISNULL(@DCU01_IOpenAssistDoor, col3012) ,ISNULL(@DCU01_IStandstillBack, col3013) ,ISNULL(@DCU02_CDoorCloseConduc, col3014) 
,ISNULL(@DCU02_IDoorClosedSafe, col3015) ,ISNULL(@DCU02_IDoorClRelease, col3016) ,ISNULL(@DCU02_IDoorPbClose, col3017) ,ISNULL(@DCU02_IDoorPbOpenIn, col3018) ,ISNULL(@DCU02_IDoorPbOpenOut, col3019) 
,ISNULL(@DCU02_IDoorReleased, col3020) ,ISNULL(@DCU02_IFootStepRel, col3021) ,ISNULL(@DCU02_IForcedClosDoor, col3022) ,ISNULL(@DCU02_ILeafsStopDoor, col3023) ,ISNULL(@DCU02_IObstacleDoor, col3024) 
,ISNULL(@DCU02_IObstacleStep, col3025) ,ISNULL(@DCU02_IOpenAssistDoor, col3026) ,ISNULL(@DCU02_IStandstillBack, col3027) ,ISNULL(@DCU03_CDoorCloseConduc, col3028) ,ISNULL(@DCU03_IDoorClosedSafe, col3029) 
,ISNULL(@DCU03_IDoorClRelease, col3030) ,ISNULL(@DCU03_IDoorPbClose, col3031) ,ISNULL(@DCU03_IDoorPbOpenIn, col3032) ,ISNULL(@DCU03_IDoorPbOpenOut, col3033) ,ISNULL(@DCU03_IDoorReleased, col3034) 
,ISNULL(@DCU03_IFootStepRel, col3035) ,ISNULL(@DCU03_IForcedClosDoor, col3036) ,ISNULL(@DCU03_ILeafsStopDoor, col3037) ,ISNULL(@DCU03_IObstacleDoor, col3038) ,ISNULL(@DCU03_IObstacleStep, col3039) 
,ISNULL(@DCU03_IOpenAssistDoor, col3040) ,ISNULL(@DCU03_IStandstillBack, col3041) ,ISNULL(@DCU04_CDoorCloseConduc, col3042) ,ISNULL(@DCU04_IDoorClosedSafe, col3043) ,ISNULL(@DCU04_IDoorClRelease, col3044) 
,ISNULL(@DCU04_IDoorPbClose, col3045) ,ISNULL(@DCU04_IDoorPbOpenIn, col3046) ,ISNULL(@DCU04_IDoorPbOpenOut, col3047) ,ISNULL(@DCU04_IDoorReleased, col3048) ,ISNULL(@DCU04_IFootStepRel, col3049) 
,ISNULL(@DCU04_IForcedClosDoor, col3050) ,ISNULL(@DCU04_ILeafsStopDoor, col3051) ,ISNULL(@DCU04_IObstacleDoor, col3052) ,ISNULL(@DCU04_IObstacleStep, col3053) ,ISNULL(@DCU04_IOpenAssistDoor, col3054) 
,ISNULL(@DCU04_IStandstillBack, col3055) ,ISNULL(@DCU05_CDoorCloseConduc, col3056) ,ISNULL(@DCU05_IDoorClosedSafe, col3057) ,ISNULL(@DCU05_IDoorClRelease, col3058) ,ISNULL(@DCU05_IDoorPbClose, col3059) 
,ISNULL(@DCU05_IDoorPbOpenIn, col3060) ,ISNULL(@DCU05_IDoorPbOpenOut, col3061) ,ISNULL(@DCU05_IDoorReleased, col3062) ,ISNULL(@DCU05_IFootStepRel, col3063) ,ISNULL(@DCU05_IForcedClosDoor, col3064) 
,ISNULL(@DCU05_ILeafsStopDoor, col3065) ,ISNULL(@DCU05_IObstacleDoor, col3066) ,ISNULL(@DCU05_IObstacleStep, col3067) ,ISNULL(@DCU05_IOpenAssistDoor, col3068) ,ISNULL(@DCU05_IStandstillBack, col3069) 
,ISNULL(@DCU06_CDoorCloseConduc, col3070) ,ISNULL(@DCU06_IDoorClosedSafe, col3071) ,ISNULL(@DCU06_IDoorClRelease, col3072) ,ISNULL(@DCU06_IDoorPbClose, col3073) ,ISNULL(@DCU06_IDoorPbOpenIn, col3074) 
,ISNULL(@DCU06_IDoorPbOpenOut, col3075) ,ISNULL(@DCU06_IDoorReleased, col3076) ,ISNULL(@DCU06_IFootStepRel, col3077) ,ISNULL(@DCU06_IForcedClosDoor, col3078) ,ISNULL(@DCU06_ILeafsStopDoor, col3079) 
,ISNULL(@DCU06_IObstacleDoor, col3080) ,ISNULL(@DCU06_IObstacleStep, col3081) ,ISNULL(@DCU06_IOpenAssistDoor, col3082) ,ISNULL(@DCU06_IStandstillBack, col3083) ,ISNULL(@DCU07_CDoorCloseConduc, col3084) 
,ISNULL(@DCU07_IDoorClosedSafe, col3085) ,ISNULL(@DCU07_IDoorClRelease, col3086) ,ISNULL(@DCU07_IDoorPbClose, col3087) ,ISNULL(@DCU07_IDoorPbOpenIn, col3088) ,ISNULL(@DCU07_IDoorPbOpenOut, col3089) 
,ISNULL(@DCU07_IDoorReleased, col3090) ,ISNULL(@DCU07_IFootStepRel, col3091) ,ISNULL(@DCU07_IForcedClosDoor, col3092) ,ISNULL(@DCU07_ILeafsStopDoor, col3093) ,ISNULL(@DCU07_IObstacleDoor, col3094) 
,ISNULL(@DCU07_IObstacleStep, col3095) ,ISNULL(@DCU07_IOpenAssistDoor, col3096) ,ISNULL(@DCU07_IStandstillBack, col3097) ,ISNULL(@DCU08_CDoorCloseConduc, col3098) ,ISNULL(@DCU08_IDoorClosedSafe, col3099) 
,ISNULL(@DCU08_IDoorClRelease, col3100) ,ISNULL(@DCU08_IDoorPbClose, col3101) ,ISNULL(@DCU08_IDoorPbOpenIn, col3102) ,ISNULL(@DCU08_IDoorPbOpenOut, col3103) ,ISNULL(@DCU08_IDoorReleased, col3104) 
,ISNULL(@DCU08_IFootStepRel, col3105) ,ISNULL(@DCU08_IForcedClosDoor, col3106) ,ISNULL(@DCU08_ILeafsStopDoor, col3107) ,ISNULL(@DCU08_IObstacleDoor, col3108) ,ISNULL(@DCU08_IObstacleStep, col3109) 
,ISNULL(@DCU08_IOpenAssistDoor, col3110) ,ISNULL(@DCU08_IStandstillBack, col3111) ,ISNULL(@DCU09_CDoorCloseConduc, col3112) ,ISNULL(@DCU09_IDoorClosedSafe, col3113) ,ISNULL(@DCU09_IDoorClRelease, col3114) 
,ISNULL(@DCU09_IDoorPbClose, col3115) ,ISNULL(@DCU09_IDoorPbOpenIn, col3116) ,ISNULL(@DCU09_IDoorPbOpenOut, col3117) ,ISNULL(@DCU09_IDoorReleased, col3118) ,ISNULL(@DCU09_IFootStepRel, col3119) 
,ISNULL(@DCU09_IForcedClosDoor, col3120) ,ISNULL(@DCU09_ILeafsStopDoor, col3121) ,ISNULL(@DCU09_IObstacleDoor, col3122) ,ISNULL(@DCU09_IObstacleStep, col3123) ,ISNULL(@DCU09_IOpenAssistDoor, col3124) 
,ISNULL(@DCU09_IStandstillBack, col3125) ,ISNULL(@DCU10_CDoorCloseConduc, col3126) ,ISNULL(@DCU10_IDoorClosedSafe, col3127) ,ISNULL(@DCU10_IDoorClRelease, col3128) ,ISNULL(@DCU10_IDoorPbClose, col3129) 
,ISNULL(@DCU10_IDoorPbOpenIn, col3130) ,ISNULL(@DCU10_IDoorPbOpenOut, col3131) ,ISNULL(@DCU10_IDoorReleased, col3132) ,ISNULL(@DCU10_IFootStepRel, col3133) ,ISNULL(@DCU10_IForcedClosDoor, col3134) 
,ISNULL(@DCU10_ILeafsStopDoor, col3135) ,ISNULL(@DCU10_IObstacleDoor, col3136) ,ISNULL(@DCU10_IObstacleStep, col3137) ,ISNULL(@DCU10_IOpenAssistDoor, col3138) ,ISNULL(@DCU10_IStandstillBack, col3139) 
,ISNULL(@DCU11_CDoorCloseConduc, col3140) ,ISNULL(@DCU11_IDoorClosedSafe, col3141) ,ISNULL(@DCU11_IDoorClRelease, col3142) ,ISNULL(@DCU11_IDoorPbClose, col3143) ,ISNULL(@DCU11_IDoorPbOpenIn, col3144) 
,ISNULL(@DCU11_IDoorPbOpenOut, col3145) ,ISNULL(@DCU11_IDoorReleased, col3146) ,ISNULL(@DCU11_IFootStepRel, col3147) ,ISNULL(@DCU11_IForcedClosDoor, col3148) ,ISNULL(@DCU11_ILeafsStopDoor, col3149) 
,ISNULL(@DCU11_IObstacleDoor, col3150) ,ISNULL(@DCU11_IObstacleStep, col3151) ,ISNULL(@DCU11_IOpenAssistDoor, col3152) ,ISNULL(@DCU11_IStandstillBack, col3153) ,ISNULL(@DCU12_CDoorCloseConduc, col3154) 
,ISNULL(@DCU12_IDoorClosedSafe, col3155) ,ISNULL(@DCU12_IDoorClRelease, col3156) ,ISNULL(@DCU12_IDoorPbClose, col3157) ,ISNULL(@DCU12_IDoorPbOpenIn, col3158) ,ISNULL(@DCU12_IDoorPbOpenOut, col3159) 
,ISNULL(@DCU12_IDoorReleased, col3160) ,ISNULL(@DCU12_IFootStepRel, col3161) ,ISNULL(@DCU12_IForcedClosDoor, col3162) ,ISNULL(@DCU12_ILeafsStopDoor, col3163) ,ISNULL(@DCU12_IObstacleDoor, col3164) 
,ISNULL(@DCU12_IObstacleStep, col3165) ,ISNULL(@DCU12_IOpenAssistDoor, col3166) ,ISNULL(@DCU12_IStandstillBack, col3167) ,ISNULL(@DCU13_CDoorCloseConduc, col3168) ,ISNULL(@DCU13_IDoorClosedSafe, col3169) 
,ISNULL(@DCU13_IDoorClRelease, col3170) ,ISNULL(@DCU13_IDoorPbClose, col3171) ,ISNULL(@DCU13_IDoorPbOpenIn, col3172) ,ISNULL(@DCU13_IDoorPbOpenOut, col3173) ,ISNULL(@DCU13_IDoorReleased, col3174) 
,ISNULL(@DCU13_IFootStepRel, col3175) ,ISNULL(@DCU13_IForcedClosDoor, col3176) ,ISNULL(@DCU13_ILeafsStopDoor, col3177) ,ISNULL(@DCU13_IObstacleDoor, col3178) ,ISNULL(@DCU13_IObstacleStep, col3179) 
,ISNULL(@DCU13_IOpenAssistDoor, col3180) ,ISNULL(@DCU13_IStandstillBack, col3181) ,ISNULL(@DCU14_CDoorCloseConduc, col3182) ,ISNULL(@DCU14_IDoorClosedSafe, col3183) ,ISNULL(@DCU14_IDoorClRelease, col3184) 
,ISNULL(@DCU14_IDoorPbClose, col3185) ,ISNULL(@DCU14_IDoorPbOpenIn, col3186) ,ISNULL(@DCU14_IDoorPbOpenOut, col3187) ,ISNULL(@DCU14_IDoorReleased, col3188) ,ISNULL(@DCU14_IFootStepRel, col3189) 
,ISNULL(@DCU14_IForcedClosDoor, col3190) ,ISNULL(@DCU14_ILeafsStopDoor, col3191) ,ISNULL(@DCU14_IObstacleDoor, col3192) ,ISNULL(@DCU14_IObstacleStep, col3193) ,ISNULL(@DCU14_IOpenAssistDoor, col3194) 
,ISNULL(@DCU14_IStandstillBack, col3195) ,ISNULL(@DCU15_CDoorCloseConduc, col3196) ,ISNULL(@DCU15_IDoorClosedSafe, col3197) ,ISNULL(@DCU15_IDoorClRelease, col3198) ,ISNULL(@DCU15_IDoorPbClose, col3199) 
,ISNULL(@DCU15_IDoorPbOpenIn, col3200) ,ISNULL(@DCU15_IDoorPbOpenOut, col3201) ,ISNULL(@DCU15_IDoorReleased, col3202) ,ISNULL(@DCU15_IFootStepRel, col3203) ,ISNULL(@DCU15_IForcedClosDoor, col3204) 
,ISNULL(@DCU15_ILeafsStopDoor, col3205) ,ISNULL(@DCU15_IObstacleDoor, col3206) ,ISNULL(@DCU15_IObstacleStep, col3207) ,ISNULL(@DCU15_IOpenAssistDoor, col3208) ,ISNULL(@DCU15_IStandstillBack, col3209) 
,ISNULL(@DCU16_CDoorCloseConduc, col3210) ,ISNULL(@DCU16_IDoorClosedSafe, col3211) ,ISNULL(@DCU16_IDoorClRelease, col3212) ,ISNULL(@DCU16_IDoorPbClose, col3213) ,ISNULL(@DCU16_IDoorPbOpenIn, col3214) 
,ISNULL(@DCU16_IDoorPbOpenOut, col3215) ,ISNULL(@DCU16_IDoorReleased, col3216) ,ISNULL(@DCU16_IFootStepRel, col3217) ,ISNULL(@DCU16_IForcedClosDoor, col3218) ,ISNULL(@DCU16_ILeafsStopDoor, col3219) 
,ISNULL(@DCU16_IObstacleDoor, col3220) ,ISNULL(@DCU16_IObstacleStep, col3221) ,ISNULL(@DCU16_IOpenAssistDoor, col3222) ,ISNULL(@DCU16_IStandstillBack, col3223) ,ISNULL(@DCU17_CDoorCloseConduc, col3224) 
,ISNULL(@DCU17_IDoorClosedSafe, col3225) ,ISNULL(@DCU17_IDoorClRelease, col3226) ,ISNULL(@DCU17_IDoorPbClose, col3227) ,ISNULL(@DCU17_IDoorPbOpenIn, col3228) ,ISNULL(@DCU17_IDoorPbOpenOut, col3229) 
,ISNULL(@DCU17_IDoorReleased, col3230) ,ISNULL(@DCU17_IFootStepRel, col3231) ,ISNULL(@DCU17_IForcedClosDoor, col3232) ,ISNULL(@DCU17_ILeafsStopDoor, col3233) ,ISNULL(@DCU17_IObstacleDoor, col3234) 
,ISNULL(@DCU17_IObstacleStep, col3235) ,ISNULL(@DCU17_IOpenAssistDoor, col3236) ,ISNULL(@DCU17_IStandstillBack, col3237) ,ISNULL(@DCU18_CDoorCloseConduc, col3238) ,ISNULL(@DCU18_IDoorClosedSafe, col3239) 
,ISNULL(@DCU18_IDoorClRelease, col3240) ,ISNULL(@DCU18_IDoorPbClose, col3241) ,ISNULL(@DCU18_IDoorPbOpenIn, col3242) ,ISNULL(@DCU18_IDoorPbOpenOut, col3243) ,ISNULL(@DCU18_IDoorReleased, col3244) 
,ISNULL(@DCU18_IFootStepRel, col3245) ,ISNULL(@DCU18_IForcedClosDoor, col3246) ,ISNULL(@DCU18_ILeafsStopDoor, col3247) ,ISNULL(@DCU18_IObstacleDoor, col3248) ,ISNULL(@DCU18_IObstacleStep, col3249) 
,ISNULL(@DCU18_IOpenAssistDoor, col3250) ,ISNULL(@DCU18_IStandstillBack, col3251) ,ISNULL(@DCU19_CDoorCloseConduc, col3252) ,ISNULL(@DCU19_IDoorClosedSafe, col3253) ,ISNULL(@DCU19_IDoorClRelease, col3254) 
,ISNULL(@DCU19_IDoorPbClose, col3255) ,ISNULL(@DCU19_IDoorPbOpenIn, col3256) ,ISNULL(@DCU19_IDoorPbOpenOut, col3257) ,ISNULL(@DCU19_IDoorReleased, col3258) ,ISNULL(@DCU19_IFootStepRel, col3259) 
,ISNULL(@DCU19_IForcedClosDoor, col3260) ,ISNULL(@DCU19_ILeafsStopDoor, col3261) ,ISNULL(@DCU19_IObstacleDoor, col3262) ,ISNULL(@DCU19_IObstacleStep, col3263) ,ISNULL(@DCU19_IOpenAssistDoor, col3264) 
,ISNULL(@DCU19_IStandstillBack, col3265) ,ISNULL(@DCU20_CDoorCloseConduc, col3266) ,ISNULL(@DCU20_IDoorClosedSafe, col3267) ,ISNULL(@DCU20_IDoorClRelease, col3268) ,ISNULL(@DCU20_IDoorPbClose, col3269) 
,ISNULL(@DCU20_IDoorPbOpenIn, col3270) ,ISNULL(@DCU20_IDoorPbOpenOut, col3271) ,ISNULL(@DCU20_IDoorReleased, col3272) ,ISNULL(@DCU20_IFootStepRel, col3273) ,ISNULL(@DCU20_IForcedClosDoor, col3274) 
,ISNULL(@DCU20_ILeafsStopDoor, col3275) ,ISNULL(@DCU20_IObstacleDoor, col3276) ,ISNULL(@DCU20_IObstacleStep, col3277) ,ISNULL(@DCU20_IOpenAssistDoor, col3278) ,ISNULL(@DCU20_IStandstillBack, col3279) 
,ISNULL(@DRS_CDisDoorRelease_4_1, col3280) ,ISNULL(@DRS_CDisDoorRelease_4_10, col3281) ,ISNULL(@DRS_CDisDoorRelease_4_11, col3282) ,ISNULL(@DRS_CDisDoorRelease_4_12, col3283) ,ISNULL(@DRS_CDisDoorRelease_4_13, col3284) 
,ISNULL(@DRS_CDisDoorRelease_4_14, col3285) ,ISNULL(@DRS_CDisDoorRelease_4_15, col3286) ,ISNULL(@DRS_CDisDoorRelease_4_16, col3287) ,ISNULL(@DRS_CDisDoorRelease_4_17, col3288) ,ISNULL(@DRS_CDisDoorRelease_4_18, col3289) 
,ISNULL(@DRS_CDisDoorRelease_4_19, col3290) ,ISNULL(@DRS_CDisDoorRelease_4_2, col3291) ,ISNULL(@DRS_CDisDoorRelease_4_20, col3292) ,ISNULL(@DRS_CDisDoorRelease_4_3, col3293) ,ISNULL(@DRS_CDisDoorRelease_4_4, col3294) 
,ISNULL(@DRS_CDisDoorRelease_4_5, col3295) ,ISNULL(@DRS_CDisDoorRelease_4_6, col3296) ,ISNULL(@DRS_CDisDoorRelease_4_7, col3297) ,ISNULL(@DRS_CDisDoorRelease_4_8, col3298) ,ISNULL(@DRS_CDisDoorRelease_4_9, col3299) 
,ISNULL(@DRS_CDoorClosAutDis_4_1, col3300) ,ISNULL(@DRS_CDoorClosAutDis_4_10, col3301) ,ISNULL(@DRS_CDoorClosAutDis_4_11, col3302) ,ISNULL(@DRS_CDoorClosAutDis_4_12, col3303) ,ISNULL(@DRS_CDoorClosAutDis_4_13, col3304) 
,ISNULL(@DRS_CDoorClosAutDis_4_14, col3305) ,ISNULL(@DRS_CDoorClosAutDis_4_15, col3306) ,ISNULL(@DRS_CDoorClosAutDis_4_16, col3307) ,ISNULL(@DRS_CDoorClosAutDis_4_17, col3308) ,ISNULL(@DRS_CDoorClosAutDis_4_18, col3309) 
,ISNULL(@DRS_CDoorClosAutDis_4_19, col3310) ,ISNULL(@DRS_CDoorClosAutDis_4_2, col3311) ,ISNULL(@DRS_CDoorClosAutDis_4_20, col3312) ,ISNULL(@DRS_CDoorClosAutDis_4_3, col3313) ,ISNULL(@DRS_CDoorClosAutDis_4_4, col3314) 
,ISNULL(@DRS_CDoorClosAutDis_4_5, col3315) ,ISNULL(@DRS_CDoorClosAutDis_4_6, col3316) ,ISNULL(@DRS_CDoorClosAutDis_4_7, col3317) ,ISNULL(@DRS_CDoorClosAutDis_4_8, col3318) ,ISNULL(@DRS_CDoorClosAutDis_4_9, col3319) 
,ISNULL(@DRS_CForcedClosDoor_4_1, col3320) ,ISNULL(@DRS_CForcedClosDoor_4_10, col3321) ,ISNULL(@DRS_CForcedClosDoor_4_11, col3322) ,ISNULL(@DRS_CForcedClosDoor_4_12, col3323) ,ISNULL(@DRS_CForcedClosDoor_4_13, col3324) 
,ISNULL(@DRS_CForcedClosDoor_4_14, col3325) ,ISNULL(@DRS_CForcedClosDoor_4_15, col3326) ,ISNULL(@DRS_CForcedClosDoor_4_16, col3327) ,ISNULL(@DRS_CForcedClosDoor_4_17, col3328) ,ISNULL(@DRS_CForcedClosDoor_4_18, col3329) 
,ISNULL(@DRS_CForcedClosDoor_4_19, col3330) ,ISNULL(@DRS_CForcedClosDoor_4_2, col3331) ,ISNULL(@DRS_CForcedClosDoor_4_20, col3332) ,ISNULL(@DRS_CForcedClosDoor_4_3, col3333) ,ISNULL(@DRS_CForcedClosDoor_4_4, col3334) 
,ISNULL(@DRS_CForcedClosDoor_4_5, col3335) ,ISNULL(@DRS_CForcedClosDoor_4_6, col3336) ,ISNULL(@DRS_CForcedClosDoor_4_7, col3337) ,ISNULL(@DRS_CForcedClosDoor_4_8, col3338) ,ISNULL(@DRS_CForcedClosDoor_4_9, col3339) 
,ISNULL(@DRS_CTestModeReq_M_4_1, col3340) ,ISNULL(@DRS_CTestModeReq_M_4_10, col3341) ,ISNULL(@DRS_CTestModeReq_M_4_11, col3342) ,ISNULL(@DRS_CTestModeReq_M_4_12, col3343) ,ISNULL(@DRS_CTestModeReq_M_4_13, col3344) 
,ISNULL(@DRS_CTestModeReq_M_4_14, col3345) ,ISNULL(@DRS_CTestModeReq_M_4_15, col3346) ,ISNULL(@DRS_CTestModeReq_M_4_16, col3347) ,ISNULL(@DRS_CTestModeReq_M_4_17, col3348) ,ISNULL(@DRS_CTestModeReq_M_4_18, col3349) 
,ISNULL(@DRS_CTestModeReq_M_4_19, col3350) ,ISNULL(@DRS_CTestModeReq_M_4_2, col3351) ,ISNULL(@DRS_CTestModeReq_M_4_20, col3352) ,ISNULL(@DRS_CTestModeReq_M_4_3, col3353) ,ISNULL(@DRS_CTestModeReq_M_4_4, col3354) 
,ISNULL(@DRS_CTestModeReq_M_4_5, col3355) ,ISNULL(@DRS_CTestModeReq_M_4_6, col3356) ,ISNULL(@DRS_CTestModeReq_M_4_7, col3357) ,ISNULL(@DRS_CTestModeReq_M_4_8, col3358) ,ISNULL(@DRS_CTestModeReq_M_4_9, col3359) 
,ISNULL(@MIO_I1DoorRelLe_4_1, col3360) ,ISNULL(@MIO_I1DoorRelLe_4_10, col3361) ,ISNULL(@MIO_I1DoorRelLe_4_11, col3362) ,ISNULL(@MIO_I1DoorRelLe_4_12, col3363) ,ISNULL(@MIO_I1DoorRelLe_4_13, col3364) 
,ISNULL(@MIO_I1DoorRelLe_4_14, col3365) ,ISNULL(@MIO_I1DoorRelLe_4_15, col3366) ,ISNULL(@MIO_I1DoorRelLe_4_16, col3367) ,ISNULL(@MIO_I1DoorRelLe_4_17, col3368) ,ISNULL(@MIO_I1DoorRelLe_4_18, col3369) 
,ISNULL(@MIO_I1DoorRelLe_4_19, col3370) ,ISNULL(@MIO_I1DoorRelLe_4_2, col3371) ,ISNULL(@MIO_I1DoorRelLe_4_20, col3372) ,ISNULL(@MIO_I1DoorRelLe_4_3, col3373) ,ISNULL(@MIO_I1DoorRelLe_4_4, col3374) 
,ISNULL(@MIO_I1DoorRelLe_4_5, col3375) ,ISNULL(@MIO_I1DoorRelLe_4_6, col3376) ,ISNULL(@MIO_I1DoorRelLe_4_7, col3377) ,ISNULL(@MIO_I1DoorRelLe_4_8, col3378) ,ISNULL(@MIO_I1DoorRelLe_4_9, col3379) 
,ISNULL(@MIO_I1DoorRelRi_4_1, col3380) ,ISNULL(@MIO_I1DoorRelRi_4_10, col3381) ,ISNULL(@MIO_I1DoorRelRi_4_11, col3382) ,ISNULL(@MIO_I1DoorRelRi_4_12, col3383) ,ISNULL(@MIO_I1DoorRelRi_4_13, col3384) 
,ISNULL(@MIO_I1DoorRelRi_4_14, col3385) ,ISNULL(@MIO_I1DoorRelRi_4_15, col3386) ,ISNULL(@MIO_I1DoorRelRi_4_16, col3387) ,ISNULL(@MIO_I1DoorRelRi_4_17, col3388) ,ISNULL(@MIO_I1DoorRelRi_4_18, col3389) 
,ISNULL(@MIO_I1DoorRelRi_4_19, col3390) ,ISNULL(@MIO_I1DoorRelRi_4_2, col3391) ,ISNULL(@MIO_I1DoorRelRi_4_20, col3392) ,ISNULL(@MIO_I1DoorRelRi_4_3, col3393) ,ISNULL(@MIO_I1DoorRelRi_4_4, col3394) 
,ISNULL(@MIO_I1DoorRelRi_4_5, col3395) ,ISNULL(@MIO_I1DoorRelRi_4_6, col3396) ,ISNULL(@MIO_I1DoorRelRi_4_7, col3397) ,ISNULL(@MIO_I1DoorRelRi_4_8, col3398) ,ISNULL(@MIO_I1DoorRelRi_4_9, col3399) 
,ISNULL(@MIO_I2DoorRelLe_4_1, col3400) ,ISNULL(@MIO_I2DoorRelLe_4_10, col3401) ,ISNULL(@MIO_I2DoorRelLe_4_11, col3402) ,ISNULL(@MIO_I2DoorRelLe_4_12, col3403) ,ISNULL(@MIO_I2DoorRelLe_4_13, col3404) 
,ISNULL(@MIO_I2DoorRelLe_4_14, col3405) ,ISNULL(@MIO_I2DoorRelLe_4_15, col3406) ,ISNULL(@MIO_I2DoorRelLe_4_16, col3407) ,ISNULL(@MIO_I2DoorRelLe_4_17, col3408) ,ISNULL(@MIO_I2DoorRelLe_4_18, col3409) 
,ISNULL(@MIO_I2DoorRelLe_4_19, col3410) ,ISNULL(@MIO_I2DoorRelLe_4_2, col3411) ,ISNULL(@MIO_I2DoorRelLe_4_20, col3412) ,ISNULL(@MIO_I2DoorRelLe_4_3, col3413) ,ISNULL(@MIO_I2DoorRelLe_4_4, col3414) 
,ISNULL(@MIO_I2DoorRelLe_4_5, col3415) ,ISNULL(@MIO_I2DoorRelLe_4_6, col3416) ,ISNULL(@MIO_I2DoorRelLe_4_7, col3417) ,ISNULL(@MIO_I2DoorRelLe_4_8, col3418) ,ISNULL(@MIO_I2DoorRelLe_4_9, col3419) 
,ISNULL(@MIO_I2DoorRelRi_4_1, col3420) ,ISNULL(@MIO_I2DoorRelRi_4_10, col3421) ,ISNULL(@MIO_I2DoorRelRi_4_11, col3422) ,ISNULL(@MIO_I2DoorRelRi_4_12, col3423) ,ISNULL(@MIO_I2DoorRelRi_4_13, col3424) 
,ISNULL(@MIO_I2DoorRelRi_4_14, col3425) ,ISNULL(@MIO_I2DoorRelRi_4_15, col3426) ,ISNULL(@MIO_I2DoorRelRi_4_16, col3427) ,ISNULL(@MIO_I2DoorRelRi_4_17, col3428) ,ISNULL(@MIO_I2DoorRelRi_4_18, col3429) 
,ISNULL(@MIO_I2DoorRelRi_4_19, col3430) ,ISNULL(@MIO_I2DoorRelRi_4_2, col3431) ,ISNULL(@MIO_I2DoorRelRi_4_20, col3432) ,ISNULL(@MIO_I2DoorRelRi_4_3, col3433) ,ISNULL(@MIO_I2DoorRelRi_4_4, col3434) 
,ISNULL(@MIO_I2DoorRelRi_4_5, col3435) ,ISNULL(@MIO_I2DoorRelRi_4_6, col3436) ,ISNULL(@MIO_I2DoorRelRi_4_7, col3437) ,ISNULL(@MIO_I2DoorRelRi_4_8, col3438) ,ISNULL(@MIO_I2DoorRelRi_4_9, col3439) 
,ISNULL(@MIO_IDoorLoopBypa1_4_1, col3440) ,ISNULL(@MIO_IDoorLoopBypa1_4_10, col3441) ,ISNULL(@MIO_IDoorLoopBypa1_4_11, col3442) ,ISNULL(@MIO_IDoorLoopBypa1_4_12, col3443) ,ISNULL(@MIO_IDoorLoopBypa1_4_13, col3444) 
,ISNULL(@MIO_IDoorLoopBypa1_4_14, col3445) ,ISNULL(@MIO_IDoorLoopBypa1_4_15, col3446) ,ISNULL(@MIO_IDoorLoopBypa1_4_16, col3447) ,ISNULL(@MIO_IDoorLoopBypa1_4_17, col3448) ,ISNULL(@MIO_IDoorLoopBypa1_4_18, col3449) 
,ISNULL(@MIO_IDoorLoopBypa1_4_19, col3450) ,ISNULL(@MIO_IDoorLoopBypa1_4_2, col3451) ,ISNULL(@MIO_IDoorLoopBypa1_4_20, col3452) ,ISNULL(@MIO_IDoorLoopBypa1_4_3, col3453) ,ISNULL(@MIO_IDoorLoopBypa1_4_4, col3454) 
,ISNULL(@MIO_IDoorLoopBypa1_4_5, col3455) ,ISNULL(@MIO_IDoorLoopBypa1_4_6, col3456) ,ISNULL(@MIO_IDoorLoopBypa1_4_7, col3457) ,ISNULL(@MIO_IDoorLoopBypa1_4_8, col3458) ,ISNULL(@MIO_IDoorLoopBypa1_4_9, col3459) 
,ISNULL(@MIO_IDoorLoopBypa2_4_1, col3460) ,ISNULL(@MIO_IDoorLoopBypa2_4_10, col3461) ,ISNULL(@MIO_IDoorLoopBypa2_4_11, col3462) ,ISNULL(@MIO_IDoorLoopBypa2_4_12, col3463) ,ISNULL(@MIO_IDoorLoopBypa2_4_13, col3464) 
,ISNULL(@MIO_IDoorLoopBypa2_4_14, col3465) ,ISNULL(@MIO_IDoorLoopBypa2_4_15, col3466) ,ISNULL(@MIO_IDoorLoopBypa2_4_16, col3467) ,ISNULL(@MIO_IDoorLoopBypa2_4_17, col3468) ,ISNULL(@MIO_IDoorLoopBypa2_4_18, col3469) 
,ISNULL(@MIO_IDoorLoopBypa2_4_19, col3470) ,ISNULL(@MIO_IDoorLoopBypa2_4_2, col3471) ,ISNULL(@MIO_IDoorLoopBypa2_4_20, col3472) ,ISNULL(@MIO_IDoorLoopBypa2_4_3, col3473) ,ISNULL(@MIO_IDoorLoopBypa2_4_4, col3474) 
,ISNULL(@MIO_IDoorLoopBypa2_4_5, col3475) ,ISNULL(@MIO_IDoorLoopBypa2_4_6, col3476) ,ISNULL(@MIO_IDoorLoopBypa2_4_7, col3477) ,ISNULL(@MIO_IDoorLoopBypa2_4_8, col3478) ,ISNULL(@MIO_IDoorLoopBypa2_4_9, col3479) 
,ISNULL(@MIO_IDoorLoopCl1_4_1, col3480) ,ISNULL(@MIO_IDoorLoopCl1_4_10, col3481) ,ISNULL(@MIO_IDoorLoopCl1_4_11, col3482) ,ISNULL(@MIO_IDoorLoopCl1_4_12, col3483) ,ISNULL(@MIO_IDoorLoopCl1_4_13, col3484) 
,ISNULL(@MIO_IDoorLoopCl1_4_14, col3485) ,ISNULL(@MIO_IDoorLoopCl1_4_15, col3486) ,ISNULL(@MIO_IDoorLoopCl1_4_16, col3487) ,ISNULL(@MIO_IDoorLoopCl1_4_17, col3488) ,ISNULL(@MIO_IDoorLoopCl1_4_18, col3489) 
,ISNULL(@MIO_IDoorLoopCl1_4_19, col3490) ,ISNULL(@MIO_IDoorLoopCl1_4_2, col3491) ,ISNULL(@MIO_IDoorLoopCl1_4_20, col3492) ,ISNULL(@MIO_IDoorLoopCl1_4_3, col3493) ,ISNULL(@MIO_IDoorLoopCl1_4_4, col3494) 
,ISNULL(@MIO_IDoorLoopCl1_4_5, col3495) ,ISNULL(@MIO_IDoorLoopCl1_4_6, col3496) ,ISNULL(@MIO_IDoorLoopCl1_4_7, col3497) ,ISNULL(@MIO_IDoorLoopCl1_4_8, col3498) ,ISNULL(@MIO_IDoorLoopCl1_4_9, col3499) 
,ISNULL(@MIO_IDoorLoopCl2_4_1, col3500) ,ISNULL(@MIO_IDoorLoopCl2_4_10, col3501) ,ISNULL(@MIO_IDoorLoopCl2_4_11, col3502) ,ISNULL(@MIO_IDoorLoopCl2_4_12, col3503) ,ISNULL(@MIO_IDoorLoopCl2_4_13, col3504) 
,ISNULL(@MIO_IDoorLoopCl2_4_14, col3505) ,ISNULL(@MIO_IDoorLoopCl2_4_15, col3506) ,ISNULL(@MIO_IDoorLoopCl2_4_16, col3507) ,ISNULL(@MIO_IDoorLoopCl2_4_17, col3508) ,ISNULL(@MIO_IDoorLoopCl2_4_18, col3509) 
,ISNULL(@MIO_IDoorLoopCl2_4_19, col3510) ,ISNULL(@MIO_IDoorLoopCl2_4_2, col3511) ,ISNULL(@MIO_IDoorLoopCl2_4_20, col3512) ,ISNULL(@MIO_IDoorLoopCl2_4_3, col3513) ,ISNULL(@MIO_IDoorLoopCl2_4_4, col3514) 
,ISNULL(@MIO_IDoorLoopCl2_4_5, col3515) ,ISNULL(@MIO_IDoorLoopCl2_4_6, col3516) ,ISNULL(@MIO_IDoorLoopCl2_4_7, col3517) ,ISNULL(@MIO_IDoorLoopCl2_4_8, col3518) ,ISNULL(@MIO_IDoorLoopCl2_4_9, col3519) 
	FROM dbo.ChannelValueDoor WITH (NOLOCK) 
	WHERE UnitID = @UnitID
	ORDER BY 1 DESC

END
ELSE
BEGIN


UPDATE ChannelValue SET UpdateRecord = 1
,col1=ISNULL(@MCG_ILatitude, col1) ,col2=ISNULL(@MCG_ILongitude, col2) ,col3=ISNULL(@DIRECTION_CONTROL, col3),col4=ISNULL(@CURRENT_SPEED,col4),col5=ISNULL(@AUX1_CDeratingReq_1_1, col5) ,col6=ISNULL(@AUX1_I3ACContactorOn_1_1, col6) 
,col7=ISNULL(@AUX1_I3ACContactorOn_2_1, col7) ,col8=ISNULL(@AUX1_I3ACContactorOn_2_2, col8) ,col9=ISNULL(@AUX1_I3ACLoadEnabled_1_1, col9) ,col10=ISNULL(@AUX1_I3ACOk_1_1, col10) ,col11=ISNULL(@AUX1_IInputVoltageOk_1_1, col11) 
,col12=ISNULL(@AUX1_IInverterEnabled_1_1, col12) ,col13=ISNULL(@AUX1_ILineContactorOn_1_1, col13) ,col14=ISNULL(@AUX1_IOutputVoltageOk_1_1, col14) ,col15=ISNULL(@AUX1_IPreChargContOn_1_1, col15) ,col16=ISNULL(@AUX1_ITempHeatSinkOk_1_1, col16) 
,col17=ISNULL(@AUX2_CDeratingReq_1_2, col17) ,col18=ISNULL(@AUX2_I3ACContactorOn_1_2, col18) ,col19=ISNULL(@AUX2_I3ACContactorOn_2_1, col19) ,col20=ISNULL(@AUX2_I3ACContactorOn_2_2, col20) ,col21=ISNULL(@AUX2_I3ACLoadEnabled_1_2, col21) 
,col22=ISNULL(@AUX2_I3ACOk_1_2, col22) ,col23=ISNULL(@AUX2_IInputVoltageOk_1_2, col23) ,col24=ISNULL(@AUX2_IInverterEnabled_1_2, col24) ,col25=ISNULL(@AUX2_ILineContactorOn_1_2, col25) ,col26=ISNULL(@AUX2_IOutputVoltageOk_1_2, col26) 
,col27=ISNULL(@AUX2_IPreChargContOn_1_2, col27) ,col28=ISNULL(@AUX2_ITempHeatSinkOk_1_2, col28) ,col29=ISNULL(@AUXY_IBatCont1On, col29) ,col30=ISNULL(@AUXY_IBatCont1On_1_1, col30) ,col31=ISNULL(@AUXY_IBatCont1On_1_2, col31) 
,col32=ISNULL(@AUXY_IBatCont1On_2_1, col32) ,col33=ISNULL(@AUXY_IBatCont1On_2_2, col33) ,col34=ISNULL(@AUXY_IBatCont1On_3_1, col34) ,col35=ISNULL(@AUXY_IBatCont1On_3_2, col35) ,col36=ISNULL(@AUXY_IBatCont1On_8_1, col36) 
,col37=ISNULL(@AUXY_IBatCont1On_8_2, col37) ,col38=ISNULL(@AUXY_IBatCont1On_8_3, col38) ,col39=ISNULL(@AUXY_IBatCont1On_8_4, col39) ,col40=ISNULL(@AUXY_IBatCont1On_8_5, col40) ,col41=ISNULL(@AUXY_IBatCont1On_8_6, col41) 
,col42=ISNULL(@AUXY_IBatCont1On_8_7, col42) ,col43=ISNULL(@AUXY_IBatCont1On_8_8, col43) ,col44=ISNULL(@AUXY_IBatCont1On_9, col44) ,col45=ISNULL(@AUXY_IBatCont2On, col45) ,col46=ISNULL(@AUXY_IBatCont2On_1_1, col46) 
,col47=ISNULL(@AUXY_IBatCont2On_1_2, col47) ,col48=ISNULL(@AUXY_IBatCont2On_2_1, col48) ,col49=ISNULL(@AUXY_IBatCont2On_2_2, col49) ,col50=ISNULL(@AUXY_IBatCont2On_3_1, col50) ,col51=ISNULL(@AUXY_IBatCont2On_3_2, col51) 
,col52=ISNULL(@AUXY_IBatCont2On_8_1, col52) ,col53=ISNULL(@AUXY_IBatCont2On_8_2, col53) ,col54=ISNULL(@AUXY_IBatCont2On_8_3, col54) ,col55=ISNULL(@AUXY_IBatCont2On_8_4, col55) ,col56=ISNULL(@AUXY_IBatCont2On_8_5, col56) 
,col57=ISNULL(@AUXY_IBatCont2On_8_6, col57) ,col58=ISNULL(@AUXY_IBatCont2On_8_7, col58) ,col59=ISNULL(@AUXY_IBatCont2On_8_8, col59) ,col60=ISNULL(@AUXY_IBatCont2On_9, col60) ,col61=ISNULL(@BCHA1_I110VAuxPowSup_2_1, col61) 
,col62=ISNULL(@BCHA1_IBattCurr_2_1, col62) ,col63=ISNULL(@BCHA1_IBattOutputCurr_2_1, col63) ,col64=ISNULL(@BCHA1_IChargCurrMax_2_1, col64) ,col65=ISNULL(@BCHA1_IDcLinkVoltage_2_1, col65) ,col66=ISNULL(@BCHA1_IExternal400Vac, col66) 
,col67=ISNULL(@BCHA1_IExternal400Vac_1_1, col67) ,col68=ISNULL(@BCHA1_IExternal400Vac_1_2, col68) ,col69=ISNULL(@BCHA1_IExternal400Vac_2_1, col69) ,col70=ISNULL(@BCHA1_IExternal400Vac_2_2, col70) ,col71=ISNULL(@BCHA1_IExternal400Vac_3_1, col71) 
,col72=ISNULL(@BCHA1_IExternal400Vac_3_2, col72) ,col73=ISNULL(@BCHA1_IExternal400Vac_8_1, col73) ,col74=ISNULL(@BCHA1_IExternal400Vac_8_2, col74) ,col75=ISNULL(@BCHA1_IExternal400Vac_8_3, col75) ,col76=ISNULL(@BCHA1_IExternal400Vac_8_4, col76) 
,col77=ISNULL(@BCHA1_IExternal400Vac_8_5, col77) ,col78=ISNULL(@BCHA1_IExternal400Vac_8_6, col78) ,col79=ISNULL(@BCHA1_IExternal400Vac_8_7, col79) ,col80=ISNULL(@BCHA1_IExternal400Vac_8_8, col80) ,col81=ISNULL(@BCHA1_IExternal400Vac_9, col81) 
,col82=ISNULL(@BCHA1_IOutputPower_2_1, col82) ,col83=ISNULL(@BCHA1_ISpecSystNokMod_2_1, col83) ,col84=ISNULL(@BCHA1_ISpecSystOkMode_2_1, col84) ,col85=ISNULL(@BCHA1_ITempBattSens1_2_1, col85) ,col86=ISNULL(@BCHA1_ITempBattSens2_2_1, col86) 
,col87=ISNULL(@BCHA1_ITempHeatSink_2_1, col87) ,col88=ISNULL(@BCHA2_I110VAuxPowSup_2_2, col88) ,col89=ISNULL(@BCHA2_IBattCurr_2_2, col89) ,col90=ISNULL(@BCHA2_IBattOutputCurr_2_2, col90) ,col91=ISNULL(@BCHA2_IChargCurrMax_2_2, col91) 
,col92=ISNULL(@BCHA2_IDcLinkVoltage_2_2, col92) ,col93=ISNULL(@BCHA2_IExternal400Vac, col93) ,col94=ISNULL(@BCHA2_IExternal400Vac_1_1, col94) ,col95=ISNULL(@BCHA2_IExternal400Vac_1_2, col95) ,col96=ISNULL(@BCHA2_IExternal400Vac_2_1, col96) 
,col97=ISNULL(@BCHA2_IExternal400Vac_2_2, col97) ,col98=ISNULL(@BCHA2_IExternal400Vac_3_1, col98) ,col99=ISNULL(@BCHA2_IExternal400Vac_3_2, col99) ,col100=ISNULL(@BCHA2_IExternal400Vac_8_1, col100) ,col101=ISNULL(@BCHA2_IExternal400Vac_8_2, col101) 
,col102=ISNULL(@BCHA2_IExternal400Vac_8_3, col102) ,col103=ISNULL(@BCHA2_IExternal400Vac_8_4, col103) ,col104=ISNULL(@BCHA2_IExternal400Vac_8_5, col104) ,col105=ISNULL(@BCHA2_IExternal400Vac_8_6, col105) ,col106=ISNULL(@BCHA2_IExternal400Vac_8_7, col106) 
,col107=ISNULL(@BCHA2_IExternal400Vac_8_8, col107) ,col108=ISNULL(@BCHA2_IExternal400Vac_9, col108) ,col109=ISNULL(@BCHA2_IOutputPower_2_2, col109) ,col110=ISNULL(@BCHA2_ISpecSystNokMod_2_2, col110) ,col111=ISNULL(@BCHA2_ISpecSystOkMode_2_2, col111) 
,col112=ISNULL(@BCHA2_ITempBattSens1_2_2, col112) ,col113=ISNULL(@BCHA2_ITempBattSens2_2_2, col113) ,col114=ISNULL(@BCHA2_ITempHeatSink_2_2, col114) ,col115=ISNULL(@BCU1_IBrPipePressureInt_5_1, col115) ,col116=ISNULL(@BCU1_IMainAirResPipeInt_5_1, col116) 
,col117=ISNULL(@BCU1_IParkBrApplied_5_1, col117) ,col118=ISNULL(@BCU1_IParkBrLocked_5_1, col118) ,col119=ISNULL(@BCU1_IParkBrReleased_5_1, col119) ,col120=ISNULL(@BCU2_IBrPipePressureInt_5_2, col120) ,col121=ISNULL(@BCU2_IMainAirResPipeInt_5_2, col121) 
,col122=ISNULL(@BCU2_IParkBrApplied_5_2, col122) ,col123=ISNULL(@BCU2_IParkBrLocked_5_2, col123) ,col124=ISNULL(@BCU2_IParkBrReleased_5_2, col124) ,col125=ISNULL(@BCUm_IBrTestRun_5_1, col125) ,col126=ISNULL(@BCUm_IBrTestRun_5_2, col126) 
,col127=ISNULL(@BCUx_IBogie1Locked_5_1, col127) ,col128=ISNULL(@BCUx_IBogie1Locked_5_2, col128) ,col129=ISNULL(@BCUx_IBogie2Locked_5_1, col129) ,col130=ISNULL(@BCUx_IBogie2Locked_5_2, col130) ,col131=ISNULL(@BCUx_IBogie3Locked_5_1, col131) 
,col132=ISNULL(@BCUx_IBogie3Locked_5_2, col132) ,col133=ISNULL(@BCUx_IBogie4Locked_5_1, col133) ,col134=ISNULL(@BCUx_IBogie4Locked_5_2, col134) ,col135=ISNULL(@BCUx_IBogie5Locked_5_1, col135) ,col136=ISNULL(@BCUx_IBogie5Locked_5_2, col136) 
,col137=ISNULL(@BCUx_IBogie6Locked_5_1, col137) ,col138=ISNULL(@BCUx_IBogie6Locked_5_2, col138) ,col139=ISNULL(@BCUx_IBogie7Locked_5_1, col139) ,col140=ISNULL(@BCUx_IBogie7Locked_5_2, col140) ,col141=ISNULL(@BCUx_IWspBogie1Slide_3_1, col141) 
,col142=ISNULL(@BCUx_IWspBogie1Slide_3_2, col142) ,col143=ISNULL(@BCUx_IWspBogie1Slide_5_1, col143) ,col144=ISNULL(@BCUx_IWspBogie1Slide_5_2, col144) ,col145=ISNULL(@BCUx_IWspBogie2Slide_3_1, col145) ,col146=ISNULL(@BCUx_IWspBogie2Slide_3_2, col146) 
,col147=ISNULL(@BCUx_IWspBogie2Slide_5_1, col147) ,col148=ISNULL(@BCUx_IWspBogie2Slide_5_2, col148) ,col149=ISNULL(@BCUx_IWspBogie3Slide_3_1, col149) ,col150=ISNULL(@BCUx_IWspBogie3Slide_3_2, col150) ,col151=ISNULL(@BCUx_IWspBogie3Slide_5_1, col151) 
,col152=ISNULL(@BCUx_IWspBogie3Slide_5_2, col152) ,col153=ISNULL(@BCUx_IWspBogie4Slide_3_1, col153) ,col154=ISNULL(@BCUx_IWspBogie4Slide_3_2, col154) ,col155=ISNULL(@BCUx_IWspBogie4Slide_5_1, col155) ,col156=ISNULL(@BCUx_IWspBogie4Slide_5_2, col156) 
,col157=ISNULL(@BCUx_IWspBogie5Slide_3_1, col157) ,col158=ISNULL(@BCUx_IWspBogie5Slide_3_2, col158) ,col159=ISNULL(@BCUx_IWspBogie5Slide_5_1, col159) ,col160=ISNULL(@BCUx_IWspBogie5Slide_5_2, col160) ,col161=ISNULL(@BCUx_IWspBogie6Slide_3_1, col161) 
,col162=ISNULL(@BCUx_IWspBogie6Slide_3_2, col162) ,col163=ISNULL(@BCUx_IWspBogie6Slide_5_1, col163) ,col164=ISNULL(@BCUx_IWspBogie6Slide_5_2, col164) ,col165=ISNULL(@BCUx_IWspBogie7Slide_3_1, col165) ,col166=ISNULL(@BCUx_IWspBogie7Slide_3_2, col166) 
,col167=ISNULL(@BCUx_IWspBogie7Slide_5_1, col167) ,col168=ISNULL(@BCUx_IWspBogie7Slide_5_2, col168) ,col169=ISNULL(@CABIN, col169) ,col170=ISNULL(@CABIN_IST, col170) ,col171=ISNULL(@CABIN_SOLL, col171) 
,col172=ISNULL(@CABIN_STATUS, col172) ,col173=ISNULL(@CYCLE_COUNTER, col173) ,col174=ISNULL(@DBC_CBCU1ModeTowing_5_1, col174) ,col175=ISNULL(@DBC_CBCU1ModeTowing_5_2, col175) ,col176=ISNULL(@DBC_CBCU2ModeTowing_5_1, col176) 
,col177=ISNULL(@DBC_CBCU2ModeTowing_5_2, col177) ,col178=ISNULL(@DBC_Cbrake_M, col178) ,col179=ISNULL(@DBC_Cbrake_M_1_1, col179) ,col180=ISNULL(@DBC_Cbrake_M_1_2, col180) ,col181=ISNULL(@DBC_Cbrake_M_2_1, col181) 
,col182=ISNULL(@DBC_Cbrake_M_2_2, col182) ,col183=ISNULL(@DBC_Cbrake_M_3_1, col183) ,col184=ISNULL(@DBC_Cbrake_M_3_2, col184) ,col185=ISNULL(@DBC_Cbrake_M_4_1, col185) ,col186=ISNULL(@DBC_Cbrake_M_4_10, col186) 
,col187=ISNULL(@DBC_Cbrake_M_4_11, col187) ,col188=ISNULL(@DBC_Cbrake_M_4_12, col188) ,col189=ISNULL(@DBC_Cbrake_M_4_13, col189) ,col190=ISNULL(@DBC_Cbrake_M_4_14, col190) ,col191=ISNULL(@DBC_Cbrake_M_4_15, col191) 
,col192=ISNULL(@DBC_Cbrake_M_4_16, col192) ,col193=ISNULL(@DBC_Cbrake_M_4_17, col193) ,col194=ISNULL(@DBC_Cbrake_M_4_18, col194) ,col195=ISNULL(@DBC_Cbrake_M_4_19, col195) ,col196=ISNULL(@DBC_Cbrake_M_4_2, col196) 
,col197=ISNULL(@DBC_Cbrake_M_4_20, col197) ,col198=ISNULL(@DBC_Cbrake_M_4_3, col198) ,col199=ISNULL(@DBC_Cbrake_M_4_4, col199) ,col200=ISNULL(@DBC_Cbrake_M_4_5, col200) ,col201=ISNULL(@DBC_Cbrake_M_4_6, col201) 
,col202=ISNULL(@DBC_Cbrake_M_4_7, col202) ,col203=ISNULL(@DBC_Cbrake_M_4_8, col203) ,col204=ISNULL(@DBC_Cbrake_M_4_9, col204) ,col205=ISNULL(@DBC_Cbrake_M_5_1, col205) ,col206=ISNULL(@DBC_Cbrake_M_5_2, col206) 
,col207=ISNULL(@DBC_Cbrake_M_8_1, col207) ,col208=ISNULL(@DBC_Cbrake_M_8_2, col208) ,col209=ISNULL(@DBC_Cbrake_M_8_3, col209) ,col210=ISNULL(@DBC_Cbrake_M_8_4, col210) ,col211=ISNULL(@DBC_Cbrake_M_8_5, col211) 
,col212=ISNULL(@DBC_Cbrake_M_8_6, col212) ,col213=ISNULL(@DBC_Cbrake_M_8_7, col213) ,col214=ISNULL(@DBC_Cbrake_M_8_8, col214) ,col215=ISNULL(@DBC_CBrakeQuick_M, col215) ,col216=ISNULL(@DBC_CBrakeQuick_M_1_1, col216) 
,col217=ISNULL(@DBC_CBrakeQuick_M_1_2, col217) ,col218=ISNULL(@DBC_CBrakeQuick_M_2_1, col218) ,col219=ISNULL(@DBC_CBrakeQuick_M_2_2, col219) ,col220=ISNULL(@DBC_CBrakeQuick_M_3_1, col220) ,col221=ISNULL(@DBC_CBrakeQuick_M_3_2, col221) 
,col222=ISNULL(@DBC_CBrakeQuick_M_4_1, col222) ,col223=ISNULL(@DBC_CBrakeQuick_M_4_10, col223) ,col224=ISNULL(@DBC_CBrakeQuick_M_4_11, col224) ,col225=ISNULL(@DBC_CBrakeQuick_M_4_12, col225) ,col226=ISNULL(@DBC_CBrakeQuick_M_4_13, col226) 
,col227=ISNULL(@DBC_CBrakeQuick_M_4_14, col227) ,col228=ISNULL(@DBC_CBrakeQuick_M_4_15, col228) ,col229=ISNULL(@DBC_CBrakeQuick_M_4_16, col229) ,col230=ISNULL(@DBC_CBrakeQuick_M_4_17, col230) ,col231=ISNULL(@DBC_CBrakeQuick_M_4_18, col231) 
,col232=ISNULL(@DBC_CBrakeQuick_M_4_19, col232) ,col233=ISNULL(@DBC_CBrakeQuick_M_4_2, col233) ,col234=ISNULL(@DBC_CBrakeQuick_M_4_20, col234) ,col235=ISNULL(@DBC_CBrakeQuick_M_4_3, col235) ,col236=ISNULL(@DBC_CBrakeQuick_M_4_4, col236) 
,col237=ISNULL(@DBC_CBrakeQuick_M_4_5, col237) ,col238=ISNULL(@DBC_CBrakeQuick_M_4_6, col238) ,col239=ISNULL(@DBC_CBrakeQuick_M_4_7, col239) ,col240=ISNULL(@DBC_CBrakeQuick_M_4_8, col240) ,col241=ISNULL(@DBC_CBrakeQuick_M_4_9, col241) 
,col242=ISNULL(@DBC_CBrakeQuick_M_5_1, col242) ,col243=ISNULL(@DBC_CBrakeQuick_M_5_2, col243) ,col244=ISNULL(@DBC_CBrakeQuick_M_8_1, col244) ,col245=ISNULL(@DBC_CBrakeQuick_M_8_2, col245) ,col246=ISNULL(@DBC_CBrakeQuick_M_8_3, col246) 
,col247=ISNULL(@DBC_CBrakeQuick_M_8_4, col247) ,col248=ISNULL(@DBC_CBrakeQuick_M_8_5, col248) ,col249=ISNULL(@DBC_CBrakeQuick_M_8_6, col249) ,col250=ISNULL(@DBC_CBrakeQuick_M_8_7, col250) ,col251=ISNULL(@DBC_CBrakeQuick_M_8_8, col251) 
,col252=ISNULL(@DBC_CBrakingEffortInt_3_1, col252) ,col253=ISNULL(@DBC_CBrakingEffortInt_3_2, col253) ,col254=ISNULL(@DBC_CBrakingEffortInt_5_1, col254) ,col255=ISNULL(@DBC_CBrakingEffortInt_5_2, col255) ,col256=ISNULL(@DBC_Cdrive_M, col256) 
,col257=ISNULL(@DBC_Cdrive_M_1_1, col257) ,col258=ISNULL(@DBC_Cdrive_M_1_2, col258) ,col259=ISNULL(@DBC_Cdrive_M_2_1, col259) ,col260=ISNULL(@DBC_Cdrive_M_2_2, col260) ,col261=ISNULL(@DBC_Cdrive_M_3_1, col261) 
,col262=ISNULL(@DBC_Cdrive_M_3_2, col262) ,col263=ISNULL(@DBC_Cdrive_M_4_1, col263) ,col264=ISNULL(@DBC_Cdrive_M_4_10, col264) ,col265=ISNULL(@DBC_Cdrive_M_4_11, col265) ,col266=ISNULL(@DBC_Cdrive_M_4_12, col266) 
,col267=ISNULL(@DBC_Cdrive_M_4_13, col267) ,col268=ISNULL(@DBC_Cdrive_M_4_14, col268) ,col269=ISNULL(@DBC_Cdrive_M_4_15, col269) ,col270=ISNULL(@DBC_Cdrive_M_4_16, col270) ,col271=ISNULL(@DBC_Cdrive_M_4_17, col271) 
,col272=ISNULL(@DBC_Cdrive_M_4_18, col272) ,col273=ISNULL(@DBC_Cdrive_M_4_19, col273) ,col274=ISNULL(@DBC_Cdrive_M_4_2, col274) ,col275=ISNULL(@DBC_Cdrive_M_4_20, col275) ,col276=ISNULL(@DBC_Cdrive_M_4_3, col276) 
,col277=ISNULL(@DBC_Cdrive_M_4_4, col277) ,col278=ISNULL(@DBC_Cdrive_M_4_5, col278) ,col279=ISNULL(@DBC_Cdrive_M_4_6, col279) ,col280=ISNULL(@DBC_Cdrive_M_4_7, col280) ,col281=ISNULL(@DBC_Cdrive_M_4_8, col281) 
,col282=ISNULL(@DBC_Cdrive_M_4_9, col282) ,col283=ISNULL(@DBC_Cdrive_M_5_1, col283) ,col284=ISNULL(@DBC_Cdrive_M_5_2, col284) ,col285=ISNULL(@DBC_Cdrive_M_8_1, col285) ,col286=ISNULL(@DBC_Cdrive_M_8_2, col286) 
,col287=ISNULL(@DBC_Cdrive_M_8_3, col287) ,col288=ISNULL(@DBC_Cdrive_M_8_4, col288) ,col289=ISNULL(@DBC_Cdrive_M_8_5, col289) ,col290=ISNULL(@DBC_Cdrive_M_8_6, col290) ,col291=ISNULL(@DBC_Cdrive_M_8_7, col291) 
,col292=ISNULL(@DBC_Cdrive_M_8_8, col292) ,col293=ISNULL(@DBC_CModeTowing_M_5_1, col293) ,col294=ISNULL(@DBC_CModeTowing_M_5_2, col294) ,col295=ISNULL(@DBC_CTCU1GrInTracConv1_3_1, col295) ,col296=ISNULL(@DBC_CTCU2GrInTracConv2_3_2, col296) 
,col297=ISNULL(@DBC_CTractiveEffort_3_1, col297) ,col298=ISNULL(@DBC_CTractiveEffort_3_2, col298) ,col299=ISNULL(@DBC_ITrainSpeedInt, col299) ,col300=ISNULL(@DBC_ITrainSpeedInt_1_1, col300) ,col301=ISNULL(@DBC_ITrainSpeedInt_1_2, col301) 
,col302=ISNULL(@DBC_ITrainSpeedInt_15, col302) ,col303=ISNULL(@DBC_ITrainSpeedInt_2_1, col303) ,col304=ISNULL(@DBC_ITrainSpeedInt_2_2, col304) ,col305=ISNULL(@DBC_ITrainSpeedInt_3_1, col305) ,col306=ISNULL(@DBC_ITrainSpeedInt_3_2, col306) 
,col307=ISNULL(@DBC_ITrainSpeedInt_4_1, col307) ,col308=ISNULL(@DBC_ITrainSpeedInt_4_10, col308) ,col309=ISNULL(@DBC_ITrainSpeedInt_4_11, col309) ,col310=ISNULL(@DBC_ITrainSpeedInt_4_12, col310) ,col311=ISNULL(@DBC_ITrainSpeedInt_4_13, col311) 
,col312=ISNULL(@DBC_ITrainSpeedInt_4_14, col312) ,col313=ISNULL(@DBC_ITrainSpeedInt_4_15, col313) ,col314=ISNULL(@DBC_ITrainSpeedInt_4_16, col314) ,col315=ISNULL(@DBC_ITrainSpeedInt_4_17, col315) ,col316=ISNULL(@DBC_ITrainSpeedInt_4_18, col316) 
,col317=ISNULL(@DBC_ITrainSpeedInt_4_19, col317) ,col318=ISNULL(@DBC_ITrainSpeedInt_4_2, col318) ,col319=ISNULL(@DBC_ITrainSpeedInt_4_20, col319) ,col320=ISNULL(@DBC_ITrainSpeedInt_4_3, col320) ,col321=ISNULL(@DBC_ITrainSpeedInt_4_4, col321) 
,col322=ISNULL(@DBC_ITrainSpeedInt_4_5, col322) ,col323=ISNULL(@DBC_ITrainSpeedInt_4_6, col323) ,col324=ISNULL(@DBC_ITrainSpeedInt_4_7, col324) ,col325=ISNULL(@DBC_ITrainSpeedInt_4_8, col325) ,col326=ISNULL(@DBC_ITrainSpeedInt_4_9, col326) 
,col327=ISNULL(@DBC_ITrainSpeedInt_5_1, col327) ,col328=ISNULL(@DBC_ITrainSpeedInt_5_2, col328) ,col329=ISNULL(@DBC_ITrainSpeedInt_8_1, col329) ,col330=ISNULL(@DBC_ITrainSpeedInt_8_2, col330) ,col331=ISNULL(@DBC_ITrainSpeedInt_8_3, col331) 
,col332=ISNULL(@DBC_ITrainSpeedInt_8_4, col332) ,col333=ISNULL(@DBC_ITrainSpeedInt_8_5, col333) ,col334=ISNULL(@DBC_ITrainSpeedInt_8_6, col334) ,col335=ISNULL(@DBC_ITrainSpeedInt_8_7, col335) ,col336=ISNULL(@DBC_ITrainSpeedInt_8_8, col336) 
,col337=ISNULL(@DBC_ITrainSpeedInt_9, col337) ,col338=ISNULL(@DBC_IZrSpdInd_4_1, col338) ,col339=ISNULL(@DBC_IZrSpdInd_4_10, col339) ,col340=ISNULL(@DBC_IZrSpdInd_4_11, col340) ,col341=ISNULL(@DBC_IZrSpdInd_4_12, col341) 
,col342=ISNULL(@DBC_IZrSpdInd_4_13, col342) ,col343=ISNULL(@DBC_IZrSpdInd_4_14, col343) ,col344=ISNULL(@DBC_IZrSpdInd_4_15, col344) ,col345=ISNULL(@DBC_IZrSpdInd_4_16, col345) ,col346=ISNULL(@DBC_IZrSpdInd_4_17, col346) 
,col347=ISNULL(@DBC_IZrSpdInd_4_18, col347) ,col348=ISNULL(@DBC_IZrSpdInd_4_19, col348) ,col349=ISNULL(@DBC_IZrSpdInd_4_2, col349) ,col350=ISNULL(@DBC_IZrSpdInd_4_20, col350) ,col351=ISNULL(@DBC_IZrSpdInd_4_3, col351) 
,col352=ISNULL(@DBC_IZrSpdInd_4_4, col352) ,col353=ISNULL(@DBC_IZrSpdInd_4_5, col353) ,col354=ISNULL(@DBC_IZrSpdInd_4_6, col354) ,col355=ISNULL(@DBC_IZrSpdInd_4_7, col355) ,col356=ISNULL(@DBC_IZrSpdInd_4_8, col356) 
,col357=ISNULL(@DBC_IZrSpdInd_4_9, col357) ,col358=ISNULL(@DBC_PRTcu1GrOut_3_1, col358) ,col359=ISNULL(@DBC_PRTcu2GrOut_3_2, col359) ,col360=ISNULL(@DRIVERBRAKE_APPLIED_SUFF, col360) ,col361=ISNULL(@DRIVERBRAKE_OPERATED, col361) 
,col362=ISNULL(@EB_STATUS, col362) ,col363=ISNULL(@ERROR_CODE_INTERNAL, col363) ,col364=ISNULL(@ERRORCODE_1, col364) ,col365=ISNULL(@ERRORCODE_2, col365) ,col366=ISNULL(@ERRORCODE_3, col366) 
,col367=ISNULL(@GW_IGwOrientation, col367) ,col368=ISNULL(@HAC_ITempOutsideInt, col368) ,col369=ISNULL(@HAC_ITempOutsideInt_1_1, col369) ,col370=ISNULL(@HAC_ITempOutsideInt_1_2, col370) ,col371=ISNULL(@HAC_ITempOutsideInt_2_1, col371) 
,col372=ISNULL(@HAC_ITempOutsideInt_2_2, col372) ,col373=ISNULL(@HAC_ITempOutsideInt_3_1, col373) ,col374=ISNULL(@HAC_ITempOutsideInt_3_2, col374) ,col375=ISNULL(@HAC_ITempOutsideInt_4_1, col375) ,col376=ISNULL(@HAC_ITempOutsideInt_4_10, col376) 
,col377=ISNULL(@HAC_ITempOutsideInt_4_11, col377) ,col378=ISNULL(@HAC_ITempOutsideInt_4_12, col378) ,col379=ISNULL(@HAC_ITempOutsideInt_4_13, col379) ,col380=ISNULL(@HAC_ITempOutsideInt_4_14, col380) ,col381=ISNULL(@HAC_ITempOutsideInt_4_15, col381) 
,col382=ISNULL(@HAC_ITempOutsideInt_4_16, col382) ,col383=ISNULL(@HAC_ITempOutsideInt_4_17, col383) ,col384=ISNULL(@HAC_ITempOutsideInt_4_18, col384) ,col385=ISNULL(@HAC_ITempOutsideInt_4_19, col385) ,col386=ISNULL(@HAC_ITempOutsideInt_4_2, col386) 
,col387=ISNULL(@HAC_ITempOutsideInt_4_20, col387) ,col388=ISNULL(@HAC_ITempOutsideInt_4_3, col388) ,col389=ISNULL(@HAC_ITempOutsideInt_4_4, col389) ,col390=ISNULL(@HAC_ITempOutsideInt_4_5, col390) ,col391=ISNULL(@HAC_ITempOutsideInt_4_6, col391) 
,col392=ISNULL(@HAC_ITempOutsideInt_4_7, col392) ,col393=ISNULL(@HAC_ITempOutsideInt_4_8, col393) ,col394=ISNULL(@HAC_ITempOutsideInt_4_9, col394) ,col395=ISNULL(@HAC_ITempOutsideInt_5_1, col395) ,col396=ISNULL(@HAC_ITempOutsideInt_5_2, col396) 
,col397=ISNULL(@HAC_ITempOutsideInt_9, col397) ,col398=ISNULL(@HVAC1_ITempAirMixedInt, col398) ,col399=ISNULL(@HVAC1_ITempDuctAir1Int, col399) ,col400=ISNULL(@HVAC1_ITempDuctAir2Int, col400) ,col401=ISNULL(@HVAC1_ITempInComInt, col401) 
,col402=ISNULL(@HVAC1_ITempOutsideInt, col402) ,col403=ISNULL(@HVAC1_ITempSetpActDuctInt, col403) ,col404=ISNULL(@HVAC1_ITempSetpActInt, col404) ,col405=ISNULL(@HVAC2_ITempAirMixedInt, col405) ,col406=ISNULL(@HVAC2_ITempDuctAir1Int, col406) 
,col407=ISNULL(@HVAC2_ITempDuctAir2Int, col407) ,col408=ISNULL(@HVAC2_ITempInComInt, col408) ,col409=ISNULL(@HVAC2_ITempOutsideInt, col409) ,col410=ISNULL(@HVAC2_ITempSetpActDuctInt, col410) ,col411=ISNULL(@HVAC2_ITempSetpActInt, col411) 
,col412=ISNULL(@HVAC3_ITempAirMixedInt, col412) ,col413=ISNULL(@HVAC3_ITempDuctAir1Int, col413) ,col414=ISNULL(@HVAC3_ITempDuctAir2Int, col414) ,col415=ISNULL(@HVAC3_ITempInComInt, col415) ,col416=ISNULL(@HVAC3_ITempOutsideInt, col416) 
,col417=ISNULL(@HVAC3_ITempSetpActDuctInt, col417) ,col418=ISNULL(@HVAC3_ITempSetpActInt, col418) ,col419=ISNULL(@HVAC4_ITempAirMixedInt, col419) ,col420=ISNULL(@HVAC4_ITempDuctAir1Int, col420) ,col421=ISNULL(@HVAC4_ITempDuctAir2Int, col421) 
,col422=ISNULL(@HVAC4_ITempInComInt, col422) ,col423=ISNULL(@HVAC4_ITempOutsideInt, col423) ,col424=ISNULL(@HVAC4_ITempSetpActDuctInt, col424) ,col425=ISNULL(@HVAC4_ITempSetpActInt, col425) ,col426=ISNULL(@HVAC5_ITempAirMixedInt, col426) 
,col427=ISNULL(@HVAC5_ITempDuctAir1Int, col427) ,col428=ISNULL(@HVAC5_ITempDuctAir2Int, col428) ,col429=ISNULL(@HVAC5_ITempInComInt, col429) ,col430=ISNULL(@HVAC5_ITempOutsideInt, col430) ,col431=ISNULL(@HVAC5_ITempSetpActDuctInt, col431) 
,col432=ISNULL(@HVAC5_ITempSetpActInt, col432) ,col433=ISNULL(@HVAC6_ITempAirMixedInt, col433) ,col434=ISNULL(@HVAC6_ITempDuctAir1Int, col434) ,col435=ISNULL(@HVAC6_ITempDuctAir2Int, col435) ,col436=ISNULL(@HVAC6_ITempInComInt, col436) 
,col437=ISNULL(@HVAC6_ITempOutsideInt, col437) ,col438=ISNULL(@HVAC6_ITempSetpActDuctInt, col438) ,col439=ISNULL(@HVAC6_ITempSetpActInt, col439) ,col440=ISNULL(@HVCC1_ITempDuctAir1Int, col440) ,col441=ISNULL(@HVCC1_ITempInComInt, col441) 
,col442=ISNULL(@HVCC1_ITempOutsideInt, col442) ,col443=ISNULL(@HVCC1_ITempSetpActDuctInt, col443) ,col444=ISNULL(@HVCC1_ITempSetpActInt, col444) ,col445=ISNULL(@HVCC2_ITempDuctAir1Int, col445) ,col446=ISNULL(@HVCC2_ITempInComInt, col446) 
,col447=ISNULL(@HVCC2_ITempOutsideInt, col447) ,col448=ISNULL(@HVCC2_ITempSetpActDuctInt, col448) ,col449=ISNULL(@HVCC2_ITempSetpActInt, col449) ,col450=ISNULL(@INadiCtrlInfo1, col450) ,col451=ISNULL(@INadiCtrlInfo2, col451) 
,col452=ISNULL(@INadiCtrlInfo3, col452) ,col453=ISNULL(@INadiNumberEntries, col453) ,col454=ISNULL(@INadiTcnAddr1, col454) ,col455=ISNULL(@INadiTcnAddr2, col455) ,col456=ISNULL(@INadiTcnAddr3, col456) 
,col457=ISNULL(@INadiTopoCount, col457) ,col458=ISNULL(@INadiUicAddr1, col458) ,col459=ISNULL(@INadiUicAddr2, col459) ,col460=ISNULL(@INadiUicAddr3, col460) ,col461=ISNULL(@INTERNAL_ERROR_CODE, col461) 
,col462=ISNULL(@INTERNAL_STATE, col462) ,col463=ISNULL(@ITrainsetNumber1, col463) ,col464=ISNULL(@ITrainsetNumber2, col464) ,col465=ISNULL(@ITrainsetNumber3, col465) ,col466=ISNULL(@MCG_IGsmSigStrength, col466) 
,col467=ISNULL(@MIO_IContBusBar400On, col467) ,col468=ISNULL(@MIO_IContBusBar400On_1_1, col468) ,col469=ISNULL(@MIO_IContBusBar400On_1_2, col469) ,col470=ISNULL(@MIO_IContBusBar400On_2_1, col470) ,col471=ISNULL(@MIO_IContBusBar400On_2_2, col471) 
,col472=ISNULL(@MIO_IContBusBar400On_3_1, col472) ,col473=ISNULL(@MIO_IContBusBar400On_3_2, col473) ,col474=ISNULL(@MIO_IContBusBar400On_8_1, col474) ,col475=ISNULL(@MIO_IContBusBar400On_8_2, col475) ,col476=ISNULL(@MIO_IContBusBar400On_8_3, col476) 
,col477=ISNULL(@MIO_IContBusBar400On_8_4, col477) ,col478=ISNULL(@MIO_IContBusBar400On_8_5, col478) ,col479=ISNULL(@MIO_IContBusBar400On_8_6, col479) ,col480=ISNULL(@MIO_IContBusBar400On_8_7, col480) ,col481=ISNULL(@MIO_IContBusBar400On_8_8, col481) 
,col482=ISNULL(@MIO_IContBusBar400On_9, col482) ,col483=ISNULL(@MIO_IHscbOff, col483) ,col484=ISNULL(@MIO_IHscbOff_1_1, col484) ,col485=ISNULL(@MIO_IHscbOff_1_2, col485) ,col486=ISNULL(@MIO_IHscbOff_2_1, col486) 
,col487=ISNULL(@MIO_IHscbOff_2_2, col487) ,col488=ISNULL(@MIO_IHscbOff_3_1, col488) ,col489=ISNULL(@MIO_IHscbOff_3_2, col489) ,col490=ISNULL(@MIO_IHscbOff_8_1, col490) ,col491=ISNULL(@MIO_IHscbOff_8_2, col491) 
,col492=ISNULL(@MIO_IHscbOff_8_3, col492) ,col493=ISNULL(@MIO_IHscbOff_8_4, col493) ,col494=ISNULL(@MIO_IHscbOff_8_5, col494) ,col495=ISNULL(@MIO_IHscbOff_8_6, col495) ,col496=ISNULL(@MIO_IHscbOff_8_7, col496) 
,col497=ISNULL(@MIO_IHscbOff_8_8, col497) ,col498=ISNULL(@MIO_IHscbOn, col498) ,col499=ISNULL(@MIO_IHscbOn_1_1, col499) ,col500=ISNULL(@MIO_IHscbOn_1_2, col500) ,col501=ISNULL(@MIO_IHscbOn_2_1, col501) 
,col502=ISNULL(@MIO_IHscbOn_2_2, col502) ,col503=ISNULL(@MIO_IHscbOn_3_1, col503) ,col504=ISNULL(@MIO_IHscbOn_3_2, col504) ,col505=ISNULL(@MIO_IHscbOn_8_1, col505) ,col506=ISNULL(@MIO_IHscbOn_8_2, col506) 
,col507=ISNULL(@MIO_IHscbOn_8_3, col507) ,col508=ISNULL(@MIO_IHscbOn_8_4, col508) ,col509=ISNULL(@MIO_IHscbOn_8_5, col509) ,col510=ISNULL(@MIO_IHscbOn_8_6, col510) ,col511=ISNULL(@MIO_IHscbOn_8_7, col511) 
,col512=ISNULL(@MIO_IHscbOn_8_8, col512) ,col513=ISNULL(@MIO_IPantoPrssSw1On, col513) ,col514=ISNULL(@MIO_IPantoPrssSw1On_1_1, col514) ,col515=ISNULL(@MIO_IPantoPrssSw1On_1_2, col515) ,col516=ISNULL(@MIO_IPantoPrssSw1On_2_1, col516) 
,col517=ISNULL(@MIO_IPantoPrssSw1On_2_2, col517) ,col518=ISNULL(@MIO_IPantoPrssSw1On_3_1, col518) ,col519=ISNULL(@MIO_IPantoPrssSw1On_3_2, col519) ,col520=ISNULL(@MIO_IPantoPrssSw1On_8_1, col520) ,col521=ISNULL(@MIO_IPantoPrssSw1On_8_2, col521) 
,col522=ISNULL(@MIO_IPantoPrssSw1On_8_3, col522) ,col523=ISNULL(@MIO_IPantoPrssSw1On_8_4, col523) ,col524=ISNULL(@MIO_IPantoPrssSw1On_8_5, col524) ,col525=ISNULL(@MIO_IPantoPrssSw1On_8_6, col525) ,col526=ISNULL(@MIO_IPantoPrssSw1On_8_7, col526) 
,col527=ISNULL(@MIO_IPantoPrssSw1On_8_8, col527) ,col528=ISNULL(@MIO_IPantoPrssSw1On_9, col528) ,col529=ISNULL(@MIO_IPantoPrssSw2On, col529) ,col530=ISNULL(@MIO_IPantoPrssSw2On_1_1, col530) ,col531=ISNULL(@MIO_IPantoPrssSw2On_1_2, col531) 
,col532=ISNULL(@MIO_IPantoPrssSw2On_2_1, col532) ,col533=ISNULL(@MIO_IPantoPrssSw2On_2_2, col533) ,col534=ISNULL(@MIO_IPantoPrssSw2On_3_1, col534) ,col535=ISNULL(@MIO_IPantoPrssSw2On_3_2, col535) ,col536=ISNULL(@MIO_IPantoPrssSw2On_8_1, col536) 
,col537=ISNULL(@MIO_IPantoPrssSw2On_8_2, col537) ,col538=ISNULL(@MIO_IPantoPrssSw2On_8_3, col538) ,col539=ISNULL(@MIO_IPantoPrssSw2On_8_4, col539) ,col540=ISNULL(@MIO_IPantoPrssSw2On_8_5, col540) ,col541=ISNULL(@MIO_IPantoPrssSw2On_8_6, col541) 
,col542=ISNULL(@MIO_IPantoPrssSw2On_8_7, col542) ,col543=ISNULL(@MIO_IPantoPrssSw2On_8_8, col543) ,col544=ISNULL(@MIO_IPantoPrssSw2On_9, col544) ,col545=ISNULL(@MMI_EB_STATUS, col545) ,col546=ISNULL(@MMI_M_ACTIVE_CABIN, col546) 
,col547=ISNULL(@MMI_M_LEVEL, col547) ,col548=ISNULL(@MMI_M_MODE, col548) ,col549=ISNULL(@MMI_M_WARNING, col549) ,col550=ISNULL(@MMI_SB_STATUS, col550) ,col551=ISNULL(@MMI_V_PERMITTED, col551) 
,col552=ISNULL(@MMI_V_TRAIN, col552) ,col553=ISNULL(@NID_STM_1, col553) ,col554=ISNULL(@NID_STM_2, col554) ,col555=ISNULL(@NID_STM_3, col555) ,col556=ISNULL(@NID_STM_4, col556) 
,col557=ISNULL(@NID_STM_5, col557) ,col558=ISNULL(@NID_STM_6, col558) ,col559=ISNULL(@NID_STM_7, col559) ,col560=ISNULL(@NID_STM_8, col560) ,col561=ISNULL(@NID_STM_DA, col561) 
,col562=ISNULL(@ONE_STM_DA, col562) ,col563=ISNULL(@ONE_STM_HS, col563) ,col564=ISNULL(@PB_ADDR, col564) ,col565=ISNULL(@PERMITTED_SPEED, col565) ,col566=ISNULL(@Q_ODO, col566) 
,col567=ISNULL(@REPORTED_DIRECTION, col567) ,col568=ISNULL(@REPORTED_SPEED, col568) ,col569=ISNULL(@REPORTED_SPEED_HISTORIC, col569) ,col570=ISNULL(@ROTATION_DIRECTION_SDU1, col570) ,col571=ISNULL(@ROTATION_DIRECTION_SDU2, col571) 
,col572=ISNULL(@SAP, col572) ,col573=ISNULL(@SB_STATUS, col573) ,col574=ISNULL(@SDU_DIRECTION_SDU1, col574) ,col575=ISNULL(@SDU_DIRECTION_SDU2, col575) ,col576=ISNULL(@SENSOR_ERROR_STATUS1_TACHO1, col576) 
,col577=ISNULL(@SENSOR_ERROR_STATUS2_TACHO2, col577) ,col578=ISNULL(@SENSOR_ERROR_STATUS3_RADAR1, col578) ,col579=ISNULL(@SENSOR_ERROR_STATUS4_RADAR2, col579) ,col580=ISNULL(@SENSOR_SPEED_HISTORIC_RADAR1, col580) ,col581=ISNULL(@SENSOR_SPEED_HISTORIC_RADAR2, col581) 
,col582=ISNULL(@SENSOR_SPEED_HISTORIC_TACHO1, col582) ,col583=ISNULL(@SENSOR_SPEED_HISTORIC_TACHO2, col583) ,col584=ISNULL(@SENSOR_SPEED_RADAR1, col584) ,col585=ISNULL(@SENSOR_SPEED_RADAR2, col585) ,col586=ISNULL(@SENSOR_SPEED_TACHO1, col586) 
,col587=ISNULL(@SENSOR_SPEED_TACHO2, col587) ,col588=ISNULL(@SLIP_SLIDE_ACC_AXLE1, col588) ,col589=ISNULL(@SLIP_SLIDE_ACC_AXLE2, col589) ,col590=ISNULL(@SLIP_SLIDE_AXLE1, col590) ,col591=ISNULL(@SLIP_SLIDE_AXLE2, col591) 
,col592=ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE1, col592) ,col593=ISNULL(@SLIP_SLIDE_RADAR_DIFF_AXLE2, col593) ,col594=ISNULL(@SLIP_SLIDE_STATUS, col594) ,col595=ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE1, col595) ,col596=ISNULL(@SLIP_SLIDE_TACHO_DIFF_AXLE2, col596) 
,col597=ISNULL(@SOFTWARE_VERSION, col597) ,col598=ISNULL(@SPL_FUNC_ID, col598) ,col599=ISNULL(@SPL_RETURN, col599) ,col600=ISNULL(@STM_STATE, col600) ,col601=ISNULL(@STM_SUB_STATE, col601) 
,col602=ISNULL(@SUPERVISION_STATE, col602) ,col603=ISNULL(@TC_CCorrectOrient_M, col603) ,col604=ISNULL(@TC_IConsistOrient, col604) ,col605=ISNULL(@TC_IGwOrient, col605) ,col606=ISNULL(@TC_INumConsists, col606) 
,col607=ISNULL(@TC_INumConsists_1_1, col607) ,col608=ISNULL(@TC_INumConsists_1_2, col608) ,col609=ISNULL(@TC_INumConsists_2_1, col609) ,col610=ISNULL(@TC_INumConsists_2_2, col610) ,col611=ISNULL(@TC_INumConsists_3_1, col611) 
,col612=ISNULL(@TC_INumConsists_3_2, col612) ,col613=ISNULL(@TC_INumConsists_4_1, col613) ,col614=ISNULL(@TC_INumConsists_4_10, col614) ,col615=ISNULL(@TC_INumConsists_4_11, col615) ,col616=ISNULL(@TC_INumConsists_4_12, col616) 
,col617=ISNULL(@TC_INumConsists_4_13, col617) ,col618=ISNULL(@TC_INumConsists_4_14, col618) ,col619=ISNULL(@TC_INumConsists_4_15, col619) ,col620=ISNULL(@TC_INumConsists_4_16, col620) ,col621=ISNULL(@TC_INumConsists_4_17, col621) 
,col622=ISNULL(@TC_INumConsists_4_18, col622) ,col623=ISNULL(@TC_INumConsists_4_19, col623) ,col624=ISNULL(@TC_INumConsists_4_2, col624) ,col625=ISNULL(@TC_INumConsists_4_20, col625) ,col626=ISNULL(@TC_INumConsists_4_3, col626) 
,col627=ISNULL(@TC_INumConsists_4_4, col627) ,col628=ISNULL(@TC_INumConsists_4_5, col628) ,col629=ISNULL(@TC_INumConsists_4_6, col629) ,col630=ISNULL(@TC_INumConsists_4_7, col630) ,col631=ISNULL(@TC_INumConsists_4_8, col631) 
,col632=ISNULL(@TC_INumConsists_4_9, col632) ,col633=ISNULL(@TC_INumConsists_5_1, col633) ,col634=ISNULL(@TC_INumConsists_5_2, col634) ,col635=ISNULL(@TC_INumConsists_8_1, col635) ,col636=ISNULL(@TC_INumConsists_8_2, col636) 
,col637=ISNULL(@TC_INumConsists_8_3, col637) ,col638=ISNULL(@TC_INumConsists_8_4, col638) ,col639=ISNULL(@TC_INumConsists_8_5, col639) ,col640=ISNULL(@TC_INumConsists_8_6, col640) ,col641=ISNULL(@TC_INumConsists_8_7, col641) 
,col642=ISNULL(@TC_INumConsists_8_8, col642) ,col643=ISNULL(@TC_INumConsists_9, col643) ,col644=ISNULL(@TC_ITclOrient, col644) ,col645=ISNULL(@TCO_STATUS, col645) ,col646=ISNULL(@TCU1_IEdSliding_3_1, col646) 
,col647=ISNULL(@TCU1_IEdSliding_5_1, col647) ,col648=ISNULL(@TCU1_ILineVoltage_3_1, col648) ,col649=ISNULL(@TCU1_ILineVoltage_5_1, col649) ,col650=ISNULL(@TCU1_ILineVoltage_5_2, col650) ,col651=ISNULL(@TCU1_IPowerLimit_3_1, col651) 
,col652=ISNULL(@TCU1_ITracBrActualInt_3_1, col652) ,col653=ISNULL(@TCU2_IEdSliding_3_2, col653) ,col654=ISNULL(@TCU2_IEdSliding_5_2, col654) ,col655=ISNULL(@TCU2_ILineVoltage_3_2, col655) ,col656=ISNULL(@TCU2_ILineVoltage_5_1, col656) 
,col657=ISNULL(@TCU2_ILineVoltage_5_2, col657) ,col658=ISNULL(@TCU2_IPowerLimit_3_2, col658) ,col659=ISNULL(@TCU2_ITracBrActualInt_3_2, col659) ,col660=ISNULL(@TRACK_SIGNAL, col660) ,col661=ISNULL(@DBC_PRTcu1GrOut, col661) 
,col662=ISNULL(@DBC_PRTcu2GrOut, col662) ,col663=ISNULL(@MPW_PRHscbEnable, col663) ,col664=ISNULL(@MPW_IPnt1UpDrvCount, col664) ,col665=ISNULL(@MPW_IPnt2UpDrvCount, col665) ,col666=ISNULL(@MPW_IPnt1UpCount, col666) 
,col667=ISNULL(@MPW_IPnt2UpCount, col667) ,col668=ISNULL(@MPW_IPnt1KmCount, col668) ,col669=ISNULL(@MPW_IPnt2KmCount, col669) ,col670=ISNULL(@DBC_IKmCounter, col670) ,col671=ISNULL(@AUXY_IAuxAirCOnCnt, col671) 
,col672=ISNULL(@MPW_IOnHscb, col672) ,col673=ISNULL(@AUXY_IAirCSwOnCount, col673) ,col674=ISNULL(@AUXY_IAirCOnCount, col674) ,col675=ISNULL(@AUXY_IAuxAirCSwOnCnt, col675) ,col676=ISNULL(@MPW_PRPantoEnable, col676) 
,col677=ISNULL(@DIA_Reserve1_AUX1, col677) ,col678=ISNULL(@DIA_Reserve1_AUX2, col678) ,col679=ISNULL(@DIA_Reserve1_BCHA_1, col679) ,col680=ISNULL(@DIA_Reserve1_BCHA_2, col680) ,col681=ISNULL(@DIA_Reserve1_BCU_1, col681)
,col682=ISNULL(@DIA_Reserve1_BCU_2, col682) ,col683=ISNULL(@DIA_Reserve1_CCUO, col683) ,col684=ISNULL(@DIA_Reserve1_DCU_1, col684) ,col685=ISNULL(@DIA_Reserve1_DCU_10, col685) ,col686=ISNULL(@DIA_Reserve1_DCU_11, col686)
,col687=ISNULL(@DIA_Reserve1_DCU_12, col687) ,col688=ISNULL(@DIA_Reserve1_DCU_13, col688) ,col689=ISNULL(@DIA_Reserve1_DCU_14, col689) ,col690=ISNULL(@DIA_Reserve1_DCU_15, col690) ,col691=ISNULL(@DIA_Reserve1_DCU_16, col691)
,col692=ISNULL(@DIA_Reserve1_DCU_17, col692) ,col693=ISNULL(@DIA_Reserve1_DCU_18, col693) ,col694=ISNULL(@DIA_Reserve1_DCU_19, col694) ,col695=ISNULL(@DIA_Reserve1_DCU_2, col695) ,col696=ISNULL(@DIA_Reserve1_DCU_20, col696)
,col697=ISNULL(@DIA_Reserve1_DCU_3, col697) ,col698=ISNULL(@DIA_Reserve1_DCU_4, col698) ,col699=ISNULL(@DIA_Reserve1_DCU_5, col699) ,col700=ISNULL(@DIA_Reserve1_DCU_6, col700) ,col701=ISNULL(@DIA_Reserve1_DCU_7, col701)
,col702=ISNULL(@DIA_Reserve1_DCU_8, col702) ,col703=ISNULL(@DIA_Reserve1_DCU_9, col703) ,col704=ISNULL(@DIA_Reserve1_HVAC_1, col704) ,col705=ISNULL(@DIA_Reserve1_HVAC_2, col705) ,col706=ISNULL(@DIA_Reserve1_HVAC_3, col706)
,col707=ISNULL(@DIA_Reserve1_HVAC_4, col707) ,col708=ISNULL(@DIA_Reserve1_HVAC_5, col708) ,col709=ISNULL(@DIA_Reserve1_HVAC_6, col709) ,col710=ISNULL(@DIA_Reserve1_HVCC_1, col710) ,col711=ISNULL(@DIA_Reserve1_HVCC_2, col711)
,col712=ISNULL(@DIA_Reserve1_PIS, col712) ,col713=ISNULL(@DIA_Reserve1_TCU_1, col713) ,col714=ISNULL(@DIA_Reserve1_TCU_2, col714) ,col715=ISNULL(@Reserve, col715)
Where Id = @ID

UPDATE ChannelValueDoor SET UpdateRecord = 1
,col3000=ISNULL(@DCU01_CDoorCloseConduc, col3000) ,col3001=ISNULL(@DCU01_IDoorClosedSafe, col3001) ,col3002=ISNULL(@DCU01_IDoorClRelease, col3002) ,col3003=ISNULL(@DCU01_IDoorPbClose, col3003) ,col3004=ISNULL(@DCU01_IDoorPbOpenIn, col3004) 
,col3005=ISNULL(@DCU01_IDoorPbOpenOut, col3005) ,col3006=ISNULL(@DCU01_IDoorReleased, col3006) ,col3007=ISNULL(@DCU01_IFootStepRel, col3007) ,col3008=ISNULL(@DCU01_IForcedClosDoor, col3008) ,col3009=ISNULL(@DCU01_ILeafsStopDoor, col3009) 
,col3010=ISNULL(@DCU01_IObstacleDoor, col3010) ,col3011=ISNULL(@DCU01_IObstacleStep, col3011) ,col3012=ISNULL(@DCU01_IOpenAssistDoor, col3012) ,col3013=ISNULL(@DCU01_IStandstillBack, col3013) ,col3014=ISNULL(@DCU02_CDoorCloseConduc, col3014) 
,col3015=ISNULL(@DCU02_IDoorClosedSafe, col3015) ,col3016=ISNULL(@DCU02_IDoorClRelease, col3016) ,col3017=ISNULL(@DCU02_IDoorPbClose, col3017) ,col3018=ISNULL(@DCU02_IDoorPbOpenIn, col3018) ,col3019=ISNULL(@DCU02_IDoorPbOpenOut, col3019) 
,col3020=ISNULL(@DCU02_IDoorReleased, col3020) ,col3021=ISNULL(@DCU02_IFootStepRel, col3021) ,col3022=ISNULL(@DCU02_IForcedClosDoor, col3022) ,col3023=ISNULL(@DCU02_ILeafsStopDoor, col3023) ,col3024=ISNULL(@DCU02_IObstacleDoor, col3024) 
,col3025=ISNULL(@DCU02_IObstacleStep, col3025) ,col3026=ISNULL(@DCU02_IOpenAssistDoor, col3026) ,col3027=ISNULL(@DCU02_IStandstillBack, col3027) ,col3028=ISNULL(@DCU03_CDoorCloseConduc, col3028) ,col3029=ISNULL(@DCU03_IDoorClosedSafe, col3029) 
,col3030=ISNULL(@DCU03_IDoorClRelease, col3030) ,col3031=ISNULL(@DCU03_IDoorPbClose, col3031) ,col3032=ISNULL(@DCU03_IDoorPbOpenIn, col3032) ,col3033=ISNULL(@DCU03_IDoorPbOpenOut, col3033) ,col3034=ISNULL(@DCU03_IDoorReleased, col3034) 
,col3035=ISNULL(@DCU03_IFootStepRel, col3035) ,col3036=ISNULL(@DCU03_IForcedClosDoor, col3036) ,col3037=ISNULL(@DCU03_ILeafsStopDoor, col3037) ,col3038=ISNULL(@DCU03_IObstacleDoor, col3038) ,col3039=ISNULL(@DCU03_IObstacleStep, col3039) 
,col3040=ISNULL(@DCU03_IOpenAssistDoor, col3040) ,col3041=ISNULL(@DCU03_IStandstillBack, col3041) ,col3042=ISNULL(@DCU04_CDoorCloseConduc, col3042) ,col3043=ISNULL(@DCU04_IDoorClosedSafe, col3043) ,col3044=ISNULL(@DCU04_IDoorClRelease, col3044) 
,col3045=ISNULL(@DCU04_IDoorPbClose, col3045) ,col3046=ISNULL(@DCU04_IDoorPbOpenIn, col3046) ,col3047=ISNULL(@DCU04_IDoorPbOpenOut, col3047) ,col3048=ISNULL(@DCU04_IDoorReleased, col3048) ,col3049=ISNULL(@DCU04_IFootStepRel, col3049) 
,col3050=ISNULL(@DCU04_IForcedClosDoor, col3050) ,col3051=ISNULL(@DCU04_ILeafsStopDoor, col3051) ,col3052=ISNULL(@DCU04_IObstacleDoor, col3052) ,col3053=ISNULL(@DCU04_IObstacleStep, col3053) ,col3054=ISNULL(@DCU04_IOpenAssistDoor, col3054) 
,col3055=ISNULL(@DCU04_IStandstillBack, col3055) ,col3056=ISNULL(@DCU05_CDoorCloseConduc, col3056) ,col3057=ISNULL(@DCU05_IDoorClosedSafe, col3057) ,col3058=ISNULL(@DCU05_IDoorClRelease, col3058) ,col3059=ISNULL(@DCU05_IDoorPbClose, col3059) 
,col3060=ISNULL(@DCU05_IDoorPbOpenIn, col3060) ,col3061=ISNULL(@DCU05_IDoorPbOpenOut, col3061) ,col3062=ISNULL(@DCU05_IDoorReleased, col3062) ,col3063=ISNULL(@DCU05_IFootStepRel, col3063) ,col3064=ISNULL(@DCU05_IForcedClosDoor, col3064) 
,col3065=ISNULL(@DCU05_ILeafsStopDoor, col3065) ,col3066=ISNULL(@DCU05_IObstacleDoor, col3066) ,col3067=ISNULL(@DCU05_IObstacleStep, col3067) ,col3068=ISNULL(@DCU05_IOpenAssistDoor, col3068) ,col3069=ISNULL(@DCU05_IStandstillBack, col3069) 
,col3070=ISNULL(@DCU06_CDoorCloseConduc, col3070) ,col3071=ISNULL(@DCU06_IDoorClosedSafe, col3071) ,col3072=ISNULL(@DCU06_IDoorClRelease, col3072) ,col3073=ISNULL(@DCU06_IDoorPbClose, col3073) ,col3074=ISNULL(@DCU06_IDoorPbOpenIn, col3074) 
,col3075=ISNULL(@DCU06_IDoorPbOpenOut, col3075) ,col3076=ISNULL(@DCU06_IDoorReleased, col3076) ,col3077=ISNULL(@DCU06_IFootStepRel, col3077) ,col3078=ISNULL(@DCU06_IForcedClosDoor, col3078) ,col3079=ISNULL(@DCU06_ILeafsStopDoor, col3079) 
,col3080=ISNULL(@DCU06_IObstacleDoor, col3080) ,col3081=ISNULL(@DCU06_IObstacleStep, col3081) ,col3082=ISNULL(@DCU06_IOpenAssistDoor, col3082) ,col3083=ISNULL(@DCU06_IStandstillBack, col3083) ,col3084=ISNULL(@DCU07_CDoorCloseConduc, col3084) 
,col3085=ISNULL(@DCU07_IDoorClosedSafe, col3085) ,col3086=ISNULL(@DCU07_IDoorClRelease, col3086) ,col3087=ISNULL(@DCU07_IDoorPbClose, col3087) ,col3088=ISNULL(@DCU07_IDoorPbOpenIn, col3088) ,col3089=ISNULL(@DCU07_IDoorPbOpenOut, col3089) 
,col3090=ISNULL(@DCU07_IDoorReleased, col3090) ,col3091=ISNULL(@DCU07_IFootStepRel, col3091) ,col3092=ISNULL(@DCU07_IForcedClosDoor, col3092) ,col3093=ISNULL(@DCU07_ILeafsStopDoor, col3093) ,col3094=ISNULL(@DCU07_IObstacleDoor, col3094) 
,col3095=ISNULL(@DCU07_IObstacleStep, col3095) ,col3096=ISNULL(@DCU07_IOpenAssistDoor, col3096) ,col3097=ISNULL(@DCU07_IStandstillBack, col3097) ,col3098=ISNULL(@DCU08_CDoorCloseConduc, col3098) ,col3099=ISNULL(@DCU08_IDoorClosedSafe, col3099) 
,col3100=ISNULL(@DCU08_IDoorClRelease, col3100) ,col3101=ISNULL(@DCU08_IDoorPbClose, col3101) ,col3102=ISNULL(@DCU08_IDoorPbOpenIn, col3102) ,col3103=ISNULL(@DCU08_IDoorPbOpenOut, col3103) ,col3104=ISNULL(@DCU08_IDoorReleased, col3104) 
,col3105=ISNULL(@DCU08_IFootStepRel, col3105) ,col3106=ISNULL(@DCU08_IForcedClosDoor, col3106) ,col3107=ISNULL(@DCU08_ILeafsStopDoor, col3107) ,col3108=ISNULL(@DCU08_IObstacleDoor, col3108) ,col3109=ISNULL(@DCU08_IObstacleStep, col3109) 
,col3110=ISNULL(@DCU08_IOpenAssistDoor, col3110) ,col3111=ISNULL(@DCU08_IStandstillBack, col3111) ,col3112=ISNULL(@DCU09_CDoorCloseConduc, col3112) ,col3113=ISNULL(@DCU09_IDoorClosedSafe, col3113) ,col3114=ISNULL(@DCU09_IDoorClRelease, col3114) 
,col3115=ISNULL(@DCU09_IDoorPbClose, col3115) ,col3116=ISNULL(@DCU09_IDoorPbOpenIn, col3116) ,col3117=ISNULL(@DCU09_IDoorPbOpenOut, col3117) ,col3118=ISNULL(@DCU09_IDoorReleased, col3118) ,col3119=ISNULL(@DCU09_IFootStepRel, col3119) 
,col3120=ISNULL(@DCU09_IForcedClosDoor, col3120) ,col3121=ISNULL(@DCU09_ILeafsStopDoor, col3121) ,col3122=ISNULL(@DCU09_IObstacleDoor, col3122) ,col3123=ISNULL(@DCU09_IObstacleStep, col3123) ,col3124=ISNULL(@DCU09_IOpenAssistDoor, col3124) 
,col3125=ISNULL(@DCU09_IStandstillBack, col3125) ,col3126=ISNULL(@DCU10_CDoorCloseConduc, col3126) ,col3127=ISNULL(@DCU10_IDoorClosedSafe, col3127) ,col3128=ISNULL(@DCU10_IDoorClRelease, col3128) ,col3129=ISNULL(@DCU10_IDoorPbClose, col3129) 
,col3130=ISNULL(@DCU10_IDoorPbOpenIn, col3130) ,col3131=ISNULL(@DCU10_IDoorPbOpenOut, col3131) ,col3132=ISNULL(@DCU10_IDoorReleased, col3132) ,col3133=ISNULL(@DCU10_IFootStepRel, col3133) ,col3134=ISNULL(@DCU10_IForcedClosDoor, col3134) 
,col3135=ISNULL(@DCU10_ILeafsStopDoor, col3135) ,col3136=ISNULL(@DCU10_IObstacleDoor, col3136) ,col3137=ISNULL(@DCU10_IObstacleStep, col3137) ,col3138=ISNULL(@DCU10_IOpenAssistDoor, col3138) ,col3139=ISNULL(@DCU10_IStandstillBack, col3139) 
,col3140=ISNULL(@DCU11_CDoorCloseConduc, col3140) ,col3141=ISNULL(@DCU11_IDoorClosedSafe, col3141) ,col3142=ISNULL(@DCU11_IDoorClRelease, col3142) ,col3143=ISNULL(@DCU11_IDoorPbClose, col3143) ,col3144=ISNULL(@DCU11_IDoorPbOpenIn, col3144) 
,col3145=ISNULL(@DCU11_IDoorPbOpenOut, col3145) ,col3146=ISNULL(@DCU11_IDoorReleased, col3146) ,col3147=ISNULL(@DCU11_IFootStepRel, col3147) ,col3148=ISNULL(@DCU11_IForcedClosDoor, col3148) ,col3149=ISNULL(@DCU11_ILeafsStopDoor, col3149) 
,col3150=ISNULL(@DCU11_IObstacleDoor, col3150) ,col3151=ISNULL(@DCU11_IObstacleStep, col3151) ,col3152=ISNULL(@DCU11_IOpenAssistDoor, col3152) ,col3153=ISNULL(@DCU11_IStandstillBack, col3153) ,col3154=ISNULL(@DCU12_CDoorCloseConduc, col3154) 
,col3155=ISNULL(@DCU12_IDoorClosedSafe, col3155) ,col3156=ISNULL(@DCU12_IDoorClRelease, col3156) ,col3157=ISNULL(@DCU12_IDoorPbClose, col3157) ,col3158=ISNULL(@DCU12_IDoorPbOpenIn, col3158) ,col3159=ISNULL(@DCU12_IDoorPbOpenOut, col3159) 
,col3160=ISNULL(@DCU12_IDoorReleased, col3160) ,col3161=ISNULL(@DCU12_IFootStepRel, col3161) ,col3162=ISNULL(@DCU12_IForcedClosDoor, col3162) ,col3163=ISNULL(@DCU12_ILeafsStopDoor, col3163) ,col3164=ISNULL(@DCU12_IObstacleDoor, col3164) 
,col3165=ISNULL(@DCU12_IObstacleStep, col3165) ,col3166=ISNULL(@DCU12_IOpenAssistDoor, col3166) ,col3167=ISNULL(@DCU12_IStandstillBack, col3167) ,col3168=ISNULL(@DCU13_CDoorCloseConduc, col3168) ,col3169=ISNULL(@DCU13_IDoorClosedSafe, col3169) 
,col3170=ISNULL(@DCU13_IDoorClRelease, col3170) ,col3171=ISNULL(@DCU13_IDoorPbClose, col3171) ,col3172=ISNULL(@DCU13_IDoorPbOpenIn, col3172) ,col3173=ISNULL(@DCU13_IDoorPbOpenOut, col3173) ,col3174=ISNULL(@DCU13_IDoorReleased, col3174) 
,col3175=ISNULL(@DCU13_IFootStepRel, col3175) ,col3176=ISNULL(@DCU13_IForcedClosDoor, col3176) ,col3177=ISNULL(@DCU13_ILeafsStopDoor, col3177) ,col3178=ISNULL(@DCU13_IObstacleDoor, col3178) ,col3179=ISNULL(@DCU13_IObstacleStep, col3179) 
,col3180=ISNULL(@DCU13_IOpenAssistDoor, col3180) ,col3181=ISNULL(@DCU13_IStandstillBack, col3181) ,col3182=ISNULL(@DCU14_CDoorCloseConduc, col3182) ,col3183=ISNULL(@DCU14_IDoorClosedSafe, col3183) ,col3184=ISNULL(@DCU14_IDoorClRelease, col3184) 
,col3185=ISNULL(@DCU14_IDoorPbClose, col3185) ,col3186=ISNULL(@DCU14_IDoorPbOpenIn, col3186) ,col3187=ISNULL(@DCU14_IDoorPbOpenOut, col3187) ,col3188=ISNULL(@DCU14_IDoorReleased, col3188) ,col3189=ISNULL(@DCU14_IFootStepRel, col3189) 
,col3190=ISNULL(@DCU14_IForcedClosDoor, col3190) ,col3191=ISNULL(@DCU14_ILeafsStopDoor, col3191) ,col3192=ISNULL(@DCU14_IObstacleDoor, col3192) ,col3193=ISNULL(@DCU14_IObstacleStep, col3193) ,col3194=ISNULL(@DCU14_IOpenAssistDoor, col3194) 
,col3195=ISNULL(@DCU14_IStandstillBack, col3195) ,col3196=ISNULL(@DCU15_CDoorCloseConduc, col3196) ,col3197=ISNULL(@DCU15_IDoorClosedSafe, col3197) ,col3198=ISNULL(@DCU15_IDoorClRelease, col3198) ,col3199=ISNULL(@DCU15_IDoorPbClose, col3199) 
,col3200=ISNULL(@DCU15_IDoorPbOpenIn, col3200) ,col3201=ISNULL(@DCU15_IDoorPbOpenOut, col3201) ,col3202=ISNULL(@DCU15_IDoorReleased, col3202) ,col3203=ISNULL(@DCU15_IFootStepRel, col3203) ,col3204=ISNULL(@DCU15_IForcedClosDoor, col3204) 
,col3205=ISNULL(@DCU15_ILeafsStopDoor, col3205) ,col3206=ISNULL(@DCU15_IObstacleDoor, col3206) ,col3207=ISNULL(@DCU15_IObstacleStep, col3207) ,col3208=ISNULL(@DCU15_IOpenAssistDoor, col3208) ,col3209=ISNULL(@DCU15_IStandstillBack, col3209) 
,col3210=ISNULL(@DCU16_CDoorCloseConduc, col3210) ,col3211=ISNULL(@DCU16_IDoorClosedSafe, col3211) ,col3212=ISNULL(@DCU16_IDoorClRelease, col3212) ,col3213=ISNULL(@DCU16_IDoorPbClose, col3213) ,col3214=ISNULL(@DCU16_IDoorPbOpenIn, col3214) 
,col3215=ISNULL(@DCU16_IDoorPbOpenOut, col3215) ,col3216=ISNULL(@DCU16_IDoorReleased, col3216) ,col3217=ISNULL(@DCU16_IFootStepRel, col3217) ,col3218=ISNULL(@DCU16_IForcedClosDoor, col3218) ,col3219=ISNULL(@DCU16_ILeafsStopDoor, col3219) 
,col3220=ISNULL(@DCU16_IObstacleDoor, col3220) ,col3221=ISNULL(@DCU16_IObstacleStep, col3221) ,col3222=ISNULL(@DCU16_IOpenAssistDoor, col3222) ,col3223=ISNULL(@DCU16_IStandstillBack, col3223) ,col3224=ISNULL(@DCU17_CDoorCloseConduc, col3224) 
,col3225=ISNULL(@DCU17_IDoorClosedSafe, col3225) ,col3226=ISNULL(@DCU17_IDoorClRelease, col3226) ,col3227=ISNULL(@DCU17_IDoorPbClose, col3227) ,col3228=ISNULL(@DCU17_IDoorPbOpenIn, col3228) ,col3229=ISNULL(@DCU17_IDoorPbOpenOut, col3229) 
,col3230=ISNULL(@DCU17_IDoorReleased, col3230) ,col3231=ISNULL(@DCU17_IFootStepRel, col3231) ,col3232=ISNULL(@DCU17_IForcedClosDoor, col3232) ,col3233=ISNULL(@DCU17_ILeafsStopDoor, col3233) ,col3234=ISNULL(@DCU17_IObstacleDoor, col3234) 
,col3235=ISNULL(@DCU17_IObstacleStep, col3235) ,col3236=ISNULL(@DCU17_IOpenAssistDoor, col3236) ,col3237=ISNULL(@DCU17_IStandstillBack, col3237) ,col3238=ISNULL(@DCU18_CDoorCloseConduc, col3238) ,col3239=ISNULL(@DCU18_IDoorClosedSafe, col3239) 
,col3240=ISNULL(@DCU18_IDoorClRelease, col3240) ,col3241=ISNULL(@DCU18_IDoorPbClose, col3241) ,col3242=ISNULL(@DCU18_IDoorPbOpenIn, col3242) ,col3243=ISNULL(@DCU18_IDoorPbOpenOut, col3243) ,col3244=ISNULL(@DCU18_IDoorReleased, col3244) 
,col3245=ISNULL(@DCU18_IFootStepRel, col3245) ,col3246=ISNULL(@DCU18_IForcedClosDoor, col3246) ,col3247=ISNULL(@DCU18_ILeafsStopDoor, col3247) ,col3248=ISNULL(@DCU18_IObstacleDoor, col3248) ,col3249=ISNULL(@DCU18_IObstacleStep, col3249) 
,col3250=ISNULL(@DCU18_IOpenAssistDoor, col3250) ,col3251=ISNULL(@DCU18_IStandstillBack, col3251) ,col3252=ISNULL(@DCU19_CDoorCloseConduc, col3252) ,col3253=ISNULL(@DCU19_IDoorClosedSafe, col3253) ,col3254=ISNULL(@DCU19_IDoorClRelease, col3254) 
,col3255=ISNULL(@DCU19_IDoorPbClose, col3255) ,col3256=ISNULL(@DCU19_IDoorPbOpenIn, col3256) ,col3257=ISNULL(@DCU19_IDoorPbOpenOut, col3257) ,col3258=ISNULL(@DCU19_IDoorReleased, col3258) ,col3259=ISNULL(@DCU19_IFootStepRel, col3259) 
,col3260=ISNULL(@DCU19_IForcedClosDoor, col3260) ,col3261=ISNULL(@DCU19_ILeafsStopDoor, col3261) ,col3262=ISNULL(@DCU19_IObstacleDoor, col3262) ,col3263=ISNULL(@DCU19_IObstacleStep, col3263) ,col3264=ISNULL(@DCU19_IOpenAssistDoor, col3264) 
,col3265=ISNULL(@DCU19_IStandstillBack, col3265) ,col3266=ISNULL(@DCU20_CDoorCloseConduc, col3266) ,col3267=ISNULL(@DCU20_IDoorClosedSafe, col3267) ,col3268=ISNULL(@DCU20_IDoorClRelease, col3268) ,col3269=ISNULL(@DCU20_IDoorPbClose, col3269) 
,col3270=ISNULL(@DCU20_IDoorPbOpenIn, col3270) ,col3271=ISNULL(@DCU20_IDoorPbOpenOut, col3271) ,col3272=ISNULL(@DCU20_IDoorReleased, col3272) ,col3273=ISNULL(@DCU20_IFootStepRel, col3273) ,col3274=ISNULL(@DCU20_IForcedClosDoor, col3274) 
,col3275=ISNULL(@DCU20_ILeafsStopDoor, col3275) ,col3276=ISNULL(@DCU20_IObstacleDoor, col3276) ,col3277=ISNULL(@DCU20_IObstacleStep, col3277) ,col3278=ISNULL(@DCU20_IOpenAssistDoor, col3278) ,col3279=ISNULL(@DCU20_IStandstillBack, col3279) 
,col3280=ISNULL(@DRS_CDisDoorRelease_4_1, col3280) ,col3281=ISNULL(@DRS_CDisDoorRelease_4_10, col3281) ,col3282=ISNULL(@DRS_CDisDoorRelease_4_11, col3282) ,col3283=ISNULL(@DRS_CDisDoorRelease_4_12, col3283) ,col3284=ISNULL(@DRS_CDisDoorRelease_4_13, col3284) 
,col3285=ISNULL(@DRS_CDisDoorRelease_4_14, col3285) ,col3286=ISNULL(@DRS_CDisDoorRelease_4_15, col3286) ,col3287=ISNULL(@DRS_CDisDoorRelease_4_16, col3287) ,col3288=ISNULL(@DRS_CDisDoorRelease_4_17, col3288) ,col3289=ISNULL(@DRS_CDisDoorRelease_4_18, col3289) 
,col3290=ISNULL(@DRS_CDisDoorRelease_4_19, col3290) ,col3291=ISNULL(@DRS_CDisDoorRelease_4_2, col3291) ,col3292=ISNULL(@DRS_CDisDoorRelease_4_20, col3292) ,col3293=ISNULL(@DRS_CDisDoorRelease_4_3, col3293) ,col3294=ISNULL(@DRS_CDisDoorRelease_4_4, col3294) 
,col3295=ISNULL(@DRS_CDisDoorRelease_4_5, col3295) ,col3296=ISNULL(@DRS_CDisDoorRelease_4_6, col3296) ,col3297=ISNULL(@DRS_CDisDoorRelease_4_7, col3297) ,col3298=ISNULL(@DRS_CDisDoorRelease_4_8, col3298) ,col3299=ISNULL(@DRS_CDisDoorRelease_4_9, col3299) 
,col3300=ISNULL(@DRS_CDoorClosAutDis_4_1, col3300) ,col3301=ISNULL(@DRS_CDoorClosAutDis_4_10, col3301) ,col3302=ISNULL(@DRS_CDoorClosAutDis_4_11, col3302) ,col3303=ISNULL(@DRS_CDoorClosAutDis_4_12, col3303) ,col3304=ISNULL(@DRS_CDoorClosAutDis_4_13, col3304) 
,col3305=ISNULL(@DRS_CDoorClosAutDis_4_14, col3305) ,col3306=ISNULL(@DRS_CDoorClosAutDis_4_15, col3306) ,col3307=ISNULL(@DRS_CDoorClosAutDis_4_16, col3307) ,col3308=ISNULL(@DRS_CDoorClosAutDis_4_17, col3308) ,col3309=ISNULL(@DRS_CDoorClosAutDis_4_18, col3309) 
,col3310=ISNULL(@DRS_CDoorClosAutDis_4_19, col3310) ,col3311=ISNULL(@DRS_CDoorClosAutDis_4_2, col3311) ,col3312=ISNULL(@DRS_CDoorClosAutDis_4_20, col3312) ,col3313=ISNULL(@DRS_CDoorClosAutDis_4_3, col3313) ,col3314=ISNULL(@DRS_CDoorClosAutDis_4_4, col3314) 
,col3315=ISNULL(@DRS_CDoorClosAutDis_4_5, col3315) ,col3316=ISNULL(@DRS_CDoorClosAutDis_4_6, col3316) ,col3317=ISNULL(@DRS_CDoorClosAutDis_4_7, col3317) ,col3318=ISNULL(@DRS_CDoorClosAutDis_4_8, col3318) ,col3319=ISNULL(@DRS_CDoorClosAutDis_4_9, col3319) 
,col3320=ISNULL(@DRS_CForcedClosDoor_4_1, col3320) ,col3321=ISNULL(@DRS_CForcedClosDoor_4_10, col3321) ,col3322=ISNULL(@DRS_CForcedClosDoor_4_11, col3322) ,col3323=ISNULL(@DRS_CForcedClosDoor_4_12, col3323) ,col3324=ISNULL(@DRS_CForcedClosDoor_4_13, col3324) 
,col3325=ISNULL(@DRS_CForcedClosDoor_4_14, col3325) ,col3326=ISNULL(@DRS_CForcedClosDoor_4_15, col3326) ,col3327=ISNULL(@DRS_CForcedClosDoor_4_16, col3327) ,col3328=ISNULL(@DRS_CForcedClosDoor_4_17, col3328) ,col3329=ISNULL(@DRS_CForcedClosDoor_4_18, col3329) 
,col3330=ISNULL(@DRS_CForcedClosDoor_4_19, col3330) ,col3331=ISNULL(@DRS_CForcedClosDoor_4_2, col3331) ,col3332=ISNULL(@DRS_CForcedClosDoor_4_20, col3332) ,col3333=ISNULL(@DRS_CForcedClosDoor_4_3, col3333) ,col3334=ISNULL(@DRS_CForcedClosDoor_4_4, col3334) 
,col3335=ISNULL(@DRS_CForcedClosDoor_4_5, col3335) ,col3336=ISNULL(@DRS_CForcedClosDoor_4_6, col3336) ,col3337=ISNULL(@DRS_CForcedClosDoor_4_7, col3337) ,col3338=ISNULL(@DRS_CForcedClosDoor_4_8, col3338) ,col3339=ISNULL(@DRS_CForcedClosDoor_4_9, col3339) 
,col3340=ISNULL(@DRS_CTestModeReq_M_4_1, col3340) ,col3341=ISNULL(@DRS_CTestModeReq_M_4_10, col3341) ,col3342=ISNULL(@DRS_CTestModeReq_M_4_11, col3342) ,col3343=ISNULL(@DRS_CTestModeReq_M_4_12, col3343) ,col3344=ISNULL(@DRS_CTestModeReq_M_4_13, col3344) 
,col3345=ISNULL(@DRS_CTestModeReq_M_4_14, col3345) ,col3346=ISNULL(@DRS_CTestModeReq_M_4_15, col3346) ,col3347=ISNULL(@DRS_CTestModeReq_M_4_16, col3347) ,col3348=ISNULL(@DRS_CTestModeReq_M_4_17, col3348) ,col3349=ISNULL(@DRS_CTestModeReq_M_4_18, col3349) 
,col3350=ISNULL(@DRS_CTestModeReq_M_4_19, col3350) ,col3351=ISNULL(@DRS_CTestModeReq_M_4_2, col3351) ,col3352=ISNULL(@DRS_CTestModeReq_M_4_20, col3352) ,col3353=ISNULL(@DRS_CTestModeReq_M_4_3, col3353) ,col3354=ISNULL(@DRS_CTestModeReq_M_4_4, col3354) 
,col3355=ISNULL(@DRS_CTestModeReq_M_4_5, col3355) ,col3356=ISNULL(@DRS_CTestModeReq_M_4_6, col3356) ,col3357=ISNULL(@DRS_CTestModeReq_M_4_7, col3357) ,col3358=ISNULL(@DRS_CTestModeReq_M_4_8, col3358) ,col3359=ISNULL(@DRS_CTestModeReq_M_4_9, col3359) 
,col3360=ISNULL(@MIO_I1DoorRelLe_4_1, col3360) ,col3361=ISNULL(@MIO_I1DoorRelLe_4_10, col3361) ,col3362=ISNULL(@MIO_I1DoorRelLe_4_11, col3362) ,col3363=ISNULL(@MIO_I1DoorRelLe_4_12, col3363) ,col3364=ISNULL(@MIO_I1DoorRelLe_4_13, col3364) 
,col3365=ISNULL(@MIO_I1DoorRelLe_4_14, col3365) ,col3366=ISNULL(@MIO_I1DoorRelLe_4_15, col3366) ,col3367=ISNULL(@MIO_I1DoorRelLe_4_16, col3367) ,col3368=ISNULL(@MIO_I1DoorRelLe_4_17, col3368) ,col3369=ISNULL(@MIO_I1DoorRelLe_4_18, col3369) 
,col3370=ISNULL(@MIO_I1DoorRelLe_4_19, col3370) ,col3371=ISNULL(@MIO_I1DoorRelLe_4_2, col3371) ,col3372=ISNULL(@MIO_I1DoorRelLe_4_20, col3372) ,col3373=ISNULL(@MIO_I1DoorRelLe_4_3, col3373) ,col3374=ISNULL(@MIO_I1DoorRelLe_4_4, col3374) 
,col3375=ISNULL(@MIO_I1DoorRelLe_4_5, col3375) ,col3376=ISNULL(@MIO_I1DoorRelLe_4_6, col3376) ,col3377=ISNULL(@MIO_I1DoorRelLe_4_7, col3377) ,col3378=ISNULL(@MIO_I1DoorRelLe_4_8, col3378) ,col3379=ISNULL(@MIO_I1DoorRelLe_4_9, col3379) 
,col3380=ISNULL(@MIO_I1DoorRelRi_4_1, col3380) ,col3381=ISNULL(@MIO_I1DoorRelRi_4_10, col3381) ,col3382=ISNULL(@MIO_I1DoorRelRi_4_11, col3382) ,col3383=ISNULL(@MIO_I1DoorRelRi_4_12, col3383) ,col3384=ISNULL(@MIO_I1DoorRelRi_4_13, col3384) 
,col3385=ISNULL(@MIO_I1DoorRelRi_4_14, col3385) ,col3386=ISNULL(@MIO_I1DoorRelRi_4_15, col3386) ,col3387=ISNULL(@MIO_I1DoorRelRi_4_16, col3387) ,col3388=ISNULL(@MIO_I1DoorRelRi_4_17, col3388) ,col3389=ISNULL(@MIO_I1DoorRelRi_4_18, col3389) 
,col3390=ISNULL(@MIO_I1DoorRelRi_4_19, col3390) ,col3391=ISNULL(@MIO_I1DoorRelRi_4_2, col3391) ,col3392=ISNULL(@MIO_I1DoorRelRi_4_20, col3392) ,col3393=ISNULL(@MIO_I1DoorRelRi_4_3, col3393) ,col3394=ISNULL(@MIO_I1DoorRelRi_4_4, col3394) 
,col3395=ISNULL(@MIO_I1DoorRelRi_4_5, col3395) ,col3396=ISNULL(@MIO_I1DoorRelRi_4_6, col3396) ,col3397=ISNULL(@MIO_I1DoorRelRi_4_7, col3397) ,col3398=ISNULL(@MIO_I1DoorRelRi_4_8, col3398) ,col3399=ISNULL(@MIO_I1DoorRelRi_4_9, col3399) 
,col3400=ISNULL(@MIO_I2DoorRelLe_4_1, col3400) ,col3401=ISNULL(@MIO_I2DoorRelLe_4_10, col3401) ,col3402=ISNULL(@MIO_I2DoorRelLe_4_11, col3402) ,col3403=ISNULL(@MIO_I2DoorRelLe_4_12, col3403) ,col3404=ISNULL(@MIO_I2DoorRelLe_4_13, col3404) 
,col3405=ISNULL(@MIO_I2DoorRelLe_4_14, col3405) ,col3406=ISNULL(@MIO_I2DoorRelLe_4_15, col3406) ,col3407=ISNULL(@MIO_I2DoorRelLe_4_16, col3407) ,col3408=ISNULL(@MIO_I2DoorRelLe_4_17, col3408) ,col3409=ISNULL(@MIO_I2DoorRelLe_4_18, col3409) 
,col3410=ISNULL(@MIO_I2DoorRelLe_4_19, col3410) ,col3411=ISNULL(@MIO_I2DoorRelLe_4_2, col3411) ,col3412=ISNULL(@MIO_I2DoorRelLe_4_20, col3412) ,col3413=ISNULL(@MIO_I2DoorRelLe_4_3, col3413) ,col3414=ISNULL(@MIO_I2DoorRelLe_4_4, col3414) 
,col3415=ISNULL(@MIO_I2DoorRelLe_4_5, col3415) ,col3416=ISNULL(@MIO_I2DoorRelLe_4_6, col3416) ,col3417=ISNULL(@MIO_I2DoorRelLe_4_7, col3417) ,col3418=ISNULL(@MIO_I2DoorRelLe_4_8, col3418) ,col3419=ISNULL(@MIO_I2DoorRelLe_4_9, col3419) 
,col3420=ISNULL(@MIO_I2DoorRelRi_4_1, col3420) ,col3421=ISNULL(@MIO_I2DoorRelRi_4_10, col3421) ,col3422=ISNULL(@MIO_I2DoorRelRi_4_11, col3422) ,col3423=ISNULL(@MIO_I2DoorRelRi_4_12, col3423) ,col3424=ISNULL(@MIO_I2DoorRelRi_4_13, col3424) 
,col3425=ISNULL(@MIO_I2DoorRelRi_4_14, col3425) ,col3426=ISNULL(@MIO_I2DoorRelRi_4_15, col3426) ,col3427=ISNULL(@MIO_I2DoorRelRi_4_16, col3427) ,col3428=ISNULL(@MIO_I2DoorRelRi_4_17, col3428) ,col3429=ISNULL(@MIO_I2DoorRelRi_4_18, col3429) 
,col3430=ISNULL(@MIO_I2DoorRelRi_4_19, col3430) ,col3431=ISNULL(@MIO_I2DoorRelRi_4_2, col3431) ,col3432=ISNULL(@MIO_I2DoorRelRi_4_20, col3432) ,col3433=ISNULL(@MIO_I2DoorRelRi_4_3, col3433) ,col3434=ISNULL(@MIO_I2DoorRelRi_4_4, col3434) 
,col3435=ISNULL(@MIO_I2DoorRelRi_4_5, col3435) ,col3436=ISNULL(@MIO_I2DoorRelRi_4_6, col3436) ,col3437=ISNULL(@MIO_I2DoorRelRi_4_7, col3437) ,col3438=ISNULL(@MIO_I2DoorRelRi_4_8, col3438) ,col3439=ISNULL(@MIO_I2DoorRelRi_4_9, col3439) 
,col3440=ISNULL(@MIO_IDoorLoopBypa1_4_1, col3440) ,col3441=ISNULL(@MIO_IDoorLoopBypa1_4_10, col3441) ,col3442=ISNULL(@MIO_IDoorLoopBypa1_4_11, col3442) ,col3443=ISNULL(@MIO_IDoorLoopBypa1_4_12, col3443) ,col3444=ISNULL(@MIO_IDoorLoopBypa1_4_13, col3444) 
,col3445=ISNULL(@MIO_IDoorLoopBypa1_4_14, col3445) ,col3446=ISNULL(@MIO_IDoorLoopBypa1_4_15, col3446) ,col3447=ISNULL(@MIO_IDoorLoopBypa1_4_16, col3447) ,col3448=ISNULL(@MIO_IDoorLoopBypa1_4_17, col3448) ,col3449=ISNULL(@MIO_IDoorLoopBypa1_4_18, col3449) 
,col3450=ISNULL(@MIO_IDoorLoopBypa1_4_19, col3450) ,col3451=ISNULL(@MIO_IDoorLoopBypa1_4_2, col3451) ,col3452=ISNULL(@MIO_IDoorLoopBypa1_4_20, col3452) ,col3453=ISNULL(@MIO_IDoorLoopBypa1_4_3, col3453) ,col3454=ISNULL(@MIO_IDoorLoopBypa1_4_4, col3454) 
,col3455=ISNULL(@MIO_IDoorLoopBypa1_4_5, col3455) ,col3456=ISNULL(@MIO_IDoorLoopBypa1_4_6, col3456) ,col3457=ISNULL(@MIO_IDoorLoopBypa1_4_7, col3457) ,col3458=ISNULL(@MIO_IDoorLoopBypa1_4_8, col3458) ,col3459=ISNULL(@MIO_IDoorLoopBypa1_4_9, col3459) 
,col3460=ISNULL(@MIO_IDoorLoopBypa2_4_1, col3460) ,col3461=ISNULL(@MIO_IDoorLoopBypa2_4_10, col3461) ,col3462=ISNULL(@MIO_IDoorLoopBypa2_4_11, col3462) ,col3463=ISNULL(@MIO_IDoorLoopBypa2_4_12, col3463) ,col3464=ISNULL(@MIO_IDoorLoopBypa2_4_13, col3464) 
,col3465=ISNULL(@MIO_IDoorLoopBypa2_4_14, col3465) ,col3466=ISNULL(@MIO_IDoorLoopBypa2_4_15, col3466) ,col3467=ISNULL(@MIO_IDoorLoopBypa2_4_16, col3467) ,col3468=ISNULL(@MIO_IDoorLoopBypa2_4_17, col3468) ,col3469=ISNULL(@MIO_IDoorLoopBypa2_4_18, col3469) 
,col3470=ISNULL(@MIO_IDoorLoopBypa2_4_19, col3470) ,col3471=ISNULL(@MIO_IDoorLoopBypa2_4_2, col3471) ,col3472=ISNULL(@MIO_IDoorLoopBypa2_4_20, col3472) ,col3473=ISNULL(@MIO_IDoorLoopBypa2_4_3, col3473) ,col3474=ISNULL(@MIO_IDoorLoopBypa2_4_4, col3474) 
,col3475=ISNULL(@MIO_IDoorLoopBypa2_4_5, col3475) ,col3476=ISNULL(@MIO_IDoorLoopBypa2_4_6, col3476) ,col3477=ISNULL(@MIO_IDoorLoopBypa2_4_7, col3477) ,col3478=ISNULL(@MIO_IDoorLoopBypa2_4_8, col3478) ,col3479=ISNULL(@MIO_IDoorLoopBypa2_4_9, col3479) 
,col3480=ISNULL(@MIO_IDoorLoopCl1_4_1, col3480) ,col3481=ISNULL(@MIO_IDoorLoopCl1_4_10, col3481) ,col3482=ISNULL(@MIO_IDoorLoopCl1_4_11, col3482) ,col3483=ISNULL(@MIO_IDoorLoopCl1_4_12, col3483) ,col3484=ISNULL(@MIO_IDoorLoopCl1_4_13, col3484) 
,col3485=ISNULL(@MIO_IDoorLoopCl1_4_14, col3485) ,col3486=ISNULL(@MIO_IDoorLoopCl1_4_15, col3486) ,col3487=ISNULL(@MIO_IDoorLoopCl1_4_16, col3487) ,col3488=ISNULL(@MIO_IDoorLoopCl1_4_17, col3488) ,col3489=ISNULL(@MIO_IDoorLoopCl1_4_18, col3489) 
,col3490=ISNULL(@MIO_IDoorLoopCl1_4_19, col3490) ,col3491=ISNULL(@MIO_IDoorLoopCl1_4_2, col3491) ,col3492=ISNULL(@MIO_IDoorLoopCl1_4_20, col3492) ,col3493=ISNULL(@MIO_IDoorLoopCl1_4_3, col3493) ,col3494=ISNULL(@MIO_IDoorLoopCl1_4_4, col3494) 
,col3495=ISNULL(@MIO_IDoorLoopCl1_4_5, col3495) ,col3496=ISNULL(@MIO_IDoorLoopCl1_4_6, col3496) ,col3497=ISNULL(@MIO_IDoorLoopCl1_4_7, col3497) ,col3498=ISNULL(@MIO_IDoorLoopCl1_4_8, col3498) ,col3499=ISNULL(@MIO_IDoorLoopCl1_4_9, col3499) 
,col3500=ISNULL(@MIO_IDoorLoopCl2_4_1, col3500) ,col3501=ISNULL(@MIO_IDoorLoopCl2_4_10, col3501) ,col3502=ISNULL(@MIO_IDoorLoopCl2_4_11, col3502) ,col3503=ISNULL(@MIO_IDoorLoopCl2_4_12, col3503) ,col3504=ISNULL(@MIO_IDoorLoopCl2_4_13, col3504) 
,col3505=ISNULL(@MIO_IDoorLoopCl2_4_14, col3505) ,col3506=ISNULL(@MIO_IDoorLoopCl2_4_15, col3506) ,col3507=ISNULL(@MIO_IDoorLoopCl2_4_16, col3507) ,col3508=ISNULL(@MIO_IDoorLoopCl2_4_17, col3508) ,col3509=ISNULL(@MIO_IDoorLoopCl2_4_18, col3509) 
,col3510=ISNULL(@MIO_IDoorLoopCl2_4_19, col3510) ,col3511=ISNULL(@MIO_IDoorLoopCl2_4_2, col3511) ,col3512=ISNULL(@MIO_IDoorLoopCl2_4_20, col3512) ,col3513=ISNULL(@MIO_IDoorLoopCl2_4_3, col3513) ,col3514=ISNULL(@MIO_IDoorLoopCl2_4_4, col3514) 
,col3515=ISNULL(@MIO_IDoorLoopCl2_4_5, col3515) ,col3516=ISNULL(@MIO_IDoorLoopCl2_4_6, col3516) ,col3517=ISNULL(@MIO_IDoorLoopCl2_4_7, col3517) ,col3518=ISNULL(@MIO_IDoorLoopCl2_4_8, col3518) ,col3519=ISNULL(@MIO_IDoorLoopCl2_4_9, col3519) 

Where Id = @ID

END
END
GO

--------------------------------------
-- Update NSP_InsertFaultInterface with new columns
--------------------------------------

RAISERROR ('-- Update NSP_InsertFaultInterface', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_InsertFaultInterface' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultInterface AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultInterface]
    @FaultCode varchar(100)
    , @TimeCreate datetime
    , @TimeEnd datetime = NULL
    , @FaultUnitNumber varchar(16)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
	DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
	DECLARE @faultCount int

	DECLARE @FaultUnitID int
	IF (SELECT COUNT(*) FROM dbo.Unit WHERE UnitNumber = @FaultUnitNumber) = 1
    BEGIN
        SELECT
			@FaultUnitID = ID 
        FROM dbo.Unit 
        WHERE UnitNumber = @FaultUnitNumber
    END
    ELSE
    BEGIN
        RAISERROR ('UnitNumber %s does not exist',1,1, @FaultUnitNumber)
        RETURN -5
    END


    IF (SELECT COUNT(*) FROM dbo.FaultMeta WHERE FaultCode = @FaultCode) = 1
    BEGIN
        SELECT
			@faultMetaID = ID 
			, @faultMetaRecovery = RecoveryProcessPath
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Col1
        , @faultLng = Col2
		, @faultLocationID = l.ID
        FROM dbo.ChannelValue
		LEFT JOIN dbo.Location l ON l.ID = (
			SELECT TOP 1 LocationID 
			FROM LocationArea 
			WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
				AND Col2 BETWEEN MinLongitude AND MaxLongitude
			ORDER BY Priority
		) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, GETDATE()))  -- in case there is a time difference between app and db server
        
		 -- get current count of faults
		SELECT TOP 1 
			@faultCount = FaultCounter 
			FROM FaultCount fc 
			WHERE fc.UnitID = @faultUnitID 
			AND fc.FaultMetaID = @faultMetaID

		-- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
		IF @faultCount IS NULL
		BEGIN
			INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
			VALUES (@faultUnitID, @faultMetaID, 0)

			SET @faultCount = 0
		END
		
		SET @faultCount = @faultCount + 1    
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO [Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[IsDelayed]
			,[CountSinceLastMaint]
			,[RecoveryStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,ISNULL(@IsDelayed, 0)
			,@faultCount
            ,RecoveryStatus = CASE
				WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
				ELSE NULL
			END

		-- increment the fault counter
		UPDATE dbo.[FaultCount]
		SET FaultCounter = @faultCount
		WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
	
	--insert Channel data
	INSERT INTO dbo.[FaultChannelValue]
	(
	        [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
		 ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
			)
		SELECT
		[ID]
		,@faultID
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		  ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
		[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
		[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
		[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
		[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
		[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
		[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
		[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
		[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
		[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
		[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
		[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
		[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
		[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
		[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
		[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
		[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
		[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
		[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
		[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
		[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
		[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
		[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
		[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
		[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
		[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
		[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
		[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
		[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
		[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
		[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
		[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
		[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
		[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
		[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
		[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
		[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
		[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
		[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
		[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
		[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
		[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]

	FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


	INSERT INTO dbo.[FaultChannelValueDoor]
	(
	        [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
		 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
		)
		SELECT
		[ID]
		,@faultID
		,[UpdateRecord]
		,[UnitID]
		,[RecordInsert]
		,[TimeStamp]
		 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
		

	FROM dbo.ChannelValueDoor
    WHERE UnitID = @FaultUnitID
		AND TimeStamp BETWEEN DATEADD(s, -30, @TimeCreate) AND @TimeCreate


	-- returning faultID
	RETURN @faultID
	
END
GO

----------------------------------------------------------------------------
-- update NSP_FleetSummary with new columns
----------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[NSP_FleetSummary]
AS
	BEGIN

		SET NOCOUNT ON;
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

		-- Get totals for Vehicle based faults
		CREATE TABLE #currentUnitFault (
			UnitID int NOT NULL PRIMARY KEY 
			, P1FaultNumber int NULL
			, P2FaultNumber int NULL
			, P3FaultNumber int NULL
			, P4FaultNumber int NULL
			, P5FaultNumber int NULL
			, RecoveryNumber int NULL
			, NotAcknowledgedNumber int NULL
		)
		
		INSERT INTO #currentUnitFault
		SELECT 
			f.FaultUnitID
			, SUM(CASE f.FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
			, SUM(CASE f.HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
			, SUM(CASE f.IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
			
		FROM dbo.VW_IX_FaultLive f WITH (NOEXPAND)
		WHERE f.FaultUnitID IS NOT NULL
			AND f.ReportingOnly = 0
		GROUP BY f.FaultUnitID

		-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
		CREATE TABLE #LatestChannelValue (
			UnitID int PRIMARY KEY
			, LastTimeStamp datetime2(3)
		)
		INSERT #LatestChannelValue( UnitID , LastTimeStamp)
		SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
		FROM dbo.ChannelValue c
		GROUP BY c.UnitID
		
		
		-- return data
		SELECT 
			fs.UnitID
			, fs.UnitID AS VehicleID
			, fs.UnitNumber
			, fs.UnitType
			, fs.FleetFormationID
			, fs.UnitPosition
			, fs.Headcode
			, fs.Diagram
			, fs.Loading
			, fs.FleetCode
			, Location = l.LocationCode 
			, P1FaultNumber		= ISNULL(vf.P1FaultNumber,0)
			, P2FaultNumber		= ISNULL(vf.P2FaultNumber,0)
			, P3FaultNumber		= ISNULL(vf.P3FaultNumber,0)
			, P4FaultNumber		= ISNULL(vf.P4FaultNumber,0)
			, P5FaultNumber		= ISNULL(vf.P5FaultNumber,0)
			, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
			, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
			, UnitStatus		= CASE fs.UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END
			, VehicleList
			, ServiceStatus		= CASE 
				WHEN fs.Headcode IS NOT NULL
					AND fs.LocationIDHeadcodeStart = l.ID
					THEN 'Ready for Service'
				WHEN fs.Headcode IS NOT NULL
					THEN 'In Service'
				ELSE 'Out of Service'
			END
			,cv.[UpdateRecord]
			,cv.[RecordInsert]
			-- temporary solution for datetime and JTDS driver 
			,CAST(cv.[TimeStamp] AS datetime) AS TimeStamp,
	-- Channel Data 
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
				[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
				[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
				[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
				[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
				[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
				[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
				[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
				[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
				[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
				[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
				[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
				[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
				[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
				[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
				[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
				[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
				[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
				[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
				[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
				[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
				[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
				[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
				[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
				[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
				[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
				[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
				[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
				[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
				[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
				[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
				[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
				[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
				[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
				[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
				[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
				[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
				[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
				[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
				[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
				[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
				[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
				,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
				,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
				,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
				,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
				,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
				,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
				,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
				,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
				,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
				,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
				,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
				,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
				,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
				,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
				,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
				,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
				,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
				,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
				,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
				,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
				,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
				,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
				,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
				,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
				,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
				,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
				,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
				,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
				,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
				,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
				,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
				,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
				,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
				,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
				,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
				,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
				,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
				,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
				,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
				,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
				,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
				,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
				,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
				,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
				,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
				,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
				,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
				,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
				,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
				,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
				,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
				,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
				,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
				,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
				,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
				,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
				,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
				,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
				,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
				,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
				,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
				,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
				,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
				,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
				,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
				,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
				,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
				,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
				,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
				,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
				,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
				,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
				,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
				,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
				,[Col3518],[Col3519]
		FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
		LEFT JOIN #LatestChannelValue vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
		LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
		LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cv.ID = cvd.ID
		LEFT JOIN #currentUnitFault vf ON fs.UnitID = vf.UnitID
		LEFT JOIN dbo.Location l ON l.ID = (
				SELECT TOP 1 LocationID 
				FROM dbo.LocationArea 
				WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
					AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
				ORDER BY Priority
		)

END
GO

----------------------------------------------------------------------------
-- Update NSP_FleetLocation with new columns
----------------------------------------------------------------------------

RAISERROR ('-- Update NSP_FleetLocation', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_FleetLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')     
    EXEC ('CREATE PROCEDURE dbo.NSP_FleetLocation AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_FleetLocation]
AS
/******************************************************************************
**  Name:           NSP_FleetLocation
**  Description:    Returns live channel data for the application
**  Call frequency: Every 5s
**  Parameters:     None
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/
BEGIN
    SET NOCOUNT ON;
    
    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        UnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
        , UnitStatus varchar(20) NULL
    )
    
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
        , SUM(CASE UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END) AS UnitStatus
    FROM dbo.VW_IX_FaultLive WITH (NOEXPAND)
    WHERE FaultUnitID IS NOT NULL
        AND Category <> 'Test'
    GROUP BY FaultUnitID

    -- return data
    SELECT 
        fs.UnitID
        , fs.UnitNumber
        , fs.UnitType
        , fs.FleetFormationID
        , fs.UnitPosition
        , fs.Headcode
        , fs.Diagram
        , fs.Loading
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM dbo.Location
            INNER JOIN dbo.LocationArea ON LocationID = Location.ID
            WHERE 
                (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                    AND Col2 BETWEEN MinLongitude AND MaxLongitude)
            ORDER BY Priority
        )
        , FaultNumber           = ISNULL(vf.FaultNumber,0)
        , WarningNumber     = ISNULL(vf.WarningNumber,0)
        , UnitStatus		=ISNULL(vf.UnitStatus,0)
        , VehicleList
        , ServiceStatus         = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                        SELECT TOP 1 LocationID
                        FROM dbo.LocationArea
                        WHERE 
                            (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                                AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                        ORDER BY Priority
                    )
                    THEN 'Ready for Service'
                WHEN fs.Headcode IS NOT NULL
                    THEN 'In Service'
                ELSE 'Out of Service'
            END
        ,cv.UpdateRecord
        ,cv.RecordInsert
        -- temporary solution for datetime and JTDS driver 
        ,cv.[TimeStamp] AS 'TimeStamp'
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
			[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
			[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
			[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
			[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
			[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
			[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
			[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
			[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
			[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
			[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
			[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
			[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
			[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
			[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
			[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
			[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
			[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
			[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
			[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
			[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
			[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
			[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
			[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
			[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
			[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
			[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
			[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
			[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
			[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
			[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
			[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
			[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
			[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
			[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
			[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
			[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
			[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
			[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
			[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
			[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
			[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
		 ,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
,[Col3518],[Col3519]
        , ServiceStatusEKE      = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                    SELECT TOP 1 LocationID
                    FROM LocationArea
                    WHERE 
                        (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                            AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                    ORDER BY Priority
                )
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END    
    FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
    INNER JOIN dbo.VW_UnitLastChannelValueTimestamp ulc WITH (NOLOCK) ON ulc.UnitID = fs.UnitID
    LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = ulc.LastTimeStamp AND ulc.UnitID = cv.UnitID
    LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.ID = cv.ID
    LEFT JOIN @currentVehicleFault vf ON fs.UnitID = vf.UnitID
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,151
           ,'update_spectrum_db_151.sql'
           ,'1.10.02')
GO