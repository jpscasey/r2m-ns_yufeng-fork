SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 152
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------

RAISERROR ('WARNING!!!!!!!!', 0, 1) WITH NOWAIT
GO
RAISERROR ('SYNONYM THAT POINT AT VIRM have been added here Please Ensure that they are Correctly SET Up', 0, 1) WITH NOWAIT
GO

RAISERROR ('IF Correction need PLEase run NSP_PopulateOtherFaultCategoryMata After to ensure Fault MEta and Category are the same.', 0, 1) WITH NOWAIT

RAISERROR ('WARNING END!!!!!!!!', 0, 1) WITH NOWAIT
GO
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_DeleteFaultCategory_VIRM')
CREATE SYNONYM [dbo].[NSP_DeleteFaultCategory_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_DeleteFaultCategory]
GO

----------------------------------------------------------------------------
----------------------------------------------------------------------------

RAISERROR ('Update NSP_DeleteFaultCategory', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_DeleteFaultCategory] AS BEGIN RETURN(1) END;')
GO

ALTER PROCEDURE [dbo].[NSP_DeleteFaultCategory](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY

		DELETE dbo.FaultCategory 
		WHERE ID = @ID
		
		EXEC [NSP_DeleteFaultCategory_VIRM]
		@ID

		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
	
END

GO
EXEC NSP_PopulateOtherFaultCategoryMeta
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,152
           ,'update_spectrum_db_152.sql'
           ,'1.11.01')
GO