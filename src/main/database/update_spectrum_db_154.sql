SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 154
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

----------------------------------------------------------------------------
-- update script
----------------------------------------------------------------------------

RAISERROR ('WARNING!!!!!!!!', 0, 1) WITH NOWAIT
GO
RAISERROR ('SYNONYM THAT POINT AT VIRM have been added here Please Ensure that they are Correctly SET Up', 0, 1) WITH NOWAIT
GO

RAISERROR ('IF Correction need PLEase run NSP_PopulateOtherFaultCategoryMeta After to ensure Fault MEta and Category are the same.', 0, 1) WITH NOWAIT

RAISERROR ('WARNING END!!!!!!!!', 0, 1) WITH NOWAIT
GO
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms where name = 'NSP_DeleteFaultMeta_VIRM')
CREATE SYNONYM [dbo].[NSP_DeleteFaultMeta_VIRM] FOR [$(db_name_virm)].[dbo].[NSP_DeleteFaultMeta]
GO

----------------------------------------------------------------------------
----------------------------------------------------------------------------

RAISERROR ('Update NSP_DeleteFaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'NSP_DeleteFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE')  
    EXEC ('CREATE PROCEDURE [dbo].[NSP_DeleteFaultMeta] AS BEGIN RETURN(1) END;')
GO

ALTER PROC [dbo].[NSP_DeleteFaultMeta](
	@ID int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRAN
	BEGIN TRY
		
		DELETE FROM dbo.FaultMetaExtraField 
		WHERE FaultMetaId = @ID
	
		DELETE dbo.FaultMeta 
		WHERE ID = @ID
		
		EXEC [NSP_DeleteFaultMeta_VIRM]
		@ID
		
		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		IF ERROR_NUMBER() = 547 -- FOREIGN KEY ERROR
    		BEGIN
    			RAISERROR ('ERR_FAULT_EXIST', 14, 1) WITH NOWAIT
    		END
    	ELSE
    		BEGIN
    			EXEC NSP_RethrowError;
    		END
    		
		ROLLBACK TRAN;
		
	END CATCH
	
END
GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,154
           ,'update_spectrum_db_154.sql'
           ,'1.11.01')
GO