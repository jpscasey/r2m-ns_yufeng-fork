SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 166
SET @scriptType = 'database update'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

    IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

        RAISERROR('******** Script already run ******** ', 20, 1) with log

    ELSE
        BEGIN

            SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
            FROM SchemaChangeLog
            WHERE ScriptType = @scriptType

            IF @scriptNumber <> @DBVersion + 1
                BEGIN
                    SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
                    RAISERROR(@errorMsg , 20, 1) with log
                END
        END
END

GO

--R2M-8278
--Create table for GPS data

RAISERROR ('-- Add ChannelValueGps table', 0, 1) WITH NOWAIT
GO

IF Exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='ChannelValueGps')
    DROP TABLE dbo.ChannelValueGps

CREATE TABLE [dbo].[ChannelValueGps] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Timestamp] datetime NOT NULL,
    [UnitId] int NOT NULL, --FK on unit table id?
    [Latitude] decimal(9,6) NOT NULL,
    [Longitude] decimal(9,6) NOT NULL
	CONSTRAINT [PK_ChannelValueGps] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelValueGps_Unit]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]'))
ALTER TABLE [dbo].[ChannelValueGps]  WITH NOCHECK ADD  CONSTRAINT [FK_ChannelValueGps_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([ID])
GO
------------------------------------------------------------
-- INSERT into SchemaChangeLog
------------------------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('database update'
           ,166
           ,'update_spectrum_db_166.sql'
           ,null)
GO