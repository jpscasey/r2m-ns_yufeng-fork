:setvar scriptNumber 001

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = $(scriptNumber)
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------



INSERT INTO dbo.HardwareType (ID, TypeName)
VALUES (1, 'OTMR1')

INSERT INTO Fleet (Name, Code) VALUES ('VIRM1', 'VIRM1')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM2', 'VIRM2')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM3', 'VIRM3')
INSERT INTO Fleet (Name, Code) VALUES ('VIRM4', 'VIRM4')
INSERT INTO Fleet (Name, Code) VALUES ('HSTNG', 'HSTNG')

INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8608', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8614', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8615', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8621', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8624', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8628', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8629', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8632', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8633', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8635', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8636', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8637', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8638', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8639', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8640', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8641', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8642', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8644', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8645', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8646', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8647', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8648', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8649', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8651', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8652', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8653', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8654', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8655', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8656', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8657', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8658', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8659', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8660', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8661', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8662', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8663', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8664', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8665', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8666', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8667', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8668', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8670', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8671', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8672', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8674', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8675', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8676', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9401', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9402', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9403', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9404', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9405', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9406', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9407', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9409', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9410', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9411', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9412', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9413', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9416', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9417', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9418', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9419', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9420', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9422', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9423', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9425', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9426', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9427', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9430', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9431', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9434', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9443', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9450', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9469', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9473', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9477', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9478', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9479', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9480', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9481', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM1'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8701', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8703', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8705', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8707', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8709', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8711', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8713', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8715', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8717', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8719', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8721', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8723', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9502', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9504', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9506', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9508', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9510', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9512', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9514', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9516', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9518', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9520', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9522', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9524', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9525', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM2'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8726', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8727', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8728', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8729', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8730', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8731', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8732', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8733', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8734', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8735', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8736', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8737', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8738', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8739', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8740', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8741', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8742', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8743', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8744', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8745', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('8746', 'VIRM-VI', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM3'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9547', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9548', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9549', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9550', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9551', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9552', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9553', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9554', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9555', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9556', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9557', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9558', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9559', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9560', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9561', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9562', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9563', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9564', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9565', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9566', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9567', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9568', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9569', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9570', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9571', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9572', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9573', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9574', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9575', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9576', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9577', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9578', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9579', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9580', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9581', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9582', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9583', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9584', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9585', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9586', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9587', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9588', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9589', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9590', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9591', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9592', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9593', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9594', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9595', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9596', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('9597', 'VIRM-IV', 1, (SELECT ID FROM Fleet WHERE Code = 'VIRM4'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10000', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10001', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10002', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10003', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10004', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10005', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10006', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10007', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10008', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10009', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10010', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10011', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10012', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10013', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10014', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10015', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10016', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10017', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10018', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10019', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10020', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10021', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10022', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10023', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10024', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10025', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10026', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10027', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10028', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10029', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10030', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10031', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10032', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10033', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10034', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10035', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10036', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10037', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10038', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10039', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10040', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10041', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10042', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10043', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10044', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10045', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10046', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10047', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10048', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10049', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10050', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10051', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10052', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10053', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10054', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10055', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10056', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10057', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10058', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10059', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10060', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10061', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10062', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10063', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10064', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10065', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10066', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10067', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10068', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10069', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10070', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10071', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10072', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10073', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10074', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10075', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10076', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10077', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10078', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10079', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10080', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10081', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10082', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10083', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10084', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10085', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10086', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10087', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10088', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10089', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10090', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10091', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10092', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10093', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10094', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10095', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10096', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10097', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10098', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10099', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10100', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10101', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10102', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10103', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10104', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10105', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10106', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10107', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10108', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10109', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10110', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10111', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10112', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10113', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10114', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10115', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10116', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10117', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10118', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10119', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10120', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10121', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10122', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10123', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10124', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10125', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10126', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10127', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10128', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10129', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10130', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10131', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10132', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10133', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10134', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10135', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10136', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10137', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10138', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10139', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10140', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10141', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10142', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10143', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10144', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10145', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10146', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10147', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10148', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10149', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10150', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10151', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10152', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10153', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10154', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10155', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10156', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10157', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10158', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10159', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10160', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10161', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10162', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10163', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10164', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10165', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10166', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10167', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10168', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10169', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10170', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10171', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10172', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10173', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10174', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10175', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10176', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10177', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10178', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10179', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10180', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10181', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10182', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10183', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10184', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10185', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10186', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10187', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10188', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10189', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10190', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10191', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10192', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10193', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10194', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10195', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10196', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10197', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10198', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10199', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10200', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10201', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10202', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10203', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10204', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10205', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10206', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10207', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10208', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10209', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10210', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10211', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10212', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10213', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10214', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10215', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10216', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10217', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10218', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10219', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))
INSERT INTO Unit (UnitNumber, UnitType, IsValid, FleetID) VALUES ('10220', 'HSTNG', 1, (SELECT ID FROM Fleet WHERE Code = 'HSTNG'))




INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('DefaultGroup','DefaultGroup',1)
INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('BrakeGroup','BrakeGroup',1)
INSERT INTO dbo.ChannelGroup (ChannelGroupName, Notes, IsVisible)
VALUES ('PressureGroup','PressureGroup',1)


INSERT INTO dbo.ChannelType ( Name ) 
VALUES ('Digital') ,('Analog'),('Binary'), ('Hexadecimal'), ('Calculated')

/*Channels*/
/*TODO*/

INSERT INTO dbo.FleetFormation (UnitIDList,UnitNumberList,ValidFrom,ValidTo)
VALUES('','',GETDATE(),NULL)

INSERT INTO [dbo].[FleetStatus] ([ValidFrom],[ValidTo],[Setcode],[Diagram],[Headcode],[IsValid],[Loading],[ConfigDate],[UnitList],[LocationIDHeadcodeStart],[LocationIDHeadcodeEnd],[UnitID],[UnitPosition],[UnitOrientation],[FleetFormationID])     
SELECT GETDATE(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,ID,1,1,1 FROM Unit



--NCU
SET IDENTITY_INSERT [dbo].[NcuModel] ON 

GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (1, N'NCU3097')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (2, N'NCU3096')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (3, N'NCU3095')
GO
INSERT [dbo].[NcuModel] ([ID], [Model]) VALUES (4, N'NCU3141')
GO
SET IDENTITY_INSERT [dbo].[NcuModel] OFF
GO
SET IDENTITY_INSERT [dbo].[NcuType] ON 

GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (1, N'3097_GA_321_DTC')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (2, N'3095_GA_321_PMS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (3, N'3096_GA_321_ATS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (4, N'3097_GA_321_DTS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (5, N'3097_GA_317_DTOSB')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (6, N'3096_GA_317_TOC')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (7, N'3095_GA_317_PMOS')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (8, N'3097_GA_317_DTOSA')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (9, N'3141_VR_SR2')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (10, N'3141_VR_SR2_B')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (11, N'3141_VR_SR2_C')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (12, N'3141_VR_SR2_D')
GO
INSERT [dbo].[NcuType] ([ID], [Name]) VALUES (13, N'3141_VR_SR2_E')
GO
SET IDENTITY_INSERT [dbo].[NcuType] OFF
GO
SET IDENTITY_INSERT [dbo].[Ncu] ON 

GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (1, 4, 9, N'P47200000 ', N'')
GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (2, 4, 9, N'P47200001 ', NULL)
GO
INSERT [dbo].[Ncu] ([ID], [NcuModelID], [NcuTypeID], [SerialNo], [Comment]) VALUES (40, 4, 12, N'P47200002 ', NULL)
GO
SET IDENTITY_INSERT [dbo].[Ncu] OFF
GO



--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------
INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,$(scriptNumber)
           ,'update_spectrum_db_load_$(scriptNumber).sql'
           ,'1.0.01')
GO