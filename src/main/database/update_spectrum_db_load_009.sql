SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 009
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------

--------------------------------------------
RAISERROR ('-- Add properties to Configuration table', 0, 1) WITH NOWAIT
GO

;MERGE [Configuration] r
USING (
    SELECT [PropertyName] = 'spectrum.merge.resources', [PropertyValue] = 'true'

) AS nr
ON r.PropertyName = nr.PropertyName
WHEN MATCHED THEN 
UPDATE SET
    [PropertyValue]    = nr.[PropertyValue]
WHEN NOT MATCHED THEN
INSERT (
    PropertyName
    , [PropertyValue])
VALUES (
    nr.PropertyName
    , nr.[PropertyValue]);

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,009
           ,'update_spectrum_db_load_009.sql'
           ,'1.0.01')
GO