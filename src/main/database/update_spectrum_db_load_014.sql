SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 014
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

------------------------------------------------------------
-- update script
------------------------------------------------------------


RAISERROR ('-- Loading ChannelGroup ', 0, 1) WITH NOWAIT
GO
INSERT ChannelGroup (ChannelGroupName, Notes, IsVisible) ----select * from ChannelGroup
SELECT categoryTobeUsedAsChannelgroupName, source.notes, 1 as isVisible 
FROM (
	SELECT 'HSP' as categoryTobeUsedAsChannelgroupName, 'STL HSP Events' as notes UNION
	SELECT 'DIA','STL DIA Events' UNION
	SELECT 'LSP','STL LSP Events' UNION
	SELECT 'DRN','STL DRN Events' UNION
	SELECT 'BSS','STL BSS Events' UNION
	SELECT 'KLM','STL KLM Events' UNION
	SELECT 'LVZ','STL LVZ Events' UNION
	SELECT 'PIS','STL PIS Events' UNION
	SELECT 'REM','STL REM Events' UNION
	SELECT 'TRC','STL TRC Events' UNION
	SELECT 'VRL','STL VRL Events' UNION
	SELECT 'ARR','STL ARR Events' UNION
	SELECT 'COM','STL COM Events' UNION
	SELECT 'CAB','STL CAB Events' UNION
	SELECT 'ATB','STL ATB Events' UNION
	SELECT 'SLTDoor', 'SLTEnvironmentDoor'  UNION
	SELECT 'SLTValues', 'SLTEnvironmentValues' 	
) source
LEFT OUTER JOIN channelgroup cg ON ChannelGroupName = categoryTobeUsedAsChannelgroupName
WHERE ChannelGroupName is null

--BEGIN TRAN

--DELETE FROM ChannelGroup WHERE ChannelGroupName = 'BrakeGroup'  and Id = 2
--DELETE FROM ChannelGroup WHERE ChannelGroupName = 'PressureGroup' and Id = 3

--END

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,014
           ,'update_spectrum_db_load_014.sql'
           ,'1.1.02')
GO
