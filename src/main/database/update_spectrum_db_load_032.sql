SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 032
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
-- Update script
---------------------------------------

RAISERROR ('-- Update Channel Groups', 0, 1) WITH NOWAIT
GO

UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Traction', Notes = 'Traction' WHERE ChannelGroupName = 'ARR'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Brakes', Notes = 'Brakes' WHERE ChannelGroupName = 'ATB'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Doors', Notes = 'Doors' WHERE ChannelGroupName = 'BSS'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Safety', Notes = 'Safety' WHERE ChannelGroupName = 'CAB'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'HVAC', Notes = 'HVAC' WHERE ChannelGroupName = 'COM'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Lighting', Notes = 'Lighting' WHERE ChannelGroupName = 'DIA'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Communication', Notes = 'Communication' WHERE ChannelGroupName = 'DRN'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Toilet', Notes = 'Toilet' WHERE ChannelGroupName = 'HSP'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'High Voltage System', Notes = 'High Voltage System' WHERE ChannelGroupName = 'KLM'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Low Voltage System', Notes = 'Low Voltage System' WHERE ChannelGroupName = 'LSP'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Air Supply System', Notes = 'Air Supply System' WHERE ChannelGroupName = 'LVZ'
UPDATE [dbo].[ChannelGroup] SET ChannelGroupName = 'Other', Notes = 'Other' WHERE ChannelGroupName = 'PIS'

GO

RAISERROR ('-- Update Channel table', 0, 1) WITH NOWAIT
GO

-- ARR Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Safety') WHERE ID BETWEEN 5000 AND 5001

-- ATB Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Safety') WHERE ID BETWEEN 5002 AND 5160

-- BSS Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Other') WHERE ID BETWEEN 5161 AND 5209

-- CAB Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Other') WHERE ID BETWEEN 5210 AND 5320

-- COM Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Communication') WHERE ID BETWEEN 5321 AND 5460

-- DIA Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Other') WHERE ID BETWEEN 5461 AND 5868

-- DRN Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Doors') WHERE ID BETWEEN 5869 AND 6530

-- HSP Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'High Voltage System') WHERE ID BETWEEN 6531 AND 6611

-- KLM Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'HVAC') WHERE ID BETWEEN 6612 AND 7045

-- LSP Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Low Voltage System') WHERE ID BETWEEN 7046 AND 7073

-- LVZ Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Air Supply System') WHERE ID BETWEEN 7074 AND 7082

-- PIS Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Communication') WHERE ID BETWEEN 7083 AND 7255

-- REM Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Brakes') WHERE ID BETWEEN 7256 AND 7836

-- TRC Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Traction') WHERE ID BETWEEN 7837 AND 8002

-- VRL Channels
UPDATE [dbo].[Channel] SET ChannelGroupID = (SELECT ID FROM ChannelGroup where ChannelGroupName = 'Lighting') WHERE ID BETWEEN 8003 AND 8008
GO

RAISERROR ('-- Delete invalid Channel Groups', 0, 1) WITH NOWAIT
GO

DELETE [dbo].[ChannelGroup] WHERE ChannelGroupName = 'REM'
DELETE [dbo].[ChannelGroup] WHERE ChannelGroupName = 'TRC'
DELETE [dbo].[ChannelGroup] WHERE ChannelGroupName = 'VRL'

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,032
           ,'update_spectrum_db_load_032.sql'
           ,'1.2.03')
GO