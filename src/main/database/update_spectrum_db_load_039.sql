SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 039
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- update script
---------------------

RAISERROR ('-- Additional SLT units for learning environment ', 0, 1) WITH NOWAIT
GO

BEGIN TRAN

SET IDENTITY_INSERT [dbo].[Unit] ON

INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2491, '2491_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2492, '2492_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2493, '2493_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2494, '2494_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2495, '2495_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2496, '2496_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2497, '2497_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2498, '2498_T', 'SLT-IV', 1, 1)
INSERT INTO [dbo].[Unit] (ID, [UnitNumber] ,[UnitType] ,[IsValid] ,[FleetID]) VALUES (2499, '2499_T', 'SLT-IV', 1, 1)

SET IDENTITY_INSERT [dbo].[Unit] OFF

COMMIT

GO

BEGIN TRAN

INSERT [dbo].[FleetFormation] (UnitIDList, UnitNumberList, ValidFrom, ValidTo)
SELECT ID, UnitNumber, getdate()-1, null
FROM dbo.Unit u
WHERE u.ID BETWEEN 2491 AND 2499

INSERT dbo.FleetStatus (ValidFrom, ValidTo, Setcode, Diagram, Headcode, IsValid, Loading, ConfigDate, UnitList, LocationIDHeadcodeStart, LocationIDHeadcodeEnd, UnitID, UnitPosition, UnitOrientation, FleetFormationID)
SELECT ValidFrom, null as ValidTo, null as Setcode, null as Diagram, null as Headcode, null as IsValid, null as Loading, null as ConfigDate, null as UnitList, null as LocationIDHeadcodeStart, null as LocationIDHeadcodeEnd, UnitIDList as UnitID, 1 as UnitPosition, 1 as UnitOrientation, f.ID as FleetFormationID
FROM [dbo].[FleetFormation] f
--WHERE f.UnitIDList BETWEEN 2491 AND 2499
WHERE f.UnitIDList IN ('2491','2492','2493','2494','2495','2496','2497','2498','2499')

COMMIT


EXEC NSP_PopulateChannelValues

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,039
           ,'update_spectrum_db_load_039.sql'
           ,'1.8.02')
GO
