SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 043
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

--------------------------------------------
-- Update - NS-632
--------------------------------------------

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491mABk3', 'mABk3', 1, 1, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491B1', 'B1', 2, 0, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491AB', 'AB', 3, 0, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491mABk2', 'mABk2', 4, 1, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492AB', 'AB', 2, 0, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492mABk3', 'mABk3', 1, 1, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492mABk2', 'mABk2', 3, 1, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493AB', 'AB', 2, 0, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493mABk3', 'mABk3', 1, 1, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493B1', 'B1', 3, 0, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493mABk2', 'mABk2', 4, 1, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494AB', 'AB', 2, 0, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494mABk3', 'mABk3', 1, 1, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494B1', 'B1', 3, 0, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494mABk2', 'mABk2', 4, 1, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495AB', 'AB', 2, 0, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mABk3', 'mABk3', 1, 1, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mB2', 'mB2', 3, 0, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495B1', 'B1', 4, 0, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mABk2', 'mABk2', 5, 1, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496AB', 'AB', 2, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mABk3', 'mABk3', 1, 1, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mB2', 'mB2', 3, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mB3', 'mB3', 4, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496B1', 'B1', 5, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mABk2', 'mABk2', 6, 1, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497AB', 'AB', 2, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mABk3', 'mABk3', 1, 1, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mB1', 'mB1', 3, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mB2', 'mB2', 4, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mB3', 'mB3', 5, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497B1', 'B1', 6, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mABk2', 'mABk2', 7, 1, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498AB', 'AB', 2, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mABk3', 'mABk3', 1, 1, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mB1', 'mB1', 3, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mB2', 'mB2', 4, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mB3', 'mB3', 5, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mABk2', 'mABk2', 6, 1, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499AB', 'AB', 1, 1, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mB1', 'mB1', 2, 0, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mB2', 'mB2', 3, 0, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mB3', 'mB3', 4, 0, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mABk2', 'mABk2', 5, 1, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

GO

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,043
           ,'update_spectrum_db_load_043.sql'
           ,'1.8.02')
GO