SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 046
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------
-- Update - NS-651
---------------------

INSERT INTO dbo.ChannelValue(UnitID,timestamp,UpdateRecord)
SELECT ID,'2017-01-01',0 FROM dbo.Unit WHERE ID NOT IN (SELECT UnitID FROM dbo.ChannelValue)

INSERT INTO dbo.ChannelValueDoor(Id,UnitID,timestamp,UpdateRecord)
SELECT t.ID,Unit.ID,'2017-01-01',0 FROM Unit 
OUTER APPLY (SELECT Top 1 ID FROM dbo.ChannelValue where UnitID = Unit.ID ORDER BY ID ASC) t
WHERE Unit.ID NOT IN (SELECT UnitID FROM dbo.ChannelValueDoor)

GO
--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,046
           ,'update_spectrum_db_load_046.sql'
           ,'1.9.01')
GO