SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 054
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
-- NS-817 Update test units with correct amount of vehicles
---------------------------------------

DELETE FROM [dbo].[Vehicle]
WHERE UnitID BETWEEN 2491 AND 2499;

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491mABK1', 'mABK1', 1, 1, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491mB1', 'mB1', 2, 0, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491B1', 'B1', 3, 0, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2491mABk2', 'mABk2', 4, 1, 2491, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492mABK1', 'mABK1', 1, 1, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492mB1', 'mB1', 2, 0, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492B1', 'B1', 3, 0, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2492mABk2', 'mABk2', 4, 1, 2492, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493mABK1', 'mABK1', 1, 1, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493mB1', 'mB1', 2, 0, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493B1', 'B1', 3, 0, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2493mABk2', 'mABk2', 4, 1, 2493, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494mABK1', 'mABK1', 1, 1, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494mB1', 'mB1', 2, 0, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494B1', 'B1', 3, 0, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2494mABk2', 'mABk2', 4, 1, 2494, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mABK1', 'mABK1', 1, 1, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mB1', 'mB1', 2, 0, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495B1', 'B1', 3, 0, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2495mABk2', 'mABk2', 4, 1, 2495, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mABK1', 'mABK1', 1, 1, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mB1', 'mB1', 2, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496B1', 'B1', 3, 0, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2496mABk2', 'mABk2', 4, 1, 2496, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mABK1', 'mABK1', 1, 1, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mB1', 'mB1', 2, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497B1', 'B1', 3, 0, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2497mABk2', 'mABk2', 4, 1, 2497, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mABK1', 'mABK1', 1, 1, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mB1', 'mB1', 2, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498B1', 'B1', 3, 0, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2498mABk2', 'mABk2', 4, 1, 2498, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mABK1', 'mABK1', 1, 1, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mB1', 'mB1', 2, 0, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499B1', 'B1', 3, 0, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
VALUES ('2499mABk2', 'mABk2', 4, 1, 2499, 'testComment', '2017-03-01 09:56:08.503', '2017-03-01 09:56:08.503', 1, 2, NULL, NULL, NULL, NULL)

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,054
           ,'update_spectrum_db_load_054.sql'
           ,'1.11.02')
GO