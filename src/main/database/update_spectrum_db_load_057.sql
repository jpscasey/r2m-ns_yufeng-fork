SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 057
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
-- R2M-4704 Add FaultMeta and FaultMetaExtraField data from SLT fleet database to SLT and VIRM fleet databases
-- FaultCategory is first updated as FaultMeta is dependant on this table
---------------------------------------


--FaultCategory Inserts, Updates

DECLARE @CatIDList TABLE (ID INT);

INSERT INTO @CatIDList
SELECT ID
FROM [dbo].[FaultCategory];

DECLARE @categoryQuery VARCHAR(50);
DECLARE @reportingOnlyQuery bit;

DECLARE @x INT;
SELECT @x = MIN(ID) FROM @CatIDList
WHILE @x IS NOT NULL
BEGIN
	
	SET @categoryQuery = (SELECT Category FROM [dbo].[FaultCategory] WHERE ID = @x);
	SET @reportingOnlyQuery = (SELECT ReportingOnly FROM [dbo].[FaultCategory] WHERE ID = @x);
	

	IF EXISTS(SELECT ID FROM [$(db_name_sng)].[dbo].[FaultCategory] WHERE ID = @x)
		BEGIN
			-- Update FaultCategory SNG
			
			UPDATE [$(db_name_sng)].[dbo].[FaultCategory] SET Category = @categoryQuery WHERE ID = @x;
			UPDATE [$(db_name_sng)].[dbo].[FaultCategory] SET ReportingOnly = @reportingOnlyQuery WHERE ID = @x;
		END
	ELSE
		BEGIN
			-- Insert FaultCategory SNG
			
			INSERT INTO [$(db_name_sng)].[dbo].[FaultCategory] (ID, Category, ReportingOnly) VALUES (@x, @categoryQuery, @reportingOnlyQuery);
		END 
	SELECT @x = MIN(ID) FROM @CatIDList WHERE ID > @x;
END


DECLARE @y INT;
SELECT @y = MIN(ID) FROM @CatIDList
WHILE @y IS NOT NULL
BEGIN
	
	SET @categoryQuery = (SELECT Category FROM [dbo].[FaultCategory] WHERE ID = @y);
	SET @reportingOnlyQuery = (SELECT ReportingOnly FROM [dbo].[FaultCategory] WHERE ID = @y);
	

	IF EXISTS(SELECT ID FROM [$(db_name_virm)].[dbo].[FaultCategory] WHERE ID = @y)
		BEGIN
			-- Update FaultCategory VIRM
			
			UPDATE [$(db_name_virm)].[dbo].[FaultCategory] SET Category = @categoryQuery WHERE ID = @y;
			UPDATE [$(db_name_virm)].[dbo].[FaultCategory] SET ReportingOnly = @reportingOnlyQuery WHERE ID = @y;
		END
	ELSE
		BEGIN
			-- Insert FaultCategory VIRM
			
			INSERT INTO [$(db_name_virm)].[dbo].[FaultCategory] (ID, Category, ReportingOnly) VALUES (@y, @categoryQuery, @reportingOnlyQuery);
		END 
	SELECT @y = MIN(ID) FROM @CatIDList WHERE ID > @y;
END


--FaultMeta, FaultMetaExtraField Inserts, Updates


DECLARE @IDList TABLE (ID INT);

INSERT INTO @IDList
SELECT ID
FROM [dbo].[FaultMeta];

-- FaultMeta table variables
DECLARE @FaultcodeQuery VARCHAR(100);
DECLARE @DescriptionQuery VARCHAR(100);
DECLARE @SummaryQuery VARCHAR(1000);
DECLARE @AdditionalInfoQuery VARCHAR(3700);
DECLARE @UrlQuery VARCHAR(500);
DECLARE @CategoryIDQuery INT;
DECLARE @HasRecoveryQuery BIT;
DECLARE @RecoveryProcessPathQuery VARCHAR(100);
DECLARE @FaultTypeIDQuery TINYINT;

-- FaultMetaExtraField table variables
DECLARE @controlRoomAdviceQuery NVARCHAR(MAX);
DECLARE @controlRoomPriorityQuery NVARCHAR(MAX);
DECLARE @engineerAdviceQuery NVARCHAR(MAX);
DECLARE @maintenanceFunctionQuery NVARCHAR(MAX);
DECLARE @qualityProfileQuery NVARCHAR(MAX);
DECLARE @safetyPriorityQuery NVARCHAR(MAX);
DECLARE @serviceRequestPriorityQuery NVARCHAR(MAX);
DECLARE @systemCodeQuery NVARCHAR(MAX);
DECLARE @trainCrewAdviceQuery NVARCHAR(MAX);
DECLARE @unitOfMeasureQuery NVARCHAR(MAX);

DECLARE @i INT;
SELECT @i = MIN(ID) FROM @IDList
WHILE @i IS NOT NULL
BEGIN

	SET @FaultcodeQuery = (SELECT FaultCode FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @DescriptionQuery = (SELECT Description FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @SummaryQuery = (SELECT Summary FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @AdditionalInfoQuery = (SELECT AdditionalInfo FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @UrlQuery = (SELECT Url FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @CategoryIDQuery = (SELECT CategoryID FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @HasRecoveryQuery = (SELECT HasRecovery FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @RecoveryProcessPathQuery = (SELECT RecoveryProcessPath FROM [dbo].[FaultMeta] WHERE ID = @i);
	SET @FaultTypeIDQuery = (SELECT FaultTypeID FROM [dbo].[FaultMeta] WHERE ID = @i);
	
	
	SET @controlRoomAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'controlRoomAdvice'));
	SET @controlRoomPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'controlRoomPriority'));
	SET @engineerAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'engineerAdvice'));
	SET @maintenanceFunctionQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'maintenanceFunction'));
	SET @qualityProfileQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'qualityProfile'));
	SET @safetyPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'safetyPriority'));
	SET @serviceRequestPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'serviceRequestPriority'));
	SET @systemCodeQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'systemCode'));
	SET @trainCrewAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'trainCrewAdvice'));
	SET @unitOfMeasureQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @i AND Field = 'unitOfMeasure'));


	IF EXISTS(SELECT ID FROM [$(db_name_sng)].[dbo].[FaultMeta] WHERE ID = @i)
		BEGIN		
			-- UpdateFaultMeta SNG
			
			EXEC	[$(db_name_sng)].[dbo].[NSP_UpdateFaultMeta]
					@ID = @i,
					@Username = NULL,
					@FaultCode = @FaultcodeQuery,
					@Description = @DescriptionQuery,
					@Summary = @SummaryQuery,
					@AdditionalInfo = @AdditionalInfoQuery,
					@Url = @UrlQuery,
					@CategoryID = @CategoryIDQuery,
					@HasRecovery = @HasRecoveryQuery,
					@RecoveryProcessPath = @RecoveryProcessPathQuery,
					@FaultTypeID = @FaultTypeIDQuery,
					@GroupID = NULL
		END
	ELSE
		BEGIN
			-- InsertFaultMeta SNG		
			
			EXEC	[$(db_name_sng)].[dbo].[NSP_InsertFaultMeta]
					@ID = @i,
					@Username = NULL,
					@FaultCode = @FaultcodeQuery,
					@Description = @DescriptionQuery,
					@Summary = @SummaryQuery,
					@AdditionalInfo = @AdditionalInfoQuery,
					@Url = @UrlQuery,
					@CategoryID = @CategoryIDQuery,
					@HasRecovery = @HasRecoveryQuery,
					@RecoveryProcessPath = @RecoveryProcessPathQuery,
					@FaultTypeID = @FaultTypeIDQuery,
					@GroupID = NULL
		END
		
		
	IF EXISTS(SELECT FaultMetaId FROM [$(db_name_sng)].[dbo].[FaultMetaExtraField] WHERE FaultMetaId = @i)
		BEGIN
			-- UpdateFaultMetaExtraField SNG
			
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @controlRoomAdviceQuery WHERE (FaultMetaId = @i AND Field = 'controlRoomAdvice');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @controlRoomPriorityQuery WHERE (FaultMetaId = @i AND Field = 'controlRoomPriority');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @engineerAdviceQuery WHERE (FaultMetaId = @i AND Field = 'engineerAdvice');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @maintenanceFunctionQuery WHERE (FaultMetaId = @i AND Field = 'maintenanceFunction');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @qualityProfileQuery WHERE (FaultMetaId = @i AND Field = 'qualityProfile');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @safetyPriorityQuery WHERE (FaultMetaId = @i AND Field = 'safetyPriority');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @serviceRequestPriorityQuery WHERE (FaultMetaId = @i AND Field = 'serviceRequestPriority');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @systemCodeQuery WHERE (FaultMetaId = @i AND Field = 'systemCode');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @trainCrewAdviceQuery WHERE (FaultMetaId = @i AND Field = 'trainCrewAdvice');
			UPDATE [$(db_name_sng)].[dbo].[FaultMetaExtraField] SET Value = @unitOfMeasureQuery WHERE (FaultMetaId = @i AND Field = 'unitOfMeasure');
		END
	ELSE IF EXISTS(SELECT FaultMetaId FROM [dbo].[FaultMetaExtraField] WHERE FaultMetaId = @i)
		BEGIN
			-- InsertFaultMetaExtraField SNG
			
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'controlRoomAdvice', @controlRoomAdviceQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'controlRoomPriority', @controlRoomPriorityQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'engineerAdvice', @engineerAdviceQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'maintenanceFunction', @maintenanceFunctionQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'qualityProfile', @qualityProfileQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'safetyPriority', @safetyPriorityQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'serviceRequestPriority', @serviceRequestPriorityQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'systemCode', @systemCodeQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'trainCrewAdvice', @trainCrewAdviceQuery);
			INSERT INTO [$(db_name_sng)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@i, 'unitOfMeasure', @unitOfMeasureQuery);
		END
			
	SELECT @i = MIN(ID) FROM @IDList WHERE ID > @i;
END

DECLARE @j INT;
SELECT @j = MIN(ID) FROM @IDList
WHILE @j IS NOT NULL
BEGIN

	SET @FaultcodeQuery = (SELECT FaultCode FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @DescriptionQuery = (SELECT Description FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @SummaryQuery = (SELECT Summary FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @AdditionalInfoQuery = (SELECT AdditionalInfo FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @UrlQuery = (SELECT Url FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @CategoryIDQuery = (SELECT CategoryID FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @HasRecoveryQuery = (SELECT HasRecovery FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @RecoveryProcessPathQuery = (SELECT RecoveryProcessPath FROM [dbo].[FaultMeta] WHERE ID = @j);
	SET @FaultTypeIDQuery = (SELECT FaultTypeID FROM [dbo].[FaultMeta] WHERE ID = @j);
	
	
	SET @controlRoomAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'controlRoomAdvice'));
	SET @controlRoomPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'controlRoomPriority'));
	SET @engineerAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'engineerAdvice'));
	SET @maintenanceFunctionQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'maintenanceFunction'));
	SET @qualityProfileQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'qualityProfile'));
	SET @safetyPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'safetyPriority'));
	SET @serviceRequestPriorityQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'serviceRequestPriority'));
	SET @systemCodeQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'systemCode'));
	SET @trainCrewAdviceQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'trainCrewAdvice'));
	SET @unitOfMeasureQuery = (SELECT Value FROM [dbo].[FaultMetaExtraField] WHERE (FaultMetaId = @j AND Field = 'unitOfMeasure'));


	IF EXISTS(SELECT ID FROM [$(db_name_virm)].[dbo].[FaultMeta] WHERE ID = @j)
		BEGIN		
			-- UpdateFaultMeta VIRM
			
			EXEC	[$(db_name_virm)].[dbo].[NSP_UpdateFaultMeta]
					@ID = @j,
					@Username = NULL,
					@FaultCode = @FaultcodeQuery,
					@Description = @DescriptionQuery,
					@Summary = @SummaryQuery,
					@AdditionalInfo = @AdditionalInfoQuery,
					@Url = @UrlQuery,
					@CategoryID = @CategoryIDQuery,
					@HasRecovery = @HasRecoveryQuery,
					@RecoveryProcessPath = @RecoveryProcessPathQuery,
					@FaultTypeID = @FaultTypeIDQuery,
					@GroupID = NULL
		END
	ELSE
		BEGIN
			-- InsertFaultMeta VIRM		
			
			EXEC	[$(db_name_virm)].[dbo].[NSP_InsertFaultMeta]
					@ID = @j,
					@Username = NULL,
					@FaultCode = @FaultcodeQuery,
					@Description = @DescriptionQuery,
					@Summary = @SummaryQuery,
					@AdditionalInfo = @AdditionalInfoQuery,
					@Url = @UrlQuery,
					@CategoryID = @CategoryIDQuery,
					@HasRecovery = @HasRecoveryQuery,
					@RecoveryProcessPath = @RecoveryProcessPathQuery,
					@FaultTypeID = @FaultTypeIDQuery,
					@GroupID = NULL
		END
		
		
	IF EXISTS(SELECT FaultMetaId FROM [$(db_name_virm)].[dbo].[FaultMetaExtraField] WHERE FaultMetaId = @j)
		BEGIN
			-- UpdateFaultMetaExtraField VIRM
			
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @controlRoomAdviceQuery WHERE (FaultMetaId = @j AND Field = 'controlRoomAdvice');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @controlRoomPriorityQuery WHERE (FaultMetaId = @j AND Field = 'controlRoomPriority');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @engineerAdviceQuery WHERE (FaultMetaId = @j AND Field = 'engineerAdvice');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @maintenanceFunctionQuery WHERE (FaultMetaId = @j AND Field = 'maintenanceFunction');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @qualityProfileQuery WHERE (FaultMetaId = @j AND Field = 'qualityProfile');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @safetyPriorityQuery WHERE (FaultMetaId = @j AND Field = 'safetyPriority');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @serviceRequestPriorityQuery WHERE (FaultMetaId = @j AND Field = 'serviceRequestPriority');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @systemCodeQuery WHERE (FaultMetaId = @j AND Field = 'systemCode');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @trainCrewAdviceQuery WHERE (FaultMetaId = @j AND Field = 'trainCrewAdvice');
			UPDATE [$(db_name_virm)].[dbo].[FaultMetaExtraField] SET Value = @unitOfMeasureQuery WHERE (FaultMetaId = @j AND Field = 'unitOfMeasure');
		END
	ELSE IF EXISTS(SELECT FaultMetaId FROM [dbo].[FaultMetaExtraField] WHERE FaultMetaId = @j)
		BEGIN
			-- InsertFaultMetaExtraField VIRM
			
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'controlRoomAdvice', @controlRoomAdviceQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'controlRoomPriority', @controlRoomPriorityQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'engineerAdvice', @engineerAdviceQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'maintenanceFunction', @maintenanceFunctionQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'qualityProfile', @qualityProfileQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'safetyPriority', @safetyPriorityQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'serviceRequestPriority', @serviceRequestPriorityQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'systemCode', @systemCodeQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'trainCrewAdvice', @trainCrewAdviceQuery);
			INSERT INTO [$(db_name_virm)].[dbo].[FaultMetaExtraField] (FaultMetaId, Field, Value) VALUES (@j, 'unitOfMeasure', @unitOfMeasureQuery);
		END
			
	SELECT @j = MIN(ID) FROM @IDList WHERE ID > @j;
END

--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,057
           ,'update_spectrum_db_load_057.sql'
           ,NULL)
GO