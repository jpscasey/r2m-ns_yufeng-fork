SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- validate if file was already run
------------------------------------------------------------

DECLARE @DBVersion int
DECLARE @scriptNumber int
DECLARE @scriptType varchar(50)
DECLARE @errorMsg varchar(100)

SET @scriptNumber = 059
SET @scriptType = 'data load'


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SchemaChangeLog' AND TABLE_TYPE = 'BASE TABLE')
BEGIN

	IF EXISTS (SELECT * FROM SchemaChangeLog WHERE ScriptType = @scriptType AND ScriptNumber = @scriptNumber)

		RAISERROR('******** Script already run ******** ', 20, 1) with log

	ELSE
		BEGIN

			SELECT @DBVersion = ISNULL(MAX(ScriptNumber), 0)
			FROM SchemaChangeLog
			WHERE ScriptType = @scriptType

			IF @scriptNumber <> @DBVersion + 1
				BEGIN
					SET @errorMsg = '******** Incorrect script to run. Please run script number ' + CONVERT(varchar, @DBVersion + 1) + ' ********'
					RAISERROR(@errorMsg , 20, 1) with log
				END
		END
END

GO

---------------------------------------
-- R2M-8226 Update Channel data types
---------------------------------------

UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA1_I110VAuxPowSup_2_1';
UPDATE [dbo].[Channel] SET DataType = 'smallint' WHERE Name = 'BCHA1_IBattCurr_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA1_IBattOutputCurr_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA1_IChargCurrMax_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA1_IDcLinkVoltage_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA1_IOutputPower_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA2_I110VAuxPowSup_2_2';
UPDATE [dbo].[Channel] SET DataType = 'smallint' WHERE Name = 'BCHA2_IBattCurr_2_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA2_IBattOutputCurr_2_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA2_IChargCurrMax_2_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA2_IDcLinkVoltage_2_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'BCHA2_IOutputPower_2_2';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'CABIN';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'CABIN_IST';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'CABIN_SOLL';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'CABIN_STATUS';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'CURRENT_SPEED';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CBrakingEffortInt_3_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CBrakingEffortInt_3_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CBrakingEffortInt_5_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CBrakingEffortInt_5_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CTractiveEffort_3_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_CTractiveEffort_3_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_1_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_1_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_15';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_2_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_2_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_3_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_3_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_10';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_11';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_12';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_13';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_14';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_15';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_16';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_17';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_18';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_19';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_20';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_3';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_4';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_5';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_6';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_7';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_8';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_4_9';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_5_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_5_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_3';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_4';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_5';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_6';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_7';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_8_8';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'DBC_ITrainSpeedInt_9';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'DIRECTION_CONTROL';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'EB_STATUS';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'ERROR_CODE_INTERNAL';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'ERRORCODE_1';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'ERRORCODE_2';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'ERRORCODE_3';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'INTERNAL_ERROR_CODE';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'INTERNAL_STATE';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'ITrainsetNumber1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'ITrainsetNumber2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'ITrainsetNumber3';
UPDATE [dbo].[Channel] SET DataType = 'decimal(9,6)' WHERE Name = 'MCG_ILatitude';
UPDATE [dbo].[Channel] SET DataType = 'decimal(9,6)' WHERE Name = 'MCG_ILongitude';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'MMI_M_LEVEL';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'MMI_V_PERMITTED';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'MMI_V_TRAIN';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'PB_ADDR';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'PERMITTED_SPEED';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'REPORTED_SPEED';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'REPORTED_SPEED_HISTORIC';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'ROTATION_DIRECTION_SDU1';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'ROTATION_DIRECTION_SDU2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SAP';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_HISTORIC_RADAR1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_HISTORIC_RADAR2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_HISTORIC_TACHO1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_HISTORIC_TACHO2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_RADAR1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_RADAR2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_TACHO1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SENSOR_SPEED_TACHO2';
UPDATE [dbo].[Channel] SET DataType = 'tinyint' WHERE Name = 'SLIP_SLIDE_AXLE2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'SPL_FUNC_ID';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU1_ILineVoltage_3_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU1_ILineVoltage_5_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU1_ILineVoltage_5_2';
UPDATE [dbo].[Channel] SET DataType = 'smallint' WHERE Name = 'TCU1_ITracBrActualInt_3_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU2_ILineVoltage_3_2';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU2_ILineVoltage_5_1';
UPDATE [dbo].[Channel] SET DataType = 'int' WHERE Name = 'TCU2_ILineVoltage_5_2';
UPDATE [dbo].[Channel] SET DataType = 'smallint' WHERE Name = 'TCU2_ITracBrActualInt_3_2';

GO

DELETE FROM dbo.ChannelValueDoor
GO
DELETE FROM dbo.ChannelValue
GO
UPDATE dbo.RuleEngineCurrentID SET Value = 0  WHERE ConfigurationName = 'SLT' AND Position = 1
GO
EXEC NSP_PopulateChannelValues
GO


--------------------------------------------
-- INSERT into SchemaChangeLog
--------------------------------------------

INSERT INTO [SchemaChangeLog]
           ([ScriptType]
           ,[ScriptNumber]
           ,[ScriptName]
           ,[ApplicationVersion])
     VALUES
           ('data load'
           ,059
           ,'update_spectrum_db_load_059.sql'
           ,NULL)
GO