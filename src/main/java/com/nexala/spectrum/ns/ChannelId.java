package com.nexala.spectrum.ns;

public class ChannelId {
    /* TO GENEREATE THE FOLLOWING CHANNEL ID REFERENCES USE THE QUERY ON "ChannelId.java.query" FILE */

    /** Laatste breedtegraad - MCG_ILatitude */
    public static final int CH1 = 1;

    /** Laatste lengtegraad - MCG_ILongitude */
    public static final int CH2 = 2;

    /** rijrichtinghandel - DIRECTION_CONTROL */
    public static final int CH3 = 3;

    /** treinsnelheid - CURRENT_SPEED */
    public static final int CH4 = 4;

    /** AUX1: Derating-vraag - AUX1_CDeratingReq_1_1 */
    public static final int CH5 = 5;

    /** AUX1: 3-fase belastingsautomaat (K11) is IN - AUX1_I3ACContactorOn_1_1 */
    public static final int CH6 = 6;

    /** Belastingsautomaat 1 - AUX1_I3ACContactorOn_2_1 */
    public static final int CH7 = 7;

    /** Belastingsautomaat 1 - AUX1_I3ACContactorOn_2_2 */
    public static final int CH8 = 8;

    /** AUX1: 3-fase belastingvrijgave - AUX1_I3ACLoadEnabled_1_1 */
    public static final int CH9 = 9;

    /** AUX1: Hulpbedrijfomzetter is bedrijfsgereed - AUX1_I3ACOk_1_1 */
    public static final int CH10 = 10;

    /** AUX1: Gelijkrichter ingangsspanning o.k. (900…1800V) - AUX1_IInputVoltageOk_1_1 */
    public static final int CH11 = 11;

    /** AUX1: Vrijgavesignaal is actief (-K55 is IN) - AUX1_IInverterEnabled_1_1 */
    public static final int CH12 = 12;

    /** AUX1: Netsnelschakelaar (-K1) is IN - AUX1_ILineContactorOn_1_1 */
    public static final int CH13 = 13;

    /** AUX1: 3-fase uitgangsspanning o.k. (380…420Veff) - AUX1_IOutputVoltageOk_1_1 */
    public static final int CH14 = 14;

    /** AUX1: Aansturing snelscheider (-K2) is actief - AUX1_IPreChargContOn_1_1 */
    public static final int CH15 = 15;

    /** AUX1: Temperatuur koelblok minder 70°C - AUX1_ITempHeatSinkOk_1_1 */
    public static final int CH16 = 16;

    /** AUX2: Derating-vraag - AUX2_CDeratingReq_1_2 */
    public static final int CH17 = 17;

    /** AUX2: 3-fase belastingsautomaat (K11) is IN - AUX2_I3ACContactorOn_1_2 */
    public static final int CH18 = 18;

    /** Belastingsautomaat 2 - AUX2_I3ACContactorOn_2_1 */
    public static final int CH19 = 19;

    /** Belastingsautomaat 2 - AUX2_I3ACContactorOn_2_2 */
    public static final int CH20 = 20;

    /** AUX2: 3-fase belastingvrijgave - AUX2_I3ACLoadEnabled_1_2 */
    public static final int CH21 = 21;

    /** AUX2: Hulpbedrijfomzetter is bedrijfsgereed - AUX2_I3ACOk_1_2 */
    public static final int CH22 = 22;

    /** AUX2: Gelijkrichter ingangsspanning o.k. (900…1800V) - AUX2_IInputVoltageOk_1_2 */
    public static final int CH23 = 23;

    /** AUX2: Vrijgavesignaal is actief (-K55 is IN) - AUX2_IInverterEnabled_1_2 */
    public static final int CH24 = 24;

    /** AUX2: Netsnelschakelaar (-K1) is IN - AUX2_ILineContactorOn_1_2 */
    public static final int CH25 = 25;

    /** AUX2: 3-fase uitgangsspanning o.k. (380…420Veff) - AUX2_IOutputVoltageOk_1_2 */
    public static final int CH26 = 26;

    /** AUX2: Aansturing snelscheider (-K2) is actief - AUX2_IPreChargContOn_1_2 */
    public static final int CH27 = 27;

    /** AUX2: Temperatuur koelblok minder 70°C - AUX2_ITempHeatSinkOk_1_2 */
    public static final int CH28 = 28;

    /** Accu-automaat 1 - AUXY_IBatCont1On */
    public static final int CH29 = 29;

    /** Accu-automaat 1 is IN - AUXY_IBatCont1On_1_1 */
    public static final int CH30 = 30;

    /** Accu-automaat 1 is IN - AUXY_IBatCont1On_1_2 */
    public static final int CH31 = 31;

    /** Accu-automaat 1 - AUXY_IBatCont1On_2_1 */
    public static final int CH32 = 32;

    /** Accu-automaat 1 - AUXY_IBatCont1On_2_2 */
    public static final int CH33 = 33;

    /** Accu-automaat 1 - AUXY_IBatCont1On_3_1 */
    public static final int CH34 = 34;

    /** Accu-automaat 1 - AUXY_IBatCont1On_3_2 */
    public static final int CH35 = 35;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_1 */
    public static final int CH36 = 36;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_2 */
    public static final int CH37 = 37;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_3 */
    public static final int CH38 = 38;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_4 */
    public static final int CH39 = 39;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_5 */
    public static final int CH40 = 40;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_6 */
    public static final int CH41 = 41;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_7 */
    public static final int CH42 = 42;

    /** Accu-automaat 1 - AUXY_IBatCont1On_8_8 */
    public static final int CH43 = 43;

    /** Accu-automaat 1 - AUXY_IBatCont1On_9 */
    public static final int CH44 = 44;

    /** Accu-automaat 2 - AUXY_IBatCont2On */
    public static final int CH45 = 45;

    /** Accu-automaat 2 is IN - AUXY_IBatCont2On_1_1 */
    public static final int CH46 = 46;

    /** Accu-automaat 2 is IN - AUXY_IBatCont2On_1_2 */
    public static final int CH47 = 47;

    /** Accu-automaat 2 - AUXY_IBatCont2On_2_1 */
    public static final int CH48 = 48;

    /** Accu-automaat 2 - AUXY_IBatCont2On_2_2 */
    public static final int CH49 = 49;

    /** Accu-automaat 2 - AUXY_IBatCont2On_3_1 */
    public static final int CH50 = 50;

    /** Accu-automaat 2 - AUXY_IBatCont2On_3_2 */
    public static final int CH51 = 51;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_1 */
    public static final int CH52 = 52;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_2 */
    public static final int CH53 = 53;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_3 */
    public static final int CH54 = 54;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_4 */
    public static final int CH55 = 55;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_5 */
    public static final int CH56 = 56;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_6 */
    public static final int CH57 = 57;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_7 */
    public static final int CH58 = 58;

    /** Accu-automaat 2 - AUXY_IBatCont2On_8_8 */
    public static final int CH59 = 59;

    /** Accu-automaat 2 - AUXY_IBatCont2On_9 */
    public static final int CH60 = 60;

    /** acculader 1 110 V hulpvoeding - BCHA1_I110VAuxPowSup_2_1 */
    public static final int CH61 = 61;

    /** acculader 1 accustroom - BCHA1_IBattCurr_2_1 */
    public static final int CH62 = 62;

    /** acculader 1 uitgangsstroom - BCHA1_IBattOutputCurr_2_1 */
    public static final int CH63 = 63;

    /** acculader 1 maximale laadstroom - BCHA1_IChargCurrMax_2_1 */
    public static final int CH64 = 64;

    /** acculader 1 DC koppelspanning - BCHA1_IDcLinkVoltage_2_1 */
    public static final int CH65 = 65;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac */
    public static final int CH66 = 66;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_1_1 */
    public static final int CH67 = 67;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_1_2 */
    public static final int CH68 = 68;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_2_1 */
    public static final int CH69 = 69;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_2_2 */
    public static final int CH70 = 70;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_3_1 */
    public static final int CH71 = 71;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_3_2 */
    public static final int CH72 = 72;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_1 */
    public static final int CH73 = 73;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_2 */
    public static final int CH74 = 74;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_3 */
    public static final int CH75 = 75;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_4 */
    public static final int CH76 = 76;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_5 */
    public static final int CH77 = 77;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_6 */
    public static final int CH78 = 78;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_7 */
    public static final int CH79 = 79;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_8_8 */
    public static final int CH80 = 80;

    /** Externe stroomvoorziening 1 - BCHA1_IExternal400Vac_9 */
    public static final int CH81 = 81;

    /** acculader 1 uitgangsvermogen - BCHA1_IOutputPower_2_1 */
    public static final int CH82 = 82;

    /** acculader 1 systeemspecifiek beperkte mode - BCHA1_ISpecSystNokMod_2_1 */
    public static final int CH83 = 83;

    /** acculader 1 systeemspecifiek normale mode - BCHA1_ISpecSystOkMode_2_1 */
    public static final int CH84 = 84;

    /** acculader 1 accutemperatuur sensor 1 - BCHA1_ITempBattSens1_2_1 */
    public static final int CH85 = 85;

    /** acculader 1 accutemperatuur sensor 2 - BCHA1_ITempBattSens2_2_1 */
    public static final int CH86 = 86;

    /** acculader 1 koelblok temperatuur - BCHA1_ITempHeatSink_2_1 */
    public static final int CH87 = 87;

    /** acculader 2 110 V hulpvoeding - BCHA2_I110VAuxPowSup_2_2 */
    public static final int CH88 = 88;

    /** acculader 2 accustroom - BCHA2_IBattCurr_2_2 */
    public static final int CH89 = 89;

    /** acculader 2 uitgangsstroom - BCHA2_IBattOutputCurr_2_2 */
    public static final int CH90 = 90;

    /** acculader 2 maximale laadstroom - BCHA2_IChargCurrMax_2_2 */
    public static final int CH91 = 91;

    /** acculader 2 DC koppelspanning - BCHA2_IDcLinkVoltage_2_2 */
    public static final int CH92 = 92;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac */
    public static final int CH93 = 93;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_1_1 */
    public static final int CH94 = 94;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_1_2 */
    public static final int CH95 = 95;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_2_1 */
    public static final int CH96 = 96;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_2_2 */
    public static final int CH97 = 97;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_3_1 */
    public static final int CH98 = 98;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_3_2 */
    public static final int CH99 = 99;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_1 */
    public static final int CH100 = 100;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_2 */
    public static final int CH101 = 101;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_3 */
    public static final int CH102 = 102;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_4 */
    public static final int CH103 = 103;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_5 */
    public static final int CH104 = 104;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_6 */
    public static final int CH105 = 105;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_7 */
    public static final int CH106 = 106;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_8_8 */
    public static final int CH107 = 107;

    /** Externe stroomvoorziening 2 - BCHA2_IExternal400Vac_9 */
    public static final int CH108 = 108;

    /** acculader 2 uitgangsvermogen - BCHA2_IOutputPower_2_2 */
    public static final int CH109 = 109;

    /** acculader 2 systeemspecifiek beperkte mode - BCHA2_ISpecSystNokMod_2_2 */
    public static final int CH110 = 110;

    /** acculader 2 systeemspecifiek normale mode - BCHA2_ISpecSystOkMode_2_2 */
    public static final int CH111 = 111;

    /** acculader 2 accutemperatuur sensor 1 - BCHA2_ITempBattSens1_2_2 */
    public static final int CH112 = 112;

    /** acculader 2 accutemperatuur sensor 2 - BCHA2_ITempBattSens2_2_2 */
    public static final int CH113 = 113;

    /** acculader 2 koelblok temperatuur - BCHA2_ITempHeatSink_2_2 */
    public static final int CH114 = 114;

    /** Druk hoofdluchtleiding van rembesturingsapparaat 1 - BCU1_IBrPipePressureInt_5_1 */
    public static final int CH115 = 115;

    /** Druk hoofdluchtleidingtank van rembesturingsapparaat 1 - BCU1_IMainAirResPipeInt_5_1 */
    public static final int CH116 = 116;

    /** Veerrem aangetrokken 1 - BCU1_IParkBrApplied_5_1 */
    public static final int CH117 = 117;

    /** Veerrem noodlossing 1 - BCU1_IParkBrLocked_5_1 */
    public static final int CH118 = 118;

    /** Veerrem gelost 1 - BCU1_IParkBrReleased_5_1 */
    public static final int CH119 = 119;

    /** Druk hoofdluchtleiding van rembesturingsapparaat 2 - BCU2_IBrPipePressureInt_5_2 */
    public static final int CH120 = 120;

    /** Druk hoofdluchtleidingtank van rembesturingsapparaat 2 - BCU2_IMainAirResPipeInt_5_2 */
    public static final int CH121 = 121;

    /** Veerrem aangetrokken 2 - BCU2_IParkBrApplied_5_2 */
    public static final int CH122 = 122;

    /** Veerrem noodlossing 2 - BCU2_IParkBrLocked_5_2 */
    public static final int CH123 = 123;

    /** Veerrem gelost 2 - BCU2_IParkBrReleased_5_2 */
    public static final int CH124 = 124;

    /** Remtest loopt - BCUm_IBrTestRun_5_1 */
    public static final int CH125 = 125;

    /** Remtest loopt - BCUm_IBrTestRun_5_2 */
    public static final int CH126 = 126;

    /** DS1 (6Te) - DS1 (4Te) afgesloten - BCUx_IBogie1Locked_5_1 */
    public static final int CH127 = 127;

    /** DS1 (6Te) - DS1 (4Te) afgesloten - BCUx_IBogie1Locked_5_2 */
    public static final int CH128 = 128;

    /** DG2 (6Te) - DG2 (4Te) afgesloten - BCUx_IBogie2Locked_5_1 */
    public static final int CH129 = 129;

    /** DG2 (6Te) - DG2 (4Te) afgesloten - BCUx_IBogie2Locked_5_2 */
    public static final int CH130 = 130;

    /** DG3 (6Te) - DG3 (4Te) afgesloten - BCUx_IBogie3Locked_5_1 */
    public static final int CH131 = 131;

    /** DG3 (6Te) - DG3 (4Te) afgesloten - BCUx_IBogie3Locked_5_2 */
    public static final int CH132 = 132;

    /** D - BCUx_IBogie4Locked_5_1 */
    public static final int CH133 = 133;

    /** DG4 (6Te) afgesloten - BCUx_IBogie4Locked_5_2 */
    public static final int CH134 = 134;

    /** DG5 (6Te) - DG4 (4Te) afgesloten - BCUx_IBogie5Locked_5_1 */
    public static final int CH135 = 135;

    /** DG5 (6Te) - DG4 (4Te) afgesloten - BCUx_IBogie5Locked_5_2 */
    public static final int CH136 = 136;

    /** DG6 (6Te) - DG5 (4Te) afgesloten - BCUx_IBogie6Locked_5_1 */
    public static final int CH137 = 137;

    /** DG6 (6Te) - DG5 (4Te) afgesloten - BCUx_IBogie6Locked_5_2 */
    public static final int CH138 = 138;

    /** DG7 (6Te) afgesloten - BCUx_IBogie7Locked_5_1 */
    public static final int CH139 = 139;

    /** DG7 (6Te) afgesloten - BCUx_IBogie7Locked_5_2 */
    public static final int CH140 = 140;

    /** DS1 (6Te) - DS1 (4Te) glijdt - BCUx_IWspBogie1Slide_3_1 */
    public static final int CH141 = 141;

    /** DS1 (6Te) - DS1 (4Te) glijdt - BCUx_IWspBogie1Slide_3_2 */
    public static final int CH142 = 142;

    /** DS1 (6Te) - DS1 (4Te) glijdt - BCUx_IWspBogie1Slide_5_1 */
    public static final int CH143 = 143;

    /** DS1 (6Te) - DS1 (4Te) glijdt - BCUx_IWspBogie1Slide_5_2 */
    public static final int CH144 = 144;

    /** DS2 (6Te) - DS2 (4Te) glijdt - BCUx_IWspBogie2Slide_3_1 */
    public static final int CH145 = 145;

    /** DS2 (6Te) - DS2 (4Te) glijdt - BCUx_IWspBogie2Slide_3_2 */
    public static final int CH146 = 146;

    /** DG2 (6Te) - DG2 (4Te) glijdt - BCUx_IWspBogie2Slide_5_1 */
    public static final int CH147 = 147;

    /** DG2 (6Te) - DG2 (4Te) glijdt - BCUx_IWspBogie2Slide_5_2 */
    public static final int CH148 = 148;

    /** DS3 (6Te) - DS3 (4Te) glijdt - BCUx_IWspBogie3Slide_3_1 */
    public static final int CH149 = 149;

    /** DS3 (6Te) - DS3 (4Te) glijdt - BCUx_IWspBogie3Slide_3_2 */
    public static final int CH150 = 150;

    /** DG3 (6Te) - DG3 (4Te) glijdt - BCUx_IWspBogie3Slide_5_1 */
    public static final int CH151 = 151;

    /** DG3 (6Te) - DG3 (4Te) glijdt - BCUx_IWspBogie3Slide_5_2 */
    public static final int CH152 = 152;

    /** DS4 (6Te) glijdt - BCUx_IWspBogie4Slide_3_1 */
    public static final int CH153 = 153;

    /** DS4 (6Te) glijdt - BCUx_IWspBogie4Slide_3_2 */
    public static final int CH154 = 154;

    /** DG4 (6Te) glijdt - BCUx_IWspBogie4Slide_5_1 */
    public static final int CH155 = 155;

    /** DG4 (6Te) glijdt - BCUx_IWspBogie4Slide_5_2 */
    public static final int CH156 = 156;

    /** DS5 (6Te) - DS5 (4Te) glijdt - BCUx_IWspBogie5Slide_3_1 */
    public static final int CH157 = 157;

    /** DS5 (6Te) - DS5 (4Te) glijdt - BCUx_IWspBogie5Slide_3_2 */
    public static final int CH158 = 158;

    /** DG5 (6Te) - DG4 (4Te) glijdt - BCUx_IWspBogie5Slide_5_1 */
    public static final int CH159 = 159;

    /** DG5 (6Te) - DG4 (4Te) glijdt - BCUx_IWspBogie5Slide_5_2 */
    public static final int CH160 = 160;

    /** DS6 (6Te) - DS6 (4Te) glijdt - BCUx_IWspBogie6Slide_3_1 */
    public static final int CH161 = 161;

    /** DS6 (6Te) - DS6 (4Te) glijdt - BCUx_IWspBogie6Slide_3_2 */
    public static final int CH162 = 162;

    /** DG6 (6Te) - DG5 (4Te) glijdt - BCUx_IWspBogie6Slide_5_1 */
    public static final int CH163 = 163;

    /** DG6 (6Te) - DG5 (4Te) glijdt - BCUx_IWspBogie6Slide_5_2 */
    public static final int CH164 = 164;

    /** DS7 (6Te) - BCUx_IWspBogie7Slide_3_1 */
    public static final int CH165 = 165;

    /** DS7 (6Te) - BCUx_IWspBogie7Slide_3_2 */
    public static final int CH166 = 166;

    /** DG7 (6Te) glijdt - BCUx_IWspBogie7Slide_5_1 */
    public static final int CH167 = 167;

    /** DG7 (6Te) glijdt - BCUx_IWspBogie7Slide_5_2 */
    public static final int CH168 = 168;

    /** Nr. of the cabin - CABIN */
    public static final int CH169 = 169;

    /** Cabin amount detected - CABIN_IST */
    public static final int CH170 = 170;

    /** Cabin amount required - CABIN_SOLL */
    public static final int CH171 = 171;

    /** cabine status - CABIN_STATUS */
    public static final int CH172 = 172;

    /** Cycle Counter - CYCLE_COUNTER */
    public static final int CH173 = 173;

    /** Wegsleepschakelaar rijtuig A - DBC_CBCU1ModeTowing_5_1 */
    public static final int CH174 = 174;

    /** Wegsleepschakelaar rijtuig A - DBC_CBCU1ModeTowing_5_2 */
    public static final int CH175 = 175;

    /** Wegsleepschakelaar rijtuig B - DBC_CBCU2ModeTowing_5_1 */
    public static final int CH176 = 176;

    /** Wegsleepschakelaar rijtuig B - DBC_CBCU2ModeTowing_5_2 */
    public static final int CH177 = 177;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M */
    public static final int CH178 = 178;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_1_1 */
    public static final int CH179 = 179;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_1_2 */
    public static final int CH180 = 180;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_2_1 */
    public static final int CH181 = 181;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_2_2 */
    public static final int CH182 = 182;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_3_1 */
    public static final int CH183 = 183;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_3_2 */
    public static final int CH184 = 184;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_1 */
    public static final int CH185 = 185;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_10 */
    public static final int CH186 = 186;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_11 */
    public static final int CH187 = 187;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_12 */
    public static final int CH188 = 188;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_13 */
    public static final int CH189 = 189;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_14 */
    public static final int CH190 = 190;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_15 */
    public static final int CH191 = 191;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_16 */
    public static final int CH192 = 192;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_17 */
    public static final int CH193 = 193;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_18 */
    public static final int CH194 = 194;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_19 */
    public static final int CH195 = 195;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_2 */
    public static final int CH196 = 196;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_20 */
    public static final int CH197 = 197;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_3 */
    public static final int CH198 = 198;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_4 */
    public static final int CH199 = 199;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_5 */
    public static final int CH200 = 200;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_6 */
    public static final int CH201 = 201;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_7 */
    public static final int CH202 = 202;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_8 */
    public static final int CH203 = 203;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_4_9 */
    public static final int CH204 = 204;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_5_1 */
    public static final int CH205 = 205;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_5_2 */
    public static final int CH206 = 206;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_1 */
    public static final int CH207 = 207;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_2 */
    public static final int CH208 = 208;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_3 */
    public static final int CH209 = 209;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_4 */
    public static final int CH210 = 210;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_5 */
    public static final int CH211 = 211;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_6 */
    public static final int CH212 = 212;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_7 */
    public static final int CH213 = 213;

    /** Bedieningscontroller op remmen - DBC_Cbrake_M_8_8 */
    public static final int CH214 = 214;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M */
    public static final int CH215 = 215;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_1_1 */
    public static final int CH216 = 216;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_1_2 */
    public static final int CH217 = 217;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_2_1 */
    public static final int CH218 = 218;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_2_2 */
    public static final int CH219 = 219;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_3_1 */
    public static final int CH220 = 220;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_3_2 */
    public static final int CH221 = 221;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_1 */
    public static final int CH222 = 222;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_10 */
    public static final int CH223 = 223;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_11 */
    public static final int CH224 = 224;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_12 */
    public static final int CH225 = 225;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_13 */
    public static final int CH226 = 226;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_14 */
    public static final int CH227 = 227;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_15 */
    public static final int CH228 = 228;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_16 */
    public static final int CH229 = 229;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_17 */
    public static final int CH230 = 230;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_18 */
    public static final int CH231 = 231;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_19 */
    public static final int CH232 = 232;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_2 */
    public static final int CH233 = 233;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_20 */
    public static final int CH234 = 234;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_3 */
    public static final int CH235 = 235;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_4 */
    public static final int CH236 = 236;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_5 */
    public static final int CH237 = 237;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_6 */
    public static final int CH238 = 238;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_7 */
    public static final int CH239 = 239;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_8 */
    public static final int CH240 = 240;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_4_9 */
    public static final int CH241 = 241;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_5_1 */
    public static final int CH242 = 242;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_5_2 */
    public static final int CH243 = 243;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_1 */
    public static final int CH244 = 244;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_2 */
    public static final int CH245 = 245;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_3 */
    public static final int CH246 = 246;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_4 */
    public static final int CH247 = 247;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_5 */
    public static final int CH248 = 248;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_6 */
    public static final int CH249 = 249;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_7 */
    public static final int CH250 = 250;

    /** Snelremming geactiveerd - DBC_CBrakeQuick_M_8_8 */
    public static final int CH251 = 251;

    /** Gewenste remkracht - DBC_CBrakingEffortInt_3_1 */
    public static final int CH252 = 252;

    /** Gewenste remkracht - DBC_CBrakingEffortInt_3_2 */
    public static final int CH253 = 253;

    /** - DBC_CBrakingEffortInt_5_1 */
    public static final int CH254 = 254;

    /** - DBC_CBrakingEffortInt_5_2 */
    public static final int CH255 = 255;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M */
    public static final int CH256 = 256;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_1_1 */
    public static final int CH257 = 257;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_1_2 */
    public static final int CH258 = 258;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_2_1 */
    public static final int CH259 = 259;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_2_2 */
    public static final int CH260 = 260;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_3_1 */
    public static final int CH261 = 261;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_3_2 */
    public static final int CH262 = 262;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_1 */
    public static final int CH263 = 263;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_10 */
    public static final int CH264 = 264;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_11 */
    public static final int CH265 = 265;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_12 */
    public static final int CH266 = 266;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_13 */
    public static final int CH267 = 267;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_14 */
    public static final int CH268 = 268;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_15 */
    public static final int CH269 = 269;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_16 */
    public static final int CH270 = 270;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_17 */
    public static final int CH271 = 271;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_18 */
    public static final int CH272 = 272;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_19 */
    public static final int CH273 = 273;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_2 */
    public static final int CH274 = 274;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_20 */
    public static final int CH275 = 275;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_3 */
    public static final int CH276 = 276;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_4 */
    public static final int CH277 = 277;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_5 */
    public static final int CH278 = 278;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_6 */
    public static final int CH279 = 279;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_7 */
    public static final int CH280 = 280;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_8 */
    public static final int CH281 = 281;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_4_9 */
    public static final int CH282 = 282;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_5_1 */
    public static final int CH283 = 283;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_5_2 */
    public static final int CH284 = 284;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_1 */
    public static final int CH285 = 285;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_2 */
    public static final int CH286 = 286;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_3 */
    public static final int CH287 = 287;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_4 */
    public static final int CH288 = 288;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_5 */
    public static final int CH289 = 289;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_6 */
    public static final int CH290 = 290;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_7 */
    public static final int CH291 = 291;

    /** Bedieningscontroller op rijden - DBC_Cdrive_M_8_8 */
    public static final int CH292 = 292;

    /** Wegsleepmode actief - DBC_CModeTowing_M_5_1 */
    public static final int CH293 = 293;

    /** Wegsleepmode actief - DBC_CModeTowing_M_5_2 */
    public static final int CH294 = 294;

    /** Aandrijvingsbesturing 1 inschakelcommando - DBC_CTCU1GrInTracConv1_3_1 */
    public static final int CH295 = 295;

    /** Aandrijvingsbesturing 1 inschakelcommando - DBC_CTCU2GrInTracConv2_3_2 */
    public static final int CH296 = 296;

    /** Richtwaarde tractievermogen - DBC_CTractiveEffort_3_1 */
    public static final int CH297 = 297;

    /** Richtwaarde tractievermogen - DBC_CTractiveEffort_3_2 */
    public static final int CH298 = 298;

    /** Snelheid - DBC_ITrainSpeedInt */
    public static final int CH299 = 299;

    /** Snelheid - DBC_ITrainSpeedInt_1_1 */
    public static final int CH300 = 300;

    /** Snelheid - DBC_ITrainSpeedInt_1_2 */
    public static final int CH301 = 301;

    /** Snelheid - DBC_ITrainSpeedInt_15 */
    public static final int CH302 = 302;

    /** Snelheid - DBC_ITrainSpeedInt_2_1 */
    public static final int CH303 = 303;

    /** Snelheid - DBC_ITrainSpeedInt_2_2 */
    public static final int CH304 = 304;

    /** Snelheid - DBC_ITrainSpeedInt_3_1 */
    public static final int CH305 = 305;

    /** Snelheid - DBC_ITrainSpeedInt_3_2 */
    public static final int CH306 = 306;

    /** Snelheid - DBC_ITrainSpeedInt_4_1 */
    public static final int CH307 = 307;

    /** Snelheid - DBC_ITrainSpeedInt_4_10 */
    public static final int CH308 = 308;

    /** Snelheid - DBC_ITrainSpeedInt_4_11 */
    public static final int CH309 = 309;

    /** Snelheid - DBC_ITrainSpeedInt_4_12 */
    public static final int CH310 = 310;

    /** Snelheid - DBC_ITrainSpeedInt_4_13 */
    public static final int CH311 = 311;

    /** Snelheid - DBC_ITrainSpeedInt_4_14 */
    public static final int CH312 = 312;

    /** Snelheid - DBC_ITrainSpeedInt_4_15 */
    public static final int CH313 = 313;

    /** Snelheid - DBC_ITrainSpeedInt_4_16 */
    public static final int CH314 = 314;

    /** Snelheid - DBC_ITrainSpeedInt_4_17 */
    public static final int CH315 = 315;

    /** Snelheid - DBC_ITrainSpeedInt_4_18 */
    public static final int CH316 = 316;

    /** Snelheid - DBC_ITrainSpeedInt_4_19 */
    public static final int CH317 = 317;

    /** Snelheid - DBC_ITrainSpeedInt_4_2 */
    public static final int CH318 = 318;

    /** Snelheid - DBC_ITrainSpeedInt_4_20 */
    public static final int CH319 = 319;

    /** Snelheid - DBC_ITrainSpeedInt_4_3 */
    public static final int CH320 = 320;

    /** Snelheid - DBC_ITrainSpeedInt_4_4 */
    public static final int CH321 = 321;

    /** Snelheid - DBC_ITrainSpeedInt_4_5 */
    public static final int CH322 = 322;

    /** Snelheid - DBC_ITrainSpeedInt_4_6 */
    public static final int CH323 = 323;

    /** Snelheid - DBC_ITrainSpeedInt_4_7 */
    public static final int CH324 = 324;

    /** Snelheid - DBC_ITrainSpeedInt_4_8 */
    public static final int CH325 = 325;

    /** Snelheid - DBC_ITrainSpeedInt_4_9 */
    public static final int CH326 = 326;

    /** Snelheid - DBC_ITrainSpeedInt_5_1 */
    public static final int CH327 = 327;

    /** Snelheid - DBC_ITrainSpeedInt_5_2 */
    public static final int CH328 = 328;

    /** Snelheid - DBC_ITrainSpeedInt_8_1 */
    public static final int CH329 = 329;

    /** Snelheid - DBC_ITrainSpeedInt_8_2 */
    public static final int CH330 = 330;

    /** Snelheid - DBC_ITrainSpeedInt_8_3 */
    public static final int CH331 = 331;

    /** Snelheid - DBC_ITrainSpeedInt_8_4 */
    public static final int CH332 = 332;

    /** Snelheid - DBC_ITrainSpeedInt_8_5 */
    public static final int CH333 = 333;

    /** Snelheid - DBC_ITrainSpeedInt_8_6 */
    public static final int CH334 = 334;

    /** Snelheid - DBC_ITrainSpeedInt_8_7 */
    public static final int CH335 = 335;

    /** Snelheid - DBC_ITrainSpeedInt_8_8 */
    public static final int CH336 = 336;

    /** Snelheid - DBC_ITrainSpeedInt_9 */
    public static final int CH337 = 337;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_1 */
    public static final int CH338 = 338;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_10 */
    public static final int CH339 = 339;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_11 */
    public static final int CH340 = 340;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_12 */
    public static final int CH341 = 341;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_13 */
    public static final int CH342 = 342;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_14 */
    public static final int CH343 = 343;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_15 */
    public static final int CH344 = 344;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_16 */
    public static final int CH345 = 345;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_17 */
    public static final int CH346 = 346;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_18 */
    public static final int CH347 = 347;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_19 */
    public static final int CH348 = 348;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_2 */
    public static final int CH349 = 349;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_20 */
    public static final int CH350 = 350;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_3 */
    public static final int CH351 = 351;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_4 */
    public static final int CH352 = 352;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_5 */
    public static final int CH353 = 353;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_6 */
    public static final int CH354 = 354;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_7 */
    public static final int CH355 = 355;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_8 */
    public static final int CH356 = 356;

    /** Stilstandsignaal - DBC_IZrSpdInd_4_9 */
    public static final int CH357 = 357;

    /** Aandrijvingsbesturing 1 uitgeschakeld - DBC_PRTcu1GrOut_3_1 */
    public static final int CH358 = 358;

    /** Aandrijvingsbesturing 1 uitgeschakeld - DBC_PRTcu2GrOut_3_2 */
    public static final int CH359 = 359;

    /** machinistenrem adequaat bediend - DRIVERBRAKE_APPLIED_SUFF */
    public static final int CH360 = 360;

    /** machinistenrem bediend - DRIVERBRAKE_OPERATED */
    public static final int CH361 = 361;

    /** status snelrem ingreep - EB_STATUS */
    public static final int CH362 = 362;

    /** Internal Error Code - ERROR_CODE_INTERNAL */
    public static final int CH363 = 363;

    /** internal error code 1 - ERRORCODE_1 */
    public static final int CH364 = 364;

    /** internal error code 2 - ERRORCODE_2 */
    public static final int CH365 = 365;

    /** internal error code 3 - ERRORCODE_3 */
    public static final int CH366 = 366;

    /** - GW_IGwOrientation */
    public static final int CH367 = 367;

    /** Buitentemperatuur - HAC_ITempOutsideInt */
    public static final int CH368 = 368;

    /** Buitentemperatuur - HAC_ITempOutsideInt_1_1 */
    public static final int CH369 = 369;

    /** Buitentemperatuur - HAC_ITempOutsideInt_1_2 */
    public static final int CH370 = 370;

    /** Buitentemperatuur - HAC_ITempOutsideInt_2_1 */
    public static final int CH371 = 371;

    /** Buitentemperatuur - HAC_ITempOutsideInt_2_2 */
    public static final int CH372 = 372;

    /** Buitentemperatuur - HAC_ITempOutsideInt_3_1 */
    public static final int CH373 = 373;

    /** Buitentemperatuur - HAC_ITempOutsideInt_3_2 */
    public static final int CH374 = 374;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_1 */
    public static final int CH375 = 375;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_10 */
    public static final int CH376 = 376;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_11 */
    public static final int CH377 = 377;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_12 */
    public static final int CH378 = 378;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_13 */
    public static final int CH379 = 379;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_14 */
    public static final int CH380 = 380;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_15 */
    public static final int CH381 = 381;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_16 */
    public static final int CH382 = 382;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_17 */
    public static final int CH383 = 383;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_18 */
    public static final int CH384 = 384;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_19 */
    public static final int CH385 = 385;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_2 */
    public static final int CH386 = 386;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_20 */
    public static final int CH387 = 387;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_3 */
    public static final int CH388 = 388;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_4 */
    public static final int CH389 = 389;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_5 */
    public static final int CH390 = 390;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_6 */
    public static final int CH391 = 391;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_7 */
    public static final int CH392 = 392;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_8 */
    public static final int CH393 = 393;

    /** Buitentemperatuur - HAC_ITempOutsideInt_4_9 */
    public static final int CH394 = 394;

    /** Buitentemperatuur - HAC_ITempOutsideInt_5_1 */
    public static final int CH395 = 395;

    /** Buitentemperatuur - HAC_ITempOutsideInt_5_2 */
    public static final int CH396 = 396;

    /** Buitentemperatuur - HAC_ITempOutsideInt_9 */
    public static final int CH397 = 397;

    /** Airco-installatie passagiersruimte 1 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC1_ITempAirMixedInt */
    public static final int CH398 = 398;

    /** Airco-installatie passagiersruimte 1 kanaaltemperatuur kanaal 1 - HVAC1_ITempDuctAir1Int */
    public static final int CH399 = 399;

    /** Airco-installatie passagiersruimte 1 kanaaltemperatuur kanaal 2 - HVAC1_ITempDuctAir2Int */
    public static final int CH400 = 400;

    /** Airco-installatie passagiersruimte 1 binnentemperatuur - HVAC1_ITempInComInt */
    public static final int CH401 = 401;

    /** Airco-installatie passagiersruimte 1 buitentemperatuur - HVAC1_ITempOutsideInt */
    public static final int CH402 = 402;

    /** Airco-installatie passagiersruimte 1 gewenste waarde kanaaltemperatuur - HVAC1_ITempSetpActDuctInt */
    public static final int CH403 = 403;

    /** Airco-installatie passagiersruimte 1 gewenste waarde binnentemperatuur - HVAC1_ITempSetpActInt */
    public static final int CH404 = 404;

    /** Airco-installatie passagiersruimte 2 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC2_ITempAirMixedInt */
    public static final int CH405 = 405;

    /** Airco-installatie passagiersruimte 2 kanaaltemperatuur kanaal 1 - HVAC2_ITempDuctAir1Int */
    public static final int CH406 = 406;

    /** Airco-installatie passagiersruimte 2 kanaaltemperatuur kanaal 2 - HVAC2_ITempDuctAir2Int */
    public static final int CH407 = 407;

    /** Airco-installatie passagiersruimte 2 binnentemperatuur - HVAC2_ITempInComInt */
    public static final int CH408 = 408;

    /** Airco-installatie passagiersruimte 2 buitentemperatuur - HVAC2_ITempOutsideInt */
    public static final int CH409 = 409;

    /** Airco-installatie passagiersruimte 2 gewenste waarde kanaaltemperatuur - HVAC2_ITempSetpActDuctInt */
    public static final int CH410 = 410;

    /** Airco-installatie passagiersruimte 2 gewenste waarde binnentemperatuur - HVAC2_ITempSetpActInt */
    public static final int CH411 = 411;

    /** Airco-installatie passagiersruimte 3 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC3_ITempAirMixedInt */
    public static final int CH412 = 412;

    /** Airco-installatie passagiersruimte 3 kanaaltemperatuur kanaal 1 - HVAC3_ITempDuctAir1Int */
    public static final int CH413 = 413;

    /** Airco-installatie passagiersruimte 3 kanaaltemperatuur kanaal 2 - HVAC3_ITempDuctAir2Int */
    public static final int CH414 = 414;

    /** Airco-installatie passagiersruimte 3 binnentemperatuur - HVAC3_ITempInComInt */
    public static final int CH415 = 415;

    /** Airco-installatie passagiersruimte 3 buitentemperatuur - HVAC3_ITempOutsideInt */
    public static final int CH416 = 416;

    /** Airco-installatie passagiersruimte 3 gewenste waarde kanaaltemperatuur - HVAC3_ITempSetpActDuctInt */
    public static final int CH417 = 417;

    /** Airco-installatie passagiersruimte 3 gewenste waarde binnentemperatuur - HVAC3_ITempSetpActInt */
    public static final int CH418 = 418;

    /** Airco-installatie passagiersruimte 4 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC4_ITempAirMixedInt */
    public static final int CH419 = 419;

    /** Airco-installatie passagiersruimte 4 kanaaltemperatuur kanaal 1 - HVAC4_ITempDuctAir1Int */
    public static final int CH420 = 420;

    /** Airco-installatie passagiersruimte 4 kanaaltemperatuur kanaal 2 - HVAC4_ITempDuctAir2Int */
    public static final int CH421 = 421;

    /** Airco-installatie passagiersruimte 4 binnentemperatuur - HVAC4_ITempInComInt */
    public static final int CH422 = 422;

    /** Airco-installatie passagiersruimte 4 buitentemperatuur - HVAC4_ITempOutsideInt */
    public static final int CH423 = 423;

    /** Airco-installatie passagiersruimte 4 gewenste waarde kanaaltemperatuur - HVAC4_ITempSetpActDuctInt */
    public static final int CH424 = 424;

    /** Airco-installatie passagiersruimte 4 gewenste waarde binnentemperatuur - HVAC4_ITempSetpActInt */
    public static final int CH425 = 425;

    /** Airco-installatie passagiersruimte 5 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC5_ITempAirMixedInt */
    public static final int CH426 = 426;

    /** Airco-installatie passagiersruimte 5 kanaaltemperatuur kanaal 1 - HVAC5_ITempDuctAir1Int */
    public static final int CH427 = 427;

    /** Airco-installatie passagiersruimte 5 kanaaltemperatuur kanaal 2 - HVAC5_ITempDuctAir2Int */
    public static final int CH428 = 428;

    /** Airco-installatie passagiersruimte 5 binnentemperatuur - HVAC5_ITempInComInt */
    public static final int CH429 = 429;

    /** Airco-installatie passagiersruimte 5 buitentemperatuur - HVAC5_ITempOutsideInt */
    public static final int CH430 = 430;

    /** Airco-installatie passagiersruimte 5 gewenste waarde kanaaltemperatuur - HVAC5_ITempSetpActDuctInt */
    public static final int CH431 = 431;

    /** Airco-installatie passagiersruimte 5 gewenste waarde binnentemperatuur - HVAC5_ITempSetpActInt */
    public static final int CH432 = 432;

    /** Airco-installatie passagiersruimte 6 mengluchttemperatuur voor verdamper/verwarmingselement - HVAC6_ITempAirMixedInt */
    public static final int CH433 = 433;

    /** Airco-installatie passagiersruimte 6 kanaaltemperatuur kanaal 1 - HVAC6_ITempDuctAir1Int */
    public static final int CH434 = 434;

    /** Airco-installatie passagiersruimte 6 kanaaltemperatuur kanaal 2 - HVAC6_ITempDuctAir2Int */
    public static final int CH435 = 435;

    /** Airco-installatie passagiersruimte 6 binnentemperatuur - HVAC6_ITempInComInt */
    public static final int CH436 = 436;

    /** Airco-installatie passagiersruimte 6 buitentemperatuur - HVAC6_ITempOutsideInt */
    public static final int CH437 = 437;

    /** Airco-installatie passagiersruimte 6 gewenste waarde kanaaltemperatuur - HVAC6_ITempSetpActDuctInt */
    public static final int CH438 = 438;

    /** Airco-installatie passagiersruimte 6 gewenste waarde binnentemperatuur - HVAC6_ITempSetpActInt */
    public static final int CH439 = 439;

    /** Airco-installatie bestuurderscabine 1 kanaaltemperatuur - HVCC1_ITempDuctAir1Int */
    public static final int CH440 = 440;

    /** Airco-installatie bestuurderscabine 1 binnentemperatuur - HVCC1_ITempInComInt */
    public static final int CH441 = 441;

    /** Airco-installatie bestuurderscabine 1 buitentemperatuur - HVCC1_ITempOutsideInt */
    public static final int CH442 = 442;

    /** Airco-installatie bestuurderscabine 1 gewenste waarde buitentemperatuur - HVCC1_ITempSetpActDuctInt */
    public static final int CH443 = 443;

    /** Airco-installatie bestuurderscabine 1 gewenste waarde binnentemperatuur - HVCC1_ITempSetpActInt */
    public static final int CH444 = 444;

    /** Airco-installatie bestuurderscabine 2 kanaaltemperatuur - HVCC2_ITempDuctAir1Int */
    public static final int CH445 = 445;

    /** Airco-installatie bestuurderscabine 2 binnentemperatuur - HVCC2_ITempInComInt */
    public static final int CH446 = 446;

    /** Airco-installatie bestuurderscabine 2 buitentemperatuur - HVCC2_ITempOutsideInt */
    public static final int CH447 = 447;

    /** Airco-installatie bestuurderscabine 2 gewenste waarde buitentemperatuur - HVCC2_ITempSetpActDuctInt */
    public static final int CH448 = 448;

    /** Airco-installatie bestuurderscabine 2 gewenste waarde binnentemperatuur - HVCC2_ITempSetpActInt */
    public static final int CH449 = 449;

    /** - INadiCtrlInfo1 */
    public static final int CH450 = 450;

    /** - INadiCtrlInfo2 */
    public static final int CH451 = 451;

    /** - INadiCtrlInfo3 */
    public static final int CH452 = 452;

    /** - INadiNumberEntries */
    public static final int CH453 = 453;

    /** - INadiTcnAddr1 */
    public static final int CH454 = 454;

    /** - INadiTcnAddr2 */
    public static final int CH455 = 455;

    /** - INadiTcnAddr3 */
    public static final int CH456 = 456;

    /** - INadiTopoCount */
    public static final int CH457 = 457;

    /** - INadiUicAddr1 */
    public static final int CH458 = 458;

    /** - INadiUicAddr2 */
    public static final int CH459 = 459;

    /** - INadiUicAddr3 */
    public static final int CH460 = 460;

    /** interne fout code - INTERNAL_ERROR_CODE */
    public static final int CH461 = 461;

    /** Internal State - INTERNAL_STATE */
    public static final int CH462 = 462;

    /** - ITrainsetNumber1 */
    public static final int CH463 = 463;

    /** - ITrainsetNumber2 */
    public static final int CH464 = 464;

    /** - ITrainsetNumber3 */
    public static final int CH465 = 465;

    /** GSM signaalsterkte - MCG_IGsmSigStrength */
    public static final int CH466 = 466;

    /** Koppelautomaat - MIO_IContBusBar400On */
    public static final int CH467 = 467;

    /** Koppelautomaat is IN - MIO_IContBusBar400On_1_1 */
    public static final int CH468 = 468;

    /** Koppelautomaat is IN - MIO_IContBusBar400On_1_2 */
    public static final int CH469 = 469;

    /** Koppelautomaat - MIO_IContBusBar400On_2_1 */
    public static final int CH470 = 470;

    /** Koppelautomaat - MIO_IContBusBar400On_2_2 */
    public static final int CH471 = 471;

    /** Koppelautomaat - MIO_IContBusBar400On_3_1 */
    public static final int CH472 = 472;

    /** Koppelautomaat - MIO_IContBusBar400On_3_2 */
    public static final int CH473 = 473;

    /** Koppelautomaat - MIO_IContBusBar400On_8_1 */
    public static final int CH474 = 474;

    /** Koppelautomaat - MIO_IContBusBar400On_8_2 */
    public static final int CH475 = 475;

    /** Koppelautomaat - MIO_IContBusBar400On_8_3 */
    public static final int CH476 = 476;

    /** Koppelautomaat - MIO_IContBusBar400On_8_4 */
    public static final int CH477 = 477;

    /** Koppelautomaat - MIO_IContBusBar400On_8_5 */
    public static final int CH478 = 478;

    /** Koppelautomaat - MIO_IContBusBar400On_8_6 */
    public static final int CH479 = 479;

    /** Koppelautomaat - MIO_IContBusBar400On_8_7 */
    public static final int CH480 = 480;

    /** Koppelautomaat - MIO_IContBusBar400On_8_8 */
    public static final int CH481 = 481;

    /** Koppelautomaat - MIO_IContBusBar400On_9 */
    public static final int CH482 = 482;

    /** Snelschakelaar UIT - MIO_IHscbOff */
    public static final int CH483 = 483;

    /** Snelschakelaar UIT - MIO_IHscbOff_1_1 */
    public static final int CH484 = 484;

    /** Snelschakelaar UIT - MIO_IHscbOff_1_2 */
    public static final int CH485 = 485;

    /** Snelschakelaar UIT - MIO_IHscbOff_2_1 */
    public static final int CH486 = 486;

    /** Snelschakelaar UIT - MIO_IHscbOff_2_2 */
    public static final int CH487 = 487;

    /** Snelschakelaar UIT - MIO_IHscbOff_3_1 */
    public static final int CH488 = 488;

    /** Snelschakelaar UIT - MIO_IHscbOff_3_2 */
    public static final int CH489 = 489;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_1 */
    public static final int CH490 = 490;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_2 */
    public static final int CH491 = 491;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_3 */
    public static final int CH492 = 492;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_4 */
    public static final int CH493 = 493;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_5 */
    public static final int CH494 = 494;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_6 */
    public static final int CH495 = 495;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_7 */
    public static final int CH496 = 496;

    /** Snelschakelaar UIT - MIO_IHscbOff_8_8 */
    public static final int CH497 = 497;

    /** Snelschakelaar IN - MIO_IHscbOn */
    public static final int CH498 = 498;

    /** Snelschakelaar IN - MIO_IHscbOn_1_1 */
    public static final int CH499 = 499;

    /** Snelschakelaar IN - MIO_IHscbOn_1_2 */
    public static final int CH500 = 500;

    /** Snelschakelaar IN - MIO_IHscbOn_2_1 */
    public static final int CH501 = 501;

    /** Snelschakelaar IN - MIO_IHscbOn_2_2 */
    public static final int CH502 = 502;

    /** Snelschakelaar IN - MIO_IHscbOn_3_1 */
    public static final int CH503 = 503;

    /** Snelschakelaar IN - MIO_IHscbOn_3_2 */
    public static final int CH504 = 504;

    /** Snelschakelaar IN - MIO_IHscbOn_8_1 */
    public static final int CH505 = 505;

    /** Snelschakelaar IN - MIO_IHscbOn_8_2 */
    public static final int CH506 = 506;

    /** Snelschakelaar IN - MIO_IHscbOn_8_3 */
    public static final int CH507 = 507;

    /** Snelschakelaar IN - MIO_IHscbOn_8_4 */
    public static final int CH508 = 508;

    /** Snelschakelaar IN - MIO_IHscbOn_8_5 */
    public static final int CH509 = 509;

    /** Snelschakelaar IN - MIO_IHscbOn_8_6 */
    public static final int CH510 = 510;

    /** Snelschakelaar IN - MIO_IHscbOn_8_7 */
    public static final int CH511 = 511;

    /** Snelschakelaar IN - MIO_IHscbOn_8_8 */
    public static final int CH512 = 512;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On */
    public static final int CH513 = 513;

    /** Stroomafnemer 1 is op - MIO_IPantoPrssSw1On_1_1 */
    public static final int CH514 = 514;

    /** Stroomafnemer 1 is op - MIO_IPantoPrssSw1On_1_2 */
    public static final int CH515 = 515;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_2_1 */
    public static final int CH516 = 516;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_2_2 */
    public static final int CH517 = 517;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_3_1 */
    public static final int CH518 = 518;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_3_2 */
    public static final int CH519 = 519;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_1 */
    public static final int CH520 = 520;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_2 */
    public static final int CH521 = 521;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_3 */
    public static final int CH522 = 522;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_4 */
    public static final int CH523 = 523;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_5 */
    public static final int CH524 = 524;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_6 */
    public static final int CH525 = 525;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_7 */
    public static final int CH526 = 526;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_8_8 */
    public static final int CH527 = 527;

    /** Stroomafnemer 1 - MIO_IPantoPrssSw1On_9 */
    public static final int CH528 = 528;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On */
    public static final int CH529 = 529;

    /** Stroomafnemer 2 is op - MIO_IPantoPrssSw2On_1_1 */
    public static final int CH530 = 530;

    /** Stroomafnemer 2 is op - MIO_IPantoPrssSw2On_1_2 */
    public static final int CH531 = 531;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_2_1 */
    public static final int CH532 = 532;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_2_2 */
    public static final int CH533 = 533;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_3_1 */
    public static final int CH534 = 534;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_3_2 */
    public static final int CH535 = 535;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_1 */
    public static final int CH536 = 536;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_2 */
    public static final int CH537 = 537;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_3 */
    public static final int CH538 = 538;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_4 */
    public static final int CH539 = 539;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_5 */
    public static final int CH540 = 540;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_6 */
    public static final int CH541 = 541;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_7 */
    public static final int CH542 = 542;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_8_8 */
    public static final int CH543 = 543;

    /** Stroomafnemer 2 - MIO_IPantoPrssSw2On_9 */
    public static final int CH544 = 544;

    /** eb status - MMI_EB_STATUS */
    public static final int CH545 = 545;

    /** cabine status - MMI_M_ACTIVE_CABIN */
    public static final int CH546 = 546;

    /** Current Level - MMI_M_LEVEL */
    public static final int CH547 = 547;

    /** Current Mode - MMI_M_MODE */
    public static final int CH548 = 548;

    /** MMI Warning State - MMI_M_WARNING */
    public static final int CH549 = 549;

    /** sb status - MMI_SB_STATUS */
    public static final int CH550 = 550;

    /** toegestane snelheid - MMI_V_PERMITTED */
    public static final int CH551 = 551;

    /** treinsnelheid - MMI_V_TRAIN */
    public static final int CH552 = 552;

    /** STM 1: NID_STM - NID_STM_1 */
    public static final int CH553 = 553;

    /** STM 2: NID_STM - NID_STM_2 */
    public static final int CH554 = 554;

    /** STM 3: NID_STM - NID_STM_3 */
    public static final int CH555 = 555;

    /** STM 4: NID_STM - NID_STM_4 */
    public static final int CH556 = 556;

    /** STM 5: NID_STM - NID_STM_5 */
    public static final int CH557 = 557;

    /** STM 6: NID_STM - NID_STM_6 */
    public static final int CH558 = 558;

    /** STM 7: NID_STM - NID_STM_7 */
    public static final int CH559 = 559;

    /** STM 8: NID_STM - NID_STM_8 */
    public static final int CH560 = 560;

    /** STM in Data Available Mode - NID_STM_DA */
    public static final int CH561 = 561;

    /** One STM in DA Mode - ONE_STM_DA */
    public static final int CH562 = 562;

    /** One STM in HS Mode - ONE_STM_HS */
    public static final int CH563 = 563;

    /** PB-Addr - PB_ADDR */
    public static final int CH564 = 564;

    /** toegestane snelheid - PERMITTED_SPEED */
    public static final int CH565 = 565;

    /** - Q_ODO */
    public static final int CH566 = 566;

    /** - REPORTED_DIRECTION */
    public static final int CH567 = 567;

    /** Speed reported by SDP as V_NOM - REPORTED_SPEED */
    public static final int CH568 = 568;

    /** Old V_NOM Speed value reported by SDP - REPORTED_SPEED_HISTORIC */
    public static final int CH569 = 569;

    /** - ROTATION_DIRECTION_SDU1 */
    public static final int CH570 = 570;

    /** - ROTATION_DIRECTION_SDU2 */
    public static final int CH571 = 571;

    /** SAP - SAP */
    public static final int CH572 = 572;

    /** status bedrijfsremming - SB_STATUS */
    public static final int CH573 = 573;

    /** The configured rotation direction of SDU 1 - SDU_DIRECTION_SDU1 */
    public static final int CH574 = 574;

    /** The configured rotation direction of SDU 2 - SDU_DIRECTION_SDU2 */
    public static final int CH575 = 575;

    /** Faut Tacho 1 - SENSOR_ERROR_STATUS1_TACHO1 */
    public static final int CH576 = 576;

    /** Faut Tacho 2 - SENSOR_ERROR_STATUS2_TACHO2 */
    public static final int CH577 = 577;

    /** Faut Radar 1 - SENSOR_ERROR_STATUS3_RADAR1 */
    public static final int CH578 = 578;

    /** Faut Radar 2 - SENSOR_ERROR_STATUS4_RADAR2 */
    public static final int CH579 = 579;

    /** Old Speed reported by Radar 1 - SENSOR_SPEED_HISTORIC_RADAR1 */
    public static final int CH580 = 580;

    /** Old Speed reported by Radar 2 - SENSOR_SPEED_HISTORIC_RADAR2 */
    public static final int CH581 = 581;

    /** Old Speed reported by Tacho 1 - SENSOR_SPEED_HISTORIC_TACHO1 */
    public static final int CH582 = 582;

    /** Old Speed reported by Tacho 2 - SENSOR_SPEED_HISTORIC_TACHO2 */
    public static final int CH583 = 583;

    /** Speed reported by Radar 1 - SENSOR_SPEED_RADAR1 */
    public static final int CH584 = 584;

    /** Speed reported by Radar 2 - SENSOR_SPEED_RADAR2 */
    public static final int CH585 = 585;

    /** Speed reported by Tacho 1 - SENSOR_SPEED_TACHO1 */
    public static final int CH586 = 586;

    /** Speed reported by Tacho 2 - SENSOR_SPEED_TACHO2 */
    public static final int CH587 = 587;

    /** Information if slip/slide of Axle 1 by ACC diff. - SLIP_SLIDE_ACC_AXLE1 */
    public static final int CH588 = 588;

    /** Information if slip/slide of Axle 2 by ACC diff. - SLIP_SLIDE_ACC_AXLE2 */
    public static final int CH589 = 589;

    /** - SLIP_SLIDE_AXLE1 */
    public static final int CH590 = 590;

    /** - SLIP_SLIDE_AXLE2 */
    public static final int CH591 = 591;

    /** Information if slip/slide of Axle 1 by radar diff. - SLIP_SLIDE_RADAR_DIFF_AXLE1 */
    public static final int CH592 = 592;

    /** Information if slip/slide of Axle 2 by radar diff. - SLIP_SLIDE_RADAR_DIFF_AXLE2 */
    public static final int CH593 = 593;

    /** Slip/Slide State - SLIP_SLIDE_STATUS */
    public static final int CH594 = 594;

    /** Information if slip/slide of Axle 1 by SDU diff. - SLIP_SLIDE_TACHO_DIFF_AXLE1 */
    public static final int CH595 = 595;

    /** Information if slip/slide of Axle 2 by SDU diff. - SLIP_SLIDE_TACHO_DIFF_AXLE2 */
    public static final int CH596 = 596;

    /** Software Version - SOFTWARE_VERSION */
    public static final int CH597 = 597;

    /** Type of SPL function - SPL_FUNC_ID */
    public static final int CH598 = 598;

    /** SPL Result - SPL_RETURN */
    public static final int CH599 = 599;

    /** STM-status - STM_STATE */
    public static final int CH600 = 600;

    /** STM-substatus - STM_SUB_STATE */
    public static final int CH601 = 601;

    /** bewaking status - SUPERVISION_STATE */
    public static final int CH602 = 602;

    /** - TC_CCorrectOrient_M */
    public static final int CH603 = 603;

    /** - TC_IConsistOrient */
    public static final int CH604 = 604;

    /** - TC_IGwOrient */
    public static final int CH605 = 605;

    /** Treinlengte - TC_INumConsists */
    public static final int CH606 = 606;

    /** Treinlengte - TC_INumConsists_1_1 */
    public static final int CH607 = 607;

    /** Treinlengte - TC_INumConsists_1_2 */
    public static final int CH608 = 608;

    /** Treinlengte - TC_INumConsists_2_1 */
    public static final int CH609 = 609;

    /** Treinlengte - TC_INumConsists_2_2 */
    public static final int CH610 = 610;

    /** Treinlengte - TC_INumConsists_3_1 */
    public static final int CH611 = 611;

    /** Treinlengte - TC_INumConsists_3_2 */
    public static final int CH612 = 612;

    /** Treinlengte - TC_INumConsists_4_1 */
    public static final int CH613 = 613;

    /** Treinlengte - TC_INumConsists_4_10 */
    public static final int CH614 = 614;

    /** Treinlengte - TC_INumConsists_4_11 */
    public static final int CH615 = 615;

    /** Treinlengte - TC_INumConsists_4_12 */
    public static final int CH616 = 616;

    /** Treinlengte - TC_INumConsists_4_13 */
    public static final int CH617 = 617;

    /** Treinlengte - TC_INumConsists_4_14 */
    public static final int CH618 = 618;

    /** Treinlengte - TC_INumConsists_4_15 */
    public static final int CH619 = 619;

    /** Treinlengte - TC_INumConsists_4_16 */
    public static final int CH620 = 620;

    /** Treinlengte - TC_INumConsists_4_17 */
    public static final int CH621 = 621;

    /** Treinlengte - TC_INumConsists_4_18 */
    public static final int CH622 = 622;

    /** Treinlengte - TC_INumConsists_4_19 */
    public static final int CH623 = 623;

    /** Treinlengte - TC_INumConsists_4_2 */
    public static final int CH624 = 624;

    /** Treinlengte - TC_INumConsists_4_20 */
    public static final int CH625 = 625;

    /** Treinlengte - TC_INumConsists_4_3 */
    public static final int CH626 = 626;

    /** Treinlengte - TC_INumConsists_4_4 */
    public static final int CH627 = 627;

    /** Treinlengte - TC_INumConsists_4_5 */
    public static final int CH628 = 628;

    /** Treinlengte - TC_INumConsists_4_6 */
    public static final int CH629 = 629;

    /** Treinlengte - TC_INumConsists_4_7 */
    public static final int CH630 = 630;

    /** Treinlengte - TC_INumConsists_4_8 */
    public static final int CH631 = 631;

    /** Treinlengte - TC_INumConsists_4_9 */
    public static final int CH632 = 632;

    /** Treinlengte - TC_INumConsists_5_1 */
    public static final int CH633 = 633;

    /** Treinlengte - TC_INumConsists_5_2 */
    public static final int CH634 = 634;

    /** Treinlengte - TC_INumConsists_8_1 */
    public static final int CH635 = 635;

    /** Treinlengte - TC_INumConsists_8_2 */
    public static final int CH636 = 636;

    /** Treinlengte - TC_INumConsists_8_3 */
    public static final int CH637 = 637;

    /** Treinlengte - TC_INumConsists_8_4 */
    public static final int CH638 = 638;

    /** Treinlengte - TC_INumConsists_8_5 */
    public static final int CH639 = 639;

    /** Treinlengte - TC_INumConsists_8_6 */
    public static final int CH640 = 640;

    /** Treinlengte - TC_INumConsists_8_7 */
    public static final int CH641 = 641;

    /** Treinlengte - TC_INumConsists_8_8 */
    public static final int CH642 = 642;

    /** Treinlengte - TC_INumConsists_9 */
    public static final int CH643 = 643;

    /** - TC_ITclOrient */
    public static final int CH644 = 644;

    /** status tractie uitschakeling - TCO_STATUS */
    public static final int CH645 = 645;

    /** Antislip-/ slingerbeveiliging aandrijvingsbesturing 2 in ingreep - TCU1_IEdSliding_3_1 */
    public static final int CH646 = 646;

    /** Antislip-/ slingerbeveiliging aandrijvingsbesturing 1 in ingreep - TCU1_IEdSliding_5_1 */
    public static final int CH647 = 647;

    /** Bovenleidingsspanning van aandrijvingsbesturing 1 - TCU1_ILineVoltage_3_1 */
    public static final int CH648 = 648;

    /** Bovenleidingsspanning van aandrijvingsbesturing 1 - TCU1_ILineVoltage_5_1 */
    public static final int CH649 = 649;

    /** Bovenleidingsspanning van aandrijvingsbesturing 1 - TCU1_ILineVoltage_5_2 */
    public static final int CH650 = 650;

    /** Netstroombegrenzing aandrijvingsbesturing 2 is actief - TCU1_IPowerLimit_3_1 */
    public static final int CH651 = 651;

    /** Meetwaarde van de tractie-/ remkracht aandrijvingsbesturings 1 - TCU1_ITracBrActualInt_3_1 */
    public static final int CH652 = 652;

    /** Antislip-/ slingerbeveiliging aandrijvingsbesturing 2 in ingreep - TCU2_IEdSliding_3_2 */
    public static final int CH653 = 653;

    /** Antislip-/ slingerbeveiliging aandrijvingsbesturing 2 in ingreep - TCU2_IEdSliding_5_2 */
    public static final int CH654 = 654;

    /** Bovenleidingsspanning van aandrijvingsbesturing 2 - TCU2_ILineVoltage_3_2 */
    public static final int CH655 = 655;

    /** Bovenleidingsspanning van aandrijvingsbesturing 2 - TCU2_ILineVoltage_5_1 */
    public static final int CH656 = 656;

    /** Bovenleidingsspanning van aandrijvingsbesturing 2 - TCU2_ILineVoltage_5_2 */
    public static final int CH657 = 657;

    /** Netstroombegrenzing aandrijvingsbesturing 2 is actief - TCU2_IPowerLimit_3_2 */
    public static final int CH658 = 658;

    /** Meetwaarde van de tractie-/ remkracht aandrijvingsbesturings 2 - TCU2_ITracBrActualInt_3_2 */
    public static final int CH659 = 659;

    /** ATP baancode - TRACK_SIGNAL */
    public static final int CH660 = 660;

    /** Aandrijving 1 geblokkeerd - DBC_PRTcu1GrOut */
    public static final int CH661 = 661;

    /** Aandrijving 2 geblokkeerd - DBC_PRTcu2GrOut */
    public static final int CH662 = 662;

    /** Snelschakelaar vrijgegeven - MPW_PRHscbEnable */
    public static final int CH663 = 663;

    /** MPW_IPnt1UpDrvCount - MPW_IPnt1UpDrvCount */
    public static final int CH664 = 664;

    /** MPW_IPnt2UpDrvCount - MPW_IPnt2UpDrvCount */
    public static final int CH665 = 665;

    /** MPW_IPnt1UpCount - MPW_IPnt1UpCount */
    public static final int CH666 = 666;

    /** MPW_IPnt2UpCount - MPW_IPnt2UpCount */
    public static final int CH667 = 667;

    /** MPW_IPnt1KmCount - MPW_IPnt1KmCount */
    public static final int CH668 = 668;

    /** MPW_IPnt2KmCount - MPW_IPnt2KmCount */
    public static final int CH669 = 669;

    /** DBC_IKmCounter - DBC_IKmCounter */
    public static final int CH670 = 670;

    /** AUXY_IAuxAirCOnCnt - AUXY_IAuxAirCOnCnt */
    public static final int CH671 = 671;

    /** MPW_IOnHscb - MPW_IOnHscb */
    public static final int CH672 = 672;

    /** AUXY_IAirCSwOnCount - AUXY_IAirCSwOnCount */
    public static final int CH673 = 673;

    /** AUXY_IAirCOnCount - AUXY_IAirCOnCount */
    public static final int CH674 = 674;

    /** AUXY_IAuxAirCSwOnCnt - AUXY_IAuxAirCSwOnCnt */
    public static final int CH675 = 675;

    /** MPW_PRPantoEnable - MPW_PRPantoEnable */
    public static final int CH676 = 676;

    /** Reserve - DIA_Reserve1_AUX1 */
    public static final int CH677 = 677;

    /** Reserve - DIA_Reserve1_AUX2 */
    public static final int CH678 = 678;

    /** Reserve - DIA_Reserve1_BCHA_1 */
    public static final int CH679 = 679;

    /** Reserve - DIA_Reserve1_BCHA_2 */
    public static final int CH680 = 680;

    /** Reserve - DIA_Reserve1_BCU_1 */
    public static final int CH681 = 681;

    /** Reserve - DIA_Reserve1_BCU_2 */
    public static final int CH682 = 682;

    /** Reserve - DIA_Reserve1_CCUO */
    public static final int CH683 = 683;

    /** Reserve - DIA_Reserve1_DCU_1 */
    public static final int CH684 = 684;

    /** Reserve - DIA_Reserve1_DCU_10 */
    public static final int CH685 = 685;

    /** Reserve - DIA_Reserve1_DCU_11 */
    public static final int CH686 = 686;

    /** Reserve - DIA_Reserve1_DCU_12 */
    public static final int CH687 = 687;

    /** Reserve - DIA_Reserve1_DCU_13 */
    public static final int CH688 = 688;

    /** Reserve - DIA_Reserve1_DCU_14 */
    public static final int CH689 = 689;

    /** Reserve - DIA_Reserve1_DCU_15 */
    public static final int CH690 = 690;

    /** Reserve - DIA_Reserve1_DCU_16 */
    public static final int CH691 = 691;

    /** Reserve - DIA_Reserve1_DCU_17 */
    public static final int CH692 = 692;

    /** Reserve - DIA_Reserve1_DCU_18 */
    public static final int CH693 = 693;

    /** Reserve - DIA_Reserve1_DCU_19 */
    public static final int CH694 = 694;

    /** Reserve - DIA_Reserve1_DCU_2 */
    public static final int CH695 = 695;

    /** Reserve - DIA_Reserve1_DCU_20 */
    public static final int CH696 = 696;

    /** Reserve - DIA_Reserve1_DCU_3 */
    public static final int CH697 = 697;

    /** Reserve - DIA_Reserve1_DCU_4 */
    public static final int CH698 = 698;

    /** Reserve - DIA_Reserve1_DCU_5 */
    public static final int CH699 = 699;

    /** Reserve - DIA_Reserve1_DCU_6 */
    public static final int CH700 = 700;

    /** Reserve - DIA_Reserve1_DCU_7 */
    public static final int CH701 = 701;

    /** Reserve - DIA_Reserve1_DCU_8 */
    public static final int CH702 = 702;

    /** Reserve - DIA_Reserve1_DCU_9 */
    public static final int CH703 = 703;

    /** Reserve - DIA_Reserve1_HVAC_1 */
    public static final int CH704 = 704;

    /** Reserve - DIA_Reserve1_HVAC_2 */
    public static final int CH705 = 705;

    /** Reserve - DIA_Reserve1_HVAC_3 */
    public static final int CH706 = 706;

    /** Reserve - DIA_Reserve1_HVAC_4 */
    public static final int CH707 = 707;

    /** Reserve - DIA_Reserve1_HVAC_5 */
    public static final int CH708 = 708;

    /** Reserve - DIA_Reserve1_HVAC_6 */
    public static final int CH709 = 709;

    /** Reserve - DIA_Reserve1_HVCC_1 */
    public static final int CH710 = 710;

    /** Reserve - DIA_Reserve1_HVCC_2 */
    public static final int CH711 = 711;

    /** Reserve - DIA_Reserve1_PIS */
    public static final int CH712 = 712;

    /** Reserve - DIA_Reserve1_TCU_1 */
    public static final int CH713 = 713;

    /** Reserve - DIA_Reserve1_TCU_2 */
    public static final int CH714 = 714;

    /** reserved - reserved */
    public static final int CH715 = 715;

    /** Deur 1 sluitopdracht via driekantsleutel - DCU01_CDoorCloseConduc */
    public static final int CH3000 = 3000;

    /** Deur 1 gesloten en vergrendeld - DCU01_IDoorClosedSafe */
    public static final int CH3001 = 3001;

    /** Deur 1 HW-vrijgave ingesteld - DCU01_IDoorClRelease */
    public static final int CH3002 = 3002;

    /** Deur 1 knop sluiten bediend - DCU01_IDoorPbClose */
    public static final int CH3003 = 3003;

    /** Deur 1 knop openen binnen bediend - DCU01_IDoorPbOpenIn */
    public static final int CH3004 = 3004;

    /** Deur 1 knop openen buiten bediend - DCU01_IDoorPbOpenOut */
    public static final int CH3005 = 3005;

    /** Deur 1 vrijgegeven - DCU01_IDoorReleased */
    public static final int CH3006 = 3006;

    /** Deur 1 vrijgegeven - DCU01_IFootStepRel */
    public static final int CH3007 = 3007;

    /** Deur 1 sluitcommando - DCU01_IForcedClosDoor */
    public static final int CH3008 = 3008;

    /** Deur 1 is blijven staan - DCU01_ILeafsStopDoor */
    public static final int CH3009 = 3009;

    /** Deur 1 obstakel gedetecteerd - DCU01_IObstacleDoor */
    public static final int CH3010 = 3010;

    /** Trede 1 obstakel gedetecteerd - DCU01_IObstacleStep */
    public static final int CH3011 = 3011;

    /** Deur 1 ontgrendeling buiten opgeslagen - DCU01_IOpenAssistDoor */
    public static final int CH3012 = 3012;

    /** Deur 1 HW-stilstand ingesteld - DCU01_IStandstillBack */
    public static final int CH3013 = 3013;

    /** Deur 2 sluitopdracht via driekantsleutel - DCU02_CDoorCloseConduc */
    public static final int CH3014 = 3014;

    /** Deur 2 gesloten en vergrendeld - DCU02_IDoorClosedSafe */
    public static final int CH3015 = 3015;

    /** Deur 2 HW-vrijgave ingesteld - DCU02_IDoorClRelease */
    public static final int CH3016 = 3016;

    /** Deur 2 knop sluiten bediend - DCU02_IDoorPbClose */
    public static final int CH3017 = 3017;

    /** Deur 2 knop openen binnen bediend - DCU02_IDoorPbOpenIn */
    public static final int CH3018 = 3018;

    /** Deur 2 knop openen buiten bediend - DCU02_IDoorPbOpenOut */
    public static final int CH3019 = 3019;

    /** Deur 2 vrijgegeven - DCU02_IDoorReleased */
    public static final int CH3020 = 3020;

    /** Deur 2 vrijgegeven - DCU02_IFootStepRel */
    public static final int CH3021 = 3021;

    /** Deur 2 sluitcommando - DCU02_IForcedClosDoor */
    public static final int CH3022 = 3022;

    /** Deur 2 is blijven staan - DCU02_ILeafsStopDoor */
    public static final int CH3023 = 3023;

    /** Deur 2 obstakel gedetecteerd - DCU02_IObstacleDoor */
    public static final int CH3024 = 3024;

    /** Trede 2 obstakel gedetecteerd - DCU02_IObstacleStep */
    public static final int CH3025 = 3025;

    /** Deur 2 ontgrendeling buiten opgeslagen - DCU02_IOpenAssistDoor */
    public static final int CH3026 = 3026;

    /** Deur 2 HW-stilstand ingesteld - DCU02_IStandstillBack */
    public static final int CH3027 = 3027;

    /** Deur 3 sluitopdracht via driekantsleutel - DCU03_CDoorCloseConduc */
    public static final int CH3028 = 3028;

    /** Deur 3 gesloten en vergrendeld - DCU03_IDoorClosedSafe */
    public static final int CH3029 = 3029;

    /** Deur 3 HW-vrijgave ingesteld - DCU03_IDoorClRelease */
    public static final int CH3030 = 3030;

    /** Deur 3 knop sluiten bediend - DCU03_IDoorPbClose */
    public static final int CH3031 = 3031;

    /** Deur 3 knop openen binnen bediend - DCU03_IDoorPbOpenIn */
    public static final int CH3032 = 3032;

    /** Deur 3 knop openen buiten bediend - DCU03_IDoorPbOpenOut */
    public static final int CH3033 = 3033;

    /** Deur 3 vrijgegeven - DCU03_IDoorReleased */
    public static final int CH3034 = 3034;

    /** Deur 3 vrijgegeven - DCU03_IFootStepRel */
    public static final int CH3035 = 3035;

    /** Deur 3 sluitcommando - DCU03_IForcedClosDoor */
    public static final int CH3036 = 3036;

    /** Deur 3 is blijven staan - DCU03_ILeafsStopDoor */
    public static final int CH3037 = 3037;

    /** Deur 3 obstakel gedetecteerd - DCU03_IObstacleDoor */
    public static final int CH3038 = 3038;

    /** Trede 3 obstakel gedetecteerd - DCU03_IObstacleStep */
    public static final int CH3039 = 3039;

    /** Deur 3 ontgrendeling buiten opgeslagen - DCU03_IOpenAssistDoor */
    public static final int CH3040 = 3040;

    /** Deur 3 HW-stilstand ingesteld - DCU03_IStandstillBack */
    public static final int CH3041 = 3041;

    /** Deur 4 sluitopdracht via driekantsleutel - DCU04_CDoorCloseConduc */
    public static final int CH3042 = 3042;

    /** Deur 4 gesloten en vergrendeld - DCU04_IDoorClosedSafe */
    public static final int CH3043 = 3043;

    /** Deur 4 HW-vrijgave ingesteld - DCU04_IDoorClRelease */
    public static final int CH3044 = 3044;

    /** Deur 4 knop sluiten bediend - DCU04_IDoorPbClose */
    public static final int CH3045 = 3045;

    /** Deur 4 knop openen binnen bediend - DCU04_IDoorPbOpenIn */
    public static final int CH3046 = 3046;

    /** Deur 4 knop openen buiten bediend - DCU04_IDoorPbOpenOut */
    public static final int CH3047 = 3047;

    /** Deur 4 vrijgegeven - DCU04_IDoorReleased */
    public static final int CH3048 = 3048;

    /** Deur 4 vrijgegeven - DCU04_IFootStepRel */
    public static final int CH3049 = 3049;

    /** Deur 4 sluitcommando - DCU04_IForcedClosDoor */
    public static final int CH3050 = 3050;

    /** Deur 4 is blijven staan - DCU04_ILeafsStopDoor */
    public static final int CH3051 = 3051;

    /** Deur 4 obstakel gedetecteerd - DCU04_IObstacleDoor */
    public static final int CH3052 = 3052;

    /** Trede 4 obstakel gedetecteerd - DCU04_IObstacleStep */
    public static final int CH3053 = 3053;

    /** Deur 4 ontgrendeling buiten opgeslagen - DCU04_IOpenAssistDoor */
    public static final int CH3054 = 3054;

    /** Deur 4 HW-stilstand ingesteld - DCU04_IStandstillBack */
    public static final int CH3055 = 3055;

    /** Deur 5 sluitopdracht via driekantsleutel - DCU05_CDoorCloseConduc */
    public static final int CH3056 = 3056;

    /** Deur 5 gesloten en vergrendeld - DCU05_IDoorClosedSafe */
    public static final int CH3057 = 3057;

    /** Deur 5 HW-vrijgave ingesteld - DCU05_IDoorClRelease */
    public static final int CH3058 = 3058;

    /** Deur 5 knop sluiten bediend - DCU05_IDoorPbClose */
    public static final int CH3059 = 3059;

    /** Deur 5 knop openen binnen bediend - DCU05_IDoorPbOpenIn */
    public static final int CH3060 = 3060;

    /** Deur 5 knop openen buiten bediend - DCU05_IDoorPbOpenOut */
    public static final int CH3061 = 3061;

    /** Deur 5 vrijgegeven - DCU05_IDoorReleased */
    public static final int CH3062 = 3062;

    /** Deur 5 vrijgegeven - DCU05_IFootStepRel */
    public static final int CH3063 = 3063;

    /** Deur 5 sluitcommando - DCU05_IForcedClosDoor */
    public static final int CH3064 = 3064;

    /** Deur 5 is blijven staan - DCU05_ILeafsStopDoor */
    public static final int CH3065 = 3065;

    /** Deur 5 obstakel gedetecteerd - DCU05_IObstacleDoor */
    public static final int CH3066 = 3066;

    /** Trede 5 obstakel gedetecteerd - DCU05_IObstacleStep */
    public static final int CH3067 = 3067;

    /** Deur 5 ontgrendeling buiten opgeslagen - DCU05_IOpenAssistDoor */
    public static final int CH3068 = 3068;

    /** Deur 5 HW-stilstand ingesteld - DCU05_IStandstillBack */
    public static final int CH3069 = 3069;

    /** Deur 6 sluitopdracht via driekantsleutel - DCU06_CDoorCloseConduc */
    public static final int CH3070 = 3070;

    /** Deur 6 gesloten en vergrendeld - DCU06_IDoorClosedSafe */
    public static final int CH3071 = 3071;

    /** Deur 6 HW-vrijgave ingesteld - DCU06_IDoorClRelease */
    public static final int CH3072 = 3072;

    /** Deur 6 knop sluiten bediend - DCU06_IDoorPbClose */
    public static final int CH3073 = 3073;

    /** Deur 6 knop openen binnen bediend - DCU06_IDoorPbOpenIn */
    public static final int CH3074 = 3074;

    /** Deur 6 knop openen buiten bediend - DCU06_IDoorPbOpenOut */
    public static final int CH3075 = 3075;

    /** Deur 6 vrijgegeven - DCU06_IDoorReleased */
    public static final int CH3076 = 3076;

    /** Deur 6 vrijgegeven - DCU06_IFootStepRel */
    public static final int CH3077 = 3077;

    /** Deur 6 sluitcommando - DCU06_IForcedClosDoor */
    public static final int CH3078 = 3078;

    /** Deur 6 is blijven staan - DCU06_ILeafsStopDoor */
    public static final int CH3079 = 3079;

    /** Deur 6 obstakel gedetecteerd - DCU06_IObstacleDoor */
    public static final int CH3080 = 3080;

    /** Trede 6 obstakel gedetecteerd - DCU06_IObstacleStep */
    public static final int CH3081 = 3081;

    /** Deur 6 ontgrendeling buiten opgeslagen - DCU06_IOpenAssistDoor */
    public static final int CH3082 = 3082;

    /** Deur 6 HW-stilstand ingesteld - DCU06_IStandstillBack */
    public static final int CH3083 = 3083;

    /** Deur 7 sluitopdracht via driekantsleutel - DCU07_CDoorCloseConduc */
    public static final int CH3084 = 3084;

    /** Deur 7 gesloten en vergrendeld - DCU07_IDoorClosedSafe */
    public static final int CH3085 = 3085;

    /** Deur 7 HW-vrijgave ingesteld - DCU07_IDoorClRelease */
    public static final int CH3086 = 3086;

    /** Deur 7 knop sluiten bediend - DCU07_IDoorPbClose */
    public static final int CH3087 = 3087;

    /** Deur 7 knop openen binnen bediend - DCU07_IDoorPbOpenIn */
    public static final int CH3088 = 3088;

    /** Deur 7 knop openen buiten bediend - DCU07_IDoorPbOpenOut */
    public static final int CH3089 = 3089;

    /** Deur 7 vrijgegeven - DCU07_IDoorReleased */
    public static final int CH3090 = 3090;

    /** Deur 7 vrijgegeven - DCU07_IFootStepRel */
    public static final int CH3091 = 3091;

    /** Deur 7 sluitcommando - DCU07_IForcedClosDoor */
    public static final int CH3092 = 3092;

    /** Deur 7 is blijven staan - DCU07_ILeafsStopDoor */
    public static final int CH3093 = 3093;

    /** Deur 7 obstakel gedetecteerd - DCU07_IObstacleDoor */
    public static final int CH3094 = 3094;

    /** Trede 7 obstakel gedetecteerd - DCU07_IObstacleStep */
    public static final int CH3095 = 3095;

    /** Deur 7 ontgrendeling buiten opgeslagen - DCU07_IOpenAssistDoor */
    public static final int CH3096 = 3096;

    /** Deur 7 HW-stilstand ingesteld - DCU07_IStandstillBack */
    public static final int CH3097 = 3097;

    /** Deur 8 sluitopdracht via driekantsleutel - DCU08_CDoorCloseConduc */
    public static final int CH3098 = 3098;

    /** Deur 8 gesloten en vergrendeld - DCU08_IDoorClosedSafe */
    public static final int CH3099 = 3099;

    /** Deur 8 HW-vrijgave ingesteld - DCU08_IDoorClRelease */
    public static final int CH3100 = 3100;

    /** Deur 8 knop sluiten bediend - DCU08_IDoorPbClose */
    public static final int CH3101 = 3101;

    /** Deur 8 knop openen binnen bediend - DCU08_IDoorPbOpenIn */
    public static final int CH3102 = 3102;

    /** Deur 8 knop openen buiten bediend - DCU08_IDoorPbOpenOut */
    public static final int CH3103 = 3103;

    /** Deur 8 vrijgegeven - DCU08_IDoorReleased */
    public static final int CH3104 = 3104;

    /** Deur 8 vrijgegeven - DCU08_IFootStepRel */
    public static final int CH3105 = 3105;

    /** Deur 8 sluitcommando - DCU08_IForcedClosDoor */
    public static final int CH3106 = 3106;

    /** Deur 8 is blijven staan - DCU08_ILeafsStopDoor */
    public static final int CH3107 = 3107;

    /** Deur 8 obstakel gedetecteerd - DCU08_IObstacleDoor */
    public static final int CH3108 = 3108;

    /** Trede 8 obstakel gedetecteerd - DCU08_IObstacleStep */
    public static final int CH3109 = 3109;

    /** Deur 8 ontgrendeling buiten opgeslagen - DCU08_IOpenAssistDoor */
    public static final int CH3110 = 3110;

    /** Deur 8 HW-stilstand ingesteld - DCU08_IStandstillBack */
    public static final int CH3111 = 3111;

    /** Deur 9 sluitopdracht via driekantsleutel - DCU09_CDoorCloseConduc */
    public static final int CH3112 = 3112;

    /** Deur 9 gesloten en vergrendeld - DCU09_IDoorClosedSafe */
    public static final int CH3113 = 3113;

    /** Deur 9 HW-vrijgave ingesteld - DCU09_IDoorClRelease */
    public static final int CH3114 = 3114;

    /** Deur 9 knop sluiten bediend - DCU09_IDoorPbClose */
    public static final int CH3115 = 3115;

    /** Deur 9 knop openen binnen bediend - DCU09_IDoorPbOpenIn */
    public static final int CH3116 = 3116;

    /** Deur 9 knop openen buiten bediend - DCU09_IDoorPbOpenOut */
    public static final int CH3117 = 3117;

    /** Deur 9 vrijgegeven - DCU09_IDoorReleased */
    public static final int CH3118 = 3118;

    /** Deur 9 vrijgegeven - DCU09_IFootStepRel */
    public static final int CH3119 = 3119;

    /** Deur 9 sluitcommando - DCU09_IForcedClosDoor */
    public static final int CH3120 = 3120;

    /** Deur 9 is blijven staan - DCU09_ILeafsStopDoor */
    public static final int CH3121 = 3121;

    /** Deur 9 obstakel gedetecteerd - DCU09_IObstacleDoor */
    public static final int CH3122 = 3122;

    /** Trede 9 obstakel gedetecteerd - DCU09_IObstacleStep */
    public static final int CH3123 = 3123;

    /** Deur 9 ontgrendeling buiten opgeslagen - DCU09_IOpenAssistDoor */
    public static final int CH3124 = 3124;

    /** Deur 9 HW-stilstand ingesteld - DCU09_IStandstillBack */
    public static final int CH3125 = 3125;

    /** Deur 10 sluitopdracht via driekantsleutel - DCU10_CDoorCloseConduc */
    public static final int CH3126 = 3126;

    /** Deur 10 gesloten en vergrendeld - DCU10_IDoorClosedSafe */
    public static final int CH3127 = 3127;

    /** Deur 10 HW-vrijgave ingesteld - DCU10_IDoorClRelease */
    public static final int CH3128 = 3128;

    /** Deur 10 knop sluiten bediend - DCU10_IDoorPbClose */
    public static final int CH3129 = 3129;

    /** Deur 10 knop openen binnen bediend - DCU10_IDoorPbOpenIn */
    public static final int CH3130 = 3130;

    /** Deur 10 knop openen buiten bediend - DCU10_IDoorPbOpenOut */
    public static final int CH3131 = 3131;

    /** Deur 10 vrijgegeven - DCU10_IDoorReleased */
    public static final int CH3132 = 3132;

    /** Deur 10 vrijgegeven - DCU10_IFootStepRel */
    public static final int CH3133 = 3133;

    /** Deur 10 sluitcommando - DCU10_IForcedClosDoor */
    public static final int CH3134 = 3134;

    /** Deur 10 is blijven staan - DCU10_ILeafsStopDoor */
    public static final int CH3135 = 3135;

    /** Deur 10 obstakel gedetecteerd - DCU10_IObstacleDoor */
    public static final int CH3136 = 3136;

    /** Trede 10 obstakel gedetecteerd - DCU10_IObstacleStep */
    public static final int CH3137 = 3137;

    /** Deur 10 ontgrendeling buiten opgeslagen - DCU10_IOpenAssistDoor */
    public static final int CH3138 = 3138;

    /** Deur 10 HW-stilstand ingesteld - DCU10_IStandstillBack */
    public static final int CH3139 = 3139;

    /** Deur 11 sluitopdracht via driekantsleutel - DCU11_CDoorCloseConduc */
    public static final int CH3140 = 3140;

    /** Deur 11 gesloten en vergrendeld - DCU11_IDoorClosedSafe */
    public static final int CH3141 = 3141;

    /** Deur 11 HW-vrijgave ingesteld - DCU11_IDoorClRelease */
    public static final int CH3142 = 3142;

    /** Deur 11 knop sluiten bediend - DCU11_IDoorPbClose */
    public static final int CH3143 = 3143;

    /** Deur 11 knop openen binnen bediend - DCU11_IDoorPbOpenIn */
    public static final int CH3144 = 3144;

    /** Deur 11 knop openen buiten bediend - DCU11_IDoorPbOpenOut */
    public static final int CH3145 = 3145;

    /** Deur 11 vrijgegeven - DCU11_IDoorReleased */
    public static final int CH3146 = 3146;

    /** Deur 11 vrijgegeven - DCU11_IFootStepRel */
    public static final int CH3147 = 3147;

    /** Deur 11 sluitcommando - DCU11_IForcedClosDoor */
    public static final int CH3148 = 3148;

    /** Deur 11 is blijven staan - DCU11_ILeafsStopDoor */
    public static final int CH3149 = 3149;

    /** Deur 11 obstakel gedetecteerd - DCU11_IObstacleDoor */
    public static final int CH3150 = 3150;

    /** Trede 11 obstakel gedetecteerd - DCU11_IObstacleStep */
    public static final int CH3151 = 3151;

    /** Deur 11 ontgrendeling buiten opgeslagen - DCU11_IOpenAssistDoor */
    public static final int CH3152 = 3152;

    /** Deur 11 HW-stilstand ingesteld - DCU11_IStandstillBack */
    public static final int CH3153 = 3153;

    /** Deur 12 sluitopdracht via driekantsleutel - DCU12_CDoorCloseConduc */
    public static final int CH3154 = 3154;

    /** Deur 12 gesloten en vergrendeld - DCU12_IDoorClosedSafe */
    public static final int CH3155 = 3155;

    /** Deur 12 HW-vrijgave ingesteld - DCU12_IDoorClRelease */
    public static final int CH3156 = 3156;

    /** Deur 12 knop sluiten bediend - DCU12_IDoorPbClose */
    public static final int CH3157 = 3157;

    /** Deur 12 knop openen binnen bediend - DCU12_IDoorPbOpenIn */
    public static final int CH3158 = 3158;

    /** Deur 12 knop openen buiten bediend - DCU12_IDoorPbOpenOut */
    public static final int CH3159 = 3159;

    /** Deur 12 vrijgegeven - DCU12_IDoorReleased */
    public static final int CH3160 = 3160;

    /** Deur 12 vrijgegeven - DCU12_IFootStepRel */
    public static final int CH3161 = 3161;

    /** Deur 12 sluitcommando - DCU12_IForcedClosDoor */
    public static final int CH3162 = 3162;

    /** Deur 12 is blijven staan - DCU12_ILeafsStopDoor */
    public static final int CH3163 = 3163;

    /** Deur 12 obstakel gedetecteerd - DCU12_IObstacleDoor */
    public static final int CH3164 = 3164;

    /** Trede 12 obstakel gedetecteerd - DCU12_IObstacleStep */
    public static final int CH3165 = 3165;

    /** Deur 12 ontgrendeling buiten opgeslagen - DCU12_IOpenAssistDoor */
    public static final int CH3166 = 3166;

    /** Deur 12 HW-stilstand ingesteld - DCU12_IStandstillBack */
    public static final int CH3167 = 3167;

    /** Deur 13 sluitopdracht via driekantsleutel - DCU13_CDoorCloseConduc */
    public static final int CH3168 = 3168;

    /** Deur 13 gesloten en vergrendeld - DCU13_IDoorClosedSafe */
    public static final int CH3169 = 3169;

    /** Deur 13 HW-vrijgave ingesteld - DCU13_IDoorClRelease */
    public static final int CH3170 = 3170;

    /** Deur 13 knop sluiten bediend - DCU13_IDoorPbClose */
    public static final int CH3171 = 3171;

    /** Deur 13 knop openen binnen bediend - DCU13_IDoorPbOpenIn */
    public static final int CH3172 = 3172;

    /** Deur 13 knop openen buiten bediend - DCU13_IDoorPbOpenOut */
    public static final int CH3173 = 3173;

    /** Deur 13 vrijgegeven - DCU13_IDoorReleased */
    public static final int CH3174 = 3174;

    /** Deur 13 vrijgegeven - DCU13_IFootStepRel */
    public static final int CH3175 = 3175;

    /** Deur 13 sluitcommando - DCU13_IForcedClosDoor */
    public static final int CH3176 = 3176;

    /** Deur 13 is blijven staan - DCU13_ILeafsStopDoor */
    public static final int CH3177 = 3177;

    /** Deur 13 obstakel gedetecteerd - DCU13_IObstacleDoor */
    public static final int CH3178 = 3178;

    /** Trede 13 obstakel gedetecteerd - DCU13_IObstacleStep */
    public static final int CH3179 = 3179;

    /** Deur 13 ontgrendeling buiten opgeslagen - DCU13_IOpenAssistDoor */
    public static final int CH3180 = 3180;

    /** Deur 13 HW-stilstand ingesteld - DCU13_IStandstillBack */
    public static final int CH3181 = 3181;

    /** Deur 14 sluitopdracht via driekantsleutel - DCU14_CDoorCloseConduc */
    public static final int CH3182 = 3182;

    /** Deur 14 gesloten en vergrendeld - DCU14_IDoorClosedSafe */
    public static final int CH3183 = 3183;

    /** Deur 14 HW-vrijgave ingesteld - DCU14_IDoorClRelease */
    public static final int CH3184 = 3184;

    /** Deur 14 knop sluiten bediend - DCU14_IDoorPbClose */
    public static final int CH3185 = 3185;

    /** Deur 14 knop openen binnen bediend - DCU14_IDoorPbOpenIn */
    public static final int CH3186 = 3186;

    /** Deur 14 knop openen buiten bediend - DCU14_IDoorPbOpenOut */
    public static final int CH3187 = 3187;

    /** Deur 14 vrijgegeven - DCU14_IDoorReleased */
    public static final int CH3188 = 3188;

    /** Deur 14 vrijgegeven - DCU14_IFootStepRel */
    public static final int CH3189 = 3189;

    /** Deur 14 sluitcommando - DCU14_IForcedClosDoor */
    public static final int CH3190 = 3190;

    /** Deur 14 is blijven staan - DCU14_ILeafsStopDoor */
    public static final int CH3191 = 3191;

    /** Deur 14 obstakel gedetecteerd - DCU14_IObstacleDoor */
    public static final int CH3192 = 3192;

    /** Trede 14 obstakel gedetecteerd - DCU14_IObstacleStep */
    public static final int CH3193 = 3193;

    /** Deur 14 ontgrendeling buiten opgeslagen - DCU14_IOpenAssistDoor */
    public static final int CH3194 = 3194;

    /** Deur 14 HW-stilstand ingesteld - DCU14_IStandstillBack */
    public static final int CH3195 = 3195;

    /** Deur 15 sluitopdracht via driekantsleutel - DCU15_CDoorCloseConduc */
    public static final int CH3196 = 3196;

    /** Deur 15 gesloten en vergrendeld - DCU15_IDoorClosedSafe */
    public static final int CH3197 = 3197;

    /** Deur 15 HW-vrijgave ingesteld - DCU15_IDoorClRelease */
    public static final int CH3198 = 3198;

    /** Deur 15 knop sluiten bediend - DCU15_IDoorPbClose */
    public static final int CH3199 = 3199;

    /** Deur 15 knop openen binnen bediend - DCU15_IDoorPbOpenIn */
    public static final int CH3200 = 3200;

    /** Deur 15 knop openen buiten bediend - DCU15_IDoorPbOpenOut */
    public static final int CH3201 = 3201;

    /** Deur 15 vrijgegeven - DCU15_IDoorReleased */
    public static final int CH3202 = 3202;

    /** Deur 15 vrijgegeven - DCU15_IFootStepRel */
    public static final int CH3203 = 3203;

    /** Deur 15 sluitcommando - DCU15_IForcedClosDoor */
    public static final int CH3204 = 3204;

    /** Deur 15 is blijven staan - DCU15_ILeafsStopDoor */
    public static final int CH3205 = 3205;

    /** Deur 15 obstakel gedetecteerd - DCU15_IObstacleDoor */
    public static final int CH3206 = 3206;

    /** Trede 15 obstakel gedetecteerd - DCU15_IObstacleStep */
    public static final int CH3207 = 3207;

    /** Deur 15 ontgrendeling buiten opgeslagen - DCU15_IOpenAssistDoor */
    public static final int CH3208 = 3208;

    /** Deur 15 HW-stilstand ingesteld - DCU15_IStandstillBack */
    public static final int CH3209 = 3209;

    /** Deur 16 sluitopdracht via driekantsleutel - DCU16_CDoorCloseConduc */
    public static final int CH3210 = 3210;

    /** Deur 16 gesloten en vergrendeld - DCU16_IDoorClosedSafe */
    public static final int CH3211 = 3211;

    /** Deur 16 HW-vrijgave ingesteld - DCU16_IDoorClRelease */
    public static final int CH3212 = 3212;

    /** Deur 16 knop sluiten bediend - DCU16_IDoorPbClose */
    public static final int CH3213 = 3213;

    /** Deur 16 knop openen binnen bediend - DCU16_IDoorPbOpenIn */
    public static final int CH3214 = 3214;

    /** Deur 16 knop openen buiten bediend - DCU16_IDoorPbOpenOut */
    public static final int CH3215 = 3215;

    /** Deur 16 vrijgegeven - DCU16_IDoorReleased */
    public static final int CH3216 = 3216;

    /** Deur 16 vrijgegeven - DCU16_IFootStepRel */
    public static final int CH3217 = 3217;

    /** Deur 16 sluitcommando - DCU16_IForcedClosDoor */
    public static final int CH3218 = 3218;

    /** Deur 16 is blijven staan - DCU16_ILeafsStopDoor */
    public static final int CH3219 = 3219;

    /** Deur 16 obstakel gedetecteerd - DCU16_IObstacleDoor */
    public static final int CH3220 = 3220;

    /** Trede 16 obstakel gedetecteerd - DCU16_IObstacleStep */
    public static final int CH3221 = 3221;

    /** Deur 16 ontgrendeling buiten opgeslagen - DCU16_IOpenAssistDoor */
    public static final int CH3222 = 3222;

    /** Deur 16 HW-stilstand ingesteld - DCU16_IStandstillBack */
    public static final int CH3223 = 3223;

    /** Deur 17 sluitopdracht via driekantsleutel - DCU17_CDoorCloseConduc */
    public static final int CH3224 = 3224;

    /** Deur 17 gesloten en vergrendeld - DCU17_IDoorClosedSafe */
    public static final int CH3225 = 3225;

    /** Deur 17 HW-vrijgave ingesteld - DCU17_IDoorClRelease */
    public static final int CH3226 = 3226;

    /** Deur 17 knop sluiten bediend - DCU17_IDoorPbClose */
    public static final int CH3227 = 3227;

    /** Deur 17 knop openen binnen bediend - DCU17_IDoorPbOpenIn */
    public static final int CH3228 = 3228;

    /** Deur 17 knop openen buiten bediend - DCU17_IDoorPbOpenOut */
    public static final int CH3229 = 3229;

    /** Deur 17 vrijgegeven - DCU17_IDoorReleased */
    public static final int CH3230 = 3230;

    /** Deur 17 vrijgegeven - DCU17_IFootStepRel */
    public static final int CH3231 = 3231;

    /** Deur 17 sluitcommando - DCU17_IForcedClosDoor */
    public static final int CH3232 = 3232;

    /** Deur 17 is blijven staan - DCU17_ILeafsStopDoor */
    public static final int CH3233 = 3233;

    /** Deur 17 obstakel gedetecteerd - DCU17_IObstacleDoor */
    public static final int CH3234 = 3234;

    /** Trede 17 obstakel gedetecteerd - DCU17_IObstacleStep */
    public static final int CH3235 = 3235;

    /** Deur 17 ontgrendeling buiten opgeslagen - DCU17_IOpenAssistDoor */
    public static final int CH3236 = 3236;

    /** Deur 17 HW-stilstand ingesteld - DCU17_IStandstillBack */
    public static final int CH3237 = 3237;

    /** Deur 18 sluitopdracht via driekantsleutel - DCU18_CDoorCloseConduc */
    public static final int CH3238 = 3238;

    /** Deur 18 gesloten en vergrendeld - DCU18_IDoorClosedSafe */
    public static final int CH3239 = 3239;

    /** Deur 18 HW-vrijgave ingesteld - DCU18_IDoorClRelease */
    public static final int CH3240 = 3240;

    /** Deur 18 knop sluiten bediend - DCU18_IDoorPbClose */
    public static final int CH3241 = 3241;

    /** Deur 18 knop openen binnen bediend - DCU18_IDoorPbOpenIn */
    public static final int CH3242 = 3242;

    /** Deur 18 knop openen buiten bediend - DCU18_IDoorPbOpenOut */
    public static final int CH3243 = 3243;

    /** Deur 18 vrijgegeven - DCU18_IDoorReleased */
    public static final int CH3244 = 3244;

    /** Deur 18 vrijgegeven - DCU18_IFootStepRel */
    public static final int CH3245 = 3245;

    /** Deur 18 sluitcommando - DCU18_IForcedClosDoor */
    public static final int CH3246 = 3246;

    /** Deur 18 is blijven staan - DCU18_ILeafsStopDoor */
    public static final int CH3247 = 3247;

    /** Deur 18 obstakel gedetecteerd - DCU18_IObstacleDoor */
    public static final int CH3248 = 3248;

    /** Trede 18 obstakel gedetecteerd - DCU18_IObstacleStep */
    public static final int CH3249 = 3249;

    /** Deur 18 ontgrendeling buiten opgeslagen - DCU18_IOpenAssistDoor */
    public static final int CH3250 = 3250;

    /** Deur 18 HW-stilstand ingesteld - DCU18_IStandstillBack */
    public static final int CH3251 = 3251;

    /** Deur 19 sluitopdracht via driekantsleutel - DCU19_CDoorCloseConduc */
    public static final int CH3252 = 3252;

    /** Deur 19 gesloten en vergrendeld - DCU19_IDoorClosedSafe */
    public static final int CH3253 = 3253;

    /** Deur 19 HW-vrijgave ingesteld - DCU19_IDoorClRelease */
    public static final int CH3254 = 3254;

    /** Deur 19 knop sluiten bediend - DCU19_IDoorPbClose */
    public static final int CH3255 = 3255;

    /** Deur 19 knop openen binnen bediend - DCU19_IDoorPbOpenIn */
    public static final int CH3256 = 3256;

    /** Deur 19 knop openen buiten bediend - DCU19_IDoorPbOpenOut */
    public static final int CH3257 = 3257;

    /** Deur 19 vrijgegeven - DCU19_IDoorReleased */
    public static final int CH3258 = 3258;

    /** Deur 19 vrijgegeven - DCU19_IFootStepRel */
    public static final int CH3259 = 3259;

    /** Deur 19 sluitcommando - DCU19_IForcedClosDoor */
    public static final int CH3260 = 3260;

    /** Deur 19 is blijven staan - DCU19_ILeafsStopDoor */
    public static final int CH3261 = 3261;

    /** Deur 19 obstakel gedetecteerd - DCU19_IObstacleDoor */
    public static final int CH3262 = 3262;

    /** Trede 19 obstakel gedetecteerd - DCU19_IObstacleStep */
    public static final int CH3263 = 3263;

    /** Deur 19 ontgrendeling buiten opgeslagen - DCU19_IOpenAssistDoor */
    public static final int CH3264 = 3264;

    /** Deur 19 HW-stilstand ingesteld - DCU19_IStandstillBack */
    public static final int CH3265 = 3265;

    /** Deur 20 sluitopdracht via driekantsleutel - DCU20_CDoorCloseConduc */
    public static final int CH3266 = 3266;

    /** Deur 20 gesloten en vergrendeld - DCU20_IDoorClosedSafe */
    public static final int CH3267 = 3267;

    /** Deur 20 HW-vrijgave ingesteld - DCU20_IDoorClRelease */
    public static final int CH3268 = 3268;

    /** Deur 20 knop sluiten bediend - DCU20_IDoorPbClose */
    public static final int CH3269 = 3269;

    /** Deur 20 knop openen binnen bediend - DCU20_IDoorPbOpenIn */
    public static final int CH3270 = 3270;

    /** Deur 20 knop openen buiten bediend - DCU20_IDoorPbOpenOut */
    public static final int CH3271 = 3271;

    /** Deur 20 vrijgegeven - DCU20_IDoorReleased */
    public static final int CH3272 = 3272;

    /** Deur 20 vrijgegeven - DCU20_IFootStepRel */
    public static final int CH3273 = 3273;

    /** Deur 20 sluitcommando - DCU20_IForcedClosDoor */
    public static final int CH3274 = 3274;

    /** Deur 20 is blijven staan - DCU20_ILeafsStopDoor */
    public static final int CH3275 = 3275;

    /** Deur 20 obstakel gedetecteerd - DCU20_IObstacleDoor */
    public static final int CH3276 = 3276;

    /** Trede 20 obstakel gedetecteerd - DCU20_IObstacleStep */
    public static final int CH3277 = 3277;

    /** Deur 20 ontgrendeling buiten opgeslagen - DCU20_IOpenAssistDoor */
    public static final int CH3278 = 3278;

    /** Deur 20 HW-stilstand ingesteld - DCU20_IStandstillBack */
    public static final int CH3279 = 3279;

    /** deurvrijgave - DRS_CDisDoorRelease_4_1 */
    public static final int CH3280 = 3280;

    /** deurvrijgave - DRS_CDisDoorRelease_4_10 */
    public static final int CH3281 = 3281;

    /** deurvrijgave - DRS_CDisDoorRelease_4_11 */
    public static final int CH3282 = 3282;

    /** deurvrijgave - DRS_CDisDoorRelease_4_12 */
    public static final int CH3283 = 3283;

    /** deurvrijgave - DRS_CDisDoorRelease_4_13 */
    public static final int CH3284 = 3284;

    /** deurvrijgave - DRS_CDisDoorRelease_4_14 */
    public static final int CH3285 = 3285;

    /** deurvrijgave - DRS_CDisDoorRelease_4_15 */
    public static final int CH3286 = 3286;

    /** deurvrijgave - DRS_CDisDoorRelease_4_16 */
    public static final int CH3287 = 3287;

    /** deurvrijgave - DRS_CDisDoorRelease_4_17 */
    public static final int CH3288 = 3288;

    /** deurvrijgave - DRS_CDisDoorRelease_4_18 */
    public static final int CH3289 = 3289;

    /** deurvrijgave - DRS_CDisDoorRelease_4_19 */
    public static final int CH3290 = 3290;

    /** deurvrijgave - DRS_CDisDoorRelease_4_2 */
    public static final int CH3291 = 3291;

    /** deurvrijgave - DRS_CDisDoorRelease_4_20 */
    public static final int CH3292 = 3292;

    /** deurvrijgave - DRS_CDisDoorRelease_4_3 */
    public static final int CH3293 = 3293;

    /** deurvrijgave - DRS_CDisDoorRelease_4_4 */
    public static final int CH3294 = 3294;

    /** deurvrijgave - DRS_CDisDoorRelease_4_5 */
    public static final int CH3295 = 3295;

    /** deurvrijgave - DRS_CDisDoorRelease_4_6 */
    public static final int CH3296 = 3296;

    /** deurvrijgave - DRS_CDisDoorRelease_4_7 */
    public static final int CH3297 = 3297;

    /** deurvrijgave - DRS_CDisDoorRelease_4_8 */
    public static final int CH3298 = 3298;

    /** deurvrijgave - DRS_CDisDoorRelease_4_9 */
    public static final int CH3299 = 3299;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_1 */
    public static final int CH3300 = 3300;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_10 */
    public static final int CH3301 = 3301;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_11 */
    public static final int CH3302 = 3302;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_12 */
    public static final int CH3303 = 3303;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_13 */
    public static final int CH3304 = 3304;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_14 */
    public static final int CH3305 = 3305;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_15 */
    public static final int CH3306 = 3306;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_16 */
    public static final int CH3307 = 3307;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_17 */
    public static final int CH3308 = 3308;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_18 */
    public static final int CH3309 = 3309;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_19 */
    public static final int CH3310 = 3310;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_2 */
    public static final int CH3311 = 3311;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_20 */
    public static final int CH3312 = 3312;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_3 */
    public static final int CH3313 = 3313;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_4 */
    public static final int CH3314 = 3314;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_5 */
    public static final int CH3315 = 3315;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_6 */
    public static final int CH3316 = 3316;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_7 */
    public static final int CH3317 = 3317;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_8 */
    public static final int CH3318 = 3318;

    /** Automatisch sluiten geblokkeerd - DRS_CDoorClosAutDis_4_9 */
    public static final int CH3319 = 3319;

    /** Sluitcommando - DRS_CForcedClosDoor_4_1 */
    public static final int CH3320 = 3320;

    /** Sluitcommando - DRS_CForcedClosDoor_4_10 */
    public static final int CH3321 = 3321;

    /** Sluitcommando - DRS_CForcedClosDoor_4_11 */
    public static final int CH3322 = 3322;

    /** Sluitcommando - DRS_CForcedClosDoor_4_12 */
    public static final int CH3323 = 3323;

    /** Sluitcommando - DRS_CForcedClosDoor_4_13 */
    public static final int CH3324 = 3324;

    /** Sluitcommando - DRS_CForcedClosDoor_4_14 */
    public static final int CH3325 = 3325;

    /** Sluitcommando - DRS_CForcedClosDoor_4_15 */
    public static final int CH3326 = 3326;

    /** Sluitcommando - DRS_CForcedClosDoor_4_16 */
    public static final int CH3327 = 3327;

    /** Sluitcommando - DRS_CForcedClosDoor_4_17 */
    public static final int CH3328 = 3328;

    /** Sluitcommando - DRS_CForcedClosDoor_4_18 */
    public static final int CH3329 = 3329;

    /** Sluitcommando - DRS_CForcedClosDoor_4_19 */
    public static final int CH3330 = 3330;

    /** Sluitcommando - DRS_CForcedClosDoor_4_2 */
    public static final int CH3331 = 3331;

    /** Sluitcommando - DRS_CForcedClosDoor_4_20 */
    public static final int CH3332 = 3332;

    /** Sluitcommando - DRS_CForcedClosDoor_4_3 */
    public static final int CH3333 = 3333;

    /** Sluitcommando - DRS_CForcedClosDoor_4_4 */
    public static final int CH3334 = 3334;

    /** Sluitcommando - DRS_CForcedClosDoor_4_5 */
    public static final int CH3335 = 3335;

    /** Sluitcommando - DRS_CForcedClosDoor_4_6 */
    public static final int CH3336 = 3336;

    /** Sluitcommando - DRS_CForcedClosDoor_4_7 */
    public static final int CH3337 = 3337;

    /** Sluitcommando - DRS_CForcedClosDoor_4_8 */
    public static final int CH3338 = 3338;

    /** Sluitcommando - DRS_CForcedClosDoor_4_9 */
    public static final int CH3339 = 3339;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_1 */
    public static final int CH3340 = 3340;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_10 */
    public static final int CH3341 = 3341;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_11 */
    public static final int CH3342 = 3342;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_12 */
    public static final int CH3343 = 3343;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_13 */
    public static final int CH3344 = 3344;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_14 */
    public static final int CH3345 = 3345;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_15 */
    public static final int CH3346 = 3346;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_16 */
    public static final int CH3347 = 3347;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_17 */
    public static final int CH3348 = 3348;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_18 */
    public static final int CH3349 = 3349;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_19 */
    public static final int CH3350 = 3350;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_2 */
    public static final int CH3351 = 3351;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_20 */
    public static final int CH3352 = 3352;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_3 */
    public static final int CH3353 = 3353;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_4 */
    public static final int CH3354 = 3354;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_5 */
    public static final int CH3355 = 3355;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_6 */
    public static final int CH3356 = 3356;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_7 */
    public static final int CH3357 = 3357;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_8 */
    public static final int CH3358 = 3358;

    /** Werkingscontrole deuren - DRS_CTestModeReq_M_4_9 */
    public static final int CH3359 = 3359;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_1 */
    public static final int CH3360 = 3360;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_10 */
    public static final int CH3361 = 3361;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_11 */
    public static final int CH3362 = 3362;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_12 */
    public static final int CH3363 = 3363;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_13 */
    public static final int CH3364 = 3364;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_14 */
    public static final int CH3365 = 3365;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_15 */
    public static final int CH3366 = 3366;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_16 */
    public static final int CH3367 = 3367;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_17 */
    public static final int CH3368 = 3368;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_18 */
    public static final int CH3369 = 3369;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_19 */
    public static final int CH3370 = 3370;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_2 */
    public static final int CH3371 = 3371;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_20 */
    public static final int CH3372 = 3372;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_3 */
    public static final int CH3373 = 3373;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_4 */
    public static final int CH3374 = 3374;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_5 */
    public static final int CH3375 = 3375;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_6 */
    public static final int CH3376 = 3376;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_7 */
    public static final int CH3377 = 3377;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_8 */
    public static final int CH3378 = 3378;

    /** Deurvrijgave links in A-rijtuig - MIO_I1DoorRelLe_4_9 */
    public static final int CH3379 = 3379;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_1 */
    public static final int CH3380 = 3380;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_10 */
    public static final int CH3381 = 3381;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_11 */
    public static final int CH3382 = 3382;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_12 */
    public static final int CH3383 = 3383;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_13 */
    public static final int CH3384 = 3384;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_14 */
    public static final int CH3385 = 3385;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_15 */
    public static final int CH3386 = 3386;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_16 */
    public static final int CH3387 = 3387;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_17 */
    public static final int CH3388 = 3388;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_18 */
    public static final int CH3389 = 3389;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_19 */
    public static final int CH3390 = 3390;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_2 */
    public static final int CH3391 = 3391;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_20 */
    public static final int CH3392 = 3392;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_3 */
    public static final int CH3393 = 3393;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_4 */
    public static final int CH3394 = 3394;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_5 */
    public static final int CH3395 = 3395;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_6 */
    public static final int CH3396 = 3396;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_7 */
    public static final int CH3397 = 3397;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_8 */
    public static final int CH3398 = 3398;

    /** Deurvrijgave rechts in A-rijtuig - MIO_I1DoorRelRi_4_9 */
    public static final int CH3399 = 3399;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_1 */
    public static final int CH3400 = 3400;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_10 */
    public static final int CH3401 = 3401;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_11 */
    public static final int CH3402 = 3402;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_12 */
    public static final int CH3403 = 3403;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_13 */
    public static final int CH3404 = 3404;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_14 */
    public static final int CH3405 = 3405;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_15 */
    public static final int CH3406 = 3406;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_16 */
    public static final int CH3407 = 3407;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_17 */
    public static final int CH3408 = 3408;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_18 */
    public static final int CH3409 = 3409;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_19 */
    public static final int CH3410 = 3410;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_2 */
    public static final int CH3411 = 3411;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_20 */
    public static final int CH3412 = 3412;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_3 */
    public static final int CH3413 = 3413;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_4 */
    public static final int CH3414 = 3414;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_5 */
    public static final int CH3415 = 3415;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_6 */
    public static final int CH3416 = 3416;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_7 */
    public static final int CH3417 = 3417;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_8 */
    public static final int CH3418 = 3418;

    /** Deurvrijgave links in B-rijtuig - MIO_I2DoorRelLe_4_9 */
    public static final int CH3419 = 3419;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_1 */
    public static final int CH3420 = 3420;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_10 */
    public static final int CH3421 = 3421;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_11 */
    public static final int CH3422 = 3422;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_12 */
    public static final int CH3423 = 3423;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_13 */
    public static final int CH3424 = 3424;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_14 */
    public static final int CH3425 = 3425;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_15 */
    public static final int CH3426 = 3426;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_16 */
    public static final int CH3427 = 3427;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_17 */
    public static final int CH3428 = 3428;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_18 */
    public static final int CH3429 = 3429;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_19 */
    public static final int CH3430 = 3430;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_2 */
    public static final int CH3431 = 3431;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_20 */
    public static final int CH3432 = 3432;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_3 */
    public static final int CH3433 = 3433;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_4 */
    public static final int CH3434 = 3434;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_5 */
    public static final int CH3435 = 3435;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_6 */
    public static final int CH3436 = 3436;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_7 */
    public static final int CH3437 = 3437;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_8 */
    public static final int CH3438 = 3438;

    /** Deurvrijgave rechts in B-rijtuig - MIO_I2DoorRelRi_4_9 */
    public static final int CH3439 = 3439;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_1 */
    public static final int CH3440 = 3440;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_10 */
    public static final int CH3441 = 3441;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_11 */
    public static final int CH3442 = 3442;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_12 */
    public static final int CH3443 = 3443;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_13 */
    public static final int CH3444 = 3444;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_14 */
    public static final int CH3445 = 3445;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_15 */
    public static final int CH3446 = 3446;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_16 */
    public static final int CH3447 = 3447;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_17 */
    public static final int CH3448 = 3448;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_18 */
    public static final int CH3449 = 3449;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_19 */
    public static final int CH3450 = 3450;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_2 */
    public static final int CH3451 = 3451;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_20 */
    public static final int CH3452 = 3452;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_3 */
    public static final int CH3453 = 3453;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_4 */
    public static final int CH3454 = 3454;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_5 */
    public static final int CH3455 = 3455;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_6 */
    public static final int CH3456 = 3456;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_7 */
    public static final int CH3457 = 3457;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_8 */
    public static final int CH3458 = 3458;

    /** Overbrugging rijblokkering rijtuig A - MIO_IDoorLoopBypa1_4_9 */
    public static final int CH3459 = 3459;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_1 */
    public static final int CH3460 = 3460;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_10 */
    public static final int CH3461 = 3461;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_11 */
    public static final int CH3462 = 3462;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_12 */
    public static final int CH3463 = 3463;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_13 */
    public static final int CH3464 = 3464;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_14 */
    public static final int CH3465 = 3465;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_15 */
    public static final int CH3466 = 3466;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_16 */
    public static final int CH3467 = 3467;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_17 */
    public static final int CH3468 = 3468;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_18 */
    public static final int CH3469 = 3469;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_19 */
    public static final int CH3470 = 3470;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_2 */
    public static final int CH3471 = 3471;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_20 */
    public static final int CH3472 = 3472;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_3 */
    public static final int CH3473 = 3473;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_4 */
    public static final int CH3474 = 3474;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_5 */
    public static final int CH3475 = 3475;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_6 */
    public static final int CH3476 = 3476;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_7 */
    public static final int CH3477 = 3477;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_8 */
    public static final int CH3478 = 3478;

    /** Overbrugging rijblokkering rijtuig B - MIO_IDoorLoopBypa2_4_9 */
    public static final int CH3479 = 3479;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_1 */
    public static final int CH3480 = 3480;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_10 */
    public static final int CH3481 = 3481;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_11 */
    public static final int CH3482 = 3482;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_12 */
    public static final int CH3483 = 3483;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_13 */
    public static final int CH3484 = 3484;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_14 */
    public static final int CH3485 = 3485;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_15 */
    public static final int CH3486 = 3486;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_16 */
    public static final int CH3487 = 3487;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_17 */
    public static final int CH3488 = 3488;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_18 */
    public static final int CH3489 = 3489;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_19 */
    public static final int CH3490 = 3490;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_2 */
    public static final int CH3491 = 3491;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_20 */
    public static final int CH3492 = 3492;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_3 */
    public static final int CH3493 = 3493;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_4 */
    public static final int CH3494 = 3494;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_5 */
    public static final int CH3495 = 3495;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_6 */
    public static final int CH3496 = 3496;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_7 */
    public static final int CH3497 = 3497;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_8 */
    public static final int CH3498 = 3498;

    /** Deurlus gesloten van rijtuig A - MIO_IDoorLoopCl1_4_9 */
    public static final int CH3499 = 3499;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_1 */
    public static final int CH3500 = 3500;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_10 */
    public static final int CH3501 = 3501;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_11 */
    public static final int CH3502 = 3502;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_12 */
    public static final int CH3503 = 3503;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_13 */
    public static final int CH3504 = 3504;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_14 */
    public static final int CH3505 = 3505;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_15 */
    public static final int CH3506 = 3506;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_16 */
    public static final int CH3507 = 3507;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_17 */
    public static final int CH3508 = 3508;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_18 */
    public static final int CH3509 = 3509;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_19 */
    public static final int CH3510 = 3510;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_2 */
    public static final int CH3511 = 3511;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_20 */
    public static final int CH3512 = 3512;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_3 */
    public static final int CH3513 = 3513;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_4 */
    public static final int CH3514 = 3514;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_5 */
    public static final int CH3515 = 3515;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_6 */
    public static final int CH3516 = 3516;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_7 */
    public static final int CH3517 = 3517;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_8 */
    public static final int CH3518 = 3518;

    /** Deurlus gesloten van rijtuig B - MIO_IDoorLoopCl2_4_9 */
    public static final int CH3519 = 3519;

    /** ARR waarschuwing - ARR0101 */
    public static final int CH5000 = 5000;

    /** ARR uitgevallen - ARR0102 */
    public static final int CH5001 = 5001;

    /** - ATB0101 */
    public static final int CH5002 = 5002;

    /** ATB Storing - ATB0102 */
    public static final int CH5003 = 5003;

    /** ATB Storing - ATB0103 */
    public static final int CH5004 = 5004;

    /** ATB Storing - ATB0104 */
    public static final int CH5005 = 5005;

    /** ATB Storing - ATB0105 */
    public static final int CH5006 = 5006;

    /** ATB Storing - ATB0106 */
    public static final int CH5007 = 5007;

    /** ATB Storing - ATB0107 */
    public static final int CH5008 = 5008;

    /** - ATB0108 */
    public static final int CH5009 = 5009;

    /** - ATB0109 */
    public static final int CH5010 = 5010;

    /** ATB Storing - ATB010A */
    public static final int CH5011 = 5011;

    /** ATB Storing - ATB010B */
    public static final int CH5012 = 5012;

    /** ATB Storing - ATB010C */
    public static final int CH5013 = 5013;

    /** ATB Storing - ATB010D */
    public static final int CH5014 = 5014;

    /** ATB Storing - ATB010E */
    public static final int CH5015 = 5015;

    /** ATB Storing - ATB010F */
    public static final int CH5016 = 5016;

    /** ATB Storing - ATB0110 */
    public static final int CH5017 = 5017;

    /** ATB Storing - ATB0111 */
    public static final int CH5018 = 5018;

    /** ATB Storing - ATB0112 */
    public static final int CH5019 = 5019;

    /** ATB Storing - ATB0113 */
    public static final int CH5020 = 5020;

    /** - ATB0114 */
    public static final int CH5021 = 5021;

    /** ATB Storing - ATB0115 */
    public static final int CH5022 = 5022;

    /** ATB Storing - ATB0116 */
    public static final int CH5023 = 5023;

    /** ATB Storing - ATB0117 */
    public static final int CH5024 = 5024;

    /** ATB Storing - ATB0118 */
    public static final int CH5025 = 5025;

    /** ATB storing - ATB0119 */
    public static final int CH5026 = 5026;

    /** ATB storing - ATB011A */
    public static final int CH5027 = 5027;

    /** ATB Storing - ATB011B */
    public static final int CH5028 = 5028;

    /** ATB Storing - ATB011C */
    public static final int CH5029 = 5029;

    /** ATB Storing - ATB011D */
    public static final int CH5030 = 5030;

    /** ATB Storing - ATB011E */
    public static final int CH5031 = 5031;

    /** ATB Storing - ATB011F */
    public static final int CH5032 = 5032;

    /** ATB Storing - ATB0120 */
    public static final int CH5033 = 5033;

    /** ATB Storing - ATB0121 */
    public static final int CH5034 = 5034;

    /** ATB Storing - ATB0122 */
    public static final int CH5035 = 5035;

    /** ATB Storing - ATB0123 */
    public static final int CH5036 = 5036;

    /** ATB Storing - ATB0124 */
    public static final int CH5037 = 5037;

    /** ATB Storing - ATB0125 */
    public static final int CH5038 = 5038;

    /** ATB Storing - ATB0126 */
    public static final int CH5039 = 5039;

    /** ATB Storing - ATB0127 */
    public static final int CH5040 = 5040;

    /** ATB Storing - ATB0128 */
    public static final int CH5041 = 5041;

    /** ATB Storing - ATB0129 */
    public static final int CH5042 = 5042;

    /** ATB Storing - ATB012A */
    public static final int CH5043 = 5043;

    /** ATB Storing - ATB012B */
    public static final int CH5044 = 5044;

    /** ATB Storing - ATB012C */
    public static final int CH5045 = 5045;

    /** ATB Storing - ATB012D */
    public static final int CH5046 = 5046;

    /** ATB Storing - ATB012E */
    public static final int CH5047 = 5047;

    /** ATB Remtest mislukt (remcircuit 1) - ATB012F */
    public static final int CH5048 = 5048;

    /** ATB Remtest mislukt (remcircuit 2) - ATB0130 */
    public static final int CH5049 = 5049;

    /** ATB Remtest mislukt (overbruggingsrelais) - ATB0131 */
    public static final int CH5050 = 5050;

    /** ATB Remtest mislukt (VDX relais) - ATB0132 */
    public static final int CH5051 = 5051;

    /** ATB Storing - ATB0133 */
    public static final int CH5052 = 5052;

    /** ATB beperking - ATB0200 */
    public static final int CH5053 = 5053;

    /** ATB Storing - ATB0201 */
    public static final int CH5054 = 5054;

    /** ATB Storing - ATB0202 */
    public static final int CH5055 = 5055;

    /** ATB Storing - ATB0203 */
    public static final int CH5056 = 5056;

    /** ATB Storing - ATB0204 */
    public static final int CH5057 = 5057;

    /** ATB Storing - ATB0205 */
    public static final int CH5058 = 5058;

    /** ATB Storing - ATB0206 */
    public static final int CH5059 = 5059;

    /** ATB Storing - ATB0207 */
    public static final int CH5060 = 5060;

    /** Storing terugmelding remhendel - ATB0208 */
    public static final int CH5061 = 5061;

    /** ATB Storing - ATB020A */
    public static final int CH5062 = 5062;

    /** ATB Storing - ATB020B */
    public static final int CH5063 = 5063;

    /** ATB Storing - ATB020C */
    public static final int CH5064 = 5064;

    /** ATB Storing - ATB020D */
    public static final int CH5065 = 5065;

    /** ATB Storing - ATB020E */
    public static final int CH5066 = 5066;

    /** ATB Storing - ATB020F */
    public static final int CH5067 = 5067;

    /** ATB Storing - ATB0210 */
    public static final int CH5068 = 5068;

    /** ATB Storing - ATB0211 */
    public static final int CH5069 = 5069;

    /** ATB Storing - ATB0212 */
    public static final int CH5070 = 5070;

    /** ATB Storing - ATB0213 */
    public static final int CH5071 = 5071;

    /** ATB Storing - ATB0214 */
    public static final int CH5072 = 5072;

    /** ATB Storing - ATB0215 */
    public static final int CH5073 = 5073;

    /** ATB Storing - ATB0216 */
    public static final int CH5074 = 5074;

    /** ATB Storing - ATB0217 */
    public static final int CH5075 = 5075;

    /** - ATB0218 */
    public static final int CH5076 = 5076;

    /** ATB Storing - ATB0219 */
    public static final int CH5077 = 5077;

    /** ATB Storing - ATB021A */
    public static final int CH5078 = 5078;

    /** ATB Storing - ATB021B */
    public static final int CH5079 = 5079;

    /** ATB Storing - ATB021C */
    public static final int CH5080 = 5080;

    /** ATB Storing - ATB021D */
    public static final int CH5081 = 5081;

    /** ATB Storing - ATB021E */
    public static final int CH5082 = 5082;

    /** ATB Storing - ATB021F */
    public static final int CH5083 = 5083;

    /** ATB Storing - ATB0220 */
    public static final int CH5084 = 5084;

    /** ATB Storing - ATB0221 */
    public static final int CH5085 = 5085;

    /** ATB Storing - ATB0222 */
    public static final int CH5086 = 5086;

    /** ATB Storing - ATB0223 */
    public static final int CH5087 = 5087;

    /** ATB Storing - ATB0224 */
    public static final int CH5088 = 5088;

    /** ATB Storing - ATB0225 */
    public static final int CH5089 = 5089;

    /** ATB Storing - ATB0226 */
    public static final int CH5090 = 5090;

    /** ATB Storing - ATB0227 */
    public static final int CH5091 = 5091;

    /** ATB Storing - ATB0228 */
    public static final int CH5092 = 5092;

    /** ATB Storing - ATB0229 */
    public static final int CH5093 = 5093;

    /** ATB Storing - ATB022A */
    public static final int CH5094 = 5094;

    /** ATB Storing - ATB022B */
    public static final int CH5095 = 5095;

    /** ATB Storing - ATB022C */
    public static final int CH5096 = 5096;

    /** ATB Storing - ATB022D */
    public static final int CH5097 = 5097;

    /** ATB Storing - ATB022E */
    public static final int CH5098 = 5098;

    /** ATB Storing - ATB022F */
    public static final int CH5099 = 5099;

    /** ATB Storing - ATB0230 */
    public static final int CH5100 = 5100;

    /** ATB Storing - ATB0231 */
    public static final int CH5101 = 5101;

    /** ATB Storing - ATB0232 */
    public static final int CH5102 = 5102;

    /** ATB Storing - ATB0233 */
    public static final int CH5103 = 5103;

    /** ATB Storing - ATB0234 */
    public static final int CH5104 = 5104;

    /** ATB Storing - ATB0235 */
    public static final int CH5105 = 5105;

    /** ATB Storing - ATB0236 */
    public static final int CH5106 = 5106;

    /** ATB Storing - ATB0237 */
    public static final int CH5107 = 5107;

    /** ATB Storing - ATB0238 */
    public static final int CH5108 = 5108;

    /** ATB Storing - ATB0239 */
    public static final int CH5109 = 5109;

    /** ATB Storing - ATB023A */
    public static final int CH5110 = 5110;

    /** ATB Storing - ATB023B */
    public static final int CH5111 = 5111;

    /** ATB Storing - ATB023C */
    public static final int CH5112 = 5112;

    /** ATB Storing - ATB023D */
    public static final int CH5113 = 5113;

    /** ATB Storing - ATB023E */
    public static final int CH5114 = 5114;

    /** ATB Storing - ATB023F */
    public static final int CH5115 = 5115;

    /** ATB - herstart binnen 1 uur noodzakelijk - ATB0240 */
    public static final int CH5116 = 5116;

    /** ATB-herstart noodzakelijk - ATB0241 */
    public static final int CH5117 = 5117;

    /** ATB Storing - ATB0242 */
    public static final int CH5118 = 5118;

    /** ATB Storing baansignaal - ATB0243 */
    public static final int CH5119 = 5119;

    /** ATB Storing baansignaal - ATB0244 */
    public static final int CH5120 = 5120;

    /** ATB Storing baansignaal - ATB0245 */
    public static final int CH5121 = 5121;

    /** ATB storing - ATB0246 */
    public static final int CH5122 = 5122;

    /** ATB storing - ATB0247 */
    public static final int CH5123 = 5123;

    /** - ATB0301 */
    public static final int CH5124 = 5124;

    /** - ATB0302 */
    public static final int CH5125 = 5125;

    /** - ATB0303 */
    public static final int CH5126 = 5126;

    /** - ATB0304 */
    public static final int CH5127 = 5127;

    /** - ATB0305 */
    public static final int CH5128 = 5128;

    /** - ATB0306 */
    public static final int CH5129 = 5129;

    /** - ATB0307 */
    public static final int CH5130 = 5130;

    /** - ATB0308 */
    public static final int CH5131 = 5131;

    /** - ATB0309 */
    public static final int CH5132 = 5132;

    /** - ATB030A */
    public static final int CH5133 = 5133;

    /** - ATB030B */
    public static final int CH5134 = 5134;

    /** - ATB030C */
    public static final int CH5135 = 5135;

    /** - ATB030D */
    public static final int CH5136 = 5136;

    /** - ATB030E */
    public static final int CH5137 = 5137;

    /** - ATB030F */
    public static final int CH5138 = 5138;

    /** - ATB0310 */
    public static final int CH5139 = 5139;

    /** - ATB0311 */
    public static final int CH5140 = 5140;

    /** - ATB0312 */
    public static final int CH5141 = 5141;

    /** - ATB0313 */
    public static final int CH5142 = 5142;

    /** - ATB0314 */
    public static final int CH5143 = 5143;

    /** - ATB0315 */
    public static final int CH5144 = 5144;

    /** - ATB0316 */
    public static final int CH5145 = 5145;

    /** - ATB0317 */
    public static final int CH5146 = 5146;

    /** - ATB0401 */
    public static final int CH5147 = 5147;

    /** ATB storing - ATB0402 */
    public static final int CH5148 = 5148;

    /** - ATB0403 */
    public static final int CH5149 = 5149;

    /** - ATB0404 */
    public static final int CH5150 = 5150;

    /** - ATB0405 */
    public static final int CH5151 = 5151;

    /** ATB storing - ATB0406 */
    public static final int CH5152 = 5152;

    /** ATB storing - ATB0407 */
    public static final int CH5153 = 5153;

    /** ATB storing - ATB0408 */
    public static final int CH5154 = 5154;

    /** ATB storing - ATB0409 */
    public static final int CH5155 = 5155;

    /** ATB storing - ATB040A */
    public static final int CH5156 = 5156;

    /** ATB storing - ATB040B */
    public static final int CH5157 = 5157;

    /** ATB storing - ATB040C */
    public static final int CH5158 = 5158;

    /** - ATB040D */
    public static final int CH5159 = 5159;

    /** ATB nog niet opgestart - ATB040F */
    public static final int CH5160 = 5160;

    /** Relais â€˜cabine actiefâ€˜ onbetrouwbaar - BSS8000 */
    public static final int CH5161 = 5161;

    /** Relais â€˜cabine actiefâ€˜ onbetrouwbaar - BSS8001 */
    public static final int CH5162 = 5162;

    /** Signalering â€˜cabine actiefâ€˜ werkt niet - BSS8002 */
    public static final int CH5163 = 5163;

    /** Signalering â€˜cabine actiefâ€˜ werkt niet - BSS8003 */
    public static final int CH5164 = 5164;

    /** Signalering â€˜cabine actiefâ€˜ werkt niet - BSS8004 */
    public static final int CH5165 = 5165;

    /** Signalering â€˜cabine actiefâ€˜ werkt niet - BSS8005 */
    public static final int CH5166 = 5166;

    /** Activering cabine defect - BSS8006 */
    public static final int CH5167 = 5167;

    /** Activering cabine defect - BSS8007 */
    public static final int CH5168 = 5168;

    /** Deactivering cabine defect - BSS8008 */
    public static final int CH5169 = 5169;

    /** Deactivering paneel defect - BSS8009 */
    public static final int CH5170 = 5170;

    /** Stuurstroomvergrendeling defect - BSS800A */
    public static final int CH5171 = 5171;

    /** Stuurstroomvergrendeling defect - BSS800B */
    public static final int CH5172 = 5172;

    /** Stuurstroomvergrendeling defect - BSS800C */
    public static final int CH5173 = 5173;

    /** Stuurstroomvergrendeling defect - BSS800D */
    public static final int CH5174 = 5174;

    /** Relais â€˜Elektrisch gekoppeldâ€˜ niet bekrachtigd - BSS800E */
    public static final int CH5175 = 5175;

    /** Relais â€˜Elektrisch gekoppeldâ€˜ niet bekrachtigd - BSS800F */
    public static final int CH5176 = 5176;

    /** Relais â€˜Elektrisch gekoppeldâ€˜ niet bekrachtigd - BSS8010 */
    public static final int CH5177 = 5177;

    /** Relais â€˜Elektrisch gekoppeldâ€˜ niet bekrachtigd - BSS8011 */
    public static final int CH5178 = 5178;

    /** commando â€˜ontkoppelenâ€˜ werkt niet - BSS8012 */
    public static final int CH5179 = 5179;

    /** commando â€˜ontkoppelenâ€˜ werkt niet - BSS8013 */
    public static final int CH5180 = 5180;

    /** Ontkoppelen lukt niet - BSS8014 */
    public static final int CH5181 = 5181;

    /** Ontkoppelen lukt niet - BSS8015 */
    public static final int CH5182 = 5182;

    /** Onbedoelde treinsplitsing - BSS8016 */
    public static final int CH5183 = 5183;

    /** Onbedoelde treinsplitsing - BSS8017 */
    public static final int CH5184 = 5184;

    /** Omschakeling van gateway computer 1 naar 2 - BSS801C */
    public static final int CH5185 = 5185;

    /** Omschakeling van gateway computer 2 naar 1 - BSS801D */
    public static final int CH5186 = 5186;

    /** CCU-O 1 Mobad Batteriespannung zu gering - BSS8022 */
    public static final int CH5187 = 5187;

    /** CCU-O 2 Mobad Batteriespannung zu gering - BSS8023 */
    public static final int CH5188 = 5188;

    /** Dodeman storing Kanaal 1 en 2 - BSS8058 */
    public static final int CH5189 = 5189;

    /** Dodeman storing Kanaal 1 en 2 - BSS8059 */
    public static final int CH5190 = 5190;

    /** Dodeman storing Kanaal 1 - BSS805A */
    public static final int CH5191 = 5191;

    /** Dodeman storing Kanaal 2 - BSS805B */
    public static final int CH5192 = 5192;

    /** Dodeman storing Kanaal 1 - BSS805C */
    public static final int CH5193 = 5193;

    /** Dodeman storing Kanaal 2 - BSS805D */
    public static final int CH5194 = 5194;

    /** Tijd voor dodemanbeproeving niet geldig - BSS805E */
    public static final int CH5195 = 5195;

    /** Brandmelding tractieomvormer gestoord - BSS805F */
    public static final int CH5196 = 5196;

    /** Brandmelding tractieomvormer gestoord - BSS8060 */
    public static final int CH5197 = 5197;

    /** Brandmelding netfilteromvormer gestoord - BSS8061 */
    public static final int CH5198 = 5198;

    /** Brandmelding netfilteromvormer gestoord - BSS8062 */
    public static final int CH5199 = 5199;

    /** Brandmelding hulpbedrijfomvormer gestoord - BSS8063 */
    public static final int CH5200 = 5200;

    /** Brandmelding hulpbedrijfomvormer gestoord - BSS8064 */
    public static final int CH5201 = 5201;

    /** A- en B Sleutel geactiveerd - BSS8065 */
    public static final int CH5202 = 5202;

    /** mogelijke fout snelschakelaar - BSS806A */
    public static final int CH5203 = 5203;

    /** mogelijke fout snelschakelaar - BSS806B */
    public static final int CH5204 = 5204;

    /** geen terugmelding stroomafnemer hoog - BSS806C */
    public static final int CH5205 = 5205;

    /** geen terugmelding snelschakelaar ingeschakeld - BSS806D */
    public static final int CH5206 = 5206;

    /** geen terugmelding snelschakelaar uitgeschakeld - BSS806E */
    public static final int CH5207 = 5207;

    /** CCU-O 1 Mobad Batteriespannung gering - BSS9064 */
    public static final int CH5208 = 5208;

    /** CCU-O 2 Mobad Batteriespannung gering - BSS9065 */
    public static final int CH5209 = 5209;

    /** Meer dan Ã©Ã©n cabine ingeschakeld - CAB0001 */
    public static final int CH5210 = 5210;

    /** - CAB0101 */
    public static final int CH5211 = 5211;

    /** Signaal â€˜Activering Cabineâ€˜ onbetrouwbaar - CAB0102 */
    public static final int CH5212 = 5212;

    /** Drukknop â€˜Gereedâ€™ permanent IN - CAB0103 */
    public static final int CH5213 = 5213;

    /** Drukknop â€˜Reinigenâ€™ permanent IN - CAB0104 */
    public static final int CH5214 = 5214;

    /** Drukknop â€˜Sluimerenâ€™ permanent IN - CAB0105 */
    public static final int CH5215 = 5215;

    /** Drukknop â€˜Deuren ontgrendelen linksâ€™ perm. IN - CAB0106 */
    public static final int CH5216 = 5216;

    /** Drukknop â€˜Deuren ontgrendelen reâ€˜ perm. IN - CAB0107 */
    public static final int CH5217 = 5217;

    /** Drukknop â€˜Deuren links sluitenâ€™ permanent IN - CAB0108 */
    public static final int CH5218 = 5218;

    /** Drukknop â€˜Deuren rechts sluitenâ€™ permanent IN - CAB0109 */
    public static final int CH5219 = 5219;

    /** Sign. â€˜Deuren ontgrendelen liâ€™ onbetrouwbaar - CAB010A */
    public static final int CH5220 = 5220;

    /** Sign. â€˜Deuren ontgrendelen reâ€™ onbetrouwbaar - CAB010B */
    public static final int CH5221 = 5221;

    /** Schakelaar â€˜Stroomafnemer opâ€˜ permanent IN - CAB010C */
    public static final int CH5222 = 5222;

    /** Signaal â€˜Stroomafnemer opâ€˜ onbetrouwbaar - CAB010D */
    public static final int CH5223 = 5223;

    /** Schakelaar â€˜Stroomafnemer neerâ€˜ permanent IN - CAB010E */
    public static final int CH5224 = 5224;

    /** Schakelaar â€˜Stroomafnemerâ€˜ onbetrouwbaar - CAB010F */
    public static final int CH5225 = 5225;

    /** Schakelaar â€˜Snelschakelaar UITâ€˜ permanent IN - CAB0110 */
    public static final int CH5226 = 5226;

    /** Schakelaar â€˜Snelschakelaar INâ€˜ permanent IN - CAB0111 */
    public static final int CH5227 = 5227;

    /** Drukknop â€˜Snelschakelaarâ€˜ onbetrouwbaar - CAB0112 */
    public static final int CH5228 = 5228;

    /** Signaal â€˜Snelschakelaar UITâ€˜ onbetrouwbaar - CAB0113 */
    public static final int CH5229 = 5229;

    /** Signaal â€˜Snelschakelaar INâ€˜ onbetrouwbaar - CAB0114 */
    public static final int CH5230 = 5230;

    /** Schakelaar â€˜Treinverlichtingâ€˜ perm. In stand IN - CAB0115 */
    public static final int CH5231 = 5231;

    /** Schakelaar â€˜Treinverlichtingâ€˜ perm. In stand UIT - CAB0116 */
    public static final int CH5232 = 5232;

    /** Voorruitverwarming schakelt niet IN - CAB0117 */
    public static final int CH5233 = 5233;

    /** Voorruitverwarming schakelt niet UIT - CAB0118 */
    public static final int CH5234 = 5234;

    /** Melding onvoldoende ruitensproeiervloeistof - CAB0119 */
    public static final int CH5235 = 5235;

    /** Ruitenwisserinstallatie storing - CAB011A */
    public static final int CH5236 = 5236;

    /** Drukknop â€˜Voorruitverwarmingâ€˜ permanent IN - CAB011B */
    public static final int CH5237 = 5237;

    /** Rijrichtingschakelaar onbetrouwbaar - CAB011C */
    public static final int CH5238 = 5238;

    /** Sign. â€˜Rijrichtingschak. in 0â€˜ onbetrouwbaar - CAB011D */
    public static final int CH5239 = 5239;

    /** Sign. â€˜Rijrichtingschak. in Vâ€˜ onbetrouwbaar - CAB011E */
    public static final int CH5240 = 5240;

    /** Sign. â€˜Rijrichtingschak. in Aâ€˜ onbetrouwbaar - CAB011F */
    public static final int CH5241 = 5241;

    /** Rij-/ Remhendel onbetrouwbaar - CAB0120 */
    public static final int CH5242 = 5242;

    /** Sign. â€˜Rij-/ Remhendel in Rijdenâ€˜ onbetrouwbaar - CAB0121 */
    public static final int CH5243 = 5243;

    /** Sign. â€˜Rij-/ Remhendel in Remâ€˜ onbetrouwbaar - CAB0122 */
    public static final int CH5244 = 5244;

    /** Sign. â€˜Rij-/ Remhendel in S-Remâ€˜ onbetrouwbaar - CAB0123 */
    public static final int CH5245 = 5245;

    /** Sign. â€˜Rij-/ Remhendel in 0â€˜ onbetrouwbaar - CAB0124 */
    public static final int CH5246 = 5246;

    /** Rij-/ Remopdracht onbetrouwbaar - CAB0125 */
    public static final int CH5247 = 5247;

    /** Schakelaar â€˜Treinverlichtingâ€˜ onbetrouwbaar - CAB0126 */
    public static final int CH5248 = 5248;

    /** Drukknop â€˜Remproefâ€˜ permanent IN - CAB0127 */
    public static final int CH5249 = 5249;

    /** Signaal â€˜Remproefâ€˜ onbetrouwbaar - CAB0128 */
    public static final int CH5250 = 5250;

    /** Drukknop â€˜Parkeerrem inâ€˜ permanent IN - CAB0129 */
    public static final int CH5251 = 5251;

    /** Signaal â€˜Parkeerrem inâ€˜ onbetrouwbaar - CAB012A */
    public static final int CH5252 = 5252;

    /** Drukknop â€˜Parkeerrem lossenâ€˜ permanent IN - CAB012B */
    public static final int CH5253 = 5253;

    /** Signaal â€˜Parkeerrem lossenâ€˜ onbetrouwbaar - CAB012C */
    public static final int CH5254 = 5254;

    /** Schakelaar â€˜Compressorâ€˜ onbetrouwbaar - CAB012D */
    public static final int CH5255 = 5255;

    /** Schakelaar â€˜Compressorâ€˜ permanent in stand IN - CAB012E */
    public static final int CH5256 = 5256;

    /** Schakelaar â€˜Compressorâ€˜ perm. in stand UIT - CAB012F */
    public static final int CH5257 = 5257;

    /** Schakelaar â€˜Klimaat Rijtuigâ€˜ onbetrouwbaar - CAB0130 */
    public static final int CH5258 = 5258;

    /** Signaal â€˜Accordering A-storingâ€˜ onbetrouwbaar - CAB0131 */
    public static final int CH5259 = 5259;

    /** Signaal â€˜Accordering A-storingâ€˜ onbetrouwbaar - CAB0132 */
    public static final int CH5260 = 5260;

    /** Signaal â€˜Dodeman-bedieningâ€˜ onbetrouwbaar - CAB0133 */
    public static final int CH5261 = 5261;

    /** Sign. â€˜Overbrugging dodemanâ€˜ onbetrouwbaar - CAB0134 */
    public static final int CH5262 = 5262;

    /** Signaal â€˜Overbrugging ATBâ€˜ onbetrouwbaar - CAB0135 */
    public static final int CH5263 = 5263;

    /** Drukknop â€˜Overbruggen noodremâ€˜ perm. IN - CAB0136 */
    public static final int CH5264 = 5264;

    /** Drukknop â€˜Ontkoppelenâ€˜ permanent IN - CAB0137 */
    public static final int CH5265 = 5265;

    /** - CAB0201 */
    public static final int CH5266 = 5266;

    /** Signaal â€˜Activering Cabineâ€˜ onbetrouwbaar - CAB0202 */
    public static final int CH5267 = 5267;

    /** Drukknop â€˜Gereedâ€™ permanent IN - CAB0203 */
    public static final int CH5268 = 5268;

    /** Drukknop â€˜Reinigenâ€™ permanent IN - CAB0204 */
    public static final int CH5269 = 5269;

    /** Drukknop â€˜Sluimerenâ€™ permanent IN - CAB0205 */
    public static final int CH5270 = 5270;

    /** Drukknop â€˜Deuren ontgrendelen linksâ€™ perm. IN - CAB0206 */
    public static final int CH5271 = 5271;

    /** Drukknop â€˜Deuren ontgrendelen reâ€˜ perm. IN - CAB0207 */
    public static final int CH5272 = 5272;

    /** Drukknop â€˜Deuren links sluitenâ€™ permanent IN - CAB0208 */
    public static final int CH5273 = 5273;

    /** Drukknop â€˜Deuren rechts sluitenâ€™ permanent IN - CAB0209 */
    public static final int CH5274 = 5274;

    /** Sign. â€˜Deuren ontgrendelen liâ€™ onbetrouwbaar - CAB020A */
    public static final int CH5275 = 5275;

    /** Sign. â€˜Deuren ontgrendelen reâ€™ onbetrouwbaar - CAB020B */
    public static final int CH5276 = 5276;

    /** Schakelaar â€˜Stroomafnemer opâ€˜ permanent IN - CAB020C */
    public static final int CH5277 = 5277;

    /** Signaal â€˜Stroomafnemer opâ€˜ onbetrouwbaar - CAB020D */
    public static final int CH5278 = 5278;

    /** Schakelaar â€˜Stroomafnemer neerâ€˜ permanent IN - CAB020E */
    public static final int CH5279 = 5279;

    /** Schakelaar â€˜Stroomafnemerâ€˜ onbetrouwbaar - CAB020F */
    public static final int CH5280 = 5280;

    /** Schakelaar â€˜Snelschakelaar UITâ€˜ permanent IN - CAB0210 */
    public static final int CH5281 = 5281;

    /** Schakelaar â€˜Snelschakelaar INâ€˜ permanent IN - CAB0211 */
    public static final int CH5282 = 5282;

    /** Drukknop â€˜Snelschakelaarâ€˜ onbetrouwbaar - CAB0212 */
    public static final int CH5283 = 5283;

    /** Signaal â€˜Snelschakelaar UITâ€˜ onbetrouwbaar - CAB0213 */
    public static final int CH5284 = 5284;

    /** Signaal â€˜Snelschakelaar INâ€˜ onbetrouwbaar - CAB0214 */
    public static final int CH5285 = 5285;

    /** Schakelaar â€˜Treinverlichtingâ€˜ perm. In stand IN - CAB0215 */
    public static final int CH5286 = 5286;

    /** Schakelaar â€˜Treinverlichtingâ€˜ perm. In stand UIT - CAB0216 */
    public static final int CH5287 = 5287;

    /** Voorruitverwarming schakelt niet IN - CAB0217 */
    public static final int CH5288 = 5288;

    /** Voorruitverwarming schakelt niet UIT - CAB0218 */
    public static final int CH5289 = 5289;

    /** Melding onvoldoende ruitensproeiervloeistof - CAB0219 */
    public static final int CH5290 = 5290;

    /** Ruitenwisserinstallatie storing - CAB021A */
    public static final int CH5291 = 5291;

    /** Drukknop â€˜Voorruitverwarmingâ€˜ permanent IN - CAB021B */
    public static final int CH5292 = 5292;

    /** Rijrichtingschakelaar onbetrouwbaar - CAB021C */
    public static final int CH5293 = 5293;

    /** Sign. â€˜Rijrichtingschak. in 0â€˜ onbetrouwbaar - CAB021D */
    public static final int CH5294 = 5294;

    /** Sign. â€˜Rijrichtingschak. in Vâ€˜ onbetrouwbaar - CAB021E */
    public static final int CH5295 = 5295;

    /** Sign. â€˜Rijrichtingschak. in Aâ€˜ onbetrouwbaar - CAB021F */
    public static final int CH5296 = 5296;

    /** Rij-/ Remhendel onbetrouwbaar - CAB0220 */
    public static final int CH5297 = 5297;

    /** Sign. â€˜Rij-/ Remhendel in Rijdenâ€˜ onbetrouwbaar - CAB0221 */
    public static final int CH5298 = 5298;

    /** Sign. â€˜Rij-/ Remhendel in Remâ€˜ onbetrouwbaar - CAB0222 */
    public static final int CH5299 = 5299;

    /** Sign. â€˜Rij-/ Remhendel in S-Remâ€˜ onbetrouwbaar - CAB0223 */
    public static final int CH5300 = 5300;

    /** Sign. â€˜Rij-/ Remhendel in 0â€˜ onbetrouwbaar - CAB0224 */
    public static final int CH5301 = 5301;

    /** Rij-/ Remcurve onbetrouwbaar - CAB0225 */
    public static final int CH5302 = 5302;

    /** Schakelaar â€˜Treinverlichtingâ€˜ onbetrouwbaar - CAB0226 */
    public static final int CH5303 = 5303;

    /** Drukknop â€˜Remproefâ€˜ permanent IN - CAB0227 */
    public static final int CH5304 = 5304;

    /** Signaal â€˜Remproefâ€˜ onbetrouwbaar - CAB0228 */
    public static final int CH5305 = 5305;

    /** Drukknop â€˜Parkeerrem inâ€˜ permanent IN - CAB0229 */
    public static final int CH5306 = 5306;

    /** Signaal â€˜Parkeerrem inâ€˜ onbetrouwbaar - CAB022A */
    public static final int CH5307 = 5307;

    /** Drukknop â€˜Parkeerrem lossenâ€˜ permanent IN - CAB022B */
    public static final int CH5308 = 5308;

    /** Signaal â€˜Parkeerrem lossenâ€˜ onbetrouwbaar - CAB022C */
    public static final int CH5309 = 5309;

    /** Schakelaar â€˜Compressorâ€˜ onbetrouwbaar - CAB022D */
    public static final int CH5310 = 5310;

    /** Schakelaar â€˜Compressorâ€˜ permanent in stand IN - CAB022E */
    public static final int CH5311 = 5311;

    /** Schakelaar â€˜Compressorâ€˜ perm. in stand UIT - CAB022F */
    public static final int CH5312 = 5312;

    /** Schakelaar â€˜Klimaat Rijtuigâ€˜ onbetrouwbaar - CAB0230 */
    public static final int CH5313 = 5313;

    /** Signaal â€˜Accordering A-storingâ€˜ onbetrouwbaar - CAB0231 */
    public static final int CH5314 = 5314;

    /** Signaal â€˜Accordering A-storingâ€˜ onbetrouwbaar - CAB0232 */
    public static final int CH5315 = 5315;

    /** Signaal â€˜Dodeman-bedieningâ€˜ onbetrouwbaar - CAB0233 */
    public static final int CH5316 = 5316;

    /** Sign. â€˜Overbrugging dodemanâ€˜ onbetrouwbaar - CAB0234 */
    public static final int CH5317 = 5317;

    /** Signaal â€˜Overbrugging ATBâ€˜ onbetrouwbaar - CAB0235 */
    public static final int CH5318 = 5318;

    /** Drukknop â€˜Overbruggen noodremâ€˜ perm. IN - CAB0236 */
    public static final int CH5319 = 5319;

    /** Drukknop â€˜Ontkoppelenâ€˜ permanent IN - CAB0237 */
    public static final int CH5320 = 5320;

    /** Communicatie besturingssysteem gestoord - COM0002 */
    public static final int CH5321 = 5321;

    /** Verbroken verbinding tractiebesturing - COM0101 */
    public static final int CH5322 = 5322;

    /** Verbroken verbinding rembesturing - COM0102 */
    public static final int CH5323 = 5323;

    /** Verbroken verbinding WTB-Gateway - COM0103 */
    public static final int CH5324 = 5324;

    /** Verbroken verbinding CCU-O - COM0104 */
    public static final int CH5325 = 5325;

    /** Verbroken verbinding diagnosescherm - COM0108 */
    public static final int CH5326 = 5326;

    /** Verbroken verbinding ATB - COM0117 */
    public static final int CH5327 = 5327;

    /** Verbroken verbinding ARR - COM0118 */
    public static final int CH5328 = 5328;

    /** Verbroken verbinding frontdisplay 1 - COM012B */
    public static final int CH5329 = 5329;

    /** Verbroken verbinding airco reizigersruimte - COM012C */
    public static final int CH5330 = 5330;

    /** Verbroken verbinding airco cabine - COM012D */
    public static final int CH5331 = 5331;

    /** Verbroken verbinding tractiebesturing - COM0201 */
    public static final int CH5332 = 5332;

    /** Verbroken verbinding rembesturing - COM0202 */
    public static final int CH5333 = 5333;

    /** Verbroken verbinding WTB-Gateway - COM0203 */
    public static final int CH5334 = 5334;

    /** Verbroken verbinding CCU-O - COM0204 */
    public static final int CH5335 = 5335;

    /** Verbroken verbinding CCU-D - COM0205 */
    public static final int CH5336 = 5336;

    /** Verbroken verbinding CCU-C - COM0206 */
    public static final int CH5337 = 5337;

    /** Verbroken boord-wal verbinding - COM0207 */
    public static final int CH5338 = 5338;

    /** Verbroken verbinding diagnosescherm - COM0208 */
    public static final int CH5339 = 5339;

    /** Verbroken verbinding infotainment - COM020A */
    public static final int CH5340 = 5340;

    /** Automatische omroep gestoord - COM0219 */
    public static final int CH5341 = 5341;

    /** Verbroken verbinding frontdisplay 2 - COM022B */
    public static final int CH5342 = 5342;

    /** Verbroken verbinding airco reizigersruimte - COM022C */
    public static final int CH5343 = 5343;

    /** Verbroken verbinding airco cabine - COM022D */
    public static final int CH5344 = 5344;

    /** Verbroken verbinding buskoppeling - COM0309 */
    public static final int CH5345 = 5345;

    /** Verbroken verbinding batterijlader - COM0315 */
    public static final int CH5346 = 5346;

    /** Verbroken verbinding airco reizigersruimte - COM032C */
    public static final int CH5348 = 5348;

    /** Verbroken verbinding buskoppeling - COM0409 */
    public static final int CH5349 = 5349;

    /** Verbroken verbinding batterijlader - COM0415 */
    public static final int CH5350 = 5350;

    /** Verbroken verbinding airco reizigersruimte - COM042C */
    public static final int CH5352 = 5352;

    /** Verbroken verbinding airco reizigersruimte - COM052C */
    public static final int CH5353 = 5353;

    /** Verbroken verbinding airco reizigersruimte - COM062C */
    public static final int CH5354 = 5354;

    /** Verbroken verbinding IP-switch 1 - COM0701 */
    public static final int CH5355 = 5355;

    /** Verbroken verbinding IP-switch 2 - COM0702 */
    public static final int CH5356 = 5356;

    /** Verbroken verbinding IP-switch 3 - COM0703 */
    public static final int CH5357 = 5357;

    /** Verbroken verbinding IP-switch 4 - COM0704 */
    public static final int CH5358 = 5358;

    /** Verbroken verbinding IP-switch 5 - COM0705 */
    public static final int CH5359 = 5359;

    /** Verbroken verbinding IP-switch 6 - COM0706 */
    public static final int CH5360 = 5360;

    /** Verbroken verbinding IP-switch 7 - COM0707 */
    public static final int CH5361 = 5361;

    /** Verbroken verbinding IP-switch 8 - COM0708 */
    public static final int CH5362 = 5362;

    /** Reisinformatie-scherm 1 gestoord - COM0709 */
    public static final int CH5363 = 5363;

    /** Reisinformatie-scherm 2 gestoord - COM070A */
    public static final int CH5364 = 5364;

    /** Reisinformatie-scherm 3 gestoord - COM070B */
    public static final int CH5365 = 5365;

    /** Reisinformatie-scherm 4 gestoord - COM070C */
    public static final int CH5366 = 5366;

    /** Reisinformatie-scherm 5 gestoord - COM070D */
    public static final int CH5367 = 5367;

    /** Reisinformatie-scherm 6 gestoord - COM070E */
    public static final int CH5368 = 5368;

    /** Reisinformatie-scherm 7 gestoord - COM070F */
    public static final int CH5369 = 5369;

    /** Reisinformatie-scherm 8 gestoord - COM0710 */
    public static final int CH5370 = 5370;

    /** Reisinformatie-scherm 9 gestoord - COM0711 */
    public static final int CH5371 = 5371;

    /** Reisinformatie-scherm 10 gestoord - COM0712 */
    public static final int CH5372 = 5372;

    /** Reisinformatie-scherm 11 gestoord - COM0713 */
    public static final int CH5373 = 5373;

    /** Reisinformatie-scherm 12 gestoord - COM0714 */
    public static final int CH5374 = 5374;

    /** Reisinformatie-scherm 13 gestoord - COM0715 */
    public static final int CH5375 = 5375;

    /** Reisinformatie-scherm 14 gestoord - COM0716 */
    public static final int CH5376 = 5376;

    /** Reisinformatie-scherm 15 gestoord - COM0717 */
    public static final int CH5377 = 5377;

    /** Reisinformatie-scherm 16 gestoord - COM0718 */
    public static final int CH5378 = 5378;

    /** Reisinformatie-scherm 17 gestoord - COM0719 */
    public static final int CH5379 = 5379;

    /** Reisinformatie-scherm 18 gestoord - COM071A */
    public static final int CH5380 = 5380;

    /** Reisinformatie-scherm 19 gestoord - COM071B */
    public static final int CH5381 = 5381;

    /** Reisinformatie-scherm 20 gestoord - COM071C */
    public static final int CH5382 = 5382;

    /** Verbroken verbinding buitendisplay 1 - COM071D */
    public static final int CH5383 = 5383;

    /** Verbroken verbinding buitendisplay 2 - COM071E */
    public static final int CH5384 = 5384;

    /** Verbroken verbinding buitendisplay 3 - COM071F */
    public static final int CH5385 = 5385;

    /** Verbroken verbinding buitendisplay 4 - COM0720 */
    public static final int CH5386 = 5386;

    /** Verbroken verbinding buitendisplay 5 - COM0721 */
    public static final int CH5387 = 5387;

    /** Verbroken verbinding buitendisplay 6 - COM0722 */
    public static final int CH5388 = 5388;

    /** Verbroken verbinding buitendisplay 7 - COM0723 */
    public static final int CH5389 = 5389;

    /** Verbroken verbinding buitendisplay 8 - COM0724 */
    public static final int CH5390 = 5390;

    /** Verbroken verbinding buitendisplay 9 - COM0725 */
    public static final int CH5391 = 5391;

    /** Verbroken verbinding buitendisplay 10 - COM0726 */
    public static final int CH5392 = 5392;

    /** Verbroken verbinding buitendisplay 11 - COM0727 */
    public static final int CH5393 = 5393;

    /** Verbroken verbinding buitendisplay 12 - COM0728 */
    public static final int CH5394 = 5394;

    /** Verbroken verbinding deursturing 1 - COM0729 */
    public static final int CH5395 = 5395;

    /** Verbroken verbinding deursturing 2 - COM072A */
    public static final int CH5396 = 5396;

    /** Verbroken verbinding deursturing 3 - COM072B */
    public static final int CH5397 = 5397;

    /** Verbroken verbinding deursturing 4 - COM072C */
    public static final int CH5398 = 5398;

    /** Verbroken verbinding deursturing 5 - COM072D */
    public static final int CH5399 = 5399;

    /** Verbroken verbinding deursturing 6 - COM072E */
    public static final int CH5400 = 5400;

    /** Verbroken verbinding deursturing 7 - COM072F */
    public static final int CH5401 = 5401;

    /** Verbroken verbinding deursturing 8 - COM0730 */
    public static final int CH5402 = 5402;

    /** Verbroken verbinding deursturing 9 - COM0731 */
    public static final int CH5403 = 5403;

    /** Verbroken verbinding deursturing 10 - COM0732 */
    public static final int CH5404 = 5404;

    /** Verbroken verbinding deursturing 11 - COM0733 */
    public static final int CH5405 = 5405;

    /** Verbroken verbinding deursturing 12 - COM0734 */
    public static final int CH5406 = 5406;

    /** Verbroken verbinding deursturing 13 - COM0735 */
    public static final int CH5407 = 5407;

    /** Verbroken verbinding deursturing 14 - COM0736 */
    public static final int CH5408 = 5408;

    /** Verbroken verbinding deursturing 15 - COM0737 */
    public static final int CH5409 = 5409;

    /** Verbroken verbinding deursturing 16 - COM0738 */
    public static final int CH5410 = 5410;

    /** Verbroken verbinding deursturing 17 - COM0739 */
    public static final int CH5411 = 5411;

    /** Verbroken verbinding deursturing 18 - COM073A */
    public static final int CH5412 = 5412;

    /** Verbroken verbinding deursturing 19 - COM073B */
    public static final int CH5413 = 5413;

    /** Verbroken verbinding deursturing 20 - COM073C */
    public static final int CH5414 = 5414;

    /** Verbroken verbinding I/O-module - COM073D */
    public static final int CH5415 = 5415;

    /** Verbroken verbinding I/O-module - COM073E */
    public static final int CH5417 = 5417;

    /** Verbroken IP-verbinding WTB-Gateway - COM073F */
    public static final int CH5419 = 5419;

    /** Verbroken IP-verbinding WTB-Gateway - COM0740 */
    public static final int CH5421 = 5421;

    /** Verbroken IP-verbinding CCU-O - COM0741 */
    public static final int CH5423 = 5423;

    /** Verbroken IP-verbinding CCU-O - COM0742 */
    public static final int CH5425 = 5425;

    /** Ringswitch verbinding onderbroken - COM0743 */
    public static final int CH5427 = 5427;

    /** Ringswitch verbinding onderbroken - COM0744 */
    public static final int CH5429 = 5429;

    /** Ringswitch verbinding onderbroken - COM0745 */
    public static final int CH5431 = 5431;

    /** Ringswitch verbinding onderbroken - COM0746 */
    public static final int CH5433 = 5433;

    /** Ringswitch verbinding onderbroken - COM0747 */
    public static final int CH5435 = 5435;

    /** Ringswitch verbinding onderbroken - COM0748 */
    public static final int CH5437 = 5437;

    /** Ringswitch verbinding onderbroken - COM0749 */
    public static final int CH5439 = 5439;

    /** Ringswitch verbinding onderbroken - COM074A */
    public static final int CH5441 = 5441;

    /** Beperkte uitval reisinformatieschermen - COM074B */
    public static final int CH5443 = 5443;

    /** Beperkte uitval reisinformatieschermen - COM074C */
    public static final int CH5445 = 5445;

    /** Beperkte uitval reisinformatieschermen - COM074D */
    public static final int CH5447 = 5447;

    /** Beperkte uitval reisinformatieschermen - COM074E */
    public static final int CH5449 = 5449;

    /** Beperkte uitval reisinformatieschermen - COM074F */
    public static final int CH5451 = 5451;

    /** Beperkte uitval reisinformatieschermen - COM0750 */
    public static final int CH5453 = 5453;

    /** Beperkte uitval reisinformatieschermen - COM0751 */
    public static final int CH5455 = 5455;

    /** Uitval reisinformatieschermen - COM0752 */
    public static final int CH5457 = 5457;

    /** - COM0753 */
    public static final int CH5459 = 5459;

    /** - COM0754 */
    public static final int CH5460 = 5460;

    /** Automaat (21F01) geopend - DIA0101 */
    public static final int CH5461 = 5461;

    /** Automaat (22F01) geopend - DIA0104 */
    public static final int CH5462 = 5462;

    /** Automaat (22F02) geopend - DIA0105 */
    public static final int CH5463 = 5463;

    /** Automaat (22F03) geopend - DIA0106 */
    public static final int CH5464 = 5464;

    /** Automaat (22F05) geopend - DIA0107 */
    public static final int CH5465 = 5465;

    /** Automaat (23F01) geopend - DIA0108 */
    public static final int CH5466 = 5466;

    /** Automaat (23F02) geopend - DIA0109 */
    public static final int CH5467 = 5467;

    /** Automaat (23F03) aangesproken - DIA010A */
    public static final int CH5468 = 5468;

    /** Automaat (23F05) geopend - DIA010C */
    public static final int CH5469 = 5469;

    /** Automaat (23F06) geopend - DIA010D */
    public static final int CH5470 = 5470;

    /** Automaat (23F07) geopend - DIA010F */
    public static final int CH5471 = 5471;

    /** Automaat (23F09) geopend - DIA0111 */
    public static final int CH5472 = 5472;

    /** Automaat (24F01) geopend - DIA0118 */
    public static final int CH5473 = 5473;

    /** Automaat (24F02) aangesproken - DIA0119 */
    public static final int CH5474 = 5474;

    /** Automaat (27F01) geopend - DIA011E */
    public static final int CH5475 = 5475;

    /** Automaat (27F02) geopend - DIA011F */
    public static final int CH5476 = 5476;

    /** Automaat (28F01) geopend - DIA0121 */
    public static final int CH5477 = 5477;

    /** Automaat (28F02) geopend - DIA0122 */
    public static final int CH5478 = 5478;

    /** Automaat (31F08) geopend - DIA0129 */
    public static final int CH5479 = 5479;

    /** Automaat (32F03) geopend - DIA012B */
    public static final int CH5480 = 5480;

    /** Automaat (32F06) geopend - DIA012C */
    public static final int CH5481 = 5481;

    /** Automaat (32F05) geopend - DIA012D */
    public static final int CH5482 = 5482;

    /** Automaat (32F07) geopend - DIA012E */
    public static final int CH5483 = 5483;

    /** Automaat (33F01) geopend - DIA0131 */
    public static final int CH5484 = 5484;

    /** Automaat (33F02) geopend - DIA0132 */
    public static final int CH5485 = 5485;

    /** Automaat (33F11) geopend - DIA0133 */
    public static final int CH5486 = 5486;

    /** Automaat (34F01) geopend - DIA0134 */
    public static final int CH5487 = 5487;

    /** Automaat (34F02) geopend - DIA0135 */
    public static final int CH5488 = 5488;

    /** Motorbeveiligingsschak. tractievent. geopend - DIA0139 */
    public static final int CH5489 = 5489;

    /** Motorbeveiligingsschak. (34Q16) geopend - DIA013C */
    public static final int CH5490 = 5490;

    /** Motorbeveiligingsschak. tractievent. geopend - DIA013D */
    public static final int CH5491 = 5491;

    /** Motorbeveiligingsschak. (34Q32) geopend - DIA0140 */
    public static final int CH5492 = 5492;

    /** Motorbeveiligingsschak. (34Q41) geopend - DIA0141 */
    public static final int CH5493 = 5493;

    /** Automaat (42F01) geopend - DIA0142 */
    public static final int CH5494 = 5494;

    /** Automaat (42F02) geopend - DIA0143 */
    public static final int CH5495 = 5495;

    /** Automaat (43F01) geopend - DIA0144 */
    public static final int CH5496 = 5496;

    /** Automaat (43F04) geopend - DIA0146 */
    public static final int CH5497 = 5497;

    /** Automaat (43F05) geopend - DIA0147 */
    public static final int CH5498 = 5498;

    /** Automaat (44F01) geopend - DIA0149 */
    public static final int CH5499 = 5499;

    /** Automaat (45F01) geopend - DIA014A */
    public static final int CH5500 = 5500;

    /** Automaat (45F03) geopend - DIA014B */
    public static final int CH5501 = 5501;

    /** Automaat (46F01) geopend - DIA014C */
    public static final int CH5502 = 5502;

    /** Automaat (47F01) geopend - DIA014F */
    public static final int CH5503 = 5503;

    /** Automaat (51F01) geopend - DIA0150 */
    public static final int CH5504 = 5504;

    /** Automaat (51F02) geopend - DIA0151 */
    public static final int CH5505 = 5505;

    /** Automaat (51F03) geopend - DIA0153 */
    public static final int CH5506 = 5506;

    /** Automaat (53F01) geopend - DIA0154 */
    public static final int CH5507 = 5507;

    /** Automaat (53F02) geopend - DIA0155 */
    public static final int CH5508 = 5508;

    /** Automaat (53F03) geopend - DIA0156 */
    public static final int CH5509 = 5509;

    /** Automaat (61F01) geopend - DIA0157 */
    public static final int CH5510 = 5510;

    /** Automaat (61F02) geopend - DIA0158 */
    public static final int CH5511 = 5511;

    /** Automaat (62F01) geopend - DIA0159 */
    public static final int CH5512 = 5512;

    /** Automaat (62F02) geopend - DIA015A */
    public static final int CH5513 = 5513;

    /** Automaat (63F01) geopend - DIA015B */
    public static final int CH5514 = 5514;

    /** Automaat (63F02) geopend - DIA015C */
    public static final int CH5515 = 5515;

    /** Automaat (71F01) geopend - DIA015D */
    public static final int CH5516 = 5516;

    /** Automaat (71F02) geopend - DIA015E */
    public static final int CH5517 = 5517;

    /** Automaat (72F01) geopend - DIA015F */
    public static final int CH5518 = 5518;

    /** Automaat (81F01) geopend - DIA0160 */
    public static final int CH5519 = 5519;

    /** Automaat (81F02) geopend - DIA0161 */
    public static final int CH5520 = 5520;

    /** Automaat (81F03) geopend - DIA0162 */
    public static final int CH5521 = 5521;

    /** Automaat (82F01) geopend - DIA0163 */
    public static final int CH5522 = 5522;

    /** Automaat (82F02) geopend - DIA0164 */
    public static final int CH5523 = 5523;

    /** Automaat (46F03) geopend - DIA0167 */
    public static final int CH5524 = 5524;

    /** Automaat (21F01) geopend - DIA0201 */
    public static final int CH5525 = 5525;

    /** Automaat (22F01) geopend - DIA0204 */
    public static final int CH5526 = 5526;

    /** Automaat (22F02) geopend - DIA0205 */
    public static final int CH5527 = 5527;

    /** Automaat (22F03) geopend - DIA0206 */
    public static final int CH5528 = 5528;

    /** Automaat (22F05) geopend - DIA0207 */
    public static final int CH5529 = 5529;

    /** Automaat (23F01) geopend - DIA0208 */
    public static final int CH5530 = 5530;

    /** Automaat (23F02) geopend - DIA0209 */
    public static final int CH5531 = 5531;

    /** Automaat (23F03) aangesproken - DIA020A */
    public static final int CH5532 = 5532;

    /** Automaat (23F05) geopend - DIA020C */
    public static final int CH5533 = 5533;

    /** Automaat (23F06) geopend - DIA020D */
    public static final int CH5534 = 5534;

    /** Automaat (23F07) geopend - DIA020F */
    public static final int CH5535 = 5535;

    /** Automaat (23F09) geopend - DIA0211 */
    public static final int CH5536 = 5536;

    /** Automaat (24F01) geopend - DIA0218 */
    public static final int CH5537 = 5537;

    /** Automaat (24F02) aangesproken - DIA0219 */
    public static final int CH5538 = 5538;

    /** Automaat (24F03) geopend - DIA021A */
    public static final int CH5539 = 5539;

    /** Automaat (24F05) geopend - DIA021B */
    public static final int CH5540 = 5540;

    /** Automaat (27F01) geopend - DIA021E */
    public static final int CH5541 = 5541;

    /** Automaat (27F02) geopend - DIA021F */
    public static final int CH5542 = 5542;

    /** Automaat (27F04) geopend - DIA0220 */
    public static final int CH5543 = 5543;

    /** Automaat (28F01) geopend - DIA0221 */
    public static final int CH5544 = 5544;

    /** Automaat (28F02) geopend - DIA0222 */
    public static final int CH5545 = 5545;

    /** Automaat (31F08) geopend - DIA0229 */
    public static final int CH5546 = 5546;

    /** Automaat (32F03) geopend - DIA022B */
    public static final int CH5547 = 5547;

    /** Automaat (32F06) geopend - DIA022C */
    public static final int CH5548 = 5548;

    /** Automaat (32F05) geopend - DIA022D */
    public static final int CH5549 = 5549;

    /** Automaat (33F01) geopend - DIA0231 */
    public static final int CH5551 = 5551;

    /** Automaat (33F02) geopend - DIA0232 */
    public static final int CH5552 = 5552;

    /** Automaat (33F11) geopend - DIA0233 */
    public static final int CH5553 = 5553;

    /** Automaat (34F01) geopend - DIA0234 */
    public static final int CH5554 = 5554;

    /** Automaat (34F02) geopend - DIA0235 */
    public static final int CH5555 = 5555;

    /** Motorbeveiligingsschak. tractievent. geopend - DIA0239 */
    public static final int CH5556 = 5556;

    /** Motorbeveiligingsschak. (34Q16) geopend - DIA023C */
    public static final int CH5557 = 5557;

    /** Motorbeveiligingsschak. tractievent. geopend - DIA023D */
    public static final int CH5558 = 5558;

    /** Motorbeveiligingsschak. (34Q32) geopend - DIA0240 */
    public static final int CH5559 = 5559;

    /** Motorbeveiligingsschak. (34Q41) geopend - DIA0241 */
    public static final int CH5560 = 5560;

    /** Automaat (42F01) geopend - DIA0242 */
    public static final int CH5561 = 5561;

    /** Automaat (42F02) geopend - DIA0243 */
    public static final int CH5562 = 5562;

    /** Automaat (43F01) geopend - DIA0244 */
    public static final int CH5563 = 5563;

    /** Automaat (43F03) geopend - DIA0245 */
    public static final int CH5564 = 5564;

    /** Automaat (43F06) geopend - DIA0248 */
    public static final int CH5565 = 5565;

    /** Automaat (45F01) geopend - DIA024A */
    public static final int CH5566 = 5566;

    /** Automaat (45F03) geopend - DIA024B */
    public static final int CH5567 = 5567;

    /** Automaat (46F01) geopend - DIA024C */
    public static final int CH5568 = 5568;

    /** Automaat (46F02) geopend - DIA024D */
    public static final int CH5569 = 5569;

    /** Automaat (46F04) geopend - DIA024E */
    public static final int CH5570 = 5570;

    /** Automaat (47F01) geopend - DIA024F */
    public static final int CH5571 = 5571;

    /** Automaat (51F01) geopend - DIA0250 */
    public static final int CH5572 = 5572;

    /** Automaat (51F02) geopend - DIA0251 */
    public static final int CH5573 = 5573;

    /** Automaat (51F03) geopend - DIA0253 */
    public static final int CH5574 = 5574;

    /** Automaat (53F01) geopend - DIA0254 */
    public static final int CH5575 = 5575;

    /** Automaat (53F02) geopend - DIA0255 */
    public static final int CH5576 = 5576;

    /** Automaat (53F03) geopend - DIA0256 */
    public static final int CH5577 = 5577;

    /** Automaat (61F01) geopend - DIA0257 */
    public static final int CH5578 = 5578;

    /** Automaat (61F02) geopend - DIA0258 */
    public static final int CH5579 = 5579;

    /** Automaat (62F01) geopend - DIA0259 */
    public static final int CH5580 = 5580;

    /** Automaat (62F02) geopend - DIA025A */
    public static final int CH5581 = 5581;

    /** Automaat (63F01) geopend - DIA025B */
    public static final int CH5582 = 5582;

    /** Automaat (63F02) geopend - DIA025C */
    public static final int CH5583 = 5583;

    /** Automaat (71F01) geopend - DIA025D */
    public static final int CH5584 = 5584;

    /** Automaat (71F02) geopend - DIA025E */
    public static final int CH5585 = 5585;

    /** Automaat (72F01) geopend - DIA025F */
    public static final int CH5586 = 5586;

    /** Automaat (81F01) geopend - DIA0260 */
    public static final int CH5587 = 5587;

    /** Automaat (81F02) geopend - DIA0261 */
    public static final int CH5588 = 5588;

    /** Automaat (81F03) geopend - DIA0262 */
    public static final int CH5589 = 5589;

    /** Automaat (82F01) geopend - DIA0263 */
    public static final int CH5590 = 5590;

    /** Automaat (82F02) geopend - DIA0264 */
    public static final int CH5591 = 5591;

    /** Automaat (46F03) geopend - DIA0267 */
    public static final int CH5592 = 5592;

    /** Automaat (23F11) geopend - DIA0313 */
    public static final int CH5593 = 5593;

    /** Automaat (23F13) geopend - DIA0315 */
    public static final int CH5594 = 5594;

    /** Automaat (23F20) geopend - DIA0317 */
    public static final int CH5595 = 5595;

    /** Automaat (24F03) geopend - DIA031A */
    public static final int CH5596 = 5596;

    /** Automaat (24F04) geopend - DIA031B */
    public static final int CH5597 = 5597;

    /** Automaat (24F05) geopend - DIA031C */
    public static final int CH5598 = 5598;

    /** Automaat (24F06) geopend - DIA031D */
    public static final int CH5599 = 5599;

    /** Automaat (28F05) geopend - DIA0325 */
    public static final int CH5600 = 5600;

    /** Automaat (28F06) geopend - DIA0326 */
    public static final int CH5601 = 5601;

    /** Automaat (31F01) geopend - DIA0327 */
    public static final int CH5602 = 5602;

    /** Automaat (31F04) geopend - DIA0328 */
    public static final int CH5603 = 5603;

    /** Automaat (32F02 / 32Q01-F01) aangesproken - DIA032A */
    public static final int CH5604 = 5604;

    /** Automaat (32F03) geopend - DIA032B */
    public static final int CH5605 = 5605;

    /** Automaat (32F04 / 32Q01-F02) aangesproken - DIA032C */
    public static final int CH5606 = 5606;

    /** Automaat (33F01) geopend - DIA0331 */
    public static final int CH5607 = 5607;

    /** Automaat (33F11) geopend - DIA0333 */
    public static final int CH5608 = 5608;

    /** Automaat (42F01) geopend - DIA0342 */
    public static final int CH5609 = 5609;

    /** Automaat (46F01) geopend - DIA034C */
    public static final int CH5610 = 5610;

    /** Automaat (53F01) geopend - DIA0354 */
    public static final int CH5611 = 5611;

    /** Automaat (53F02) geopend - DIA0355 */
    public static final int CH5612 = 5612;

    /** Automaat (53F03) geopend - DIA0356 */
    public static final int CH5613 = 5613;

    /** Automaat (61F01) geopend - DIA0357 */
    public static final int CH5614 = 5614;

    /** Automaat (61F02) geopend - DIA0358 */
    public static final int CH5615 = 5615;

    /** Automaat (82F01) geopend - DIA0363 */
    public static final int CH5616 = 5616;

    /** Automaat (82F02) geopend - DIA0364 */
    public static final int CH5617 = 5617;

    /** Automaat (82F03) geopend - DIA0365 */
    public static final int CH5618 = 5618;

    /** Automaat (82F04) geopend - DIA0366 */
    public static final int CH5619 = 5619;

    /** Automaat (46F03) geopend - DIA0367 */
    public static final int CH5620 = 5620;

    /** Automaat (21F11) geopend - DIA0402 */
    public static final int CH5621 = 5621;

    /** Automaat (21F12) geopend - DIA0403 */
    public static final int CH5622 = 5622;

    /** Automaat (28F05) geopend - DIA0425 */
    public static final int CH5623 = 5623;

    /** Automaat (28F06) geopend - DIA0426 */
    public static final int CH5624 = 5624;

    /** Automaat (31F01) geopend - DIA0427 */
    public static final int CH5625 = 5625;

    /** Automaat (32F02 / 32Q01-F01) aangesproken - DIA042A */
    public static final int CH5626 = 5626;

    /** Automaat (32F03) geopend - DIA042B */
    public static final int CH5627 = 5627;

    /** Automaat (32F04 / 32Q01-F02) aangesproken - DIA042C */
    public static final int CH5628 = 5628;

    /** Automaat (33F01) geopend - DIA0431 */
    public static final int CH5629 = 5629;

    /** Automaat (33F11) geopend - DIA0433 */
    public static final int CH5630 = 5630;

    /** Automaat (34F03) geopend - DIA0436 */
    public static final int CH5631 = 5631;

    /** Automaat (34F04) geopend - DIA0437 */
    public static final int CH5632 = 5632;

    /** Motorbeveiligingsschak. (34Q5) geopend - DIA0438 */
    public static final int CH5633 = 5633;

    /** Automaat (42F01) geopend - DIA0442 */
    public static final int CH5634 = 5634;

    /** Automaat (46F01) geopend - DIA044C */
    public static final int CH5635 = 5635;

    /** Automaat (53F01) geopend - DIA0454 */
    public static final int CH5636 = 5636;

    /** Automaat (53F02) geopend - DIA0455 */
    public static final int CH5637 = 5637;

    /** Automaat (53F03) geopend - DIA0456 */
    public static final int CH5638 = 5638;

    /** Automaat (61F01) geopend - DIA0457 */
    public static final int CH5639 = 5639;

    /** Automaat (61F02) geopend - DIA0458 */
    public static final int CH5640 = 5640;

    /** Automaat (82F01) geopend - DIA0463 */
    public static final int CH5641 = 5641;

    /** Automaat (82F02) geopend - DIA0464 */
    public static final int CH5642 = 5642;

    /** Automaat (82F03) geopend - DIA0465 */
    public static final int CH5643 = 5643;

    /** Automaat (82F04) geopend - DIA0466 */
    public static final int CH5644 = 5644;

    /** Automaat (46F03) geopend - DIA0467 */
    public static final int CH5645 = 5645;

    /** Automaat (28F03) geopend - DIA0523 */
    public static final int CH5646 = 5646;

    /** Automaat (28F04) geopend - DIA0524 */
    public static final int CH5647 = 5647;

    /** Automaat (33F01) geopend - DIA0531 */
    public static final int CH5648 = 5648;

    /** Automaat (33F11) geopend - DIA0533 */
    public static final int CH5649 = 5649;

    /** Automaat (42F01) geopend - DIA0542 */
    public static final int CH5650 = 5650;

    /** Automaat (46F01) geopend - DIA054C */
    public static final int CH5651 = 5651;

    /** Automaat (53F01) geopend - DIA0554 */
    public static final int CH5652 = 5652;

    /** Automaat (53F02) geopend - DIA0555 */
    public static final int CH5653 = 5653;

    /** Automaat (53F03) geopend - DIA0556 */
    public static final int CH5654 = 5654;

    /** Automaat (61F01) geopend - DIA0557 */
    public static final int CH5655 = 5655;

    /** Automaat (61F02) geopend - DIA0558 */
    public static final int CH5656 = 5656;

    /** Automaat (82F01) geopend - DIA0563 */
    public static final int CH5657 = 5657;

    /** Automaat (82F02) geopend - DIA0564 */
    public static final int CH5658 = 5658;

    /** Automaat (82F03) geopend - DIA0565 */
    public static final int CH5659 = 5659;

    /** Automaat (46F04) geopend - DIA0566 */
    public static final int CH5660 = 5660;

    /** Automaat (46F03) geopend - DIA0567 */
    public static final int CH5661 = 5661;

    /** Automaat (23F11) geopend - DIA0613 */
    public static final int CH5662 = 5662;

    /** Automaat (23F20) geopend - DIA0617 */
    public static final int CH5663 = 5663;

    /** Automaat (24F03) geopend - DIA061A */
    public static final int CH5664 = 5664;

    /** Automaat (24F05) geopend - DIA061C */
    public static final int CH5665 = 5665;

    /** Automaat (28F03) geopend - DIA0623 */
    public static final int CH5666 = 5666;

    /** Automaat (28F04) geopend - DIA0624 */
    public static final int CH5667 = 5667;

    /** Automaat (33F01) geopend - DIA0631 */
    public static final int CH5668 = 5668;

    /** Automaat (33F11) geopend - DIA0633 */
    public static final int CH5669 = 5669;

    /** Automaat (42F01) geopend - DIA0642 */
    public static final int CH5670 = 5670;

    /** Automaat (46F01) geopend - DIA064C */
    public static final int CH5671 = 5671;

    /** Automaat (53F01) geopend - DIA0654 */
    public static final int CH5672 = 5672;

    /** Automaat (53F02) geopend - DIA0655 */
    public static final int CH5673 = 5673;

    /** Automaat (53F03) geopend - DIA0656 */
    public static final int CH5674 = 5674;

    /** Automaat (61F01) geopend - DIA0657 */
    public static final int CH5675 = 5675;

    /** Automaat (61F02) geopend - DIA0658 */
    public static final int CH5676 = 5676;

    /** Automaat (82F01) geopend - DIA0663 */
    public static final int CH5677 = 5677;

    /** Automaat (82F02) geopend - DIA0664 */
    public static final int CH5678 = 5678;

    /** Automaat (82F03) geopend - DIA0665 */
    public static final int CH5679 = 5679;

    /** Automaat (82F04) geopend - DIA0666 */
    public static final int CH5680 = 5680;

    /** Automaat (46F03) geopend - DIA0667 */
    public static final int CH5681 = 5681;

    /** Comm. â€˜Deuren ontgrendelen liâ€™ onbetrouwbaar - DIA7000 */
    public static final int CH5682 = 5682;

    /** Comm. â€˜Deuren ontgrendelen reâ€™ onbetrouwbaar - DIA7001 */
    public static final int CH5683 = 5683;

    /** Treindraad â€˜Deuren ontgrendelen linksâ€˜ storing - DIA7002 */
    public static final int CH5684 = 5684;

    /** Treindraad â€˜Deuren ontgrendelen rechtsâ€˜ storing - DIA7003 */
    public static final int CH5685 = 5685;

    /** Bedrijfsurenteller stroomafnemer 1 overschreden - DIA7004 */
    public static final int CH5686 = 5686;

    /** Bedrijfsurenteller stroomafnemer 2 overschreden - DIA7005 */
    public static final int CH5687 = 5687;

    /** Bedrijfsurenteller stroomafnemer 1 overschreden - DIA7006 */
    public static final int CH5688 = 5688;

    /** Bedrijfsurenteller stroomafnemer 2 overschreden - DIA7007 */
    public static final int CH5689 = 5689;

    /** Bedrijfskilometerteller stroomafnemer 1 overschreden - DIA7008 */
    public static final int CH5690 = 5690;

    /** Bedrijfskilometerteller stroomafnemer 2 overschreden - DIA7009 */
    public static final int CH5691 = 5691;

    /** Teller snelschakelaar overschreden - DIA700A */
    public static final int CH5692 = 5692;

    /** Elektr. / mech. Koppeling onbetrouwbaar - DIA700B */
    public static final int CH5693 = 5693;

    /** Elektr. / mech. Koppeling onbetrouwbaar - DIA700C */
    public static final int CH5694 = 5694;

    /** Signaal elektrische Koppeling onbetrouwbaar - DIA700D */
    public static final int CH5695 = 5695;

    /** Signaal elektrische Koppeling onbetrouwbaar - DIA700E */
    public static final int CH5696 = 5696;

    /** Signaal elektrische Koppeling onbetrouwbaar - DIA700F */
    public static final int CH5697 = 5697;

    /** Signaal elektrische Koppeling onbetrouwbaar - DIA7010 */
    public static final int CH5698 = 5698;

    /** Treinconfiguratie onbetrouwbaar - DIA7011 */
    public static final int CH5699 = 5699;

    /** Bedrijfsurenteller Compressor overschreden - DIA7012 */
    public static final int CH5700 = 5700;

    /** Bedrijfsurenteller Compressor overschreden - DIA7013 */
    public static final int CH5701 = 5701;

    /** Bedrijfsurenteller hulpbedrijfscompressor overschreden - DIA7014 */
    public static final int CH5702 = 5702;

    /** Bedrijfsurenteller hulpbedrijfscompressor overschreden - DIA7015 */
    public static final int CH5703 = 5703;

    /** Kilometerteller overschreden - DIA7016 */
    public static final int CH5704 = 5704;

    /** Foutgeheugen 90% vol - DIA7017 */
    public static final int CH5705 = 5705;

    /** Foutgeheugen vol - DIA7018 */
    public static final int CH5706 = 5706;

    /** Stroomafn. 1 Drukschakelaar onbetrouwbaar - DIA7019 */
    public static final int CH5707 = 5707;

    /** Stroomafn. 2 Drukschakelaar onbetrouwbaar - DIA701A */
    public static final int CH5708 = 5708;

    /** Stroomafn. 1 Keuzeschakel. onbetrouwbaar - DIA701B */
    public static final int CH5709 = 5709;

    /** Stroomafn. 2 Keuzeschakel. onbetrouwbaar - DIA701C */
    public static final int CH5710 = 5710;

    /** Sig. â€˜Stroomafnemer houdenâ€˜ onbetrouwbaar - DIA701D */
    public static final int CH5711 = 5711;

    /** Tractie afgeschakeld - DIA701F */
    public static final int CH5712 = 5712;

    /** Tractie afgeschakeld - DIA7020 */
    public static final int CH5713 = 5713;

    /** Tractie afgeschakeld - DIA7021 */
    public static final int CH5714 = 5714;

    /** Tractie afgeschakeld - DIA7022 */
    public static final int CH5715 = 5715;

    /** Storing diagnosescherm - DIA7028 */
    public static final int CH5716 = 5716;

    /** Storing diagnosescherm - DIA7029 */
    public static final int CH5717 = 5717;

    /** CCU-O storing - DIA702A */
    public static final int CH5718 = 5718;

    /** CCU-O storing - DIA702B */
    public static final int CH5719 = 5719;

    /** I/O-Module storing - DIA702C */
    public static final int CH5720 = 5720;

    /** I/O-Module storing - DIA702D */
    public static final int CH5721 = 5721;

    /** I/O-Module storing - DIA702E */
    public static final int CH5722 = 5722;

    /** I/O-Module storing - DIA702F */
    public static final int CH5723 = 5723;

    /** I/O-Module storing - DIA7030 */
    public static final int CH5724 = 5724;

    /** I/O-Module storing - DIA7031 */
    public static final int CH5725 = 5725;

    /** I/O-Module storing - DIA7032 */
    public static final int CH5726 = 5726;

    /** I/O-Module storing - DIA7033 */
    public static final int CH5727 = 5727;

    /** I/O-Module storing - DIA7034 */
    public static final int CH5728 = 5728;

    /** I/O-Module storing - DIA7035 */
    public static final int CH5729 = 5729;

    /** I/O-Module storing - DIA7036 */
    public static final int CH5730 = 5730;

    /** I/O-Module storing - DIA7037 */
    public static final int CH5731 = 5731;

    /** I/O-Module storing - DIA7038 */
    public static final int CH5732 = 5732;

    /** I/O-Module storing - DIA7039 */
    public static final int CH5733 = 5733;

    /** I/O-Module storing - DIA703A */
    public static final int CH5734 = 5734;

    /** I/O-Module storing - DIA703B */
    public static final int CH5735 = 5735;

    /** I/O-Module storing - DIA703C */
    public static final int CH5736 = 5736;

    /** I/O-Module storing - DIA703D */
    public static final int CH5737 = 5737;

    /** I/O-Module storing - DIA703E */
    public static final int CH5738 = 5738;

    /** I/O-Module storing - DIA703F */
    public static final int CH5739 = 5739;

    /** I/O-Module storing - DIA7040 */
    public static final int CH5740 = 5740;

    /** I/O-Module storing - DIA7041 */
    public static final int CH5741 = 5741;

    /** Parameter: Maximaal audioniveau OUT1 - DIA7042 */
    public static final int CH5742 = 5742;

    /** Parameter: Minimaal audioniveau OUT1 - DIA7043 */
    public static final int CH5743 = 5743;

    /** Par.: Aantal personeelsoproep (gong of jingle) - DIA7044 */
    public static final int CH5744 = 5744;

    /** Parameter: Aantal PO (gong of jingle) - DIA7045 */
    public static final int CH5745 = 5745;

    /** Par.: Deursluiting na einde waarschuwingstoon - DIA7046 */
    public static final int CH5746 = 5746;

    /** Par.: Deursluiting na start waarschuwingstoon - DIA7047 */
    public static final int CH5747 = 5747;

    /** Parameter: Duur deuropening - DIA7048 */
    public static final int CH5748 = 5748;

    /** Parameter: Aantal retourbewegingen deur - DIA7049 */
    public static final int CH5749 = 5749;

    /** Par.: Deuropening na start waarschuwingstoon - DIA704A */
    public static final int CH5750 = 5750;

    /** Par.: Herstart deursluiting na obstakel - DIA704B */
    public static final int CH5751 = 5751;

    /** Par.: Herstart trede intrekken na obstakel - DIA704C */
    public static final int CH5752 = 5752;

    /** Parameter: Aantal PRM (gong of jingle) - DIA704D */
    public static final int CH5753 = 5753;

    /** Par.: Altern. curve voor richtwaarde Rijden - DIA704E */
    public static final int CH5754 = 5754;

    /** Par.: Altern. waarde voor richtwaarde Rijden - DIA704F */
    public static final int CH5755 = 5755;

    /** Par.: Offset temperatuur passagiersruimte - DIA7050 */
    public static final int CH5756 = 5756;

    /** Parameter: Inschakeltijd LED â€˜ontkoppelenâ€˜ - DIA7051 */
    public static final int CH5757 = 5757;

    /** Parameter: Min. temperatuur CLEANING - DIA7052 */
    public static final int CH5758 = 5758;

    /** Parameter: Min. temperatuur warmhouden - DIA7053 */
    public static final int CH5759 = 5759;

    /** Par.: Uitschakeltijd voorruitverwarming - DIA7054 */
    public static final int CH5760 = 5760;

    /** Par.: Inschakeltemp. koppelingsverwarming - DIA7055 */
    public static final int CH5761 = 5761;

    /** Parameter: Hysterese koppelingsverwarming - DIA7056 */
    public static final int CH5762 = 5762;

    /** Par.: Uitschakeltemp. voorruitverwarming - DIA7057 */
    public static final int CH5763 = 5763;

    /** Parameter: HMI-uitschakeling - DIA7058 */
    public static final int CH5764 = 5764;

    /** Parameter: Zomertijd einde week - DIA7059 */
    public static final int CH5765 = 5765;

    /** Parameter: Zomertijd einde verschil - DIA705A */
    public static final int CH5766 = 5766;

    /** Parameter: Zomertijd einde uur - DIA705B */
    public static final int CH5767 = 5767;

    /** Parameter: Zomertijd einde maand - DIA705C */
    public static final int CH5768 = 5768;

    /** Parameter: Zomertijd einde dag - DIA705D */
    public static final int CH5769 = 5769;

    /** Parameter: Zomertijd start verschil - DIA705E */
    public static final int CH5770 = 5770;

    /** Parameter: Zomertijd start uur - DIA705F */
    public static final int CH5771 = 5771;

    /** Parameter: Zomertijd start maand - DIA7060 */
    public static final int CH5772 = 5772;

    /** Parameter: Zomertijd start dag - DIA7061 */
    public static final int CH5773 = 5773;

    /** Parameter: Zomertijd start week - DIA7062 */
    public static final int CH5774 = 5774;

    /** Parameter: HMI-taal cabine 1 - DIA7063 */
    public static final int CH5775 = 5775;

    /** Parameter: HMI-taal cabine 2 - DIA7064 */
    public static final int CH5776 = 5776;

    /** Par.: Autom. uitschakeltijd verl. pass. ruimte - DIA7065 */
    public static final int CH5777 = 5777;

    /** Parameter: Autom. uitschakeltijd displays - DIA7066 */
    public static final int CH5778 = 5778;

    /** Par.: Tijd tot bedrijfsmode wisseling - DIA7067 */
    public static final int CH5779 = 5779;

    /** Par.: Autom. wisselen van CLEANING naar DOZE - DIA7068 */
    public static final int CH5780 = 5780;

    /** Par.: Autom. wisselen van READY naar DOZE - DIA7069 */
    public static final int CH5781 = 5781;

    /** Par.: Uitschakeltijd noodvent. temp. te hoog - DIA706A */
    public static final int CH5782 = 5782;

    /** Par: Inschakeltijd noodvent. temp. te hoog - DIA706B */
    public static final int CH5783 = 5783;

    /** Par.: Uitschakeltijd noodventilatie temp. te laag - DIA706C */
    public static final int CH5784 = 5784;

    /** Par.: Inschakeltijd noodventilatie temp. te laag - DIA706D */
    public static final int CH5785 = 5785;

    /** Par.: Offset Zomer/Winter noodventilatie - DIA706E */
    public static final int CH5786 = 5786;

    /** Parameter: Zomer/Winter noodventilatie - DIA706F */
    public static final int CH5787 = 5787;

    /** Par: Inschakelvertraging noodv. temp. te hoog - DIA7070 */
    public static final int CH5788 = 5788;

    /** Par.: Inschakelvertrag. noodvent. temp. te laag - DIA7071 */
    public static final int CH5789 = 5789;

    /** Parameter: Inschakeltijd noodvent. zomer - DIA7072 */
    public static final int CH5790 = 5790;

    /** Parameter: Deursluiting na CLEANING - DIA7073 */
    public static final int CH5791 = 5791;

    /** Par.: Vrijgavetemp. voorruitverwarming - DIA7074 */
    public static final int CH5792 = 5792;

    /** Parameter: Volumeregeling interne PA - DIA7075 */
    public static final int CH5793 = 5793;

    /** Parameter: Volumeregeling externe PA - DIA7076 */
    public static final int CH5794 = 5794;

    /** Parameter: Volumeregeling PRM - DIA7077 */
    public static final int CH5795 = 5795;

    /** Parameter: Volumeregeling cabine - DIA7078 */
    public static final int CH5796 = 5796;

    /** Parameter: Aantal PCU alarmtoon - DIA7079 */
    public static final int CH5797 = 5797;

    /** Parameter: Starttijd voor super silent mode - DIA707A */
    public static final int CH5798 = 5798;

    /** Parameter: Starttijd voor super silent mode - DIA707B */
    public static final int CH5799 = 5799;

    /** Parameter: Eindtijd voor super silent mode - DIA707C */
    public static final int CH5800 = 5800;

    /** Parameter: Eindtijd voor super silent mode - DIA707D */
    public static final int CH5801 = 5801;

    /** Sub-bedrijfstoestand â€˜ON TRANSPORTâ€˜ actief - DIA9000 */
    public static final int CH5802 = 5802;

    /** Sub-bedrijfstoestand â€˜WORKSHOPâ€˜ actief - DIA9001 */
    public static final int CH5803 = 5803;

    /** Cabine 1 ingeschakeld - DIA9002 */
    public static final int CH5804 = 5804;

    /** Cabine 2 ingeschakeld - DIA9003 */
    public static final int CH5805 = 5805;

    /** Wijziging van de treinconfiguratie - DIA9004 */
    public static final int CH5806 = 5806;

    /** Treininitialisatie met â€˜Jaâ€˜ beantwoord - DIA9005 */
    public static final int CH5807 = 5807;

    /** Treininitialisatie met â€˜Neeâ€˜ beantwoord - DIA9006 */
    public static final int CH5808 = 5808;

    /** Sleepbedrijf - DIA9007 */
    public static final int CH5809 = 5809;

    /** Storingsrit - DIA9008 */
    public static final int CH5810 = 5810;

    /** 110V noodstroomvoorziening - DIA9009 */
    public static final int CH5811 = 5811;

    /** A-fout bevestigd - DIA900A */
    public static final int CH5812 = 5812;

    /** Drukknop storingsrit ingedrukt - DIA9015 */
    public static final int CH5813 = 5813;

    /** Drukknop storingsrit ingedrukt - DIA9016 */
    public static final int CH5814 = 5814;

    /** Reset historiegeheugen rembesturing 1 - DIA9017 */
    public static final int CH5815 = 5815;

    /** Reset historiegeheugen rembesturing 2 - DIA9018 */
    public static final int CH5816 = 5816;

    /** Reset historiegeheugen airco in cabine - DIA9019 */
    public static final int CH5817 = 5817;

    /** Reset historiegeheugen airco in cabine - DIA901A */
    public static final int CH5818 = 5818;

    /** Reset historiegeheugen airco in reizigersruimte - DIA901B */
    public static final int CH5819 = 5819;

    /** Reset historiegeheugen airco in reizigersruimte - DIA901C */
    public static final int CH5820 = 5820;

    /** Reset historiegeheugen airco in reizigersruimte - DIA901D */
    public static final int CH5821 = 5821;

    /** Reset historiegeheugen airco in reizigersruimte - DIA901E */
    public static final int CH5822 = 5822;

    /** Reset historiegeheugen airco in reizigersruimte - DIA901F */
    public static final int CH5823 = 5823;

    /** Reset historiegeheugen airco in reizigersruimte - DIA9020 */
    public static final int CH5824 = 5824;

    /** Reset historiegeheugen tractiebesturing - DIA9021 */
    public static final int CH5825 = 5825;

    /** Bedrijfsmode â€˜OPERATEâ€˜ actief - DIA9022 */
    public static final int CH5826 = 5826;

    /** Bedrijfsmode â€˜READYâ€˜ actief - DIA9023 */
    public static final int CH5827 = 5827;

    /** Bedrijfsmode â€˜DOZEâ€˜ actief - DIA9024 */
    public static final int CH5828 = 5828;

    /** Bedrijfsmode â€˜CLEANINGâ€˜ actief - DIA9025 */
    public static final int CH5829 = 5829;

    /** Stroomafnemer 1 op - DIA9026 */
    public static final int CH5830 = 5830;

    /** Stroomafnemer 2 op - DIA9027 */
    public static final int CH5831 = 5831;

    /** Snelschakelaar IN - DIA9028 */
    public static final int CH5832 = 5832;

    /** Stoorstroombewaking overbrugd - DIA9029 */
    public static final int CH5833 = 5833;

    /** Stoorstroombewaking overbrugd - DIA902A */
    public static final int CH5834 = 5834;

    /** Richting 1 actief - DIA902B */
    public static final int CH5835 = 5835;

    /** Richting 2 actief - DIA902C */
    public static final int CH5836 = 5836;

    /** Rijremhendel stand rijden - DIA902D */
    public static final int CH5837 = 5837;

    /** Rijremhendel stand remmen - DIA902E */
    public static final int CH5838 = 5838;

    /** Rijremhendel stand snelremmen - DIA902F */
    public static final int CH5839 = 5839;

    /** Bedrijfsmode â€˜DOZEâ€˜ niet mogelijk - DIA9030 */
    public static final int CH5840 = 5840;

    /** Bedrijfsmode â€˜DOZEâ€˜ niet mogelijk - DIA9031 */
    public static final int CH5841 = 5841;

    /** Airco passagiersruimte is uitgeschakeld - DIA9032 */
    public static final int CH5842 = 5842;

    /** CCU-O 1 is Master - DIA9033 */
    public static final int CH5843 = 5843;

    /** CCU-O 2 is Master - DIA9034 */
    public static final int CH5844 = 5844;

    /** Bedrijfsurenteller Compressor - DIA9035 */
    public static final int CH5845 = 5845;

    /** Teller Compressor - DIA9036 */
    public static final int CH5846 = 5846;

    /** Urenteller Hulp-bedrives Compressor veranderd - DIA9037 */
    public static final int CH5847 = 5847;

    /** Teller Hulp-bedrives Compressor - DIA9038 */
    public static final int CH5848 = 5848;

    /** Kilometerteller - DIA9039 */
    public static final int CH5849 = 5849;

    /** Snelschakelaar - DIA9040 */
    public static final int CH5850 = 5850;

    /** Bedrijfskilometerteller Stroomafnemer 1 - DIA9041 */
    public static final int CH5851 = 5851;

    /** Bedrijfskilometerteller Stroomafnemer 1 - DIA9042 */
    public static final int CH5852 = 5852;

    /** Bedrijfskilometerteller Stroomafnemer 1 - DIA9043 */
    public static final int CH5853 = 5853;

    /** Bedrijfskilometerteller Stroomafnemer 2 - DIA9044 */
    public static final int CH5854 = 5854;

    /** Bedrijfskilometerteller Stroomafnemer 2 - DIA9045 */
    public static final int CH5855 = 5855;

    /** Bedrijfskilometerteller Stroomafnemer 2 - DIA9046 */
    public static final int CH5856 = 5856;

    /** Gateway 1 aansluiting A1 storing - DIA9058 */
    public static final int CH5857 = 5857;

    /** Gateway 1 aansluiting A2 storing - DIA9059 */
    public static final int CH5858 = 5858;

    /** Gateway 1 aansluiting B1 storing - DIA905A */
    public static final int CH5859 = 5859;

    /** Gateway 1 aansluiting B2 storing - DIA905B */
    public static final int CH5860 = 5860;

    /** Gateway 2 aansluiting A1 storing - DIA905C */
    public static final int CH5861 = 5861;

    /** Gateway 2 aansluiting A2 storing - DIA905D */
    public static final int CH5862 = 5862;

    /** Gateway 2 aansluiting B1 storing - DIA905E */
    public static final int CH5863 = 5863;

    /** Gateway 2 aansluiting B2 storing - DIA905F */
    public static final int CH5864 = 5864;

    /** CCU-O 1 aansluiting A is actief - DIA9060 */
    public static final int CH5865 = 5865;

    /** CCU-O 1 aansluiting B is actief - DIA9061 */
    public static final int CH5866 = 5866;

    /** CCU-O 2 aansluiting A is actief - DIA9062 */
    public static final int CH5867 = 5867;

    /** CCU-O 2 aansluiting B is actief - DIA9063 */
    public static final int CH5868 = 5868;

    /** Deur 1 storing - DRN0102 */
    public static final int CH5869 = 5869;

    /** Deur 1 storing - DRN0103 */
    public static final int CH5870 = 5870;

    /** Deur 1 storing - DRN0104 */
    public static final int CH5871 = 5871;

    /** Deur 1 storing - DRN0105 */
    public static final int CH5872 = 5872;

    /** Deur 1 storing - DRN0106 */
    public static final int CH5873 = 5873;

    /** Deur 1 storing - DRN0107 */
    public static final int CH5874 = 5874;

    /** Deur 1 noodontgrendeling binnen bediend - DRN0108 */
    public static final int CH5875 = 5875;

    /** Deur 1 schakelaar buitenbedrijfstelling bediend - DRN0109 */
    public static final int CH5876 = 5876;

    /** Deur 1 storing - DRN0110 */
    public static final int CH5877 = 5877;

    /** Deur 1 trede storing - DRN0111 */
    public static final int CH5878 = 5878;

    /** Deur 1 - DRN0112 */
    public static final int CH5879 = 5879;

    /** Deur 1 storing - DRN0113 */
    public static final int CH5880 = 5880;

    /** Deur 1 storing - DRN0114 */
    public static final int CH5881 = 5881;

    /** Deur 1 storing - DRN0117 */
    public static final int CH5882 = 5882;

    /** Deur 1 storing - DRN0119 */
    public static final int CH5883 = 5883;

    /** Deur 1 storing - DRN0120 */
    public static final int CH5884 = 5884;

    /** Deur 1 storing - DRN0121 */
    public static final int CH5885 = 5885;

    /** Deur 1 storing - DRN0122 */
    public static final int CH5886 = 5886;

    /** Deur 1 storing - DRN0123 */
    public static final int CH5887 = 5887;

    /** Deur 1 storing - DRN0124 */
    public static final int CH5888 = 5888;

    /** Deur 1 storing - DRN0125 */
    public static final int CH5889 = 5889;

    /** Deur 1 storing - DRN0126 */
    public static final int CH5890 = 5890;

    /** Deur 1 storing - DRN0127 */
    public static final int CH5891 = 5891;

    /** Deur 1 storing - DRN0128 */
    public static final int CH5892 = 5892;

    /** Deur 1 storing - DRN0129 */
    public static final int CH5893 = 5893;

    /** Deur 1 storing - DRN0130 */
    public static final int CH5894 = 5894;

    /** Deur 1 storing trede - DRN0132 */
    public static final int CH5895 = 5895;

    /** Deur 1 storing trede - DRN0133 */
    public static final int CH5896 = 5896;

    /** Deur 1 storing trede - DRN0134 */
    public static final int CH5897 = 5897;

    /** Deur 1 storing trede - DRN0135 */
    public static final int CH5898 = 5898;

    /** Deur 1 storing trede - DRN0136 */
    public static final int CH5899 = 5899;

    /** Deur 1 storing trede - DRN0137 */
    public static final int CH5900 = 5900;

    /** Deur 1 storing trede - DRN0138 */
    public static final int CH5901 = 5901;

    /** Deur 2 storing - DRN0202 */
    public static final int CH5902 = 5902;

    /** Deur 2 storing - DRN0203 */
    public static final int CH5903 = 5903;

    /** Deur 2 storing - DRN0204 */
    public static final int CH5904 = 5904;

    /** Deur 2 storing - DRN0205 */
    public static final int CH5905 = 5905;

    /** Deur 2 storing - DRN0206 */
    public static final int CH5906 = 5906;

    /** Deur 2 storing - DRN0207 */
    public static final int CH5907 = 5907;

    /** Deur 2 noodontgrendeling binnen bediend - DRN0208 */
    public static final int CH5908 = 5908;

    /** Deur 2 schakelaar buitenbedrijfstelling bediend - DRN0209 */
    public static final int CH5909 = 5909;

    /** Deur 2 storing - DRN0210 */
    public static final int CH5910 = 5910;

    /** Deur 2 trede storing - DRN0211 */
    public static final int CH5911 = 5911;

    /** Deur 2 - DRN0212 */
    public static final int CH5912 = 5912;

    /** Deur 2 storing - DRN0213 */
    public static final int CH5913 = 5913;

    /** Deur 2 storing - DRN0214 */
    public static final int CH5914 = 5914;

    /** Deur 2 storing - DRN0217 */
    public static final int CH5915 = 5915;

    /** Deur 2 storing - DRN0219 */
    public static final int CH5916 = 5916;

    /** Deur 2 storing - DRN0220 */
    public static final int CH5917 = 5917;

    /** Deur 2 storing - DRN0221 */
    public static final int CH5918 = 5918;

    /** Deur 2 storing - DRN0222 */
    public static final int CH5919 = 5919;

    /** Deur 2 storing - DRN0223 */
    public static final int CH5920 = 5920;

    /** Deur 2 storing - DRN0224 */
    public static final int CH5921 = 5921;

    /** Deur 2 storing - DRN0225 */
    public static final int CH5922 = 5922;

    /** Deur 2 storing - DRN0226 */
    public static final int CH5923 = 5923;

    /** Deur 2 storing - DRN0227 */
    public static final int CH5924 = 5924;

    /** Deur 2 storing - DRN0228 */
    public static final int CH5925 = 5925;

    /** Deur 2 storing - DRN0229 */
    public static final int CH5926 = 5926;

    /** Deur 2 storing - DRN0230 */
    public static final int CH5927 = 5927;

    /** Deur 2 storing trede - DRN0232 */
    public static final int CH5928 = 5928;

    /** Deur 2 storing trede - DRN0233 */
    public static final int CH5929 = 5929;

    /** Deur 2 storing trede - DRN0234 */
    public static final int CH5930 = 5930;

    /** Deur 2 storing trede - DRN0235 */
    public static final int CH5931 = 5931;

    /** Deur 2 storing trede - DRN0236 */
    public static final int CH5932 = 5932;

    /** Deur 2 storing trede - DRN0237 */
    public static final int CH5933 = 5933;

    /** Deur 2 storing trede - DRN0238 */
    public static final int CH5934 = 5934;

    /** Deur 3 storing - DRN0302 */
    public static final int CH5935 = 5935;

    /** Deur 3 storing - DRN0303 */
    public static final int CH5936 = 5936;

    /** Deur 3 storing - DRN0304 */
    public static final int CH5937 = 5937;

    /** Deur 3 storing - DRN0305 */
    public static final int CH5938 = 5938;

    /** Deur 3 storing - DRN0306 */
    public static final int CH5939 = 5939;

    /** Deur 3 storing - DRN0307 */
    public static final int CH5940 = 5940;

    /** Deur 3 noodontgrendeling binnen bediend - DRN0308 */
    public static final int CH5941 = 5941;

    /** Deur 3 schakelaar buitenbedrijfstelling bediend - DRN0309 */
    public static final int CH5942 = 5942;

    /** Deur 3 storing - DRN0310 */
    public static final int CH5943 = 5943;

    /** Deur 3 trede storing - DRN0311 */
    public static final int CH5944 = 5944;

    /** Deur 3 - DRN0312 */
    public static final int CH5945 = 5945;

    /** Deur 3 storing - DRN0313 */
    public static final int CH5946 = 5946;

    /** Deur 3 storing - DRN0314 */
    public static final int CH5947 = 5947;

    /** Deur 3 storing - DRN0317 */
    public static final int CH5948 = 5948;

    /** Deur 3 storing - DRN0319 */
    public static final int CH5949 = 5949;

    /** Deur 3 storing - DRN0320 */
    public static final int CH5950 = 5950;

    /** Deur 3 storing - DRN0321 */
    public static final int CH5951 = 5951;

    /** Deur 3 storing - DRN0322 */
    public static final int CH5952 = 5952;

    /** Deur 3 storing - DRN0323 */
    public static final int CH5953 = 5953;

    /** Deur 3 storing - DRN0324 */
    public static final int CH5954 = 5954;

    /** Deur 3 storing - DRN0325 */
    public static final int CH5955 = 5955;

    /** Deur 3 storing - DRN0326 */
    public static final int CH5956 = 5956;

    /** Deur 3 storing - DRN0327 */
    public static final int CH5957 = 5957;

    /** Deur 3 storing - DRN0328 */
    public static final int CH5958 = 5958;

    /** Deur 3 storing - DRN0329 */
    public static final int CH5959 = 5959;

    /** Deur 3 storing - DRN0330 */
    public static final int CH5960 = 5960;

    /** Deur 3 storing trede - DRN0332 */
    public static final int CH5961 = 5961;

    /** Deur 3 storing trede - DRN0333 */
    public static final int CH5962 = 5962;

    /** Deur 3 storing trede - DRN0334 */
    public static final int CH5963 = 5963;

    /** Deur 3 storing trede - DRN0335 */
    public static final int CH5964 = 5964;

    /** Deur 3 storing trede - DRN0336 */
    public static final int CH5965 = 5965;

    /** Deur 3 storing trede - DRN0337 */
    public static final int CH5966 = 5966;

    /** Deur 3 storing trede - DRN0338 */
    public static final int CH5967 = 5967;

    /** Deur 4 storing - DRN0402 */
    public static final int CH5968 = 5968;

    /** Deur 4 storing - DRN0403 */
    public static final int CH5969 = 5969;

    /** Deur 4 storing - DRN0404 */
    public static final int CH5970 = 5970;

    /** Deur 4 storing - DRN0405 */
    public static final int CH5971 = 5971;

    /** Deur 4 storing - DRN0406 */
    public static final int CH5972 = 5972;

    /** Deur 4 storing - DRN0407 */
    public static final int CH5973 = 5973;

    /** Deur 4 noodontgrendeling binnen bediend - DRN0408 */
    public static final int CH5974 = 5974;

    /** Deur 4 schakelaar buitenbedrijfstelling bediend - DRN0409 */
    public static final int CH5975 = 5975;

    /** Deur 4 storing - DRN0410 */
    public static final int CH5976 = 5976;

    /** Deur 4 trede storing - DRN0411 */
    public static final int CH5977 = 5977;

    /** Deur 4 - DRN0412 */
    public static final int CH5978 = 5978;

    /** Deur 4 storing - DRN0413 */
    public static final int CH5979 = 5979;

    /** Deur 4 storing - DRN0414 */
    public static final int CH5980 = 5980;

    /** Deur 4 storing - DRN0417 */
    public static final int CH5981 = 5981;

    /** Deur 4 storing - DRN0419 */
    public static final int CH5982 = 5982;

    /** Deur 4 storing - DRN0420 */
    public static final int CH5983 = 5983;

    /** Deur 4 storing - DRN0421 */
    public static final int CH5984 = 5984;

    /** Deur 4 storing - DRN0422 */
    public static final int CH5985 = 5985;

    /** Deur 4 storing - DRN0423 */
    public static final int CH5986 = 5986;

    /** Deur 4 storing - DRN0424 */
    public static final int CH5987 = 5987;

    /** Deur 4 storing - DRN0425 */
    public static final int CH5988 = 5988;

    /** Deur 4 storing - DRN0426 */
    public static final int CH5989 = 5989;

    /** Deur 4 storing - DRN0427 */
    public static final int CH5990 = 5990;

    /** Deur 4 storing - DRN0428 */
    public static final int CH5991 = 5991;

    /** Deur 4 storing - DRN0429 */
    public static final int CH5992 = 5992;

    /** Deur 4 storing - DRN0430 */
    public static final int CH5993 = 5993;

    /** Deur 4 storing trede - DRN0432 */
    public static final int CH5994 = 5994;

    /** Deur 4 storing trede - DRN0433 */
    public static final int CH5995 = 5995;

    /** Deur 4 storing trede - DRN0434 */
    public static final int CH5996 = 5996;

    /** Deur 4 storing trede - DRN0435 */
    public static final int CH5997 = 5997;

    /** Deur 4 storing trede - DRN0436 */
    public static final int CH5998 = 5998;

    /** Deur 4 storing trede - DRN0437 */
    public static final int CH5999 = 5999;

    /** Deur 4 storing trede - DRN0438 */
    public static final int CH6000 = 6000;

    /** Deur 5 storing - DRN0502 */
    public static final int CH6001 = 6001;

    /** Deur 5 storing - DRN0503 */
    public static final int CH6002 = 6002;

    /** Deur 5 storing - DRN0504 */
    public static final int CH6003 = 6003;

    /** Deur 5 storing - DRN0505 */
    public static final int CH6004 = 6004;

    /** Deur 5 storing - DRN0506 */
    public static final int CH6005 = 6005;

    /** Deur 5 storing - DRN0507 */
    public static final int CH6006 = 6006;

    /** Deur 5 noodontgrendeling binnen bediend - DRN0508 */
    public static final int CH6007 = 6007;

    /** Deur 5 schakelaar buitenbedrijfstelling bediend - DRN0509 */
    public static final int CH6008 = 6008;

    /** Deur 5 storing - DRN0510 */
    public static final int CH6009 = 6009;

    /** Deur 5 trede storing - DRN0511 */
    public static final int CH6010 = 6010;

    /** Deur 5 - DRN0512 */
    public static final int CH6011 = 6011;

    /** Deur 5 storing - DRN0513 */
    public static final int CH6012 = 6012;

    /** Deur 5 storing - DRN0514 */
    public static final int CH6013 = 6013;

    /** Deur 5 storing - DRN0517 */
    public static final int CH6014 = 6014;

    /** Deur 5 storing - DRN0519 */
    public static final int CH6015 = 6015;

    /** Deur 5 storing - DRN0520 */
    public static final int CH6016 = 6016;

    /** Deur 5 storing - DRN0521 */
    public static final int CH6017 = 6017;

    /** Deur 5 storing - DRN0522 */
    public static final int CH6018 = 6018;

    /** Deur 5 storing - DRN0523 */
    public static final int CH6019 = 6019;

    /** Deur 5 storing - DRN0524 */
    public static final int CH6020 = 6020;

    /** Deur 5 storing - DRN0525 */
    public static final int CH6021 = 6021;

    /** Deur 5 storing - DRN0526 */
    public static final int CH6022 = 6022;

    /** Deur 5 storing - DRN0527 */
    public static final int CH6023 = 6023;

    /** Deur 5 storing - DRN0528 */
    public static final int CH6024 = 6024;

    /** Deur 5 storing - DRN0529 */
    public static final int CH6025 = 6025;

    /** Deur 5 storing - DRN0530 */
    public static final int CH6026 = 6026;

    /** Deur 5 storing trede - DRN0532 */
    public static final int CH6027 = 6027;

    /** Deur 5 storing trede - DRN0533 */
    public static final int CH6028 = 6028;

    /** Deur 5 storing trede - DRN0534 */
    public static final int CH6029 = 6029;

    /** Deur 5 storing trede - DRN0535 */
    public static final int CH6030 = 6030;

    /** Deur 5 storing trede - DRN0536 */
    public static final int CH6031 = 6031;

    /** Deur 5 storing trede - DRN0537 */
    public static final int CH6032 = 6032;

    /** Deur 5 storing trede - DRN0538 */
    public static final int CH6033 = 6033;

    /** Deur 6 storing - DRN0602 */
    public static final int CH6034 = 6034;

    /** Deur 6 storing - DRN0603 */
    public static final int CH6035 = 6035;

    /** Deur 6 storing - DRN0604 */
    public static final int CH6036 = 6036;

    /** Deur 6 storing - DRN0605 */
    public static final int CH6037 = 6037;

    /** Deur 6 storing - DRN0606 */
    public static final int CH6038 = 6038;

    /** Deur 6 storing - DRN0607 */
    public static final int CH6039 = 6039;

    /** Deur 6 noodontgrendeling binnen bediend - DRN0608 */
    public static final int CH6040 = 6040;

    /** Deur 6 schakelaar buitenbedrijfstelling bediend - DRN0609 */
    public static final int CH6041 = 6041;

    /** Deur 6 storing - DRN0610 */
    public static final int CH6042 = 6042;

    /** Deur 6 trede storing - DRN0611 */
    public static final int CH6043 = 6043;

    /** Deur 6 - DRN0612 */
    public static final int CH6044 = 6044;

    /** Deur 6 storing - DRN0613 */
    public static final int CH6045 = 6045;

    /** Deur 6 storing - DRN0614 */
    public static final int CH6046 = 6046;

    /** Deur 6 storing - DRN0617 */
    public static final int CH6047 = 6047;

    /** Deur 6 storing - DRN0619 */
    public static final int CH6048 = 6048;

    /** Deur 6 storing - DRN0620 */
    public static final int CH6049 = 6049;

    /** Deur 6 storing - DRN0621 */
    public static final int CH6050 = 6050;

    /** Deur 6 storing - DRN0622 */
    public static final int CH6051 = 6051;

    /** Deur 6 storing - DRN0623 */
    public static final int CH6052 = 6052;

    /** Deur 6 storing - DRN0624 */
    public static final int CH6053 = 6053;

    /** Deur 6 storing - DRN0625 */
    public static final int CH6054 = 6054;

    /** Deur 6 storing - DRN0626 */
    public static final int CH6055 = 6055;

    /** Deur 6 storing - DRN0627 */
    public static final int CH6056 = 6056;

    /** Deur 6 storing - DRN0628 */
    public static final int CH6057 = 6057;

    /** Deur 6 storing - DRN0629 */
    public static final int CH6058 = 6058;

    /** Deur 6 storing - DRN0630 */
    public static final int CH6059 = 6059;

    /** Deur 6 storing trede - DRN0632 */
    public static final int CH6060 = 6060;

    /** Deur 6 storing trede - DRN0633 */
    public static final int CH6061 = 6061;

    /** Deur 6 storing trede - DRN0634 */
    public static final int CH6062 = 6062;

    /** Deur 6 storing trede - DRN0635 */
    public static final int CH6063 = 6063;

    /** Deur 6 storing trede - DRN0636 */
    public static final int CH6064 = 6064;

    /** Deur 6 storing trede - DRN0637 */
    public static final int CH6065 = 6065;

    /** Deur 6 storing trede - DRN0638 */
    public static final int CH6066 = 6066;

    /** Deur 7 storing - DRN0702 */
    public static final int CH6067 = 6067;

    /** Deur 7 storing - DRN0703 */
    public static final int CH6068 = 6068;

    /** Deur 7 storing - DRN0704 */
    public static final int CH6069 = 6069;

    /** Deur 7 storing - DRN0705 */
    public static final int CH6070 = 6070;

    /** Deur 7 storing - DRN0706 */
    public static final int CH6071 = 6071;

    /** Deur 7 storing - DRN0707 */
    public static final int CH6072 = 6072;

    /** Deur 7 noodontgrendeling binnen bediend - DRN0708 */
    public static final int CH6073 = 6073;

    /** Deur 7 schakelaar buitenbedrijfstelling bediend - DRN0709 */
    public static final int CH6074 = 6074;

    /** Deur 7 storing - DRN0710 */
    public static final int CH6075 = 6075;

    /** Deur 7 trede storing - DRN0711 */
    public static final int CH6076 = 6076;

    /** Deur 7 - DRN0712 */
    public static final int CH6077 = 6077;

    /** Deur 7 storing - DRN0713 */
    public static final int CH6078 = 6078;

    /** Deur 7 storing - DRN0714 */
    public static final int CH6079 = 6079;

    /** Deur 7 storing - DRN0717 */
    public static final int CH6080 = 6080;

    /** Deur 7 storing - DRN0719 */
    public static final int CH6081 = 6081;

    /** Deur 7 storing - DRN0720 */
    public static final int CH6082 = 6082;

    /** Deur 7 storing - DRN0721 */
    public static final int CH6083 = 6083;

    /** Deur 7 storing - DRN0722 */
    public static final int CH6084 = 6084;

    /** Deur 7 storing - DRN0723 */
    public static final int CH6085 = 6085;

    /** Deur 7 storing - DRN0724 */
    public static final int CH6086 = 6086;

    /** Deur 7 storing - DRN0725 */
    public static final int CH6087 = 6087;

    /** Deur 7 storing - DRN0726 */
    public static final int CH6088 = 6088;

    /** Deur 7 storing - DRN0727 */
    public static final int CH6089 = 6089;

    /** Deur 7 storing - DRN0728 */
    public static final int CH6090 = 6090;

    /** Deur 7 storing - DRN0729 */
    public static final int CH6091 = 6091;

    /** Deur 7 storing - DRN0730 */
    public static final int CH6092 = 6092;

    /** Deur 7 storing trede - DRN0732 */
    public static final int CH6093 = 6093;

    /** Deur 7 storing trede - DRN0733 */
    public static final int CH6094 = 6094;

    /** Deur 7 storing trede - DRN0734 */
    public static final int CH6095 = 6095;

    /** Deur 7 storing trede - DRN0735 */
    public static final int CH6096 = 6096;

    /** Deur 7 storing trede - DRN0736 */
    public static final int CH6097 = 6097;

    /** Deur 7 storing trede - DRN0737 */
    public static final int CH6098 = 6098;

    /** Deur 7 storing trede - DRN0738 */
    public static final int CH6099 = 6099;

    /** Deur 8 storing - DRN0802 */
    public static final int CH6100 = 6100;

    /** Deur 8 storing - DRN0803 */
    public static final int CH6101 = 6101;

    /** Deur 8 storing - DRN0804 */
    public static final int CH6102 = 6102;

    /** Deur 8 storing - DRN0805 */
    public static final int CH6103 = 6103;

    /** Deur 8 storing - DRN0806 */
    public static final int CH6104 = 6104;

    /** Deur 8 storing - DRN0807 */
    public static final int CH6105 = 6105;

    /** Deur 8 noodontgrendeling binnen bediend - DRN0808 */
    public static final int CH6106 = 6106;

    /** Deur 8 schakelaar buitenbedrijfstelling bediend - DRN0809 */
    public static final int CH6107 = 6107;

    /** Deur 8 storing - DRN0810 */
    public static final int CH6108 = 6108;

    /** Deur 8 trede storing - DRN0811 */
    public static final int CH6109 = 6109;

    /** Deur 8 - DRN0812 */
    public static final int CH6110 = 6110;

    /** Deur 8 storing - DRN0813 */
    public static final int CH6111 = 6111;

    /** Deur 8 storing - DRN0814 */
    public static final int CH6112 = 6112;

    /** Deur 8 storing - DRN0817 */
    public static final int CH6113 = 6113;

    /** Deur 8 storing - DRN0819 */
    public static final int CH6114 = 6114;

    /** Deur 8 storing - DRN0820 */
    public static final int CH6115 = 6115;

    /** Deur 8 storing - DRN0821 */
    public static final int CH6116 = 6116;

    /** Deur 8 storing - DRN0822 */
    public static final int CH6117 = 6117;

    /** Deur 8 storing - DRN0823 */
    public static final int CH6118 = 6118;

    /** Deur 8 storing - DRN0824 */
    public static final int CH6119 = 6119;

    /** Deur 8 storing - DRN0825 */
    public static final int CH6120 = 6120;

    /** Deur 8 storing - DRN0826 */
    public static final int CH6121 = 6121;

    /** Deur 8 storing - DRN0827 */
    public static final int CH6122 = 6122;

    /** Deur 8 storing - DRN0828 */
    public static final int CH6123 = 6123;

    /** Deur 8 storing - DRN0829 */
    public static final int CH6124 = 6124;

    /** Deur 8 storing - DRN0830 */
    public static final int CH6125 = 6125;

    /** Deur 8 storing trede - DRN0832 */
    public static final int CH6126 = 6126;

    /** Deur 8 storing trede - DRN0833 */
    public static final int CH6127 = 6127;

    /** Deur 8 storing trede - DRN0834 */
    public static final int CH6128 = 6128;

    /** Deur 8 storing trede - DRN0835 */
    public static final int CH6129 = 6129;

    /** Deur 8 storing trede - DRN0836 */
    public static final int CH6130 = 6130;

    /** Deur 8 storing trede - DRN0837 */
    public static final int CH6131 = 6131;

    /** Deur 8 storing trede - DRN0838 */
    public static final int CH6132 = 6132;

    /** Deur 9 storing - DRN0902 */
    public static final int CH6133 = 6133;

    /** Deur 9 storing - DRN0903 */
    public static final int CH6134 = 6134;

    /** Deur 9 storing - DRN0904 */
    public static final int CH6135 = 6135;

    /** Deur 9 storing - DRN0905 */
    public static final int CH6136 = 6136;

    /** Deur 9 storing - DRN0906 */
    public static final int CH6137 = 6137;

    /** Deur 9 storing - DRN0907 */
    public static final int CH6138 = 6138;

    /** Deur 9 noodontgrendeling binnen bediend - DRN0908 */
    public static final int CH6139 = 6139;

    /** Deur 9 schakelaar buitenbedrijfstelling bediend - DRN0909 */
    public static final int CH6140 = 6140;

    /** Deur 9 storing - DRN0910 */
    public static final int CH6141 = 6141;

    /** Deur 9 trede storing - DRN0911 */
    public static final int CH6142 = 6142;

    /** Deur 9 - DRN0912 */
    public static final int CH6143 = 6143;

    /** Deur 9 storing - DRN0913 */
    public static final int CH6144 = 6144;

    /** Deur 9 storing - DRN0914 */
    public static final int CH6145 = 6145;

    /** Deur 9 storing - DRN0917 */
    public static final int CH6146 = 6146;

    /** Deur 9 storing - DRN0919 */
    public static final int CH6147 = 6147;

    /** Deur 9 storing - DRN0920 */
    public static final int CH6148 = 6148;

    /** Deur 9 storing - DRN0921 */
    public static final int CH6149 = 6149;

    /** Deur 9 storing - DRN0922 */
    public static final int CH6150 = 6150;

    /** Deur 9 storing - DRN0923 */
    public static final int CH6151 = 6151;

    /** Deur 9 storing - DRN0924 */
    public static final int CH6152 = 6152;

    /** Deur 9 storing - DRN0925 */
    public static final int CH6153 = 6153;

    /** Deur 9 storing - DRN0926 */
    public static final int CH6154 = 6154;

    /** Deur 9 storing - DRN0927 */
    public static final int CH6155 = 6155;

    /** Deur 9 storing - DRN0928 */
    public static final int CH6156 = 6156;

    /** Deur 9 storing - DRN0929 */
    public static final int CH6157 = 6157;

    /** Deur 9 storing - DRN0930 */
    public static final int CH6158 = 6158;

    /** Deur 9 storing trede - DRN0932 */
    public static final int CH6159 = 6159;

    /** Deur 9 storing trede - DRN0933 */
    public static final int CH6160 = 6160;

    /** Deur 9 storing trede - DRN0934 */
    public static final int CH6161 = 6161;

    /** Deur 9 storing trede - DRN0935 */
    public static final int CH6162 = 6162;

    /** Deur 9 storing trede - DRN0936 */
    public static final int CH6163 = 6163;

    /** Deur 9 storing trede - DRN0937 */
    public static final int CH6164 = 6164;

    /** Deur 9 storing trede - DRN0938 */
    public static final int CH6165 = 6165;

    /** Deur 10 storing - DRN1002 */
    public static final int CH6166 = 6166;

    /** Deur 10 storing - DRN1003 */
    public static final int CH6167 = 6167;

    /** Deur 10 storing - DRN1004 */
    public static final int CH6168 = 6168;

    /** Deur 10 storing - DRN1005 */
    public static final int CH6169 = 6169;

    /** Deur 10 storing - DRN1006 */
    public static final int CH6170 = 6170;

    /** Deur 10 storing - DRN1007 */
    public static final int CH6171 = 6171;

    /** Deur 10 noodontgrendeling binnen bediend - DRN1008 */
    public static final int CH6172 = 6172;

    /** Deur 10 schakelaar buitenbedrijfstelling bediend - DRN1009 */
    public static final int CH6173 = 6173;

    /** Deur 10 storing - DRN1010 */
    public static final int CH6174 = 6174;

    /** Deur 10 trede storing - DRN1011 */
    public static final int CH6175 = 6175;

    /** Deur 10 - DRN1012 */
    public static final int CH6176 = 6176;

    /** Deur 10 storing - DRN1013 */
    public static final int CH6177 = 6177;

    /** Deur 10 storing - DRN1014 */
    public static final int CH6178 = 6178;

    /** Deur 10 storing - DRN1017 */
    public static final int CH6179 = 6179;

    /** Deur 10 storing - DRN1019 */
    public static final int CH6180 = 6180;

    /** Deur 10 storing - DRN1020 */
    public static final int CH6181 = 6181;

    /** Deur 10 storing - DRN1021 */
    public static final int CH6182 = 6182;

    /** Deur 10 storing - DRN1022 */
    public static final int CH6183 = 6183;

    /** Deur 10 storing - DRN1023 */
    public static final int CH6184 = 6184;

    /** Deur 10 storing - DRN1024 */
    public static final int CH6185 = 6185;

    /** Deur 10 storing - DRN1025 */
    public static final int CH6186 = 6186;

    /** Deur 10 storing - DRN1026 */
    public static final int CH6187 = 6187;

    /** Deur 10 storing - DRN1027 */
    public static final int CH6188 = 6188;

    /** Deur 10 storing - DRN1028 */
    public static final int CH6189 = 6189;

    /** Deur 10 storing - DRN1029 */
    public static final int CH6190 = 6190;

    /** Deur 10 storing - DRN1030 */
    public static final int CH6191 = 6191;

    /** Deur 10 storing trede - DRN1032 */
    public static final int CH6192 = 6192;

    /** Deur 10 storing trede - DRN1033 */
    public static final int CH6193 = 6193;

    /** Deur 10 storing trede - DRN1034 */
    public static final int CH6194 = 6194;

    /** Deur 10 storing trede - DRN1035 */
    public static final int CH6195 = 6195;

    /** Deur 10 storing trede - DRN1036 */
    public static final int CH6196 = 6196;

    /** Deur 10 storing trede - DRN1037 */
    public static final int CH6197 = 6197;

    /** Deur 10 storing trede - DRN1038 */
    public static final int CH6198 = 6198;

    /** Deur 11 storing - DRN1102 */
    public static final int CH6199 = 6199;

    /** Deur 11 storing - DRN1103 */
    public static final int CH6200 = 6200;

    /** Deur 11 storing - DRN1104 */
    public static final int CH6201 = 6201;

    /** Deur 11 storing - DRN1105 */
    public static final int CH6202 = 6202;

    /** Deur 11 storing - DRN1106 */
    public static final int CH6203 = 6203;

    /** Deur 11 storing - DRN1107 */
    public static final int CH6204 = 6204;

    /** Deur 11 noodontgrendeling binnen bediend - DRN1108 */
    public static final int CH6205 = 6205;

    /** Deur 11 schakelaar buitenbedrijfstelling bediend - DRN1109 */
    public static final int CH6206 = 6206;

    /** Deur 11 storing - DRN1110 */
    public static final int CH6207 = 6207;

    /** Deur 11 trede storing - DRN1111 */
    public static final int CH6208 = 6208;

    /** Deur 11 - DRN1112 */
    public static final int CH6209 = 6209;

    /** Deur 11 storing - DRN1113 */
    public static final int CH6210 = 6210;

    /** Deur 11 storing - DRN1114 */
    public static final int CH6211 = 6211;

    /** Deur 11 storing - DRN1117 */
    public static final int CH6212 = 6212;

    /** Deur 11 storing - DRN1119 */
    public static final int CH6213 = 6213;

    /** Deur 11 storing - DRN1120 */
    public static final int CH6214 = 6214;

    /** Deur 11 storing - DRN1121 */
    public static final int CH6215 = 6215;

    /** Deur 11 storing - DRN1122 */
    public static final int CH6216 = 6216;

    /** Deur 11 storing - DRN1123 */
    public static final int CH6217 = 6217;

    /** Deur 11 storing - DRN1124 */
    public static final int CH6218 = 6218;

    /** Deur 11 storing - DRN1125 */
    public static final int CH6219 = 6219;

    /** Deur 11 storing - DRN1126 */
    public static final int CH6220 = 6220;

    /** Deur 11 storing - DRN1127 */
    public static final int CH6221 = 6221;

    /** Deur 11 storing - DRN1128 */
    public static final int CH6222 = 6222;

    /** Deur 11 storing - DRN1129 */
    public static final int CH6223 = 6223;

    /** Deur 11 storing - DRN1130 */
    public static final int CH6224 = 6224;

    /** Deur 11 storing trede - DRN1132 */
    public static final int CH6225 = 6225;

    /** Deur 11 storing trede - DRN1133 */
    public static final int CH6226 = 6226;

    /** Deur 11 storing trede - DRN1134 */
    public static final int CH6227 = 6227;

    /** Deur 11 storing trede - DRN1135 */
    public static final int CH6228 = 6228;

    /** Deur 11 storing trede - DRN1136 */
    public static final int CH6229 = 6229;

    /** Deur 11 storing trede - DRN1137 */
    public static final int CH6230 = 6230;

    /** Deur 11 storing trede - DRN1138 */
    public static final int CH6231 = 6231;

    /** Deur 12 storing - DRN1202 */
    public static final int CH6232 = 6232;

    /** Deur 12 storing - DRN1203 */
    public static final int CH6233 = 6233;

    /** Deur 12 storing - DRN1204 */
    public static final int CH6234 = 6234;

    /** Deur 12 storing - DRN1205 */
    public static final int CH6235 = 6235;

    /** Deur 12 storing - DRN1206 */
    public static final int CH6236 = 6236;

    /** Deur 12 storing - DRN1207 */
    public static final int CH6237 = 6237;

    /** Deur 12 noodontgrendeling binnen bediend - DRN1208 */
    public static final int CH6238 = 6238;

    /** Deur 12 schakelaar buitenbedrijfstelling bediend - DRN1209 */
    public static final int CH6239 = 6239;

    /** Deur 12 storing - DRN1210 */
    public static final int CH6240 = 6240;

    /** Deur 12 trede storing - DRN1211 */
    public static final int CH6241 = 6241;

    /** Deur 12 - DRN1212 */
    public static final int CH6242 = 6242;

    /** Deur 12 storing - DRN1213 */
    public static final int CH6243 = 6243;

    /** Deur 12 storing - DRN1214 */
    public static final int CH6244 = 6244;

    /** Deur 12 storing - DRN1217 */
    public static final int CH6245 = 6245;

    /** Deur 12 storing - DRN1219 */
    public static final int CH6246 = 6246;

    /** Deur 12 storing - DRN1220 */
    public static final int CH6247 = 6247;

    /** Deur 12 storing - DRN1221 */
    public static final int CH6248 = 6248;

    /** Deur 12 storing - DRN1222 */
    public static final int CH6249 = 6249;

    /** Deur 12 storing - DRN1223 */
    public static final int CH6250 = 6250;

    /** Deur 12 storing - DRN1224 */
    public static final int CH6251 = 6251;

    /** Deur 12 storing - DRN1225 */
    public static final int CH6252 = 6252;

    /** Deur 12 storing - DRN1226 */
    public static final int CH6253 = 6253;

    /** Deur 12 storing - DRN1227 */
    public static final int CH6254 = 6254;

    /** Deur 12 storing - DRN1228 */
    public static final int CH6255 = 6255;

    /** Deur 12 storing - DRN1229 */
    public static final int CH6256 = 6256;

    /** Deur 12 storing - DRN1230 */
    public static final int CH6257 = 6257;

    /** Deur 12 storing trede - DRN1232 */
    public static final int CH6258 = 6258;

    /** Deur 12 storing trede - DRN1233 */
    public static final int CH6259 = 6259;

    /** Deur 12 storing trede - DRN1234 */
    public static final int CH6260 = 6260;

    /** Deur 12 storing trede - DRN1235 */
    public static final int CH6261 = 6261;

    /** Deur 12 storing trede - DRN1236 */
    public static final int CH6262 = 6262;

    /** Deur 12 storing trede - DRN1237 */
    public static final int CH6263 = 6263;

    /** Deur 12 storing trede - DRN1238 */
    public static final int CH6264 = 6264;

    /** Deur 13 storing - DRN1302 */
    public static final int CH6265 = 6265;

    /** Deur 13 storing - DRN1303 */
    public static final int CH6266 = 6266;

    /** Deur 13 storing - DRN1304 */
    public static final int CH6267 = 6267;

    /** Deur 13 storing - DRN1305 */
    public static final int CH6268 = 6268;

    /** Deur 13 storing - DRN1306 */
    public static final int CH6269 = 6269;

    /** Deur 13 storing - DRN1307 */
    public static final int CH6270 = 6270;

    /** Deur 13 noodontgrendeling binnen bediend - DRN1308 */
    public static final int CH6271 = 6271;

    /** Deur 13 schakelaar buitenbedrijfstelling bediend - DRN1309 */
    public static final int CH6272 = 6272;

    /** Deur 13 storing - DRN1310 */
    public static final int CH6273 = 6273;

    /** Deur 13 trede storing - DRN1311 */
    public static final int CH6274 = 6274;

    /** Deur 13 - DRN1312 */
    public static final int CH6275 = 6275;

    /** Deur 13 storing - DRN1313 */
    public static final int CH6276 = 6276;

    /** Deur 13 storing - DRN1314 */
    public static final int CH6277 = 6277;

    /** Deur 13 storing - DRN1317 */
    public static final int CH6278 = 6278;

    /** Deur 13 storing - DRN1319 */
    public static final int CH6279 = 6279;

    /** Deur 13 storing - DRN1320 */
    public static final int CH6280 = 6280;

    /** Deur 13 storing - DRN1321 */
    public static final int CH6281 = 6281;

    /** Deur 13 storing - DRN1322 */
    public static final int CH6282 = 6282;

    /** Deur 13 storing - DRN1323 */
    public static final int CH6283 = 6283;

    /** Deur 13 storing - DRN1324 */
    public static final int CH6284 = 6284;

    /** Deur 13 storing - DRN1325 */
    public static final int CH6285 = 6285;

    /** Deur 13 storing - DRN1326 */
    public static final int CH6286 = 6286;

    /** Deur 13 storing - DRN1327 */
    public static final int CH6287 = 6287;

    /** Deur 13 storing - DRN1328 */
    public static final int CH6288 = 6288;

    /** Deur 13 storing - DRN1329 */
    public static final int CH6289 = 6289;

    /** Deur 13 storing - DRN1330 */
    public static final int CH6290 = 6290;

    /** Deur 13 storing trede - DRN1332 */
    public static final int CH6291 = 6291;

    /** Deur 13 storing trede - DRN1333 */
    public static final int CH6292 = 6292;

    /** Deur 13 storing trede - DRN1334 */
    public static final int CH6293 = 6293;

    /** Deur 13 storing trede - DRN1335 */
    public static final int CH6294 = 6294;

    /** Deur 13 storing trede - DRN1336 */
    public static final int CH6295 = 6295;

    /** Deur 13 storing trede - DRN1337 */
    public static final int CH6296 = 6296;

    /** Deur 13 storing trede - DRN1338 */
    public static final int CH6297 = 6297;

    /** Deur 14 storing - DRN1402 */
    public static final int CH6298 = 6298;

    /** Deur 14 storing - DRN1403 */
    public static final int CH6299 = 6299;

    /** Deur 14 storing - DRN1404 */
    public static final int CH6300 = 6300;

    /** Deur 14 storing - DRN1405 */
    public static final int CH6301 = 6301;

    /** Deur 14 storing - DRN1406 */
    public static final int CH6302 = 6302;

    /** Deur 14 storing - DRN1407 */
    public static final int CH6303 = 6303;

    /** Deur 14 noodontgrendeling binnen bediend - DRN1408 */
    public static final int CH6304 = 6304;

    /** Deur 14 schakelaar buitenbedrijfstelling bediend - DRN1409 */
    public static final int CH6305 = 6305;

    /** Deur 14 storing - DRN1410 */
    public static final int CH6306 = 6306;

    /** Deur 14 trede storing - DRN1411 */
    public static final int CH6307 = 6307;

    /** Deur 14 - DRN1412 */
    public static final int CH6308 = 6308;

    /** Deur 14 storing - DRN1413 */
    public static final int CH6309 = 6309;

    /** Deur 14 storing - DRN1414 */
    public static final int CH6310 = 6310;

    /** Deur 14 storing - DRN1417 */
    public static final int CH6311 = 6311;

    /** Deur 14 storing - DRN1419 */
    public static final int CH6312 = 6312;

    /** Deur 14 storing - DRN1420 */
    public static final int CH6313 = 6313;

    /** Deur 14 storing - DRN1421 */
    public static final int CH6314 = 6314;

    /** Deur 14 storing - DRN1422 */
    public static final int CH6315 = 6315;

    /** Deur 14 storing - DRN1423 */
    public static final int CH6316 = 6316;

    /** Deur 14 storing - DRN1424 */
    public static final int CH6317 = 6317;

    /** Deur 14 storing - DRN1425 */
    public static final int CH6318 = 6318;

    /** Deur 14 storing - DRN1426 */
    public static final int CH6319 = 6319;

    /** Deur 14 storing - DRN1427 */
    public static final int CH6320 = 6320;

    /** Deur 14 storing - DRN1428 */
    public static final int CH6321 = 6321;

    /** Deur 14 storing - DRN1429 */
    public static final int CH6322 = 6322;

    /** Deur 14 storing - DRN1430 */
    public static final int CH6323 = 6323;

    /** Deur 14 storing trede - DRN1432 */
    public static final int CH6324 = 6324;

    /** Deur 14 storing trede - DRN1433 */
    public static final int CH6325 = 6325;

    /** Deur 14 storing trede - DRN1434 */
    public static final int CH6326 = 6326;

    /** Deur 14 storing trede - DRN1435 */
    public static final int CH6327 = 6327;

    /** Deur 14 storing trede - DRN1436 */
    public static final int CH6328 = 6328;

    /** Deur 14 storing trede - DRN1437 */
    public static final int CH6329 = 6329;

    /** Deur 14 storing trede - DRN1438 */
    public static final int CH6330 = 6330;

    /** Deur 15 storing - DRN1502 */
    public static final int CH6331 = 6331;

    /** Deur 15 storing - DRN1503 */
    public static final int CH6332 = 6332;

    /** Deur 15 storing - DRN1504 */
    public static final int CH6333 = 6333;

    /** Deur 15 storing - DRN1505 */
    public static final int CH6334 = 6334;

    /** Deur 15 storing - DRN1506 */
    public static final int CH6335 = 6335;

    /** Deur 15 storing - DRN1507 */
    public static final int CH6336 = 6336;

    /** Deur 15 noodontgrendeling binnen bediend - DRN1508 */
    public static final int CH6337 = 6337;

    /** Deur 15 schakelaar buitenbedrijfstelling bediend - DRN1509 */
    public static final int CH6338 = 6338;

    /** Deur 15 storing - DRN1510 */
    public static final int CH6339 = 6339;

    /** Deur 15 trede storing - DRN1511 */
    public static final int CH6340 = 6340;

    /** Deur 15 - DRN1512 */
    public static final int CH6341 = 6341;

    /** Deur 15 storing - DRN1513 */
    public static final int CH6342 = 6342;

    /** Deur 15 storing - DRN1514 */
    public static final int CH6343 = 6343;

    /** Deur 15 storing - DRN1517 */
    public static final int CH6344 = 6344;

    /** Deur 15 storing - DRN1519 */
    public static final int CH6345 = 6345;

    /** Deur 15 storing - DRN1520 */
    public static final int CH6346 = 6346;

    /** Deur 15 storing - DRN1521 */
    public static final int CH6347 = 6347;

    /** Deur 15 storing - DRN1522 */
    public static final int CH6348 = 6348;

    /** Deur 15 storing - DRN1523 */
    public static final int CH6349 = 6349;

    /** Deur 15 storing - DRN1524 */
    public static final int CH6350 = 6350;

    /** Deur 15 storing - DRN1525 */
    public static final int CH6351 = 6351;

    /** Deur 15 storing - DRN1526 */
    public static final int CH6352 = 6352;

    /** Deur 15 storing - DRN1527 */
    public static final int CH6353 = 6353;

    /** Deur 15 storing - DRN1528 */
    public static final int CH6354 = 6354;

    /** Deur 15 storing - DRN1529 */
    public static final int CH6355 = 6355;

    /** Deur 15 storing - DRN1530 */
    public static final int CH6356 = 6356;

    /** Deur 15 storing trede - DRN1532 */
    public static final int CH6357 = 6357;

    /** Deur 15 storing trede - DRN1533 */
    public static final int CH6358 = 6358;

    /** Deur 15 storing trede - DRN1534 */
    public static final int CH6359 = 6359;

    /** Deur 15 storing trede - DRN1535 */
    public static final int CH6360 = 6360;

    /** Deur 15 storing trede - DRN1536 */
    public static final int CH6361 = 6361;

    /** Deur 15 storing trede - DRN1537 */
    public static final int CH6362 = 6362;

    /** Deur 15 storing trede - DRN1538 */
    public static final int CH6363 = 6363;

    /** Deur 16 storing - DRN1602 */
    public static final int CH6364 = 6364;

    /** Deur 16 storing - DRN1603 */
    public static final int CH6365 = 6365;

    /** Deur 16 storing - DRN1604 */
    public static final int CH6366 = 6366;

    /** Deur 16 storing - DRN1605 */
    public static final int CH6367 = 6367;

    /** Deur 16 storing - DRN1606 */
    public static final int CH6368 = 6368;

    /** Deur 16 storing - DRN1607 */
    public static final int CH6369 = 6369;

    /** Deur 16 noodontgrendeling binnen bediend - DRN1608 */
    public static final int CH6370 = 6370;

    /** Deur 16 schakelaar buitenbedrijfstelling bediend - DRN1609 */
    public static final int CH6371 = 6371;

    /** Deur 16 storing - DRN1610 */
    public static final int CH6372 = 6372;

    /** Deur 16 trede storing - DRN1611 */
    public static final int CH6373 = 6373;

    /** Deur 16 - DRN1612 */
    public static final int CH6374 = 6374;

    /** Deur 16 storing - DRN1613 */
    public static final int CH6375 = 6375;

    /** Deur 16 storing - DRN1614 */
    public static final int CH6376 = 6376;

    /** Deur 16 storing - DRN1617 */
    public static final int CH6377 = 6377;

    /** Deur 16 storing - DRN1619 */
    public static final int CH6378 = 6378;

    /** Deur 16 storing - DRN1620 */
    public static final int CH6379 = 6379;

    /** Deur 16 storing - DRN1621 */
    public static final int CH6380 = 6380;

    /** Deur 16 storing - DRN1622 */
    public static final int CH6381 = 6381;

    /** Deur 16 storing - DRN1623 */
    public static final int CH6382 = 6382;

    /** Deur 16 storing - DRN1624 */
    public static final int CH6383 = 6383;

    /** Deur 16 storing - DRN1625 */
    public static final int CH6384 = 6384;

    /** Deur 16 storing - DRN1626 */
    public static final int CH6385 = 6385;

    /** Deur 16 storing - DRN1627 */
    public static final int CH6386 = 6386;

    /** Deur 16 storing - DRN1628 */
    public static final int CH6387 = 6387;

    /** Deur 16 storing - DRN1629 */
    public static final int CH6388 = 6388;

    /** Deur 16 storing - DRN1630 */
    public static final int CH6389 = 6389;

    /** Deur 16 storing trede - DRN1632 */
    public static final int CH6390 = 6390;

    /** Deur 16 storing trede - DRN1633 */
    public static final int CH6391 = 6391;

    /** Deur 16 storing trede - DRN1634 */
    public static final int CH6392 = 6392;

    /** Deur 16 storing trede - DRN1635 */
    public static final int CH6393 = 6393;

    /** Deur 16 storing trede - DRN1636 */
    public static final int CH6394 = 6394;

    /** Deur 16 storing trede - DRN1637 */
    public static final int CH6395 = 6395;

    /** Deur 16 storing trede - DRN1638 */
    public static final int CH6396 = 6396;

    /** Deur 17 storing - DRN1702 */
    public static final int CH6397 = 6397;

    /** Deur 17 storing - DRN1703 */
    public static final int CH6398 = 6398;

    /** Deur 17 storing - DRN1704 */
    public static final int CH6399 = 6399;

    /** Deur 17 storing - DRN1705 */
    public static final int CH6400 = 6400;

    /** Deur 17 storing - DRN1706 */
    public static final int CH6401 = 6401;

    /** Deur 17 storing - DRN1707 */
    public static final int CH6402 = 6402;

    /** Deur 17 noodontgrendeling binnen bediend - DRN1708 */
    public static final int CH6403 = 6403;

    /** Deur 17 schakelaar buitenbedrijfstelling bediend - DRN1709 */
    public static final int CH6404 = 6404;

    /** Deur 17 storing - DRN1710 */
    public static final int CH6405 = 6405;

    /** Deur 17 trede storing - DRN1711 */
    public static final int CH6406 = 6406;

    /** Deur 17 - DRN1712 */
    public static final int CH6407 = 6407;

    /** Deur 17 storing - DRN1713 */
    public static final int CH6408 = 6408;

    /** Deur 17 storing - DRN1714 */
    public static final int CH6409 = 6409;

    /** Deur 17 storing - DRN1717 */
    public static final int CH6410 = 6410;

    /** Deur 17 storing - DRN1719 */
    public static final int CH6411 = 6411;

    /** Deur 17 storing - DRN1720 */
    public static final int CH6412 = 6412;

    /** Deur 17 storing - DRN1721 */
    public static final int CH6413 = 6413;

    /** Deur 17 storing - DRN1722 */
    public static final int CH6414 = 6414;

    /** Deur 17 storing - DRN1723 */
    public static final int CH6415 = 6415;

    /** Deur 17 storing - DRN1724 */
    public static final int CH6416 = 6416;

    /** Deur 17 storing - DRN1725 */
    public static final int CH6417 = 6417;

    /** Deur 17 storing - DRN1726 */
    public static final int CH6418 = 6418;

    /** Deur 17 storing - DRN1727 */
    public static final int CH6419 = 6419;

    /** Deur 17 storing - DRN1728 */
    public static final int CH6420 = 6420;

    /** Deur 17 storing - DRN1729 */
    public static final int CH6421 = 6421;

    /** Deur 17 storing - DRN1730 */
    public static final int CH6422 = 6422;

    /** Deur 17 storing trede - DRN1732 */
    public static final int CH6423 = 6423;

    /** Deur 17 storing trede - DRN1733 */
    public static final int CH6424 = 6424;

    /** Deur 17 storing trede - DRN1734 */
    public static final int CH6425 = 6425;

    /** Deur 17 storing trede - DRN1735 */
    public static final int CH6426 = 6426;

    /** Deur 17 storing trede - DRN1736 */
    public static final int CH6427 = 6427;

    /** Deur 17 storing trede - DRN1737 */
    public static final int CH6428 = 6428;

    /** Deur 17 storing trede - DRN1738 */
    public static final int CH6429 = 6429;

    /** Deur 18 storing - DRN1802 */
    public static final int CH6430 = 6430;

    /** Deur 18 storing - DRN1803 */
    public static final int CH6431 = 6431;

    /** Deur 18 storing - DRN1804 */
    public static final int CH6432 = 6432;

    /** Deur 18 storing - DRN1805 */
    public static final int CH6433 = 6433;

    /** Deur 18 storing - DRN1806 */
    public static final int CH6434 = 6434;

    /** Deur 18 storing - DRN1807 */
    public static final int CH6435 = 6435;

    /** Deur 18 noodontgrendeling binnen bediend - DRN1808 */
    public static final int CH6436 = 6436;

    /** Deur 18 schakelaar buitenbedrijfstelling bediend - DRN1809 */
    public static final int CH6437 = 6437;

    /** Deur 18 storing - DRN1810 */
    public static final int CH6438 = 6438;

    /** Deur 18 trede storing - DRN1811 */
    public static final int CH6439 = 6439;

    /** Deur 18 - DRN1812 */
    public static final int CH6440 = 6440;

    /** Deur 18 storing - DRN1813 */
    public static final int CH6441 = 6441;

    /** Deur 18 storing - DRN1814 */
    public static final int CH6442 = 6442;

    /** Deur 18 storing - DRN1817 */
    public static final int CH6443 = 6443;

    /** Deur 18 storing - DRN1819 */
    public static final int CH6444 = 6444;

    /** Deur 18 storing - DRN1820 */
    public static final int CH6445 = 6445;

    /** Deur 18 storing - DRN1821 */
    public static final int CH6446 = 6446;

    /** Deur 18 storing - DRN1822 */
    public static final int CH6447 = 6447;

    /** Deur 18 storing - DRN1823 */
    public static final int CH6448 = 6448;

    /** Deur 18 storing - DRN1824 */
    public static final int CH6449 = 6449;

    /** Deur 18 storing - DRN1825 */
    public static final int CH6450 = 6450;

    /** Deur 18 storing - DRN1826 */
    public static final int CH6451 = 6451;

    /** Deur 18 storing - DRN1827 */
    public static final int CH6452 = 6452;

    /** Deur 18 storing - DRN1828 */
    public static final int CH6453 = 6453;

    /** Deur 18 storing - DRN1829 */
    public static final int CH6454 = 6454;

    /** Deur 18 storing - DRN1830 */
    public static final int CH6455 = 6455;

    /** Deur 18 storing trede - DRN1832 */
    public static final int CH6456 = 6456;

    /** Deur 18 storing trede - DRN1833 */
    public static final int CH6457 = 6457;

    /** Deur 18 storing trede - DRN1834 */
    public static final int CH6458 = 6458;

    /** Deur 18 storing trede - DRN1835 */
    public static final int CH6459 = 6459;

    /** Deur 18 storing trede - DRN1836 */
    public static final int CH6460 = 6460;

    /** Deur 18 storing trede - DRN1837 */
    public static final int CH6461 = 6461;

    /** Deur 18 storing trede - DRN1838 */
    public static final int CH6462 = 6462;

    /** Deur 19 storing - DRN1902 */
    public static final int CH6463 = 6463;

    /** Deur 19 storing - DRN1903 */
    public static final int CH6464 = 6464;

    /** Deur 19 storing - DRN1904 */
    public static final int CH6465 = 6465;

    /** Deur 19 storing - DRN1905 */
    public static final int CH6466 = 6466;

    /** Deur 19 storing - DRN1906 */
    public static final int CH6467 = 6467;

    /** Deur 19 storing - DRN1907 */
    public static final int CH6468 = 6468;

    /** Deur 19 noodontgrendeling binnen bediend - DRN1908 */
    public static final int CH6469 = 6469;

    /** Deur 19 schakelaar buitenbedrijfstelling bediend - DRN1909 */
    public static final int CH6470 = 6470;

    /** Deur 19 storing - DRN1910 */
    public static final int CH6471 = 6471;

    /** Deur 19 trede storing - DRN1911 */
    public static final int CH6472 = 6472;

    /** Deur 19 - DRN1912 */
    public static final int CH6473 = 6473;

    /** Deur 19 storing - DRN1913 */
    public static final int CH6474 = 6474;

    /** Deur 19 storing - DRN1914 */
    public static final int CH6475 = 6475;

    /** Deur 19 storing - DRN1917 */
    public static final int CH6476 = 6476;

    /** Deur 19 storing - DRN1919 */
    public static final int CH6477 = 6477;

    /** Deur 19 storing - DRN1920 */
    public static final int CH6478 = 6478;

    /** Deur 19 storing - DRN1921 */
    public static final int CH6479 = 6479;

    /** Deur 19 storing - DRN1922 */
    public static final int CH6480 = 6480;

    /** Deur 19 storing - DRN1923 */
    public static final int CH6481 = 6481;

    /** Deur 19 storing - DRN1924 */
    public static final int CH6482 = 6482;

    /** Deur 19 storing - DRN1925 */
    public static final int CH6483 = 6483;

    /** Deur 19 storing - DRN1926 */
    public static final int CH6484 = 6484;

    /** Deur 19 storing - DRN1927 */
    public static final int CH6485 = 6485;

    /** Deur 19 storing - DRN1928 */
    public static final int CH6486 = 6486;

    /** Deur 19 storing - DRN1929 */
    public static final int CH6487 = 6487;

    /** Deur 19 storing - DRN1930 */
    public static final int CH6488 = 6488;

    /** Deur 19 storing trede - DRN1932 */
    public static final int CH6489 = 6489;

    /** Deur 19 storing trede - DRN1933 */
    public static final int CH6490 = 6490;

    /** Deur 19 storing trede - DRN1934 */
    public static final int CH6491 = 6491;

    /** Deur 19 storing trede - DRN1935 */
    public static final int CH6492 = 6492;

    /** Deur 19 storing trede - DRN1936 */
    public static final int CH6493 = 6493;

    /** Deur 19 storing trede - DRN1937 */
    public static final int CH6494 = 6494;

    /** Deur 19 storing trede - DRN1938 */
    public static final int CH6495 = 6495;

    /** Deur 20 storing - DRN2002 */
    public static final int CH6496 = 6496;

    /** Deur 20 storing - DRN2003 */
    public static final int CH6497 = 6497;

    /** Deur 20 storing - DRN2004 */
    public static final int CH6498 = 6498;

    /** Deur 20 storing - DRN2005 */
    public static final int CH6499 = 6499;

    /** Deur 20 storing - DRN2006 */
    public static final int CH6500 = 6500;

    /** Deur 20 storing - DRN2007 */
    public static final int CH6501 = 6501;

    /** Deur 20 noodontgrendeling binnen bediend - DRN2008 */
    public static final int CH6502 = 6502;

    /** Deur 20 schakelaar buitenbedrijfstelling bediend - DRN2009 */
    public static final int CH6503 = 6503;

    /** Deur 20 storing - DRN2010 */
    public static final int CH6504 = 6504;

    /** Deur 20 trede storing - DRN2011 */
    public static final int CH6505 = 6505;

    /** Deur 20 - DRN2012 */
    public static final int CH6506 = 6506;

    /** Deur 20 storing - DRN2013 */
    public static final int CH6507 = 6507;

    /** Deur 20 storing - DRN2014 */
    public static final int CH6508 = 6508;

    /** Deur 20 storing - DRN2017 */
    public static final int CH6509 = 6509;

    /** Deur 20 storing - DRN2019 */
    public static final int CH6510 = 6510;

    /** Deur 20 storing - DRN2020 */
    public static final int CH6511 = 6511;

    /** Deur 20 storing - DRN2021 */
    public static final int CH6512 = 6512;

    /** Deur 20 storing - DRN2022 */
    public static final int CH6513 = 6513;

    /** Deur 20 storing - DRN2023 */
    public static final int CH6514 = 6514;

    /** Deur 20 storing - DRN2024 */
    public static final int CH6515 = 6515;

    /** Deur 20 storing - DRN2025 */
    public static final int CH6516 = 6516;

    /** Deur 20 storing - DRN2026 */
    public static final int CH6517 = 6517;

    /** Deur 20 storing - DRN2027 */
    public static final int CH6518 = 6518;

    /** Deur 20 storing - DRN2028 */
    public static final int CH6519 = 6519;

    /** Deur 20 storing - DRN2029 */
    public static final int CH6520 = 6520;

    /** Deur 20 storing - DRN2030 */
    public static final int CH6521 = 6521;

    /** Deur 20 storing trede - DRN2032 */
    public static final int CH6522 = 6522;

    /** Deur 20 storing trede - DRN2033 */
    public static final int CH6523 = 6523;

    /** Deur 20 storing trede - DRN2034 */
    public static final int CH6524 = 6524;

    /** Deur 20 storing trede - DRN2035 */
    public static final int CH6525 = 6525;

    /** Deur 20 storing trede - DRN2036 */
    public static final int CH6526 = 6526;

    /** Deur 20 storing trede - DRN2037 */
    public static final int CH6527 = 6527;

    /** Deur 20 storing trede - DRN2038 */
    public static final int CH6528 = 6528;

    /** Verschillende bedieningsprocedures - DRN8018 */
    public static final int CH6529 = 6529;

    /** Ongeldige bedieninsprocedure in diagnosecom. - DRN8019 */
    public static final int CH6530 = 6530;

    /** Omvormer voor hulpinstallaties storing - HSP0105 */
    public static final int CH6531 = 6531;

    /** Omvormer voor hulpinstallaties storing - HSP0112 */
    public static final int CH6532 = 6532;

    /** Omvormer voor hulpinstallaties storing - HSP0113 */
    public static final int CH6533 = 6533;

    /** Omvormer voor hulpinstallaties storing - HSP0116 */
    public static final int CH6534 = 6534;

    /** Omvormer voor hulpinstallaties storing - HSP0119 */
    public static final int CH6536 = 6536;

    /** Omvormer voor hulpinstallaties storing - HSP011F */
    public static final int CH6537 = 6537;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0120 */
    public static final int CH6538 = 6538;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0121 */
    public static final int CH6539 = 6539;

    /** Temp. Omvormer voor hulpinst. meer 70Â°C - HSP0132 */
    public static final int CH6540 = 6540;

    /** Omvormer voor hulpinstallaties storing - HSP0133 */
    public static final int CH6541 = 6541;

    /** Temp. Omvormer voor hulpinst. meer 60Â°C - HSP0137 */
    public static final int CH6542 = 6542;

    /** Omvormer voor hulpinstallaties storing - HSP0138 */
    public static final int CH6543 = 6543;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP013C */
    public static final int CH6544 = 6544;

    /** Omvormer voor hulpinstallaties storing - HSP0141 */
    public static final int CH6545 = 6545;

    /** Omvormer voor hulpinstallaties storing - HSP0142 */
    public static final int CH6546 = 6546;

    /** Omvormer voor hulpinstallaties storing - HSP0143 */
    public static final int CH6547 = 6547;

    /** Omvormer voor hulpinstallaties storing - HSP0144 */
    public static final int CH6548 = 6548;

    /** Omvormer voor hulpinstallaties storing - HSP014A */
    public static final int CH6549 = 6549;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP014B */
    public static final int CH6550 = 6550;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP014C */
    public static final int CH6551 = 6551;

    /** Omvormer voor hulpinstallaties storing - HSP0150 */
    public static final int CH6552 = 6552;

    /** Omvormer voor hulpinstallaties storing - HSP0151 */
    public static final int CH6553 = 6553;

    /** Omvormer voor hulpinstallaties storing - HSP0152 */
    public static final int CH6554 = 6554;

    /** Omvormer voor hulpinstallaties storing - HSP015A */
    public static final int CH6555 = 6555;

    /** Omvormer voor hulpinstallaties storing - HSP015B */
    public static final int CH6556 = 6556;

    /** Omvormer voor hulpinstallaties storing - HSP0164 */
    public static final int CH6557 = 6557;

    /** Omvormer voor hulpinstallaties storing - HSP0165 */
    public static final int CH6558 = 6558;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0166 */
    public static final int CH6559 = 6559;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP01FF */
    public static final int CH6560 = 6560;

    /** Omvormer voor hulpinstallaties storing - HSP0205 */
    public static final int CH6561 = 6561;

    /** Omvormer voor hulpinstallaties storing - HSP0212 */
    public static final int CH6562 = 6562;

    /** Omvormer voor hulpinstallaties storing - HSP0213 */
    public static final int CH6563 = 6563;

    /** Omvormer voor hulpinstallaties storing - HSP0216 */
    public static final int CH6564 = 6564;

    /** Omvormer voor hulpinstallaties storing - HSP0219 */
    public static final int CH6566 = 6566;

    /** Omvormer voor hulpinstallaties storing - HSP021F */
    public static final int CH6567 = 6567;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0220 */
    public static final int CH6568 = 6568;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0221 */
    public static final int CH6569 = 6569;

    /** Temp. Omvormer voor hulpinst. meer 70Â°C - HSP0232 */
    public static final int CH6570 = 6570;

    /** Omvormer voor hulpinstallaties storing - HSP0233 */
    public static final int CH6571 = 6571;

    /** Temp. Omvormer voor hulpinst. meer 60Â°C - HSP0237 */
    public static final int CH6572 = 6572;

    /** Omvormer voor hulpinstallaties storing - HSP0238 */
    public static final int CH6573 = 6573;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP023C */
    public static final int CH6574 = 6574;

    /** Omvormer voor hulpinstallaties storing - HSP0241 */
    public static final int CH6575 = 6575;

    /** Omvormer voor hulpinstallaties storing - HSP0242 */
    public static final int CH6576 = 6576;

    /** Omvormer voor hulpinstallaties storing - HSP0243 */
    public static final int CH6577 = 6577;

    /** Omvormer voor hulpinstallaties storing - HSP0244 */
    public static final int CH6578 = 6578;

    /** Omvormer voor hulpinstallaties storing - HSP024A */
    public static final int CH6579 = 6579;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP024B */
    public static final int CH6580 = 6580;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP024C */
    public static final int CH6581 = 6581;

    /** Omvormer voor hulpinstallaties storing - HSP0250 */
    public static final int CH6582 = 6582;

    /** Omvormer voor hulpinstallaties storing - HSP0251 */
    public static final int CH6583 = 6583;

    /** Omvormer voor hulpinstallaties storing - HSP0252 */
    public static final int CH6584 = 6584;

    /** Omvormer voor hulpinstallaties storing - HSP025A */
    public static final int CH6585 = 6585;

    /** Omvormer voor hulpinstallaties storing - HSP025B */
    public static final int CH6586 = 6586;

    /** Omvormer voor hulpinstallaties storing - HSP0264 */
    public static final int CH6587 = 6587;

    /** Omvormer voor hulpinstallaties storing - HSP0265 */
    public static final int CH6588 = 6588;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP0266 */
    public static final int CH6589 = 6589;

    /** Omvormer voor hulpinstallaties uitgevallen - HSP02FF */
    public static final int CH6590 = 6590;

    /** Stroomafnemer 1 - HSP801A */
    public static final int CH6591 = 6591;

    /** Stroomafnemer 2 - HSP801B */
    public static final int CH6592 = 6592;

    /** Stroomafnemer 1 - HSP804D */
    public static final int CH6593 = 6593;

    /** Stroomafnemer 2 - HSP804E */
    public static final int CH6594 = 6594;

    /** Beide hulpomvormers uitgevallen - HSP8057 */
    public static final int CH6595 = 6595;

    /** Stroomafnemer 1 en 2 hoog - HSP8066 */
    public static final int CH6596 = 6596;

    /** Stroomafnemer 1 en 2 uitgekozen - HSP8067 */
    public static final int CH6597 = 6597;

    /** Stroomafnemer 1 gekozen en drukschakelaar voor stroomafnemer 2 aktief - HSP8068 */
    public static final int CH6598 = 6598;

    /** Stroomafnemer 2 gekozen en drukschakelaar voor stroomafnemer 1 aktief - HSP8069 */
    public static final int CH6599 = 6599;

    /** Uitschakeling door HSO-beveiliging - HSP900B */
    public static final int CH6600 = 6600;

    /** Uitschakeling door HSO-beveiliging - HSP900C */
    public static final int CH6601 = 6601;

    /** Uitschakeling door HSO-beveiliging - HSP900D */
    public static final int CH6602 = 6602;

    /** Uitschakeling door HSO-beveiliging - HSP900E */
    public static final int CH6603 = 6603;

    /** Uitschakeling door HSO-beveiliging - HSP900F */
    public static final int CH6604 = 6604;

    /** Uitschakeling door HSO-beveiliging - HSP9010 */
    public static final int CH6605 = 6605;

    /** Uitschakeling door HSO-beveiliging - HSP9011 */
    public static final int CH6606 = 6606;

    /** Uitschakeling door HSO-beveiliging - HSP9012 */
    public static final int CH6607 = 6607;

    /** Uitschakeling door HSO-beveiliging - HSP9013 */
    public static final int CH6608 = 6608;

    /** Uitschakeling door HSO-beveiliging - HSP9014 */
    public static final int CH6609 = 6609;

    /** Omvormer voor hulpinstallaties buiten dienst - HSP9068 */
    public static final int CH6610 = 6610;

    /** Omvormer voor hulpinstallaties buiten dienst - HSP9069 */
    public static final int CH6611 = 6611;

    /** Verwarming in cabine uitgevallen - KLM0113 */
    public static final int CH6612 = 6612;

    /** Storing in verwarming in cabine - KLM0114 */
    public static final int CH6613 = 6613;

    /** Vloerverwarming storing - KLM0116 */
    public static final int CH6614 = 6614;

    /** Verwarming in cabine uitgevallen - KLM0117 */
    public static final int CH6615 = 6615;

    /** Verwarming in cabine uitgevallen - KLM0118 */
    public static final int CH6616 = 6616;

    /** Vloerverwarming uitgevallen - KLM0119 */
    public static final int CH6617 = 6617;

    /** Vloerverwarming uitgevallen - KLM0120 */
    public static final int CH6618 = 6618;

    /** Geen buitenluchttoevoer - KLM0121 */
    public static final int CH6619 = 6619;

    /** Geen buitenluchttoevoer - KLM0122 */
    public static final int CH6620 = 6620;

    /** Storing in ventilatie cabine - KLM0123 */
    public static final int CH6621 = 6621;

    /** Storing in ventilatie cabine - KLM0124 */
    public static final int CH6622 = 6622;

    /** Ventilatie cabine continu in - KLM0125 */
    public static final int CH6623 = 6623;

    /** Storing in ventilatie cabine - KLM0126 */
    public static final int CH6624 = 6624;

    /** Storing in ventilatie cabine - KLM0127 */
    public static final int CH6625 = 6625;

    /** Storing in ventilatie cabine - KLM0128 */
    public static final int CH6626 = 6626;

    /** Storing in ventilatie cabine - KLM0129 */
    public static final int CH6627 = 6627;

    /** Airco in cabine uitgevallen - KLM0130 */
    public static final int CH6628 = 6628;

    /** Koeling in cabine uitgevallen - KLM0151 */
    public static final int CH6629 = 6629;

    /** Storing in koeling cabine - KLM0152 */
    public static final int CH6630 = 6630;

    /** Storing in koeling cabine - KLM0153 */
    public static final int CH6631 = 6631;

    /** Storing in koeling cabine - KLM0154 */
    public static final int CH6632 = 6632;

    /** Storing in koeling cabine - KLM0155 */
    public static final int CH6633 = 6633;

    /** Storing in koeling cabine - KLM0156 */
    public static final int CH6634 = 6634;

    /** Storing in koeling cabine - KLM0157 */
    public static final int CH6635 = 6635;

    /** Storing in koeling cabine - KLM0158 */
    public static final int CH6636 = 6636;

    /** Storing in koeling cabine - KLM0161 */
    public static final int CH6637 = 6637;

    /** Storing in koeling cabine - KLM0162 */
    public static final int CH6638 = 6638;

    /** Koeling in cabine uitgevallen - KLM0163 */
    public static final int CH6639 = 6639;

    /** Koeling in cabine uitgevallen - KLM0164 */
    public static final int CH6640 = 6640;

    /** Koeling in cabine uitgevallen - KLM0165 */
    public static final int CH6641 = 6641;

    /** Storing schakelaar â€˜temperatuurinstellingâ€˜ - KLM0171 */
    public static final int CH6642 = 6642;

    /** Storing schakelaar â€˜temperatuurinstellingâ€˜ - KLM0172 */
    public static final int CH6643 = 6643;

    /** Storing in bediening â€˜ventilatorsnelheidâ€˜ cabine - KLM0173 */
    public static final int CH6644 = 6644;

    /** Storing in bediening â€˜ventilatorsnelheidâ€˜ cabine - KLM0174 */
    public static final int CH6645 = 6645;

    /** Buitentemperatuursensor functioneert niet - KLM0175 */
    public static final int CH6646 = 6646;

    /** Buitentemperatuursensor functioneert niet - KLM0176 */
    public static final int CH6647 = 6647;

    /** Binnentemperatuursensor functioneert niet - KLM0177 */
    public static final int CH6648 = 6648;

    /** Binnentemperatuursensor functioneert niet - KLM0178 */
    public static final int CH6649 = 6649;

    /** Storing sensor kanaaltemperatuur - KLM0179 */
    public static final int CH6650 = 6650;

    /** Storing sensor kanaaltemperatuur - KLM0180 */
    public static final int CH6651 = 6651;

    /** Storing temperatuursensor apparatenkast - KLM0181 */
    public static final int CH6652 = 6652;

    /** Storing temperatuursensor apparatenkast - KLM0182 */
    public static final int CH6653 = 6653;

    /** Storing temperatuursensor Elektronicakast - KLM0183 */
    public static final int CH6654 = 6654;

    /** Storing temperatuursensor Elektronicakast - KLM0184 */
    public static final int CH6655 = 6655;

    /** Temperatuur te hoog in elektronicakast cabine - KLM0185 */
    public static final int CH6656 = 6656;

    /** Temperatuur te hoog in elektronicakast cabine - KLM0186 */
    public static final int CH6657 = 6657;

    /** Omvormer schakelkastventilator storing - KLM0187 */
    public static final int CH6658 = 6658;

    /** temperatuur schakelkastventilator te hoog - KLM0188 */
    public static final int CH6659 = 6659;

    /** Tempratuur cabine te koud / te warm - KLM0190 */
    public static final int CH6660 = 6660;

    /** Koeling in cabine uitgevallen - KLM0198 */
    public static final int CH6661 = 6661;

    /** Verwarming in cabine uitgevallen - KLM0213 */
    public static final int CH6662 = 6662;

    /** Storing in verwarming in cabine - KLM0214 */
    public static final int CH6663 = 6663;

    /** Vloerverwarming storing - KLM0216 */
    public static final int CH6664 = 6664;

    /** Verwarming in cabine uitgevallen - KLM0217 */
    public static final int CH6665 = 6665;

    /** Verwarming in cabine uitgevallen - KLM0218 */
    public static final int CH6666 = 6666;

    /** Vloerverwarming uitgevallen - KLM0219 */
    public static final int CH6667 = 6667;

    /** Vloerverwarming uitgevallen - KLM0220 */
    public static final int CH6668 = 6668;

    /** Geen buitenluchttoevoer - KLM0221 */
    public static final int CH6669 = 6669;

    /** Geen buitenluchttoevoer - KLM0222 */
    public static final int CH6670 = 6670;

    /** Storing in ventilatie cabine - KLM0223 */
    public static final int CH6671 = 6671;

    /** Storing in ventilatie cabine - KLM0224 */
    public static final int CH6672 = 6672;

    /** Ventilatie cabine continu in - KLM0225 */
    public static final int CH6673 = 6673;

    /** Storing in ventilatie cabine - KLM0226 */
    public static final int CH6674 = 6674;

    /** Storing in ventilatie cabine - KLM0227 */
    public static final int CH6675 = 6675;

    /** Storing in ventilatie cabine - KLM0228 */
    public static final int CH6676 = 6676;

    /** Storing in ventilatie cabine - KLM0229 */
    public static final int CH6677 = 6677;

    /** Airco in cabine uitgevallen - KLM0230 */
    public static final int CH6678 = 6678;

    /** Koeling in cabine uitgevallen - KLM0251 */
    public static final int CH6679 = 6679;

    /** Storing in koeling cabine - KLM0252 */
    public static final int CH6680 = 6680;

    /** Storing in koeling cabine - KLM0253 */
    public static final int CH6681 = 6681;

    /** Storing in koeling cabine - KLM0254 */
    public static final int CH6682 = 6682;

    /** Storing in koeling cabine - KLM0255 */
    public static final int CH6683 = 6683;

    /** Storing in koeling cabine - KLM0256 */
    public static final int CH6684 = 6684;

    /** Storing in koeling cabine - KLM0257 */
    public static final int CH6685 = 6685;

    /** Storing in koeling cabine - KLM0258 */
    public static final int CH6686 = 6686;

    /** Storing in koeling cabine - KLM0261 */
    public static final int CH6687 = 6687;

    /** Storing in koeling cabine - KLM0262 */
    public static final int CH6688 = 6688;

    /** Koeling in cabine uitgevallen - KLM0263 */
    public static final int CH6689 = 6689;

    /** Koeling in cabine uitgevallen - KLM0264 */
    public static final int CH6690 = 6690;

    /** Koeling in cabine uitgevallen - KLM0265 */
    public static final int CH6691 = 6691;

    /** Storing schakelaar â€˜temperatuurinstellingâ€˜ - KLM0271 */
    public static final int CH6692 = 6692;

    /** Storing schakelaar â€˜temperatuurinstellingâ€˜ - KLM0272 */
    public static final int CH6693 = 6693;

    /** Storing in bediening â€˜ventilatorsnelheidâ€˜ cabine - KLM0273 */
    public static final int CH6694 = 6694;

    /** Storing in bediening â€˜ventilatorsnelheidâ€˜ cabine - KLM0274 */
    public static final int CH6695 = 6695;

    /** Buitentemperatuursensor functioneert niet - KLM0275 */
    public static final int CH6696 = 6696;

    /** Buitentemperatuursensor functioneert niet - KLM0276 */
    public static final int CH6697 = 6697;

    /** Binnentemperatuursensor functioneert niet - KLM0277 */
    public static final int CH6698 = 6698;

    /** Binnentemperatuursensor functioneert niet - KLM0278 */
    public static final int CH6699 = 6699;

    /** Storing sensor kanaaltemperatuur - KLM0279 */
    public static final int CH6700 = 6700;

    /** Storing sensor kanaaltemperatuur - KLM0280 */
    public static final int CH6701 = 6701;

    /** Storing temperatuursensor apparatenkast - KLM0281 */
    public static final int CH6702 = 6702;

    /** Storing temperatuursensor apparatenkast - KLM0282 */
    public static final int CH6703 = 6703;

    /** Storing temperatuursensor Elektronicakast - KLM0283 */
    public static final int CH6704 = 6704;

    /** Storing temperatuursensor Elektronicakast - KLM0284 */
    public static final int CH6705 = 6705;

    /** Temperatuur te hoog in elektronicakast cabine - KLM0285 */
    public static final int CH6706 = 6706;

    /** Temperatuur te hoog in elektronicakast cabine - KLM0286 */
    public static final int CH6707 = 6707;

    /** Omvormer schakelkastventilator storing - KLM0287 */
    public static final int CH6708 = 6708;

    /** temperatuur schakelkastventilator te hoog - KLM0288 */
    public static final int CH6709 = 6709;

    /** Tempratuur cabine te koud / te warm - KLM0290 */
    public static final int CH6710 = 6710;

    /** Koeling in cabine uitgevallen - KLM0298 */
    public static final int CH6711 = 6711;

    /** Storing in verwarming reizigersruimte - KLM1113 */
    public static final int CH6712 = 6712;

    /** Storing in verwarming reizigersruimte - KLM1114 */
    public static final int CH6713 = 6713;

    /** Storing in verwarming reizigersruimte - KLM1115 */
    public static final int CH6714 = 6714;

    /** Storing in verwarming reizigersruimte - KLM1116 */
    public static final int CH6715 = 6715;

    /** Verwarming in reizigersruimte uitgevallen - KLM1117 */
    public static final int CH6716 = 6716;

    /** Verwarming in reizigersruimte uitgevallen - KLM1118 */
    public static final int CH6717 = 6717;

    /** Airco in reizigersruimte uitgevallen - KLM1121 */
    public static final int CH6718 = 6718;

    /** Airco in reizigersruimte uitgevallen - KLM1122 */
    public static final int CH6719 = 6719;

    /** Storing in airco reizigersruimte - KLM1123 */
    public static final int CH6720 = 6720;

    /** Storing in ventilatie reizigersruimte - KLM1124 */
    public static final int CH6721 = 6721;

    /** Storing in ventilatie reizigersruimte - KLM1125 */
    public static final int CH6722 = 6722;

    /** Airco in reizigersruimte uitgevallen - KLM1126 */
    public static final int CH6723 = 6723;

    /** Airco in reizigersruimte uitgevallen - KLM1127 */
    public static final int CH6724 = 6724;

    /** Airco in reizigersruimte uitgevallen - KLM1128 */
    public static final int CH6725 = 6725;

    /** Storing in ventilatie reizigersruimte - KLM1129 */
    public static final int CH6726 = 6726;

    /** Storing in ventilatie reizigersruimte - KLM1130 */
    public static final int CH6727 = 6727;

    /** Storing in ventilatie reizigersruimte - KLM1131 */
    public static final int CH6728 = 6728;

    /** Storing in ventilatie reizigersruimte - KLM1132 */
    public static final int CH6729 = 6729;

    /** Storing in ventilatie reizigersruimte - KLM1133 */
    public static final int CH6730 = 6730;

    /** Airco in reizigersruimte uitgevallen - KLM1134 */
    public static final int CH6731 = 6731;

    /** Airco in reizigersruimte uitgevallen - KLM1135 */
    public static final int CH6732 = 6732;

    /** Airco Noodbedrijf uitgevallen - KLM1136 */
    public static final int CH6733 = 6733;

    /** Storing in ventilatie reizigersruimte - KLM1137 */
    public static final int CH6734 = 6734;

    /** Storing in ventilatie reizigersruimte - KLM1138 */
    public static final int CH6735 = 6735;

    /** Storing in ventilatie reizigersruimte - KLM1139 */
    public static final int CH6736 = 6736;

    /** Storing Koeling in reizigersruimte - KLM1151 */
    public static final int CH6737 = 6737;

    /** Storing Koeling in reizigersruimte - KLM1152 */
    public static final int CH6738 = 6738;

    /** Storing Koeling in reizigersruimte - KLM1153 */
    public static final int CH6739 = 6739;

    /** Storing Koeling in reizigersruimte - KLM1154 */
    public static final int CH6740 = 6740;

    /** Storing Koeling in reizigersruimte - KLM1155 */
    public static final int CH6741 = 6741;

    /** Storing Koeling in reizigersruimte - KLM1156 */
    public static final int CH6742 = 6742;

    /** Koeling in reizigersruimte uitgevallen - KLM1157 */
    public static final int CH6743 = 6743;

    /** Koeling in reizigersruimte uitgevallen - KLM1158 */
    public static final int CH6744 = 6744;

    /** Koeling in reizigersruimte uitgevallen - KLM1160 */
    public static final int CH6745 = 6745;

    /** Koeling in reizigersruimte uitgevallen - KLM1161 */
    public static final int CH6746 = 6746;

    /** Storing Koeling in reizigersruimte - KLM1162 */
    public static final int CH6747 = 6747;

    /** Koeling in reizigersruimte uitgevallen - KLM1163 */
    public static final int CH6748 = 6748;

    /** Storing Koeling in reizigersruimte - KLM1164 */
    public static final int CH6749 = 6749;

    /** Koeling in reizigersruimte uitgevallen - KLM1165 */
    public static final int CH6750 = 6750;

    /** Koeling in reizigersruimte uitgevallen - KLM1166 */
    public static final int CH6751 = 6751;

    /** Koeling in reizigersruimte uitgevallen - KLM1167 */
    public static final int CH6752 = 6752;

    /** Storing Koeling in reizigersruimte - KLM1168 */
    public static final int CH6753 = 6753;

    /** Buitentemperatuursensor functioneert niet - KLM1171 */
    public static final int CH6754 = 6754;

    /** Buitentemperatuursensor functioneert niet - KLM1172 */
    public static final int CH6755 = 6755;

    /** Binnentemperatuursensor functioneert niet - KLM1173 */
    public static final int CH6756 = 6756;

    /** Binnentemperatuursensor functioneert niet - KLM1174 */
    public static final int CH6757 = 6757;

    /** Storing temperatuuropnemer - KLM1175 */
    public static final int CH6758 = 6758;

    /** Storing temperatuuropnemer - KLM1176 */
    public static final int CH6759 = 6759;

    /** Storing temperatuuropnemer - KLM1177 */
    public static final int CH6760 = 6760;

    /** Storing temperatuuropnemer - KLM1178 */
    public static final int CH6761 = 6761;

    /** Storing temperatuuropnemer - KLM1179 */
    public static final int CH6762 = 6762;

    /** Storing temperatuuropnemer - KLM1180 */
    public static final int CH6763 = 6763;

    /** Reizigersruimte te koud / te warm - KLM1190 */
    public static final int CH6764 = 6764;

    /** Koeling in reizigersruimte uitgevallen - KLM1198 */
    public static final int CH6765 = 6765;

    /** Storing in verwarming reizigersruimte - KLM1213 */
    public static final int CH6766 = 6766;

    /** Storing in verwarming reizigersruimte - KLM1214 */
    public static final int CH6767 = 6767;

    /** Storing in verwarming reizigersruimte - KLM1215 */
    public static final int CH6768 = 6768;

    /** Storing in verwarming reizigersruimte - KLM1216 */
    public static final int CH6769 = 6769;

    /** Verwarming in reizigersruimte uitgevallen - KLM1217 */
    public static final int CH6770 = 6770;

    /** Verwarming in reizigersruimte uitgevallen - KLM1218 */
    public static final int CH6771 = 6771;

    /** Airco in reizigersruimte uitgevallen - KLM1221 */
    public static final int CH6772 = 6772;

    /** Airco in reizigersruimte uitgevallen - KLM1222 */
    public static final int CH6773 = 6773;

    /** Storing in airco reizigersruimte - KLM1223 */
    public static final int CH6774 = 6774;

    /** Storing in ventilatie reizigersruimte - KLM1224 */
    public static final int CH6775 = 6775;

    /** Storing in ventilatie reizigersruimte - KLM1225 */
    public static final int CH6776 = 6776;

    /** Airco in reizigersruimte uitgevallen - KLM1226 */
    public static final int CH6777 = 6777;

    /** Airco in reizigersruimte uitgevallen - KLM1227 */
    public static final int CH6778 = 6778;

    /** Airco in reizigersruimte uitgevallen - KLM1228 */
    public static final int CH6779 = 6779;

    /** Storing in ventilatie reizigersruimte - KLM1229 */
    public static final int CH6780 = 6780;

    /** Storing in ventilatie reizigersruimte - KLM1230 */
    public static final int CH6781 = 6781;

    /** Storing in ventilatie reizigersruimte - KLM1231 */
    public static final int CH6782 = 6782;

    /** Storing in ventilatie reizigersruimte - KLM1232 */
    public static final int CH6783 = 6783;

    /** Storing in ventilatie reizigersruimte - KLM1233 */
    public static final int CH6784 = 6784;

    /** Airco in reizigersruimte uitgevallen - KLM1234 */
    public static final int CH6785 = 6785;

    /** Airco in reizigersruimte uitgevallen - KLM1235 */
    public static final int CH6786 = 6786;

    /** Airco Noodbedrijf uitgevallen - KLM1236 */
    public static final int CH6787 = 6787;

    /** Storing in ventilatie reizigersruimte - KLM1237 */
    public static final int CH6788 = 6788;

    /** Storing in ventilatie reizigersruimte - KLM1238 */
    public static final int CH6789 = 6789;

    /** Storing in ventilatie reizigersruimte - KLM1239 */
    public static final int CH6790 = 6790;

    /** Storing Koeling in reizigersruimte - KLM1251 */
    public static final int CH6791 = 6791;

    /** Storing Koeling in reizigersruimte - KLM1252 */
    public static final int CH6792 = 6792;

    /** Storing Koeling in reizigersruimte - KLM1253 */
    public static final int CH6793 = 6793;

    /** Storing Koeling in reizigersruimte - KLM1254 */
    public static final int CH6794 = 6794;

    /** Storing Koeling in reizigersruimte - KLM1255 */
    public static final int CH6795 = 6795;

    /** Storing Koeling in reizigersruimte - KLM1256 */
    public static final int CH6796 = 6796;

    /** Koeling in reizigersruimte uitgevallen - KLM1257 */
    public static final int CH6797 = 6797;

    /** Koeling in reizigersruimte uitgevallen - KLM1258 */
    public static final int CH6798 = 6798;

    /** Koeling in reizigersruimte uitgevallen - KLM1260 */
    public static final int CH6799 = 6799;

    /** Koeling in reizigersruimte uitgevallen - KLM1261 */
    public static final int CH6800 = 6800;

    /** Storing Koeling in reizigersruimte - KLM1262 */
    public static final int CH6801 = 6801;

    /** Koeling in reizigersruimte uitgevallen - KLM1263 */
    public static final int CH6802 = 6802;

    /** Storing Koeling in reizigersruimte - KLM1264 */
    public static final int CH6803 = 6803;

    /** Koeling in reizigersruimte uitgevallen - KLM1265 */
    public static final int CH6804 = 6804;

    /** Koeling in reizigersruimte uitgevallen - KLM1266 */
    public static final int CH6805 = 6805;

    /** Koeling in reizigersruimte uitgevallen - KLM1267 */
    public static final int CH6806 = 6806;

    /** Storing Koeling in reizigersruimte - KLM1268 */
    public static final int CH6807 = 6807;

    /** Buitentemperatuursensor functioneert niet - KLM1271 */
    public static final int CH6808 = 6808;

    /** Buitentemperatuursensor functioneert niet - KLM1272 */
    public static final int CH6809 = 6809;

    /** Binnentemperatuursensor functioneert niet - KLM1273 */
    public static final int CH6810 = 6810;

    /** Binnentemperatuursensor functioneert niet - KLM1274 */
    public static final int CH6811 = 6811;

    /** Storing temperatuuropnemer - KLM1275 */
    public static final int CH6812 = 6812;

    /** Storing temperatuuropnemer - KLM1276 */
    public static final int CH6813 = 6813;

    /** Storing temperatuuropnemer - KLM1277 */
    public static final int CH6814 = 6814;

    /** Storing temperatuuropnemer - KLM1278 */
    public static final int CH6815 = 6815;

    /** Storing temperatuuropnemer - KLM1279 */
    public static final int CH6816 = 6816;

    /** Storing temperatuuropnemer - KLM1280 */
    public static final int CH6817 = 6817;

    /** Reizigersruimte te koud / te warm - KLM1290 */
    public static final int CH6818 = 6818;

    /** Koeling in reizigersruimte uitgevallen - KLM1298 */
    public static final int CH6819 = 6819;

    /** Storing in verwarming reizigersruimte - KLM1313 */
    public static final int CH6820 = 6820;

    /** Storing in verwarming reizigersruimte - KLM1314 */
    public static final int CH6821 = 6821;

    /** Storing in verwarming reizigersruimte - KLM1315 */
    public static final int CH6822 = 6822;

    /** Storing in verwarming reizigersruimte - KLM1316 */
    public static final int CH6823 = 6823;

    /** Verwarming in reizigersruimte uitgevallen - KLM1317 */
    public static final int CH6824 = 6824;

    /** Verwarming in reizigersruimte uitgevallen - KLM1318 */
    public static final int CH6825 = 6825;

    /** Airco in reizigersruimte uitgevallen - KLM1321 */
    public static final int CH6826 = 6826;

    /** Airco in reizigersruimte uitgevallen - KLM1322 */
    public static final int CH6827 = 6827;

    /** Storing in airco reizigersruimte - KLM1323 */
    public static final int CH6828 = 6828;

    /** Storing in ventilatie reizigersruimte - KLM1324 */
    public static final int CH6829 = 6829;

    /** Storing in ventilatie reizigersruimte - KLM1325 */
    public static final int CH6830 = 6830;

    /** Airco in reizigersruimte uitgevallen - KLM1326 */
    public static final int CH6831 = 6831;

    /** Airco in reizigersruimte uitgevallen - KLM1327 */
    public static final int CH6832 = 6832;

    /** Airco in reizigersruimte uitgevallen - KLM1328 */
    public static final int CH6833 = 6833;

    /** Storing in ventilatie reizigersruimte - KLM1329 */
    public static final int CH6834 = 6834;

    /** Storing in ventilatie reizigersruimte - KLM1330 */
    public static final int CH6835 = 6835;

    /** Storing in ventilatie reizigersruimte - KLM1331 */
    public static final int CH6836 = 6836;

    /** Storing in ventilatie reizigersruimte - KLM1332 */
    public static final int CH6837 = 6837;

    /** Storing in ventilatie reizigersruimte - KLM1333 */
    public static final int CH6838 = 6838;

    /** Airco in reizigersruimte uitgevallen - KLM1334 */
    public static final int CH6839 = 6839;

    /** Airco in reizigersruimte uitgevallen - KLM1335 */
    public static final int CH6840 = 6840;

    /** Airco Noodbedrijf uitgevallen - KLM1336 */
    public static final int CH6841 = 6841;

    /** Storing in ventilatie reizigersruimte - KLM1337 */
    public static final int CH6842 = 6842;

    /** Storing in ventilatie reizigersruimte - KLM1338 */
    public static final int CH6843 = 6843;

    /** Storing in ventilatie reizigersruimte - KLM1339 */
    public static final int CH6844 = 6844;

    /** Storing Koeling in reizigersruimte - KLM1351 */
    public static final int CH6845 = 6845;

    /** Storing Koeling in reizigersruimte - KLM1352 */
    public static final int CH6846 = 6846;

    /** Storing Koeling in reizigersruimte - KLM1353 */
    public static final int CH6847 = 6847;

    /** Storing Koeling in reizigersruimte - KLM1354 */
    public static final int CH6848 = 6848;

    /** Storing Koeling in reizigersruimte - KLM1355 */
    public static final int CH6849 = 6849;

    /** Storing Koeling in reizigersruimte - KLM1356 */
    public static final int CH6850 = 6850;

    /** Koeling in reizigersruimte uitgevallen - KLM1357 */
    public static final int CH6851 = 6851;

    /** Koeling in reizigersruimte uitgevallen - KLM1358 */
    public static final int CH6852 = 6852;

    /** Koeling in reizigersruimte uitgevallen - KLM1360 */
    public static final int CH6853 = 6853;

    /** Koeling in reizigersruimte uitgevallen - KLM1361 */
    public static final int CH6854 = 6854;

    /** Storing Koeling in reizigersruimte - KLM1362 */
    public static final int CH6855 = 6855;

    /** Koeling in reizigersruimte uitgevallen - KLM1363 */
    public static final int CH6856 = 6856;

    /** Storing Koeling in reizigersruimte - KLM1364 */
    public static final int CH6857 = 6857;

    /** Koeling in reizigersruimte uitgevallen - KLM1365 */
    public static final int CH6858 = 6858;

    /** Koeling in reizigersruimte uitgevallen - KLM1366 */
    public static final int CH6859 = 6859;

    /** Koeling in reizigersruimte uitgevallen - KLM1367 */
    public static final int CH6860 = 6860;

    /** Storing Koeling in reizigersruimte - KLM1368 */
    public static final int CH6861 = 6861;

    /** Buitentemperatuursensor functioneert niet - KLM1371 */
    public static final int CH6862 = 6862;

    /** Buitentemperatuursensor functioneert niet - KLM1372 */
    public static final int CH6863 = 6863;

    /** Binnentemperatuursensor functioneert niet - KLM1373 */
    public static final int CH6864 = 6864;

    /** Binnentemperatuursensor functioneert niet - KLM1374 */
    public static final int CH6865 = 6865;

    /** Storing temperatuuropnemer - KLM1375 */
    public static final int CH6866 = 6866;

    /** Storing temperatuuropnemer - KLM1376 */
    public static final int CH6867 = 6867;

    /** Storing temperatuuropnemer - KLM1377 */
    public static final int CH6868 = 6868;

    /** Storing temperatuuropnemer - KLM1378 */
    public static final int CH6869 = 6869;

    /** Storing temperatuuropnemer - KLM1379 */
    public static final int CH6870 = 6870;

    /** Storing temperatuuropnemer - KLM1380 */
    public static final int CH6871 = 6871;

    /** Reizigersruimte te koud / te warm - KLM1390 */
    public static final int CH6872 = 6872;

    /** Koeling in reizigersruimte uitgevallen - KLM1398 */
    public static final int CH6873 = 6873;

    /** Storing in verwarming reizigersruimte - KLM1413 */
    public static final int CH6874 = 6874;

    /** Storing in verwarming reizigersruimte - KLM1414 */
    public static final int CH6875 = 6875;

    /** Storing in verwarming reizigersruimte - KLM1415 */
    public static final int CH6876 = 6876;

    /** Storing in verwarming reizigersruimte - KLM1416 */
    public static final int CH6877 = 6877;

    /** Verwarming in reizigersruimte uitgevallen - KLM1417 */
    public static final int CH6878 = 6878;

    /** Verwarming in reizigersruimte uitgevallen - KLM1418 */
    public static final int CH6879 = 6879;

    /** Airco in reizigersruimte uitgevallen - KLM1421 */
    public static final int CH6880 = 6880;

    /** Airco in reizigersruimte uitgevallen - KLM1422 */
    public static final int CH6881 = 6881;

    /** Storing in airco reizigersruimte - KLM1423 */
    public static final int CH6882 = 6882;

    /** Storing in ventilatie reizigersruimte - KLM1424 */
    public static final int CH6883 = 6883;

    /** Storing in ventilatie reizigersruimte - KLM1425 */
    public static final int CH6884 = 6884;

    /** Airco in reizigersruimte uitgevallen - KLM1426 */
    public static final int CH6885 = 6885;

    /** Airco in reizigersruimte uitgevallen - KLM1427 */
    public static final int CH6886 = 6886;

    /** Airco in reizigersruimte uitgevallen - KLM1428 */
    public static final int CH6887 = 6887;

    /** Storing in ventilatie reizigersruimte - KLM1429 */
    public static final int CH6888 = 6888;

    /** Storing in ventilatie reizigersruimte - KLM1430 */
    public static final int CH6889 = 6889;

    /** Storing in ventilatie reizigersruimte - KLM1431 */
    public static final int CH6890 = 6890;

    /** Storing in ventilatie reizigersruimte - KLM1432 */
    public static final int CH6891 = 6891;

    /** Storing in ventilatie reizigersruimte - KLM1433 */
    public static final int CH6892 = 6892;

    /** Airco in reizigersruimte uitgevallen - KLM1434 */
    public static final int CH6893 = 6893;

    /** Airco in reizigersruimte uitgevallen - KLM1435 */
    public static final int CH6894 = 6894;

    /** Airco Noodbedrijf uitgevallen - KLM1436 */
    public static final int CH6895 = 6895;

    /** Storing in ventilatie reizigersruimte - KLM1437 */
    public static final int CH6896 = 6896;

    /** Storing in ventilatie reizigersruimte - KLM1438 */
    public static final int CH6897 = 6897;

    /** Storing in ventilatie reizigersruimte - KLM1439 */
    public static final int CH6898 = 6898;

    /** Storing Koeling in reizigersruimte - KLM1451 */
    public static final int CH6899 = 6899;

    /** Storing Koeling in reizigersruimte - KLM1452 */
    public static final int CH6900 = 6900;

    /** Storing Koeling in reizigersruimte - KLM1453 */
    public static final int CH6901 = 6901;

    /** Storing Koeling in reizigersruimte - KLM1454 */
    public static final int CH6902 = 6902;

    /** Storing Koeling in reizigersruimte - KLM1455 */
    public static final int CH6903 = 6903;

    /** Storing Koeling in reizigersruimte - KLM1456 */
    public static final int CH6904 = 6904;

    /** Koeling in reizigersruimte uitgevallen - KLM1457 */
    public static final int CH6905 = 6905;

    /** Koeling in reizigersruimte uitgevallen - KLM1458 */
    public static final int CH6906 = 6906;

    /** Koeling in reizigersruimte uitgevallen - KLM1460 */
    public static final int CH6907 = 6907;

    /** Koeling in reizigersruimte uitgevallen - KLM1461 */
    public static final int CH6908 = 6908;

    /** Storing Koeling in reizigersruimte - KLM1462 */
    public static final int CH6909 = 6909;

    /** Koeling in reizigersruimte uitgevallen - KLM1463 */
    public static final int CH6910 = 6910;

    /** Storing Koeling in reizigersruimte - KLM1464 */
    public static final int CH6911 = 6911;

    /** Koeling in reizigersruimte uitgevallen - KLM1465 */
    public static final int CH6912 = 6912;

    /** Koeling in reizigersruimte uitgevallen - KLM1466 */
    public static final int CH6913 = 6913;

    /** Koeling in reizigersruimte uitgevallen - KLM1467 */
    public static final int CH6914 = 6914;

    /** Storing Koeling in reizigersruimte - KLM1468 */
    public static final int CH6915 = 6915;

    /** Buitentemperatuursensor functioneert niet - KLM1471 */
    public static final int CH6916 = 6916;

    /** Buitentemperatuursensor functioneert niet - KLM1472 */
    public static final int CH6917 = 6917;

    /** Binnentemperatuursensor functioneert niet - KLM1473 */
    public static final int CH6918 = 6918;

    /** Binnentemperatuursensor functioneert niet - KLM1474 */
    public static final int CH6919 = 6919;

    /** Storing temperatuuropnemer - KLM1475 */
    public static final int CH6920 = 6920;

    /** Storing temperatuuropnemer - KLM1476 */
    public static final int CH6921 = 6921;

    /** Storing temperatuuropnemer - KLM1477 */
    public static final int CH6922 = 6922;

    /** Storing temperatuuropnemer - KLM1478 */
    public static final int CH6923 = 6923;

    /** Storing temperatuuropnemer - KLM1479 */
    public static final int CH6924 = 6924;

    /** Storing temperatuuropnemer - KLM1480 */
    public static final int CH6925 = 6925;

    /** Reizigersruimte te koud / te warm - KLM1490 */
    public static final int CH6926 = 6926;

    /** Koeling in reizigersruimte uitgevallen - KLM1498 */
    public static final int CH6927 = 6927;

    /** Storing in verwarming reizigersruimte - KLM1513 */
    public static final int CH6928 = 6928;

    /** Storing in verwarming reizigersruimte - KLM1514 */
    public static final int CH6929 = 6929;

    /** Storing in verwarming reizigersruimte - KLM1515 */
    public static final int CH6930 = 6930;

    /** Storing in verwarming reizigersruimte - KLM1516 */
    public static final int CH6931 = 6931;

    /** Verwarming in reizigersruimte uitgevallen - KLM1517 */
    public static final int CH6932 = 6932;

    /** Verwarming in reizigersruimte uitgevallen - KLM1518 */
    public static final int CH6933 = 6933;

    /** Airco in reizigersruimte uitgevallen - KLM1521 */
    public static final int CH6934 = 6934;

    /** Airco in reizigersruimte uitgevallen - KLM1522 */
    public static final int CH6935 = 6935;

    /** Storing in airco reizigersruimte - KLM1523 */
    public static final int CH6936 = 6936;

    /** Storing in ventilatie reizigersruimte - KLM1524 */
    public static final int CH6937 = 6937;

    /** Storing in ventilatie reizigersruimte - KLM1525 */
    public static final int CH6938 = 6938;

    /** Airco in reizigersruimte uitgevallen - KLM1526 */
    public static final int CH6939 = 6939;

    /** Airco in reizigersruimte uitgevallen - KLM1527 */
    public static final int CH6940 = 6940;

    /** Airco in reizigersruimte uitgevallen - KLM1528 */
    public static final int CH6941 = 6941;

    /** Storing in ventilatie reizigersruimte - KLM1529 */
    public static final int CH6942 = 6942;

    /** Storing in ventilatie reizigersruimte - KLM1530 */
    public static final int CH6943 = 6943;

    /** Storing in ventilatie reizigersruimte - KLM1531 */
    public static final int CH6944 = 6944;

    /** Storing in ventilatie reizigersruimte - KLM1532 */
    public static final int CH6945 = 6945;

    /** Storing in ventilatie reizigersruimte - KLM1533 */
    public static final int CH6946 = 6946;

    /** Airco in reizigersruimte uitgevallen - KLM1534 */
    public static final int CH6947 = 6947;

    /** Airco in reizigersruimte uitgevallen - KLM1535 */
    public static final int CH6948 = 6948;

    /** Airco Noodbedrijf uitgevallen - KLM1536 */
    public static final int CH6949 = 6949;

    /** Storing in ventilatie reizigersruimte - KLM1537 */
    public static final int CH6950 = 6950;

    /** Storing in ventilatie reizigersruimte - KLM1538 */
    public static final int CH6951 = 6951;

    /** Storing in ventilatie reizigersruimte - KLM1539 */
    public static final int CH6952 = 6952;

    /** Storing Koeling in reizigersruimte - KLM1551 */
    public static final int CH6953 = 6953;

    /** Storing Koeling in reizigersruimte - KLM1552 */
    public static final int CH6954 = 6954;

    /** Storing Koeling in reizigersruimte - KLM1553 */
    public static final int CH6955 = 6955;

    /** Storing Koeling in reizigersruimte - KLM1554 */
    public static final int CH6956 = 6956;

    /** Storing Koeling in reizigersruimte - KLM1555 */
    public static final int CH6957 = 6957;

    /** Storing Koeling in reizigersruimte - KLM1556 */
    public static final int CH6958 = 6958;

    /** Koeling in reizigersruimte uitgevallen - KLM1557 */
    public static final int CH6959 = 6959;

    /** Koeling in reizigersruimte uitgevallen - KLM1558 */
    public static final int CH6960 = 6960;

    /** Koeling in reizigersruimte uitgevallen - KLM1560 */
    public static final int CH6961 = 6961;

    /** Koeling in reizigersruimte uitgevallen - KLM1561 */
    public static final int CH6962 = 6962;

    /** Storing Koeling in reizigersruimte - KLM1562 */
    public static final int CH6963 = 6963;

    /** Koeling in reizigersruimte uitgevallen - KLM1563 */
    public static final int CH6964 = 6964;

    /** Storing Koeling in reizigersruimte - KLM1564 */
    public static final int CH6965 = 6965;

    /** Koeling in reizigersruimte uitgevallen - KLM1565 */
    public static final int CH6966 = 6966;

    /** Koeling in reizigersruimte uitgevallen - KLM1566 */
    public static final int CH6967 = 6967;

    /** Koeling in reizigersruimte uitgevallen - KLM1567 */
    public static final int CH6968 = 6968;

    /** Storing Koeling in reizigersruimte - KLM1568 */
    public static final int CH6969 = 6969;

    /** Buitentemperatuursensor functioneert niet - KLM1571 */
    public static final int CH6970 = 6970;

    /** Buitentemperatuursensor functioneert niet - KLM1572 */
    public static final int CH6971 = 6971;

    /** Binnentemperatuursensor functioneert niet - KLM1573 */
    public static final int CH6972 = 6972;

    /** Binnentemperatuursensor functioneert niet - KLM1574 */
    public static final int CH6973 = 6973;

    /** Storing temperatuuropnemer - KLM1575 */
    public static final int CH6974 = 6974;

    /** Storing temperatuuropnemer - KLM1576 */
    public static final int CH6975 = 6975;

    /** Storing temperatuuropnemer - KLM1577 */
    public static final int CH6976 = 6976;

    /** Storing temperatuuropnemer - KLM1578 */
    public static final int CH6977 = 6977;

    /** Storing temperatuuropnemer - KLM1579 */
    public static final int CH6978 = 6978;

    /** Storing temperatuuropnemer - KLM1580 */
    public static final int CH6979 = 6979;

    /** Reizigersruimte te koud / te warm - KLM1590 */
    public static final int CH6980 = 6980;

    /** Koeling in reizigersruimte uitgevallen - KLM1598 */
    public static final int CH6981 = 6981;

    /** Storing in verwarming reizigersruimte - KLM1613 */
    public static final int CH6982 = 6982;

    /** Storing in verwarming reizigersruimte - KLM1614 */
    public static final int CH6983 = 6983;

    /** Storing in verwarming reizigersruimte - KLM1615 */
    public static final int CH6984 = 6984;

    /** Storing in verwarming reizigersruimte - KLM1616 */
    public static final int CH6985 = 6985;

    /** Verwarming in reizigersruimte uitgevallen - KLM1617 */
    public static final int CH6986 = 6986;

    /** Verwarming in reizigersruimte uitgevallen - KLM1618 */
    public static final int CH6987 = 6987;

    /** Airco in reizigersruimte uitgevallen - KLM1621 */
    public static final int CH6988 = 6988;

    /** Airco in reizigersruimte uitgevallen - KLM1622 */
    public static final int CH6989 = 6989;

    /** Storing in airco reizigersruimte - KLM1623 */
    public static final int CH6990 = 6990;

    /** Storing in ventilatie reizigersruimte - KLM1624 */
    public static final int CH6991 = 6991;

    /** Storing in ventilatie reizigersruimte - KLM1625 */
    public static final int CH6992 = 6992;

    /** Airco in reizigersruimte uitgevallen - KLM1626 */
    public static final int CH6993 = 6993;

    /** Airco in reizigersruimte uitgevallen - KLM1627 */
    public static final int CH6994 = 6994;

    /** Airco in reizigersruimte uitgevallen - KLM1628 */
    public static final int CH6995 = 6995;

    /** Storing in ventilatie reizigersruimte - KLM1629 */
    public static final int CH6996 = 6996;

    /** Storing in ventilatie reizigersruimte - KLM1630 */
    public static final int CH6997 = 6997;

    /** Storing in ventilatie reizigersruimte - KLM1631 */
    public static final int CH6998 = 6998;

    /** Storing in ventilatie reizigersruimte - KLM1632 */
    public static final int CH6999 = 6999;

    /** Storing in ventilatie reizigersruimte - KLM1633 */
    public static final int CH7000 = 7000;

    /** Airco in reizigersruimte uitgevallen - KLM1634 */
    public static final int CH7001 = 7001;

    /** Airco in reizigersruimte uitgevallen - KLM1635 */
    public static final int CH7002 = 7002;

    /** Airco Noodbedrijf uitgevallen - KLM1636 */
    public static final int CH7003 = 7003;

    /** Storing in ventilatie reizigersruimte - KLM1637 */
    public static final int CH7004 = 7004;

    /** Storing in ventilatie reizigersruimte - KLM1638 */
    public static final int CH7005 = 7005;

    /** Storing in ventilatie reizigersruimte - KLM1639 */
    public static final int CH7006 = 7006;

    /** Storing Koeling in reizigersruimte - KLM1651 */
    public static final int CH7007 = 7007;

    /** Storing Koeling in reizigersruimte - KLM1652 */
    public static final int CH7008 = 7008;

    /** Storing Koeling in reizigersruimte - KLM1653 */
    public static final int CH7009 = 7009;

    /** Storing Koeling in reizigersruimte - KLM1654 */
    public static final int CH7010 = 7010;

    /** Storing Koeling in reizigersruimte - KLM1655 */
    public static final int CH7011 = 7011;

    /** Storing Koeling in reizigersruimte - KLM1656 */
    public static final int CH7012 = 7012;

    /** Koeling in reizigersruimte uitgevallen - KLM1657 */
    public static final int CH7013 = 7013;

    /** Koeling in reizigersruimte uitgevallen - KLM1658 */
    public static final int CH7014 = 7014;

    /** Koeling in reizigersruimte uitgevallen - KLM1660 */
    public static final int CH7015 = 7015;

    /** Koeling in reizigersruimte uitgevallen - KLM1661 */
    public static final int CH7016 = 7016;

    /** Storing Koeling in reizigersruimte - KLM1662 */
    public static final int CH7017 = 7017;

    /** Koeling in reizigersruimte uitgevallen - KLM1663 */
    public static final int CH7018 = 7018;

    /** Storing Koeling in reizigersruimte - KLM1664 */
    public static final int CH7019 = 7019;

    /** Koeling in reizigersruimte uitgevallen - KLM1665 */
    public static final int CH7020 = 7020;

    /** Koeling in reizigersruimte uitgevallen - KLM1666 */
    public static final int CH7021 = 7021;

    /** Koeling in reizigersruimte uitgevallen - KLM1667 */
    public static final int CH7022 = 7022;

    /** Storing Koeling in reizigersruimte - KLM1668 */
    public static final int CH7023 = 7023;

    /** Buitentemperatuursensor functioneert niet - KLM1671 */
    public static final int CH7024 = 7024;

    /** Buitentemperatuursensor functioneert niet - KLM1672 */
    public static final int CH7025 = 7025;

    /** Binnentemperatuursensor functioneert niet - KLM1673 */
    public static final int CH7026 = 7026;

    /** Binnentemperatuursensor functioneert niet - KLM1674 */
    public static final int CH7027 = 7027;

    /** Storing temperatuuropnemer - KLM1675 */
    public static final int CH7028 = 7028;

    /** Storing temperatuuropnemer - KLM1676 */
    public static final int CH7029 = 7029;

    /** Storing temperatuuropnemer - KLM1677 */
    public static final int CH7030 = 7030;

    /** Storing temperatuuropnemer - KLM1678 */
    public static final int CH7031 = 7031;

    /** Storing temperatuuropnemer - KLM1679 */
    public static final int CH7032 = 7032;

    /** Storing temperatuuropnemer - KLM1680 */
    public static final int CH7033 = 7033;

    /** Reizigersruimte te koud / te warm - KLM1690 */
    public static final int CH7034 = 7034;

    /** Koeling in reizigersruimte uitgevallen - KLM1698 */
    public static final int CH7035 = 7035;

    /** Snelschakelaar uitgeschakeld - KLM801E */
    public static final int CH7036 = 7036;

    /** Elektronikakast te warm - KLM8024 */
    public static final int CH7037 = 7037;

    /** Elektronikakast te warm - KLM8025 */
    public static final int CH7038 = 7038;

    /** Noodventilatie ingeschakeld - KLM9051 */
    public static final int CH7039 = 7039;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9052 */
    public static final int CH7040 = 7040;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9053 */
    public static final int CH7041 = 7041;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9054 */
    public static final int CH7042 = 7042;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9055 */
    public static final int CH7043 = 7043;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9056 */
    public static final int CH7044 = 7044;

    /** Noodventilatie op diagnosescherm ingeschakeld. - KLM9057 */
    public static final int CH7045 = 7045;

    /** Batterijlader uitgevallen - LSP0101 */
    public static final int CH7046 = 7046;

    /** Batterijlading beperkt - LSP0102 */
    public static final int CH7047 = 7047;

    /** Batterijlading beperkt - LSP0103 */
    public static final int CH7048 = 7048;

    /** Batterijlader uitgevallen - LSP0104 */
    public static final int CH7049 = 7049;

    /** Batterijlader uitgevallen - LSP0105 */
    public static final int CH7050 = 7050;

    /** Batterijlader uitgevallen - LSP0106 */
    public static final int CH7051 = 7051;

    /** Batterijlader uitgevallen - LSP0107 */
    public static final int CH7052 = 7052;

    /** Batterijlader uitgevallen - LSP0108 */
    public static final int CH7053 = 7053;

    /** Batterijveiligheid aangesproken - LSP0109 */
    public static final int CH7054 = 7054;

    /** Batterijlader uitgevallen - LSP0201 */
    public static final int CH7055 = 7055;

    /** Batterijlading beperkt - LSP0202 */
    public static final int CH7056 = 7056;

    /** Batterijlading beperkt - LSP0203 */
    public static final int CH7057 = 7057;

    /** Batterijlader uitgevallen - LSP0204 */
    public static final int CH7058 = 7058;

    /** Batterijlader uitgevallen - LSP0205 */
    public static final int CH7059 = 7059;

    /** Batterijlader uitgevallen - LSP0206 */
    public static final int CH7060 = 7060;

    /** Batterijlader uitgevallen - LSP0207 */
    public static final int CH7061 = 7061;

    /** Batterijlader uitgevallen - LSP0208 */
    public static final int CH7062 = 7062;

    /** Batterijveiligheid aangesproken - LSP0209 */
    public static final int CH7063 = 7063;

    /** Batterijrelais niet ingeschakeld - LSP8026 */
    public static final int CH7064 = 7064;

    /** Batterijrelais niet ingeschakeld - LSP8027 */
    public static final int CH7065 = 7065;

    /** Beide laders uitgevallen - LSP8028 */
    public static final int CH7066 = 7066;

    /** Batterijspanning in hele treinstel te laag - LSP8029 */
    public static final int CH7067 = 7067;

    /** Batterijspanning van 1 batterij te laag - LSP802A */
    public static final int CH7068 = 7068;

    /** Batterijspanning van 1 batterij te laag - LSP802B */
    public static final int CH7069 = 7069;

    /** Beide 110/24 V omzetters in storing - LSP802C */
    public static final int CH7070 = 7070;

    /** Beide 110/24 V omzetters in storing - LSP802D */
    public static final int CH7071 = 7071;

    /** EÃ©n 110/24 V omzetter in storing - LSP802E */
    public static final int CH7072 = 7072;

    /** EÃ©n 110/24 V omzetter in storing - LSP802F */
    public static final int CH7073 = 7073;

    /** Compressor schakelt niet in - LVZ8030 */
    public static final int CH7074 = 7074;

    /** Compressor schakelt niet uit - LVZ8031 */
    public static final int CH7075 = 7075;

    /** Minder dan 8 bar in stroomafnemerreservoir - LVZ8032 */
    public static final int CH7076 = 7076;

    /** Therm. beveiliging hulpcompressor aangespr. - LVZ8033 */
    public static final int CH7077 = 7077;

    /** Meer dan 8,5 bar in stroomafnemerreservoir - LVZ8034 */
    public static final int CH7078 = 7078;

    /** Hulpcompressor schakelt niet in - LVZ8035 */
    public static final int CH7079 = 7079;

    /** Hulpcompressor schakelt niet uit - LVZ8036 */
    public static final int CH7080 = 7080;

    /** Onjuiste waarde druk in stroomafnemerreser. - LVZ8037 */
    public static final int CH7081 = 7081;

    /** Druk in stroomafnemerreser. minder dan 7 bar - LVZ8038 */
    public static final int CH7082 = 7082;

    /** Storing Frontdisplay - PIS0101 */
    public static final int CH7083 = 7083;

    /** Storing Frontdisplay - PIS0102 */
    public static final int CH7084 = 7084;

    /** Storing Frontdisplay - PIS0103 */
    public static final int CH7085 = 7085;

    /** Storing Frontdisplay - PIS0104 */
    public static final int CH7086 = 7086;

    /** Frontdisplay uitgevallen - PIS0105 */
    public static final int CH7087 = 7087;

    /** Storing Frontdisplay - PIS0201 */
    public static final int CH7088 = 7088;

    /** Storing Frontdisplay - PIS0202 */
    public static final int CH7089 = 7089;

    /** Storing Frontdisplay - PIS0203 */
    public static final int CH7090 = 7090;

    /** Storing Frontdisplay - PIS0204 */
    public static final int CH7091 = 7091;

    /** Frontdisplay uitgevallen - PIS0205 */
    public static final int CH7092 = 7092;

    /** Storing Zijdisplay 1 - PIS1101 */
    public static final int CH7093 = 7093;

    /** Storing Zijdisplay 1 - PIS1102 */
    public static final int CH7094 = 7094;

    /** Storing Zijdisplay 1 - PIS1103 */
    public static final int CH7095 = 7095;

    /** Storing Zijdisplay 1 - PIS1104 */
    public static final int CH7096 = 7096;

    /** Zijdisplay 1 uitgevallen - PIS1105 */
    public static final int CH7097 = 7097;

    /** Storing Zijdisplay 2 - PIS1201 */
    public static final int CH7098 = 7098;

    /** Storing Zijdisplay 2 - PIS1202 */
    public static final int CH7099 = 7099;

    /** Storing Zijdisplay 2 - PIS1203 */
    public static final int CH7100 = 7100;

    /** Storing Zijdisplay 2 - PIS1204 */
    public static final int CH7101 = 7101;

    /** Zijdisplay 2 uitgevallen - PIS1205 */
    public static final int CH7102 = 7102;

    /** Storing Zijdisplay 3 - PIS1301 */
    public static final int CH7103 = 7103;

    /** Storing Zijdisplay 3 - PIS1302 */
    public static final int CH7104 = 7104;

    /** Storing Zijdisplay 3 - PIS1303 */
    public static final int CH7105 = 7105;

    /** Storing Zijdisplay 3 - PIS1304 */
    public static final int CH7106 = 7106;

    /** Zijdisplay 3 uitgevallen - PIS1305 */
    public static final int CH7107 = 7107;

    /** Storing Zijdisplay 4 - PIS1401 */
    public static final int CH7108 = 7108;

    /** Storing Zijdisplay 4 - PIS1402 */
    public static final int CH7109 = 7109;

    /** Storing Zijdisplay 4 - PIS1403 */
    public static final int CH7110 = 7110;

    /** Storing Zijdisplay 4 - PIS1404 */
    public static final int CH7111 = 7111;

    /** Zijdisplay 4 uitgevallen - PIS1405 */
    public static final int CH7112 = 7112;

    /** Storing Zijdisplay 5 - PIS1501 */
    public static final int CH7113 = 7113;

    /** Storing Zijdisplay 5 - PIS1502 */
    public static final int CH7114 = 7114;

    /** Storing Zijdisplay 5 - PIS1503 */
    public static final int CH7115 = 7115;

    /** Storing Zijdisplay 5 - PIS1504 */
    public static final int CH7116 = 7116;

    /** Zijdisplay 5 uitgevallen - PIS1505 */
    public static final int CH7117 = 7117;

    /** Storing Zijdisplay 6 - PIS1601 */
    public static final int CH7118 = 7118;

    /** Storing Zijdisplay 6 - PIS1602 */
    public static final int CH7119 = 7119;

    /** Storing Zijdisplay 6 - PIS1603 */
    public static final int CH7120 = 7120;

    /** Storing Zijdisplay 6 - PIS1604 */
    public static final int CH7121 = 7121;

    /** Zijdisplay 6 uitgevallen - PIS1605 */
    public static final int CH7122 = 7122;

    /** Storing Zijdisplay 7 - PIS1701 */
    public static final int CH7123 = 7123;

    /** Storing Zijdisplay 7 - PIS1702 */
    public static final int CH7124 = 7124;

    /** Storing Zijdisplay 7 - PIS1703 */
    public static final int CH7125 = 7125;

    /** Storing Zijdisplay 7 - PIS1704 */
    public static final int CH7126 = 7126;

    /** Zijdisplay 7 uitgevallen - PIS1705 */
    public static final int CH7127 = 7127;

    /** Storing Zijdisplay 8 - PIS1801 */
    public static final int CH7128 = 7128;

    /** Storing Zijdisplay 8 - PIS1802 */
    public static final int CH7129 = 7129;

    /** Storing Zijdisplay 8 - PIS1803 */
    public static final int CH7130 = 7130;

    /** Storing Zijdisplay 8 - PIS1804 */
    public static final int CH7131 = 7131;

    /** Zijdisplay 8 uitgevallen - PIS1805 */
    public static final int CH7132 = 7132;

    /** Storing Zijdisplay 9 - PIS1901 */
    public static final int CH7133 = 7133;

    /** Storing Zijdisplay 9 - PIS1902 */
    public static final int CH7134 = 7134;

    /** Storing Zijdisplay 9 - PIS1903 */
    public static final int CH7135 = 7135;

    /** Storing Zijdisplay 9 - PIS1904 */
    public static final int CH7136 = 7136;

    /** Zijdisplay 9 uitgevallen - PIS1905 */
    public static final int CH7137 = 7137;

    /** Storing Zijdisplay 10 - PIS2001 */
    public static final int CH7138 = 7138;

    /** Storing Zijdisplay 10 - PIS2002 */
    public static final int CH7139 = 7139;

    /** Storing Zijdisplay 10 - PIS2003 */
    public static final int CH7140 = 7140;

    /** Storing Zijdisplay 10 - PIS2004 */
    public static final int CH7141 = 7141;

    /** Zijdisplay 10 uitgevallen - PIS2005 */
    public static final int CH7142 = 7142;

    /** Storing Zijdisplay 11 - PIS2101 */
    public static final int CH7143 = 7143;

    /** Storing Zijdisplay 11 - PIS2102 */
    public static final int CH7144 = 7144;

    /** Storing Zijdisplay 11 - PIS2103 */
    public static final int CH7145 = 7145;

    /** Storing Zijdisplay 11 - PIS2104 */
    public static final int CH7146 = 7146;

    /** Zijdisplay 11 uitgevallen - PIS2105 */
    public static final int CH7147 = 7147;

    /** Storing Zijdisplay 12 - PIS2201 */
    public static final int CH7148 = 7148;

    /** Storing Zijdisplay 12 - PIS2202 */
    public static final int CH7149 = 7149;

    /** Storing Zijdisplay 12 - PIS2203 */
    public static final int CH7150 = 7150;

    /** Storing Zijdisplay 12 - PIS2204 */
    public static final int CH7151 = 7151;

    /** Zijdisplay 12 uitgevallen - PIS2205 */
    public static final int CH7152 = 7152;

    /** Automatsche omroep gestoord - PIS3001 */
    public static final int CH7153 = 7153;

    /** Omroepversterker uitgevallen - PIS3002 */
    public static final int CH7154 = 7154;

    /** Automatische omroep uitgevallen - PIS3003 */
    public static final int CH7155 = 7155;

    /** Automatische omroep uitgevallen - PIS3004 */
    public static final int CH7156 = 7156;

    /** Automatische omroep uitgevallen - PIS3005 */
    public static final int CH7157 = 7157;

    /** Omroep en treintelefoon functioneren niet - PIS3006 */
    public static final int CH7158 = 7158;

    /** Omroep en treintelefoon functioneren niet - PIS3007 */
    public static final int CH7159 = 7159;

    /** Reizigersintercom 1 uitgevallen - PIS3008 */
    public static final int CH7160 = 7160;

    /** Reizigersintercom 2 uitgevallen - PIS3009 */
    public static final int CH7161 = 7161;

    /** Reizigersintercom 3 uitgevallen - PIS300A */
    public static final int CH7162 = 7162;

    /** Reizigersintercom 4 uitgevallen - PIS300B */
    public static final int CH7163 = 7163;

    /** Reizigersintercom 5 uitgevallen - PIS300C */
    public static final int CH7164 = 7164;

    /** Reizigersintercom 6 uitgevallen - PIS300D */
    public static final int CH7165 = 7165;

    /** Reizigersintercom 7 uitgevallen - PIS300E */
    public static final int CH7166 = 7166;

    /** Reizigersintercom 8 uitgevallen - PIS300F */
    public static final int CH7167 = 7167;

    /** Reizigersintercom 9 uitgevallen - PIS3010 */
    public static final int CH7168 = 7168;

    /** Reizigersintercom 10 uitgevallen - PIS3011 */
    public static final int CH7169 = 7169;

    /** Reisinformatie gestoord - PIS4001 */
    public static final int CH7170 = 7170;

    /** Reisinformatie uitgevallen - PIS4002 */
    public static final int CH7171 = 7171;

    /** Binnen-display 1 uitgevallen - PIS4101 */
    public static final int CH7172 = 7172;

    /** Binnen-display 1 storing - PIS4102 */
    public static final int CH7173 = 7173;

    /** Binnen-display 1 uitgevallen - PIS4103 */
    public static final int CH7174 = 7174;

    /** Binnen-display 1 uitgevallen - PIS4104 */
    public static final int CH7175 = 7175;

    /** Binnen-display 2 uitgevallen - PIS4201 */
    public static final int CH7176 = 7176;

    /** Binnen-display 2 storing - PIS4202 */
    public static final int CH7177 = 7177;

    /** Binnen-display 2 uitgevallen - PIS4203 */
    public static final int CH7178 = 7178;

    /** Binnen-display 2 uitgevallen - PIS4204 */
    public static final int CH7179 = 7179;

    /** Binnen-display 3 uitgevallen - PIS4301 */
    public static final int CH7180 = 7180;

    /** Binnen-display 3 storing - PIS4302 */
    public static final int CH7181 = 7181;

    /** Binnen-display 3 uitgevallen - PIS4303 */
    public static final int CH7182 = 7182;

    /** Binnen-display 3 uitgevallen - PIS4304 */
    public static final int CH7183 = 7183;

    /** Binnen-display 4 uitgevallen - PIS4401 */
    public static final int CH7184 = 7184;

    /** Binnen-display 4 storing - PIS4402 */
    public static final int CH7185 = 7185;

    /** Binnen-display 4 uitgevallen - PIS4403 */
    public static final int CH7186 = 7186;

    /** Binnen-display 4 uitgevallen - PIS4404 */
    public static final int CH7187 = 7187;

    /** Binnen-display 5 uitgevallen - PIS4501 */
    public static final int CH7188 = 7188;

    /** Binnen-display 5 storing - PIS4502 */
    public static final int CH7189 = 7189;

    /** Binnen-display 5 uitgevallen - PIS4503 */
    public static final int CH7190 = 7190;

    /** Binnen-display 5 uitgevallen - PIS4504 */
    public static final int CH7191 = 7191;

    /** Binnen-display 6 uitgevallen - PIS4601 */
    public static final int CH7192 = 7192;

    /** Binnen-display 6 storing - PIS4602 */
    public static final int CH7193 = 7193;

    /** Binnen-display 6 uitgevallen - PIS4603 */
    public static final int CH7194 = 7194;

    /** Binnen-display 6 uitgevallen - PIS4604 */
    public static final int CH7195 = 7195;

    /** Binnen-display 7 uitgevallen - PIS4701 */
    public static final int CH7196 = 7196;

    /** Binnen-display 7 storing - PIS4702 */
    public static final int CH7197 = 7197;

    /** Binnen-display 7 uitgevallen - PIS4703 */
    public static final int CH7198 = 7198;

    /** Binnen-display 7 uitgevallen - PIS4704 */
    public static final int CH7199 = 7199;

    /** Binnen-display 8 uitgevallen - PIS4801 */
    public static final int CH7200 = 7200;

    /** Binnen-display 8 storing - PIS4802 */
    public static final int CH7201 = 7201;

    /** Binnen-display 8 uitgevallen - PIS4803 */
    public static final int CH7202 = 7202;

    /** Binnen-display 8 uitgevallen - PIS4804 */
    public static final int CH7203 = 7203;

    /** Binnen-display 9 uitgevallen - PIS4901 */
    public static final int CH7204 = 7204;

    /** Binnen-display 9 storing - PIS4902 */
    public static final int CH7205 = 7205;

    /** Binnen-display 9 uitgevallen - PIS4903 */
    public static final int CH7206 = 7206;

    /** Binnen-display 9 uitgevallen - PIS4904 */
    public static final int CH7207 = 7207;

    /** Binnen-display 10 uitgevallen - PIS5001 */
    public static final int CH7208 = 7208;

    /** Binnen-display 10 storing - PIS5002 */
    public static final int CH7209 = 7209;

    /** Binnen-display 10 uitgevallen - PIS5003 */
    public static final int CH7210 = 7210;

    /** Binnen-display 10 uitgevallen - PIS5004 */
    public static final int CH7211 = 7211;

    /** Binnen-display 11 uitgevallen - PIS5101 */
    public static final int CH7212 = 7212;

    /** Binnen-display 11 storing - PIS5102 */
    public static final int CH7213 = 7213;

    /** Binnen-display 11 uitgevallen - PIS5103 */
    public static final int CH7214 = 7214;

    /** Binnen-display 11 uitgevallen - PIS5104 */
    public static final int CH7215 = 7215;

    /** Binnen-display 12 uitgevallen - PIS5201 */
    public static final int CH7216 = 7216;

    /** Binnen-display 12 storing - PIS5202 */
    public static final int CH7217 = 7217;

    /** Binnen-display 12 uitgevallen - PIS5203 */
    public static final int CH7218 = 7218;

    /** Binnen-display 12 uitgevallen - PIS5204 */
    public static final int CH7219 = 7219;

    /** Binnen-display 13 uitgevallen - PIS5301 */
    public static final int CH7220 = 7220;

    /** Binnen-display 13 storing - PIS5302 */
    public static final int CH7221 = 7221;

    /** Binnen-display 13 uitgevallen - PIS5303 */
    public static final int CH7222 = 7222;

    /** Binnen-display 13 uitgevallen - PIS5304 */
    public static final int CH7223 = 7223;

    /** Binnen-display 14 uitgevallen - PIS5401 */
    public static final int CH7224 = 7224;

    /** Binnen-display 14 storing - PIS5402 */
    public static final int CH7225 = 7225;

    /** Binnen-display 14 uitgevallen - PIS5403 */
    public static final int CH7226 = 7226;

    /** Binnen-display 14 uitgevallen - PIS5404 */
    public static final int CH7227 = 7227;

    /** Binnen-display 15 uitgevallen - PIS5501 */
    public static final int CH7228 = 7228;

    /** Binnen-display 15 storing - PIS5502 */
    public static final int CH7229 = 7229;

    /** Binnen-display 15 uitgevallen - PIS5503 */
    public static final int CH7230 = 7230;

    /** Binnen-display 15 uitgevallen - PIS5504 */
    public static final int CH7231 = 7231;

    /** Binnen-display 16 uitgevallen - PIS5601 */
    public static final int CH7232 = 7232;

    /** Binnen-display 16 storing - PIS5602 */
    public static final int CH7233 = 7233;

    /** Binnen-display 16 uitgevallen - PIS5603 */
    public static final int CH7234 = 7234;

    /** Binnen-display 16 uitgevallen - PIS5604 */
    public static final int CH7235 = 7235;

    /** Binnen-display 17 uitgevallen - PIS5701 */
    public static final int CH7236 = 7236;

    /** Binnen-display 17 storing - PIS5702 */
    public static final int CH7237 = 7237;

    /** Binnen-display 17 uitgevallen - PIS5703 */
    public static final int CH7238 = 7238;

    /** Binnen-display 17 uitgevallen - PIS5704 */
    public static final int CH7239 = 7239;

    /** Binnen-display 18 uitgevallen - PIS5801 */
    public static final int CH7240 = 7240;

    /** Binnen-display 18 storing - PIS5802 */
    public static final int CH7241 = 7241;

    /** Binnen-display 18 uitgevallen - PIS5803 */
    public static final int CH7242 = 7242;

    /** Binnen-display 18 uitgevallen - PIS5804 */
    public static final int CH7243 = 7243;

    /** Binnen-display 19 uitgevallen - PIS5901 */
    public static final int CH7244 = 7244;

    /** Binnen-display 19 storing - PIS5902 */
    public static final int CH7245 = 7245;

    /** Binnen-display 19 uitgevallen - PIS5903 */
    public static final int CH7246 = 7246;

    /** Binnen-display 19 uitgevallen - PIS5904 */
    public static final int CH7247 = 7247;

    /** Binnen-display 20 uitgevallen - PIS6001 */
    public static final int CH7248 = 7248;

    /** Binnen-display 20 storing - PIS6002 */
    public static final int CH7249 = 7249;

    /** Binnen-display 20 uitgevallen - PIS6003 */
    public static final int CH7250 = 7250;

    /** Binnen-display 20 uitgevallen - PIS6004 */
    public static final int CH7251 = 7251;

    /** Storing camerabewaking - PIS8039 */
    public static final int CH7252 = 7252;

    /** Camerabewaking uitgevallen - PIS803A */
    public static final int CH7253 = 7253;

    /** Storing camerabewaking - PIS803B */
    public static final int CH7254 = 7254;

    /** Camerabewaking uitgevallen - PIS803C */
    public static final int CH7255 = 7255;

    /** Rembesturing gestoord - REM0101 */
    public static final int CH7256 = 7256;

    /** Rembesturing gestoord - REM0102 */
    public static final int CH7257 = 7257;

    /** Snelremcircuit gestoord - REM0103 */
    public static final int CH7258 = 7258;

    /** Snelremcircuit gestoord - REM0104 */
    public static final int CH7259 = 7259;

    /** Rembesturing gestoord - REM0105 */
    public static final int CH7260 = 7260;

    /** Bremssteuerung gestÃ¶rt - REM0106 */
    public static final int CH7261 = 7261;

    /** Snelremcircuit gestoord - REM0107 */
    public static final int CH7262 = 7262;

    /** Snelremcircuit gestoord - REM0108 */
    public static final int CH7263 = 7263;

    /** Rembesturing gestoord - REM0109 */
    public static final int CH7264 = 7264;

    /** Rembesturing gestoord - REM010A */
    public static final int CH7265 = 7265;

    /** Snelremcircuit gestoord - REM010B */
    public static final int CH7266 = 7266;

    /** Snelremcircuit gestoord - REM010C */
    public static final int CH7267 = 7267;

    /** Rembesturing gestoord - REM010D */
    public static final int CH7268 = 7268;

    /** Snelremcircuit gestoord - REM010E */
    public static final int CH7269 = 7269;

    /** Rembesturing gestoord - REM010F */
    public static final int CH7270 = 7270;

    /** Rembesturing gestoord - REM0110 */
    public static final int CH7271 = 7271;

    /** Indirecte rem vergrendeld - REM0111 */
    public static final int CH7272 = 7272;

    /** Indirecte rem vergrendeld - REM0112 */
    public static final int CH7273 = 7273;

    /** Rembesturing gestoord - REM0113 */
    public static final int CH7274 = 7274;

    /** Rembesturing - onderhoud nodig - REM0114 */
    public static final int CH7275 = 7275;

    /** Pneumatische rem gestoord - REM0118 */
    public static final int CH7276 = 7276;

    /** Pneumatische rem gestoord - REM0119 */
    public static final int CH7277 = 7277;

    /** Ten onrechte signaal stilstand - REM011A */
    public static final int CH7278 = 7278;

    /** Geen signaal stilstand - REM011B */
    public static final int CH7279 = 7279;

    /** Rembesturing gestoord - REM0120 */
    public static final int CH7280 = 7280;

    /** Rembesturing gestoord - REM0121 */
    public static final int CH7281 = 7281;

    /** Rembesturing gestoord - REM0122 */
    public static final int CH7282 = 7282;

    /** Rembesturing gestoord - REM0123 */
    public static final int CH7283 = 7283;

    /** Rembesturing gestoord - REM0124 */
    public static final int CH7284 = 7284;

    /** Rembesturing gestoord - REM0125 */
    public static final int CH7285 = 7285;

    /** Rembesturing gestoord - REM0126 */
    public static final int CH7286 = 7286;

    /** Rembesturing gestoord - REM0127 */
    public static final int CH7287 = 7287;

    /** Rembesturing gestoord - REM0128 */
    public static final int CH7288 = 7288;

    /** Rembesturing gestoord - REM0129 */
    public static final int CH7289 = 7289;

    /** Rembesturing gestoord - REM012A */
    public static final int CH7290 = 7290;

    /** Rembesturing gestoord - REM012B */
    public static final int CH7291 = 7291;

    /** Rembesturing gestoord - REM012C */
    public static final int CH7292 = 7292;

    /** Rembesturing gestoord - REM012E */
    public static final int CH7293 = 7293;

    /** Rembesturing gestoord - REM012F */
    public static final int CH7294 = 7294;

    /** Componentuitval rembesturing - REM0130 */
    public static final int CH7295 = 7295;

    /** Componentuitval rembesturing - REM0131 */
    public static final int CH7296 = 7296;

    /** Componentuitval rembesturing - REM0133 */
    public static final int CH7297 = 7297;

    /** Componentuitval rembesturing - REM0134 */
    public static final int CH7298 = 7298;

    /** Componentuitval rembesturing - REM0135 */
    public static final int CH7299 = 7299;

    /** Componentuitval rembesturing - REM0136 */
    public static final int CH7300 = 7300;

    /** Componentuitval rembesturing - REM0137 */
    public static final int CH7301 = 7301;

    /** Computerstoring rembesturing - REM0138 */
    public static final int CH7302 = 7302;

    /** Computerstoring rembesturing - REM0139 */
    public static final int CH7303 = 7303;

    /** Computerstoring rembesturing - REM013A */
    public static final int CH7304 = 7304;

    /** Computerstoring rembesturing - REM013B */
    public static final int CH7305 = 7305;

    /** Computerstoring rembesturing - REM013C */
    public static final int CH7306 = 7306;

    /** Computerstoring rembesturing - REM013D */
    public static final int CH7307 = 7307;

    /** Computerstoring rembesturing - REM013E */
    public static final int CH7308 = 7308;

    /** Computerstoring rembesturing - REM013F */
    public static final int CH7309 = 7309;

    /** Computerstoring rembesturing - REM0140 */
    public static final int CH7310 = 7310;

    /** Computerstoring rembesturing - REM0141 */
    public static final int CH7311 = 7311;

    /** Computerstoring rembesturing - REM0142 */
    public static final int CH7312 = 7312;

    /** Computerstoring rembesturing - REM0144 */
    public static final int CH7313 = 7313;

    /** Computerstoring rembesturing - REM0146 */
    public static final int CH7314 = 7314;

    /** Computerstoring rembesturing - REM0147 */
    public static final int CH7315 = 7315;

    /** Besturing snelremcircuit gestoord - REM0148 */
    public static final int CH7316 = 7316;

    /** Besturing snelremcircuit gestoord - REM0149 */
    public static final int CH7317 = 7317;

    /** Besturing snelremcircuit gestoord - REM014A */
    public static final int CH7318 = 7318;

    /** Besturing snelremcircuit gestoord - REM014B */
    public static final int CH7319 = 7319;

    /** Besturing snelremcircuit gestoord - REM014C */
    public static final int CH7320 = 7320;

    /** MVB-storing rembesturing - REM014D */
    public static final int CH7321 = 7321;

    /** MVB-storing rembesturing - REM014E */
    public static final int CH7322 = 7322;

    /** Rembesturing gestoord - REM0151 */
    public static final int CH7323 = 7323;

    /** Rembesturing gestoord - REM0152 */
    public static final int CH7324 = 7324;

    /** Rembesturing gestoord - REM0153 */
    public static final int CH7325 = 7325;

    /** Rembesturing gestoord - REM0154 */
    public static final int CH7326 = 7326;

    /** Rembesturing gestoord - REM0155 */
    public static final int CH7327 = 7327;

    /** Rembesturing gestoord - REM0156 */
    public static final int CH7328 = 7328;

    /** Rembesturing gestoord - REM0157 */
    public static final int CH7329 = 7329;

    /** Rembesturing gestoord - REM0158 */
    public static final int CH7330 = 7330;

    /** Pneumatische rem niet gelost - REM0159 */
    public static final int CH7331 = 7331;

    /** Pneumatische rem niet gelost - REM015A */
    public static final int CH7332 = 7332;

    /** Pneumatische rem niet gelost - REM015B */
    public static final int CH7333 = 7333;

    /** Pneumatische rem niet gelost - REM015C */
    public static final int CH7334 = 7334;

    /** Pneumatische rem gestoord - REM0161 */
    public static final int CH7335 = 7335;

    /** Pneumatische rem gestoord - REM0162 */
    public static final int CH7336 = 7336;

    /** Pneumatische rem gestoord - REM0163 */
    public static final int CH7337 = 7337;

    /** Pneumatische rem gestoord - REM0164 */
    public static final int CH7338 = 7338;

    /** MG-rem gestoord - REM0169 */
    public static final int CH7339 = 7339;

    /** MG-rem lost niet - REM016A */
    public static final int CH7340 = 7340;

    /** MG-rem gestoord - REM016B */
    public static final int CH7341 = 7341;

    /** MG-rem lost niet - REM016C */
    public static final int CH7342 = 7342;

    /** MG-rem gestoord - REM016D */
    public static final int CH7343 = 7343;

    /** MG-rem lost niet - REM016E */
    public static final int CH7344 = 7344;

    /** MG-rem gestoord - REM016F */
    public static final int CH7345 = 7345;

    /** MG-rem lost niet - REM0170 */
    public static final int CH7346 = 7346;

    /** Rembesturing gestoord - REM0201 */
    public static final int CH7347 = 7347;

    /** Rembesturing gestoord - REM0202 */
    public static final int CH7348 = 7348;

    /** Rembesturing gestoord - REM0203 */
    public static final int CH7349 = 7349;

    /** Rembesturing gestoord - REM0204 */
    public static final int CH7350 = 7350;

    /** Rembesturing gestoord - REM0205 */
    public static final int CH7351 = 7351;

    /** Bremssteuerung gestÃ¶rt - REM0206 */
    public static final int CH7352 = 7352;

    /** Rembesturing gestoord - REM0207 */
    public static final int CH7353 = 7353;

    /** Rembesturing gestoord - REM0208 */
    public static final int CH7354 = 7354;

    /** Rembesturing gestoord - REM0209 */
    public static final int CH7355 = 7355;

    /** Rembesturing gestoord - REM020A */
    public static final int CH7356 = 7356;

    /** Rembesturing gestoord - REM020B */
    public static final int CH7357 = 7357;

    /** Rembesturing gestoord - REM020C */
    public static final int CH7358 = 7358;

    /** Rembesturing gestoord - REM020D */
    public static final int CH7359 = 7359;

    /** Rembesturing gestoord - REM020E */
    public static final int CH7360 = 7360;

    /** Rembesturing gestoord - REM020F */
    public static final int CH7361 = 7361;

    /** Rembesturing gestoord - REM0210 */
    public static final int CH7362 = 7362;

    /** Indirecte rem vergrendeld - REM0211 */
    public static final int CH7363 = 7363;

    /** Indirecte rem vergrendeld - REM0212 */
    public static final int CH7364 = 7364;

    /** Rembesturing gestoord - REM0213 */
    public static final int CH7365 = 7365;

    /** Rembesturing - onderhoud nodig - REM0214 */
    public static final int CH7366 = 7366;

    /** Pneumatische rem gestoord - REM0218 */
    public static final int CH7367 = 7367;

    /** Pneumatische rem gestoord - REM0219 */
    public static final int CH7368 = 7368;

    /** Ten onrechte signaal stilstand - REM021A */
    public static final int CH7369 = 7369;

    /** Geen signaal stilstand - REM021B */
    public static final int CH7370 = 7370;

    /** Rembesturing gestoord - REM0220 */
    public static final int CH7371 = 7371;

    /** Rembesturing gestoord - REM0221 */
    public static final int CH7372 = 7372;

    /** Rembesturing gestoord - REM0222 */
    public static final int CH7373 = 7373;

    /** Rembesturing gestoord - REM0223 */
    public static final int CH7374 = 7374;

    /** Rembesturing gestoord - REM0224 */
    public static final int CH7375 = 7375;

    /** Rembesturing gestoord - REM0225 */
    public static final int CH7376 = 7376;

    /** Rembesturing gestoord - REM0226 */
    public static final int CH7377 = 7377;

    /** Rembesturing gestoord - REM0227 */
    public static final int CH7378 = 7378;

    /** Rembesturing gestoord - REM0228 */
    public static final int CH7379 = 7379;

    /** Rembesturing gestoord - REM0229 */
    public static final int CH7380 = 7380;

    /** Rembesturing gestoord - REM022A */
    public static final int CH7381 = 7381;

    /** Rembesturing gestoord - REM022B */
    public static final int CH7382 = 7382;

    /** Rembesturing gestoord - REM022C */
    public static final int CH7383 = 7383;

    /** Rembesturing gestoord - REM022E */
    public static final int CH7384 = 7384;

    /** Rembesturing gestoord - REM022F */
    public static final int CH7385 = 7385;

    /** Componentuitval rembesturing - REM0230 */
    public static final int CH7386 = 7386;

    /** Componentuitval rembesturing - REM0231 */
    public static final int CH7387 = 7387;

    /** Componentuitval rembesturing - REM0233 */
    public static final int CH7388 = 7388;

    /** Componentuitval rembesturing - REM0234 */
    public static final int CH7389 = 7389;

    /** Componentuitval rembesturing - REM0235 */
    public static final int CH7390 = 7390;

    /** Componentuitval rembesturing - REM0236 */
    public static final int CH7391 = 7391;

    /** Componentuitval rembesturing - REM0237 */
    public static final int CH7392 = 7392;

    /** Computerstoring rembesturing - REM0238 */
    public static final int CH7393 = 7393;

    /** Computerstoring rembesturing - REM0239 */
    public static final int CH7394 = 7394;

    /** Computerstoring rembesturing - REM023A */
    public static final int CH7395 = 7395;

    /** Computerstoring rembesturing - REM023B */
    public static final int CH7396 = 7396;

    /** Computerstoring rembesturing - REM023C */
    public static final int CH7397 = 7397;

    /** Computerstoring rembesturing - REM023D */
    public static final int CH7398 = 7398;

    /** Computerstoring rembesturing - REM023E */
    public static final int CH7399 = 7399;

    /** Computerstoring rembesturing - REM023F */
    public static final int CH7400 = 7400;

    /** Computerstoring rembesturing - REM0240 */
    public static final int CH7401 = 7401;

    /** Computerstoring rembesturing - REM0241 */
    public static final int CH7402 = 7402;

    /** Computerstoring rembesturing - REM0242 */
    public static final int CH7403 = 7403;

    /** Computerstoring rembesturing - REM0244 */
    public static final int CH7404 = 7404;

    /** Computerstoring rembesturing - REM0246 */
    public static final int CH7405 = 7405;

    /** Computerstoring rembesturing - REM0247 */
    public static final int CH7406 = 7406;

    /** Besturing snelremcircuit gestoord - REM0248 */
    public static final int CH7407 = 7407;

    /** Besturing snelremcircuit gestoord - REM0249 */
    public static final int CH7408 = 7408;

    /** Besturing snelremcircuit gestoord - REM024A */
    public static final int CH7409 = 7409;

    /** Besturing snelremcircuit gestoord - REM024B */
    public static final int CH7410 = 7410;

    /** Besturing snelremcircuit gestoord - REM024C */
    public static final int CH7411 = 7411;

    /** MVB-storing rembesturing - REM024D */
    public static final int CH7412 = 7412;

    /** MVB-storing rembesturing - REM024E */
    public static final int CH7413 = 7413;

    /** Rembesturing gestoord - REM0251 */
    public static final int CH7414 = 7414;

    /** Rembesturing gestoord - REM0252 */
    public static final int CH7415 = 7415;

    /** Rembesturing gestoord - REM0253 */
    public static final int CH7416 = 7416;

    /** Rembesturing gestoord - REM0254 */
    public static final int CH7417 = 7417;

    /** Rembesturing gestoord - REM0255 */
    public static final int CH7418 = 7418;

    /** Rembesturing gestoord - REM0256 */
    public static final int CH7419 = 7419;

    /** Pneumatische rem niet gelost - REM0259 */
    public static final int CH7420 = 7420;

    /** Pneumatische rem niet gelost - REM025A */
    public static final int CH7421 = 7421;

    /** Pneumatische rem niet gelost - REM025B */
    public static final int CH7422 = 7422;

    /** Pneumatische rem gestoord - REM0261 */
    public static final int CH7423 = 7423;

    /** Pneumatische rem gestoord - REM0262 */
    public static final int CH7424 = 7424;

    /** Pneumatische rem gestoord - REM0263 */
    public static final int CH7425 = 7425;

    /** MG-rem gestoord - REM0269 */
    public static final int CH7426 = 7426;

    /** MG-rem lost niet - REM026A */
    public static final int CH7427 = 7427;

    /** MG-rem gestoord - REM026B */
    public static final int CH7428 = 7428;

    /** MG-rem lost niet - REM026C */
    public static final int CH7429 = 7429;

    /** Pneumatische anti-blokkeer gestoord - REM1105 */
    public static final int CH7430 = 7430;

    /** Pneumatische anti-blokkeer gestoord - REM1107 */
    public static final int CH7431 = 7431;

    /** EP-rem gestoord - REM1108 */
    public static final int CH7432 = 7432;

    /** EP-rem gestoord - REM1109 */
    public static final int CH7433 = 7433;

    /** EP-rem gestoord - REM110B */
    public static final int CH7434 = 7434;

    /** Uitval luchtveer - REM110C */
    public static final int CH7435 = 7435;

    /** Pneumatische rem gestoord - REM110D */
    public static final int CH7436 = 7436;

    /** Snelrem gestoord - REM110E */
    public static final int CH7437 = 7437;

    /** Pneumatische rem niet gelost - REM110F */
    public static final int CH7438 = 7438;

    /** Pneumatische rem gestoord - REM1110 */
    public static final int CH7439 = 7439;

    /** Parkeerrem lost niet - REM1111 */
    public static final int CH7440 = 7440;

    /** Parkeerrem niet gelost - REM1112 */
    public static final int CH7441 = 7441;

    /** Parkeerrem wordt niet ingeschakeld - REM1113 */
    public static final int CH7442 = 7442;

    /** EP-rem gestoord - REM1117 */
    public static final int CH7443 = 7443;

    /** Pneumatische rem gestoord - REM1118 */
    public static final int CH7444 = 7444;

    /** Anti-blokkeer gestoord - REM1119 */
    public static final int CH7445 = 7445;

    /** Pneumatische rem gestoord - REM111A */
    public static final int CH7446 = 7446;

    /** Parkeerrem diagnose gestoord - REM111B */
    public static final int CH7447 = 7447;

    /** EP-rem afgesloten - REM111C */
    public static final int CH7448 = 7448;

    /** Parkeerrem afgesloten - REM111D */
    public static final int CH7449 = 7449;

    /** - REM1121 */
    public static final int CH7450 = 7450;

    /** - REM1122 */
    public static final int CH7451 = 7451;

    /** - REM1123 */
    public static final int CH7452 = 7452;

    /** - REM1124 */
    public static final int CH7453 = 7453;

    /** - REM1125 */
    public static final int CH7454 = 7454;

    /** - REM1126 */
    public static final int CH7455 = 7455;

    /** - REM1127 */
    public static final int CH7456 = 7456;

    /** - REM1128 */
    public static final int CH7457 = 7457;

    /** - REM1129 */
    public static final int CH7458 = 7458;

    /** - REM112A */
    public static final int CH7459 = 7459;

    /** - REM112B */
    public static final int CH7460 = 7460;

    /** - REM112C */
    public static final int CH7461 = 7461;

    /** - REM112E */
    public static final int CH7462 = 7462;

    /** - REM112F */
    public static final int CH7463 = 7463;

    /** - REM1132 */
    public static final int CH7464 = 7464;

    /** - REM1136 */
    public static final int CH7465 = 7465;

    /** - REM1137 */
    public static final int CH7466 = 7466;

    /** - REM1138 */
    public static final int CH7467 = 7467;

    /** - REM113A */
    public static final int CH7468 = 7468;

    /** - REM113B */
    public static final int CH7469 = 7469;

    /** - REM113C */
    public static final int CH7470 = 7470;

    /** - REM113D */
    public static final int CH7471 = 7471;

    /** - REM113E */
    public static final int CH7472 = 7472;

    /** - REM113F */
    public static final int CH7473 = 7473;

    /** - REM1140 */
    public static final int CH7474 = 7474;

    /** - REM1141 */
    public static final int CH7475 = 7475;

    /** - REM1142 */
    public static final int CH7476 = 7476;

    /** - REM1143 */
    public static final int CH7477 = 7477;

    /** - REM1144 */
    public static final int CH7478 = 7478;

    /** - REM1145 */
    public static final int CH7479 = 7479;

    /** - REM1146 */
    public static final int CH7480 = 7480;

    /** - REM1147 */
    public static final int CH7481 = 7481;

    /** - REM1148 */
    public static final int CH7482 = 7482;

    /** - REM1149 */
    public static final int CH7483 = 7483;

    /** - REM114A */
    public static final int CH7484 = 7484;

    /** - REM114D */
    public static final int CH7485 = 7485;

    /** - REM1159 */
    public static final int CH7486 = 7486;

    /** - REM115A */
    public static final int CH7487 = 7487;

    /** - REM115D */
    public static final int CH7488 = 7488;

    /** - REM115E */
    public static final int CH7489 = 7489;

    /** - REM115F */
    public static final int CH7490 = 7490;

    /** - REM1160 */
    public static final int CH7491 = 7491;

    /** Pneumatische anti-blokkeer gestoord - REM1205 */
    public static final int CH7492 = 7492;

    /** Pneumatische anti-blokkeer gestoord - REM1207 */
    public static final int CH7493 = 7493;

    /** EP-rem gestoord - REM1208 */
    public static final int CH7494 = 7494;

    /** EP-rem gestoord - REM1209 */
    public static final int CH7495 = 7495;

    /** EP-rem gestoord - REM120B */
    public static final int CH7496 = 7496;

    /** Uitval luchtveer - REM120C */
    public static final int CH7497 = 7497;

    /** Pneumatische rem gestoord - REM120D */
    public static final int CH7498 = 7498;

    /** Snelrem gestoord - REM120E */
    public static final int CH7499 = 7499;

    /** Pneumatische rem niet gelost - REM120F */
    public static final int CH7500 = 7500;

    /** Pneumatische rem gestoord - REM1210 */
    public static final int CH7501 = 7501;

    /** EP-rem gestoord - REM1217 */
    public static final int CH7502 = 7502;

    /** Pneumatische rem gestoord - REM1218 */
    public static final int CH7503 = 7503;

    /** Pneumatische rem gestoord - REM121A */
    public static final int CH7504 = 7504;

    /** EP-rem afgesloten - REM121C */
    public static final int CH7505 = 7505;

    /** - REM1221 */
    public static final int CH7506 = 7506;

    /** - REM1222 */
    public static final int CH7507 = 7507;

    /** - REM1223 */
    public static final int CH7508 = 7508;

    /** - REM1224 */
    public static final int CH7509 = 7509;

    /** - REM1225 */
    public static final int CH7510 = 7510;

    /** - REM1226 */
    public static final int CH7511 = 7511;

    /** - REM1227 */
    public static final int CH7512 = 7512;

    /** - REM1228 */
    public static final int CH7513 = 7513;

    /** - REM1229 */
    public static final int CH7514 = 7514;

    /** - REM122A */
    public static final int CH7515 = 7515;

    /** - REM122B */
    public static final int CH7516 = 7516;

    /** - REM122C */
    public static final int CH7517 = 7517;

    /** - REM122E */
    public static final int CH7518 = 7518;

    /** - REM122F */
    public static final int CH7519 = 7519;

    /** - REM1236 */
    public static final int CH7520 = 7520;

    /** - REM1237 */
    public static final int CH7521 = 7521;

    /** - REM1238 */
    public static final int CH7522 = 7522;

    /** - REM123A */
    public static final int CH7523 = 7523;

    /** - REM123B */
    public static final int CH7524 = 7524;

    /** - REM123C */
    public static final int CH7525 = 7525;

    /** - REM123D */
    public static final int CH7526 = 7526;

    /** - REM123E */
    public static final int CH7527 = 7527;

    /** - REM1241 */
    public static final int CH7528 = 7528;

    /** - REM1242 */
    public static final int CH7529 = 7529;

    /** - REM1245 */
    public static final int CH7530 = 7530;

    /** - REM1246 */
    public static final int CH7531 = 7531;

    /** - REM1247 */
    public static final int CH7532 = 7532;

    /** - REM1248 */
    public static final int CH7533 = 7533;

    /** - REM1249 */
    public static final int CH7534 = 7534;

    /** - REM124A */
    public static final int CH7535 = 7535;

    /** - REM124D */
    public static final int CH7536 = 7536;

    /** - REM1259 */
    public static final int CH7537 = 7537;

    /** - REM125A */
    public static final int CH7538 = 7538;

    /** - REM125B */
    public static final int CH7539 = 7539;

    /** - REM125D */
    public static final int CH7540 = 7540;

    /** - REM125E */
    public static final int CH7541 = 7541;

    /** - REM125F */
    public static final int CH7542 = 7542;

    /** - REM1260 */
    public static final int CH7543 = 7543;

    /** Pneumatische anti-blokkeer gestoord - REM1305 */
    public static final int CH7544 = 7544;

    /** Pneumatische anti-blokkeer gestoord - REM1307 */
    public static final int CH7545 = 7545;

    /** EP-rem gestoord - REM1308 */
    public static final int CH7546 = 7546;

    /** EP-rem gestoord - REM1309 */
    public static final int CH7547 = 7547;

    /** EP-rem gestoord - REM130B */
    public static final int CH7548 = 7548;

    /** Uitval luchtveer - REM130C */
    public static final int CH7549 = 7549;

    /** Pneumatische rem gestoord - REM130D */
    public static final int CH7550 = 7550;

    /** Snelrem gestoord - REM130E */
    public static final int CH7551 = 7551;

    /** Pneumatische rem niet gelost - REM130F */
    public static final int CH7552 = 7552;

    /** Pneumatische rem gestoord - REM1310 */
    public static final int CH7553 = 7553;

    /** EP-rem gestoord - REM1317 */
    public static final int CH7554 = 7554;

    /** Pneumatische rem gestoord - REM1318 */
    public static final int CH7555 = 7555;

    /** Pneumatische rem gestoord - REM131A */
    public static final int CH7556 = 7556;

    /** EP-rem vergrendeld - REM131C */
    public static final int CH7557 = 7557;

    /** - REM1321 */
    public static final int CH7558 = 7558;

    /** - REM1322 */
    public static final int CH7559 = 7559;

    /** - REM1323 */
    public static final int CH7560 = 7560;

    /** - REM1324 */
    public static final int CH7561 = 7561;

    /** - REM1325 */
    public static final int CH7562 = 7562;

    /** - REM1326 */
    public static final int CH7563 = 7563;

    /** - REM1327 */
    public static final int CH7564 = 7564;

    /** - REM1328 */
    public static final int CH7565 = 7565;

    /** - REM1329 */
    public static final int CH7566 = 7566;

    /** - REM132A */
    public static final int CH7567 = 7567;

    /** - REM132B */
    public static final int CH7568 = 7568;

    /** - REM132C */
    public static final int CH7569 = 7569;

    /** - REM132E */
    public static final int CH7570 = 7570;

    /** - REM132F */
    public static final int CH7571 = 7571;

    /** - REM1336 */
    public static final int CH7572 = 7572;

    /** - REM1337 */
    public static final int CH7573 = 7573;

    /** - REM1338 */
    public static final int CH7574 = 7574;

    /** - REM133A */
    public static final int CH7575 = 7575;

    /** - REM133B */
    public static final int CH7576 = 7576;

    /** - REM133C */
    public static final int CH7577 = 7577;

    /** - REM133D */
    public static final int CH7578 = 7578;

    /** - REM133E */
    public static final int CH7579 = 7579;

    /** - REM1341 */
    public static final int CH7580 = 7580;

    /** - REM1342 */
    public static final int CH7581 = 7581;

    /** - REM1345 */
    public static final int CH7582 = 7582;

    /** - REM1346 */
    public static final int CH7583 = 7583;

    /** - REM1347 */
    public static final int CH7584 = 7584;

    /** - REM1348 */
    public static final int CH7585 = 7585;

    /** - REM1349 */
    public static final int CH7586 = 7586;

    /** - REM134A */
    public static final int CH7587 = 7587;

    /** - REM134D */
    public static final int CH7588 = 7588;

    /** - REM1359 */
    public static final int CH7589 = 7589;

    /** - REM135A */
    public static final int CH7590 = 7590;

    /** - REM135D */
    public static final int CH7591 = 7591;

    /** - REM135E */
    public static final int CH7592 = 7592;

    /** - REM135F */
    public static final int CH7593 = 7593;

    /** - REM1360 */
    public static final int CH7594 = 7594;

    /** Pneumatische anti-blokkeer gestoord - REM1405 */
    public static final int CH7595 = 7595;

    /** Pneumatische anti-blokkeer gestoord - REM1407 */
    public static final int CH7596 = 7596;

    /** EP-rem gestoord - REM1408 */
    public static final int CH7597 = 7597;

    /** EP-rem gestoord - REM1409 */
    public static final int CH7598 = 7598;

    /** EP-rem gestoord - REM140B */
    public static final int CH7599 = 7599;

    /** Uitval luchtveer - REM140C */
    public static final int CH7600 = 7600;

    /** Pneumatische rem gestoord - REM140D */
    public static final int CH7601 = 7601;

    /** Snelrem gestoord - REM140E */
    public static final int CH7602 = 7602;

    /** Pneumatische rem niet gelost - REM140F */
    public static final int CH7603 = 7603;

    /** Pneumatische rem gestoord - REM1410 */
    public static final int CH7604 = 7604;

    /** EP-rem gestoord - REM1417 */
    public static final int CH7605 = 7605;

    /** Pneumatische rem gestoord - REM1418 */
    public static final int CH7606 = 7606;

    /** Pneumatische rem gestoord - REM141A */
    public static final int CH7607 = 7607;

    /** EP-rem vergrendeld - REM141C */
    public static final int CH7608 = 7608;

    /** - REM1421 */
    public static final int CH7609 = 7609;

    /** - REM1422 */
    public static final int CH7610 = 7610;

    /** - REM1423 */
    public static final int CH7611 = 7611;

    /** - REM1424 */
    public static final int CH7612 = 7612;

    /** - REM1425 */
    public static final int CH7613 = 7613;

    /** - REM1426 */
    public static final int CH7614 = 7614;

    /** - REM1427 */
    public static final int CH7615 = 7615;

    /** - REM1428 */
    public static final int CH7616 = 7616;

    /** - REM1429 */
    public static final int CH7617 = 7617;

    /** - REM142A */
    public static final int CH7618 = 7618;

    /** - REM142B */
    public static final int CH7619 = 7619;

    /** - REM142C */
    public static final int CH7620 = 7620;

    /** - REM142E */
    public static final int CH7621 = 7621;

    /** - REM142F */
    public static final int CH7622 = 7622;

    /** - REM1436 */
    public static final int CH7623 = 7623;

    /** - REM1437 */
    public static final int CH7624 = 7624;

    /** - REM1438 */
    public static final int CH7625 = 7625;

    /** - REM143A */
    public static final int CH7626 = 7626;

    /** - REM143B */
    public static final int CH7627 = 7627;

    /** - REM143C */
    public static final int CH7628 = 7628;

    /** - REM143D */
    public static final int CH7629 = 7629;

    /** - REM143E */
    public static final int CH7630 = 7630;

    /** - REM1441 */
    public static final int CH7631 = 7631;

    /** - REM1442 */
    public static final int CH7632 = 7632;

    /** - REM1445 */
    public static final int CH7633 = 7633;

    /** - REM1446 */
    public static final int CH7634 = 7634;

    /** - REM1447 */
    public static final int CH7635 = 7635;

    /** - REM1448 */
    public static final int CH7636 = 7636;

    /** - REM1449 */
    public static final int CH7637 = 7637;

    /** - REM144A */
    public static final int CH7638 = 7638;

    /** - REM144D */
    public static final int CH7639 = 7639;

    /** - REM1459 */
    public static final int CH7640 = 7640;

    /** - REM145A */
    public static final int CH7641 = 7641;

    /** - REM145D */
    public static final int CH7642 = 7642;

    /** - REM145E */
    public static final int CH7643 = 7643;

    /** - REM145F */
    public static final int CH7644 = 7644;

    /** - REM1460 */
    public static final int CH7645 = 7645;

    /** Pneumatische anti-blokkeer gestoord - REM1505 */
    public static final int CH7646 = 7646;

    /** Pneumatische anti-blokkeer gestoord - REM1507 */
    public static final int CH7647 = 7647;

    /** EP-rem gestoord - REM1508 */
    public static final int CH7648 = 7648;

    /** EP-rem gestoord - REM1509 */
    public static final int CH7649 = 7649;

    /** EP-rem gestoord - REM150B */
    public static final int CH7650 = 7650;

    /** Uitval luchtveer - REM150C */
    public static final int CH7651 = 7651;

    /** Pneumatische rem gestoord - REM150D */
    public static final int CH7652 = 7652;

    /** Snelrem gestoord - REM150E */
    public static final int CH7653 = 7653;

    /** Pneumatische rem niet gelost - REM150F */
    public static final int CH7654 = 7654;

    /** Pneumatische rem gestoord - REM1510 */
    public static final int CH7655 = 7655;

    /** EP-rem gestoord - REM1517 */
    public static final int CH7656 = 7656;

    /** Pneumatische rem gestoord - REM1518 */
    public static final int CH7657 = 7657;

    /** Pneumatische rem gestoord - REM151A */
    public static final int CH7658 = 7658;

    /** EP-rem vergrendeld - REM151C */
    public static final int CH7659 = 7659;

    /** - REM1521 */
    public static final int CH7660 = 7660;

    /** - REM1522 */
    public static final int CH7661 = 7661;

    /** - REM1523 */
    public static final int CH7662 = 7662;

    /** - REM1524 */
    public static final int CH7663 = 7663;

    /** - REM1525 */
    public static final int CH7664 = 7664;

    /** - REM1526 */
    public static final int CH7665 = 7665;

    /** - REM1527 */
    public static final int CH7666 = 7666;

    /** - REM1528 */
    public static final int CH7667 = 7667;

    /** - REM1529 */
    public static final int CH7668 = 7668;

    /** - REM152A */
    public static final int CH7669 = 7669;

    /** - REM152B */
    public static final int CH7670 = 7670;

    /** - REM152C */
    public static final int CH7671 = 7671;

    /** - REM152E */
    public static final int CH7672 = 7672;

    /** - REM152F */
    public static final int CH7673 = 7673;

    /** - REM1536 */
    public static final int CH7674 = 7674;

    /** - REM1537 */
    public static final int CH7675 = 7675;

    /** - REM1538 */
    public static final int CH7676 = 7676;

    /** - REM153A */
    public static final int CH7677 = 7677;

    /** - REM153B */
    public static final int CH7678 = 7678;

    /** - REM153C */
    public static final int CH7679 = 7679;

    /** - REM153D */
    public static final int CH7680 = 7680;

    /** - REM153E */
    public static final int CH7681 = 7681;

    /** - REM1541 */
    public static final int CH7682 = 7682;

    /** - REM1542 */
    public static final int CH7683 = 7683;

    /** - REM1545 */
    public static final int CH7684 = 7684;

    /** - REM1546 */
    public static final int CH7685 = 7685;

    /** - REM1547 */
    public static final int CH7686 = 7686;

    /** - REM1548 */
    public static final int CH7687 = 7687;

    /** - REM1549 */
    public static final int CH7688 = 7688;

    /** - REM154A */
    public static final int CH7689 = 7689;

    /** - REM154D */
    public static final int CH7690 = 7690;

    /** - REM1559 */
    public static final int CH7691 = 7691;

    /** - REM155A */
    public static final int CH7692 = 7692;

    /** - REM155D */
    public static final int CH7693 = 7693;

    /** - REM155E */
    public static final int CH7694 = 7694;

    /** - REM155F */
    public static final int CH7695 = 7695;

    /** - REM1560 */
    public static final int CH7696 = 7696;

    /** Pneumatische anti-blokkeer gestoord - REM1605 */
    public static final int CH7697 = 7697;

    /** Pneumatische anti-blokkeer gestoord - REM1607 */
    public static final int CH7698 = 7698;

    /** EP-rem gestoord - REM1608 */
    public static final int CH7699 = 7699;

    /** EP-rem gestoord - REM1609 */
    public static final int CH7700 = 7700;

    /** EP-rem gestoord - REM160B */
    public static final int CH7701 = 7701;

    /** Uitval luchtveer - REM160C */
    public static final int CH7702 = 7702;

    /** Pneumatische rem gestoord - REM160D */
    public static final int CH7703 = 7703;

    /** Snelrem gestoord - REM160E */
    public static final int CH7704 = 7704;

    /** Pneumatische rem niet gelost - REM160F */
    public static final int CH7705 = 7705;

    /** Pneumatische rem gestoord - REM1610 */
    public static final int CH7706 = 7706;

    /** Parkeerrem lost niet - REM1611 */
    public static final int CH7707 = 7707;

    /** Parkeerrem niet gelost - REM1612 */
    public static final int CH7708 = 7708;

    /** Parkeerrem wordt niet ingeschakeld - REM1613 */
    public static final int CH7709 = 7709;

    /** EP-rem gestoord - REM1617 */
    public static final int CH7710 = 7710;

    /** Pneumatische rem gestoord - REM1618 */
    public static final int CH7711 = 7711;

    /** Anti-blokkeer gestoord - REM1619 */
    public static final int CH7712 = 7712;

    /** Pneumatische rem gestoord - REM161A */
    public static final int CH7713 = 7713;

    /** Parkeerrem diagnose gestoord - REM161B */
    public static final int CH7714 = 7714;

    /** EP-rem afgesloten - REM161C */
    public static final int CH7715 = 7715;

    /** Parkeerrem afgesloten - REM161D */
    public static final int CH7716 = 7716;

    /** - REM1621 */
    public static final int CH7717 = 7717;

    /** - REM1622 */
    public static final int CH7718 = 7718;

    /** - REM1623 */
    public static final int CH7719 = 7719;

    /** - REM1624 */
    public static final int CH7720 = 7720;

    /** - REM1625 */
    public static final int CH7721 = 7721;

    /** - REM1626 */
    public static final int CH7722 = 7722;

    /** - REM1627 */
    public static final int CH7723 = 7723;

    /** - REM1628 */
    public static final int CH7724 = 7724;

    /** - REM1629 */
    public static final int CH7725 = 7725;

    /** - REM162A */
    public static final int CH7726 = 7726;

    /** - REM162B */
    public static final int CH7727 = 7727;

    /** - REM162C */
    public static final int CH7728 = 7728;

    /** - REM162E */
    public static final int CH7729 = 7729;

    /** - REM162F */
    public static final int CH7730 = 7730;

    /** - REM1632 */
    public static final int CH7731 = 7731;

    /** - REM1636 */
    public static final int CH7732 = 7732;

    /** - REM1637 */
    public static final int CH7733 = 7733;

    /** - REM1638 */
    public static final int CH7734 = 7734;

    /** - REM163A */
    public static final int CH7735 = 7735;

    /** - REM163B */
    public static final int CH7736 = 7736;

    /** - REM163C */
    public static final int CH7737 = 7737;

    /** - REM163D */
    public static final int CH7738 = 7738;

    /** - REM163E */
    public static final int CH7739 = 7739;

    /** - REM163F */
    public static final int CH7740 = 7740;

    /** - REM1640 */
    public static final int CH7741 = 7741;

    /** - REM1641 */
    public static final int CH7742 = 7742;

    /** - REM1642 */
    public static final int CH7743 = 7743;

    /** - REM1643 */
    public static final int CH7744 = 7744;

    /** - REM1644 */
    public static final int CH7745 = 7745;

    /** - REM1645 */
    public static final int CH7746 = 7746;

    /** - REM1646 */
    public static final int CH7747 = 7747;

    /** - REM1647 */
    public static final int CH7748 = 7748;

    /** - REM1648 */
    public static final int CH7749 = 7749;

    /** - REM1649 */
    public static final int CH7750 = 7750;

    /** - REM164A */
    public static final int CH7751 = 7751;

    /** - REM164D */
    public static final int CH7752 = 7752;

    /** - REM1659 */
    public static final int CH7753 = 7753;

    /** - REM165A */
    public static final int CH7754 = 7754;

    /** - REM165D */
    public static final int CH7755 = 7755;

    /** - REM165E */
    public static final int CH7756 = 7756;

    /** - REM165F */
    public static final int CH7757 = 7757;

    /** - REM1660 */
    public static final int CH7758 = 7758;

    /** Pneumatische anti-blokkeer gestoord - REM1705 */
    public static final int CH7759 = 7759;

    /** Pneumatische anti-blokkeer gestoord - REM1707 */
    public static final int CH7760 = 7760;

    /** EP-rem gestoord - REM1708 */
    public static final int CH7761 = 7761;

    /** EP-rem gestoord - REM1709 */
    public static final int CH7762 = 7762;

    /** EP-rem gestoord - REM170B */
    public static final int CH7763 = 7763;

    /** Uitval luchtveer - REM170C */
    public static final int CH7764 = 7764;

    /** Pneumatische rem gestoord - REM170D */
    public static final int CH7765 = 7765;

    /** Snelrem gestoord - REM170E */
    public static final int CH7766 = 7766;

    /** Pneumatische rem niet gelost - REM170F */
    public static final int CH7767 = 7767;

    /** Pneumatische rem gestoord - REM1710 */
    public static final int CH7768 = 7768;

    /** Parkeerrem lost niet - REM1711 */
    public static final int CH7769 = 7769;

    /** Parkeerrem niet gelost - REM1712 */
    public static final int CH7770 = 7770;

    /** Parkeerrem wordt niet ingeschakeld - REM1713 */
    public static final int CH7771 = 7771;

    /** EP-rem gestoord - REM1717 */
    public static final int CH7772 = 7772;

    /** Pneumatische rem gestoord - REM1718 */
    public static final int CH7773 = 7773;

    /** Anti-blokkeer gestoord - REM1719 */
    public static final int CH7774 = 7774;

    /** Pneumatische rem gestoord - REM171A */
    public static final int CH7775 = 7775;

    /** Parkeerrem diagnose gestoord - REM171B */
    public static final int CH7776 = 7776;

    /** EP-rem afgesloten - REM171C */
    public static final int CH7777 = 7777;

    /** Parkeerrem afgesloten - REM171D */
    public static final int CH7778 = 7778;

    /** - REM1721 */
    public static final int CH7779 = 7779;

    /** - REM1722 */
    public static final int CH7780 = 7780;

    /** - REM1723 */
    public static final int CH7781 = 7781;

    /** - REM1724 */
    public static final int CH7782 = 7782;

    /** - REM1725 */
    public static final int CH7783 = 7783;

    /** - REM1726 */
    public static final int CH7784 = 7784;

    /** - REM1727 */
    public static final int CH7785 = 7785;

    /** - REM1728 */
    public static final int CH7786 = 7786;

    /** - REM1729 */
    public static final int CH7787 = 7787;

    /** - REM172A */
    public static final int CH7788 = 7788;

    /** - REM172B */
    public static final int CH7789 = 7789;

    /** - REM172C */
    public static final int CH7790 = 7790;

    /** - REM172E */
    public static final int CH7791 = 7791;

    /** - REM172F */
    public static final int CH7792 = 7792;

    /** - REM1732 */
    public static final int CH7793 = 7793;

    /** - REM1736 */
    public static final int CH7794 = 7794;

    /** - REM1737 */
    public static final int CH7795 = 7795;

    /** - REM1738 */
    public static final int CH7796 = 7796;

    /** - REM173A */
    public static final int CH7797 = 7797;

    /** - REM173B */
    public static final int CH7798 = 7798;

    /** - REM173C */
    public static final int CH7799 = 7799;

    /** - REM173D */
    public static final int CH7800 = 7800;

    /** - REM173E */
    public static final int CH7801 = 7801;

    /** - REM173F */
    public static final int CH7802 = 7802;

    /** - REM1740 */
    public static final int CH7803 = 7803;

    /** - REM1741 */
    public static final int CH7804 = 7804;

    /** - REM1742 */
    public static final int CH7805 = 7805;

    /** - REM1743 */
    public static final int CH7806 = 7806;

    /** - REM1744 */
    public static final int CH7807 = 7807;

    /** - REM1745 */
    public static final int CH7808 = 7808;

    /** - REM1746 */
    public static final int CH7809 = 7809;

    /** - REM1747 */
    public static final int CH7810 = 7810;

    /** - REM1748 */
    public static final int CH7811 = 7811;

    /** - REM1749 */
    public static final int CH7812 = 7812;

    /** - REM174A */
    public static final int CH7813 = 7813;

    /** - REM174D */
    public static final int CH7814 = 7814;

    /** - REM1759 */
    public static final int CH7815 = 7815;

    /** - REM175A */
    public static final int CH7816 = 7816;

    /** - REM175D */
    public static final int CH7817 = 7817;

    /** - REM175E */
    public static final int CH7818 = 7818;

    /** - REM175F */
    public static final int CH7819 = 7819;

    /** - REM1760 */
    public static final int CH7820 = 7820;

    /** Parkeerremmen niet geactiveerd - REM803D */
    public static final int CH7821 = 7821;

    /** Parkeerremmen niet gelost - REM803E */
    public static final int CH7822 = 7822;

    /** Remstoring draaistel 1 - REM803F */
    public static final int CH7823 = 7823;

    /** Remstoring draaistel 2 - REM8040 */
    public static final int CH7824 = 7824;

    /** Remstoring draaistel 3 - REM8041 */
    public static final int CH7825 = 7825;

    /** Remstoring draaistel 4 - REM8042 */
    public static final int CH7826 = 7826;

    /** Remstoring draaistel 5 - REM8043 */
    public static final int CH7827 = 7827;

    /** Remstoring draaistel 6 - REM8044 */
    public static final int CH7828 = 7828;

    /** Remstoring draaistel 7 - REM8045 */
    public static final int CH7829 = 7829;

    /** Niet alle pneumatische remmen zijn gelost - REM8046 */
    public static final int CH7830 = 7830;

    /** Parkeerrem lost niet - REM8047 */
    public static final int CH7831 = 7831;

    /** Parkeerrem lost niet - REM8048 */
    public static final int CH7832 = 7832;

    /** Parkeerrem niet bekrachtigd - REM8049 */
    public static final int CH7833 = 7833;

    /** Parkeerrem niet bekrachtigd - REM804A */
    public static final int CH7834 = 7834;

    /** Signalering parkeerrem gelost niet betrouwbaar - REM804B */
    public static final int CH7835 = 7835;

    /** Signalering parkeerrem gelost niet betrouwbaar - REM804C */
    public static final int CH7836 = 7836;

    /** Tractie uitgevallen - TRC0118 */
    public static final int CH7837 = 7837;

    /** Tractie uitgevallen - TRC0119 */
    public static final int CH7838 = 7838;

    /** Tractie uitgevallen - TRC011A */
    public static final int CH7839 = 7839;

    /** Storing tractie - TRC011C */
    public static final int CH7840 = 7840;

    /** Storing tractie - TRC011F */
    public static final int CH7841 = 7841;

    /** Storing tractie - TRC0120 */
    public static final int CH7842 = 7842;

    /** Storing tractie - TRC0121 */
    public static final int CH7843 = 7843;

    /** Tractie uitgevallen - TRC0122 */
    public static final int CH7844 = 7844;

    /** Tractie uitgevallen - TRC0124 */
    public static final int CH7845 = 7845;

    /** Storing tractie - TRC0125 */
    public static final int CH7846 = 7846;

    /** Fout in draairichting tractiemotor/toerenopnemer - TRC0127 */
    public static final int CH7847 = 7847;

    /** Temperatuur te hoog - TRC0128 */
    public static final int CH7848 = 7848;

    /** Maximale tractiestroom bereikt - TRC0129 */
    public static final int CH7849 = 7849;

    /** Storing tractie - TRC012C */
    public static final int CH7850 = 7850;

    /** Storing tractie - TRC012D */
    public static final int CH7851 = 7851;

    /** Storing tractie - TRC012E */
    public static final int CH7852 = 7852;

    /** Tractie uitgevallen - TRC012F */
    public static final int CH7853 = 7853;

    /** Tractie uitgevallen - TRC0130 */
    public static final int CH7854 = 7854;

    /** Tractie uitgevallen - TRC0131 */
    public static final int CH7855 = 7855;

    /** Tractie uitgevallen - TRC0132 */
    public static final int CH7856 = 7856;

    /** Tractie uitgevallen - TRC0133 */
    public static final int CH7857 = 7857;

    /** Tractie uitgevallen - TRC0137 */
    public static final int CH7858 = 7858;

    /** Tractie uitgevallen - TRC0138 */
    public static final int CH7859 = 7859;

    /** Storing tractie - TRC0139 */
    public static final int CH7860 = 7860;

    /** Tractie uitgevallen - TRC013A */
    public static final int CH7861 = 7861;

    /** Tractie uitgevallen - TRC013B */
    public static final int CH7862 = 7862;

    /** Storing tractie - TRC013C */
    public static final int CH7863 = 7863;

    /** Storing tractie - TRC013D */
    public static final int CH7864 = 7864;

    /** Storing tractie - TRC013E */
    public static final int CH7865 = 7865;

    /** Storing tractie - TRC013F */
    public static final int CH7866 = 7866;

    /** Storing tractie - TRC0140 */
    public static final int CH7867 = 7867;

    /** Storing TCU-stoorstroomdetector (UWDS) - TRC0161 */
    public static final int CH7868 = 7868;

    /** Tractie uitgevallen - TRC016A */
    public static final int CH7869 = 7869;

    /** Batterij TCU leeg - TRC016B */
    public static final int CH7870 = 7870;

    /** Storing Ventilator Tractiesturing - TRC016C */
    public static final int CH7871 = 7871;

    /** Storing centrale eenheid TCU - TRC016D */
    public static final int CH7872 = 7872;

    /** Tractie uitgevallen - TRC017B */
    public static final int CH7873 = 7873;

    /** Storing TCU 110 V-voeding - TRC017C */
    public static final int CH7874 = 7874;

    /** Storing TCU 110 V-uitgang - TRC0183 */
    public static final int CH7875 = 7875;

    /** Tractie uitgevallen - TRC0184 */
    public static final int CH7876 = 7876;

    /** Storing communicatie tractiesturing - TRC0185 */
    public static final int CH7877 = 7877;

    /** Storing communicatie tractiesturing - TRC0187 */
    public static final int CH7878 = 7878;

    /** Storing communicatie rembesturing - TRC0188 */
    public static final int CH7879 = 7879;

    /** Storing tractie - TRC0192 */
    public static final int CH7880 = 7880;

    /** Storing tractie - TRC0193 */
    public static final int CH7881 = 7881;

    /** Storing tractie - TRC0194 */
    public static final int CH7882 = 7882;

    /** Storing tractie - TRC0195 */
    public static final int CH7883 = 7883;

    /** Storing tractie - TRC0196 */
    public static final int CH7884 = 7884;

    /** Storing tractie - TRC0197 */
    public static final int CH7885 = 7885;

    /** Storing tractie - TRC0198 */
    public static final int CH7886 = 7886;

    /** Storing tractie - TRC019A */
    public static final int CH7887 = 7887;

    /** Storing tractie - TRC019B */
    public static final int CH7888 = 7888;

    /** Tractie uitgevallen - TRC019D */
    public static final int CH7889 = 7889;

    /** Stoorstroomdetector is geactiveerd - TRC01A4 */
    public static final int CH7890 = 7890;

    /** Koellichaam is heet - TRC01AA */
    public static final int CH7891 = 7891;

    /** Koelmiddel is heet - TRC01AC */
    public static final int CH7892 = 7892;

    /** Netsmoorspoel is heet - TRC01AE */
    public static final int CH7893 = 7893;

    /** Tractiemotor is heet - TRC01B0 */
    public static final int CH7894 = 7894;

    /** Remweerstand is heet - TRC01B4 */
    public static final int CH7895 = 7895;

    /** Storing tractie - TRC01B5 */
    public static final int CH7896 = 7896;

    /** Maximaal toerental - TRC01BC */
    public static final int CH7897 = 7897;

    /** Wieldiameterverschil te groot (waarschuwing) - TRC01BE */
    public static final int CH7898 = 7898;

    /** Wieldiameterverschil te groot (fout) - TRC01BF */
    public static final int CH7899 = 7899;

    /** Storing communicatie tractiesturing - TRC01C4 */
    public static final int CH7900 = 7900;

    /** Tractiesturing gestoord - TRC01DF */
    public static final int CH7901 = 7901;

    /** Storing tractie - TRC01E2 */
    public static final int CH7902 = 7902;

    /** Storing tractie - TRC01E5 */
    public static final int CH7903 = 7903;

    /** Koelmiddelcirculatie laag (uitschakeling) - TRC01E8 */
    public static final int CH7904 = 7904;

    /** Storing tractie - TRC01ED */
    public static final int CH7905 = 7905;

    /** Storing tractie - TRC01EE */
    public static final int CH7906 = 7906;

    /** Koelmiddelniveau te laag (waarschuwing) - TRC01EF */
    public static final int CH7907 = 7907;

    /** Koelmiddelniveau zeer laag (uitschakeling) - TRC01F0 */
    public static final int CH7908 = 7908;

    /** Koelmiddelfilter koelsysteem vervangen - TRC01F5 */
    public static final int CH7909 = 7909;

    /** Tractie uitgevallen - TRC01F7 */
    public static final int CH7910 = 7910;

    /** Treinconfiguratie niet correct - TRC01F9 */
    public static final int CH7911 = 7911;

    /** Tractieomvormer continu buiten bedrijf - TRC01FF */
    public static final int CH7912 = 7912;

    /** Aandrijving tractie uitgevallen - TRC0218 */
    public static final int CH7913 = 7913;

    /** Aandrijving tractie uitgevallen - TRC0219 */
    public static final int CH7914 = 7914;

    /** Aandrijving tractie uitgevallen - TRC021A */
    public static final int CH7915 = 7915;

    /** Storing aandrijving tractie - TRC021C */
    public static final int CH7916 = 7916;

    /** Storing aandrijving tractie - TRC021F */
    public static final int CH7917 = 7917;

    /** Storing aandrijving tractie - TRC0220 */
    public static final int CH7918 = 7918;

    /** Storing aandrijving tractie - TRC0221 */
    public static final int CH7919 = 7919;

    /** Aandrijving tractie uitgevallen - TRC0222 */
    public static final int CH7920 = 7920;

    /** Aandrijving tractie uitgevallen - TRC0224 */
    public static final int CH7921 = 7921;

    /** Storing aandrijving tractie - TRC0225 */
    public static final int CH7922 = 7922;

    /** Fout in draairichting tractiemotor/toerenopnemer - TRC0227 */
    public static final int CH7923 = 7923;

    /** Temperatuur te hoog - TRC0228 */
    public static final int CH7924 = 7924;

    /** Maximale tractiestroom bereikt - TRC0229 */
    public static final int CH7925 = 7925;

    /** Storing aandrijving tractie - TRC022C */
    public static final int CH7926 = 7926;

    /** Storing aandrijving tractie - TRC022D */
    public static final int CH7927 = 7927;

    /** Storing aandrijving tractie - TRC022E */
    public static final int CH7928 = 7928;

    /** Aandrijving tractie uitgevallen - TRC022F */
    public static final int CH7929 = 7929;

    /** Aandrijving tractie uitgevallen - TRC0230 */
    public static final int CH7930 = 7930;

    /** Aandrijving tractie uitgevallen - TRC0231 */
    public static final int CH7931 = 7931;

    /** Aandrijving tractie uitgevallen - TRC0232 */
    public static final int CH7932 = 7932;

    /** Aandrijving tractie uitgevallen - TRC0233 */
    public static final int CH7933 = 7933;

    /** Aandrijving tractie uitgevallen - TRC0237 */
    public static final int CH7934 = 7934;

    /** Aandrijving tractie uitgevallen - TRC0238 */
    public static final int CH7935 = 7935;

    /** Storing aandrijving tractie - TRC0239 */
    public static final int CH7936 = 7936;

    /** Aandrijving tractie uitgevallen - TRC023A */
    public static final int CH7937 = 7937;

    /** Aandrijving tractie uitgevallen - TRC023B */
    public static final int CH7938 = 7938;

    /** Storing aandrijving tractie - TRC023C */
    public static final int CH7939 = 7939;

    /** Storing aandrijving tractie - TRC023D */
    public static final int CH7940 = 7940;

    /** Storing aandrijving tractie - TRC023E */
    public static final int CH7941 = 7941;

    /** Storing aandrijving tractie - TRC023F */
    public static final int CH7942 = 7942;

    /** Storing aandrijving tractie - TRC0240 */
    public static final int CH7943 = 7943;

    /** Storing TBE-stoorstroomdetector (UWDS) - TRC0261 */
    public static final int CH7944 = 7944;

    /** Aandrijving tractie uitgevallen - TRC026A */
    public static final int CH7945 = 7945;

    /** Batterij TBE leeg - TRC026B */
    public static final int CH7946 = 7946;

    /** Storing Ventilator Tractiesturing - TRC026C */
    public static final int CH7947 = 7947;

    /** Storing centrale eenheid TBE - TRC026D */
    public static final int CH7948 = 7948;

    /** Aandrijving tractie uitgevallen - TRC027B */
    public static final int CH7949 = 7949;

    /** Storing TBE 110 V-voeding - TRC027C */
    public static final int CH7950 = 7950;

    /** Storing TBE 110 V-uitgang - TRC0283 */
    public static final int CH7951 = 7951;

    /** Aandrijving tractie uitgevallen - TRC0284 */
    public static final int CH7952 = 7952;

    /** Storing communicatie tractiesturing - TRC0285 */
    public static final int CH7953 = 7953;

    /** Storing communicatie tractiesturing - TRC0287 */
    public static final int CH7954 = 7954;

    /** Storing communicatie rembesturing - TRC0288 */
    public static final int CH7955 = 7955;

    /** Storing aandrijving tractie - TRC0292 */
    public static final int CH7956 = 7956;

    /** Storing aandrijving tractie - TRC0293 */
    public static final int CH7957 = 7957;

    /** Storing aandrijving tractie - TRC0294 */
    public static final int CH7958 = 7958;

    /** Storing aandrijving tractie - TRC0295 */
    public static final int CH7959 = 7959;

    /** Storing aandrijving tractie - TRC0296 */
    public static final int CH7960 = 7960;

    /** Storing aandrijving tractie - TRC0297 */
    public static final int CH7961 = 7961;

    /** Storing aandrijving tractie - TRC0298 */
    public static final int CH7962 = 7962;

    /** Storing aandrijving tractie - TRC029A */
    public static final int CH7963 = 7963;

    /** Storing aandrijving tractie - TRC029B */
    public static final int CH7964 = 7964;

    /** Aandrijving tractie uitgevallen - TRC029D */
    public static final int CH7965 = 7965;

    /** Stoorstroomdetector is geactiveerd - TRC02A4 */
    public static final int CH7966 = 7966;

    /** Koellichaam is heet - TRC02AA */
    public static final int CH7967 = 7967;

    /** Koelmiddel is heet - TRC02AC */
    public static final int CH7968 = 7968;

    /** Netsmoorspoel is heet - TRC02AE */
    public static final int CH7969 = 7969;

    /** Tractiemotor is heet - TRC02B0 */
    public static final int CH7970 = 7970;

    /** Remweerstand is heet - TRC02B4 */
    public static final int CH7971 = 7971;

    /** Storing aandrijving tractie - TRC02B5 */
    public static final int CH7972 = 7972;

    /** Maximaal toerental - TRC02BC */
    public static final int CH7973 = 7973;

    /** Wieldiameterverschil te groot (waarschuwing) - TRC02BE */
    public static final int CH7974 = 7974;

    /** Wieldiameterverschil te groot (fout) - TRC02BF */
    public static final int CH7975 = 7975;

    /** Storing communicatie tractiesturing - TRC02C4 */
    public static final int CH7976 = 7976;

    /** Bedrijfstoestand niet correct - TRC02DF */
    public static final int CH7977 = 7977;

    /** Storing aandrijving tractie - TRC02E2 */
    public static final int CH7978 = 7978;

    /** Storing aandrijving tractie - TRC02E5 */
    public static final int CH7979 = 7979;

    /** Koelmiddelcirculatie laag (uitschakeling) - TRC02E8 */
    public static final int CH7980 = 7980;

    /** Storing aandrijving tractie - TRC02ED */
    public static final int CH7981 = 7981;

    /** Storing aandrijving tractie - TRC02EE */
    public static final int CH7982 = 7982;

    /** Koelmiddelniveau te laag (waarschuwing) - TRC02EF */
    public static final int CH7983 = 7983;

    /** Koelmiddelniveau zeer laag (uitschakeling) - TRC02F0 */
    public static final int CH7984 = 7984;

    /** Koelmiddelfilter koelsysteem vervangen - TRC02F5 */
    public static final int CH7985 = 7985;

    /** Aandrijving tractie uitgevallen - TRC02F7 */
    public static final int CH7986 = 7986;

    /** Treinconfiguratie niet correct - TRC02F9 */
    public static final int CH7987 = 7987;

    /** Tractieomvormer continu buiten bedrijf - TRC02FF */
    public static final int CH7988 = 7988;

    /** Storing tractie - TRC804F */
    public static final int CH7989 = 7989;

    /** Storing tractie - TRC8050 */
    public static final int CH7990 = 7990;

    /** BCU beperkt ED-remvermogen - TRC9047 */
    public static final int CH7991 = 7991;

    /** BCU schakelt ED-remvermogen uit - TRC9048 */
    public static final int CH7992 = 7992;

    /** Maximale netstroom beperkt het vermogen - TRC9049 */
    public static final int CH7993 = 7993;

    /** Lage netspanning beperkt het vermogen - TRC904A */
    public static final int CH7994 = 7994;

    /** Hoge netspanning beperkt het vermogen - TRC904B */
    public static final int CH7995 = 7995;

    /** RBE beperkt ED-remvermogen - TRC904C */
    public static final int CH7996 = 7996;

    /** RBE schakelt ED-remvermogen uit - TRC904D */
    public static final int CH7997 = 7997;

    /** Maximale netstroom beperkt het vermogen - TRC904E */
    public static final int CH7998 = 7998;

    /** Lage netspanning beperkt het vermogen - TRC904F */
    public static final int CH7999 = 7999;

    /** Hoge netspanning beperkt het vermogen - TRC9050 */
    public static final int CH8000 = 8000;

    /** Antiblokkeer-/slipbeveiliging beperkt het vermogen - TRC9066 */
    public static final int CH8001 = 8001;

    /** Antiblokkeer-/slipbewveiliging beperkt - TRC9067 */
    public static final int CH8002 = 8002;

    /** Noodverlichting uitgevallen - VRL8051 */
    public static final int CH8003 = 8003;

    /** Noodverlichting uitgevallen - VRL8052 */
    public static final int CH8004 = 8004;

    /** Geen sluitsein - VRL8053 */
    public static final int CH8005 = 8005;

    /** Geen sluitsein - VRL8054 */
    public static final int CH8006 = 8006;

    /** Schakelaar â€˜frontseinenâ€˜ uitgeschakeld - VRL8055 */
    public static final int CH8007 = 8007;

    /** Schakelaar â€˜frontseinenâ€˜ uitgeschakeld - VRL8056 */
    public static final int CH8008 = 8008;

    /** CCU-D Mobad batterij spanning te gering - BSS8021 */
    public static final int CH8009 = 8009;

    /** CCU-D Mobad batterij spanning gering - BSS906A */
    public static final int CH8010 = 8010;

    /** Lamp DD brandt onterecht - CAB701E */
    public static final int CH8011 = 8011;

    /** HMI uitgevallen - COM0109 */
    public static final int CH8012 = 8012;

    /** Uitval reisinformatiesysteem - COM010A */
    public static final int CH8013 = 8013;

    /** Uitval reisinformatiesysteem - COM010B */
    public static final int CH8014 = 8014;

    /** Uitval reisinformatiesysteem - COM010C */
    public static final int CH8015 = 8015;

    /** Uitval reisinformatiesysteem - COM010D */
    public static final int CH8016 = 8016;

    /** Uitval reisinformatiesysteem - COM010E */
    public static final int CH8017 = 8017;

    /** Uitval reisinformatiesysteem - COM010F */
    public static final int CH8018 = 8018;

    /** Uitval reisinformatiesysteem - COM0111 */
    public static final int CH8019 = 8019;

    /** Verbroken IP-verbinding WTB-Gateway - COM0112 */
    public static final int CH8020 = 8020;

    /** Snelheidsmeter defect - COM0113 */
    public static final int CH8021 = 8021;

    /** Snelheidsmeter defect - COM0114 */
    public static final int CH8022 = 8022;

    /** Batterijlader 1 uitgevallen - COM0316 */
    public static final int CH8023 = 8023;

    /** Batterijlader 2 uitgevallen - COM0416 */
    public static final int CH8024 = 8024;

    /** Verbroken IP-verbinding WTB-Gateway - COM0755 */
    public static final int CH8025 = 8025;

    /** Verbroken IP-verbinding CCU-O - COM0756 */
    public static final int CH8026 = 8026;

    /** Verbroken IP-verbinding CCU-O - COM0757 */
    public static final int CH8027 = 8027;

    /** Ringswitch verbinding onderbroken - COM0758 */
    public static final int CH8028 = 8028;

    /** Ringswitch verbinding onderbroken - COM0759 */
    public static final int CH8029 = 8029;

    /** Ringswitch verbinding onderbroken - COM075A */
    public static final int CH8030 = 8030;

    /** Ringswitch verbinding onderbroken - COM075B */
    public static final int CH8031 = 8031;

    /** Ringswitch verbinding onderbroken - COM075C */
    public static final int CH8032 = 8032;

    /** Ringswitch verbinding onderbroken - COM075D */
    public static final int CH8033 = 8033;

    /** Ringswitch verbinding onderbroken - COM075E */
    public static final int CH8034 = 8034;

    /** Ringswitch verbinding onderbroken - COM075F */
    public static final int CH8035 = 8035;

    /** Verbroken verbinding I/O-module - DIA022E */
    public static final int CH8036 = 8036;

    /** Parameter: Lengte fase tijd - DIA707E */
    public static final int CH8037 = 8037;

    /** Parameter: Aantal fasen per feedback package - DIA707F */
    public static final int CH8038 = 8038;

    /** Parameter: Package 1 starttijd minuten - DIA7080 */
    public static final int CH8039 = 8039;

    /** Parameter: Package 1 starttijd uren - DIA7081 */
    public static final int CH8040 = 8040;

    /** Parameter: Package 2 starttijd minuten - DIA7082 */
    public static final int CH8041 = 8041;

    /** Parameter: Package 2 starttijd uren - DIA7083 */
    public static final int CH8042 = 8042;

    /** sliding step 1 reserved 1 - DRN807E */
    public static final int CH8043 = 8043;

    /** sliding step 1 reserved 2 - DRN807F */
    public static final int CH8044 = 8044;

    /** sliding step 1 reserved 3 - DRN8080 */
    public static final int CH8045 = 8045;

    /** sliding step 1 reserved 4 - DRN8081 */
    public static final int CH8046 = 8046;

    /** sliding step 1 reserved 5 - DRN8082 */
    public static final int CH8047 = 8047;

    /** sliding step 1 reserved 6 - DRN8083 */
    public static final int CH8048 = 8048;

    /** sliding step 1 reserved 7 - DRN8084 */
    public static final int CH8049 = 8049;

    /** sliding step 1 reserved 8 - DRN8085 */
    public static final int CH8050 = 8050;

    /** sliding step 2 reserved 1 - DRN8086 */
    public static final int CH8051 = 8051;

    /** sliding step 2 reserved 2 - DRN8087 */
    public static final int CH8052 = 8052;

    /** sliding step 2 reserved 3 - DRN8088 */
    public static final int CH8053 = 8053;

    /** sliding step 2 reserved 4 - DRN8089 */
    public static final int CH8054 = 8054;

    /** sliding step 2 reserved 5 - DRN808A */
    public static final int CH8055 = 8055;

    /** sliding step 2 reserved 6 - DRN808B */
    public static final int CH8056 = 8056;

    /** sliding step 2 reserved 7 - DRN808C */
    public static final int CH8057 = 8057;

    /** sliding step 2 reserved 8 - DRN808D */
    public static final int CH8058 = 8058;

    /** sliding step 1 reserved 9 - DRN9072 */
    public static final int CH8059 = 8059;

    /** sliding step 1 reserved 10 - DRN9073 */
    public static final int CH8060 = 8060;

    /** sliding step 2 reserved 9 - DRN9074 */
    public static final int CH8061 = 8061;

    /** sliding step 2 reserved 10 - DRN9075 */
    public static final int CH8062 = 8062;

    /** Omvormer 1 voor hulpinstallaties uitgevallen - HSP0106 */
    public static final int CH8063 = 8063;

    /** Omvormer 2 voor hulpinstallaties uitgevallen - HSP0107 */
    public static final int CH8064 = 8064;

    /** Snelschakelaar uitgeschakeld - HSP801E */
    public static final int CH8065 = 8065;

    /** Stroomafnemer komt niet op - LVZ801F */
    public static final int CH8066 = 8066;

    /** HR-druk te laag - LVZ8020 */
    public static final int CH8067 = 8067;

    /** UWC 80% level indicator of the waste water treatment system - UWC806F */
    public static final int CH8068 = 8068;

    /** UWC 95% level indicator of the waste water treatment system - UWC8070 */
    public static final int CH8069 = 8069;

    /** UWC level of fresh water is too low - UWC8071 */
    public static final int CH8070 = 8070;

    /** UWC Call for aid - UWC8072 */
    public static final int CH8071 = 8071;

    /** UWC leaked waste water - UWC8073 */
    public static final int CH8072 = 8072;

    /** Toilet is defect - UWC8074 */
    public static final int CH8073 = 8073;

    /** UWC low voltage freeze draining - UWC8075 */
    public static final int CH8074 = 8074;

    /** UWC reserved 1 - UWC8076 */
    public static final int CH8075 = 8075;

    /** UWC reserved 2 - UWC8077 */
    public static final int CH8076 = 8076;

    /** UWC reserved 3 - UWC8078 */
    public static final int CH8077 = 8077;

    /** UWC reserved 4 - UWC8079 */
    public static final int CH8078 = 8078;

    /** UWC reserved 5 - UWC807A */
    public static final int CH8079 = 8079;

    /** UWC reserved 6 - UWC807B */
    public static final int CH8080 = 8080;

    /** UWC reserved 7 - UWC807C */
    public static final int CH8081 = 8081;

    /** UWC reserved 8 - UWC807D */
    public static final int CH8082 = 8082;

    /** UWC reserved 9 - UWC9070 */
    public static final int CH8083 = 8083;

    /** UWC reserved 10 - UWC9071 */
    public static final int CH8084 = 8084;

    /** Tractie afgeschakeld - DIA7023 */
    public static final int CH8085 = 8085;
    
    /** Laatste breedtegraad - VT_GPSLatitude */
    public static final int CH8086 = 8086;
    
    /** Laatste lengtegraad - VT_GPSLongitude */
    public static final int CH8087 = 8087;

    /** ATBvv-override blijft actief - BSS8096 */
    public static final int CH8088 = 8088;

    /** Drukknop ‘Meldlampen testen’ permanent IN - CAB0138 */
    public static final int CH8089 = 8089;

    /** Drukknop ‘Meldlampen testen’ permanent IN - CAB0139 */
    public static final int CH8090 = 8090;

    /** Drukknop ‘Meldlampen testen’ permanent IN - CAB0238 */
    public static final int CH8091 = 8091;

    /** Drukknop ‘Meldlampen testen’ permanent IN - CAB0239 */
    public static final int CH8092 = 8092;

    /** Line circuit breaker 91F01 for UWC electronic is released - DIA0468 */
    public static final int CH8093 = 8093;

    /** Line circuit breaker 63F05 for heating tapes is released - DIA0469 */
    public static final int CH8094 = 8094;

    /** Line circuit breaker 63F04 for hand dryer is released - DIA046A */
    public static final int CH8095 = 8095;

    /** Line circuit breaker 63F03 for heating and is released - DIA046B */
    public static final int CH8096 = 8096;

    /** Line circuit breaker 91F02 for bio reactor is released - DIA046C */
    public static final int CH8097 = 8097;

    /** Line circuit breaker 91F01 for UWC electronic is released - DIA0668 */
    public static final int CH8098 = 8098;

    /** Line circuit breaker 63F05 for heating tapes is released - DIA0669 */
    public static final int CH8099 = 8099;

    /** Line circuit breaker 63F04 for hand dryer is released - DIA066A */
    public static final int CH8100 = 8100;

    /** Line circuit breaker 63F03 for heating and is released - DIA066B */
    public static final int CH8101 = 8101;

    /** Line circuit breaker 63F06 for bio reactor is released - DIA066C */
    public static final int CH8102 = 8102;

    /** Ondergrens snelheid vuilwaterlozing toilet ligt buiten het bereik van 0-140 km/h - DIA7084 */
    public static final int CH8103 = 8103;

    /** Parameterfout: Bovengrens snelheid vuilwaterlozing - DIA7085 */
    public static final int CH8104 = 8104;

    /** Hulpoproep rolstoelplaats 1 - DRN807B */
    public static final int CH8105 = 8105;

    /** Hulpoproep rolstoelplaats 2 - DRN807C */
    public static final int CH8106 = 8106;

    /** HR-druk kleiner dan 5 bar - REM8095 */
    public static final int CH8107 = 8107;

    /** Hulpoproep toilet - RIS8072 */
    public static final int CH8108 = 8108;

    /** Hulpoproep rolstoelplaats is niet plausibel - RIS9072 */
    public static final int CH8109 = 8109;

    /** Hulpoproep rolstoelplaats is niet plausibel - RIS9074 */
    public static final int CH8110 = 8110;

    /** Bioreactor 80% vol - SNT806F */
    public static final int CH8111 = 8111;

    /** Bioreactor 95% vol - SNT8070 */
    public static final int CH8112 = 8112;

    /** Schoonwatertank toilet leeg - SNT8071 */
    public static final int CH8113 = 8113;

    /** Lek in afvalwatertank of schoonwatertank van toilet - SNT8073 */
    public static final int CH8114 = 8114;

    /** Toilet defect - SNT8074 */
    public static final int CH8115 = 8115;

    /** Vorstlozing toilet mislukt - SNT8075 */
    public static final int CH8116 = 8116;

    /** Niveausensor of verwarming van bioreactor defect - SNT8076 */
    public static final int CH8117 = 8117;

    /** Toiletpot verstopt - SNT8077 */
    public static final int CH8118 = 8118;

    /** Toilet deur defect - SNT807A */
    public static final int CH8119 = 8119;

    /** Bioreactor defect - SNT807D */
    public static final int CH8120 = 8120;

    /** Parameterfout snelheid afvalwaterlozing - SNT808E */
    public static final int CH8121 = 8121;

    /** Lamp DD brandt onterecht - DIA701E */
    public static final int CH8122 = 8122;

    /** Verbroken IP-verbinding WTB-Gateway - COM0115 */
    public static final int CH8123 = 8123;

    /** Laatste GPS snelheid - VT_GPSSpeed */
    public static final int CH8124 = 8124;
}