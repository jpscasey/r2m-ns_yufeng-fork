package com.nexala.spectrum.ns;

public class ChannelIdSng {
	/** Aantal complete deur cycles - ETH_A1_DOOR1_A1_DR_CYCLE_COUNTER */
	public static final int CH1 = 1;

	/** Aantal complete deur cycles’ permanent IN - ETH_A1_DOOR1_A1_SS_CYCLE_COUNTER */
	public static final int CH2 = 2;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_A1_DR_CYCLE_COUNTER */
	public static final int CH3 = 3;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_A1_SS_CYCLE_COUNTER */
	public static final int CH4 = 4;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_A1_DR_CYCLE_COUNTER */
	public static final int CH5 = 5;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_A1_SS_CYCLE_COUNTER */
	public static final int CH6 = 6;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_A1_DR_CYCLE_COUNTER */
	public static final int CH7 = 7;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_A1_SS_CYCLE_COUNTER */
	public static final int CH8 = 8;

	/** Aantal complete deur cycles - ETH_A1_DOOR1_B_DR_CYCLE_COUNTER */
	public static final int CH9 = 9;

	/** Aantal complete deur cyclesd - ETH_A1_DOOR1_B_SS_CYCLE_COUNTER */
	public static final int CH10 = 10;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_B_DR_CYCLE_COUNTER */
	public static final int CH11 = 11;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_B_SS_CYCLE_COUNTER */
	public static final int CH12 = 12;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_B_DR_CYCLE_COUNTER */
	public static final int CH13 = 13;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_B_SS_CYCLE_COUNTER */
	public static final int CH14 = 14;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_B_DR_CYCLE_COUNTER */
	public static final int CH15 = 15;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_B_SS_CYCLE_COUNTER */
	public static final int CH16 = 16;

	/** Aantal complete deur cycles - ETH_A1_DOOR1_C1_DR_CYCLE_COUNTER */
	public static final int CH17 = 17;

	/** Aantal complete deur cycles - ETH_A1_DOOR1_C1_SS_CYCLE_COUNTER */
	public static final int CH18 = 18;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_C1_DR_CYCLE_COUNTER */
	public static final int CH19 = 19;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_C1_SS_CYCLE_COUNTER */
	public static final int CH20 = 20;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_C1_DR_CYCLE_COUNTER */
	public static final int CH21 = 21;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_C1_SS_CYCLE_COUNTER */
	public static final int CH22 = 22;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_C1_DR_CYCLE_COUNTER */
	public static final int CH23 = 23;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_C1_SS_CYCLE_COUNTER */
	public static final int CH24 = 24;

	/** Aantal complete deur cycles - ETH_A1_DOOR1_A2_DR_CYCLE_COUNTER */
	public static final int CH25 = 25;

	/** Aantal complete deur cycles - ETH_A1_DOOR1_A2_SS_CYCLE_COUNTER */
	public static final int CH26 = 26;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_A2_DR_CYCLE_COUNTER */
	public static final int CH27 = 27;

	/** Aantal complete deur cycles - ETH_A1_DOOR2_A2_SS_CYCLE_COUNTER */
	public static final int CH28 = 28;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_A2_DR_CYCLE_COUNTER */
	public static final int CH29 = 29;

	/** Aantal complete deur cycles - ETH_A1_DOOR3_A2_SS_CYCLE_COUNTER */
	public static final int CH30 = 30;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_A2_DR_CYCLE_COUNTER */
	public static final int CH31 = 31;

	/** Aantal complete deur cycles - ETH_A1_DOOR4_A2_SS_CYCLE_COUNTER */
	public static final int CH32 = 32;

	/** Communicatie met gekoppeld treinstel gestoord - AL1040_UT */
	public static final int CH33 = 33;

	/** Centrale computer (TCMS) gestoord - AL1041_A1 */
	public static final int CH34 = 34;

	/** Centrale computer (TCMS) gestoord - AL1041_A2 */
	public static final int CH35 = 35;

	/** Stuurstroomcircuit relais cabine bezet gestoord! - AL1044_A1 */
	public static final int CH36 = 36;

	/** Stuurstroomcircuit relais cabine bezet gestoord! - AL1044_A2 */
	public static final int CH37 = 37;

	/** Stuurstroomcircuit relais cabine bezet gestoord - AL1045_A1 */
	public static final int CH38 = 38;

	/** Stuurstroomcircuit relais cabine bezet gestoord - AL1045_A2 */
	public static final int CH39 = 39;

	/** Werkplaats mode actief - AL1090_UT */
	public static final int CH40 = 40;

	/** Treinstelmode = sluimeren. - AL1091_UT */
	public static final int CH41 = 41;

	/** Treinstelmode = in opzending. - AL1092_UT */
	public static final int CH42 = 42;

	/** Batterij gestoord - AL1101_A1 */
	public static final int CH43 = 43;

	/** Batterij gestoord - AL1101_A2 */
	public static final int CH44 = 44;

	/** Primaire verbruikers batterij gestoord - AL1102_A1 */
	public static final int CH45 = 45;

	/** Primaire verbruikers batterij gestoord - AL1102_A2 */
	public static final int CH46 = 46;

	/** Secundaire verbruikers batterij gestoord - AL1104_A1 */
	public static final int CH47 = 47;

	/** Secundaire verbruikers batterij gestoord - AL1104_A2 */
	public static final int CH48 = 48;

	/** Secundaire verbruikers batterij gestoord - AL1105_B */
	public static final int CH49 = 49;

	/** Secundaire batterijverbruikers gestoord - AL1106_C1 */
	public static final int CH50 = 50;

	/** Noodbedrijf (degraded mode TCMS) niet mogelijk - AL1107_A1 */
	public static final int CH51 = 51;

	/** Noodbedrijf (degraded mode TCMS) niet mogelijk - AL1107_B */
	public static final int CH52 = 52;

	/** Noodbedrijf (degraded mode TCMS) niet mogelijk - AL1107_C1 */
	public static final int CH53 = 53;

	/** Noodbedrijf (degraded mode TCMS) niet mogelijk - AL1107_A2 */
	public static final int CH54 = 54;

	/** Stroomafnemer gestoord - AL1201_B */
	public static final int CH55 = 55;

	/** Hulpcompressor stroomafnemer gestoord - AL1202_B */
	public static final int CH56 = 56;

	/** Stroomafnemer keuzeschakelaar gestoord - AL12151_B */
	public static final int CH57 = 57;

	/** Stroomafnemer keuzeschakelaar gestoord - AL12152_B */
	public static final int CH58 = 58;

	/** Stroomafnemer keuzeschakelaar gestoord - AL1216_B */
	public static final int CH59 = 59;

	/** Stroomafnemers afgesloten - AL1217_B */
	public static final int CH60 = 60;

	/** Stroomafnemer 1 gestoord - AL1218_B */
	public static final int CH61 = 61;

	/** Stroomafnemer 2 gestoord - AL1219_B */
	public static final int CH62 = 62;

	/** Stroomafnemer 1 autodropsysteem is aangesproken - AL1220_B */
	public static final int CH63 = 63;

	/** Stroomafnemer 2 autodropsysteem is aangesproken - AL1221_B */
	public static final int CH64 = 64;

	/** Stroomafnemer schakelaar op/neer gestoord - AL1222_A1 */
	public static final int CH65 = 65;

	/** Stroomafnemer schakelaar op/neer gestoord - AL1222_A2 */
	public static final int CH66 = 66;

	/** Stroomfnemer keuzeschakelaar gestoord - AL1223_B */
	public static final int CH67 = 67;

	/** Stroomafnemerkeuzeschakelaar gestoord. - AL1225_B */
	public static final int CH68 = 68;

	/** Snelschakelaar gestoord - AL1301_B */
	public static final int CH69 = 69;

	/** Snelschakelaar afgeschakeld - AL1312_B */
	public static final int CH70 = 70;

	/** Stoorstroomdetectie uitgeschakeld op cabine achterwand - AL1315_UT */
	public static final int CH71 = 71;

	/** Snelschakelaar afgeschakeld - AL1316_B */
	public static final int CH72 = 72;

	/** Snelschakelaar komt niet in - AL1317_B */
	public static final int CH73 = 73;

	/** Snelschakelaar schakelt niet af, geen beveiliging HS! - AL1318_B */
	public static final int CH74 = 74;

	/** SNS schakelaar in/uit gestoord - AL1322_A1 */
	public static final int CH75 = 75;

	/** SNS schakelaar in/uit gestoord - AL1322_A2 */
	public static final int CH76 = 76;

	/** Statische omvormer gestoord - AL1401_A1 */
	public static final int CH77 = 77;

	/** Statische omvormer gestoord - AL1401_A2 */
	public static final int CH78 = 78;

	/** 230V contactdozen voor reizigers gestoord - AL1403_A1 */
	public static final int CH79 = 79;

	/** 230V contactdozen voor reizigers gestoord - AL1403_B */
	public static final int CH80 = 80;

	/** 230V contactdozen voor reizigers gestoord - AL1403_C1 */
	public static final int CH81 = 81;

	/** 230V contactdozen voor reizigers gestoord - AL1403_A2 */
	public static final int CH82 = 82;

	/** Beide statische omvormers gestoord - AL1405_UT */
	public static final int CH83 = 83;

	/** Treinstelmode = vrijgeschakeld. - AL1406_B */
	public static final int CH84 = 84;

	/** Batterij bewaking actief - AL1408_UT */
	public static final int CH85 = 85;

	/** Batterijverbruikers afgeschakeld - AL1409_UT */
	public static final int CH86 = 86;

	/** Batterij bewaking uitgeschakeld - AL1414_UT */
	public static final int CH87 = 87;

	/** Batterijverbruikers worden afgeschakeld - AL1415_UT */
	public static final int CH88 = 88;

	/** Batterijlading gestoord - AL1416_A1 */
	public static final int CH89 = 89;

	/** Batterijlading gestoord - AL1416_A2 */
	public static final int CH90 = 90;

	/** Statische omvormer gestoord - AL1449_A1 */
	public static final int CH91 = 91;

	/** Statische omvormer gestoord - AL1449_A2 */
	public static final int CH92 = 92;

	/** Statische omvormer gestoord - AL1450_A1 */
	public static final int CH93 = 93;

	/** Statische omvormer gestoord - AL1450_A2 */
	public static final int CH94 = 94;

	/** Batterijlading gestoord - AL1454_A1 */
	public static final int CH95 = 95;

	/** Batterijlading gestoord - AL1454_A2 */
	public static final int CH96 = 96;

	/** Energiemeetsysteem gestoord - AL1720_B */
	public static final int CH97 = 97;

	/** Stilstand signaal gestoord - AL1801_B */
	public static final int CH98 = 98;

	/** Stilstand signaal gestoord - AL1803_A1 */
	public static final int CH99 = 99;

	/** Stilstand signaal gestoord - AL1803_A2 */
	public static final int CH100 = 100;

	/** Stilstand signaal gestoord - AL1805_A1 */
	public static final int CH101 = 101;

	/** Stilstand signaal gestoord - AL1805_A2 */
	public static final int CH102 = 102;

	/** Stuurstroom in twee cabines ingeschakeld - AL2002_A1 */
	public static final int CH103 = 103;

	/** Stuurstroom in twee cabines ingeschakeld - AL2002_A2 */
	public static final int CH104 = 104;

	/** Na combineren stuurstroom in twee cabines ingeschakeld - AL2004_UT */
	public static final int CH105 = 105;

	/** Stuurstroomcircuit relais cabine bezet gestoord - AL2005_A1 */
	public static final int CH106 = 106;

	/** Stuurstroomcircuit relais cabine bezet gestoord - AL2005_A2 */
	public static final int CH107 = 107;

	/** Rij- remhendel gestoord - AL2054_A1 */
	public static final int CH108 = 108;

	/** Rij- remhendel gestoord - AL2054_A2 */
	public static final int CH109 = 109;

	/** Rijrichtingschakelaar gestoord - AL2055_A1 */
	public static final int CH110 = 110;

	/** Rijrichtingschakelaar gestoord - AL2055_A2 */
	public static final int CH111 = 111;

	/** Rij- remhendel gestoord - AL2056_A1 */
	public static final int CH112 = 112;

	/** Rij- remhendel gestoord - AL2056_A2 */
	public static final int CH113 = 113;

	/** Rij- remhendel gestoord - AL2060_A1 */
	public static final int CH114 = 114;

	/** Rij- remhendel gestoord - AL2060_A2 */
	public static final int CH115 = 115;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2101_A1 */
	public static final int CH116 = 116;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2101_A2 */
	public static final int CH117 = 117;

	/** Tractie-installatie gestoord - AL2103_A1 */
	public static final int CH118 = 118;

	/** Tractie-installatie gestoord - AL2103_A2 */
	public static final int CH119 = 119;

	/** Tractie-installatie gestoord - AL2110_A1 */
	public static final int CH120 = 120;

	/** Tractie-installatie gestoord - AL2110_A2 */
	public static final int CH121 = 121;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2111_A1 */
	public static final int CH122 = 122;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2111_A2 */
	public static final int CH123 = 123;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2112_A1 */
	public static final int CH124 = 124;

	/** Tractie-installatie en laagspanningsomzetter gestoord - AL2112_A2 */
	public static final int CH125 = 125;

	/** Tractie-installatie test niet geslaagd - AL2151_A1 */
	public static final int CH126 = 126;

	/** Tractie-installatie test niet geslaagd - AL2151_A2 */
	public static final int CH127 = 127;

	/** Tractie-installatie test niet geldig - AL2152_A1 */
	public static final int CH128 = 128;

	/** Tractie-installatie test niet geldig - AL2152_A2 */
	public static final int CH129 = 129;

	/** Snelschakelaar afgeschakeld - AL2162_A1 */
	public static final int CH130 = 130;

	/** Snelschakelaar afgeschakeld - AL2162_A2 */
	public static final int CH131 = 131;

	/** Tractie-installatie gestoord - AL2170_A1 */
	public static final int CH132 = 132;

	/** Tractie-installatie gestoord - AL2170_A2 */
	public static final int CH133 = 133;

	/** Tractie-installatie uitgeschakeld - AL2180_A1 */
	public static final int CH134 = 134;

	/** Tractie-installatie uitgeschakeld - AL2180_A2 */
	public static final int CH135 = 135;

	/** Tractie-installatie gestoord - AL2183_A1 */
	public static final int CH136 = 136;

	/** Tractie-installatie gestoord - AL2183_A2 */
	public static final int CH137 = 137;

	/** Wieldiameterverschil te groot - AL2192_A1 */
	public static final int CH138 = 138;

	/** Wieldiameterverschil te groot - AL2192_A2 */
	public static final int CH139 = 139;

	/** Wieldiameter instellingen buiten bereik - AL2193_A1 */
	public static final int CH140 = 140;

	/** Wieldiameter instellingen buiten bereik - AL2193_A2 */
	public static final int CH141 = 141;

	/** Tractievrijgave circuit gestoord - AL2301_A1 */
	public static final int CH142 = 142;

	/** Tractievrijgave circuit gestoord - AL2301_A2 */
	public static final int CH143 = 143;

	/** EP-rem in actieve cabine gestoord - AL3001_A1 */
	public static final int CH144 = 144;

	/** EP-rem in actieve cabine gestoord - AL3001_A2 */
	public static final int CH145 = 145;

	/** EP-rem niet actieve cabine gestoord - AL3002_A1 */
	public static final int CH146 = 146;

	/** EP-rem niet actieve cabine gestoord - AL3002_A2 */
	public static final int CH147 = 147;

	/** EP-rem gestoord - AL3003_C1 */
	public static final int CH148 = 148;

	/** EP-rem gestoord - AL3010_A1 */
	public static final int CH149 = 149;

	/** EP-rem gestoord - AL3010_A2 */
	public static final int CH150 = 150;

	/** EP-rem DST 1 gestoord - AL30451_A1 */
	public static final int CH151 = 151;

	/** EP-rem DST 2 gestoord - AL30452_A1 */
	public static final int CH152 = 152;

	/** EP-rem DST 3 gestoord - AL30453_C1 */
	public static final int CH153 = 153;

	/** EP-rem DST3-SNG3 || DST4-SNG4 gestoord - AL30456_A2 */
	public static final int CH154 = 154;

	/** EP-rem DST4-SNG3 || DST5-SNG4 gestoord - AL30457_A2 */
	public static final int CH155 = 155;

	/** Rembesturing gestoord - AL3050_A1 */
	public static final int CH156 = 156;

	/** Rembesturing gestoord - AL3050_A2 */
	public static final int CH157 = 157;

	/** Rembesturing gestoord - AL3051_C1 */
	public static final int CH158 = 158;

	/** Directe rem DST 1 afgesloten - AL30521_A1 */
	public static final int CH159 = 159;

	/** Directe rem DST 2 afgesloten - AL30522_A1 */
	public static final int CH160 = 160;

	/** Directe rem DST 3 afgesloten - AL30523_C1 */
	public static final int CH161 = 161;

	/** Directe rem DST 3 - SNG 3 || DST 4 - SNG 4 afgesloten - AL30526_A2 */
	public static final int CH162 = 162;

	/** Directe rem DST 4 - SNG 3 || DST 5 - SNG 4 afgesloten - AL30527_A2 */
	public static final int CH163 = 163;

	/** Indirecte rem van 2 draaistellen afgesloten! - AL3053_UT */
	public static final int CH164 = 164;

	/** Hoofd reservoir druk lager dan 6,5 bar - AL3054_UT */
	public static final int CH165 = 165;

	/** Halterem uitgeschakeld - AL3057_UT */
	public static final int CH166 = 166;

	/** Indirecte rem afgesloten - AL3058_A1 */
	public static final int CH167 = 167;

	/** Indirecte rem afgesloten - AL3058_C1 */
	public static final int CH168 = 168;

	/** Indirecte rem afgesloten - AL3058_A2 */
	public static final int CH169 = 169;

	/** Pn-rem (gedeeltelijk) overbrugd - AL3059_A1 */
	public static final int CH170 = 170;

	/** Pn-rem (gedeeltelijk) overbrugd - AL3059_C1 */
	public static final int CH171 = 171;

	/** Pn-rem (gedeeltelijk) overbrugd - AL3059_A2 */
	public static final int CH172 = 172;

	/** Remnoodbedrijf actief - AL3060_A1 */
	public static final int CH173 = 173;

	/** Remnoodbedrijf actief - AL3060_A2 */
	public static final int CH174 = 174;

	/** Pn-rem DST 1 gestoord - AL30611_A1 */
	public static final int CH175 = 175;

	/** Pn-rem DST 2 gestoord - AL30612_A1 */
	public static final int CH176 = 176;

	/** Pn-rem DST 3 gestoord - AL30613_C1 */
	public static final int CH177 = 177;

	/** Pn-rem DST3-SNG3 || DST4-SNG4 gestoord - AL30616_A2 */
	public static final int CH178 = 178;

	/** Pn-rem DST4-SNG3 || DST5-SNG4 gestoord - AL30617_A2 */
	public static final int CH179 = 179;

	/** Pn-rem gestoord - AL3062_A1 */
	public static final int CH180 = 180;

	/** Pn-rem gestoord - AL3062_A2 */
	public static final int CH181 = 181;

	/** Pn-rem afgesloten - AL3070_A1 */
	public static final int CH182 = 182;

	/** Pn-rem afgesloten - AL3070_A2 */
	public static final int CH183 = 183;

	/** Remnoodbedrijf ingeschakeld in niet actieve cabine - AL3078_A1 */
	public static final int CH184 = 184;

	/** Remnoodbedrijf ingeschakeld in niet actieve cabine - AL3078_A2 */
	public static final int CH185 = 185;

	/** Vaste rem DST 1 - AL30801_A1 */
	public static final int CH186 = 186;

	/** Vaste rem DST 2 - AL30802_A1 */
	public static final int CH187 = 187;

	/** Vaste rem DST 3 - AL30803_C1 */
	public static final int CH188 = 188;

	/** Vaste rem DST3-SNG3 || DST4-SNG4 - AL30806_A2 */
	public static final int CH189 = 189;

	/** Vaste rem DST 4 - SNG3 || DST 5 - SNG 4 - AL30807_A2 */
	public static final int CH190 = 190;

	/** EP-rem DST 1 gestoord! - AL30811_A1 */
	public static final int CH191 = 191;

	/** EP-rem DST 2 gestoord! - AL30812_A1 */
	public static final int CH192 = 192;

	/** EP-rem DST 3 gestoord! - AL30813_C1 */
	public static final int CH193 = 193;

	/** EP-rem DST3-SNG3 || DST4-SNG4 gestoord! - AL30816_A2 */
	public static final int CH194 = 194;

	/** EP-rem DST4-SNG3 || DST5-SNG4 gestoord! - AL30817_A2 */
	public static final int CH195 = 195;

	/** Vaste rem - AL3082_A1 */
	public static final int CH196 = 196;

	/** Vaste rem - AL3082_C1 */
	public static final int CH197 = 197;

	/** Vaste rem - AL3082_A2 */
	public static final int CH198 = 198;

	/** ABI DST 1 gestoord - AL30851_A1 */
	public static final int CH199 = 199;

	/** ABI DST 2 gestoord - AL30852_A1 */
	public static final int CH200 = 200;

	/** ABI DST 3 gestoord - AL30853_C1 */
	public static final int CH201 = 201;

	/** ABI DST3-SNG3 || DST4-SNG4 gestoord - AL30856_A2 */
	public static final int CH202 = 202;

	/** ABI DST4-SNG3 || DST5-SNG4 gestoord - AL30857_A2 */
	public static final int CH203 = 203;

	/** EP-Rem test niet geldig! - AL3090_A1 */
	public static final int CH204 = 204;

	/** EP-Rem test niet geldig! - AL3090_A2 */
	public static final int CH205 = 205;

	/** P-rem/Mg-rem test niet geldig! - AL3091_A1 */
	public static final int CH206 = 206;

	/** P-rem/Mg-rem test niet geldig! - AL3091_A2 */
	public static final int CH207 = 207;

	/** Dodeman test niet geldig! - AL3092_A1 */
	public static final int CH208 = 208;

	/** Dodeman test niet geldig! - AL3092_A2 */
	public static final int CH209 = 209;

	/** Remlus test niet geldig - AL3093_A1 */
	public static final int CH210 = 210;

	/** Remlus test niet geldig - AL3093_A2 */
	public static final int CH211 = 211;

	/** EP-rem test niet geldig in niet actieve cabine - AL3095_A1 */
	public static final int CH212 = 212;

	/** EP-rem test niet geldig in niet actieve cabine - AL3095_A2 */
	public static final int CH213 = 213;

	/** P-rem/Mg-rem test niet geldig in niet actieve cabine - AL3096_A1 */
	public static final int CH214 = 214;

	/** P-rem/Mg-rem test niet geldig in niet actieve cabine - AL3096_A2 */
	public static final int CH215 = 215;

	/** Dodeman test niet geldig in niet actieve cabine - AL3097_A1 */
	public static final int CH216 = 216;

	/** Dodeman test niet geldig in niet actieve cabine - AL3097_A2 */
	public static final int CH217 = 217;

	/** Remlus test niet geldig in niet actieve cabine - AL3098_A1 */
	public static final int CH218 = 218;

	/** Remlus test niet geldig in niet actieve cabine - AL3098_A2 */
	public static final int CH219 = 219;

	/** Parkeerrem gestoord - AL3201_A1 */
	public static final int CH220 = 220;

	/** Parkeerrem gestoord - AL3201_A2 */
	public static final int CH221 = 221;

	/** Status parkeerrem niet betrouwbaar (niet gelost) - AL3250_A1 */
	public static final int CH222 = 222;

	/** Status parkeerrem niet betrouwbaar (niet gelost) - AL3250_A2 */
	public static final int CH223 = 223;

	/** Status parkeerrem niet betrouwbaar (niet aangelegd) - AL3251_A1 */
	public static final int CH224 = 224;

	/** Status parkeerrem niet betrouwbaar (niet aangelegd) - AL3251_A2 */
	public static final int CH225 = 225;

	/** Parkeerrem gestoord (niet aangelegd) - AL3252_A1 */
	public static final int CH226 = 226;

	/** Parkeerrem gestoord (niet aangelegd) - AL3252_A2 */
	public static final int CH227 = 227;

	/** Parkeerrem gestoord (niet gelost) - AL3253_A1 */
	public static final int CH228 = 228;

	/** Parkeerrem gestoord (niet gelost) - AL3253_A2 */
	public static final int CH229 = 229;

	/** Parkeerrem buiten dienst gesteld - AL3255_A1 */
	public static final int CH230 = 230;

	/** Parkeerrem buiten dienst gesteld - AL3255_A2 */
	public static final int CH231 = 231;

	/** Snelremlus in niet bediende cabine gestoord - AL3301_A1 */
	public static final int CH232 = 232;

	/** Snelremlus in niet bediende cabine gestoord - AL3301_A2 */
	public static final int CH233 = 233;

	/** Snelremlus gestoord - AL3302_A1 */
	public static final int CH234 = 234;

	/** Snelremlus gestoord - AL3302_A2 */
	public static final int CH235 = 235;

	/** Verbinding met dodeman klep verbroken:SIFA CONTROL AND EMERGENCY VALVES CIRCUIT BREAKER DISCONNECTED - AL3303_A1 */
	public static final int CH236 = 236;

	/** Verbinding met dodeman klep verbroken:SIFA CONTROL AND EMERGENCY VALVES CIRCUIT BREAKER DISCONNECTED - AL3303_A2 */
	public static final int CH237 = 237;

	/** Automaat 33D01 uitgevallen: EMERGENCY BRAKE CIRCUIT BREAKER 33Q01 DISCONNECTED - AL3304_B */
	public static final int CH238 = 238;

	/** Automaat 33D02 uitgevallen: EMERGENCY BRAKE CIRCUIT BREAKER 33Q02 DISCONNECTED - AL3305_C1 */
	public static final int CH239 = 239;

	/** EMERGENCY CTRL MRP PRESS. DISABLED - AL3349_A1 */
	public static final int CH240 = 240;

	/** EMERGENCY CTRL MRP PRESS. DISABLED - AL3349_A2 */
	public static final int CH241 = 241;

	/** ERROR IN TCMS CCUCONFILE.INI - AL3350_A1 */
	public static final int CH242 = 242;

	/** ERROR IN TCMS PARAMFILE.INI - AL3350_A2 */
	public static final int CH243 = 243;

	/** Snelremklep afgesloten. - AL3351_A1 */
	public static final int CH244 = 244;

	/** Snelremklep afgesloten. - AL3351_A2 */
	public static final int CH245 = 245;

	/** Hoofdreservoir druk opnemer gestoord - AL3360_A1 */
	public static final int CH246 = 246;

	/** Hoofdreservoir druk opnemer gestoord - AL3360_A2 */
	public static final int CH247 = 247;

	/** SECOND FAILURE DETECED - AL3364_A1 */
	public static final int CH248 = 248;

	/** SECOND FAILURE DETECED - AL3364_A2 */
	public static final int CH249 = 249;

	/** MRP PRESSURE TRANSDUCER FAILURES - AL3365_UT */
	public static final int CH250 = 250;

	/** Snelremming vanuit niet bediende cabine - AL3366_A1 */
	public static final int CH251 = 251;

	/** Snelremming vanuit niet bediende cabine - AL3366_A2 */
	public static final int CH252 = 252;

	/** Magneetrem gestoord - AL3701_A1 */
	public static final int CH253 = 253;

	/** Magneetrem gestoord - AL3701_A2 */
	public static final int CH254 = 254;

	/** Magneetrem uitgeschakeld - AL3752_A1 */
	public static final int CH255 = 255;

	/** Magneetrem uitgeschakeld - AL3752_A2 */
	public static final int CH256 = 256;

	/** Magneetrem status niet plausibel - AL3753_A1 */
	public static final int CH257 = 257;

	/** Magneetrem status niet plausibel - AL3753_A2 */
	public static final int CH258 = 258;

	/** Event Recorder en dodeman gestoord - AL4001_B */
	public static final int CH259 = 259;

	/** Dodeman uitgeschakeld op cabine achterwand - AL4050_A1 */
	public static final int CH260 = 260;

	/** Dodeman uitgeschakeld op cabine achterwand - AL4050_A2 */
	public static final int CH261 = 261;

	/** Event Recorder en dodeman gestoord - AL4051_B */
	public static final int CH262 = 262;

	/** Event Recorder en dodeman gestoord - AL4052_B */
	public static final int CH263 = 263;

	/** Event Recorder en dodeman gestoord - AL4090_B */
	public static final int CH264 = 264;

	/** Noodrem reizigers gestoord - AL4101_A1 */
	public static final int CH265 = 265;

	/** Noodrem reizigers gestoord - AL4101_A2 */
	public static final int CH266 = 266;

	/** Overname noodrem reizigers gestoord - AL4102_A1 */
	public static final int CH267 = 267;

	/** Overname noodrem reizigers gestoord - AL4102_A2 */
	public static final int CH268 = 268;

	/** Noodrem overbrugging uitgeschakeld op cabine achterwand - AL4110_A1 */
	public static final int CH269 = 269;

	/** Noodrem overbrugging uitgeschakeld op cabine achterwand - AL4110_A2 */
	public static final int CH270 = 270;

	/** Noodrem reizigers uitgeschakeld op cabine achterwand - AL4111_A1 */
	public static final int CH271 = 271;

	/** Noodrem reizigers uitgeschakeld op cabine achterwand - AL4111_A2 */
	public static final int CH272 = 272;

	/** Noodrem reizigers gestoord - AL4150_A1 */
	public static final int CH273 = 273;

	/** Noodrem reizigers gestoord - AL4150_A2 */
	public static final int CH274 = 274;

	/** DMI en geluidsignalen ATB gestoord - AL4301_A1 */
	public static final int CH275 = 275;

	/** DMI en geluidsignalen ATB gestoord - AL4301_A2 */
	public static final int CH276 = 276;

	/** BTM van ETCS gestoord - AL4302_A1 */
	public static final int CH277 = 277;

	/** BTM van ETCS gestoord - AL4302_A2 */
	public static final int CH278 = 278;

	/** EVC1 van ETCS gestoord - AL4304_B */
	public static final int CH279 = 279;

	/** EVC2 van ETCS gestoord - AL4305_B */
	public static final int CH280 = 280;

	/** EVC3 van ETCS gestoord - AL4306_B */
	public static final int CH281 = 281;

	/** ETCS gestoord - AL4307_B */
	public static final int CH282 = 282;

	/** Radar van ETCS gestoord - AL4308_B */
	public static final int CH283 = 283;

	/** Communicatie switch van ETCS gestoord - AL4309_B */
	public static final int CH284 = 284;

	/** ATB gestoord - AL4310_B */
	public static final int CH285 = 285;

	/** ATB geluidsignalen (gong en bel) gestoord - AL4311_B */
	public static final int CH286 = 286;

	/** Herstart van ETCS nodig. - AL4312_UT */
	public static final int CH287 = 287;

	/** ERTMS/ATB uitgeschakeld op cabine achterwand - AL4351_A1 */
	public static final int CH288 = 288;

	/** ERTMS/ATB uitgeschakeld op cabine achterwand - AL4351_A2 */
	public static final int CH289 = 289;

	/** ERTMS/ATB uitgeschakeld op cabine achterwand - AL4352_A1 */
	public static final int CH290 = 290;

	/** ERTMS/ATB uitgeschakeld op cabine achterwand - AL4352_A2 */
	public static final int CH291 = 291;

	/** Communicatie van ETCS gestoord - AL4353_B */
	public static final int CH292 = 292;

	/** ETCS gestoord - AL4354_B */
	public static final int CH293 = 293;

	/** BTM module van ETCS gestoord - AL4356_A1 */
	public static final int CH294 = 294;

	/** BTM module van ETCS gestoord - AL4356_A2 */
	public static final int CH295 = 295;

	/** BTM antenne van ETCS gestoord - AL4358_A1 */
	public static final int CH296 = 296;

	/** BTM antenne van ETCS gestoord - AL4358_A2 */
	public static final int CH297 = 297;

	/** Communicatie ETCS/ATB gestoord - AL4364_B */
	public static final int CH298 = 298;

	/** ATB gestoord - AL4365_B */
	public static final int CH299 = 299;

	/** Snelheidsmeting van ETCS gestoord - AL4368_B */
	public static final int CH300 = 300;

	/** GSM-R module van ETCS gestoord - AL4371_B */
	public static final int CH301 = 301;

	/** DMI van ETCS gestoord - AL4373_A1 */
	public static final int CH302 = 302;

	/** DMI van ETCS gestoord - AL4373_A2 */
	public static final int CH303 = 303;

	/** ATB gestoord - AL4456_B */
	public static final int CH304 = 304;

	/** ATB gestoord - AL4457_B */
	public static final int CH305 = 305;

	/** ATB gestoord - AL4458_B */
	public static final int CH306 = 306;

	/** ATB gestoord - AL4459_B */
	public static final int CH307 = 307;

	/** ATB gestoord - AL4460_B */
	public static final int CH308 = 308;

	/** ATB gestoord - AL4468_B */
	public static final int CH309 = 309;

	/** ATB gestoord - AL4469_B */
	public static final int CH310 = 310;

	/** ATB gestoord - AL4470_B */
	public static final int CH311 = 311;

	/** Brand gedetecteerd in tractieomvormer! - AL4704_A1 */
	public static final int CH312 = 312;

	/** Brand gedetecteerd in tractieomvormer! - AL4704_A2 */
	public static final int CH313 = 313;

	/** Brand gedetecteerd in snelschakelaar! - AL4707_B */
	public static final int CH314 = 314;

	/** Brand gedetecteerd in de batterijkast! - AL4708_A1 */
	public static final int CH315 = 315;

	/** Brand gedetecteerd in de batterijkast! - AL4708_A2 */
	public static final int CH316 = 316;

	/** Deur 1 en 4 gestoord - AL5001_A1 */
	public static final int CH317 = 317;

	/** Deur 1 en 4 gestoord - AL5001_B */
	public static final int CH318 = 318;

	/** Deur 1 en 4 gestoord - AL5001_C1 */
	public static final int CH319 = 319;

	/** Deur 1 en 4 gestoord - AL5001_A2 */
	public static final int CH320 = 320;

	/** Deur 2 en 3 gestoord - AL5002_A1 */
	public static final int CH321 = 321;

	/** Deur 2 en 3 gestoord - AL5002_B */
	public static final int CH322 = 322;

	/** Deur 2 en 3 gestoord - AL5002_C1 */
	public static final int CH323 = 323;

	/** Deur 2 en 3 gestoord - AL5002_A2 */
	public static final int CH324 = 324;

	/** Deurvrijgave niet mogelijk - AL5003_A1 */
	public static final int CH325 = 325;

	/** Deurvrijgave niet mogelijk - AL5003_A2 */
	public static final int CH326 = 326;

	/** Deurvrijgave links gestoord! - AL5016_A1 */
	public static final int CH327 = 327;

	/** Deurvrijgave links gestoord! - AL5016_A2 */
	public static final int CH328 = 328;

	/** Deurvrijgave rechts gestoord! - AL5017_A1 */
	public static final int CH329 = 329;

	/** Deurvrijgave rechts gestoord! - AL5017_A2 */
	public static final int CH330 = 330;

	/** Deurvrijgave rechts niet mogelijk - AL5019_A1 */
	public static final int CH331 = 331;

	/** Deurvrijgave rechts niet mogelijk - AL5019_A2 */
	public static final int CH332 = 332;

	/** Deurvrijgave links niet mogelijk - AL5020_A1 */
	public static final int CH333 = 333;

	/** Deurvrijgave links niet mogelijk - AL5020_A2 */
	public static final int CH334 = 334;

	/** Deur 1 en 3 gestoord - AL5021_A1 */
	public static final int CH335 = 335;

	/** Deur 1 en 3 gestoord - AL5021_B */
	public static final int CH336 = 336;

	/** Deur 1 en 3 gestoord - AL5021_C1 */
	public static final int CH337 = 337;

	/** Deur 1 en 3 gestoord - AL5021_A2 */
	public static final int CH338 = 338;

	/** Deur 2 en 4 gestoord - AL5022_A1 */
	public static final int CH339 = 339;

	/** Deur 2 en 4 gestoord - AL5022_B */
	public static final int CH340 = 340;

	/** Deur 2 en 4 gestoord - AL5022_C1 */
	public static final int CH341 = 341;

	/** Deur 2 en 4 gestoord - AL5022_A2 */
	public static final int CH342 = 342;

	/** Deur 1 en 4 gestoord - AL5023_A1 */
	public static final int CH343 = 343;

	/** Deur 1 en 4 gestoord - AL5023_B */
	public static final int CH344 = 344;

	/** Deur 1 en 4 gestoord - AL5023_C1 */
	public static final int CH345 = 345;

	/** Deur 1 en 4 gestoord - AL5023_A2 */
	public static final int CH346 = 346;

	/** Deur 2 en 3 gestoord - AL5024_A1 */
	public static final int CH347 = 347;

	/** Deur 2 en 3 gestoord - AL5024_B */
	public static final int CH348 = 348;

	/** Deur 2 en 3 gestoord - AL5024_C1 */
	public static final int CH349 = 349;

	/** Deur 2 en 3 gestoord - AL5024_A2 */
	public static final int CH350 = 350;

	/** Centraal sluiten vanuit cabine niet mogelijk - AL5025_A1 */
	public static final int CH351 = 351;

	/** Centraal sluiten vanuit cabine niet mogelijk - AL5025_A2 */
	public static final int CH352 = 352;

	/** Deurvrijgave van enkel rijtuig niet mogelijk - AL5026_A1 */
	public static final int CH353 = 353;

	/** Deurvrijgave van enkel rijtuig niet mogelijk - AL5026_B */
	public static final int CH354 = 354;

	/** Deurvrijgave van enkel rijtuig niet mogelijk - AL5026_C1 */
	public static final int CH355 = 355;

	/** Deurvrijgave van enkel rijtuig niet mogelijk - AL5026_A2 */
	public static final int CH356 = 356;

	/** Centraal sluiten vanuit cabine niet mogelijk - AL5027_A1 */
	public static final int CH357 = 357;

	/** Centraal sluiten vanuit cabine niet mogelijk - AL5027_A2 */
	public static final int CH358 = 358;

	/** Deurvrijgave deur 1 en 4 tijdens de rit mogelijk - AL5028_A1 */
	public static final int CH359 = 359;

	/** Deurvrijgave deur 1 en 4 tijdens de rit mogelijk - AL5028_B */
	public static final int CH360 = 360;

	/** Deurvrijgave deur 1 en 4 tijdens de rit mogelijk - AL5028_C1 */
	public static final int CH361 = 361;

	/** Deurvrijgave deur 1 en 4 tijdens de rit mogelijk - AL5028_A2 */
	public static final int CH362 = 362;

	/** Deurvrijgave deur 2 en 3 tijdens de rit mogelijk - AL5029_A1 */
	public static final int CH363 = 363;

	/** Deurvrijgave deur 2 en 3 tijdens de rit mogelijk - AL5029_B */
	public static final int CH364 = 364;

	/** Deurvrijgave deur 2 en 3 tijdens de rit mogelijk - AL5029_C1 */
	public static final int CH365 = 365;

	/** Deurvrijgave deur 2 en 3 tijdens de rit mogelijk - AL5029_A2 */
	public static final int CH366 = 366;

	/** Deur 1 nood geopend! - AL51105_A1 */
	public static final int CH367 = 367;

	/** Deur 1 nood geopend! - AL51105_B */
	public static final int CH368 = 368;

	/** Deur 1 nood geopend! - AL51105_C1 */
	public static final int CH369 = 369;

	/** Deur 1 nood geopend! - AL51105_A2 */
	public static final int CH370 = 370;

	/** Deurvrijgave deur 1 gestoord! - AL51106_A1 */
	public static final int CH371 = 371;

	/** Deurvrijgave deur 1 gestoord! - AL51106_B */
	public static final int CH372 = 372;

	/** Deurvrijgave deur 1 gestoord! - AL51106_C1 */
	public static final int CH373 = 373;

	/** Deurvrijgave deur 1 gestoord! - AL51106_A2 */
	public static final int CH374 = 374;

	/** Centraal sluiten bij deur 1 gestoord - AL51107_A1 */
	public static final int CH375 = 375;

	/** Centraal sluiten bij deur 1 gestoord - AL51107_B */
	public static final int CH376 = 376;

	/** Centraal sluiten bij deur 1 gestoord - AL51107_C1 */
	public static final int CH377 = 377;

	/** Centraal sluiten bij deur 1 gestoord - AL51107_A2 */
	public static final int CH378 = 378;

	/** Deur 1 gestoord - AL51110_A1 */
	public static final int CH379 = 379;

	/** Deur 1 gestoord - AL51110_B */
	public static final int CH380 = 380;

	/** Deur 1 gestoord - AL51110_C1 */
	public static final int CH381 = 381;

	/** Deur 1 gestoord - AL51110_A2 */
	public static final int CH382 = 382;

	/** Trede van deur 1 gestoord - AL51111_A1 */
	public static final int CH383 = 383;

	/** Trede van deur 1 gestoord - AL51111_B */
	public static final int CH384 = 384;

	/** Trede van deur 1 gestoord - AL51111_C1 */
	public static final int CH385 = 385;

	/** Trede van deur 1 gestoord - AL51111_A2 */
	public static final int CH386 = 386;

	/** Deuren dicht signaal deur 1 onbetrouwbaar! - AL51114_A1 */
	public static final int CH387 = 387;

	/** Deuren dicht signaal deur 1 onbetrouwbaar! - AL51114_B */
	public static final int CH388 = 388;

	/** Deuren dicht signaal deur 1 onbetrouwbaar! - AL51114_C1 */
	public static final int CH389 = 389;

	/** Deuren dicht signaal deur 1 onbetrouwbaar! - AL51114_A2 */
	public static final int CH390 = 390;

	/** Eindschakelaar trede van deur 1 onbetrouwbaar! - AL51115_A1 */
	public static final int CH391 = 391;

	/** Eindschakelaar trede van deur 1 onbetrouwbaar! - AL51115_B */
	public static final int CH392 = 392;

	/** Eindschakelaar trede van deur 1 onbetrouwbaar! - AL51115_C1 */
	public static final int CH393 = 393;

	/** Eindschakelaar trede van deur 1 onbetrouwbaar! - AL51115_A2 */
	public static final int CH394 = 394;

	/** Deur/trede deur 1 niet gesloten/ingeschoven! - AL51118_A1 */
	public static final int CH395 = 395;

	/** Deur/trede deur 1 niet gesloten/ingeschoven! - AL51118_B */
	public static final int CH396 = 396;

	/** Deur/trede deur 1 niet gesloten/ingeschoven! - AL51118_C1 */
	public static final int CH397 = 397;

	/** Deur/trede deur 1 niet gesloten/ingeschoven! - AL51118_A2 */
	public static final int CH398 = 398;

	/** Drukknop sluiten deur 1 gestoord - AL51126_A1 */
	public static final int CH399 = 399;

	/** Drukknop sluiten deur 1 gestoord - AL51126_B */
	public static final int CH400 = 400;

	/** Drukknop sluiten deur 1 gestoord - AL51126_C1 */
	public static final int CH401 = 401;

	/** Drukknop sluiten deur 1 gestoord - AL51126_A2 */
	public static final int CH402 = 402;

	/** Deuren dicht signaal deur 1 gestoord - AL51127_A1 */
	public static final int CH403 = 403;

	/** Deuren dicht signaal deur 1 gestoord - AL51127_B */
	public static final int CH404 = 404;

	/** Deuren dicht signaal deur 1 gestoord - AL51127_C1 */
	public static final int CH405 = 405;

	/** Deuren dicht signaal deur 1 gestoord - AL51127_A2 */
	public static final int CH406 = 406;

	/** Deur 1 gestoord - AL51130_A1 */
	public static final int CH407 = 407;

	/** Deur 1 gestoord - AL51130_B */
	public static final int CH408 = 408;

	/** Deur 1 gestoord - AL51130_C1 */
	public static final int CH409 = 409;

	/** Deur 1 gestoord - AL51130_A2 */
	public static final int CH410 = 410;

	/** Inklembeveiliging deur 1 gestoord - AL51133_A1 */
	public static final int CH411 = 411;

	/** Inklembeveiliging deur 1 gestoord - AL51133_B */
	public static final int CH412 = 412;

	/** Inklembeveiliging deur 1 gestoord - AL51133_C1 */
	public static final int CH413 = 413;

	/** Inklembeveiliging deur 1 gestoord - AL51133_A2 */
	public static final int CH414 = 414;

	/** Inklembeveiliging trede van deur 1 gestoord - AL51134_A1 */
	public static final int CH415 = 415;

	/** Inklembeveiliging trede van deur 1 gestoord - AL51134_B */
	public static final int CH416 = 416;

	/** Inklembeveiliging trede van deur 1 gestoord - AL51134_C1 */
	public static final int CH417 = 417;

	/** Inklembeveiliging trede van deur 1 gestoord - AL51134_A2 */
	public static final int CH418 = 418;

	/** Deur 1 gestoord - AL51150_A1 */
	public static final int CH419 = 419;

	/** Deur 1 gestoord - AL51150_B */
	public static final int CH420 = 420;

	/** Deur 1 gestoord - AL51150_C1 */
	public static final int CH421 = 421;

	/** Deur 1 gestoord - AL51150_A2 */
	public static final int CH422 = 422;

	/** Trede van deur 1 gestoord - AL51156_A1 */
	public static final int CH423 = 423;

	/** Trede van deur 1 gestoord - AL51156_B */
	public static final int CH424 = 424;

	/** Trede van deur 1 gestoord - AL51156_C1 */
	public static final int CH425 = 425;

	/** Trede van deur 1 gestoord - AL51156_A2 */
	public static final int CH426 = 426;

	/** Deur 1 gestoord - AL51167_A1 */
	public static final int CH427 = 427;

	/** Deur 1 gestoord - AL51167_B */
	public static final int CH428 = 428;

	/** Deur 1 gestoord - AL51167_C1 */
	public static final int CH429 = 429;

	/** Deur 1 gestoord - AL51167_A2 */
	public static final int CH430 = 430;

	/** Trede deur 1 niet volledig ingeschoven! - AL51171_A1 */
	public static final int CH431 = 431;

	/** Trede deur 1 niet volledig ingeschoven! - AL51171_B */
	public static final int CH432 = 432;

	/** Trede deur 1 niet volledig ingeschoven! - AL51171_C1 */
	public static final int CH433 = 433;

	/** Trede deur 1 niet volledig ingeschoven! - AL51171_A2 */
	public static final int CH434 = 434;

	/** Deur 3 nood geopend! - AL51305_A1 */
	public static final int CH435 = 435;

	/** Deur 3 nood geopend! - AL51305_B */
	public static final int CH436 = 436;

	/** Deur 3 nood geopend! - AL51305_C1 */
	public static final int CH437 = 437;

	/** Deur 3 nood geopend! - AL51305_A2 */
	public static final int CH438 = 438;

	/** Deurvrijgave deur 3 gestoord! - AL51306_A1 */
	public static final int CH439 = 439;

	/** Deurvrijgave deur 3 gestoord! - AL51306_B */
	public static final int CH440 = 440;

	/** Deurvrijgave deur 3 gestoord! - AL51306_C1 */
	public static final int CH441 = 441;

	/** Deurvrijgave deur 3 gestoord! - AL51306_A2 */
	public static final int CH442 = 442;

	/** Centraal sluiten bij deur 3 gestoord - AL51307_A1 */
	public static final int CH443 = 443;

	/** Centraal sluiten bij deur 3 gestoord - AL51307_B */
	public static final int CH444 = 444;

	/** Centraal sluiten bij deur 3 gestoord - AL51307_C1 */
	public static final int CH445 = 445;

	/** Centraal sluiten bij deur 3 gestoord - AL51307_A2 */
	public static final int CH446 = 446;

	/** Deur 3 gestoord - AL51310_A1 */
	public static final int CH447 = 447;

	/** Deur 3 gestoord - AL51310_B */
	public static final int CH448 = 448;

	/** Deur 3 gestoord - AL51310_C1 */
	public static final int CH449 = 449;

	/** Deur 3 gestoord - AL51310_A2 */
	public static final int CH450 = 450;

	/** Trede van deur 3 gestoord - AL51311_A1 */
	public static final int CH451 = 451;

	/** Trede van deur 3 gestoord - AL51311_B */
	public static final int CH452 = 452;

	/** Trede van deur 3 gestoord - AL51311_C1 */
	public static final int CH453 = 453;

	/** Trede van deur 3 gestoord - AL51311_A2 */
	public static final int CH454 = 454;

	/** Deuren dicht signaal deur 3 onbetrouwbaar! - AL51314_A1 */
	public static final int CH455 = 455;

	/** Deuren dicht signaal deur 3 onbetrouwbaar! - AL51314_B */
	public static final int CH456 = 456;

	/** Deuren dicht signaal deur 3 onbetrouwbaar! - AL51314_C1 */
	public static final int CH457 = 457;

	/** Deuren dicht signaal deur 3 onbetrouwbaar! - AL51314_A2 */
	public static final int CH458 = 458;

	/** Eindschakelaar trede van deur 3 onbetrouwbaar! - AL51315_A1 */
	public static final int CH459 = 459;

	/** Eindschakelaar trede van deur 3 onbetrouwbaar! - AL51315_B */
	public static final int CH460 = 460;

	/** Eindschakelaar trede van deur 3 onbetrouwbaar! - AL51315_C1 */
	public static final int CH461 = 461;

	/** Eindschakelaar trede van deur 3 onbetrouwbaar! - AL51315_A2 */
	public static final int CH462 = 462;

	/** Deur/trede deur 3 niet gesloten/ingeschoven! - AL51318_A1 */
	public static final int CH463 = 463;

	/** Deur/trede deur 3 niet gesloten/ingeschoven! - AL51318_B */
	public static final int CH464 = 464;

	/** Deur/trede deur 3 niet gesloten/ingeschoven! - AL51318_C1 */
	public static final int CH465 = 465;

	/** Deur/trede deur 3 niet gesloten/ingeschoven! - AL51318_A2 */
	public static final int CH466 = 466;

	/** Drukknop sluiten deur 3 gestoord - AL51326_A1 */
	public static final int CH467 = 467;

	/** Drukknop sluiten deur 3 gestoord - AL51326_B */
	public static final int CH468 = 468;

	/** Drukknop sluiten deur 3 gestoord - AL51326_C1 */
	public static final int CH469 = 469;

	/** Drukknop sluiten deur 3 gestoord - AL51326_A2 */
	public static final int CH470 = 470;

	/** Deuren dicht signaal deur 3 gestoord - AL51327_A1 */
	public static final int CH471 = 471;

	/** Deuren dicht signaal deur 3 gestoord - AL51327_B */
	public static final int CH472 = 472;

	/** Deuren dicht signaal deur 3 gestoord - AL51327_C1 */
	public static final int CH473 = 473;

	/** Deuren dicht signaal deur 3 gestoord - AL51327_A2 */
	public static final int CH474 = 474;

	/** Deur 3 gestoord - AL51330_A1 */
	public static final int CH475 = 475;

	/** Deur 3 gestoord - AL51330_B */
	public static final int CH476 = 476;

	/** Deur 3 gestoord - AL51330_C1 */
	public static final int CH477 = 477;

	/** Deur 3 gestoord - AL51330_A2 */
	public static final int CH478 = 478;

	/** Inklembeveiliging deur 3 gestoord - AL51333_A1 */
	public static final int CH479 = 479;

	/** Inklembeveiliging deur 3 gestoord - AL51333_B */
	public static final int CH480 = 480;

	/** Inklembeveiliging deur 3 gestoord - AL51333_C1 */
	public static final int CH481 = 481;

	/** Inklembeveiliging deur 3 gestoord - AL51333_A2 */
	public static final int CH482 = 482;

	/** Inklembeveiliging trede van deur 3 gestoord - AL51334_A1 */
	public static final int CH483 = 483;

	/** Inklembeveiliging trede van deur 3 gestoord - AL51334_B */
	public static final int CH484 = 484;

	/** Inklembeveiliging trede van deur 3 gestoord - AL51334_C1 */
	public static final int CH485 = 485;

	/** Inklembeveiliging trede van deur 3 gestoord - AL51334_A2 */
	public static final int CH486 = 486;

	/** Deur 3 gestoord - AL51350_A1 */
	public static final int CH487 = 487;

	/** Deur 3 gestoord - AL51350_B */
	public static final int CH488 = 488;

	/** Deur 3 gestoord - AL51350_C1 */
	public static final int CH489 = 489;

	/** Deur 3 gestoord - AL51350_A2 */
	public static final int CH490 = 490;

	/** Trede van deur 3 gestoord - AL51356_A1 */
	public static final int CH491 = 491;

	/** Trede van deur 3 gestoord - AL51356_B */
	public static final int CH492 = 492;

	/** Trede van deur 3 gestoord - AL51356_C1 */
	public static final int CH493 = 493;

	/** Trede van deur 3 gestoord - AL51356_A2 */
	public static final int CH494 = 494;

	/** Deur 3 gestoord - AL51367_A1 */
	public static final int CH495 = 495;

	/** Deur 3 gestoord - AL51367_B */
	public static final int CH496 = 496;

	/** Deur 3 gestoord - AL51367_C1 */
	public static final int CH497 = 497;

	/** Deur 3 gestoord - AL51367_A2 */
	public static final int CH498 = 498;

	/** Trede deur 3 niet volledig ingeschoven! - AL51371_A1 */
	public static final int CH499 = 499;

	/** Trede deur 3 niet volledig ingeschoven! - AL51371_B */
	public static final int CH500 = 500;

	/** Trede deur 3 niet volledig ingeschoven! - AL51371_C1 */
	public static final int CH501 = 501;

	/** Trede deur 3 niet volledig ingeschoven! - AL51371_A2 */
	public static final int CH502 = 502;

	/** Deur 2 nood geopend! - AL52205_A1 */
	public static final int CH503 = 503;

	/** Deur 2 nood geopend! - AL52205_B */
	public static final int CH504 = 504;

	/** Deur 2 nood geopend! - AL52205_C1 */
	public static final int CH505 = 505;

	/** Deur 2 nood geopend! - AL52205_A2 */
	public static final int CH506 = 506;

	/** Deurvrijgave deur 2 gestoord! - AL52206_A1 */
	public static final int CH507 = 507;

	/** Deurvrijgave deur 2 gestoord! - AL52206_B */
	public static final int CH508 = 508;

	/** Deurvrijgave deur 2 gestoord! - AL52206_C1 */
	public static final int CH509 = 509;

	/** Deurvrijgave deur 2 gestoord! - AL52206_A2 */
	public static final int CH510 = 510;

	/** Centraal sluiten bij deur 2 gestoord - AL52207_A1 */
	public static final int CH511 = 511;

	/** Centraal sluiten bij deur 2 gestoord - AL52207_B */
	public static final int CH512 = 512;

	/** Centraal sluiten bij deur 2 gestoord - AL52207_C1 */
	public static final int CH513 = 513;

	/** Centraal sluiten bij deur 2 gestoord - AL52207_A2 */
	public static final int CH514 = 514;

	/** Deur 2 gestoord - AL52210_A1 */
	public static final int CH515 = 515;

	/** Deur 2 gestoord - AL52210_B */
	public static final int CH516 = 516;

	/** Deur 2 gestoord - AL52210_C1 */
	public static final int CH517 = 517;

	/** Deur 2 gestoord - AL52210_A2 */
	public static final int CH518 = 518;

	/** Trede van deur 2 gestoord - AL52211_A1 */
	public static final int CH519 = 519;

	/** Trede van deur 2 gestoord - AL52211_B */
	public static final int CH520 = 520;

	/** Trede van deur 2 gestoord - AL52211_C1 */
	public static final int CH521 = 521;

	/** Trede van deur 2 gestoord - AL52211_A2 */
	public static final int CH522 = 522;

	/** Deuren dicht signaal deur 2 onbetrouwbaar! - AL52214_A1 */
	public static final int CH523 = 523;

	/** Deuren dicht signaal deur 2 onbetrouwbaar! - AL52214_B */
	public static final int CH524 = 524;

	/** Deuren dicht signaal deur 2 onbetrouwbaar! - AL52214_C1 */
	public static final int CH525 = 525;

	/** Deuren dicht signaal deur 2 onbetrouwbaar! - AL52214_A2 */
	public static final int CH526 = 526;

	/** Eindschakelaar trede van deur 2 onbetrouwbaar! - AL52215_A1 */
	public static final int CH527 = 527;

	/** Eindschakelaar trede van deur 2 onbetrouwbaar! - AL52215_B */
	public static final int CH528 = 528;

	/** Eindschakelaar trede van deur 2 onbetrouwbaar! - AL52215_C1 */
	public static final int CH529 = 529;

	/** Eindschakelaar trede van deur 2 onbetrouwbaar! - AL52215_A2 */
	public static final int CH530 = 530;

	/** Deur/trede deur 2 niet gesloten/ingeschoven! - AL52218_A1 */
	public static final int CH531 = 531;

	/** Deur/trede deur 2 niet gesloten/ingeschoven! - AL52218_B */
	public static final int CH532 = 532;

	/** Deur/trede deur 2 niet gesloten/ingeschoven! - AL52218_C1 */
	public static final int CH533 = 533;

	/** Deur/trede deur 2 niet gesloten/ingeschoven! - AL52218_A2 */
	public static final int CH534 = 534;

	/** Drukknop sluiten deur 2 gestoord - AL52226_A1 */
	public static final int CH535 = 535;

	/** Drukknop sluiten deur 2 gestoord - AL52226_B */
	public static final int CH536 = 536;

	/** Drukknop sluiten deur 2 gestoord - AL52226_C1 */
	public static final int CH537 = 537;

	/** Drukknop sluiten deur 2 gestoord - AL52226_A2 */
	public static final int CH538 = 538;

	/** Deuren dicht signaal deur 2 gestoord - AL52227_A1 */
	public static final int CH539 = 539;

	/** Deuren dicht signaal deur 2 gestoord - AL52227_B */
	public static final int CH540 = 540;

	/** Deuren dicht signaal deur 2 gestoord - AL52227_C1 */
	public static final int CH541 = 541;

	/** Deuren dicht signaal deur 2 gestoord - AL52227_A2 */
	public static final int CH542 = 542;

	/** Deur 2 gestoord - AL52230_A1 */
	public static final int CH543 = 543;

	/** Deur 2 gestoord - AL52230_B */
	public static final int CH544 = 544;

	/** Deur 2 gestoord - AL52230_C1 */
	public static final int CH545 = 545;

	/** Deur 2 gestoord - AL52230_A2 */
	public static final int CH546 = 546;

	/** Inklembeveiliging deur 2 gestoord - AL52233_A1 */
	public static final int CH547 = 547;

	/** Inklembeveiliging deur 2 gestoord - AL52233_B */
	public static final int CH548 = 548;

	/** Inklembeveiliging deur 2 gestoord - AL52233_C1 */
	public static final int CH549 = 549;

	/** Inklembeveiliging deur 2 gestoord - AL52233_A2 */
	public static final int CH550 = 550;

	/** Inklembeveiliging trede van deur 2 gestoord - AL52234_A1 */
	public static final int CH551 = 551;

	/** Inklembeveiliging trede van deur 2 gestoord - AL52234_B */
	public static final int CH552 = 552;

	/** Inklembeveiliging trede van deur 2 gestoord - AL52234_C1 */
	public static final int CH553 = 553;

	/** Inklembeveiliging trede van deur 2 gestoord - AL52234_A2 */
	public static final int CH554 = 554;

	/** Deur 2 gestoord - AL52250_A1 */
	public static final int CH555 = 555;

	/** Deur 2 gestoord - AL52250_B */
	public static final int CH556 = 556;

	/** Deur 2 gestoord - AL52250_C1 */
	public static final int CH557 = 557;

	/** Deur 2 gestoord - AL52250_A2 */
	public static final int CH558 = 558;

	/** Trede van deur 2 gestoord - AL52256_A1 */
	public static final int CH559 = 559;

	/** Trede van deur 2 gestoord - AL52256_B */
	public static final int CH560 = 560;

	/** Trede van deur 2 gestoord - AL52256_C1 */
	public static final int CH561 = 561;

	/** Trede van deur 2 gestoord - AL52256_A2 */
	public static final int CH562 = 562;

	/** Deur 2 gestoord - AL52267_A1 */
	public static final int CH563 = 563;

	/** Deur 2 gestoord - AL52267_B */
	public static final int CH564 = 564;

	/** Deur 2 gestoord - AL52267_C1 */
	public static final int CH565 = 565;

	/** Deur 2 gestoord - AL52267_A2 */
	public static final int CH566 = 566;

	/** Trede deur 2 niet volledig ingeschoven! - AL52271_A1 */
	public static final int CH567 = 567;

	/** Trede deur 2 niet volledig ingeschoven! - AL52271_B */
	public static final int CH568 = 568;

	/** Trede deur 2 niet volledig ingeschoven! - AL52271_C1 */
	public static final int CH569 = 569;

	/** Trede deur 2 niet volledig ingeschoven! - AL52271_A2 */
	public static final int CH570 = 570;

	/** Deur 4 nood geopend! - AL52405_A1 */
	public static final int CH571 = 571;

	/** Deur 4 nood geopend! - AL52405_B */
	public static final int CH572 = 572;

	/** Deur 4 nood geopend! - AL52405_C1 */
	public static final int CH573 = 573;

	/** Deur 4 nood geopend! - AL52405_A2 */
	public static final int CH574 = 574;

	/** Deurvrijgave deur 4 gestoord! - AL52406_A1 */
	public static final int CH575 = 575;

	/** Deurvrijgave deur 4 gestoord! - AL52406_B */
	public static final int CH576 = 576;

	/** Deurvrijgave deur 4 gestoord! - AL52406_C1 */
	public static final int CH577 = 577;

	/** Deurvrijgave deur 4 gestoord! - AL52406_A2 */
	public static final int CH578 = 578;

	/** Centraal sluiten bij deur 4 gestoord - AL52407_A1 */
	public static final int CH579 = 579;

	/** Centraal sluiten bij deur 4 gestoord - AL52407_B */
	public static final int CH580 = 580;

	/** Centraal sluiten bij deur 4 gestoord - AL52407_C1 */
	public static final int CH581 = 581;

	/** Centraal sluiten bij deur 4 gestoord - AL52407_A2 */
	public static final int CH582 = 582;

	/** Deur 4 gestoord - AL52410_A1 */
	public static final int CH583 = 583;

	/** Deur 4 gestoord - AL52410_B */
	public static final int CH584 = 584;

	/** Deur 4 gestoord - AL52410_C1 */
	public static final int CH585 = 585;

	/** Deur 4 gestoord - AL52410_A2 */
	public static final int CH586 = 586;

	/** Trede van deur 4 gestoord - AL52411_A1 */
	public static final int CH587 = 587;

	/** Trede van deur 4 gestoord - AL52411_B */
	public static final int CH588 = 588;

	/** Trede van deur 4 gestoord - AL52411_C1 */
	public static final int CH589 = 589;

	/** Trede van deur 4 gestoord - AL52411_A2 */
	public static final int CH590 = 590;

	/** Deuren dicht signaal deur 4 onbetrouwbaar! - AL52414_A1 */
	public static final int CH591 = 591;

	/** Deuren dicht signaal deur 4 onbetrouwbaar! - AL52414_B */
	public static final int CH592 = 592;

	/** Deuren dicht signaal deur 4 onbetrouwbaar! - AL52414_C1 */
	public static final int CH593 = 593;

	/** Deuren dicht signaal deur 4 onbetrouwbaar! - AL52414_A2 */
	public static final int CH594 = 594;

	/** Eindschakelaar trede van deur 4 onbetrouwbaar! - AL52415_A1 */
	public static final int CH595 = 595;

	/** Eindschakelaar trede van deur 4 onbetrouwbaar! - AL52415_B */
	public static final int CH596 = 596;

	/** Eindschakelaar trede van deur 4 onbetrouwbaar! - AL52415_C1 */
	public static final int CH597 = 597;

	/** Eindschakelaar trede van deur 4 onbetrouwbaar! - AL52415_A2 */
	public static final int CH598 = 598;

	/** Deur/trede deur 4 niet gesloten/ingeschoven! - AL52418_A1 */
	public static final int CH599 = 599;

	/** Deur/trede deur 4 niet gesloten/ingeschoven! - AL52418_B */
	public static final int CH600 = 600;

	/** Deur/trede deur 4 niet gesloten/ingeschoven! - AL52418_C1 */
	public static final int CH601 = 601;

	/** Deur/trede deur 4 niet gesloten/ingeschoven! - AL52418_A2 */
	public static final int CH602 = 602;

	/** Drukknop sluiten deur 4 gestoord - AL52426_A1 */
	public static final int CH603 = 603;

	/** Drukknop sluiten deur 4 gestoord - AL52426_B */
	public static final int CH604 = 604;

	/** Drukknop sluiten deur 4 gestoord - AL52426_C1 */
	public static final int CH605 = 605;

	/** Drukknop sluiten deur 4 gestoord - AL52426_A2 */
	public static final int CH606 = 606;

	/** Deuren dicht signaal deur 4 gestoord - AL52427_A1 */
	public static final int CH607 = 607;

	/** Deuren dicht signaal deur 4 gestoord - AL52427_B */
	public static final int CH608 = 608;

	/** Deuren dicht signaal deur 4 gestoord - AL52427_C1 */
	public static final int CH609 = 609;

	/** Deuren dicht signaal deur 4 gestoord - AL52427_A2 */
	public static final int CH610 = 610;

	/** Deur 4 gestoord - AL52430_A1 */
	public static final int CH611 = 611;

	/** Deur 4 gestoord - AL52430_B */
	public static final int CH612 = 612;

	/** Deur 4 gestoord - AL52430_C1 */
	public static final int CH613 = 613;

	/** Deur 4 gestoord - AL52430_A2 */
	public static final int CH614 = 614;

	/** Inklembeveiliging deur 4 gestoord - AL52433_A1 */
	public static final int CH615 = 615;

	/** Inklembeveiliging deur 4 gestoord - AL52433_B */
	public static final int CH616 = 616;

	/** Inklembeveiliging deur 4 gestoord - AL52433_C1 */
	public static final int CH617 = 617;

	/** Inklembeveiliging deur 4 gestoord - AL52433_A2 */
	public static final int CH618 = 618;

	/** Inklembeveiliging trede van deur 4 gestoord - AL52434_A1 */
	public static final int CH619 = 619;

	/** Inklembeveiliging trede van deur 4 gestoord - AL52434_B */
	public static final int CH620 = 620;

	/** Inklembeveiliging trede van deur 4 gestoord - AL52434_C1 */
	public static final int CH621 = 621;

	/** Inklembeveiliging trede van deur 4 gestoord - AL52434_A2 */
	public static final int CH622 = 622;

	/** Deur 4 gestoord - AL52450_A1 */
	public static final int CH623 = 623;

	/** Deur 4 gestoord - AL52450_B */
	public static final int CH624 = 624;

	/** Deur 4 gestoord - AL52450_C1 */
	public static final int CH625 = 625;

	/** Deur 4 gestoord - AL52450_A2 */
	public static final int CH626 = 626;

	/** Trede van deur 4 gestoord - AL52456_A1 */
	public static final int CH627 = 627;

	/** Trede van deur 4 gestoord - AL52456_B */
	public static final int CH628 = 628;

	/** Trede van deur 4 gestoord - AL52456_C1 */
	public static final int CH629 = 629;

	/** Trede van deur 4 gestoord - AL52456_A2 */
	public static final int CH630 = 630;

	/** Deur 4 gestoord - AL52467_A1 */
	public static final int CH631 = 631;

	/** Deur 4 gestoord - AL52467_B */
	public static final int CH632 = 632;

	/** Deur 4 gestoord - AL52467_C1 */
	public static final int CH633 = 633;

	/** Deur 4 gestoord - AL52467_A2 */
	public static final int CH634 = 634;

	/** Trede deur 4 niet volledig ingeschoven! - AL52471_A1 */
	public static final int CH635 = 635;

	/** Trede deur 4 niet volledig ingeschoven! - AL52471_B */
	public static final int CH636 = 636;

	/** Trede deur 4 niet volledig ingeschoven! - AL52471_C1 */
	public static final int CH637 = 637;

	/** Trede deur 4 niet volledig ingeschoven! - AL52471_A2 */
	public static final int CH638 = 638;

	/** Lamp deuren dicht uitgeschakeld op cabine achterwand - AL5410_A1 */
	public static final int CH639 = 639;

	/** Lamp deuren dicht uitgeschakeld op cabine achterwand - AL5410_A2 */
	public static final int CH640 = 640;

	/** Signalering deuren dicht gestoord - AL5411_A1 */
	public static final int CH641 = 641;

	/** Signalering deuren dicht gestoord - AL5411_A2 */
	public static final int CH642 = 642;

	/** Signalering deuren dicht gestoord - AL5412_A1 */
	public static final int CH643 = 643;

	/** Signalering deuren dicht gestoord - AL5412_B */
	public static final int CH644 = 644;

	/** Signalering deuren dicht gestoord - AL5412_C1 */
	public static final int CH645 = 645;

	/** Signalering deuren dicht gestoord - AL5412_A2 */
	public static final int CH646 = 646;

	/** Signalering deuren dicht gestoord - AL5413_A1 */
	public static final int CH647 = 647;

	/** Signalering deuren dicht gestoord - AL5413_B */
	public static final int CH648 = 648;

	/** Signalering deuren dicht gestoord - AL5413_C1 */
	public static final int CH649 = 649;

	/** Signalering deuren dicht gestoord - AL5413_A2 */
	public static final int CH650 = 650;

	/** Signalering deuren dicht circuit gestoord! - AL5415_A1 */
	public static final int CH651 = 651;

	/** Signalering deuren dicht circuit gestoord! - AL5415_A2 */
	public static final int CH652 = 652;

	/** Compressor uitgevallen - AL6001_B */
	public static final int CH653 = 653;

	/** Compressor gestoord - AL6002_B */
	public static final int CH654 = 654;

	/** Compressor gestoord - AL6003_B */
	public static final int CH655 = 655;

	/** Compressor uitgevallen - AL6005_B */
	public static final int CH656 = 656;

	/** Compressor gestoord - AL6006_B */
	public static final int CH657 = 657;

	/** Hoofdreservoir afsluitkraan compressor afgesloten - AL6010_B */
	public static final int CH658 = 658;

	/** Klimaatsysteem cabine gestoord - AL7001_A1 */
	public static final int CH659 = 659;

	/** Klimaatsysteem cabine gestoord - AL7001_A2 */
	public static final int CH660 = 660;

	/** Klimaatsysteem cabine gestoord - AL7003_A1 */
	public static final int CH661 = 661;

	/** Klimaatsysteem cabine gestoord - AL7003_A2 */
	public static final int CH662 = 662;

	/** Noodventilatie uitgeschakeld op cabine achterwand - AL7005_A1 */
	public static final int CH663 = 663;

	/** Noodventilatie uitgeschakeld op cabine achterwand - AL7005_A2 */
	public static final int CH664 = 664;

	/** Rijtuig klimaat uitgeschakeld op cabine achterwand - AL7006_UT */
	public static final int CH665 = 665;

	/** Klimaatsysteem cabine in limp mode - AL7013_A1 */
	public static final int CH666 = 666;

	/** Klimaatsysteem cabine in limp mode - AL7013_A2 */
	public static final int CH667 = 667;

	/** Klimaatsysteem cabine gestoord - AL7015_A1 */
	public static final int CH668 = 668;

	/** Klimaatsysteem cabine gestoord - AL7015_A2 */
	public static final int CH669 = 669;

	/** Klimaatsysteem cabine gestoord - AL7016_A1 */
	public static final int CH670 = 670;

	/** Klimaatsysteem cabine gestoord - AL7016_A2 */
	public static final int CH671 = 671;

	/** Klimaatsysteem cabine gestoord - AL7017_A1 */
	public static final int CH672 = 672;

	/** Klimaatsysteem cabine gestoord - AL7017_A2 */
	public static final int CH673 = 673;

	/** Klimaatsysteem cabine gestoord - AL7018_A1 */
	public static final int CH674 = 674;

	/** Klimaatsysteem cabine gestoord - AL7018_A2 */
	public static final int CH675 = 675;

	/** Convector verwarming cabine gestoord - AL7019_A1 */
	public static final int CH676 = 676;

	/** Convector verwarming cabine gestoord - AL7019_A2 */
	public static final int CH677 = 677;

	/** Klimaatsysteem reizigers gestoord - AL7101_A1 */
	public static final int CH678 = 678;

	/** Klimaatsysteem reizigers gestoord - AL7101_B */
	public static final int CH679 = 679;

	/** Klimaatsysteem reizigers gestoord - AL7101_C1 */
	public static final int CH680 = 680;

	/** Klimaatsysteem reizigers gestoord - AL7101_A2 */
	public static final int CH681 = 681;

	/** Klimaatsysteem reizigers gestoord - AL7102_A1 */
	public static final int CH682 = 682;

	/** Klimaatsysteem reizigers gestoord - AL7102_B */
	public static final int CH683 = 683;

	/** Klimaatsysteem reizigers gestoord - AL7102_C1 */
	public static final int CH684 = 684;

	/** Klimaatsysteem reizigers gestoord - AL7102_A2 */
	public static final int CH685 = 685;

	/** Klimaatsysteem reizigers gestoord - AL7103_A1 */
	public static final int CH686 = 686;

	/** Klimaatsysteem reizigers gestoord - AL7103_B */
	public static final int CH687 = 687;

	/** Klimaatsysteem reizigers gestoord - AL7103_C1 */
	public static final int CH688 = 688;

	/** Klimaatsysteem reizigers gestoord - AL7103_A2 */
	public static final int CH689 = 689;

	/** Klimaatsysteem reizigers in limp mode - AL7113_A1 */
	public static final int CH690 = 690;

	/** Klimaatsysteem reizigers in limp mode - AL7113_B */
	public static final int CH691 = 691;

	/** Klimaatsysteem reizigers in limp mode - AL7113_C1 */
	public static final int CH692 = 692;

	/** Klimaatsysteem reizigers in limp mode - AL7113_A2 */
	public static final int CH693 = 693;

	/** Klimaatsysteem reizigers gestoord - AL7114_A1 */
	public static final int CH694 = 694;

	/** Klimaatsysteem reizigers gestoord - AL7114_B */
	public static final int CH695 = 695;

	/** Klimaatsysteem reizigers gestoord - AL7114_C1 */
	public static final int CH696 = 696;

	/** Klimaatsysteem reizigers gestoord - AL7114_A2 */
	public static final int CH697 = 697;

	/** Klimaatsysteem reizigers gestoord - AL7115_A1 */
	public static final int CH698 = 698;

	/** Klimaatsysteem reizigers gestoord - AL7115_B */
	public static final int CH699 = 699;

	/** Klimaatsysteem reizigers gestoord - AL7115_C1 */
	public static final int CH700 = 700;

	/** Klimaatsysteem reizigers gestoord - AL7115_A2 */
	public static final int CH701 = 701;

	/** Ventilatie klimaatsysteem reizigers gestoord - AL7116_A1 */
	public static final int CH702 = 702;

	/** Ventilatie klimaatsysteem reizigers gestoord - AL7116_B */
	public static final int CH703 = 703;

	/** Ventilatie klimaatsysteem reizigers gestoord - AL7116_C1 */
	public static final int CH704 = 704;

	/** Ventilatie klimaatsysteem reizigers gestoord - AL7116_A2 */
	public static final int CH705 = 705;

	/** Klimaatsysteem reizigers gestoord - AL7117_A1 */
	public static final int CH706 = 706;

	/** Klimaatsysteem reizigers gestoord - AL7117_B */
	public static final int CH707 = 707;

	/** Klimaatsysteem reizigers gestoord - AL7117_C1 */
	public static final int CH708 = 708;

	/** Klimaatsysteem reizigers gestoord - AL7117_A2 */
	public static final int CH709 = 709;

	/** Verwarming reizigers gestoord - AL7118_A1 */
	public static final int CH710 = 710;

	/** Verwarming reizigers gestoord - AL7118_B */
	public static final int CH711 = 711;

	/** Verwarming reizigers gestoord - AL7118_C1 */
	public static final int CH712 = 712;

	/** Verwarming reizigers gestoord - AL7118_A2 */
	public static final int CH713 = 713;

	/** Verwarming reizigers gestoord - AL7119_A1 */
	public static final int CH714 = 714;

	/** Verwarming reizigers gestoord - AL7119_B */
	public static final int CH715 = 715;

	/** Verwarming reizigers gestoord - AL7119_C1 */
	public static final int CH716 = 716;

	/** Verwarming reizigers gestoord - AL7119_A2 */
	public static final int CH717 = 717;

	/** Cabineverlichting en/of interieurverlichting gestoord - AL7401_A1 */
	public static final int CH718 = 718;

	/** Cabineverlichting en/of interieurverlichting gestoord - AL7401_A2 */
	public static final int CH719 = 719;

	/** Interieurverlichting gestoord - AL7402_B */
	public static final int CH720 = 720;

	/** Interieurverlichting gestoord - AL7402_C1 */
	public static final int CH721 = 721;

	/** Front- en/of sluitseinen gestoord - AL7601_A1 */
	public static final int CH722 = 722;

	/** Front- en/of sluitseinen gestoord - AL7601_A2 */
	public static final int CH723 = 723;

	/** Gevaarsein ingeschakeld - AL7611_A1 */
	public static final int CH724 = 724;

	/** Gevaarsein ingeschakeld - AL7611_A2 */
	public static final int CH725 = 725;

	/** GSM-R gestoord - AL8001_A1 */
	public static final int CH726 = 726;

	/** GSM-R gestoord - AL8001_A2 */
	public static final int CH727 = 727;

	/** Cabine linker scherm (ETD) gestoord - AL8101_A1 */
	public static final int CH728 = 728;

	/** Cabine linker scherm (ETD) gestoord - AL8101_A2 */
	public static final int CH729 = 729;

	/** Omroep gestoord - AL8301_A1 */
	public static final int CH730 = 730;

	/** Omroep gestoord - AL8302_B */
	public static final int CH731 = 731;

	/** Omroep gestoord - AL8302_C1 */
	public static final int CH732 = 732;

	/** Omroep gestoord - AL8302_A2 */
	public static final int CH733 = 733;

	/** Bestemmingsaanduiding of RIS/OBIS schermen gestoord - AL8303_A1 */
	public static final int CH734 = 734;

	/** Bestemmingsaanduiding of RIS/OBIS schermen gestoord - AL8303_B */
	public static final int CH735 = 735;

	/** Bestemmingsaanduiding of RIS/OBIS schermen gestoord - AL8303_C1 */
	public static final int CH736 = 736;

	/** Bestemmingsaanduiding of RIS/OBIS schermen gestoord - AL8303_A2 */
	public static final int CH737 = 737;

	/** Omroep gestoord - AL8350_A1 */
	public static final int CH738 = 738;

	/** Omroep gestoord - AL8350_B */
	public static final int CH739 = 739;

	/** Omroep gestoord - AL8350_C1 */
	public static final int CH740 = 740;

	/** Omroep gestoord - AL8350_A2 */
	public static final int CH741 = 741;

	/** Omroep vanuit cabine gestoord - AL8351_A1 */
	public static final int CH742 = 742;

	/** Omroep vanuit cabine gestoord - AL8351_A2 */
	public static final int CH743 = 743;

	/** Omroep in storingsbedrijf - AL8355_UT */
	public static final int CH744 = 744;

	/** HMI gestoord in niet actieve cabine - AL8701_A1 */
	public static final int CH745 = 745;

	/** HMI gestoord in niet actieve cabine - AL8701_A2 */
	public static final int CH746 = 746;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8702_A1 */
	public static final int CH747 = 747;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8702_A2 */
	public static final int CH748 = 748;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8703_A1 */
	public static final int CH749 = 749;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8703_A2 */
	public static final int CH750 = 750;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8705_B */
	public static final int CH751 = 751;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8706_B */
	public static final int CH752 = 752;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8707_C1 */
	public static final int CH753 = 753;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8708_C1 */
	public static final int CH754 = 754;

	/** Werkplaats diagnose (SDIAG) gestoord - AL8709_B */
	public static final int CH755 = 755;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8710_A1 */
	public static final int CH756 = 756;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8710_B */
	public static final int CH757 = 757;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8710_C1 */
	public static final int CH758 = 758;

	/** Centrale computer (IO1 van TCMS) gestoord - AL8710_A2 */
	public static final int CH759 = 759;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8711_A1 */
	public static final int CH760 = 760;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8711_B */
	public static final int CH761 = 761;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8711_C1 */
	public static final int CH762 = 762;

	/** Centrale computer (IO2 van TCMS) gestoord - AL8711_A2 */
	public static final int CH763 = 763;

	/** Centrale computer (IO3 van TCMS) gestoord - AL8712_A1 */
	public static final int CH764 = 764;

	/** Centrale computer (IO3 van TCMS) gestoord - AL8712_A2 */
	public static final int CH765 = 765;

	/** HMI gestoord - AL8713_A1 */
	public static final int CH766 = 766;

	/** HMI gestoord - AL8713_A2 */
	public static final int CH767 = 767;

	/** Centrale computer (TCMS) gestoord - AL8715_A1 */
	public static final int CH768 = 768;

	/** Centrale computer (TCMS) gestoord - AL8715_B */
	public static final int CH769 = 769;

	/** Centrale computer (TCMS) gestoord - AL8715_C1 */
	public static final int CH770 = 770;

	/** Centrale computer (TCMS) gestoord - AL8715_A2 */
	public static final int CH771 = 771;

	/** Centrale computer (TCMS) gestoord - AL8730_A1 */
	public static final int CH772 = 772;

	/** Centrale computer (TCMS) gestoord - AL8730_B */
	public static final int CH773 = 773;

	/** Centrale computer (TCMS) gestoord - AL8730_C1 */
	public static final int CH774 = 774;

	/** Centrale computer (TCMS) gestoord - AL8730_A2 */
	public static final int CH775 = 775;

	/** Centrale computer (TCMS) gestoord - AL8731_A1 */
	public static final int CH776 = 776;

	/** Centrale computer (TCMS) gestoord - AL8731_B */
	public static final int CH777 = 777;

	/** Centrale computer (TCMS) gestoord - AL8731_C1 */
	public static final int CH778 = 778;

	/** Centrale computer (TCMS) gestoord - AL8731_A2 */
	public static final int CH779 = 779;

	/** Centrale computer (TCMS) gestoord - AL8732_A1 */
	public static final int CH780 = 780;

	/** Centrale computer (TCMS) gestoord - AL8732_A2 */
	public static final int CH781 = 781;

	/** Centrale computer (IO1_1 van TCMS) gestoord - AL8735_A1 */
	public static final int CH782 = 782;

	/** Centrale computer (IO1_1 van TCMS) gestoord - AL8735_B */
	public static final int CH783 = 783;

	/** Centrale computer (IO1_1 van TCMS) gestoord - AL8735_C1 */
	public static final int CH784 = 784;

	/** Centrale computer (IO1_1 van TCMS) gestoord - AL8735_A2 */
	public static final int CH785 = 785;

	/** Centrale computer (TCMS) gestoord - AL8736_A1 */
	public static final int CH786 = 786;

	/** Centrale computer (TCMS) gestoord - AL8736_B */
	public static final int CH787 = 787;

	/** Centrale computer (TCMS) gestoord - AL8736_C1 */
	public static final int CH788 = 788;

	/** Centrale computer (TCMS) gestoord - AL8736_A2 */
	public static final int CH789 = 789;

	/** Centrale computer (TCMS) gestoord - AL8737_A1 */
	public static final int CH790 = 790;

	/** Centrale computer (TCMS) gestoord - AL8737_A2 */
	public static final int CH791 = 791;

	/** Parameters in CCU niet plausibel. - AL8775_UT */
	public static final int CH792 = 792;

	/** Trein samenstelling niet plausibel! - AL8778_UT */
	public static final int CH793 = 793;

	/** Trein samenstelling niet plausibel! - AL8779_UT */
	public static final int CH794 = 794;

	/** Trein samenstelling niet plausibel! - AL8781_UT */
	public static final int CH795 = 795;

	/** Trein samenstelling niet plausibel! - AL8782_UT */
	public static final int CH796 = 796;

	/** Relais trein gekoppeld gestoord. - AL8783_A1 */
	public static final int CH797 = 797;

	/** Relais trein gekoppeld gestoord. - AL8783_A2 */
	public static final int CH798 = 798;

	/** Ethernet switch comfort netwerk gestoord - AL8801_A1 */
	public static final int CH799 = 799;

	/** Ethernet switch comfort netwerk gestoord - AL8801_B */
	public static final int CH800 = 800;

	/** Ethernet switch comfort netwerk gestoord - AL8801_C1 */
	public static final int CH801 = 801;

	/** Ethernet switch comfort netwerk gestoord - AL8801_A2 */
	public static final int CH802 = 802;

	/** Ethernet switch comfort netwerk gestoord - AL8802_A1 */
	public static final int CH803 = 803;

	/** Ethernet switch comfort netwerk gestoord - AL8802_B */
	public static final int CH804 = 804;

	/** Ethernet switch comfort netwerk gestoord - AL8802_C1 */
	public static final int CH805 = 805;

	/** Ethernet switch comfort netwerk gestoord - AL8802_A2 */
	public static final int CH806 = 806;

	/** Ethernet switch maintenance netwerk gestoord - AL8803_A1 */
	public static final int CH807 = 807;

	/** Ethernet switch maintenance netwerk gestoord - AL8803_B */
	public static final int CH808 = 808;

	/** Ethernet switch maintenance netwerk gestoord - AL8803_C1 */
	public static final int CH809 = 809;

	/** Ethernet switch maintenance netwerk gestoord - AL8803_A2 */
	public static final int CH810 = 810;

	/** RIS/OBIS gestoord - AL8804_B */
	public static final int CH811 = 811;

	/** OBIS server gestoord - AL8853_UT */
	public static final int CH812 = 812;

	/** OBIS reisinformatie gestoord - AL8854_UT */
	public static final int CH813 = 813;

	/** OBIS ETD applicaties gestoord - AL8855_UT */
	public static final int CH814 = 814;

	/** OBIS omroep gestoord - AL8856_UT */
	public static final int CH815 = 815;

	/** OBIS BBA gestoord - AL8857_UT */
	public static final int CH816 = 816;

	/** OBIS PIS gestoord - AL8858_UT */
	public static final int CH817 = 817;

	/** ETD gestoord - AL8859_A1 */
	public static final int CH818 = 818;

	/** ETD gestoord - AL8859_A2 */
	public static final int CH819 = 819;

	/** OBIS CCU gestoord - AL8860_B */
	public static final int CH820 = 820;

	/** Diverse cabine functies gestoord - AL9001_A1 */
	public static final int CH821 = 821;

	/** Diverse cabine functies gestoord - AL9001_A2 */
	public static final int CH822 = 822;

	/** Beide tyfoons gestoord - AL9003_A1 */
	public static final int CH823 = 823;

	/** Beide tyfoons gestoord - AL9003_A2 */
	public static final int CH824 = 824;

	/** Toilet gestoord - AL9501_B */
	public static final int CH825 = 825;

	/** Toilet gestoord - AL9502_B */
	public static final int CH826 = 826;

	/** Elektrische systemen in toilet gestoord - AL9503_B */
	public static final int CH827 = 827;

	/** Call for aid knop bediend in toilet - AL9559_B */
	public static final int CH828 = 828;

	/** Toilet gestoord - AL9560_B */
	public static final int CH829 = 829;

	/** SDD waarschuwing - AL2197_A1 */
	public static final int CH830 = 830;

	/** SDD waarschuwing - AL2197_A2 */
	public static final int CH831 = 831;

	/** Magneetrem - AL3754_A1 */
	public static final int CH832 = 832;

	/** Magneetrem - AL3754_A2 */
	public static final int CH833 = 833;

	/** Magneetrem - AL3754_C1 */
	public static final int CH834 = 834;

	/** Magneetrem - AL3754_B */
	public static final int CH835 = 835;
	
	/** Laatste GPS breedtegraad - VT_GPSLatitude */
	public static final int CH2001 = 2001;

	/** Laatste GPS lengtegraad - VT_GPSLongitude */
	public static final int CH2002 = 2002;

	/** Laatste GPS snelheid - VT_GPSSpeed */
	public static final int CH2003 = 2003;

}