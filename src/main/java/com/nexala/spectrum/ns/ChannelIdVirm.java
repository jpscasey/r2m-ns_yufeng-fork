package com.nexala.spectrum.ns;

public class ChannelIdVirm {
    /** MCG_ILatitude - Laatste breedtegraad */
    public static final int CH1 = 1;

    /** MCG_ILongitude - Laatste lengtegraad */
    public static final int CH2 = 2;

    /** 36A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH3 = 3;

    /** 36A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH4 = 4;

    /** 36A1Verweindrin - Verwarming eindruimte */
    public static final int CH5 = 5;

    /** 317A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH6 = 6;

    /** 327A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH7 = 7;

    /** 337A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH8 = 8;

    /** 347A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH9 = 9;

    /** 34A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH10 = 10;

    /** 38A3_Compressor_1_IN - Compressor 1 in */
    public static final int CH11 = 11;

    /** 38A3_Druk__8_BAR - Druk < 8bar */
    public static final int CH12 = 12;

    /** 56A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH13 = 13;

    /** 56A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH14 = 14;

    /** 56A1Verweindrin - Verwarming eindruimte */
    public static final int CH15 = 15;

    /** 517A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH16 = 16;

    /** 527A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH17 = 17;

    /** 537A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH18 = 18;

    /** 547A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH19 = 19;

    /** 54A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH20 = 20;

    /** 64B5_HR_DRUK - Hoofdreservoir druk */
    public static final int CH21 = 21;

    /** 6Treinleidingdruk - Treinleiding druk */
    public static final int CH22 = 22;

    /** 66A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH23 = 23;

    /** 66A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH24 = 24;

    /** 66A1Verweindrin - Verwarming eindruimte */
    public static final int CH25 = 25;

    /** 617A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH26 = 26;

    /** 627A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH27 = 27;

    /** 637A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH28 = 28;

    /** 647A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH29 = 29;

    /** 64A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH30 = 30;

    /** 68A3_Compressor_Gevraagd - Treindraad 402 compressor gevraagd */
    public static final int CH31 = 31;

    /** 6TLdrukkleinerals05Bar - TL druk kleiner als 0.5Bar */
    public static final int CH32 = 32;

    /** 68A2_Druk__5_BAR - Druk lager dan 5 bar */
    public static final int CH33 = 33;

    /** 68A2_Druk__85_BAR - Druk hoger dan 8 */
    public static final int CH34 = 34;

    /** 68A2_Druk__95_BAR - Druk hoger dan 9 */
    public static final int CH35 = 35;

    /** 73A8_MW_UBAT_VOLT - Gemeten batterijspanning */
    public static final int CH36 = 36;

    /** 73A2_M_UT - Uitgangsspanning 2 */
    public static final int CH37 = 37;

    /** 73A2_M_IT_GEM - Uitgangsstroom 1 */
    public static final int CH38 = 38;

    /** 76A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH39 = 39;

    /** 76A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH40 = 40;

    /** 76A1Verweindrin - Verwarming eindruimte */
    public static final int CH41 = 41;

    /** 717A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH42 = 42;

    /** 727A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH43 = 43;

    /** 737A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH44 = 44;

    /** 747A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH45 = 45;

    /** 74A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH46 = 46;

    /** 74A1_SLIP_ABI - Schlupf / schleudern Ausgang */
    public static final int CH47 = 47;

    /** 72A1_BELADING - VTI_belading */
    public static final int CH48 = 48;

    /** 72A1_IN_Gevraagd_Koppel - VTI_gevraagd_koppel */
    public static final int CH49 = 49;

    /** 72A1_IN_Vooruit - CF_trdr_vooruit */
    public static final int CH50 = 50;

    /** 72A1_IN_Achteruit - CF_trdr_achteruit */
    public static final int CH51 = 51;

    /** 7Actueelkoppel - Actuele tractie */
    public static final int CH52 = 52;

    /** 72A1_ILACT - Actuele Il */
    public static final int CH53 = 53;

    /** 72A1_METING_LIJNSPANNING - Actuele Ul */
    public static final int CH54 = 54;

    /** 72A1_Inverter_Frequentie - Inverter frequentie */
    public static final int CH55 = 55;

    /** 72A1_Detectie_Wielslip - Wielslip gedetecteerd */
    public static final int CH56 = 56;

    /** 78A4_IN_HS_AANWEZIG - Hoogspanning aanwezig */
    public static final int CH57 = 57;

    /** 72A1_Tractie_Levert_Koppel - Tractie inst. levert koppel */
    public static final int CH58 = 58;

    /** 13A8_MW_UBAT_VOLT - Gemeten batterijspanning */
    public static final int CH59 = 59;

    /** 13A2_M_UT - Uitgangsspanning 2 */
    public static final int CH60 = 60;

    /** 13A2_M_IT_GEM - Uitgangsstroom 1 */
    public static final int CH61 = 61;

    /** 18A3_Gereed_Continu - Bedrijfstoestand gereed continu */
    public static final int CH62 = 62;

    /** 18A3_Bediende_Cabine - Bediende cabine */
    public static final int CH63 = 63;

    /** 18A3_Bedrijfstoestand_Gereed - 73A2_M_UT */
    public static final int CH64 = 64;

    /** 18A3_IN_Dienstvaardig - Bedrijfstoestand dienstvaardig */
    public static final int CH65 = 65;

    /** 18A4_ATB_Snelrem - ATB Snelrem */
    public static final int CH66 = 66;

    /** 18A4_IN_HS_AANWEZIG - Hoogspanning aanwezig */
    public static final int CH67 = 67;

    /** 16A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH68 = 68;

    /** 16A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH69 = 69;

    /** 16A1Verwcabinein - Verwarming cabine */
    public static final int CH70 = 70;

    /** 117A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH71 = 71;

    /** 127A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH72 = 72;

    /** 137A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH73 = 73;

    /** 147A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH74 = 74;

    /** 14A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH75 = 75;

    /** 14A1_SLIP_ABI - Schlupf / schleudern Ausgang */
    public static final int CH76 = 76;

    /** 1standremkraan5 - 5.Brems-Stellung */
    public static final int CH77 = 77;

    /** 1standremkraan4 - 4.Brems-Stellung */
    public static final int CH78 = 78;

    /** 1standremkraan3 - 3.Brems-Stellung */
    public static final int CH79 = 79;

    /** 1standremkraan2 - 2.Brems-Stellung */
    public static final int CH80 = 80;

    /** 1standremkraan1 - 1.Brems-Stellung */
    public static final int CH81 = 81;

    /** 1standremkraanT - Fahr-Stellung */
    public static final int CH82 = 82;

    /** 1standremkraanA - Abschluss-Stellung */
    public static final int CH83 = 83;

    /** 14A1_Rem_Noodbedrijf - Notbetrieb */
    public static final int CH84 = 84;

    /** 14A1_ST_SB - Schnellbrems-Stellung */
    public static final int CH85 = 85;

    /** 1standremkraan7 - 7.Brems-Stellung */
    public static final int CH86 = 86;

    /** 1standremkraan6 - 6.Brems-Stellung */
    public static final int CH87 = 87;

    /** 14A1Noodremreizigers - Reisende-Notbremse Alarm */
    public static final int CH88 = 88;

    /** 14A1_Ingang_Treindraad_17 - 42917 Stufe (Zugsignal) */
    public static final int CH89 = 89;

    /** 14A1_Ingang_Treindraad_27 - 42918 Stufe (Zugsignal) */
    public static final int CH90 = 90;

    /** 14A1_Ingang_Treindraad_47 - 42920 Stufe (Zugsignal) */
    public static final int CH91 = 91;

    /** 12A1_BELADING - VTI_belading */
    public static final int CH92 = 92;

    /** 12A1_IN_Gevraagd_Koppel - VTI_gevraagd_koppel */
    public static final int CH93 = 93;

    /** 12A1_IN_Vooruit - CF_trdr_vooruit */
    public static final int CH94 = 94;

    /** 12A1_IN_Achteruit - CF_trdr_achteruit */
    public static final int CH95 = 95;

    /** 1Actueelkoppel - Actuele tractie */
    public static final int CH96 = 96;

    /** 12A1_ILACT - Actuele Il */
    public static final int CH97 = 97;

    /** 12A1_METING_LIJNSPANNING - Actuele Ul */
    public static final int CH98 = 98;

    /** 12A1_Inverter_Frequentie - Inverter frequentie */
    public static final int CH99 = 99;

    /** 12A1_Detectie_Wielslip - Wielslip gedetecteerd */
    public static final int CH100 = 100;

    /** 18A2_ATB_Treinsnelheid - ATB treinsnelheid */
    public static final int CH101 = 101;

    /** 18A2_ATB_Bewaakte_Snelheid - ATB bewaakte snelheid */
    public static final int CH102 = 102;

    /** 12A1_Tractie_Levert_Koppel - Tractie inst. levert koppel */
    public static final int CH103 = 103;

    /** 23A8_MW_UBAT_VOLT - Gemeten batterijspanning */
    public static final int CH104 = 104;

    /** 23A2_M_UT - Uitgangsspanning 2 */
    public static final int CH105 = 105;

    /** 23A2_M_IT_GEM - Uitgangsstroom 1 */
    public static final int CH106 = 106;

    /** 28A3_Gereed_Continu - Bedrijfstoestand gereed continu */
    public static final int CH107 = 107;

    /** 28A3_Bediende_Cabine - Bediende cabine */
    public static final int CH108 = 108;

    /** 28A3_Bedrijfstoestand_Gereed - Bedrijfstoestand gereed */
    public static final int CH109 = 109;

    /** 28A3_IN_Dienstvaardig - Bedrijfstoestand dienstvaardig */
    public static final int CH110 = 110;

    /** 28A4_ATB_Snelrem - ATB Snelrem */
    public static final int CH111 = 111;

    /** 28A4_IN_HS_AANWEZIG - Hoogspanning aanwezig */
    public static final int CH112 = 112;

    /** 26A1_UIT_VERWARMINGONDERIN - Heizung unten (HS) */
    public static final int CH113 = 113;

    /** 26A1_UIT_VERWARMINGBOVENIN - Heizung oben (HS) */
    public static final int CH114 = 114;

    /** 26A1Verwcabinein - Verwarming cabine */
    public static final int CH115 = 115;

    /** 217A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH116 = 116;

    /** 227A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH117 = 117;

    /** 237A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH118 = 118;

    /** 247A1_Vrijgave_Openen - Vrijgave openen */
    public static final int CH119 = 119;

    /** 24A1_DS_HL_MG - Drucksch. Hauptleitung MG-Bremse */
    public static final int CH120 = 120;

    /** 24A1_SLIP_ABI - Schlupf / schleudern Ausgang */
    public static final int CH121 = 121;

    /** 2standremkraan5 - 5.Brems-Stellung */
    public static final int CH122 = 122;

    /** 2standremkraan4 - 4.Brems-Stellung */
    public static final int CH123 = 123;

    /** 2standremkraan3 - 3.Brems-Stellung */
    public static final int CH124 = 124;

    /** 2standremkraan2 - 2.Brems-Stellung */
    public static final int CH125 = 125;

    /** 2standremkraan1 - 1.Brems-Stellung */
    public static final int CH126 = 126;

    /** 2standremkraanT - Fahr-Stellung */
    public static final int CH127 = 127;

    /** 2standremkraanA - Abschluss-Stellung */
    public static final int CH128 = 128;

    /** 24A1_Rem_Noodbedrijf - Notbetrieb */
    public static final int CH129 = 129;

    /** 24A1_ST_SB - Schnellbrems-Stellung */
    public static final int CH130 = 130;

    /** 2standremkraan7 - 7.Brems-Stellung */
    public static final int CH131 = 131;

    /** 2standremkraan6 - 6.Brems-Stellung */
    public static final int CH132 = 132;

    /** 24A1Noodremreizigers - Reisende-Notbremse Alarm */
    public static final int CH133 = 133;

    /** 22A1_BELADING - VTI_belading */
    public static final int CH134 = 134;

    /** 22A1_IN_Gevraagd_Koppel - VTI_gevraagd_koppel */
    public static final int CH135 = 135;

    /** 22A1_IN_Vooruit - CF_trdr_vooruit */
    public static final int CH136 = 136;

    /** 22A1_IN_Achteruit - CF_trdr_achteruit */
    public static final int CH137 = 137;

    /** 2Actueelkoppel - Actuele tractie */
    public static final int CH138 = 138;

    /** 22A1_ILACT - Actuele Il */
    public static final int CH139 = 139;

    /** 22A1_METING_LIJNSPANNING - Actuele Ul */
    public static final int CH140 = 140;

    /** 22A1_Inverter_Frequentie - Inverter frequentie */
    public static final int CH141 = 141;

    /** 22A1_Detectie_Wielslip - Wielslip gedetecteerd */
    public static final int CH142 = 142;

    /** 28A2_ATB_Treinsnelheid - ATB treinsnelheid */
    public static final int CH143 = 143;

    /** 28A2_ATB_Bewaakte_Snelheid - ATB bewaakte snelheid */
    public static final int CH144 = 144;

    /** 22A1_Tractie_Levert_Koppel - Tractie inst. levert koppel */
    public static final int CH145 = 145;

    /** DIENSTVAARD - Tijd in bedrijfstoestand Dienstv. */
    public static final int CH1001 = 1001;

    /** DIV_KOPPL_1 - Aantal keer gekoppeld zijde mBvk1 */
    public static final int CH1002 = 1002;

    /** DIV_KOPPL_2 - Aantal keer gekoppeld zijde mBvk2 */
    public static final int CH1003 = 1003;

    /** DR01_KLEM - Deur 1 inklemming sluiten */
    public static final int CH1004 = 1004;

    /** DR02_KLEM - Deur 2 inklemming sluiten */
    public static final int CH1005 = 1005;

    /** DR03_KLEM - Deur 3 inklemming sluiten */
    public static final int CH1006 = 1006;

    /** DR04_KLEM - Deur 4 inklemming sluiten */
    public static final int CH1007 = 1007;

    /** DR05_KLEM - Deur 5 inklemming sluiten */
    public static final int CH1008 = 1008;

    /** DR06_KLEM - Deur 6 inklemming sluiten */
    public static final int CH1009 = 1009;

    /** DR07_KLEM - Deur 7 inklemming sluiten */
    public static final int CH1010 = 1010;

    /** DR08_KLEM - Deur 8 inklemming sluiten */
    public static final int CH1011 = 1011;

    /** DR09_KLEM - Deur 9 inklemming sluiten */
    public static final int CH1012 = 1012;

    /** DR10_KLEM - Deur 10 inklemming sluiten */
    public static final int CH1013 = 1013;

    /** DR11_KLEM - Deur 11 inklemming sluiten */
    public static final int CH1014 = 1014;

    /** DR12_KLEM - Deur 12 inklemming sluiten */
    public static final int CH1015 = 1015;

    /** DR13_KLEM - Deur 13 inklemming sluiten */
    public static final int CH1016 = 1016;

    /** DR14_KLEM - Deur 14 inklemming sluiten */
    public static final int CH1017 = 1017;

    /** DR15_KLEM - Deur 15 inklemming sluiten */
    public static final int CH1018 = 1018;

    /** DR16_KLEM - Deur 16 inklemming sluiten */
    public static final int CH1019 = 1019;

    /** DR17_KLEM - Deur 17 inklemming sluiten */
    public static final int CH1020 = 1020;

    /** DR18_KLEM - Deur 18 inklemming sluiten */
    public static final int CH1021 = 1021;

    /** DR19_KLEM - Deur 19 inklemming sluiten */
    public static final int CH1022 = 1022;

    /** DR20_KLEM - Deur 20 inklemming sluiten */
    public static final int CH1023 = 1023;

    /** DR21_KLEM - Deur 21 inklemming sluiten */
    public static final int CH1024 = 1024;

    /** DR22_KLEM - Deur 22 inklemming sluiten */
    public static final int CH1025 = 1025;

    /** DR23_KLEM - Deur 23 inklemming sluiten */
    public static final int CH1026 = 1026;

    /** DR24_KLEM - Deur 24 inklemming sluiten */
    public static final int CH1027 = 1027;

    /** DRN_OCS_11 - Aantal keer deur gesloten (deur 1) */
    public static final int CH1028 = 1028;

    /** DRN_OCS_12 - Aantal keer deur gesloten (deur 2) */
    public static final int CH1029 = 1029;

    /** DRN_OCS_13 - Aantal keer deur gesloten (deur 3) */
    public static final int CH1030 = 1030;

    /** DRN_OCS_14 - Aantal keer deur gesloten (deur 4) */
    public static final int CH1031 = 1031;

    /** DRN_OCS_21 - Aantal keer deur gesloten (deur 24) */
    public static final int CH1032 = 1032;

    /** DRN_OCS_22 - Aantal keer deur gesloten (deur 23) */
    public static final int CH1033 = 1033;

    /** DRN_OCS_23 - Aantal keer deur gesloten (deur 22) */
    public static final int CH1034 = 1034;

    /** DRN_OCS_24 - Aantal keer deur gesloten (deur 21) */
    public static final int CH1035 = 1035;

    /** DRN_OCS_31 - Aantal keer deur gesloten (deur 20) */
    public static final int CH1036 = 1036;

    /** DRN_OCS_32 - Aantal keer deur gesloten (deur 19) */
    public static final int CH1037 = 1037;

    /** DRN_OCS_33 - Aantal keer deur gesloten (deur 18) */
    public static final int CH1038 = 1038;

    /** DRN_OCS_34 - Aantal keer deur gesloten (deur 17) */
    public static final int CH1039 = 1039;

    /** DRN_OCS_51 - Aantal keer deur gesloten (deur 5) */
    public static final int CH1040 = 1040;

    /** DRN_OCS_52 - Aantal keer deur gesloten (deur 6) */
    public static final int CH1041 = 1041;

    /** DRN_OCS_53 - Aantal keer deur gesloten (deur 7) */
    public static final int CH1042 = 1042;

    /** DRN_OCS_54 - Aantal keer deur gesloten (deur 8) */
    public static final int CH1043 = 1043;

    /** DRN_OCS_61 - Aantal keer deur gesloten (deur 13) */
    public static final int CH1044 = 1044;

    /** DRN_OCS_62 - Aantal keer deur gesloten (deur 14) */
    public static final int CH1045 = 1045;

    /** DRN_OCS_63 - Aantal keer deur gesloten (deur 15) */
    public static final int CH1046 = 1046;

    /** DRN_OCS_64 - Aantal keer deur gesloten (deur 16) */
    public static final int CH1047 = 1047;

    /** DRN_OCS_71 - Aantal keer deur gesloten (deur 9) */
    public static final int CH1048 = 1048;

    /** DRN_OCS_72 - Aantal keer deur gesloten (deur 10) */
    public static final int CH1049 = 1049;

    /** DRN_OCS_73 - Aantal keer deur gesloten (deur 11) */
    public static final int CH1050 = 1050;

    /** DRN_OCS_74 - Aantal keer deur gesloten (deur 12) */
    public static final int CH1051 = 1051;

    /** DR_KLEM_Z1 - Alle inklemmingen zijde 1 */
    public static final int CH1052 = 1052;

    /** DR_KLEM_Z2 - Alle inklemmingen zijde 2 */
    public static final int CH1053 = 1053;

    /** GEREED - Tijd in bedrijfstoestand Gereed */
    public static final int CH1054 = 1054;

    /** GEREEDCONT - Tijd in bedrijfstoestand Gereed C. */
    public static final int CH1055 = 1055;

    /** HSP_SA_KM1 - Afgelegde weg stroomafn. 1 gekozen */
    public static final int CH1056 = 1056;

    /** HSP_SA_KM2 - Afgelegde weg stroomafn. 2 gekozen */
    public static final int CH1057 = 1057;

    /** HS_AAN_SSUIT - Uitschakelen snelschak. (HS aanw.) */
    public static final int CH1058 = 1058;

    /** HS_AFW_SSUIT - Uitschakelen snelschak.(HS afwezig) */
    public static final int CH1059 = 1059;

    /** KLM_BUIBOV_1 - Schak.naar 100% buitenlucht BOV.1 */
    public static final int CH1060 = 1060;

    /** KLM_BUIBOV_2 - Schak.naar 100% buitenlucht BOV.2 */
    public static final int CH1061 = 1061;

    /** KLM_BUIBOV_3 - Schak.naar 100% buitenlucht BOV.3 */
    public static final int CH1062 = 1062;

    /** KLM_BUIBOV_5 - Schak.naar 100% buitenlucht BOV.5 */
    public static final int CH1063 = 1063;

    /** KLM_BUIBOV_6 - Schak.naar 100% buitenlucht BOV.6 */
    public static final int CH1064 = 1064;

    /** KLM_BUIBOV_7 - Schak.naar 100% buitenlucht BOV.7 */
    public static final int CH1065 = 1065;

    /** KLM_BUIOND_1 - Schak.naar 100% buitenlucht OND.1 */
    public static final int CH1066 = 1066;

    /** KLM_BUIOND_2 - Schak.naar 100% buitenlucht OND.2 */
    public static final int CH1067 = 1067;

    /** KLM_BUIOND_3 - Schak.naar 100% buitenlucht OND.3 */
    public static final int CH1068 = 1068;

    /** KLM_BUIOND_5 - Schak.naar 100% buitenlucht OND.5 */
    public static final int CH1069 = 1069;

    /** KLM_BUIOND_6 - Schak.naar 100% buitenlucht OND.6 */
    public static final int CH1070 = 1070;

    /** KLM_BUIOND_7 - Schak.naar 100% buitenlucht OND.7 */
    public static final int CH1071 = 1071;

    /** KLM_CMP11_T - Draaitijd koelcompressor WE1 mBvk1 */
    public static final int CH1072 = 1072;

    /** KLM_CMP12_T - Draaitijd koelcompressor WE2 mBvk1 */
    public static final int CH1073 = 1073;

    /** KLM_CMP21_T - Draaitijd koelcompressor WE1 mBvk2 */
    public static final int CH1074 = 1074;

    /** KLM_CMP22_T - Draaitijd koelcompressor WE2 mBvk2 */
    public static final int CH1075 = 1075;

    /** KLM_CMP31_T - Draaitijd koelcompressor WE1 ABv3/4 */
    public static final int CH1076 = 1076;

    /** KLM_CMP32_T - Draaitijd koelcompressor WE2 ABv3/4 */
    public static final int CH1077 = 1077;

    /** KLM_CMP51_T - Draaitijd koelcompressor WE1 ABv5 */
    public static final int CH1078 = 1078;

    /** KLM_CMP52_T - Draaitijd koelcompressor WE2 ABv5 */
    public static final int CH1079 = 1079;

    /** KLM_CMP61_T - Draaitijd koelcompressor WE1 ABv6 */
    public static final int CH1080 = 1080;

    /** KLM_CMP62_T - Draaitijd koelcompressor WE2 ABv6 */
    public static final int CH1081 = 1081;

    /** KLM_CMP71_T - Draaitijd koelcompressor WE1 mBv7 */
    public static final int CH1082 = 1082;

    /** KLM_CMP72_T - Draaitijd koelcompressor WE2 mBv7 */
    public static final int CH1083 = 1083;

    /** KLM_HS_BOV1 - Insch. HS schak. verw. Boven mBvk1 */
    public static final int CH1084 = 1084;

    /** KLM_HS_BOV2 - Insch. HS schak. verw. Boven mBvk2 */
    public static final int CH1085 = 1085;

    /** KLM_HS_BOV3 - Insch. HS schak. verw. Boven ABv3/4 */
    public static final int CH1086 = 1086;

    /** KLM_HS_BOV5 - Insch. HS schak. verw. Boven ABv5 */
    public static final int CH1087 = 1087;

    /** KLM_HS_BOV6 - Insch. HS schak. verw. Boven ABv6 */
    public static final int CH1088 = 1088;

    /** KLM_HS_BOV7 - Insch. HS schak. verw. Boven mBv7 */
    public static final int CH1089 = 1089;

    /** KLM_HS_END1 - Insch. HS schak. verw. Eind mBvk1 */
    public static final int CH1090 = 1090;

    /** KLM_HS_END2 - Insch. HS schak. verw. Eind mBvk2 */
    public static final int CH1091 = 1091;

    /** KLM_HS_END3 - Insch. HS schak. verw. Eind ABv3/4 */
    public static final int CH1092 = 1092;

    /** KLM_HS_END5 - Insch. HS schak. verw. Eind ABv5 */
    public static final int CH1093 = 1093;

    /** KLM_HS_END6 - Insch. HS schak. verw. Eind ABv6 */
    public static final int CH1094 = 1094;

    /** KLM_HS_END7 - Insch. HS schak. verw. Eind mBv7 */
    public static final int CH1095 = 1095;

    /** KLM_HS_OND1 - Insch. HS schak. verw. Onder mBvk1 */
    public static final int CH1096 = 1096;

    /** KLM_HS_OND2 - Insch. HS schak. verw. Onder mBvk2 */
    public static final int CH1097 = 1097;

    /** KLM_HS_OND3 - Insch. HS schak. verw. Onder ABv3/4 */
    public static final int CH1098 = 1098;

    /** KLM_HS_OND5 - Insch. HS schak. verw. Onder ABv5 */
    public static final int CH1099 = 1099;

    /** KLM_HS_OND6 - Insch. HS schak. verw. Onder ABv6 */
    public static final int CH1100 = 1100;

    /** KLM_HS_OND7 - Insch. HS schak. verw. Onder mBv7 */
    public static final int CH1101 = 1101;

    /** KLM_RELBOV_1 - Schak.naar 100% recirculatie BOV.1 */
    public static final int CH1102 = 1102;

    /** KLM_RELBOV_2 - Schak.naar 100% recirculatie BOV.2 */
    public static final int CH1103 = 1103;

    /** KLM_RELBOV_3 - Schak.naar 100% recirculatie BOV.3 */
    public static final int CH1104 = 1104;

    /** KLM_RELBOV_5 - Schak.naar 100% recirculatie BOV.5 */
    public static final int CH1105 = 1105;

    /** KLM_RELBOV_6 - Schak.naar 100% recirculatie BOV.6 */
    public static final int CH1106 = 1106;

    /** KLM_RELBOV_7 - Schak.naar 100% recirculatie BOV.7 */
    public static final int CH1107 = 1107;

    /** KLM_RELOND_1 - Schak.naar 100% recirculatie OND.1 */
    public static final int CH1108 = 1108;

    /** KLM_RELOND_2 - Schak.naar 100% recirculatie OND.2 */
    public static final int CH1109 = 1109;

    /** KLM_RELOND_3 - Schak.naar 100% recirculatie OND.3 */
    public static final int CH1110 = 1110;

    /** KLM_RELOND_5 - Schak.naar 100% recirculatie OND.5 */
    public static final int CH1111 = 1111;

    /** KLM_RELOND_6 - Schak.naar 100% recirculatie OND.6 */
    public static final int CH1112 = 1112;

    /** KLM_RELOND_7 - Schak.naar 100% recirculatie OND.7 */
    public static final int CH1113 = 1113;

    /** KLM_VENT1_M - Draaitijd ventilatoren mBvk1 */
    public static final int CH1114 = 1114;

    /** KLM_VENT2_M - Draaitijd ventilatoren mBvk2 */
    public static final int CH1115 = 1115;

    /** KLM_VENT3_M - Draaitijd ventilatoren ABv3/4 */
    public static final int CH1116 = 1116;

    /** KLM_VENT5_M - Draaitijd ventilatoren ABv5 */
    public static final int CH1117 = 1117;

    /** KLM_VENT6_M - Draaitijd ventilatoren ABv6 */
    public static final int CH1118 = 1118;

    /** KLM_VENT7_M - Draaitijd ventilatoren mBv7 */
    public static final int CH1119 = 1119;

    /** KM_TELLER - Km-teller */
    public static final int CH1120 = 1120;

    /** LSP_BAT_1_M - Draaitijd Batterij lader mBvk1 */
    public static final int CH1121 = 1121;

    /** LSP_BAT_2_M - Draaitijd Batterij lader mBvk2 */
    public static final int CH1122 = 1122;

    /** LSP_BAT_7_M - Draaitijd Batterij lader mBv7 */
    public static final int CH1123 = 1123;

    /** LSP_DCM_1_IN - Inschakelen DCM mBvk1 */
    public static final int CH1124 = 1124;

    /** LSP_DCM_1_M - Draaitijd DCM mBvk1 */
    public static final int CH1125 = 1125;

    /** LSP_DCM_2_IN - Inschakelen DCM mBvk2 */
    public static final int CH1126 = 1126;

    /** LSP_DCM_2_M - Draaitijd DCM mBvk2 */
    public static final int CH1127 = 1127;

    /** LSP_DCM_7_IN - Inschakelen DCM mBv7 */
    public static final int CH1128 = 1128;

    /** LSP_DCM_7_M - Draaitijd DCM mBv7 */
    public static final int CH1129 = 1129;

    /** LS_EN_DIENST - Verbruikte LS energie in Dienstv. */
    public static final int CH1130 = 1130;

    /** LS_EN_GEREED - Verbruikte LS energie in Gereed */
    public static final int CH1131 = 1131;

    /** LS_EN_GER_C - Verbruikte LS energie in Gereed C. */
    public static final int CH1132 = 1132;

    /** LVZ_CMP1_IN - Inschakelen compressor ABv3/4 */
    public static final int CH1133 = 1133;

    /** LVZ_CMP1_M - Draaitijd compressor ABv3/4 */
    public static final int CH1134 = 1134;

    /** NDREM1_VGR5 - Noodremming V>5km/h mBvk1 */
    public static final int CH1135 = 1135;

    /** NDREM2_VGR5 - Noodremming V>5km/h mBvk2 */
    public static final int CH1136 = 1136;

    /** REM_H_BED1 - Remhandel bediend mBvk1 */
    public static final int CH1137 = 1137;

    /** REM_H_BED2 - Remhandel bediend mBvk2 */
    public static final int CH1138 = 1138;

    /** REM_KIN_EP - Verbruikte energie bij (E)P remming */
    public static final int CH1139 = 1139;

    /** REM_KIN_NR - Verbruikte energie bij noodremming */
    public static final int CH1140 = 1140;

    /** REM_NR_BED1 - Remmingen in remnoodbedrijf mBvk1 */
    public static final int CH1141 = 1141;

    /** REM_NR_BED2 - Remmingen in remnoodbedrijf mBvk2 */
    public static final int CH1142 = 1142;

    /** REM_P_BED - Parkeerrem bediend */
    public static final int CH1143 = 1143;

    /** REM_TRDR17_1 - Sturing treindraad 1/7 mBvk1 */
    public static final int CH1144 = 1144;

    /** REM_TRDR17_2 - Sturing treindraad 1/7 mBvk2 */
    public static final int CH1145 = 1145;

    /** REM_TRDR27_1 - Sturing treindraad 2/7 mBvk1 */
    public static final int CH1146 = 1146;

    /** REM_TRDR27_2 - Sturing treindraad 2/7 mBvk2 */
    public static final int CH1147 = 1147;

    /** REM_TRDR47_1 - Sturing treindraad 4/7 mBvk1 */
    public static final int CH1148 = 1148;

    /** REM_TRDR47_2 - Sturing treindraad 4/7 mBvk2 */
    public static final int CH1149 = 1149;

    /** REM_VENT17 - Remventiel 1/7 aangestuurd */
    public static final int CH1150 = 1150;

    /** REM_VENT27 - Remventiel 2/7 aangestuurd */
    public static final int CH1151 = 1151;

    /** REM_VENT47 - Remventiel 4/7 aangestuurd */
    public static final int CH1152 = 1152;

    /** SNT_DRN_D_3 - Aantal deurbewegingen ABv34 */
    public static final int CH1153 = 1153;

    /** SNT_DRN_D_6 - Aantal deurbewegingen ABv6 */
    public static final int CH1154 = 1154;

    /** SNT_FRST_R_3 - Aantal keer vorstleging bior. ABv34 */
    public static final int CH1155 = 1155;

    /** SNT_FRST_R_5 - Aantal keer vorstleging bior. ABv5 */
    public static final int CH1156 = 1156;

    /** SNT_FRST_R_6 - Aantal keer vorstleging bior. ABv6 */
    public static final int CH1157 = 1157;

    /** SNT_HYG_R_3 - Aantal keer hygiÃ«nisatie ABv34 */
    public static final int CH1158 = 1158;

    /** SNT_HYG_R_5 - Aantal keer hygiÃ«nisatie ABv5 */
    public static final int CH1159 = 1159;

    /** SNT_HYG_R_6 - Aantal keer hygiÃ«nisatie ABv6 */
    public static final int CH1160 = 1160;

    /** SNT_REV_SP_3 - Aantal reverse spoelingen ABv34 */
    public static final int CH1161 = 1161;

    /** SNT_REV_SP_5 - Aantal reverse spoelingen ABv5 */
    public static final int CH1162 = 1162;

    /** SNT_REV_SP_6 - Aantal reverse spoelingen ABv6 */
    public static final int CH1163 = 1163;

    /** SNT_SERV_SP3 - Aantal service spoelingen ABv34 */
    public static final int CH1164 = 1164;

    /** SNT_SERV_SP5 - Aantal service spoelingen ABv5 */
    public static final int CH1165 = 1165;

    /** SNT_SERV_SP6 - Aantal service spoelingen ABv6 */
    public static final int CH1166 = 1166;

    /** SNT_SPOEL_3 - Aantal spoelingen vacuÃ¼m toil.ABv34 */
    public static final int CH1167 = 1167;

    /** SNT_SPOEL_5 - Aantal spoelingen vacuÃ¼m toil. ABv5 */
    public static final int CH1168 = 1168;

    /** SNT_SPOEL_6 - Aantal spoelingen vacuÃ¼m toil. ABv6 */
    public static final int CH1169 = 1169;

    /** SNT_UR_SP_3 - Aantal urinoir spoelingen ABv34 */
    public static final int CH1170 = 1170;

    /** SNT_UR_SP_6 - Aantal urinoir spoelingen ABv6 */
    public static final int CH1171 = 1171;

    /** SNT_WAS_KL_3 - Aantal keer watertap.wastafelABv34 */
    public static final int CH1172 = 1172;

    /** SNT_WAS_KL_5 - Aantal keer watertap.wastafel ABv5 */
    public static final int CH1173 = 1173;

    /** SNT_WAS_KL_6 - Aantal keer watertap.wastafel ABv6 */
    public static final int CH1174 = 1174;

    /** SNT_Y1_OVER3 - Aantal keer schak.Y1(overflow)ABv34 */
    public static final int CH1175 = 1175;

    /** SNT_Y1_OVER5 - Aantal keer schak.Y1(overflow) ABv5 */
    public static final int CH1176 = 1176;

    /** SNT_Y1_OVER6 - Aantal keer schak.Y1(overflow) ABv6 */
    public static final int CH1177 = 1177;

    /** SNT_Y2_HYG_3 - Aantal keer schak.Y2 (hygiÃ«n.)ABv34 */
    public static final int CH1178 = 1178;

    /** SNT_Y2_HYG_5 - Aantal keer schak.Y2 (hygiÃ«n.) ABv5 */
    public static final int CH1179 = 1179;

    /** SNT_Y2_HYG_6 - Aantal keer schak.Y2 (hygiÃ«n.) ABv6 */
    public static final int CH1180 = 1180;

    /** SNT_Y3_TRA_3 - Aantal keer schak.Y3 transportABv34 */
    public static final int CH1181 = 1181;

    /** SNT_Y3_TRA_5 - Aantal keer schak.Y3 transport ABv5 */
    public static final int CH1182 = 1182;

    /** SNT_Y3_TRA_6 - Aantal keer schak.Y3 transport ABv6 */
    public static final int CH1183 = 1183;

    /** SNT_Y4_FRST3 - Aantal keer schak.Y4 vorstlegi.ABv3 */
    public static final int CH1184 = 1184;

    /** SNT_Y4_FRST5 - Aantal keer schak.Y4 vorstlegi.ABv5 */
    public static final int CH1185 = 1185;

    /** SNT_Y4_FRST6 - Aantal keer schak.Y4 vorstlegi.ABv6 */
    public static final int CH1186 = 1186;

    /** SNT_Y5_TRA_3 - Aantal keer schak.Y5 transportABv34 */
    public static final int CH1187 = 1187;

    /** SNT_Y5_TRA_5 - Aantal keer schak.Y5 transport ABv5 */
    public static final int CH1188 = 1188;

    /** SNT_Y5_TRA_6 - Aantal keer schak.Y5 transport ABv6 */
    public static final int CH1189 = 1189;

    /** TRC_EN_AAN1 - Verbruikte tractie energie mBvk1 */
    public static final int CH1190 = 1190;

    /** TRC_EN_AAN2 - Verbruikte tractie energie mBvk2 */
    public static final int CH1191 = 1191;

    /** TRC_EN_AAN7 - Verbruikte tractie energie mBv7 */
    public static final int CH1192 = 1192;

    /** TRC_EN_REC1 - Teruggeleverde energie mBvk1 */
    public static final int CH1193 = 1193;

    /** TRC_EN_REC2 - Teruggeleverde energie mBvk2 */
    public static final int CH1194 = 1194;

    /** TRC_EN_REC7 - Teruggeleverde energie mBv7 */
    public static final int CH1195 = 1195;

    /** TRC_IN_1_M - Draaitijd aandrijving mBvk1 */
    public static final int CH1196 = 1196;

    /** TRC_IN_2_M - Draaitijd aandrijving mBvk2 */
    public static final int CH1197 = 1197;

    /** TRC_IN_7_M - Draaitijd aandrijving mBv7 */
    public static final int CH1198 = 1198;

    /** TRC_LS_1_IN - Inschakelen lijnschakelaar mBvk1 */
    public static final int CH1199 = 1199;

    /** TRC_LS_2_IN - Inschakelen lijnschakelaar mBvk2 */
    public static final int CH1200 = 1200;

    /** TRC_LS_7_IN - Inschakelen lijnschakelaar mBv7 */
    public static final int CH1201 = 1201;

    /** TRC_VENT_1_M - Draaitijd ventilator hoog mBvk1 */
    public static final int CH1202 = 1202;

    /** TRC_VENT_2_M - Draaitijd ventilator hoog mBvk2 */
    public static final int CH1203 = 1203;

    /** TRC_VENT_7_M - Draaitijd ventilator hoog mBv7 */
    public static final int CH1204 = 1204;

    /** VERW_EN_B_D - Verbruikte verw. energie boven in D */
    public static final int CH1205 = 1205;

    /** VERW_EN_B_G - Verbruikte verw. energie boven in G */
    public static final int CH1206 = 1206;

    /** VERW_EN_B_GC - Verbruikte verw. energie boven inGc */
    public static final int CH1207 = 1207;

    /** VERW_EN_E_D - Verbruikte verw. energie eind. in D */
    public static final int CH1208 = 1208;

    /** VERW_EN_E_G - Verbruikte verw. energie eind. in G */
    public static final int CH1209 = 1209;

    /** VERW_EN_E_GC - Verbruikte verw. energie eind. inGc */
    public static final int CH1210 = 1210;

    /** VERW_EN_O_D - Verbruikte verw. energie onder in D */
    public static final int CH1211 = 1211;

    /** VERW_EN_O_G - Verbruikte verw. energie onder in G */
    public static final int CH1212 = 1212;

    /** VERW_EN_O_GC - Verbruikte verw. energie onder inGc */
    public static final int CH1213 = 1213;

    /** ATB001 - ATB code A, een kanaal afgeschakeld mBvk1. */
    public static final int CH5001 = 5001;

    /** ATB002 - ATB code A, een kanaal afgeschakeld mBvk2. */
    public static final int CH5002 = 5002;

    /** ATB003 - ATB code E, storing linker spoel mBvk1 */
    public static final int CH5003 = 5003;

    /** ATB004 - ATB code E, storing linker spoel mBvk2 */
    public static final int CH5004 = 5004;

    /** ATB005 - ATB code F storing rechter spoel mBvk1 */
    public static final int CH5005 = 5005;

    /** ATB006 - ATB code F storing rechter spoel mBvk2 */
    public static final int CH5006 = 5006;

    /** ATB007 - ATB code H storing akoest. signal. mBvk1 */
    public static final int CH5007 = 5007;

    /** ATB008 - ATB code H storing akoest. signal. mBvk2 */
    public static final int CH5008 = 5008;

    /** ATB011 - ATB code 3 wel remdet. mBvk1 */
    public static final int CH5009 = 5009;

    /** ATB012 - ATB code 3 wel remdet. mBvk2 */
    public static final int CH5010 = 5010;

    /** ATB015 - ATB code 5,ernstige fout in ATB appa mBvk1 */
    public static final int CH5011 = 5011;

    /** ATB016 - ATB code 5,ernstige fout in ATB appa mBvk2 */
    public static final int CH5012 = 5012;

    /** ATB017 - ATB code 6, geen snelheidsmeting mBvk1 */
    public static final int CH5013 = 5013;

    /** ATB018 - ATB code 6, geen snelheidsmeting mBvk2 */
    public static final int CH5014 = 5014;

    /** ATB019 - ATB code7, onjuiste input ATB-signalen.mBvk1 */
    public static final int CH5015 = 5015;

    /** ATB020 - ATB code7, onjuiste input ATB-signalen.mBvk2 */
    public static final int CH5016 = 5016;

    /** ATB021 - ATB code 9, ATB drukknop(pen) defect mBvk1 */
    public static final int CH5017 = 5017;

    /** ATB022 - ATB code 9, ATB drukknop(pen) defect mBvk2 */
    public static final int CH5018 = 5018;

    /** ATB030 - ATB BD lamp in niet bediende cabine 1 OL */
    public static final int CH5019 = 5019;

    /** ATB031 - ATB BD lamp in niet bediende cabine 2 OL */
    public static final int CH5020 = 5020;

    /** ATB032 - ATB-Baancode sign. niet in tegenfase (mBvk1) */
    public static final int CH5021 = 5021;

    /** ATB033 - ATB-Baancode sign.niet in tegenfase (mBvk2) */
    public static final int CH5022 = 5022;

    /** ATB034 - ATB signaal is verminkt (mBvk1) */
    public static final int CH5023 = 5023;

    /** ATB035 - ATB signaal is verminkt (mBvk2) */
    public static final int CH5024 = 5024;

    /** ATB036 - ATB defect niet bediende cabine (mBvk1) */
    public static final int CH5025 = 5025;

    /** ATB037 - ATB defect niet bediende cabine (mBvk2) */
    public static final int CH5026 = 5026;

    /** ATB038 - ATB BD defect in cabine1 gekoppeld treinstel. */
    public static final int CH5027 = 5027;

    /** ATB039 - ATB BD defect in cabine2 gekoppeld treinstel. */
    public static final int CH5028 = 5028;

    /** ATB041 - ATB bij opstarten niet ontgrendeld (mBvk1) */
    public static final int CH5029 = 5029;

    /** ATB042 - ATB bij opstarten niet ontgrendeld (mBvk2) */
    public static final int CH5030 = 5030;

    /** ATB043 - ATB FC0 na remopdr. geen remdetectie mBvk1 */
    public static final int CH5031 = 5031;

    /** ATB044 - ATB FC0 na remopdr. geen remdetectie mBvk2 */
    public static final int CH5032 = 5032;

    /** ATB045 - ATB FC4, attentie drukknop defect (mBvk1) */
    public static final int CH5033 = 5033;

    /** ATB046 - ATB FC4, attentie drukknop defect (mBvk2) */
    public static final int CH5034 = 5034;

    /** ATB053 - Storing inleescirc ATB-snelheidsmeter mBvk1 */
    public static final int CH5035 = 5035;

    /** ATB054 - Storing inleescirc ATB-snelheidsmeter mBvk2 */
    public static final int CH5036 = 5036;

    /** ATB055 - BRR mBvk1 volgt niet snelrem. BRR mBvk2 */
    public static final int CH5037 = 5037;

    /** ATB056 - BRR mBvk2 volgt niet snelrem. BRR mBvk1 */
    public static final int CH5038 = 5038;

    /** ATB057 - ATB mBvk1, Buitenbedr. geen bew. snelheid */
    public static final int CH5039 = 5039;

    /** ATB058 - ATB mBvk2, Buitenbedr. geen bew. snelheid */
    public static final int CH5040 = 5040;

    /** ATB059 - ATB BD beide ATB Install geven snelrem */
    public static final int CH5041 = 5041;

    /** ATB060 - Circuit ATB-snelremlamp defect (mBvk1) */
    public static final int CH5042 = 5042;

    /** ATB061 - Circuit ATB-snelremlamp defect (mBvk2) */
    public static final int CH5043 = 5043;

    /** ATB062 - Circuit ATB-snelremlamp defect (mBvk1) */
    public static final int CH5044 = 5044;

    /** ATB063 - Circuit ATB-snelremlamp defect (mBvk2) */
    public static final int CH5045 = 5045;

    /** ATB064 - Snelheidsomz. Bewaakte V defect (mBvk1) */
    public static final int CH5046 = 5046;

    /** ATB065 - Snelheidsomz. Bewaakte V defect (mBvk2) */
    public static final int CH5047 = 5047;

    /** ATB066 - Snelheidsomz. werkelijke. V defect (mBvk1) */
    public static final int CH5048 = 5048;

    /** ATB067 - Snelheidsomz. werkelijke. V defect (mBvk2) */
    public static final int CH5049 = 5049;

    /** ATB068 - 9F1 uitgeschakeld (mBvk1) */
    public static final int CH5050 = 5050;

    /** ATB069 - 9F1 uitgeschakeld (mBvk2) */
    public static final int CH5051 = 5051;

    /** ATB070 - 9F2 uitgeschakeld niet bed. cab. (mBvk1) */
    public static final int CH5052 = 5052;

    /** ATB071 - 9F2 uitgeschakeld niet bed. cab. (mBvk2) */
    public static final int CH5053 = 5053;

    /** ATB072 - ATB signaleringen defect (mBvk1) */
    public static final int CH5054 = 5054;

    /** ATB073 - ATB signaleringen defect (mBvk2) */
    public static final int CH5055 = 5055;

    /** ATB080 - ORBIT systeem niet OK mBvk1 */
    public static final int CH5056 = 5056;

    /** ATB081 - ORBIT systeem niet OK mBvk2 */
    public static final int CH5057 = 5057;

    /** DVR040 - Harddisk videorecorder defect mBvk1 VIRMm */
    public static final int CH5058 = 5058;

    /** DVR041 - Harddisk videorecorder defect mBvk2 VIRMm */
    public static final int CH5059 = 5059;

    /** DVR042 - Harddisk videorecorder defect ABv34 VIRMm */
    public static final int CH5060 = 5060;

    /** DVR043 - Harddisk videorecorder defect ABv5 VIRMm */
    public static final int CH5061 = 5061;

    /** DVR044 - Harddisk videorecorder defect ABv6 VIRMm */
    public static final int CH5062 = 5062;

    /** DVR045 - Harddisk videorecorder defect mBv7 VIRMm */
    public static final int CH5063 = 5063;

    /** DVR050 - Camera 1, geen video opnames mBvk1 VIRMm */
    public static final int CH5064 = 5064;

    /** DVR051 - Camera 1, geen video opnames mBvk2 VIRMm */
    public static final int CH5065 = 5065;

    /** DVR052 - Camera 1, geen video opnames ABv34 VIRMm */
    public static final int CH5066 = 5066;

    /** DVR053 - Camera 1, geen video opnames ABv5 VIRMm */
    public static final int CH5067 = 5067;

    /** DVR054 - Camera 1, geen video opnames ABv6 VIRMm */
    public static final int CH5068 = 5068;

    /** DVR055 - Camera 1, geen video opnames mBv7 VIRMm */
    public static final int CH5069 = 5069;

    /** DVR060 - Camera 2, geen video opnames mBvk1 VIRMm */
    public static final int CH5070 = 5070;

    /** DVR061 - Camera 2, geen video opnames mBvk2 VIRMm */
    public static final int CH5071 = 5071;

    /** DVR062 - Camera 2, geen video opnames ABv34 VIRMm */
    public static final int CH5072 = 5072;

    /** DVR063 - Camera 2, geen video opnames ABv5 VIRMm */
    public static final int CH5073 = 5073;

    /** DVR064 - Camera 2, geen video opnames ABv6 VIRMm */
    public static final int CH5074 = 5074;

    /** DVR065 - Camera 2, geen video opnames mBv7 VIRMm */
    public static final int CH5075 = 5075;

    /** DVR070 - Camera 3, geen video opnames mBvk1 VIRMm */
    public static final int CH5076 = 5076;

    /** DVR071 - Camera 3, geen video opnames mBvk2 VIRMm */
    public static final int CH5077 = 5077;

    /** DVR072 - Camera 3, geen video opnames ABv34 VIRMm */
    public static final int CH5078 = 5078;

    /** DVR073 - Camera 3, geen video opnames ABv5 VIRMm */
    public static final int CH5079 = 5079;

    /** DVR074 - Camera 3, geen video opnames ABv6 VIRMm */
    public static final int CH5080 = 5080;

    /** DVR075 - Camera 3, geen video opnames mBv7 VIRMm */
    public static final int CH5081 = 5081;

    /** DVR080 - Camera 4, geen video opnames mBvk1 VIRMm */
    public static final int CH5082 = 5082;

    /** DVR081 - Camera 4, geen video opnames mBvk2 VIRMm */
    public static final int CH5083 = 5083;

    /** DVR082 - Camera 4, geen video opnames ABv34 VIRMm */
    public static final int CH5084 = 5084;

    /** DVR083 - Camera 4, geen video opnames ABv5 VIRMm */
    public static final int CH5085 = 5085;

    /** DVR084 - Camera 4, geen video opnames ABv6 VIRMm */
    public static final int CH5086 = 5086;

    /** DVR085 - Camera 4, geen video opnames mBv7 VIRMm */
    public static final int CH5087 = 5087;

    /** DVR090 - Camera 1 afgedekt, mBvk1 VIRMm */
    public static final int CH5088 = 5088;

    /** DVR091 - Camera 1 afgedekt, mBvk2 VIRMm */
    public static final int CH5089 = 5089;

    /** DVR092 - Camera 1 afgedekt, ABv34 VIRMm */
    public static final int CH5090 = 5090;

    /** DVR093 - Camera 1 afgedekt, ABv5 VIRMm */
    public static final int CH5091 = 5091;

    /** DVR094 - Camera 1 afgedekt, ABv6 VIRMm */
    public static final int CH5092 = 5092;

    /** DVR095 - Camera 1 afgedekt, mBv7 VIRMm */
    public static final int CH5093 = 5093;

    /** DVR100 - Camera 2 afgedekt, mBvk1 VIRMm */
    public static final int CH5094 = 5094;

    /** DVR101 - Camera 2 afgedekt, mBvk2 VIRMm */
    public static final int CH5095 = 5095;

    /** DVR102 - Camera 2 afgedekt, ABv34 VIRMm */
    public static final int CH5096 = 5096;

    /** DVR103 - Camera 2 afgedekt, ABv5 VIRMm */
    public static final int CH5097 = 5097;

    /** DVR104 - Camera 2 afgedekt, ABv6 VIRMm */
    public static final int CH5098 = 5098;

    /** DVR105 - Camera 2 afgedekt, mBv7 VIRMm */
    public static final int CH5099 = 5099;

    /** DVR110 - Camera 3 afgedekt, mBvk1 VIRMm */
    public static final int CH5100 = 5100;

    /** DVR111 - Camera 3 afgedekt, mBvk2 VIRMm */
    public static final int CH5101 = 5101;

    /** DVR112 - Camera 3 afgedekt, ABv34 VIRMm */
    public static final int CH5102 = 5102;

    /** DVR113 - Camera 3 afgedekt, ABv5 VIRMm */
    public static final int CH5103 = 5103;

    /** DVR114 - Camera 3 afgedekt, ABv6 VIRMm */
    public static final int CH5104 = 5104;

    /** DVR115 - Camera 3 afgedekt, mBv7 VIRMm */
    public static final int CH5105 = 5105;

    /** DVR120 - Camera 4 afgedekt, mBvk1 VIRMm */
    public static final int CH5106 = 5106;

    /** DVR121 - Camera 4 afgedekt, mBvk2 VIRMm */
    public static final int CH5107 = 5107;

    /** DVR122 - Camera 4 afgedekt, ABv34 VIRMm */
    public static final int CH5108 = 5108;

    /** DVR123 - Camera 4 afgedekt, ABv5 VIRMm */
    public static final int CH5109 = 5109;

    /** DVR124 - Camera 4 afgedekt, ABv6 VIRMm */
    public static final int CH5110 = 5110;

    /** DVR125 - Camera 4 afgedekt, mBv7 VIRMm */
    public static final int CH5111 = 5111;

    /** DVR130 - Uitval comm.tussen RIOM-DVR mBvk1 VIRMm */
    public static final int CH5112 = 5112;

    /** DVR131 - Uitval comm.tussen RIOM-DVR mBvk2 VIRMm */
    public static final int CH5113 = 5113;

    /** DVR132 - Uitval comm.tussen RIOM-DVR ABv34 VIRMm */
    public static final int CH5114 = 5114;

    /** DVR133 - Uitval comm.tussen RIOM-DVR ABv5 VIRMm */
    public static final int CH5115 = 5115;

    /** DVR134 - Uitval comm.tussen RIOM-DVR ABv6 VIRMm */
    public static final int CH5116 = 5116;

    /** DVR135 - Uitval comm.tussen RIOM-DVR mBv7 VIRMm */
    public static final int CH5117 = 5117;

    /** HS0001 - Terugvalniveau 18A4_SS_VGR_OH */
    public static final int CH5118 = 5118;

    /** HS0002 - Terugvalniveau 28A4_SS_VGR_OH */
    public static final int CH5119 = 5119;

    /** HS0003 - Terugvalniveau 78A4_SS_VGR_OH */
    public static final int CH5120 = 5120;

    /** HSP002 - Geen hoogspanning mBvk1 */
    public static final int CH5121 = 5121;

    /** HSP004 - Geen hoogspanning mBvk2 */
    public static final int CH5122 = 5122;

    /** HSP005 - Geen hoogspanning mBv7 */
    public static final int CH5123 = 5123;

    /** HSP006 - Snelschakelaar uit */
    public static final int CH5124 = 5124;

    /** HSP007 - Ingang SS_IN 68A3 onterecht laag. */
    public static final int CH5125 = 5125;

    /** HSP008 - Ingang HS_OK 18A4 onterecht hoog */
    public static final int CH5126 = 5126;

    /** HSP009 - Ingang HS_OK 28A4 onterecht hoog */
    public static final int CH5127 = 5127;

    /** HSP010 - Ingang HS_OK 78A4 onterecht hoog */
    public static final int CH5128 = 5128;

    /** HSP011 - Ingang HS_OK 18A4 onterecht laag */
    public static final int CH5129 = 5129;

    /** HSP012 - Ingang HS_OK 28A4 onterecht laag */
    public static final int CH5130 = 5130;

    /** HSP013 - Ingang HS_OK 78A4 onterecht laag */
    public static final int CH5131 = 5131;

    /** HSP015 - Ing.stoorstroomd. vergr. 18A4 ont. hoog */
    public static final int CH5132 = 5132;

    /** HSP017 - Ing.stoorstroomd. vergr. 28A4 ont. hoog */
    public static final int CH5133 = 5133;

    /** HSP018 - Ing.stoorstroomd. vergr. 78A4 ont. hoog */
    public static final int CH5134 = 5134;

    /** HSP020 - Stoorstroomingreep mBvk1 */
    public static final int CH5135 = 5135;

    /** HSP022 - Stoorstroomingreep mBvk2 */
    public static final int CH5136 = 5136;

    /** HSP023 - Stoorstroomingreep mBv7 */
    public static final int CH5137 = 5137;

    /** HSP025 - Stoorstroomdetector mBvk1 niet actief */
    public static final int CH5138 = 5138;

    /** HSP027 - Stoorstroomdetector mBvk2 niet actief */
    public static final int CH5139 = 5139;

    /** HSP028 - Stoorstroomdetector mBv7 niet actief */
    public static final int CH5140 = 5140;

    /** HSP031 - Stoorstroomdetector mBvk1 overbrugd */
    public static final int CH5141 = 5141;

    /** HSP032 - Stoorstroomdetector mBvk2 overbrugd */
    public static final int CH5142 = 5142;

    /** HSP033 - Stoorstroomdetector mBv7 overbrugd */
    public static final int CH5143 = 5143;

    /** HSP034 - Lijnspanning DCM onterecht laag mBvk1 */
    public static final int CH5144 = 5144;

    /** HSP035 - Lijnspanning DCM onterecht laag mBvk2 */
    public static final int CH5145 = 5145;

    /** HSP036 - Lijnspanning DCM onterecht laag mBv7 */
    public static final int CH5146 = 5146;

    /** HSP037 - Filterspanning DCM onterecht laag mBvk1 */
    public static final int CH5147 = 5147;

    /** HSP038 - Filterspanning DCM onterecht laag mBvk2 */
    public static final int CH5148 = 5148;

    /** HSP039 - Filterspanning DCM onterecht laag mBv7 */
    public static final int CH5149 = 5149;

    /** HSP040 - Lijnspanning DCM onterecht hoog mBvk1 */
    public static final int CH5150 = 5150;

    /** HSP041 - Lijnspanning DCM onterecht hoog mBvk2 */
    public static final int CH5151 = 5151;

    /** HSP042 - Lijnspanning DCM onterecht hoog mBv7 */
    public static final int CH5152 = 5152;

    /** HSP043 - Filterspanning DCM onterecht hoog mBvk1 */
    public static final int CH5153 = 5153;

    /** HSP044 - Filterspanning DCM onterecht hoog mBvk2 */
    public static final int CH5154 = 5154;

    /** HSP045 - Filterspanning DCM onterecht hoog mBv7 */
    public static final int CH5155 = 5155;

    /** HSP046 - Geen hoogspanning op DCM mBvk1 */
    public static final int CH5156 = 5156;

    /** HSP047 - Geen hoogspanning op DCM mBvk2 */
    public static final int CH5157 = 5157;

    /** HSP048 - Geen hoogspanning op DCM mBv7 */
    public static final int CH5158 = 5158;

    /** HSP051 - Ing.stoorstroomd. vergr. 78A4 ont. hoog */
    public static final int CH5159 = 5159;

    /** HSP052 - Ing.stoorstroomd. vergr. 18A4 ont. hoog */
    public static final int CH5160 = 5160;

    /** HSP053 - Ing.stoorstroomd. vergr. 28A4 ont. hoog */
    public static final int CH5161 = 5161;

    /** HSP055 - Stoorstroomingreep mBvk1 na slip/stilstand */
    public static final int CH5162 = 5162;

    /** HSP056 - Stoorstroomingreep mBvk2 na slip/stilstand */
    public static final int CH5163 = 5163;

    /** HSP057 - Stoorstroomingreep mBv7 na slip/stilstand */
    public static final int CH5164 = 5164;

    /** HSP058 - SA1 defect, automatisch omlaag gestuurd. */
    public static final int CH5165 = 5165;

    /** HSP059 - SA2 defect, automatisch omlaag gestuurd. */
    public static final int CH5166 = 5166;

    /** HSP060 - Autodrop bewaking SA1 defect (rijdend) */
    public static final int CH5167 = 5167;

    /** HSP061 - Autodrop bewaking SA2 defect (rijdend) */
    public static final int CH5168 = 5168;

    /** HSP062 - Autodrop SA1 actief/defect/buiten bedrijf */
    public static final int CH5169 = 5169;

    /** HSP063 - Autodrop SA2 actief/defect/buiten bedrijf */
    public static final int CH5170 = 5170;

    /** HSP064 - Ing. luchtdruk stroomafn. OK defect (68A4) */
    public static final int CH5171 = 5171;

    /** HSP065 - Ing. stroomafnemer gek. 1/2 defect (68A3) */
    public static final int CH5172 = 5172;

    /** HSP066 - Snelschakelaar uit (stroomafnemer niet op) */
    public static final int CH5173 = 5173;

    /** HSP067 - Snelschakelaar uit (stroomafnemer op) */
    public static final int CH5174 = 5174;

    /** LVZ001 - Uitval Compressor */
    public static final int CH5175 = 5175;

    /** LVZ002 - HR druk te laag, lager dan 5 Bar */
    public static final int CH5176 = 5176;

    /** LVZ003 - HR druk te laag, compressor gevraagd */
    public static final int CH5177 = 5177;

    /** LVZ004 - Defecte PU omzetter of drukschakelaar HR */
    public static final int CH5178 = 5178;

    /** LVZ005 - Defecte PU omzetter of drukschakelaar HR */
    public static final int CH5179 = 5179;

    /** LVZ006 - HR druk te laag, lager dan 7,5 bar */
    public static final int CH5180 = 5180;

    /** LVZ040 - Geheugen diaprintje 34A11 gestoord ABv34 */
    public static final int CH5181 = 5181;

    /** LVZ041 - Overstroom bij opstart 34A11 1x ABv34 */
    public static final int CH5182 = 5182;

    /** LVZ042 - Overstroom bij opstart 34A11 2x ABv34 */
    public static final int CH5183 = 5183;

    /** LVZ043 - Overstroom bij opstart 34A11 3x ABv34 */
    public static final int CH5184 = 5184;

    /** LVZ044 - Overstroom 34A11 in bedrijf ABv34 */
    public static final int CH5185 = 5185;

    /** LVZ045 - Overtemperatuur 34A11 ABv34 */
    public static final int CH5186 = 5186;

    /** LVZ046 - Zelftest faalt (inv. getript) 34A11 ABv34 */
    public static final int CH5187 = 5187;

    /** LVZ047 - Storing besturingsprint 34A11, ABv3/4 */
    public static final int CH5188 = 5188;

    /** LVZ048 - Uitval comm. besturingsprint 34A11, ABv3/4 */
    public static final int CH5189 = 5189;

    /** LVZ049 - Overstroom tijdens uitschak 34A11 ABv34 */
    public static final int CH5190 = 5190;

    /** LVZ050 - Storing in inschakelcircuit compressor */
    public static final int CH5191 = 5191;

    /** LVZ051 - Compressor inverter 34A11 defect */
    public static final int CH5192 = 5192;

    /** LVZ052 - Uitval compressor bij depotvoeding */
    public static final int CH5193 = 5193;

    /** LVZ053 - Storing uitgangscircuit compressorinverter */
    public static final int CH5194 = 5194;

    /** LVZ054 - Compressorinv. 34A11 bereikt geen 50 Hz */
    public static final int CH5195 = 5195;

    /** LVZ055 - Geen omschakeling voed.34A11 naar 326A11 */
    public static final int CH5196 = 5196;

    /** LVZ056 - Ontgrendeling omschak. voeding werkt niet */
    public static final int CH5197 = 5197;

    /** LVZ057 - Uitval compressor (326A11 niet defect) */
    public static final int CH5198 = 5198;

    /** LVZ058 - Uitval compressor (326A11 defect) */
    public static final int CH5199 = 5199;

    /** LVZ059 - Compresssorinverter 34A11 start niet op. */
    public static final int CH5200 = 5200;

    /** LVZ060 - Inverter 34A11 start niet op, compressor in */
    public static final int CH5201 = 5201;

    /** LVZ061 - Inv. 34A11 start niet op, compressor uit */
    public static final int CH5202 = 5202;

    /** LVZ062 - Inv. 326A11 start niet op (intern defect) */
    public static final int CH5203 = 5203;

    /** LVZ063 - Inverter 34A11 communiceert niet */
    public static final int CH5204 = 5204;

    /** LVZ064 - Inverter 326A11 communiceert niet */
    public static final int CH5205 = 5205;

    /** LVZ070 - Uitval Compressor VIRMm */
    public static final int CH5206 = 5206;

    /** LVZ071 - Defecte PU omzetter of drukschak. HR VIRMm */
    public static final int CH5207 = 5207;

    /** LVZ072 - Defecte PU omzetter of drukschak. HR VIRMm */
    public static final int CH5208 = 5208;

    /** LVZ073 - HR druk te laag, lager dan 7,5 bar VIRMm */
    public static final int CH5209 = 5209;

    /** LVZ074 - Uitval compressor bij depotvoeding VIRMm */
    public static final int CH5210 = 5210;

    /** LVZ075 - Inv.34A11 start niet op, compres.uit VIRMm */
    public static final int CH5211 = 5211;

    /** LVZ076 - HR druk te laag, compressor gevraagd VIRMm */
    public static final int CH5212 = 5212;

    /** TR0001 - Automaat 2F7 aangesproken mBvk1 */
    public static final int CH5213 = 5213;

    /** TR0002 - Automaat 2F7 aangesproken mBvk2 */
    public static final int CH5214 = 5214;

    /** TR0003 - Terugval 12A1_MDC_VRAAN_OKE */
    public static final int CH5215 = 5215;

    /** TR0004 - Terugval 22A1_MDC_VRAAN_OKE */
    public static final int CH5216 = 5216;

    /** TR0005 - Terugval 72A1_VRAAN_OKE */
    public static final int CH5217 = 5217;

    /** TR0010 - Terugval 12A1_MDC_OL */
    public static final int CH5218 = 5218;

    /** TR0011 - Terugval 22A1_MDC_OL */
    public static final int CH5219 = 5219;

    /** TR0012 - Terugval 72A1_OL */
    public static final int CH5220 = 5220;

    /** TRC003 - Laadfout zonder historie mBvk1 */
    public static final int CH5221 = 5221;

    /** TRC004 - Laadfout zonder historie mBvk2 */
    public static final int CH5222 = 5222;

    /** TRC005 - Laadfout zonder historie mBv7 */
    public static final int CH5223 = 5223;

    /** TRC008 - Laadfout na Uc bewaking mBvk1 */
    public static final int CH5224 = 5224;

    /** TRC009 - Laadfout na Uc bewaking mBvk2 */
    public static final int CH5225 = 5225;

    /** TRC010 - Laadfout na Uc bewaking mBv7 */
    public static final int CH5226 = 5226;

    /** TRC013 - Differentiaalbev. aangesproken mBvk1 */
    public static final int CH5227 = 5227;

    /** TRC014 - Differentiaalbev. aangesproken mBvk2 */
    public static final int CH5228 = 5228;

    /** TRC015 - Differentiaalbev. aangesproken mBv7 */
    public static final int CH5229 = 5229;

    /** TRC018 - FC2 Lijnschakelaar besturing faalt mBvk1 */
    public static final int CH5230 = 5230;

    /** TRC019 - FC2 Lijnschakelaar besturing faalt mBvk2 */
    public static final int CH5231 = 5231;

    /** TRC020 - FC2 Lijnschakelaar besturing faalt mBv7 */
    public static final int CH5232 = 5232;

    /** TRC023 - FC3 Lijnschakelaar schakelt niet uit mBvk1 */
    public static final int CH5233 = 5233;

    /** TRC024 - FC3 Lijnschakelaar schakelt niet uit mBvk2 */
    public static final int CH5234 = 5234;

    /** TRC025 - FC3 Lijnschakelaar schakelt niet uit mBv7 */
    public static final int CH5235 = 5235;

    /** TRC028 - FC4 Lijnschakelaar extern geopend mBvk1 */
    public static final int CH5236 = 5236;

    /** TRC029 - FC4 Lijnschakelaar extern geopend mBvk2 */
    public static final int CH5237 = 5237;

    /** TRC030 - FC4 Lijnschakelaar extern geopend mBv7 */
    public static final int CH5238 = 5238;

    /** TRC033 - Laadfout na UcMIN ingreep mBvk1 */
    public static final int CH5239 = 5239;

    /** TRC034 - Laadfout na UcMIN ingreep mBvk2 */
    public static final int CH5240 = 5240;

    /** TRC035 - Laadfout na UcMIN ingreep mBv7 */
    public static final int CH5241 = 5241;

    /** TRC038 - Laadfout na Ub bewaking mBvk1 */
    public static final int CH5242 = 5242;

    /** TRC039 - Laadfout na Ub bewaking mBvk2 */
    public static final int CH5243 = 5243;

    /** TRC040 - Laadfout na Ub bewaking mBv7 */
    public static final int CH5244 = 5244;

    /** TRC043 - FC7 Ub bewaking mBvk1 */
    public static final int CH5245 = 5245;

    /** TRC044 - FC7 Ub bewaking mBvk2 */
    public static final int CH5246 = 5246;

    /** TRC045 - FC7 Ub bewaking mBv7 */
    public static final int CH5247 = 5247;

    /** TRC048 - FC8 Defect aan spanningsmeting Uc mBvk1 */
    public static final int CH5248 = 5248;

    /** TRC049 - FC8 Defect aan spanningsmeting Uc mBvk2 */
    public static final int CH5249 = 5249;

    /** TRC050 - FC8 Defect aan spanningsmeting Uc mBv7 */
    public static final int CH5250 = 5250;

    /** TRC053 - FC9 Te lage spanning Uc mBvk1 */
    public static final int CH5251 = 5251;

    /** TRC054 - FC9 Te lage spanning Uc mBvk2 */
    public static final int CH5252 = 5252;

    /** TRC055 - FC9 Te lage spanning Uc mBv7 */
    public static final int CH5253 = 5253;

    /** TRC058 - Communicatieuitval met module 1A1 mBvk1 */
    public static final int CH5254 = 5254;

    /** TRC059 - Communicatieuitval met module 1A1 mBvk2 */
    public static final int CH5255 = 5255;

    /** TRC060 - Communicatieuitval met module 1A1 mBv7 */
    public static final int CH5256 = 5256;

    /** TRC063 - FC12 Ontlaadfout mBvk1 */
    public static final int CH5257 = 5257;

    /** TRC064 - FC12 Ontlaadfout mBvk2 */
    public static final int CH5258 = 5258;

    /** TRC065 - FC12 Ontlaadfout mBv7 */
    public static final int CH5259 = 5259;

    /** TRC073 - FC13 Overstroom motoren (4x in 1min) mBvk1 */
    public static final int CH5260 = 5260;

    /** TRC074 - FC13 Overstroom motoren (4x in 1min) mBvk2 */
    public static final int CH5261 = 5261;

    /** TRC075 - FC13 Overstroom motoren (4x in 1 min) mBv7 */
    public static final int CH5262 = 5262;

    /** TRC078 - FC14 dIm/dt te groot mBvk1 */
    public static final int CH5263 = 5263;

    /** TRC079 - FC14 dIm/dt te groot mBvk2 */
    public static final int CH5264 = 5264;

    /** TRC080 - FC14 dIm/dt te groot mBv7 */
    public static final int CH5265 = 5265;

    /** TRC083 - FC15 Uitval voeding module 1A2 mBvk1 */
    public static final int CH5266 = 5266;

    /** TRC084 - FC15 Uitval voeding module 1A2 mBvk2 */
    public static final int CH5267 = 5267;

    /** TRC085 - FC15 Uitval voeding module 1A2 mBv7 */
    public static final int CH5268 = 5268;

    /** TRC088 - FC16 Uitval voeding module 1A3 mBvk1 */
    public static final int CH5269 = 5269;

    /** TRC089 - FC16 Uitval voeding module 1A3 mBvk2 */
    public static final int CH5270 = 5270;

    /** TRC090 - FC16 Uitval voeding module 1A3 mBv7 */
    public static final int CH5271 = 5271;

    /** TRC093 - Communicatieuitval met module 1A2 mBvk1 */
    public static final int CH5272 = 5272;

    /** TRC094 - Communicatieuitval met module 1A2 mBvk2 */
    public static final int CH5273 = 5273;

    /** TRC095 - Communicatieuitval met module 1A2 mBv7 */
    public static final int CH5274 = 5274;

    /** TRC098 - Communicatieuitval met module 1A3 mBvk1 */
    public static final int CH5275 = 5275;

    /** TRC099 - Communicatieuitval met module 1A3 mBvk2 */
    public static final int CH5276 = 5276;

    /** TRC100 - Communicatieuitval met module 1A3 mBv7 */
    public static final int CH5277 = 5277;

    /** TRC103 - FC19 Remweerstand te warm mBvk1 */
    public static final int CH5278 = 5278;

    /** TRC104 - FC19 Remweerstand te warm mBvk2 */
    public static final int CH5279 = 5279;

    /** TRC105 - FC19 Remweerstand te warm mBv7 */
    public static final int CH5280 = 5280;

    /** TRC108 - FC20 Uitval voeding module 1A4 mBvk1 */
    public static final int CH5281 = 5281;

    /** TRC109 - FC20 Uitval voeding module 1A4 mBvk2 */
    public static final int CH5282 = 5282;

    /** TRC110 - FC20 Uitval voeding module 1A4 mBv7 */
    public static final int CH5283 = 5283;

    /** TRC113 - FC21 Uitval voeding module 1A1 mBvk1 */
    public static final int CH5284 = 5284;

    /** TRC114 - FC21 Uitval voeding module 1A1 mBvk2 */
    public static final int CH5285 = 5285;

    /** TRC115 - FC21 Uitval voeding module 1A1 mBv7 */
    public static final int CH5286 = 5286;

    /** TRC118 - FC22 Beveiligingsprint defect mBvk1 */
    public static final int CH5287 = 5287;

    /** TRC119 - FC22 Beveiligingsprint defect mBvk2 */
    public static final int CH5288 = 5288;

    /** TRC120 - FC22 Beveiligingsprint defect mBv7 */
    public static final int CH5289 = 5289;

    /** TRC123 - Communicatieuitval met module 1A4 mBvk1 */
    public static final int CH5290 = 5290;

    /** TRC124 - Communicatieuitval met module 1A4 mBvk2 */
    public static final int CH5291 = 5291;

    /** TRC125 - Communicatieuitval met module 1A4 mBv7 */
    public static final int CH5292 = 5292;

    /** TRC128 - FC24 Pulskop motor 1 circuit defect mBvk1 */
    public static final int CH5293 = 5293;

    /** TRC129 - FC24 Pulskop motor 1 circuit defect mBvk2 */
    public static final int CH5294 = 5294;

    /** TRC130 - FC24 Pulskop motor 1 circuit defect mBv7 */
    public static final int CH5295 = 5295;

    /** TRC133 - FC25 Pulskop motor 2 circuit defect mBvk1 */
    public static final int CH5296 = 5296;

    /** TRC134 - FC25 Pulskop motor 2 circuit defect mBvk2 */
    public static final int CH5297 = 5297;

    /** TRC135 - FC25 Pulskop motor 2 circuit defect mBv7 */
    public static final int CH5298 = 5298;

    /** TRC138 - FC26 Beide pulskoppen defect mBvk1 */
    public static final int CH5299 = 5299;

    /** TRC139 - FC26 Beide pulskoppen defect mBvk2 */
    public static final int CH5300 = 5300;

    /** TRC140 - FC26 Beide pulskoppen defect mBv7 */
    public static final int CH5301 = 5301;

    /** TRC153 - 1A1 geen comm. & defect 1e keer mBvk1 */
    public static final int CH5302 = 5302;

    /** TRC154 - 1A1 geen comm. & defect 1e keer mBvk2 */
    public static final int CH5303 = 5303;

    /** TRC155 - 1A1 geen comm. & defect 1e keer mBv7 */
    public static final int CH5304 = 5304;

    /** TRC158 - 1A2 geen comm. & defect 1e keer mBvk1 */
    public static final int CH5305 = 5305;

    /** TRC159 - 1A2 geen comm. & defect 1e keer mBvk2 */
    public static final int CH5306 = 5306;

    /** TRC160 - 1A2 geen comm. & defect 1e keer mBv7 */
    public static final int CH5307 = 5307;

    /** TRC163 - 1A3 geen comm. & defect 1e keer mBvk1 */
    public static final int CH5308 = 5308;

    /** TRC164 - 1A3 geen comm. & defect 1e keer mBvk2 */
    public static final int CH5309 = 5309;

    /** TRC165 - 1A3 geen comm. & defect 1e keer mBv7 */
    public static final int CH5310 = 5310;

    /** TRC168 - 1A4 geen comm. & defect 1e keer mBvk1 */
    public static final int CH5311 = 5311;

    /** TRC169 - 1A4 geen comm. & defect 1e keer mBvk2 */
    public static final int CH5312 = 5312;

    /** TRC170 - 1A4 geen comm. & defect 1e keer mBv7 */
    public static final int CH5313 = 5313;

    /** TRC173 - 1A1 geen comm. & defect 2e keer mBvk1 */
    public static final int CH5314 = 5314;

    /** TRC174 - 1A1 geen comm. & defect 2e keer mBvk2 */
    public static final int CH5315 = 5315;

    /** TRC175 - 1A1 geen comm. & defect 2e keer mBv7 */
    public static final int CH5316 = 5316;

    /** TRC178 - 1A2 geen comm. & defect 2e keer mBvk1 */
    public static final int CH5317 = 5317;

    /** TRC179 - 1A2 geen comm. & defect 2e keer mBvk2 */
    public static final int CH5318 = 5318;

    /** TRC180 - 1A2 geen comm. & defect 2e keer mBv7 */
    public static final int CH5319 = 5319;

    /** TRC183 - Geen koeling SWITZ-modules mBvk1 */
    public static final int CH5320 = 5320;

    /** TRC184 - Geen koeling SWITZ-modules mBvk2 */
    public static final int CH5321 = 5321;

    /** TRC185 - Geen koeling SWITZ-modules mBv7 */
    public static final int CH5322 = 5322;

    /** TRC193 - 1A3 geen comm. & defect 2e keer mBvk1 */
    public static final int CH5323 = 5323;

    /** TRC194 - 1A3 geen comm. & defect 2e keer mBvk2 */
    public static final int CH5324 = 5324;

    /** TRC195 - 1A3 geen comm. & defect 2e keer mBv7 */
    public static final int CH5325 = 5325;

    /** TRC198 - 1A4 geen comm. & defect 2e keer mBvk1 */
    public static final int CH5326 = 5326;

    /** TRC199 - 1A4 geen comm. & defect 2e keer mBvk2 */
    public static final int CH5327 = 5327;

    /** TRC200 - 1A4 geen comm. & defect 2e keer mBv7 */
    public static final int CH5328 = 5328;

    /** TRC203 - 1A1 top/bottom 1e keer mBvk1 */
    public static final int CH5329 = 5329;

    /** TRC204 - 1A1 top/bottom 1e keer mBvk2 */
    public static final int CH5330 = 5330;

    /** TRC205 - 1A1 top/bottom 1e keer mBv7 */
    public static final int CH5331 = 5331;

    /** TRC208 - 1A2 top/bottom 1e keer mBvk1 */
    public static final int CH5332 = 5332;

    /** TRC209 - 1A2 top/bottom 1e keer mBvk2 */
    public static final int CH5333 = 5333;

    /** TRC210 - 1A2 top/bottom 1e keer mBv7 */
    public static final int CH5334 = 5334;

    /** TRC213 - 1A3 top/bottom 1e keer mBvk1 */
    public static final int CH5335 = 5335;

    /** TRC214 - 1A3 top/bottom 1e keer mBvk2 */
    public static final int CH5336 = 5336;

    /** TRC215 - 1A3 top/bottom 1e keer mBv7 */
    public static final int CH5337 = 5337;

    /** TRC218 - 1A4 top/bottom 1e keer mBvk1 */
    public static final int CH5338 = 5338;

    /** TRC219 - 1A4 top/bottom 1e keer mBvk2 */
    public static final int CH5339 = 5339;

    /** TRC220 - 1A4 top/bottom 1e keer mBv7 */
    public static final int CH5340 = 5340;

    /** TRC223 - 1A1 top/bottom 2e keer mBvk1 */
    public static final int CH5341 = 5341;

    /** TRC224 - 1A1 top/bottom 2e keer mBvk2 */
    public static final int CH5342 = 5342;

    /** TRC225 - 1A1 top/bottom 2e keer mBv7 */
    public static final int CH5343 = 5343;

    /** TRC228 - 1A2 top/bottom 2e keer mBvk1 */
    public static final int CH5344 = 5344;

    /** TRC229 - 1A2 top/bottom 2e keer mBvk2 */
    public static final int CH5345 = 5345;

    /** TRC230 - 1A2 top/bottom 2e keer mBv7 */
    public static final int CH5346 = 5346;

    /** TRC233 - 1A3 top/bottom 2e keer mBvk1 */
    public static final int CH5347 = 5347;

    /** TRC234 - 1A3 top/bottom 2e keer mBvk2 */
    public static final int CH5348 = 5348;

    /** TRC235 - 1A3 top/bottom 2e keer mBv7 */
    public static final int CH5349 = 5349;

    /** TRC238 - 1A4 top/bottom 2e keer mBvk1 */
    public static final int CH5350 = 5350;

    /** TRC239 - 1A4 top/bottom 2e keer mBvk2 */
    public static final int CH5351 = 5351;

    /** TRC240 - 1A4 top/bottom 2e keer mBv7 */
    public static final int CH5352 = 5352;

    /** TRC243 - Overstroom module 1A1 mBvk1 */
    public static final int CH5353 = 5353;

    /** TRC244 - Overstroom module 1A1 mBvk2 */
    public static final int CH5354 = 5354;

    /** TRC245 - Overstroom module 1A1 mBv7 */
    public static final int CH5355 = 5355;

    /** TRC248 - Print 2A116 defect mBvk1 (IO6 ingangen) */
    public static final int CH5356 = 5356;

    /** TRC249 - Print 2A116 defect mBvk2 (IO6 ingangen) */
    public static final int CH5357 = 5357;

    /** TRC250 - Print 2A116 defect mBv7 (IO6 ingangen) */
    public static final int CH5358 = 5358;

    /** TRC253 - Print 2A113 defect mBvk1 (IO5 ingangen) */
    public static final int CH5359 = 5359;

    /** TRC254 - Print 2A113 defect mBvk2 (IO5 ingangen) */
    public static final int CH5360 = 5360;

    /** TRC255 - Print 2A113 defect mBv7 (IO5 ingangen) */
    public static final int CH5361 = 5361;

    /** TRC258 - Bedrijfstemp. tractiemotor 1 mBvk1 is te hoog. */
    public static final int CH5362 = 5362;

    /** TRC259 - Bedrijfstemp. tractiemotor 1 mBvk2 is te hoog. */
    public static final int CH5363 = 5363;

    /** TRC260 - Bedrijfstemp. tractiemotor 1 mBv7 is te hoog. */
    public static final int CH5364 = 5364;

    /** TRC263 - Bedrijfstemp. tractiemotor 2 mBvk1 is te hoog. */
    public static final int CH5365 = 5365;

    /** TRC264 - Bedrijfstemp. tractiemotor 2 mBvk2 is te hoog. */
    public static final int CH5366 = 5366;

    /** TRC265 - Bedrijfstemp. tractiemotor 2 mBv7 is te hoog. */
    public static final int CH5367 = 5367;

    /** TRC268 - Temp. voeler tractiemotor 1 mBvk1 is defect. */
    public static final int CH5368 = 5368;

    /** TRC269 - Temp. voeler tractiemotor 1 mBvk2 is defect. */
    public static final int CH5369 = 5369;

    /** TRC270 - Temp. voeler tractiemotor 1 mBv7 is defect. */
    public static final int CH5370 = 5370;

    /** TRC273 - Temp. voeler tractiemotor 2 mBvk1 is defect. */
    public static final int CH5371 = 5371;

    /** TRC274 - Temp. voeler tractiemotor 2 mBvk2 is defect. */
    public static final int CH5372 = 5372;

    /** TRC275 - Temp. voeler tractiemotor 2 mBv7 is defect. */
    public static final int CH5373 = 5373;

    /** TRC278 - Ingang of uitgang motorvent. hoog mBvk1 */
    public static final int CH5374 = 5374;

    /** TRC279 - Ingang of uitgang motorvent. hoog mBvk2 */
    public static final int CH5375 = 5375;

    /** TRC280 - Ingang of uitgang motorvent. hoog mBv7 */
    public static final int CH5376 = 5376;

    /** TRC282 - Ventilatie tractiemotor gestoord mBvk1 */
    public static final int CH5377 = 5377;

    /** TRC284 - Ventilatie tractiemotor gestoord mBvk2 */
    public static final int CH5378 = 5378;

    /** TRC285 - Ventilatie tractiemotor gestoord mBv7 */
    public static final int CH5379 = 5379;

    /** TRC287 - Ventilatie HS ruimte gestoord mBvk1 */
    public static final int CH5380 = 5380;

    /** TRC289 - Ventilatie HS ruimte gestoord mBvk2 */
    public static final int CH5381 = 5381;

    /** TRC290 - Ventilatie HS ruimte gestoord mBv7 */
    public static final int CH5382 = 5382;

    /** TRC293 - Ventilatie afl.weerstand gestoord mBvk1 */
    public static final int CH5383 = 5383;

    /** TRC294 - Ventilatie afl.weerstand gestoord mBvk2 */
    public static final int CH5384 = 5384;

    /** TRC295 - Ventilatie afl.weerstand gestoord mBv7 */
    public static final int CH5385 = 5385;

    /** TRC298 - Ingang 'Ubatt > 88V' onterecht laag mBvk1 */
    public static final int CH5386 = 5386;

    /** TRC299 - Ingang 'Ubatt > 88V' onterecht laag mBvk2 */
    public static final int CH5387 = 5387;

    /** TRC300 - Ingang 'Ubatt > 88V' onterecht laag mBv7 */
    public static final int CH5388 = 5388;

    /** TRC308 - RS422 comm. 2A1-4A1 gestoord mBvk1 */
    public static final int CH5389 = 5389;

    /** TRC309 - RS422 comm. 2A1-4A1 gestoord mBvk2 */
    public static final int CH5390 = 5390;

    /** TRC310 - RS422 comm. 2A1-4A1 gestoord mBv7 */
    public static final int CH5391 = 5391;

    /** TRC313 - Ingang analoge koppelref defect mBvk1 */
    public static final int CH5392 = 5392;

    /** TRC314 - Ingang analoge koppelref defect mBvk2 */
    public static final int CH5393 = 5393;

    /** TRC315 - Ingang analoge koppelref defect mBv7 */
    public static final int CH5394 = 5394;

    /** TRC318 - Ingang ILLIM1 onterecht hoog mBvk1 */
    public static final int CH5395 = 5395;

    /** TRC319 - Ingang ILLIM1 onterecht hoog mBvk2 */
    public static final int CH5396 = 5396;

    /** TRC320 - Ingang ILLIM1 onterecht hoog mBv7 */
    public static final int CH5397 = 5397;

    /** TRC323 - Ingang ILLIM1 onterecht laag mBvk1 */
    public static final int CH5398 = 5398;

    /** TRC324 - Ingang ILLIM1 onterecht laag mBvk2 */
    public static final int CH5399 = 5399;

    /** TRC325 - Ingang ILLIM1 onterecht laag mBv7 */
    public static final int CH5400 = 5400;

    /** TRC328 - Ingang ILLIM2 onterecht hoog mBvk1 */
    public static final int CH5401 = 5401;

    /** TRC329 - Ingang ILLIM2 onterecht hoog mBvk2 */
    public static final int CH5402 = 5402;

    /** TRC330 - Ingang ILLIM2 onterecht hoog mBv7 */
    public static final int CH5403 = 5403;

    /** TRC333 - Ingang ILLIM2 onterecht laag mBvk1 */
    public static final int CH5404 = 5404;

    /** TRC334 - Ingang ILLIM2 onterecht laag mBvk2 */
    public static final int CH5405 = 5405;

    /** TRC335 - Ingang ILLIM2 onterecht laag mBv7 */
    public static final int CH5406 = 5406;

    /** TRC336 - Ingang TRC_BB onterecht hoog mBvk1 */
    public static final int CH5407 = 5407;

    /** TRC337 - Ingang TRC_BB onterecht hoog mBvk2 */
    public static final int CH5408 = 5408;

    /** TRC340 - Ingang TRC_BB onterecht hoog mBv7 */
    public static final int CH5409 = 5409;

    /** TRC344 - Ingang ILLIM1 defect mBvk1 of mBvk2 */
    public static final int CH5410 = 5410;

    /** TRC350 - Ingang ILLIM2 defect mBvk1 of mBvk2 */
    public static final int CH5411 = 5411;

    /** TRC351 - TRC's schakelen gelijktijdig in */
    public static final int CH5412 = 5412;

    /** TRC355 - Ingang STRAFN onterecht hoog mBvk1 */
    public static final int CH5413 = 5413;

    /** TRC356 - Ingang STRAFN onterecht hoog mBvk2 */
    public static final int CH5414 = 5414;

    /** TRC359 - Ingang STRAFN onterecht laag mBvk1 */
    public static final int CH5415 = 5415;

    /** TRC360 - Ingang STRAFN onterecht laag mBvk2 */
    public static final int CH5416 = 5416;

    /** TRC363 - Ingang rijbevel onterecht laag mBvk1 */
    public static final int CH5417 = 5417;

    /** TRC364 - Ingang rijbevel onterecht laag mBvk2 */
    public static final int CH5418 = 5418;

    /** TRC365 - Ingang rijbevel onterecht laag mBv7 */
    public static final int CH5419 = 5419;

    /** TRC368 - Ingang rijbevel onterecht hoog mBvk1 */
    public static final int CH5420 = 5420;

    /** TRC369 - Ingang rijbevel onterecht hoog mBvk2 */
    public static final int CH5421 = 5421;

    /** TRC370 - Ingang rijbevel onterecht hoog mBv7 */
    public static final int CH5422 = 5422;

    /** TRC373 - Ingang 20% aanzet onterecht laag mBvk1 */
    public static final int CH5423 = 5423;

    /** TRC374 - Ingang 20% aanzet onterecht laag mBvk2 */
    public static final int CH5424 = 5424;

    /** TRC375 - Ingang 20% aanzet onterecht laag mBv7 */
    public static final int CH5425 = 5425;

    /** TRC378 - Ingang 20% aanzet onterecht hoog mBvk1 */
    public static final int CH5426 = 5426;

    /** TRC379 - Ingang 20% aanzet onterecht hoog mBvk2 */
    public static final int CH5427 = 5427;

    /** TRC380 - Ingang 20% aanzet onterecht hoog mBv7 */
    public static final int CH5428 = 5428;

    /** TRC383 - Ingang 95% aanzet onterecht laag mBvk1 */
    public static final int CH5429 = 5429;

    /** TRC384 - Ingang 95% aanzet onterecht laag mBvk2 */
    public static final int CH5430 = 5430;

    /** TRC385 - Ingang 95% aanzet onterecht laag mBv7 */
    public static final int CH5431 = 5431;

    /** TRC388 - Ingang 95% aanzet onterecht hoog mBvk1 */
    public static final int CH5432 = 5432;

    /** TRC389 - Ingang 95% aanzet onterecht hoog mBvk2 */
    public static final int CH5433 = 5433;

    /** TRC390 - Ingang 95% aanzet onterecht hoog mBv7 */
    public static final int CH5434 = 5434;

    /** TRC393 - Ingang NEMB onterecht laag mBvk1 */
    public static final int CH5435 = 5435;

    /** TRC394 - Ingang NEMB onterecht laag mBvk2 */
    public static final int CH5436 = 5436;

    /** TRC395 - Ingang NEMB onterecht laag mBv7 */
    public static final int CH5437 = 5437;

    /** TRC398 - Ingang NEMB onterecht hoog mBvk1 */
    public static final int CH5438 = 5438;

    /** TRC399 - Ingang NEMB onterecht hoog mBvk2 */
    public static final int CH5439 = 5439;

    /** TRC400 - Ingang NEMB onterecht hoog mBv7 */
    public static final int CH5440 = 5440;

    /** TRC403 - Ingang AANVRIJ onterecht laag mBvk1 */
    public static final int CH5441 = 5441;

    /** TRC404 - Ingang AANVRIJ onterecht laag mBvk2 */
    public static final int CH5442 = 5442;

    /** TRC405 - Ingang AANVRIJ onterecht laag mBv7 */
    public static final int CH5443 = 5443;

    /** TRC408 - Ingang AANVRIJ onterecht hoog mBvk1 */
    public static final int CH5444 = 5444;

    /** TRC409 - Ingang AANVRIJ onterecht hoog mBvk2 */
    public static final int CH5445 = 5445;

    /** TRC410 - Ingang AANVRIJ onterecht hoog mBv7 */
    public static final int CH5446 = 5446;

    /** TRC428 - Ingang SSONOK onterecht laag mBvk1 */
    public static final int CH5447 = 5447;

    /** TRC429 - Ingang SSONOK onterecht laag mBvk2 */
    public static final int CH5448 = 5448;

    /** TRC430 - Ingang SSONOK onterecht laag mBv7 */
    public static final int CH5449 = 5449;

    /** TRC433 - Ing. SSONOK OH/ ing. SS vergr RIO OL (mBvk1) */
    public static final int CH5450 = 5450;

    /** TRC434 - Ing. SSONOK OH/ ing. SS vergr RIO OL (mBvk2) */
    public static final int CH5451 = 5451;

    /** TRC435 - Ing. SSONOK OH/ ing. SS vergr RIO OL (mBv7) */
    public static final int CH5452 = 5452;

    /** TRC438 - Ingang HSVRIJ onterecht laag mBvk1 */
    public static final int CH5453 = 5453;

    /** TRC439 - Ingang HSVRIJ onterecht laag mBvk2 */
    public static final int CH5454 = 5454;

    /** TRC440 - Ingang HSVRIJ onterecht laag mBv7 */
    public static final int CH5455 = 5455;

    /** TRC443 - Ingang HSVRIJ onterecht hoog mBvk1 */
    public static final int CH5456 = 5456;

    /** TRC444 - Ingang HSVRIJ onterecht hoog mBvk2 */
    public static final int CH5457 = 5457;

    /** TRC445 - Ingang HSVRIJ onterecht hoog mBv7 */
    public static final int CH5458 = 5458;

    /** TRC453 - Ingang vooruit onterecht laag mBvk1 */
    public static final int CH5459 = 5459;

    /** TRC454 - Ingang vooruit onterecht laag mBvk2 */
    public static final int CH5460 = 5460;

    /** TRC455 - Ingang vooruit onterecht laag mBv7 */
    public static final int CH5461 = 5461;

    /** TRC458 - Ingang vooruit onterecht hoog mBvk1 */
    public static final int CH5462 = 5462;

    /** TRC459 - Ingang vooruit onterecht hoog mBvk2 */
    public static final int CH5463 = 5463;

    /** TRC460 - Ingang vooruit onterecht hoog mBv7 */
    public static final int CH5464 = 5464;

    /** TRC463 - Rijrichtingfout mBvk1 */
    public static final int CH5465 = 5465;

    /** TRC464 - Rijrichtingfout mBvk2 */
    public static final int CH5466 = 5466;

    /** TRC465 - Rijrichtingfout mBv7 */
    public static final int CH5467 = 5467;

    /** TRC468 - Ingang achteruit onterecht laag mBvk1 */
    public static final int CH5468 = 5468;

    /** TRC469 - Ingang achteruit onterecht laag mBvk2 */
    public static final int CH5469 = 5469;

    /** TRC470 - Ingang achteruit onterecht laag mBv7 */
    public static final int CH5470 = 5470;

    /** TRC473 - Ingang achteruit onterecht hoog mBvk1 */
    public static final int CH5471 = 5471;

    /** TRC474 - Ingang achteruit onterecht hoog mBvk2 */
    public static final int CH5472 = 5472;

    /** TRC475 - Ingang achteruit onterecht hoog mBv7 */
    public static final int CH5473 = 5473;

    /** TRC478 - Uitval tractie mBvk1 */
    public static final int CH5474 = 5474;

    /** TRC479 - Uitval tractie mBvk2 */
    public static final int CH5475 = 5475;

    /** TRC480 - Uitval tractie mBv7 */
    public static final int CH5476 = 5476;

    /** TRC483 - RS485 interface defect mBvk1 */
    public static final int CH5477 = 5477;

    /** TRC484 - RS485 interface defect mBvk2 */
    public static final int CH5478 = 5478;

    /** TRC485 - RS485 interface defect mBv7 */
    public static final int CH5479 = 5479;

    /** TRC486 - Tractie mBvk1 buiten bedrijf geschakeld */
    public static final int CH5480 = 5480;

    /** TRC487 - Tractie mBvk2 buiten bedrijf geschakeld */
    public static final int CH5481 = 5481;

    /** TRC488 - Tractie mBv7 buiten bedrijf geschakeld */
    public static final int CH5482 = 5482;

    /** TRC493 - Frequentiesignaal koppelref BC mBvk1 */
    public static final int CH5483 = 5483;

    /** TRC494 - Frequentiesignaal koppelref BC mBvk2 */
    public static final int CH5484 = 5484;

    /** TRC495 - Geen vrijgave aanzetten vanuit mBvk1 */
    public static final int CH5485 = 5485;

    /** TRC496 - Geen vrijgave aanzetten vanuit mBvk2 */
    public static final int CH5486 = 5486;

    /** TRC497 - Geen rijbevel vanuit mBvk1 */
    public static final int CH5487 = 5487;

    /** TRC498 - Geen rijbevel vanuit mBvk2 */
    public static final int CH5488 = 5488;

    /** TRC499 - Geen 20% aanzetkoppel vanuit mBvk1 */
    public static final int CH5489 = 5489;

    /** TRC500 - Geen 20% aanzetkoppel vanuit mBvk2 */
    public static final int CH5490 = 5490;

    /** TRC501 - Rijrichtingfout BC mBvk1 */
    public static final int CH5491 = 5491;

    /** TRC502 - Rijrichtingfout BC mBvk2 */
    public static final int CH5492 = 5492;

    /** TRC503 - Storing treindr vooruit trstel12 */
    public static final int CH5493 = 5493;

    /** TRC504 - Storing treindr vooruit trstel23 */
    public static final int CH5494 = 5494;

    /** TRC505 - Storing treindr achteruit trstel12 */
    public static final int CH5495 = 5495;

    /** TRC506 - Storing treindr achteruit trstel23 */
    public static final int CH5496 = 5496;

    /** TRC507 - Storing treindr vrijgave aanz. trstel12 */
    public static final int CH5497 = 5497;

    /** TRC508 - Storing treindr vrijgave aanz. trstel23 */
    public static final int CH5498 = 5498;

    /** TRC509 - Storing treindr geen noodrem trstel12 */
    public static final int CH5499 = 5499;

    /** TRC510 - Storing treindr geen noodrem trstel23 */
    public static final int CH5500 = 5500;

    /** TRC511 - Storing treindr rijbevel trstel12 */
    public static final int CH5501 = 5501;

    /** TRC512 - Storing treindr rijbevel trstel23 */
    public static final int CH5502 = 5502;

    /** TRC513 - Storing treindr 20% aanzetk. trstel12 */
    public static final int CH5503 = 5503;

    /** TRC514 - Storing treindr 20% aanzetk. trstel23 */
    public static final int CH5504 = 5504;

    /** TRC515 - Storing treindr 95% aanzetk. trstel12 */
    public static final int CH5505 = 5505;

    /** TRC516 - Storing treindr 95% aanzetk. trstel23 */
    public static final int CH5506 = 5506;

    /** TRC517 - Storing treindr beperken lijnstr1 trstel12 */
    public static final int CH5507 = 5507;

    /** TRC518 - Storing treindr beperken lijnstr1 trstel23 */
    public static final int CH5508 = 5508;

    /** TRC519 - Storing treindr beperken lijnstr2 trstel12 */
    public static final int CH5509 = 5509;

    /** TRC520 - Storing treindr beperken lijnstr2 trstel23 */
    public static final int CH5510 = 5510;

    /** TRC521 - Automaat 2F7 aangesproken mBvk1 */
    public static final int CH5511 = 5511;

    /** TRC522 - Automaat 2F7 aangesproken mBvk2 */
    public static final int CH5512 = 5512;

    /** TRC523 - Automaat 2F7 aangesproken mBv7 */
    public static final int CH5513 = 5513;

    /** TRC536 - Overstroom module 1A2 mBvk1 */
    public static final int CH5514 = 5514;

    /** TRC537 - Overstroom module 1A2 mBvk2 */
    public static final int CH5515 = 5515;

    /** TRC538 - Overstroom module 1A2 mBv7 */
    public static final int CH5516 = 5516;

    /** TRC539 - Overstroom module 1A3 mBvk1 */
    public static final int CH5517 = 5517;

    /** TRC540 - Overstroom module 1A3 mBvk2 */
    public static final int CH5518 = 5518;

    /** TRC541 - Overstroom module 1A3 mBv7 */
    public static final int CH5519 = 5519;

    /** TRC542 - Overstroom module 1A4 mBvk1 */
    public static final int CH5520 = 5520;

    /** TRC543 - Overstroom module 1A4 mBvk2 */
    public static final int CH5521 = 5521;

    /** TRC544 - Overstroom module 1A4 mBv7 */
    public static final int CH5522 = 5522;

    /** TRC545 - Overtemperatuur module 1A1 mBvk1 */
    public static final int CH5523 = 5523;

    /** TRC546 - Overtemperatuur module 1A1 mBvk2 */
    public static final int CH5524 = 5524;

    /** TRC547 - Overtemperatuur module 1A1 mBv7 */
    public static final int CH5525 = 5525;

    /** TRC548 - Overtemperatuur module 1A2 mBvk1 */
    public static final int CH5526 = 5526;

    /** TRC549 - Overtemperatuur module 1A2 mBvk2 */
    public static final int CH5527 = 5527;

    /** TRC550 - Overtemperatuur module 1A2 mBv7 */
    public static final int CH5528 = 5528;

    /** TRC551 - Overtemperatuur module 1A3 mBvk1 */
    public static final int CH5529 = 5529;

    /** TRC552 - Overtemperatuur module 1A3 mBvk2 */
    public static final int CH5530 = 5530;

    /** TRC553 - Overtemperatuur module 1A3 mBv7 */
    public static final int CH5531 = 5531;

    /** TRC554 - Overtemperatuur module 1A4 mBvk1 */
    public static final int CH5532 = 5532;

    /** TRC555 - Overtemperatuur module 1A4 mBvk2 */
    public static final int CH5533 = 5533;

    /** TRC556 - Overtemperatuur module 1A4 mBv7 */
    public static final int CH5534 = 5534;

    /** Laatste GPS breedtegraad - VT_GPSLatitude */
    public static final int CH8001 = 8001;

    /** Laatste GPS lengtegraad - VT_GPSLongitude */
    public static final int CH8002 = 8002;

    /** Laatste GPS snelheid - VT_GPSSpeed */
    public static final int CH8003 = 8003;
    
}
