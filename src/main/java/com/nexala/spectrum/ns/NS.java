package com.nexala.spectrum.ns;

public class NS {

    // Datasources
    public static final String SQL_XML = "sql.xml";
    public static final String NS_DS = "java:/NSDS";
    public static final String NS_DS_FLYWAY = "java:/NSDS_FLYWAY";
    public static final String NS_DS_VIRM = "java:/NSDS_VIRM";
    public static final String NS_DS_VIRM_FLYWAY = "java:/NSDS_VIRM_FLYWAY";
    public static final String NS_DS_SNG = "java:/NSDS_SNG";
    public static final String NS_DS_SNG_FLYWAY = "java:/NSDS_SNG_FLYWAY";
    
    // Fleet Codes
    // Remove this, use config file instead
    public static final String FLEET_SLT = "SLT";
    public static final String FLEET_SLT_FAULT = "SLTFault";
    public static final String FLEET_VIRM = "VIRM";
    public static final String FLEET_VIRM_FAULT = "VIRMFault";
    public static final String FLEET_SNG = "SNG";
    public static final String FLEET_SNG_FAULT = "SNGFault";

    // Channel names
    public static final String CAB_1 = "Cab1";
    public static final String CAB_2 = "Cab2";
    public static final String GPS_LATITUDE_VIEW = "GpsLatitude";
    public static final String GPS_LONGITUDE_VIEW = "GpsLongitude";
    public static final String SPEED_VIEW = "Speed";
    public static final String LEADING_VEHICLE_VIEW = "LeadingVehicle";
    
    public static final String FAULT_P1_NUMBER = "P1FaultNumber";
    public static final String FAULT_P2_NUMBER = "P2FaultNumber";
    public static final String FAULT_P3_NUMBER = "P3FaultNumber";
    public static final String FAULT_P4_NUMBER = "P4FaultNumber";
    public static final String FAULT_P5_NUMBER = "P5FaultNumber";
    
    public static final String UNIT_STATUS = "UnitStatus";
    public static final String UNIT_MAINTENANCE_LOCATION = "UnitMaintLoc";
    public static final String UNIT_LAST_MAINTENANCE_DATE = "LastMaintDate";

    public static final String FAULT_NUMBER_RECOVERY = "FaultRecoveryNumber";
    public static final String FAULT_NUMBER_NOT_ACKNOWLEDGED = "FaultNotAcknowledgedNumber";
    public static final String OPERATIONAL_UNITS_COUNT = "UnitStatus";
    
    public static final String MAXIMO_SYSTEM_NAME = "Maximo";
    
    public static final String DIRECTION_LEVER = "DirectionLever";
    public static final String TRACTION_BRAKE_LEVER = "TractionBrakeLever";
    public static final String CIRCUIT_BREAKER_SWITCH ="CircuitBreakerSwitch";
    public static final String DIA_9016_VALUE ="Dia9016Value";

    public static final String ACTIVE_CAB = "ActiveCab";
    public static final String CAB1 = "CAB1";
    public static final String CAB2 = "CAB2";
    
    // Train Icon Names
    public static final String TRAIN_ICON_P1 = "train_p1";
    public static final String TRAIN_ICON_P2 = "train_p2";
    public static final String TRAIN_ICON_P3 = "train_p3";
    public static final String TRAIN_ICON_P4 = "train_p4";
    public static final String TRAIN_ICON_P5 = "train_p5";
    public static final String TRAIN_ICON_INACTIVE = "train_inactive";

    // Hardware types
    public static final String HARDWARE_TYPE_SLT_VALUES = "SLTValues";
    public static final String HARDWARE_TYPE_VIRM_COUNTERS = "Counters";
    public static final String HARDWARE_TYPE_VIRM_DEFAULT = "R2M Default Channels";
    public static final String HARDWARE_TYPE_VIRM_SENSOR = "Sensor Channels";
    public static final String HARDWARE_TYPE_EVENT = "Event";
    public static final String HARDWARE_TYPE_VT = "Virtual Train";

    // Fleet Summary columns
    public static final String FLEET_SUMMARY_GPS_COORDINATES = "GPSCoordinates";
    public static final String FLEET_SUMMARY_LATITUDE = "Latitude";
    public static final String FLEET_SUMMARY_LONGITUDE = "Longitude";
    public static final String FLEET_SUMMARY_DATE = "Date";
    
    public static final String HAS_P1_FAULTS = "HasP1Faults";
    public static final String HAS_P2_FAULTS = "HasP2Faults";
    public static final String HAS_P3_FAULTS = "HasP3Faults";
    public static final String HAS_P4_FAULTS = "HasP4Faults";
    public static final String HAS_P5_FAULTS = "HasP5Faults";
    public static final String HAS_NO_FAULTS = "HasNoFaults";
    public static final String HAS_FAULTS_RECOVERY = "HasFaultsWithRecovery";
    public static final String HAS_FAULTS_UNACKNOWLEDGED = "HasFaultsNotAcknowledged";
    public static final String HAS_OPERATIONAL_UNITS = "HasOperationalUnits";
    public static final String IS_UNIT1_IN_OPERATION = "IsUnit1InOperation";
    public static final String IS_UNIT2_IN_OPERATION = "IsUnit2InOperation";
    public static final String IS_UNIT3_IN_OPERATION = "IsUnit3InOperation";

    // Queries - see sql.xml
    public static final String SEARCH_UNITS_QUERY = "searchUnitsQuery";
    public static final String SEARCH_FLEET_UNITS_QUERY = "searchFleetUnitsQuery";

    public static final String HISTORIC_DATA_QUERY = "historicDataQuery";
    public static final String HISTORIC_EVENT_DATA_QUERY = "historicEventDataQuery";
    public static final String HISTORIC_CHANNEL_DATA = "historicChannelData";
    public static final String HISTORIC_EVENT_CHANNEL_DATA = "historicEventChannelData";
    public static final String EVENT_CHANNEL_DATA = "eventChannelData";
    public static final String EVENT_FAULT_CHANNEL_DATA = "eventFaultChannelData";
    public static final String EVENT_GPS_CHANNEL_DATA = "eventGpsChannelData";
    public static final String UNIT_LATEST_TIMESTAMP = "unitLatestTimestamp";
    public static final String UNIT_LATEST_TIMESTAMP_SLT = "unitLatestTimestampSlt";
    
    public static final String CHANNEL_VALUE_GPS_QUERY = "channelValueGpsLatestQuery";    
    public static final String HISTORIC_GPS_DATA_QUERY = "historicGpsDataQuery";
    public static final String HISTORIC_CHANNEL_VALUE_GPS_QUERY = "historicChannelValueGpsByTimeStampQuery";
    public static final String HISTORIC_GPS_CHANNEL_DATA = "historicGpsChannelData";
    
    public static final String FLEET_QUERY = "fleetQuery";
    public static final String HISTORIC_FLEET_QUERY_KEY = "historicFleetQuery";
    public static final String GROUP_CHANNEL_QUERY = "groupChannelQuery";

    public static final String FAULT_CHANNEL_DATA_UNIT = "faultChannelDataUnit";
    
    public static final String SAVE_SERVICE_REQUEST = "saveServiceRequest";
    public static final String ADD_SERVICE_REQUEST_TO_FAULT = "addServiceRequestToFault";
    
    public static final String UPDATE_FAULT_SERVICE_REQUEST = "updateFaultServiceRequest";
    public static final String FIND_EVENT_WITH_SERVICE_REQUEST = "findEventWithServiceRequest";
    public static final String CREATE_RULESENGINE_GROUP = "createRulesEngineGroup";
    public static final String ADD_FAULT_TO_GROUP = "addFaultToGroup";

    // Event fields IDs
    public static final String FAULT_GROUP_STATUS = "groupStatus";
    public static final String EVENT_TRAIN_CREW_ADVICE = "trainCrewAdvice";
    public static final String EVENT_CONTROL_ROOM_ADVICE = "controlRoomAdvice";
    public static final String EVENT_ENGINEER_ADVICE = "engineerAdvice";
    public static final String EVENT_QUALITY_PROFILE = "qualityProfile";
    public static final String EVENT_UNIT_OF_MEASURE = "unitOfMeasure";
    public static final String EVENT_CONTROL_ROOM_PRIORITY = "controlRoomPriority";
    public static final String EVENT_SAFEY_PRIORITY = "safetyPriority";
    public static final String EVENT_SERVICE_REQUEST_PRIORITY = "serviceRequestPriority";
    public static final String EVENT_MAINTENANCE_FUNCTION = "maintenanceFunction";
    public static final String EVENT_SYSTEM_CODE = "systemCode";
    public static final String EVENT_POSITION_CODE = "positionCode";
    
    public static final String EVENT_SERVICE_REQUEST = "maximoId";
    public static final String EVENT_GROUP_ID = "faultGroupId";
    public static final String EVENT_MAXIMO_FAULT_CODE = "maximoFaultCode";
    
    public static final String FAULT_NOT_GROUPED_KEY = "";
    public static final String FAULT_GROUPED_KEY = "grouped";
    public static final String FAULT_GROUP_LEAD_KEY = "groupLead";
    
    public static final String SERVICE_REQUEST_STATUS = "serviceRequestStatus";
    
    // Fleet Summary
    public static final String OUT_OF_SERVICE = "Out of Service";
    public static final String READY_FOR_SERVICE = "Ready for Service";
    public static final String IN_SERVICE = "In Service";
    public static final String TRACTION_GROUP = "Traction";
    public static final String BRAKES_GROUP = "Brakes";
    public static final String AIR_SUPPLY_GROUP = "Air Supply System";
    public static final String HVAC_GROUP = "HVAC";
    public static final String TOILETS_GROUP = "Toilet";
    public static final String COMMUNICATION_GROUP = "Communication";
    public static final String DOORS_GROUP = "Doors";
    public static final String SAFETY_GROUP = "Safety";
    public static final String LIGHTING_GROUP = "Lighting";
    public static final String HIGH_VOLTAGE_GROUP = "High Voltage System";
    public static final String LOW_VOLTAGE_GROUP = "Low Voltage System";
    public static final String OTHER_GROUP = "Other";
    
    // Map markers types
    public static final String MAP_DETECTOR_MARKER = "detector";
}
