package com.nexala.spectrum.ns;

import com.google.inject.servlet.ServletModule;
import com.nexala.spectrum.ns.service.MaximoServiceRequestService;

public class RestModule extends ServletModule {

    /**
     * @see com.google.inject.servlet.ServletModule#configureServlets()
     */
    @Override
    protected void configureServlets() {
        bind(MaximoServiceRequestService.class);
    }

}
