package com.nexala.spectrum.ns;

import com.google.inject.Module;
import com.nexala.spectrum.configuration.ModuleCollection;

public class ServicesModuleCollection implements ModuleCollection {
    /**
     * @see com.nexala.spectrum.configuration.ModuleCollection#getModules()
     */
    @Override
    public Module[] getModules() {
        return new Module[] {
                new RestModule()
                };
    }
}
