package com.nexala.spectrum.ns;

import com.google.inject.Module;
import com.nexala.spectrum.configuration.ModuleCollection;
import com.nexala.spectrum.ns.modules.NSModule;
import com.nexala.spectrum.ns.modules.ServiceModule;
import com.nexala.spectrum.ns.modules.fleets.FleetSltFaultModule;
import com.nexala.spectrum.ns.modules.fleets.FleetSltModule;
import com.nexala.spectrum.ns.modules.fleets.FleetSngFaultModule;
import com.nexala.spectrum.ns.modules.fleets.FleetSngModule;
import com.nexala.spectrum.ns.modules.fleets.FleetVirmFaultModule;
import com.nexala.spectrum.ns.modules.fleets.FleetVirmModule;

public class WebModuleCollection implements ModuleCollection {

    public Module[] getModules() {
        return new Module[] {
                new FleetSltModule(),
                new FleetSltFaultModule(),
                new FleetVirmModule(),
                new FleetVirmFaultModule(),
                new FleetSngModule(),
                new FleetSngFaultModule(),
                new NSModule(),
                new ServiceModule()
        };
    }

}
