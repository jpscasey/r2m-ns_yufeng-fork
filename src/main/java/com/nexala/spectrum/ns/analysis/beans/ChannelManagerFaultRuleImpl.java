package com.nexala.spectrum.ns.analysis.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.manager.AbstractChannelManager;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.typesafe.config.Config;

/**
 * Implementation of channel manager of r2m for the NS FaultRules engine
 * Initialize the channel manager with 'channels' from the fault table
 * @author BRedaha
 *
 */
@Singleton
public class ChannelManagerFaultRuleImpl extends AbstractChannelManager {
    private ChannelConfiguration channelConf;

    private ApplicationConfiguration appConf;

    private String fleetId;
    
    public static final String EXTRA_CHANNEL_HOUR = "hour";
    public static final String EXTRA_CHANNEL_MINUTE = "minute";
    public static final String EXTRA_CHANNEL_TIMESTAMP = "timestamp";
    public static final String EXTRA_CHANNEL_FAULT_COUNT = "faultcount";
    
    
    @Inject
    public ChannelManagerFaultRuleImpl(ChannelConfiguration channelConf, ApplicationConfiguration appConf,
            @Named(Spectrum.FLEET_ID) String fleetId) {
        this.channelConf = channelConf;
        this.appConf = appConf;
        this.fleetId = fleetId;
        initialize();
    }
    
    protected void initialize() {
        addExtraChannels();

        // Add the extra channels from the project configuration if any.
        String path = "r2m." + fleetId + ".stream-analysis.extra-channels";
        if (appConf.get().hasPath(path)) {
            List<? extends Config> config = appConf.get().getConfigList(path);
            for (Config c : config) {
                Class<?> returnType = null;
                String name = c.getString("name");
                String type = c.getString("type");

                if (type != null) {
                    if (type.equalsIgnoreCase("Double")) {
                        returnType = Double.TYPE;
                    } else if (type.equalsIgnoreCase("Boolean")) {
                        returnType = Boolean.TYPE;
                    } else if (type.equalsIgnoreCase("String")) {
                        returnType = String.class;
                    } else if (type.equalsIgnoreCase("Integer")) {
                        returnType = Integer.TYPE;
                    } else if (type.equalsIgnoreCase("Long")) {
                        returnType = Long.TYPE;
                    }
                }

                addChannel(name, returnType);
            }
        }
    }
    
    @Override
    protected void addExtraChannels() {
        typeByName.put(EXTRA_CHANNEL_HOUR, Integer.TYPE);
        typeByName.put(EXTRA_CHANNEL_MINUTE, Integer.TYPE);
        typeByName.put(EXTRA_CHANNEL_TIMESTAMP, Long.TYPE);
        typeByName.put(EXTRA_CHANNEL_FAULT_COUNT, Integer.TYPE);
    }
    
}
