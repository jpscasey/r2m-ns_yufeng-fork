package com.nexala.spectrum.ns.analysis.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.EventReference;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataSet;
import com.nexala.spectrum.analysis.data.GenericAnalysisProvider;
import com.nexala.spectrum.analysis.data.GenericEventChannelLoader;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.ns.db.dao.LiveAnalysisDataDao;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Event;

/**
 * Analysis provider for Fault-based rules
 * 
 * @author BRedaha
 *
 */
public class NSFaultAnalysisProvider extends GenericAnalysisProvider {
    
    private final static Logger logger = Logger.getLogger(NSFaultAnalysisProvider.class);
    
    @Inject
    private LiveAnalysisDataDao faultChannelDataDao;
    
    @Inject
    private EventDao eventDao;
    
    @Inject
    private ChannelManager cm;
    
    @Inject
    private GenericEventChannelLoader loader;
    
    @Inject
    private MaximoServiceRequestProvider serviceRequestProvider;
    
    @Override
    public Map<Integer, List<EventReference>> getActivatedEvents() {      
        
        Map<Integer, List<EventReference>> result = new HashMap<>();
        List<Event> allLive = eventDao.getAllLive();
        for (Event current : allLive) {
            Integer sourceId = (Integer) current.getField(Spectrum.EVENT_SOURCE_ID);
            
            List<EventReference> lst = result.get(sourceId);
            
            if (lst == null) {
                lst = new ArrayList<>();
                result.put(sourceId, lst);
            }
            
            EventReference event = new EventReference();
            event.setId(((Integer) current.getField(Spectrum.EVENT_ID)).longValue());
            event.setFaultCode((String) current.getField(Spectrum.EVENT_CODE));
            lst.add(event);
        }
        
        return result;
    }

    @Override
    public AnalysisDataMap<GenericEvent> getData(List<Long> lastRecordIds) {     
        List<AnalysisDataSet> liveFaultData = faultChannelDataDao.getLiveData(lastRecordIds.size() == 0 ? null : lastRecordIds.get(0));
        
        // get the last fault row ID
        Long lastLiveFaultId = lastRecordIds.size() == 0 ? null : lastRecordIds.get(0);
        for (AnalysisDataSet ds : liveFaultData) {
            Long rowId = ds.getRowId();
            
            if (lastLiveFaultId == null || rowId > lastLiveFaultId) {
                lastLiveFaultId = rowId;
            }
        }
        
        return new AnalysisDataMap<GenericEvent>(getUnitMap(liveFaultData), lastLiveFaultId);
    }

    @Override
    public Map<Integer, List<GenericEvent>> getData(Integer stockId, long startTime, long endTime) {
        List<AnalysisDataSet> liveFaultData = faultChannelDataDao.getData(stockId, startTime, endTime);
        
        return getUnitMap(liveFaultData);
    }

    @Override
    public long createFault(String faultCode, Long timestamp, Integer sourceId, String ruleName, String contextId, String fleetId, Map<String, String> extraFields) {
    	String trimmedFleetId = fleetId.replace("Fault", "");
    	
        long eventId = super.createFault(faultCode, timestamp, sourceId, ruleName, contextId, trimmedFleetId, extraFields);
        
        if (extraFields.containsKey("createServiceRequest") && extraFields.get("createServiceRequest").equalsIgnoreCase("true")) {
            try {
                serviceRequestProvider.createAutomatedServiceRequest(faultCode, sourceId, eventId, ruleName);
            } catch (Exception e) {
                logger.error("Unable to create automated Service Request", e);
            }
        }
        return eventId;
    }
    
    private Map<Integer, List<GenericEvent>> getUnitMap(List<AnalysisDataSet> faultDataSetList) {
        Map<Integer, List<DataSet>> faultDataMap = getDataSetMap(faultDataSetList);
        
        Set<Integer> ids = new HashSet<Integer>();
        ids.addAll(faultDataMap.keySet());
        
        Map<Integer, List<GenericEvent>> unitMap = new HashMap<Integer, List<GenericEvent>>();
        
        for (Integer id : ids) {
            unitMap.put(id, getMerged(id, faultDataMap.get(id)));
        }
        
        return unitMap;
    }
    
    private Map<Integer, List<DataSet>> getDataSetMap(List<? extends DataSet> dataSetList) {
        Map<Integer, List<DataSet>> dataSetMap = new HashMap<Integer, List<DataSet>>();
        
        if (dataSetList != null) {
            for (DataSet ds : dataSetList) {
                Integer id = ds.getSourceId();

                if (dataSetMap.get(id) == null) {
                    dataSetMap.put(id, new ArrayList<DataSet>());
                }

                dataSetMap.get(id).add(ds);
            }
        }

        return dataSetMap;
    }
    
    private List<GenericEvent> getMerged(Integer sourceId, List<DataSet> liveFaultData) {
        List<GenericEvent> stockList = new ArrayList<GenericEvent>();
        
        SortedSet<Long> timestamps = new TreeSet<Long>();
        
        Map<Long, List<DataSet>> faultDataMap = new HashMap<Long, List<DataSet>>();
        if (liveFaultData != null) {
            for (DataSet ds : liveFaultData) {
            	List<DataSet> faultDataList = faultDataMap.get(ds.getTimestamp());
            	if (faultDataList == null) {
            		faultDataMap.put(ds.getTimestamp(), new ArrayList<DataSet>());
            	}
            	faultDataMap.get(ds.getTimestamp()).add(ds);
                timestamps.add(ds.getTimestamp());
            }
        }
        
        DataSet faultDataSet = null;
        
        for (Long ts : timestamps) {
            
            List<DataSet> dataSetList = faultDataMap.get(ts);
            for (DataSet ds : dataSetList) {
            	GenericEvent event = cm.getEventInstance(ts);
	            if (ds != null) {
	                faultDataSet = ds;
	            }
	            
	            if (faultDataSet != null) {
	                loader.loadDataSet(event, faultDataSet);
	            }
	            
	            stockList.add(event);
            }
            
        }
        
        return stockList;
    }

}
