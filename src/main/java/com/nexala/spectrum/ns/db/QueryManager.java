package com.nexala.spectrum.ns.db;

import java.io.InputStream;

import com.nexala.spectrum.db.BaseQueryManager;
import com.nexala.spectrum.ns.NS;

public class QueryManager extends BaseQueryManager {

    @Override
    public String getKey() {
        return "SPECTRUM-NS";
    }
    
    @Override
    public InputStream getSqlResource() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResourceAsStream(NS.SQL_XML);
    }
}
