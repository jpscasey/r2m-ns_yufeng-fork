package com.nexala.spectrum.ns.db.beans;

public class FirstEventRequestService {
	private Long faultId;
	private Integer faultGroupId;
	private Integer serviceRequestId;
	
	public FirstEventRequestService(Long faultId, Integer faultGroupId, Integer serviceRequestId) {
		this.faultId = faultId;
		this.faultGroupId = faultGroupId;
		this.serviceRequestId = serviceRequestId;
	}

	public Long getFaultId() {
		return faultId;
	}

	public void setFaultId(Long faultId) {
		this.faultId = faultId;
	}

	public Integer getFaultGroupId() {
		return faultGroupId;
	}

	public void setFaultGroupId(Integer faultGroupId) {
		this.faultGroupId = faultGroupId;
	}

	public Integer getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(Integer serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}
}