package com.nexala.spectrum.ns.db.beans;

import java.util.Date;

public class MaximoServiceRequest {

    private int id;
    
    private int faultId;
    
    private int unitId;
    
    private String externalCode;
    
    private String status;
    
    private Date createTime;
    
    private Date lastUpdateTime;
    
    private boolean createdByRule;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFaultId() {
        return faultId;
    }

    public void setFaultId(int faultId) {
        this.faultId = faultId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public boolean isCreatedByRule() {
        return createdByRule;
    }

    public void setCreatedByRule(boolean createdByRule) {
        this.createdByRule = createdByRule;
    }
    
}
