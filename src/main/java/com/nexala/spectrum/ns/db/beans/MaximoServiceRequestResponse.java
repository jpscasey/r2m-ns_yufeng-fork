package com.nexala.spectrum.ns.db.beans;

public class MaximoServiceRequestResponse {
	private String serviceRequestId;
	private boolean maxLimitReached;
	
	private String statusCode;
	private String statusMessageCode;
	private String statusMessageText;
	private String statusMessageDescription;

	public String getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}

	public boolean isMaxLimitReached() {
		return maxLimitReached;
	}

	public void setMaxLimitReached(boolean maxLimitReached) {
		this.maxLimitReached = maxLimitReached;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessageCode() {
		return statusMessageCode;
	}

	public void setStatusMessageCode(String statusMessageCode) {
		this.statusMessageCode = statusMessageCode;
	}

	public String getStatusMessageText() {
		return statusMessageText;
	}

	public void setStatusMessageText(String statusMessageText) {
		this.statusMessageText = statusMessageText;
	}

	public String getStatusMessageDescription() {
		return statusMessageDescription;
	}

	public void setStatusMessageDescription(String statusMessageDescription) {
		this.statusMessageDescription = statusMessageDescription;
	}
}
