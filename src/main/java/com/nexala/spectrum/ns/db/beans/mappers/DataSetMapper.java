package com.nexala.spectrum.ns.db.beans.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.CalculatedChannel;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelCalculationStrategy;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.ChannelCollectionMapper;
import com.nexala.spectrum.db.ChannelCollectionMapperFactory;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;

public class DataSetMapper extends JdbcRowMapper<DataSet> {

    private final BoxedResultSetFactory factory;
    
    private final ChannelCollectionMapper ccMapper;
    
    private final String channelId;

    private final CalculatedChannelConfiguration ccc;
    
    @Inject
    public DataSetMapper(@Assisted ChannelConfiguration channelConf,
            @Assisted CalculatedChannelConfiguration ccc,
            @Assisted String channelId,
            ChannelCollectionMapperFactory ccMapperFactory,
            BoxedResultSetFactory factory) {
        this.channelId = channelId;
        this.ccc = ccc;
        this.ccMapper = ccMapperFactory.create(channelConf);
        this.factory = factory;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public DataSet createRow(ResultSet resultSet) throws SQLException {
        
        BoxedResultSet rs = factory.create(resultSet);
        Long timestamp = null;
        
        int id = -1;
        try {
            id = resultSet.getInt(channelId);
        } catch (Exception ex) {
        }

        try {
            timestamp = rs.getTimestamp(Spectrum.TIMESTAMP);
        } catch (Exception ex) {}

        ChannelCollection channels = ccMapper.createRow(resultSet);

        for (CalculatedChannel c : ccc.getCalculatedChannels()) {
            ChannelConfig channel = c.getChannel();
            ChannelCalculationStrategy ccs = c.getStrategy();
            Channel<?> calculatedValue = ccs.calculate(channel, channels);

            if (calculatedValue != null) {
                channels.put(calculatedValue);
            }
        }

        DataSet data = new DataSetImpl(id, timestamp, channels);
        return data;
    }
}
