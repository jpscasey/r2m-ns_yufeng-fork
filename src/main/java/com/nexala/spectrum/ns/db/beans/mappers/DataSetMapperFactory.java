package com.nexala.spectrum.ns.db.beans.mappers;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;

public interface DataSetMapperFactory {
    DataSetMapper create(@Assisted ChannelConfiguration channelConf,
            @Assisted CalculatedChannelConfiguration ccConf,
            @Assisted String channelId);
}
