package com.nexala.spectrum.ns.db.beans.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.db.JdbcRowMapper;

public class EventChannelMapper extends JdbcRowMapper<Channel<?>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Channel<?> createRow(ResultSet rs) throws SQLException {
        Integer channelId = rs.getInt("ChannelID");
        String channelName = String.valueOf(channelId);
        Channel<?> channel = new ChannelBoolean(channelName,
                rs.getBoolean("Value"), ChannelCategory.NORMAL);
        channel.setId(channelId);
        channel.setColumnName("Col" + channelName);
        return channel;
    }
}
