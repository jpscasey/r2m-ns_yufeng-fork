package com.nexala.spectrum.ns.db.dao;

import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapper;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class ChannelValueDao extends GenericDao<DataSet, Long> {

    private ChannelConfiguration configuration;

    private DataSetMapper rowMapper;

    @Inject
    public ChannelValueDao(ChannelConfiguration conf,
            CalculatedChannelConfiguration cconf,
            DataSetMapperFactory datasetMapperFactory) {
        this.configuration = conf;
        this.rowMapper = datasetMapperFactory.create(conf, cconf, Spectrum.UNIT_ID);
    }

    public List<DataSet> getHistoricData(int unitId, Long timestamp) {
        String query = new QueryManager().getQuery(NS.HISTORIC_DATA_QUERY);
        Date time = null;
        if (timestamp != null) {
            time = new Date(timestamp);
        }

        return findByStoredProcedure(query, rowMapper, time, unitId);
    }
    
    public List<DataSet> getLiveFleetData() {
        String query = new QueryManager().getQuery(NS.FLEET_QUERY);
        return findByStoredProcedure(query, rowMapper);
    }
    
    public List<DataSet> getFleetData(long timestamp) {
        String query = new QueryManager().getQuery(NS.HISTORIC_FLEET_QUERY_KEY);
        return findByStoredProcedure(query, rowMapper, new Date(timestamp));
    }
    
    public List<DataSet> getGroupChannelData(List<Integer> channelIds,
            String unitIds) {
        if (unitIds == null) {
            throw new IllegalArgumentException(
                    "At least one unit id needs to be specified");
        }

        if (channelIds == null || channelIds.size() == 0) {
            throw new IllegalArgumentException(
                    "At least one col name needs to be specified");
        }

        String sep = "";
        StringBuilder sbColumn = new StringBuilder();
        for (Integer channelId : channelIds) {
            sbColumn.append(sep);
            sbColumn.append(configuration.getConfig(channelId).getShortName());
            sep = ", ";
        }

        String query = new QueryManager()
                .getQuery(NS.GROUP_CHANNEL_QUERY);

        return findByStoredProcedure(query, rowMapper, unitIds,
                sbColumn.toString());
    }
}
