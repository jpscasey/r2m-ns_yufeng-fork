package com.nexala.spectrum.ns.db.dao;

import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapper;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class ChannelValueGpsDao extends GenericDao<DataSet, Long> {

    private ChannelConfiguration configuration;

    private DataSetMapper rowMapper;

    @Inject
    public ChannelValueGpsDao(ChannelConfiguration conf,
            CalculatedChannelConfiguration cconf,
            DataSetMapperFactory datasetMapperFactory) {
        this.configuration = conf;
        this.rowMapper = datasetMapperFactory.create(conf, cconf, Spectrum.UNIT_ID);
    }
    
    public List<DataSet> getHistoricData(int unitId, Long timestamp) {
        String query = new QueryManager().getQuery(NS.HISTORIC_GPS_DATA_QUERY);
        Date time = null;
        if (timestamp != null) {
            time = new Date(timestamp);
        }
        
        return findByStoredProcedure(query, rowMapper, time, unitId);
    }
    
    public List<DataSet> getLiveGpsData() {
        String query = new QueryManager().getQuery(NS.CHANNEL_VALUE_GPS_QUERY);
        return findByStoredProcedure(query, rowMapper);
    }
    
    public List<DataSet> getHistoricGpsData(long timestamp) {
        String query = new QueryManager().getQuery(NS.HISTORIC_CHANNEL_VALUE_GPS_QUERY);
        return findByStoredProcedure(query, rowMapper, new Date(timestamp));
    }
}