package com.nexala.spectrum.ns.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.view.conf.maps.Detector;
import com.nexala.spectrum.view.conf.maps.DetectorSiteMarker;

public class DetectorDao extends GenericDao<DetectorSiteMarker, Long> {
    
    private class DetectorMapper extends JdbcRowMapper<DetectorSiteMarker> {
        @Override
        public DetectorSiteMarker createRow(ResultSet rs) throws SQLException {
            
           int id = rs.getInt("ID");
           String siteName = rs.getString("SiteName");
           String tracks = rs.getString("TracksBetween");
           Double latitude = rs.getDouble("Latitude");
           Double longitude = rs.getDouble("Longitude");
           
           Integer detectorId = rs.getInt("TagReaderId");
           String from = rs.getString("From");
           String to = rs.getString("To");
            
           Detector d = new Detector(detectorId, id, from, to);
           List<Detector> detectorList = new ArrayList<Detector>();
           detectorList.add(d);
            
           return new DetectorSiteMarker(id, siteName, tracks, latitude, longitude, detectorList);
        }
    }
    
    public List<DetectorSiteMarker> getDetectorMarkers(){
        List<DetectorSiteMarker> result = new ArrayList<DetectorSiteMarker>();        
               
        String query = new QueryManager().getQuery("detectorQuery");
        result.addAll(findByQuery(query, new DetectorMapper()));
                
        return result;        
    }

}
