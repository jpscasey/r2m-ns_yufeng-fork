package com.nexala.spectrum.ns.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapper;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.ns.db.beans.mappers.EventChannelMapper;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.utils.DateUtils;

public class EventChannelValueDao extends GenericDao<DataSet, Long> {

    private DataSetMapper rowMapper;

    @Inject
    private EventChannelMapper eventChannelValueMapper;

    @Inject
    private DateUtils dateUtils;

    @Inject
    public EventChannelValueDao(ChannelConfiguration conf, CalculatedChannelConfiguration cconf,
            DataSetMapperFactory datasetMapperFactory) {
        this.rowMapper = datasetMapperFactory.create(conf, cconf, Spectrum.UNIT_ID);
    }

    private final JdbcRowMapper<DataSet> eventDataSetMapper = new JdbcRowMapper<DataSet>() {
        @Override
        public DataSet createRow(ResultSet rs) throws SQLException {
            
            int unitId = rs.getInt(Spectrum.UNIT_ID);

            Long timestamp = rs.getTimestamp(Spectrum.TIMESTAMP, dateUtils.getDatabaseCalendar()).getTime();

            Channel<?> channel = eventChannelValueMapper.createRow(rs);
            
            Channel<Long> timestampCh = new ChannelLong(Spectrum.TIMESTAMP,timestamp);
            
            return new DataSetImpl(unitId, timestamp, new ChannelCollection(timestampCh, channel));
        }
    };

    public List<DataSet> getEventChannelDataByUnit(Integer eventId, Integer unitId) {

        //dataSetChannelList will only contain one DataSet, the channel values from the time of the event
        List<DataSet> dataSetChannelList = getDataSet(eventId, unitId);
        //dataSetEventChannelList will contain a list of DataSets, the most recent event channel values in the last 24hrs
        List<DataSet> dataSetEventChannelList = getEventDataSet(eventId, unitId);
        Collections.sort(dataSetEventChannelList, new DataSetTimestampComparator().reversed());
        

        List<DataSet> dataSetGpsChannelList = getGpsDataSet(eventId, unitId);
        Collections.sort(dataSetGpsChannelList, new DataSetTimestampComparator().reversed());

        List<DataSet> allData = new ArrayList<DataSet>();
              
        if(!dataSetChannelList.isEmpty()) {             
            allData.add(dataSetChannelList.get(0));
        } else if(!dataSetEventChannelList.isEmpty()) { 
            allData.add(dataSetEventChannelList.get(0));
        } else if(!dataSetGpsChannelList.isEmpty()) {   
            allData.add(dataSetGpsChannelList.get(0));
        } else {
            return allData;
        }
        
        for(DataSet ds : dataSetEventChannelList) {
            for(Channel<?> c : ds.getChannels()) {
                if(!c.getName().equals(Spectrum.TIMESTAMP)) {
                    allData.get(0).getChannels().put(c);
                } else if(ds.getTimestamp() > allData.get(0).getTimestamp()) {
                    allData.get(0).getChannels().put(c);
                }
            }
        } 
        
        for(DataSet ds : dataSetGpsChannelList) {
            for(Channel<?> c : ds.getChannels()) {
                if(!c.getName().equals(Spectrum.TIMESTAMP)) {
                    allData.get(0).getChannels().put(c);
                } else if(ds.getTimestamp() > allData.get(0).getTimestamp()) {
                    allData.get(0).getChannels().put(c);
                }
            }
        }
        
        return allData;
    }


    private List<DataSet> getDataSet(Integer eventId, Integer unitId) {
        String query = new QueryManager().getQuery(NS.EVENT_CHANNEL_DATA);

        return findByStoredProcedure(query, rowMapper, eventId, unitId);
    }
    
    private List<DataSet> getEventDataSet(Integer eventId, Integer vehicleId) {
        String query = new QueryManager().getQuery(NS.EVENT_FAULT_CHANNEL_DATA);

        return findByStoredProcedure(query, eventDataSetMapper, eventId, vehicleId);
    }
    
    private List<DataSet> getGpsDataSet(Integer eventId, Integer unitId) {
        String query = new QueryManager().getQuery(NS.EVENT_GPS_CHANNEL_DATA);

        return findByStoredProcedure(query, rowMapper, eventId, unitId);
    }

    private void addDataSetToMap(DataSet ds, Map<Long, ChannelCollection> allData) {
        Long ts = ds.getTimestamp();
        if (ts == null)
            return;

        if (!allData.containsKey(ts)) {
            allData.put(ts, new ChannelCollection());
        }
        if (ds.getChannels() != null) {
            allData.get(ts).getChannels()
                    .putAll(ds.getChannels().getChannels());
        }
    }

    class DataSetTimestampComparator implements Comparator<DataSet> {
        @Override
        public int compare(DataSet o1, DataSet o2) {
            return o1.getTimestamp().compareTo(o2.getTimestamp());
        }
    }
}
