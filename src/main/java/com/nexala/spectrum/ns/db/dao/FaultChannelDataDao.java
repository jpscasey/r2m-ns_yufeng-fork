package com.nexala.spectrum.ns.db.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.GenericEventChannelLoader;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapper;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class FaultChannelDataDao extends GenericDao<DataSet, Long> {

    private DataSetMapper rowMapper;

    @Inject
    private GenericEventChannelLoader loader;

    @Inject
    private ChannelManager cm;

    @Inject
    public FaultChannelDataDao(ChannelConfiguration conf, CalculatedChannelConfiguration cconf,
            DataSetMapperFactory datasetMapperFactory) {
        this.rowMapper = datasetMapperFactory.create(conf, cconf, Spectrum.UNIT_ID);
    }
    
    public Map<Integer, List<GenericEvent>> getData(Integer unitId, long timeFrom, long timeTo) {
        String query = new QueryManager().getQuery(NS.FAULT_CHANNEL_DATA_UNIT);

        List<DataSet> data = findData(query, unitId, timeFrom, timeTo);

        return getUnitMap(data);
    }
    
    private List<DataSet> findData(String query, Integer unitId, long timeFrom, long timeTo) {
        if (unitId == null) {
            return findByStoredProcedure(query, rowMapper, new Date(timeFrom), new Date(timeTo));
        }

        return findByStoredProcedure(query, rowMapper, new Date(timeFrom), new Date(timeTo), unitId);
    }

    private Map<Integer, List<GenericEvent>> getUnitMap(List<DataSet> data) {

        Map<Integer, List<DataSet>> dataSetIdMap = getDataSetMap(data);

        Set<Integer> ids = new HashSet<Integer>();
        ids.addAll(dataSetIdMap.keySet());

        Map<Integer, List<GenericEvent>> unitMap = new HashMap<Integer, List<GenericEvent>>();

        for (Integer id : ids) {
            unitMap.put(id, getMerged(dataSetIdMap.get(id)));
        }

        return unitMap;
    }

    private Map<Integer, List<DataSet>> getDataSetMap(List<DataSet> dataSetList) {

        Map<Integer, List<DataSet>> dataSetMap = new HashMap<Integer, List<DataSet>>();

        if (dataSetList != null) {
            for (DataSet ds : dataSetList) {
                Integer id = ds.getSourceId();

                if (dataSetMap.get(id) == null) {
                    dataSetMap.put(id, new ArrayList<DataSet>());
                }

                dataSetMap.get(id).add(ds);
            }
        }

        return dataSetMap;
    }

    private List<GenericEvent> getMerged(List<DataSet> data) {

        List<GenericEvent> unitList = new ArrayList<GenericEvent>();

        SortedSet<Long> timestamps = new TreeSet<Long>();

        Map<Long, DataSet> dataSetMap = new HashMap<Long, DataSet>();

        if (data != null) {
            for (DataSet v : data) {
                dataSetMap.put(v.getTimestamp(), v);
                timestamps.add(v.getTimestamp());
            }
        }

        DataSet dataSet = dataSetMap.get(timestamps.first());

        for (Long ts : timestamps) {
            GenericEvent unit = cm.getEventInstance(ts);

            DataSet ds = dataSetMap.get(ts);
            if (ds != null) {
                dataSet = ds;
            }

            loader.loadDataSet(unit, dataSet);

            unitList.add(unit);
        }

        return unitList;
    }
}
