package com.nexala.spectrum.ns.db.dao;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapper;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.rest.data.beans.DataSet;


public class HistoricChannelValueDao extends GenericDao<DataSet, Long> {

    private DataSetMapper rowMapper;

    @Inject
    public HistoricChannelValueDao(
            ChannelConfiguration conf,
            CalculatedChannelConfiguration cconf,
            DataSetMapperFactory datasetMapperFactory) {
        rowMapper = datasetMapperFactory.create(conf, cconf, Spectrum.UNIT_ID);
    }
    
    private class DataSetTimestampComparator implements Comparator<DataSet> {
        @Override
        public int compare(DataSet o1, DataSet o2) {
            return o1.getTimestamp().compareTo(o2.getTimestamp());
        }
    }

    public List<DataSet> find(int unitId, Long dateTo, int interval, Integer endRow, List<ChannelConfig> channels) {

        StringBuilder list = new StringBuilder();

        list.append("cv.UnitID");

        for (ChannelConfig config : channels) {
            if (!config.getHardwareType().equals(NS.HARDWARE_TYPE_EVENT) && 
                    !config.getHardwareType().equals(NS.HARDWARE_TYPE_VT)) {
                list.append(",");
                list.append(config.getShortName());
            }
        }

        String query = new QueryManager().getQuery(NS.HISTORIC_CHANNEL_DATA);

        List<DataSet> units = this.findByStoredProcedure(query, rowMapper,
                    (dateTo != null ? new Date(dateTo) : null), interval,
                    unitId, list.toString(), endRow);
        
        Collections.sort(units, new DataSetTimestampComparator());

        return units;
    }
}
