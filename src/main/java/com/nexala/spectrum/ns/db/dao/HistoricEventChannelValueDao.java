package com.nexala.spectrum.ns.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.EventChannelMapper;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.utils.DateUtils;

public class HistoricEventChannelValueDao extends GenericDao<DataSet, Long> {
	
	
	@Inject
    private DateUtils dateUtils;

	
	@Inject
    private EventChannelMapper eventChannelValueMapper;
	
	private final JdbcRowMapper<DataSet> eventDataSetMapper = new JdbcRowMapper<DataSet>() {
        @Override
        public DataSet createRow(ResultSet rs) throws SQLException {
            
            int unitId = rs.getInt(Spectrum.UNIT_ID);

            Long timestamp = rs.getTimestamp(Spectrum.TIMESTAMP, dateUtils.getDatabaseCalendar()).getTime();

            Channel<?> channel = eventChannelValueMapper.createRow(rs);
            
            Channel<Long> timestampCh = new ChannelLong(Spectrum.TIMESTAMP,timestamp);
            
            return new DataSetImpl(unitId, timestamp, new ChannelCollection(timestampCh, channel));
        }
    };

    public List<DataSet> getHistoricEventData(int unitId, Long timestamp) {
        String query = new QueryManager().getQuery(NS.HISTORIC_EVENT_DATA_QUERY);

        Date time = null;
        if (timestamp != null) {
            time = new Date(timestamp);
        }

        return findByStoredProcedure(query, eventDataSetMapper, time, unitId);
    }

    public List<DataSet> find(int unitId, Long dateTo, int interval, List<ChannelConfig> channels) {

        StringBuilder list = new StringBuilder();

        list.append("1");

        for (ChannelConfig config : channels) {
            if (config.getHardwareType().equals(NS.HARDWARE_TYPE_EVENT)) {
                list.append(",");
                list.append(config.getId());
            }
        }

        String query = new QueryManager().getQuery(NS.HISTORIC_EVENT_CHANNEL_DATA);

        List<DataSet> result = findByStoredProcedure(query, eventDataSetMapper,
                (dateTo != null ? new Date(dateTo) : null), interval, unitId, list.toString());

        return result;
    }

}
