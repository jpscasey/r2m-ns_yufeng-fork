package com.nexala.spectrum.ns.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;
import com.nexala.spectrum.ns.db.beans.mappers.EventChannelMapper;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.utils.DateUtils;

public class HistoricEventDataSetDao extends GenericDao<DataSet, Long> {

    @Inject
    private EventChannelMapper eventChannelValueMapper;

    @Inject
    private DateUtils dateUtils;
    
    private final JdbcRowMapper<DataSet> eventDataSetMapper = new JdbcRowMapper<DataSet>() {
        @Override
        public DataSet createRow(ResultSet rs) throws SQLException {
            
            int unitId = rs.getInt(Spectrum.UNIT_ID);

            Long timestamp = rs.getTimestamp(Spectrum.TIMESTAMP, dateUtils.getDatabaseCalendar()).getTime();

            Channel<?> channel = eventChannelValueMapper.createRow(rs);
            
            Channel<Long> timestampCh = new ChannelLong(Spectrum.TIMESTAMP,timestamp);
            
            return new DataSetImpl(unitId, timestamp, new ChannelCollection(timestampCh, channel));
        }
    };
    
    public List<DataSet> find(int unitId, Long dateTo, int interval, List<ChannelConfig> channels) {

        List<DataSet> dataSets = new ArrayList<DataSet>();
        String query = new QueryManager().getQuery(NS.HISTORIC_EVENT_CHANNEL_DATA);

        Date time = null;
        if (dateTo != null) {
            time = new Date(dateTo);
        }
        
        StringBuilder channelList = buildChanList(channels);

        if (channelList.length() > 0) {

            channelList.deleteCharAt(0);

            dataSets = this.findByStoredProcedure(query, getRowMapper(), time,
                    interval, unitId, channelList.toString());
        }

        return dataSets;
    }

    public StringBuilder buildChanList(List<ChannelConfig> channels) {
        StringBuilder sb = new StringBuilder();
        for (ChannelConfig channel : channels) {
            if (channel.getHardwareType().equals(NS.HARDWARE_TYPE_EVENT)) {
                sb.append(",");
                sb.append(channel.getId());
            }
        }
        return sb;
    }
    
    @Override
    public RowMapper<DataSet> getRowMapper() {
        return eventDataSetMapper;
    }
}
