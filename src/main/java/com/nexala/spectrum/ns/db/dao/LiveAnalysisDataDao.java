/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.ns.db.dao;

import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.data.AnalysisDataSet;
import com.nexala.spectrum.analysis.data.GenericEventChannelLoader;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapper;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapperFactory;

// TODO this looks generic enough to be a spectrum-base default implementation doesn't it?
public class LiveAnalysisDataDao extends GenericDao<AnalysisDataSet, Long> {
    
    private AnalysisDataSetMapper aRowMapper = null;
    private final ApplicationConfiguration appConf;
    private final String fleetId;
    private final String keyChannel;
        
    @Inject
    public LiveAnalysisDataDao(
            @Named(Spectrum.FLEET_ID) String fleetId,
            ApplicationConfiguration appConf,
            ChannelConfiguration conf,
            CalculatedChannelConfiguration cconf,
            AnalysisDataSetMapperFactory factory,
            GenericEventChannelLoader loader) {
        this.appConf = appConf;
        this.fleetId = fleetId;
        this.keyChannel = appConf.get().getString("r2m."+fleetId+".stream-analysis.keyChannel");
        this.aRowMapper = factory.create(conf, cconf, keyChannel);
    }
    
    public List<AnalysisDataSet> getLiveData(Long lastRecordId) {
        String liveChannelDataQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataSp");
        if(fleetId == "SLT" || fleetId == "VIRM" || fleetId == "SNG") {
        	Integer ruleEngineDelay = appConf.get().getInt("r2m."+fleetId+".stream-analysis.ruleEngineDelay");
        	return findByStoredProcedureWithTimeout(liveChannelDataQuery, aRowMapper, 60, lastRecordId, ruleEngineDelay);
        }
        else {
        	return findByStoredProcedureWithTimeout(liveChannelDataQuery, aRowMapper, 60, lastRecordId);
        }
    }
    
    public List<AnalysisDataSet> getEnrichmentData(Long lastRecordId) {
        String enrichmentChannelDataQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataLatestSinceSp");
        Integer maxTimestampDifferenceMinutes = appConf.get().getInt("r2m."+fleetId+".stream-analysis.maxChannelTimeDifferenceMinutes");
        
        return findByStoredProcedureWithTimeout(enrichmentChannelDataQuery, aRowMapper, 60, lastRecordId, maxTimestampDifferenceMinutes);
    }
    
    public List<AnalysisDataSet> getData(Integer stockId, long startTime, long endTime) {
        String ruleTesterQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataTesterSp");
        
        return findByStoredProcedureWithTimeout(ruleTesterQuery, aRowMapper, 60, new Date(startTime), new Date(endTime), stockId);
    }
    
    public List<AnalysisDataSet> getEnrichmentData(Integer stockId, long startTime) {
        String ruleTesterQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataTesterLatestSinceSp");
        Integer maxTimestampDifferenceMinutes = appConf.get().getInt("r2m."+fleetId+".stream-analysis.maxChannelTimeDifferenceMinutes");
        
        return findByStoredProcedureWithTimeout(ruleTesterQuery, aRowMapper, 60, new Date(startTime), stockId, maxTimestampDifferenceMinutes);
    }
    
}
