package com.nexala.spectrum.ns.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.beans.FirstEventRequestService;
import com.nexala.spectrum.ns.db.beans.MaximoServiceRequest;

public class MaximoServiceRequestDao extends GenericDao<MaximoServiceRequest, Integer> {

    @Inject
    private ApplicationConfiguration appConf;
    
    public void saveServiceRequest(Long eventId, String serviceRequestId, boolean createdByRules) {
        executeStoredProcedure(appConf.getQuery(NS.SAVE_SERVICE_REQUEST), eventId, serviceRequestId, createdByRules);
    }
    
    public void addServiceRequestToFault(Long faultId, String serviceRequestCode) {
    	String query = appConf.getQuery(NS.ADD_SERVICE_REQUEST_TO_FAULT);
    	executeUpdateByQuery(query, serviceRequestCode, Integer.valueOf(faultId.toString()));
	}

	public void updateFaultServiceRequest(Long eventId, Integer serviceRequestId) {
		executeUpdateByQuery(appConf.getQuery(NS.UPDATE_FAULT_SERVICE_REQUEST), serviceRequestId, eventId);
	}
	
	public void addFaultToGroup(Integer groupId, Long eventId) {
		executeUpdateByQuery(appConf.getQuery(NS.ADD_FAULT_TO_GROUP), groupId, false, eventId);
	}
	
	public void createServiceRequestFaultGroup(Long firstEventId, Long recentEventId, String username) {
		Connection c = null;
		PreparedStatement ps = null;

		try {
			c = this.getConnection();

			// transaction start
			c.setAutoCommit(false);

			// create new fault group
			
			Long newFaultGroupId = null;

			ps = c.prepareStatement(appConf.getQuery(NS.CREATE_RULESENGINE_GROUP), Statement.RETURN_GENERATED_KEYS);
			this.addParameterToPreparedStatement(ps, 1, username);

			Integer rowsUpdated = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rowsUpdated.equals(1) && rs.next()) {
				newFaultGroupId = rs.getLong(1);
			} else {
				throw new SQLException("Creating Fault Group failed, no ID obtained.");
			}
			
			// if the fault group has successfully been created add the first 2 events
			
			if (newFaultGroupId != null) {
				// add the first event to the group and make it a group lead
                ps = c.prepareStatement(appConf.getQuery(NS.ADD_FAULT_TO_GROUP));
				this.addParameterToPreparedStatement(ps, 1, newFaultGroupId);
                this.addParameterToPreparedStatement(ps, 2, true);
                this.addParameterToPreparedStatement(ps, 3, firstEventId);
                
                ps.executeUpdate();
                
                // then add the recent event to the fault group
                ps = c.prepareStatement(appConf.getQuery(NS.ADD_FAULT_TO_GROUP));
                this.addParameterToPreparedStatement(ps, 1, newFaultGroupId);
                this.addParameterToPreparedStatement(ps, 2, false);
                this.addParameterToPreparedStatement(ps, 3, recentEventId);
                
                ps.executeUpdate();
			}
			
			// transaction ok
			c.commit();
		} catch (SQLException e) {
			if (c != null) {
				try {
					c.rollback();
				} catch (SQLException ex1) {
					logger.error("Error rolling back database changes", ex1);
				}
			}
			throw new DataAccessException(e);
		} finally {
			DbUtil.closeStatement(ps);
			DbUtil.closeConnection(c);
		}
	}
	
    public List<FirstEventRequestService> findEventWithServiceRequest(Object... parameters) throws DataAccessException {
    	String query = appConf.getQuery(NS.FIND_EVENT_WITH_SERVICE_REQUEST);
    	
        ArrayList<FirstEventRequestService> results = new ArrayList<FirstEventRequestService>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = this.getConnection();
            logger.debug(String.format("Executing query: %s", query.replaceAll("[\\n\\s]+", " ")));
            ps = c.prepareStatement(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(ps, i++, parameter);
            }
            rs = ps.executeQuery();
            
            while(rs.next()) {
            	long faultId = rs.getLong("FaultID");
            	int faultGroupId = rs.getInt("FaultGroupID");
            	int serviceRequestId = rs.getInt("ServiceRequestID");
            	
            	results.add(new FirstEventRequestService(faultId, faultGroupId, serviceRequestId));
            }
            
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
        logger.debug(String.format("%d rows returned", results.size()));
        return results;
    }
    
}
