package com.nexala.spectrum.ns.db.dao;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.nexala.spectrum.db.dao.TimestampDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;

public class TimestampDaoNS extends TimestampDao {

        public Long getLastUnitTimestamp(int unitId, Long timestamp) {
            Long newerTimestamp = null;
            
            String query = new QueryManager().getQuery(NS.UNIT_LATEST_TIMESTAMP);
            List<Long> latestTimestamps = getTimestampByQuery(query, unitId, timestamp);
            Iterables.removeIf(latestTimestamps, Predicates.isNull());
            
            if (latestTimestamps.size() > 0) {
                newerTimestamp = Collections.max(latestTimestamps);
            }
            
            return newerTimestamp;
        } 

        public Long getLastUnitTimestampSlt(int unitId, Long timestamp) {
            Long newerTimestamp = null;
            
            String query = new QueryManager().getQuery(NS.UNIT_LATEST_TIMESTAMP_SLT);
            List<Long> latestTimestamps = getTimestampByQuery(query, unitId, timestamp);
            Iterables.removeIf(latestTimestamps, Predicates.isNull());
            
            if (latestTimestamps.size() > 0) {
                newerTimestamp = Collections.max(latestTimestamps);
            }
            
            return newerTimestamp;
        } 
}
