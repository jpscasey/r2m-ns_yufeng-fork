package com.nexala.spectrum.ns.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.UnitSearchResult;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.QueryManager;

public class UnitSearchDao extends GenericDao<UnitSearchResult, Long> {
    private class UnitSearchMapper extends JdbcRowMapper<UnitSearchResult> {
        @Override
        public UnitSearchResult createRow(ResultSet rs) throws SQLException {
            UnitSearchResult unit = new UnitSearchResult(rs.getInt("UnitId"),
                    rs.getString("UnitNumber"),
                    rs.getString("UnitType"),
                    rs.getString("Headcode"));

            return unit;
        }
    };    

    public List<UnitSearchResult> getFleetUnits(String fleetId, String substr)
            throws DataAccessException {
        String query = new QueryManager().getQuery(NS.SEARCH_FLEET_UNITS_QUERY);

        return findByQuery(query, new UnitSearchMapper(), fleetId, "%" + substr
                + "%", "%" + substr + "%");
    }
}
