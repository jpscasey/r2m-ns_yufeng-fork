package com.nexala.spectrum.ns.modules;

import com.nexala.spectrum.licensing.LicenceValidatorProvider;
import com.nexala.spectrum.modules.GlobalConfigurationModule;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.LicenceValidatorProviderNS;
import com.nexala.spectrum.ns.providers.MapMarkerProviderNS;
import com.nexala.spectrum.ns.providers.NSCommonEventProvider;
import com.nexala.spectrum.rest.data.CommonEventProvider;
import com.nexala.spectrum.view.conf.EventDetailConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerProvider;
import com.nexala.spectrum.view.templates.TemplateConfiguration;

public class NSModule extends GlobalConfigurationModule {

//    @Override
//    protected Class<? extends MapConfiguration> supplyMapConfiguration() {
//        return MapConfiguration.class;
//    }

    // TODO This could have a default implementation in spectrum-base
    @Override
    protected Class<? extends LicenceValidatorProvider> supplyLicenceValidatorProvider() {
        return LicenceValidatorProviderNS.class;
    }

    // TODO This could have a default implementation in spectrum-base
    @Override
    protected Class<? extends TemplateConfiguration> supplyTemplateConfiguration() {
        return TemplateConfiguration.class;
    }

    @Override
    protected Class<? extends CommonEventProvider> supplyCommonEventProvider() {
        return NSCommonEventProvider.class;
    }

    @Override
    protected Class<? extends EventDetailConfiguration> supplyEventDetailConfiguration() {

        return EventDetailConfiguration.class;
    }

    @Override
    protected String getGlobalDatasource() {
        return NS.NS_DS;
    }
    
    @Override
    protected Class<? extends MapMarkerProvider> supplyMapMarkerProvider() {
        return MapMarkerProviderNS.class;
    }
}
