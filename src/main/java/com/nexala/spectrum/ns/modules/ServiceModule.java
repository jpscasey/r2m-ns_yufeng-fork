package com.nexala.spectrum.ns.modules;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataProvider;
import com.nexala.spectrum.eventanalysis.rest.EAnalysisProvider;
import com.nexala.spectrum.modules.FleetProvidersMapModule;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.ns.providers.slt.FleetProviderSlt;
import com.nexala.spectrum.ns.providers.slt.Slt;
import com.nexala.spectrum.ns.providers.slt.SltFault;
import com.nexala.spectrum.ns.providers.slt.UnitProviderSlt;
import com.nexala.spectrum.ns.providers.sng.FleetProviderSng;
import com.nexala.spectrum.ns.providers.sng.Sng;
import com.nexala.spectrum.ns.providers.sng.SngFault;
import com.nexala.spectrum.ns.providers.sng.UnitProviderSng;
import com.nexala.spectrum.ns.providers.virm.FleetProviderVirm;
import com.nexala.spectrum.ns.providers.virm.UnitProviderVirm;
import com.nexala.spectrum.ns.providers.virm.Virm;
import com.nexala.spectrum.ns.providers.virm.VirmFault;
import com.nexala.spectrum.rest.data.AdhesionProvider;
import com.nexala.spectrum.rest.data.CabDataProvider;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChannelDefinitionProvider;
import com.nexala.spectrum.rest.data.ChartDataProvider;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;
import com.nexala.spectrum.rest.data.DetectorProvider;
import com.nexala.spectrum.rest.data.DownloadProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.FaultMetaProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.GroupProvider;
import com.nexala.spectrum.rest.data.LocationProvider;
import com.nexala.spectrum.rest.data.MapProvider;
import com.nexala.spectrum.rest.data.RecoveryProvider;
import com.nexala.spectrum.rest.data.StaticChartProvider;
import com.nexala.spectrum.rest.data.StockProvider;
import com.nexala.spectrum.rest.data.SystemConfigProvider;
import com.nexala.spectrum.rest.data.UnitDetailProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.service.CabConfigurationProvider;
import com.nexala.spectrum.view.conf.UnitConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;
import com.nexala.spectrum.web.service.RuleTestDataProvider;

public class ServiceModule extends FleetProvidersMapModule {

    @Override
    protected void configure() {
        super.configure();
        bindMap(new TypeLiteral<MaximoServiceRequestProvider>() {}, supplyMaximoServiceRequestProviders());
    }
    
    @Override
    protected Map<String, Key<? extends UnitConfiguration>> supplyUnitConfigurations() {
        Map<String, Key<? extends UnitConfiguration>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<UnitConfiguration>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<UnitConfiguration>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<UnitConfiguration>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends UnitDetailConfiguration>> supplyUnitDetailConfigurations() {
        Map<String, Key<? extends UnitDetailConfiguration>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<UnitDetailConfiguration>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<UnitDetailConfiguration>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<UnitDetailConfiguration>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends CabConfigurationProvider>> supplyCabConfigurationProviders() {
        Map<String, Key<? extends CabConfigurationProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<CabConfigurationProvider>(){}, Slt.class));
        //providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<CabConfigurationProvider>(){}, Virm.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends CabDataProvider>> supplyCabDataProviders() {
        Map<String, Key<? extends CabDataProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<CabDataProvider>(){}, Slt.class));
        //providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<CabDataProvider>(){}, Virm.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends ChannelDataProvider>> supplyChannelDataProviders() {
        Map<String, Key<? extends ChannelDataProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<ChannelDataProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<ChannelDataProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<ChannelDataProvider>(){}, Sng.class));
        return providerMap;
    }

    protected Map<String, Key<? extends GpsDataProvider>> supplyGpsDataProviders() {
        Map<String, Key<? extends GpsDataProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<GpsDataProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<GpsDataProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<GpsDataProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends ChannelDefinitionProvider>> supplyChannelDefinitionProviders() {
        Map<String, Key<? extends ChannelDefinitionProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<ChannelDefinitionProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<ChannelDefinitionProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<ChannelDefinitionProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends ChartDataProvider>> supplyChartDataProviders() {
        Map<String, Key<? extends ChartDataProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<ChartDataProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<ChartDataProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<ChartDataProvider>(){}, Sng.class));
        return providerMap;
    }
    
    @Override
    protected Map<String, Key<? extends StaticChartProvider>> supplyStaticChartProviders() {
        Map<String, Key<? extends StaticChartProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<StaticChartProvider>(){}, Virm.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends DownloadProvider>> supplyDownloadProviders() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<String, Key<? extends EventProvider>> supplyEventProviders() {
        Map<String, Key<? extends EventProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<EventProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<EventProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<EventProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends EAnalysisProvider>> supplyEAnalysisProviders() {
        Map<String, Key<? extends EAnalysisProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<EAnalysisProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<EAnalysisProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<EAnalysisProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends FleetProvider>> supplyFleetProviders() {
        return getMapBinding(TypeLiteral.get(FleetProvider.class), 
                FleetProviderSlt.class, 
                null, 
                FleetProviderVirm.class,
                null,
                FleetProviderSng.class,
                null);
    }

    @Override
    protected Map<String, Key<? extends GroupProvider>> supplyGroupProviders() {
        Map<String, Key<? extends GroupProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<GroupProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<GroupProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<GroupProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends LocationProvider>> supplyLocationProviders() {
        Map<String, Key<? extends LocationProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<LocationProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<LocationProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<LocationProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends MapProvider>> supplyMapProviders() {
        Map<String, Key<? extends MapProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<MapProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<MapProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<MapProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends RuleTestDataProvider<GenericEvent>>> supplyRuleTestDataProviders() {
        Map<String, Key<? extends RuleTestDataProvider<GenericEvent>>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, Slt.class));
        providerMap.put(NS.FLEET_SLT_FAULT, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, SltFault.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, Virm.class));
        providerMap.put(NS.FLEET_VIRM_FAULT, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, VirmFault.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, Sng.class));
        providerMap.put(NS.FLEET_SNG_FAULT, Key.get(new TypeLiteral<RuleTestDataProvider<GenericEvent>>(){}, SngFault.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends StockProvider>> supplyStockProviders() {
        Map<String, Key<? extends StockProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<StockProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<StockProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<StockProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends UnitDetailProvider>> supplyUnitDetailProviders() {
        Map<String, Key<? extends UnitDetailProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<UnitDetailProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<UnitDetailProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<UnitDetailProvider>(){}, Sng.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends UnitProvider>> supplyUnitProviders() {
        return getMapBinding(TypeLiteral.get(UnitProvider.class), 
                UnitProviderSlt.class, 
                null,
                UnitProviderVirm.class,
                null,
                UnitProviderSng.class,
                null);
    }

    @Override
    protected Map<String, Key<? extends AnalysisDataProvider<GenericEvent>>> supplyAnalysisDataProviders() {
        Map<String, Key<? extends AnalysisDataProvider<GenericEvent>>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT,
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, Slt.class));
        
        // Fault Rules analysis provider, using 'ghost fleet' SLTFault
        providerMap.put(NS.FLEET_SLT_FAULT, 
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, SltFault.class));
        
        providerMap.put(NS.FLEET_VIRM,
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, Virm.class));
        
        // Fault Rules analysis provider, using 'ghost fleet' VIRMFault
        providerMap.put(NS.FLEET_VIRM_FAULT, 
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, VirmFault.class));
        
        providerMap.put(NS.FLEET_SNG,
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, Sng.class));
        
        // Fault Rules analysis provider, using 'ghost fleet' SNGFault
        providerMap.put(NS.FLEET_SNG_FAULT, 
                Key.get(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {
                }, SngFault.class));
        
        return providerMap;
    }
    
	protected Map<String, Key<? extends RecoveryProvider>> supplyRecoveryProviders() {
	    Map<String, Key<? extends RecoveryProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<RecoveryProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<RecoveryProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<RecoveryProvider>(){}, Sng.class));
        return providerMap;
	}
    
    private Map<String, Key<? extends MaximoServiceRequestProvider>> supplyMaximoServiceRequestProviders() {
        Map<String, Key<? extends MaximoServiceRequestProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<MaximoServiceRequestProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<MaximoServiceRequestProvider>(){}, Virm.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<MaximoServiceRequestProvider>(){}, Sng.class));
        return providerMap;
    }

    private static <T> Map<String, Key<? extends T>> getMapBinding(TypeLiteral<T> iface, Class<? extends T> sltImpl,
            Class<? extends T> sltFaultImpl, Class<? extends T> virmImpl, Class<? extends T> virmFaultImpl,
            Class<? extends T> sngImpl, Class<? extends T> sngFaultImpl) {
        
        Map<String, Key<? extends T>> result = new LinkedHashMap<String, Key<? extends T>>();

        if (sltImpl != null) {
            result.put(NS.FLEET_SLT, Key.get(sltImpl));
        }
        
        if (sltFaultImpl != null) {
            result.put(NS.FLEET_SLT_FAULT,  Key.get(sltFaultImpl));
        }
        
        if (virmImpl != null) {
            result.put(NS.FLEET_VIRM,  Key.get(virmImpl));
        }
        
        if (virmFaultImpl != null) {
            result.put(NS.FLEET_VIRM_FAULT,  Key.get(virmFaultImpl));
        }
        
        if (sngImpl != null) {
            result.put(NS.FLEET_SNG,  Key.get(sngImpl));
        }
        
        if (sngFaultImpl != null) {
            result.put(NS.FLEET_SNG_FAULT,  Key.get(sngFaultImpl));
        }

        return result;
    }

    @Override
    protected Map<String, Key<? extends AdhesionProvider>> supplyAdhesionProviders() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<String, Key<? extends ChartVehicleTypeProvider>> supplyChartVehicleTypeProviders() {
        Map<String, Key<? extends ChartVehicleTypeProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<ChartVehicleTypeProvider>(){}, Virm.class));
        return providerMap;
    }

    @Override
    protected Map<String, Key<? extends FaultMetaProvider>> supplyFaultMetaProviders() {
        Map<String, Key<? extends FaultMetaProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<FaultMetaProvider>(){}, Slt.class));
        providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<FaultMetaProvider>(){}, Sng.class));
        providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<FaultMetaProvider>(){}, Virm.class));
        return providerMap;
    }

    
    protected Map<String, Key<? extends DetectorProvider>> supplyDetectorProviders() {
        Map<String, Key<? extends DetectorProvider>> providerMap = new HashMap<>();
        providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<DetectorProvider>(){}, Slt.class));
        return providerMap;
    }
    
    @Override
    protected Map<String, Key<? extends SystemConfigProvider>> supplySystemConfigProviders() {
    	Map<String, Key<? extends SystemConfigProvider>> providerMap = new HashMap<>();
    	providerMap.put(NS.FLEET_SLT, Key.get(new TypeLiteral<SystemConfigProvider>() {}, Slt.class));
    	providerMap.put(NS.FLEET_SNG, Key.get(new TypeLiteral<SystemConfigProvider>() {}, Sng.class));
    	providerMap.put(NS.FLEET_VIRM, Key.get(new TypeLiteral<SystemConfigProvider>() {}, Virm.class));
    	return providerMap;
    }
}
