package com.nexala.spectrum.ns.modules.fleets;

import javax.inject.Singleton;

import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataProvider;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.db.ChannelCollectionMapperFactory;
import com.nexala.spectrum.db.dao.ConnectionProvider;
import com.nexala.spectrum.db.dao.JndiConnectionProvider;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapperFactory;
import com.nexala.spectrum.modules.AbstractFleetModule;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.analysis.beans.ChannelManagerFaultRuleImpl;
import com.nexala.spectrum.ns.analysis.data.NSFaultAnalysisProvider;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProviderNS;
import com.nexala.spectrum.ns.providers.RuleTestDataProviderNS;
import com.nexala.spectrum.ns.providers.slt.SltFault;
import com.nexala.spectrum.web.service.RuleTestDataProvider;


/**
 * Guice module containing bindings for 'ghost fleet' SLTFault, used for the rule-over-rules instance of the rules engine
 * 
 * @author BRedaha
 *
 */
public class FleetSltFaultModule extends AbstractFleetModule {

    public FleetSltFaultModule() {
        super(NS.FLEET_SLT_FAULT, SltFault.class);
    }
    
    @Override
    public void configure() {
        super.configure();
        bind(String.class).annotatedWith(Names.named("dataSource")).toInstance(NS.NS_DS);
        bind(ConnectionProvider.class).to(JndiConnectionProvider.class).in(Singleton.class);
        
        // Factories
        install(new FactoryModuleBuilder().build(ChannelCollectionMapperFactory.class));
        install(new FactoryModuleBuilder().build(DataSetMapperFactory.class));
        install(new FactoryModuleBuilder().build(AnalysisDataSetMapperFactory.class));
        
        Key<AnalysisDataProvider<GenericEvent>> analysisDataProvider = Key.get(
                new TypeLiteral<AnalysisDataProvider<GenericEvent>>(){}, SltFault.class);
        bind(analysisDataProvider).to(NSFaultAnalysisProvider.class);
        expose(analysisDataProvider);
        
        // Mappers
        bind(ChannelManager.class).to(ChannelManagerFaultRuleImpl.class);
        
        // Multi Fleet Providers
        bind(MaximoServiceRequestProvider.class).to(MaximoServiceRequestProviderNS.class);
        bindAndExposeWithAnnotation(new TypeLiteral<RuleTestDataProvider<GenericEvent>>() {}, RuleTestDataProviderNS.class);
    }
    
    private <T> void bindAndExposeWithAnnotation(TypeLiteral<T> interfaceClass, Class<? extends T> implemetation) {
        Key<T> eAProvider = Key.get(interfaceClass, SltFault.class);
        bind(interfaceClass).to(implemetation);
        bind(eAProvider).to(implemetation);
        bind(implemetation);
        
        expose(eAProvider);
        
    }
}
