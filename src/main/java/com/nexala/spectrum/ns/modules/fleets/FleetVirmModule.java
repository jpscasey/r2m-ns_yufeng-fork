package com.nexala.spectrum.ns.modules.fleets;

import com.google.inject.Key;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.nexala.spectrum.analysis.bean.ChannelManagerImpl;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataProvider;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.charting.StaticChartFactory;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.db.ChannelCollectionMapperFactory;
import com.nexala.spectrum.db.dao.ConnectionProvider;
import com.nexala.spectrum.db.dao.JndiConnectionProvider;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapperFactory;
import com.nexala.spectrum.eventanalysis.rest.DefaultEAnalysisProvider;
import com.nexala.spectrum.eventanalysis.rest.EAnalysisProvider;
import com.nexala.spectrum.modules.AbstractFleetModule;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.beans.mappers.DataSetMapperFactory;
import com.nexala.spectrum.ns.providers.AnalysisProviderNS;
import com.nexala.spectrum.ns.providers.ChartDataProviderNS;
import com.nexala.spectrum.ns.providers.EventProviderNS;
import com.nexala.spectrum.ns.providers.GpsDataProviderNS;
import com.nexala.spectrum.ns.providers.GroupProviderNS;
import com.nexala.spectrum.ns.providers.LocationProviderNS;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProviderNS;
import com.nexala.spectrum.ns.providers.RuleTestDataProviderNS;
import com.nexala.spectrum.ns.providers.virm.ChannelDataProviderVirm;
import com.nexala.spectrum.ns.providers.virm.ChartVehicleTypeProviderVirm;
import com.nexala.spectrum.ns.providers.virm.FleetProviderVirm;
import com.nexala.spectrum.ns.providers.virm.MapProviderVirm;
import com.nexala.spectrum.ns.providers.virm.UnitDetailProviderVirm;
import com.nexala.spectrum.ns.providers.virm.UnitProviderVirm;
import com.nexala.spectrum.ns.providers.virm.Virm;
import com.nexala.spectrum.ns.view.conf.CalculatedChannelConfigurationVirm;
import com.nexala.spectrum.ns.view.conf.UnitConfigurationVirmImpl;
import com.nexala.spectrum.providers.chart.impl.StaticChartProviderImpl;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChannelDefinitionProvider;
import com.nexala.spectrum.rest.data.ChartDataProvider;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;
import com.nexala.spectrum.rest.data.DefaultChannelDefinitionProvider;
import com.nexala.spectrum.rest.data.DefaultRecoveryProvider;
import com.nexala.spectrum.rest.data.DefaultStockProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.FaultMetaProvider;
import com.nexala.spectrum.rest.data.FaultMetaProviderImpl;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.GroupProvider;
import com.nexala.spectrum.rest.data.LocationProvider;
import com.nexala.spectrum.rest.data.MapProvider;
import com.nexala.spectrum.rest.data.RecoveryProvider;
import com.nexala.spectrum.rest.data.StaticChartProvider;
import com.nexala.spectrum.rest.data.StockProvider;
import com.nexala.spectrum.rest.data.SystemConfigProvider;
import com.nexala.spectrum.rest.data.SystemConfigProviderImpl;
import com.nexala.spectrum.rest.data.UnitDetailProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.EventChannelDataCache;
import com.nexala.spectrum.utils.FlywayUtils;
import com.nexala.spectrum.view.conf.DefaultChannelDataConfiguration;
import com.nexala.spectrum.view.conf.StaticDataplotConfigurationImpl;
import com.nexala.spectrum.view.conf.UnitConfiguration;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultUnitDetailConfiguration;
import com.nexala.spectrum.view.conf.unit.StaticDataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;
import com.nexala.spectrum.web.service.RuleTestDataProvider;

public class FleetVirmModule extends AbstractFleetModule {

    public FleetVirmModule() {
        super(NS.FLEET_VIRM, Virm.class);
    }
    
    @Override
    protected void configure() {
        super.configure();
        
        FlywayUtils.doFlywayMigration(NS.NS_DS_VIRM_FLYWAY, NS.FLEET_VIRM);
        
        bind(String.class).annotatedWith(Names.named("dataSource")).toInstance(NS.NS_DS_VIRM);
        bind(ConnectionProvider.class).to(JndiConnectionProvider.class).in(Singleton.class);
        
        // Factories
        install(new FactoryModuleBuilder().build(ChannelCollectionMapperFactory.class));
        install(new FactoryModuleBuilder().build(DataSetMapperFactory.class));
        install(new FactoryModuleBuilder().build(AnalysisDataSetMapperFactory.class));
        install(new FactoryModuleBuilder().build(StaticChartFactory.class));
        
        // Mappers
        bind(ChannelManager.class).to(ChannelManagerImpl.class);
        bind(EventChannelDataCache.class).in(Singleton.class);
                       
        // Specific Fleet Providers
        bindAndExpose(UnitProvider.class, UnitProviderVirm.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(UnitDetailProvider.class), UnitDetailProviderVirm.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(MapProvider.class), MapProviderVirm.class);
        bindAndExpose(FleetProvider.class, FleetProviderVirm.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(ChannelDataProvider.class), ChannelDataProviderVirm.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(GpsDataProvider.class), GpsDataProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(ChannelDefinitionProvider.class), DefaultChannelDefinitionProvider.class);
        
        // Multi Fleet Configurations
        bindAndExposeWithAnnotation(TypeLiteral.get(UnitConfiguration.class), UnitConfigurationVirmImpl.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(ChannelDataConfiguration.class), DefaultChannelDataConfiguration.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(UnitDetailConfiguration.class), DefaultUnitDetailConfiguration.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(CalculatedChannelConfiguration.class), CalculatedChannelConfigurationVirm.class);
        //bindAndExposeWithAnnotation(TypeLiteral.get(CabConfiguration.class), CabConfigurationNS.class);
        
        // Multi Fleet Providers
        bindAndExposeWithAnnotation(TypeLiteral.get(ChartDataProvider.class), ChartDataProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(StaticChartProvider.class), StaticChartProviderImpl.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(StaticDataPlotConfiguration.class), StaticDataplotConfigurationImpl.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(ChartVehicleTypeProvider.class), ChartVehicleTypeProviderVirm.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(GroupProvider.class), GroupProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(LocationProvider.class), LocationProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(StockProvider.class), DefaultStockProvider.class);
        bindAndExposeWithAnnotation(new TypeLiteral<RuleTestDataProvider<GenericEvent>>() {}, RuleTestDataProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(EventProvider.class), EventProviderNS.class);
        bindAndExposeWithAnnotation(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {}, AnalysisProviderNS.class);
        //bindAndExposeWithAnnotation(TypeLiteral.get(CabConfigurationProvider.class), CabConfigurationProviderNS.class);
        //bindAndExposeWithAnnotation(TypeLiteral.get(CabDataProvider.class), CabDataProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(EAnalysisProvider.class), DefaultEAnalysisProvider.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(RecoveryProvider.class), DefaultRecoveryProvider.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(MaximoServiceRequestProvider.class), MaximoServiceRequestProviderNS.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(FaultMetaProvider.class), FaultMetaProviderImpl.class);
        bindAndExposeWithAnnotation(TypeLiteral.get(SystemConfigProvider.class), SystemConfigProviderImpl.class);
    }

    private <T> void bindAndExposeWithAnnotation(TypeLiteral<T> interfaceClass, Class<? extends T> implemetation) {
        Key<T> eAProvider = Key.get(interfaceClass, Virm.class);
        bind(interfaceClass).to(implemetation);
        bind(eAProvider).to(implemetation);
        bind(implemetation);
        
        expose(eAProvider);
        
    }
    
}
