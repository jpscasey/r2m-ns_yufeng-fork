package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataSet;
import com.nexala.spectrum.analysis.data.GenericAnalysisProvider;
import com.nexala.spectrum.db.beans.EventChannelValue;
import com.nexala.spectrum.db.dao.SimpleEventChannelValueDao;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.ns.db.dao.LiveAnalysisDataDao;
import com.nexala.spectrum.ns.db.dao.LiveAnalysisDataGpsDao;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class AnalysisProviderNS extends GenericAnalysisProvider {
    
    private final static Logger LOGGER = Logger.getLogger(AnalysisProviderNS.class);
    
    @Inject
    private LiveAnalysisDataDao channelDataDao;
    
    @Inject
    private SimpleEventChannelValueDao eventChannelDataDao;
    
    @Inject
    private LiveAnalysisDataGpsDao gpsChannelDataDao;

    @Inject
    private MaximoServiceRequestProvider serviceRequestProvider;
 
    @Inject
    private SourceMerger sourceMerger;
    
    @Inject
    private UnitDao unitDao;
    
    @Override
    public long createFault(String faultCode, Long timestamp, Integer sourceId, String ruleName, String contextId, String fleetId, Map<String, String> extraFields) {
		long eventId = super.createFault(faultCode, timestamp, sourceId, ruleName, contextId, fleetId, extraFields);

		if (extraFields.containsKey("createServiceRequest")
				&& extraFields.get("createServiceRequest").equalsIgnoreCase("true")
				&& isUnitInOperation(sourceId)) {
			try {
				serviceRequestProvider.createAutomatedServiceRequest(faultCode, sourceId, eventId, ruleName);
			} catch (Exception e) {
				LOGGER.error("Unable to create automated Service Request", e);
			}
		}

		return eventId;
    }
    
    @Override
    public AnalysisDataMap<GenericEvent> getData(List<Long> lastRecordIds) {
        if (lastRecordIds.size() != 3) {
            return null;
        }
        
        List<AnalysisDataSet> liveChannelData = channelDataDao.getLiveData(lastRecordIds.get(0));
        
        Comparator<AnalysisDataSet> ascOrder = new Comparator<AnalysisDataSet>() {
            @Override
            public int compare(AnalysisDataSet ds1, AnalysisDataSet ds2) {
                return ds1.getTimestamp().compareTo(ds2.getTimestamp());
            }
        };
        
        Collections.sort(liveChannelData, ascOrder);

        List<AnalysisDataSet> channelEnrichmentData = Collections.emptyList();
        
        List<EventChannelValue> liveEventChannelData = Collections.emptyList();
        List<EventChannelValue> eventChannelEnrichmentData = Collections.emptyList();
        
        List<AnalysisDataSet> liveGpsChannelData = Collections.emptyList();
        List<AnalysisDataSet> gpsChannelEnrichmentData = Collections.emptyList();
        
        if (!liveChannelData.isEmpty()) {

            channelEnrichmentData = channelDataDao.getEnrichmentData(lastRecordIds.get(0));
            
            Long lastTimestamp = liveChannelData.get(liveChannelData.size()-1).getTimestamp();

            liveEventChannelData = eventChannelDataDao.getLiveEventChannelData(lastRecordIds.get(1), lastTimestamp);
            
            if (liveEventChannelData.size() == 10000) {
                LOGGER.warn("The max number of event channel data was retrieved, the data received is probably out of sync");
            }            

            // If we have new event channel data, then get all the other event channel values that are still valid, and 
            // enrich the live data with this data
            // This is to support cases where we trigger a fault when multiple event channel values are required
            eventChannelEnrichmentData = eventChannelDataDao.getEventChannelDataEnrichmentData(lastRecordIds.get(1));        
            
            // VT Gps data            
            liveGpsChannelData = gpsChannelDataDao.getGpsData(lastRecordIds.get(2), lastTimestamp);
            gpsChannelEnrichmentData = gpsChannelDataDao.getGpsEnrichmentData(lastRecordIds.get(2));                       
        }
        
        // Get the last channel value timestamp and update by 5 mins
        Long lastLiveChannelTime = lastRecordIds.get(0);
        for (AnalysisDataSet ds : liveChannelData) {
            Long rowId = ds.getTimestamp();
            
            if (lastLiveChannelTime == null || rowId > lastLiveChannelTime) {
                lastLiveChannelTime = rowId;
            }
        }
        
        // Get the last event channel value id
        Long lastLiveEventChannelTime = lastRecordIds.get(1);
        for (EventChannelValue ecv : liveEventChannelData) {
            Long rowId = ecv.getTimestamp();
            
            if (lastLiveEventChannelTime == null || rowId > lastLiveEventChannelTime) {
                lastLiveEventChannelTime = rowId;
            }
        }
        
        // Get the last gps channel value id
        Long lastLiveGpsChannelTime = lastRecordIds.get(2);
        for (AnalysisDataSet gds : liveGpsChannelData) {
            Long rowId = gds.getTimestamp();
            
            if (lastLiveGpsChannelTime == null || rowId > lastLiveGpsChannelTime) {
                lastLiveGpsChannelTime = rowId;
            }
        }

        return new AnalysisDataMap<GenericEvent>(getUnitMap(liveChannelData, channelEnrichmentData, liveEventChannelData, eventChannelEnrichmentData, liveGpsChannelData, gpsChannelEnrichmentData), lastLiveChannelTime, lastLiveEventChannelTime, lastLiveGpsChannelTime);
    }
    
    
    @Override
    public Map<Integer, List<GenericEvent>> getData(Integer stockId, long startTime, long endTime) {
        List<AnalysisDataSet> liveChannelData = channelDataDao.getData(stockId, startTime, endTime);
        List<AnalysisDataSet> channelEnrichmentData = channelDataDao.getEnrichmentData(stockId, startTime);
        List<EventChannelValue> liveEventChannelData = eventChannelDataDao.getEventChannelData(stockId, startTime, endTime);

        // If we have new event channel data, then get all the other event channel values that are still valid, and 
        // enrich the live data with this data
        // This is to support cases where we trigger a fault when multiple event channel values are required
        List<EventChannelValue> eventChannelEnrichmentData = eventChannelDataDao.getEventChannelDataEnrichmentData(stockId, startTime);  
        

        List<AnalysisDataSet> liveGpsChannelData = gpsChannelDataDao.getGpsData(stockId, startTime, endTime);
        List<AnalysisDataSet> gpsChannelEnrichmentData = gpsChannelDataDao.getGpsEnrichmentData(stockId, startTime);
        
        return getUnitMap(liveChannelData, channelEnrichmentData, liveEventChannelData, eventChannelEnrichmentData, liveGpsChannelData, gpsChannelEnrichmentData);
    }

    private Map<Integer, List<GenericEvent>> getUnitMap(List<AnalysisDataSet> liveChannelData, List<AnalysisDataSet> channelEnrichmentData, List<EventChannelValue> liveEventChannelData, 
            List<EventChannelValue> eventChannelEnrichmentData, List<AnalysisDataSet> liveGpsChannelData, List<AnalysisDataSet> gpsChannelEnrichmentData) {
        // Convert the lists into maps, where the key is the unit ID.
        Map<Integer, List<DataSet>> channelDataMap = getDataSetMap(liveChannelData);
        Map<Integer, List<DataSet>> channelEnrichmentDataMap = getDataSetMap(channelEnrichmentData);
        Map<Integer, List<EventChannelValue>> eventChannelDataMap = geEventChannelValueMap(liveEventChannelData);
        Map<Integer, List<EventChannelValue>> eventChannelEnrichmentDataMap = geEventChannelValueMap(eventChannelEnrichmentData);
        Map<Integer, List<DataSet>> gpsChannelDataMap = getDataSetMap(liveGpsChannelData);
        Map<Integer, List<DataSet>> gpsChannelEnrichmentDataMap = getDataSetMap(gpsChannelEnrichmentData);
        
        // Get all of the unit IDs of live data only (not enrichment data)
        Set<Integer> ids = new HashSet<Integer>();
        ids.addAll(channelDataMap.keySet());
        ids.addAll(eventChannelDataMap.keySet());
        ids.addAll(gpsChannelDataMap.keySet());
        
        Map<Integer, List<GenericEvent>> unitMap = new HashMap<Integer, List<GenericEvent>>();
        
        Integer maxTimestampDifferenceMinutes = appConf.get().getInt("r2m."+fleetId+".stream-analysis.maxEventChannelTimeDifferenceMinutes");
        Integer maxTimeDiffMs =  maxTimestampDifferenceMinutes*60000;
        
        for (Integer id : ids) {
            unitMap.put(id, sourceMerger.mergeSlt(channelDataMap.get(id), channelEnrichmentDataMap.get(id), eventChannelDataMap.get(id), 
                    eventChannelEnrichmentDataMap.get(id), gpsChannelDataMap.get(id), gpsChannelEnrichmentDataMap.get(id), maxTimeDiffMs));
        }
        
        return unitMap;
    }
    
    private Map<Integer, List<DataSet>> getDataSetMap(List<? extends DataSet> dataSetList) {

        Map<Integer, List<DataSet>> dataSetMap = new HashMap<Integer, List<DataSet>>();

        if (dataSetList != null) {
            for (DataSet ds : dataSetList) {
                Integer id = ds.getSourceId();

                if (dataSetMap.get(id) == null) {
                    dataSetMap.put(id, new ArrayList<DataSet>());
                }

                dataSetMap.get(id).add(ds);
            }
        }

        return dataSetMap;
    }
    
    private Map<Integer, List<EventChannelValue>> geEventChannelValueMap(List<EventChannelValue> eventChannelValueList) {

        Map<Integer, List<EventChannelValue>> eventChannelValueMap = new HashMap<Integer, List<EventChannelValue>>();

        if (eventChannelValueList != null) {
            for (EventChannelValue ecv : eventChannelValueList) {
                Integer id = ecv.getSourceId();

                if (eventChannelValueMap.get(id) == null) {
                    eventChannelValueMap.put(id, new ArrayList<EventChannelValue>());
                }

                eventChannelValueMap.get(id).add(ecv);
            }
        }

        return eventChannelValueMap;
    }
    
    private boolean isUnitInOperation(int unitId) {
    	String unitStatus = new String();
        List<String> list =  unitDao.getUnitStatus(unitId);
        if(list.size() > 0){
        	unitStatus =  list.get(0);
        }
        if(unitStatus != null && (unitStatus.equals("BVD") || unitStatus.equals("OVERSTAND")) ){
        	return true;
        }
        else return false;
    	
    }
    
}
