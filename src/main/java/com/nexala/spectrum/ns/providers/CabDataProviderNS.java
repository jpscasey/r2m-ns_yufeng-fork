package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.dao.HistoricChannelValueDao;
import com.nexala.spectrum.ns.db.dao.HistoricEventChannelValueDao;
import com.nexala.spectrum.rest.data.CabDataProvider;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.service.CabConfigurationProvider;
import com.nexala.spectrum.view.conf.cab.CabConfiguration;

public class CabDataProviderNS implements CabDataProvider {

    @Inject
    private CabConfigurationProvider configurationProvider;

    @Inject
    private ChannelConfiguration channelConfiguration;

    @Inject
    private UnitDao unitDao;

    @Inject
    private HistoricChannelValueDao cdDao;

    @Inject
    private HistoricEventChannelValueDao ecvDao;
    
    private Comparator<DataSet> dataSetComparator = new Comparator<DataSet>() {
        @Override
        public int compare(DataSet ds1, DataSet ds2) {
            String ds1Value = ds1.getChannels().getByName(Spectrum.TIMESTAMP).getStringValue();
            String ds2Value = ds2.getChannels().getByName(Spectrum.TIMESTAMP).getStringValue();
            
            if (ds1Value == null || ds2Value == null)
                return 0;
            return ds1Value.compareTo(ds2Value);
        }
    };

    @Override
    public DataSet getVehicleData(int vehicleId, int fleetFormationId, Long prevTimestamp, Long timestamp) {
        CabConfiguration configuration = configurationProvider.getCabConfiguration(vehicleId);

        long ts = (timestamp != null ? timestamp : System.currentTimeMillis());
        long timeDiff = (prevTimestamp != null ? ts - prevTimestamp : 0);

        if (timeDiff < configuration.getDataInterval() || timeDiff > configuration.getDataIntervalLimit()) {
            timeDiff = configuration.getDataIntervalLimit();
        }

        List<ChannelConfig> configs = new ArrayList<ChannelConfig>();
        for (Integer channelId : configuration.getChannelIds()) {
            configs.add(channelConfiguration.getConfig(channelId));
        }

        List<DataSet> data = cdDao.find(vehicleId, ts, (int) (timeDiff / 1000), null, configs);

        DataSet v = DataUtils.getVehicleData(data, channelConfiguration, configuration.hasTransientStates());

        List<DataSet> events = ecvDao.find(vehicleId, ts, (int) (timeDiff / 1000), configs);
        Collections.sort(events, dataSetComparator);
        List<Channel<?>> evChannelList = new ArrayList<Channel<?>>();
        for (DataSet evc : events) {
            evChannelList.addAll(evc.getChannels().getChannels().values());
        }
        // reverse to ensure timestamp and channel timestamp are the same
        Collections.reverse(evChannelList);

        if (v == null) {
            v = new DataSetImpl(vehicleId, ts, new ChannelCollection());
        }
                
        addEventData(v.getChannels(), evChannelList);
        calculateCustomChannels(v.getChannels());

        return v;
    }

    @Override
    public List<StockDesc> getStockForUnit(int unitId) {
        List<Unit> formation = unitDao.findUnitFormationByUnitId(unitId);
        List<StockDesc> stock = new ArrayList<StockDesc>();

        for (Unit u : formation) {
            stock.add(new StockDesc(u.getId(), u.getUnitNumber(), 0, null, false, u.getUnitType()));
        }

        return stock;
    }

    private void calculateCustomChannels(ChannelCollection collection) {
        calculateDirectionLeverChannel(collection);
        calculateTractionLeverChannel(collection);
        calculateCircuitBreakerChannel(collection);
        calculateDia9016Value(collection);
    }

    private void addEventData(ChannelCollection collection, List<Channel<?>> events) {
        // making sure only one entry per channel id
        Map<Integer, Channel<?>> eventMap = new HashMap<>();
        for (Channel<?> event : events) {
            eventMap.put(event.getId(), event);
        }

        for (Channel<?> entry : eventMap.values()) {
            collection.put(entry);
        }
    }

    private void calculateDirectionLeverChannel(ChannelCollection collection) {
        ChannelBoolean dia902B = (ChannelBoolean) collection.getById(ChannelId.CH5835);
        ChannelBoolean dia902C = (ChannelBoolean) collection.getById(ChannelId.CH5836);
        ChannelInt directionChannel = new ChannelInt(NS.DIRECTION_LEVER);

        if (dia902B != null && dia902B.getValue() != null && dia902B.getValue() == true) {
            directionChannel.setValue(2);
        } else if (dia902C != null && dia902C.getValue() != null && dia902C.getValue() == true) {
            directionChannel.setValue(0);
        } else {
            directionChannel.setValue(1);
        }
        collection.put(directionChannel);
    }

    private void calculateTractionLeverChannel(ChannelCollection collection) {
        ChannelBoolean dia902D = (ChannelBoolean) collection.getById(ChannelId.CH5837);
        ChannelBoolean dia902E = (ChannelBoolean) collection.getById(ChannelId.CH5838);
        ChannelBoolean dia902F = (ChannelBoolean) collection.getById(ChannelId.CH5839);
        ChannelInt tractionBrakeChannel = new ChannelInt(NS.TRACTION_BRAKE_LEVER);

        if (dia902D != null && dia902D.getValue() != null && dia902D.getValue() == true) {
            tractionBrakeChannel.setValue(3);
        } else if (dia902E != null && dia902E.getValue() != null && dia902E.getValue() == true) {
            tractionBrakeChannel.setValue(1);
        } else if (dia902F != null && dia902F.getValue() != null && dia902F.getValue() == true) {
            tractionBrakeChannel.setValue(0);
        } else {
            tractionBrakeChannel.setValue(2);
        }
        collection.put(tractionBrakeChannel);
    }

    private void calculateCircuitBreakerChannel(ChannelCollection collection) {
        ChannelBoolean dia9028 = (ChannelBoolean) collection.getById(ChannelId.CH5832);
        ChannelBoolean invertedDia9028 = new ChannelBoolean(NS.CIRCUIT_BREAKER_SWITCH);
        if (dia9028 != null && dia9028.getValue() != null) {
            invertedDia9028.setValue(!dia9028.getValue());
            collection.put(invertedDia9028);
        }
    }

    private void calculateDia9016Value(ChannelCollection collection) {
        ChannelBoolean dia9016 = (ChannelBoolean) collection.getById(ChannelId.CH5814);
        ChannelString dia9016Value = new ChannelString(NS.DIA_9016_VALUE, "");
        if (dia9016 != null && dia9016.getValue() != null && dia9016.getValue() == true) {
            dia9016Value.setValue("S");
        }
        collection.put(dia9016Value);
    }
}
