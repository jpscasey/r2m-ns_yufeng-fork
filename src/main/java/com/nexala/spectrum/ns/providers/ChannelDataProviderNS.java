package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.CalculatedChannel;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.db.dao.VehicleDao;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.db.dao.ChannelValueDao;
import com.nexala.spectrum.ns.db.dao.ChannelValueGpsDao;
import com.nexala.spectrum.ns.db.dao.HistoricChannelValueDao;
import com.nexala.spectrum.ns.db.dao.HistoricChannelValueGpsDao;
import com.nexala.spectrum.ns.db.dao.HistoricEventChannelValueDao;
import com.nexala.spectrum.ns.db.dao.HistoricEventDataSetDao;
import com.nexala.spectrum.ns.db.dao.TimestampDaoNS;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChannelLookupProvider;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.validation.ChannelValidator;

public abstract class ChannelDataProviderNS implements ChannelDataProvider {

    @Inject
    private CalculatedChannelConfiguration calculatedConf;

    @Inject
    private ChannelValueDao channelValueDao;
    
    @Inject
    private ChannelValueGpsDao channelValueGpsDao;

    @Inject
    private HistoricChannelValueDao cdDao;
    
    @Inject
    private HistoricChannelValueGpsDao gpsDao;

    @Inject
    private HistoricEventChannelValueDao eventChannelValueDao;

    @Inject
    private HistoricEventDataSetDao eventDataSetDao;

    @Inject
    private VehicleDao vehicleDao;

    @Inject
    private UnitDao unitDao;
    
    @Inject
    private TimestampDaoNS timestampDao;

    @Inject
    private ChannelValidator validationEngine;
    
    @Inject
    private ChannelConfiguration channelConfig;
    
    @Inject
    private ChannelLookupProvider lookUp;

    private Comparator<DataSet> dataSetComparator = new Comparator<DataSet>() {
        @Override
        public int compare(DataSet ds1, DataSet ds2) {
            String ds1Value = ds1.getChannels().getByName(Spectrum.TIMESTAMP).getStringValue();
            String ds2Value = ds2.getChannels().getByName(Spectrum.TIMESTAMP).getStringValue();
            
            if (ds1Value == null || ds2Value == null)
                return 0;
            return ds1Value.compareTo(ds2Value);
        }
    };
    
    public List<DataSet> getVehicleData(int unitId, Long endTime,
            int interval, List<ChannelConfig> channels, boolean loadLookupValues) {
        return getVehicleData(unitId, endTime, interval, null, channels,
                loadLookupValues);
    }

    @Override
    public List<DataSet> getVehicleData(int unitId, Long endTime,
            int interval, Integer endRow, List<ChannelConfig> channels,
            boolean loadLookupValues) {
        List<DataSet> result = new ArrayList<DataSet>();
        List<DataSet> eventChannelValues = new ArrayList<DataSet>();
        List<ChannelConfig> channelAdded = new ArrayList<ChannelConfig>();
        List<ChannelConfig> channelRemoved = new ArrayList<ChannelConfig>();

        for (CalculatedChannel ccc : calculatedConf.getCalculatedChannels()) {
            for (ChannelConfig config : channels) {
                if (config.getId() == ccc.getChannel().getId()) {
                    channelRemoved.add(config);
                    for (ChannelConfig dp : ccc.getDependencies()) {
                        channelAdded.add(dp);
                    }
                }
            }
        }

        channels.addAll(channelAdded);
        channels.removeAll(channelRemoved);

        List<DataSet> data = cdDao.find(unitId, endTime, interval, endRow, channels);
        List<DataSet> eventChannels = eventDataSetDao.find(unitId, endTime, interval, channels);
        List<DataSet> gpsChannels = gpsDao.find(unitId, endTime, interval, endRow, channels);

        // order the data by timestamp
        Collections.sort(data, dataSetComparator);
        Collections.sort(eventChannels, dataSetComparator);
        Collections.sort(gpsChannels, dataSetComparator);
        
        Map<Long, DataSet> results = new HashMap<Long, DataSet>(); 
        
        for(DataSet ds: data){
            results.put(ds.getTimestamp(), ds);
        }
        
        for(DataSet evc: eventChannels){          
            if(results.get(evc.getTimestamp()) != null){
                results.get(evc.getTimestamp()).getChannels().getChannels().putAll(evc.getChannels().getChannels());
            } else {
                results.put(evc.getTimestamp(), evc);
            }
        }
        
        for(DataSet gps: gpsChannels){          
            if(results.get(gps.getTimestamp()) != null){
                results.get(gps.getTimestamp()).getChannels().getChannels().putAll(gps.getChannels().getChannels());
            } else {
                results.put(gps.getTimestamp(), gps);
            }
        }
        
        for(DataSet ds : results.values()){
            result.add(ds);
        }
                
        validate(result);
        Collections.sort(result, dataSetComparator);
        
        return result;
    }


    @Override
    public List<DataSet> getLiveChannelData() {
        List<DataSet> data = channelValueDao.getLiveFleetData();
        validate(data);
        return data;
    }

    @Override
    public List<DataSet> getLiveChannelData(List<Integer> channelIds,
            String unitIds) {
        List<Integer> channelCalculated = new ArrayList<Integer>();
        List<Integer> channelRemoved = new ArrayList<Integer>();

        // This logic is copied and used in other places within the app
        // Put it in a central place for everyone to enjoy
        for (CalculatedChannel ccc : calculatedConf.getCalculatedChannels()) {
            for (Integer channelId : channelIds) {
                if (channelId.compareTo(ccc.getChannel().getId()) == 0) {
                    channelCalculated.add(channelId);
                    for (ChannelConfig depen : ccc.getDependencies())
                        channelRemoved.add(depen.getId());

                }
            }
        }

        channelRemoved.addAll(channelIds);
        channelRemoved.removeAll(channelCalculated);

        List<DataSet> values = channelValueDao.getGroupChannelData(
                channelRemoved, unitIds);

        validate(values);
        DataUtils.loadLookupValues(lookUp, channelConfig, values);

        return DataUtils.getVehiclesInColumn(values, channelConfig, channelIds);
    }

    @Override
    public List<DataSet> getUnitData(int unitId, Long timestamp, int direction) {
        List<Unit> formation = unitDao.findUnitFormationByUnitId(unitId);
        List<DataSet> data = new ArrayList<DataSet>();

        for (Unit u : formation) {
            List<DataSet> channelValueData = channelValueDao.getHistoricData(u.getId(), timestamp);
            DataSet cv = (channelValueData != null && channelValueData.size() > 0) ? channelValueData
                    .get(0) : new DataSetImpl(u.getId(), timestamp, null);

            List<DataSet> eventChannelData = eventChannelValueDao.getHistoricEventData(u.getId(), timestamp);
            Collections.sort(eventChannelData, dataSetComparator);
            List<Channel<?>> evChannelList = new ArrayList<Channel<?>>();
            for(DataSet evc: eventChannelData){ 
            	evChannelList.addAll(evc.getChannels().getChannels().values());
            }
            //reverse to ensure timestamp and channel timestamp are the same
            Collections.reverse(eventChannelData);
            DataSet ecv = (eventChannelData != null && eventChannelData.size() > 0) ? eventChannelData
                    .get(0) : new DataSetImpl(u.getId(), null, null);
            if(ecv.getChannels() != null)        
            	ecv.getChannels().setChannels(evChannelList);        
                    
            List<DataSet> channelValueGpsData = channelValueGpsDao.getHistoricData(u.getId(), timestamp);
            DataSet gpsData = (channelValueGpsData != null && channelValueGpsData.size() > 0) ? 
                    channelValueGpsData.get(0) : new DataSetImpl(u.getId(), null, null);
                                
            data.add(mergeGpsDataSetChannels(mergeDataSetChannels(cv, ecv), gpsData));
        }

        validate(data);
        return data;
    }

    public static DataSet mergeDataSetChannels(DataSet dataSet1,
            DataSet dataSet2) {

        ChannelCollection collection = new ChannelCollection();
        if (dataSet1.getChannels() != null) {
            collection.getChannels().putAll(
                    dataSet1.getChannels().getChannels());
        }
        if (dataSet2.getChannels() != null) {
            collection.getChannels().putAll(
                    dataSet2.getChannels().getChannels());
        }
        Long latestTimestamp = null;
        boolean isLatestTimestampSet2 = DataUtils.isSecondDataSetLatestTimestamp(dataSet1, dataSet2);
        if(isLatestTimestampSet2){
        	latestTimestamp = dataSet2.getTimestamp();
        	collection.put(dataSet2.getChannels().getByName(Spectrum.TIMESTAMP));
        }
        else {
        	latestTimestamp = dataSet1.getTimestamp();
        	collection.put(dataSet1.getChannels().getByName(Spectrum.TIMESTAMP));
        }
        
        DataSet newDataSet = new DataSetImpl(dataSet1.getSourceId(),
        		latestTimestamp, collection);

        return newDataSet;
    }
    
    public DataSet mergeGpsDataSetChannels(DataSet dataSet1,
            DataSet dataSet2) {
        
        ChannelCollection collection = new ChannelCollection();
        
        //put all the channel value data into collection
        if (dataSet1.getChannels() != null) {
            collection.getChannels().putAll(
                    dataSet1.getChannels().getChannels());
        }
        
        //overwrite the channelvalue with the gps location
        collection.put(dataSet2.getChannels().getByName(Spectrum.LOCATION));
        
        //add the GPS Latitude and Longitude to the dataset
        collection.put(dataSet2.getChannels().getById(getVTLatitudeChannel(collection)));
        collection.put(dataSet2.getChannels().getById(getVTLongitudeChannel(collection)));    
        collection.put(dataSet2.getChannels().getById(getVTSpeedChannel(collection)));        

        
        Long latestTimestamp = null;
        boolean isLatestTimestampGps = DataUtils.isSecondDataSetLatestTimestamp(dataSet1, dataSet2);
        if(isLatestTimestampGps){
        	latestTimestamp = dataSet2.getTimestamp();
        	collection.put(dataSet2.getChannels().getByName(Spectrum.TIMESTAMP));
        }
        else {
        	latestTimestamp = dataSet1.getTimestamp();
        	collection.put(dataSet1.getChannels().getByName(Spectrum.TIMESTAMP));
        }
                
        DataSet newDataSet = new DataSetImpl(dataSet1.getSourceId(),
                latestTimestamp, collection);

        return newDataSet;
    }
    

    @Override
    public List<DataSet> getHistoricFleetData(long timestamp) {
        List<DataSet> data = channelValueDao.getFleetData(timestamp);

        validate(data);

        return data;
    }
    
    @Override
    public String getActiveVehicle(String unitIds) {
        return null;
    }

    @Override
    public Long getUnitTimestamp(int unitId, Long timestamp) {
        
        Long newerTimestamp = timestampDao.getLastUnitTimestamp(unitId, timestamp);
        
        return newerTimestamp;
    }
    
    @Override
    public Long getUnitLastMaintTimestamp(int unitId) {
    	return unitDao.getLastMaintTimestamp(unitId) != null ? unitDao.getLastMaintTimestamp(unitId).getTime() : null;
    }

    @Override
    public String getVehicleNumberById(String vehicleID) {
        return vehicleDao.getVehicleNumberById(vehicleID);
    }

    private void validate(List<DataSet> data) {
        validationEngine.validate(data);
    }

    @Override
    public List<DataSet> getVehicleDataExport(int vehicleId, long startTime, long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues) {
        return getVehicleData(vehicleId, endTime, (int) ((endTime - startTime) / 1000), null, channels, loadLookupValues);
    }

    @Override
    public List<DataSet> getSampledVehicleData(int vehicleId, Long startTime, Long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues, Long samplingRate) {
        return getVehicleData(vehicleId, endTime, (int) ((endTime - startTime) / 1000), null, channels, loadLookupValues);

    }

    @Override
    public List<DataSet> getVehicleDataChart(int vehicleId, long startTime, long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues) {
        return getVehicleData(vehicleId, endTime, (int) ((endTime - startTime) / 1000), null, channels, loadLookupValues);
    }
    
    protected abstract int getVTSpeedChannel(ChannelCollection channels);
    
    protected abstract int getVTLatitudeChannel(ChannelCollection channels);
    
    protected abstract int getVTLongitudeChannel(ChannelCollection channels);

}
