package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.charting.ChartChannel;
import com.nexala.spectrum.db.dao.charting.ChartChannelDao;
import com.nexala.spectrum.db.dao.charting.ChartConfigurationDao;
import com.nexala.spectrum.db.dao.charting.DataViewConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.ChartDataProvider;

public class ChartDataProviderNS implements ChartDataProvider {

    @Inject
    private ChannelConfiguration channelConfiguration;

    @Inject
    private ChartConfigurationDao chartConfigurationDao;
    
    @Inject
    private ChartChannelDao chartChannelDao;

    @Override
    public List<ChartChannel> findChannels(long chartId) {
        List<ChartChannel> channels = chartChannelDao.findByChartId(chartId);
        return channels;
    }

    @Override
    public ChannelConfiguration getChannelConfiguration() {
        return channelConfiguration;
    }

    @Override
    public List<DataViewConfiguration> findUserConfigurations(Licence licence) {
        return chartConfigurationDao.findUserConfigurations(licence);
    }

    @Override
    public List<DataViewConfiguration> findSystemConfigurations() {
        return chartConfigurationDao.findSystemConfigurations();
    }

    @Override
    public DataViewConfiguration findChart(Long chartId, String chartName, String userName) {
        return chartConfigurationDao.findChart(chartId, chartName, userName);
    }

    @Override
    public DataViewConfiguration createChart(String name, List<Integer> channelIds, String userId)
            throws DataAccessException {
        return chartConfigurationDao.createChart(name, channelIds, userId);
    }

    @Override
    public void saveChart(Long chartId, List<Integer> channelIds, String userId) throws DataAccessException {
        chartConfigurationDao.saveChart(chartId, channelIds, userId);
    }

    @Override
    public void deleteChart(Long configurationId, String userId) throws DataAccessException {
        chartConfigurationDao.deleteChart(configurationId, userId);
    }

    @Override
    public void renameChart(Long chartId, String newName, String userId) throws DataAccessException {
        chartConfigurationDao.renameChart(chartId, newName, userId);
    }

    @Override
    public List<ChannelConfig> getConfigurations(List<Integer> channelIds) {
        List<ChannelConfig> configurations = new ArrayList<ChannelConfig>();

        for (Integer id : channelIds) {
            ChannelConfig config = channelConfiguration.getConfig(id);

            if (config != null) {
                configurations.add(config);
            }
        }

        return configurations;
    }

    @Override
    public List<ChannelConfig> getConfigurations(List<Integer> channelIds, Integer vehicleId) {
        return getConfigurations(channelIds);
    }
}
