package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.ns.db.dao.DetectorDao;
import com.nexala.spectrum.rest.data.DetectorProvider;
import com.nexala.spectrum.view.conf.maps.Detector;
import com.nexala.spectrum.view.conf.maps.DetectorSiteMarker;

public class DetectorProviderNS implements DetectorProvider {

    private DetectorDao detectorDao;
    
    @Inject
    public DetectorProviderNS(DetectorDao detectorDao) {
        this.detectorDao = detectorDao;
    }
    
    @Override
    public List<DetectorSiteMarker> getDetectorData(){
        
        //contains a list of every site with a detector
        //sites with multiple detectors are duplicated for each detector
        List<DetectorSiteMarker> prepData = detectorDao.getDetectorMarkers();
        
        //create map of the site ids and DetectorSiteMarkers to contain only one site and its detectors
        Map<Integer, DetectorSiteMarker> map = new HashMap<Integer, DetectorSiteMarker>();
                
        for(DetectorSiteMarker site : prepData){
            //add the site to the map
            if(!map.containsKey(site.getId())){
                map.put(site.getId(), site);
            } else {
                if(map.get(site.getId()).getDetectors().size() < 2){
                    //add the detector for the site to the site in the map
                    List<Detector> d = new ArrayList<Detector>();
                    d.add(map.get(site.getId()).getDetectors().get(0));
                    d.add(site.getDetectors().get(0));
                    map.get(site.getId()).setDetectors(d);
                }
            }
        }
        
        List<DetectorSiteMarker> result = new ArrayList<DetectorSiteMarker>();   

        //contains each site and all detectors at the site
        for(DetectorSiteMarker e : map.values()){
            result.add(e);
        }
        
        return result;
    }

}
