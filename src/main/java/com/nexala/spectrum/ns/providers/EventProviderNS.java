package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.FaultGroupInputParam;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.dao.EventChannelValueDao;
import com.nexala.spectrum.ns.db.dao.MaximoServiceRequestDao;
import com.nexala.spectrum.rest.data.ChannelLookupProvider;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.DefaultEventProvider;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.validation.ChannelValidator;

public class EventProviderNS extends DefaultEventProvider {

    @Inject
    private ChannelValidator validationEngine;

    @Inject
    private ChannelConfiguration channelConfig;

    @Inject
    private EventChannelValueDao eventChannelValueDao;

    @Inject
    private ChannelLookupProvider lookupProvider;
    
    @Inject
    private MaximoServiceRequestDao maximoDao;

    @Inject
    public EventProviderNS(EventDao eventDao) {
        super(eventDao);
        
    }

    @Override
    public ChannelData getEventChannelDataForStock(Integer eventId, Integer unitId, Integer groupId) {
        List<DataSet> values = eventChannelValueDao.getEventChannelDataByUnit(eventId, unitId);
        validate(values);

        ChannelGroupConfig groupConfig = channelConfig.getGroup(groupId);

        ChannelData channelData = new ChannelData();

        if (groupConfig != null) {
            DataUtils.loadLookupValues(lookupProvider, channelConfig, values);
            channelData.setColumns(getColumns(groupConfig));
            channelData.setData(DataUtils.getChannelEventList(values, channelConfig,
                    groupConfig.getChannelIdList()));
        }

        return channelData;
    }

    private void validate(List<DataSet> data) {
        validationEngine.validate(data);
    }
    
    @Override
    public List<Event> searchEvents(Date dateFrom, Date dateTo, String live, String keyword, Integer limit) {
        List<Event> result = eventDao.searchEvents(limit, dateFrom, dateTo, live, keyword);
        for (Event event : result) {
            event.setField(NS.FAULT_GROUP_STATUS, computeGroupStatus(event));
        }
        
        return result;
    }

    @Override
    public List<Event> searchEvents(Date dateFrom, Date dateTo, String code, String type, String description, String category,
            String live, List<EventSearchParameter> parameters, Integer limit) {
        List<Event> result =  eventDao.searchEvents(limit, dateFrom, dateTo, live, code, type, description, category, parameters);
        for (Event event : result) {
            event.setField(NS.FAULT_GROUP_STATUS, computeGroupStatus(event));
        }
        
        return result;
    }
    
    @Override
    public void createFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId) {
        
        for (FaultGroupInputParam param : params) {
            if (param.getIsFaultGroupLead()) {
                leadEventId = param.getFaultId();
            }
        }
        
        Event leadEvent = getEventById(leadEventId, false);
        String serviceRequestCode = (String) leadEvent.getField(NS.EVENT_SERVICE_REQUEST);
        
        eventDao.groupFaults(username, params, leadEventId);

        // add Service Request code to all faults in the group
        addServiceRequestToGroup(params, serviceRequestCode);
    }
    
    @Override
    public void updateFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId) {
        
        Event leadEvent = getEventById(leadEventId, false);
        String serviceRequestCode = (String) leadEvent.getField(NS.EVENT_SERVICE_REQUEST);
        Long eventGroupId = Long.valueOf(leadEvent.getField(NS.EVENT_GROUP_ID).toString());

        // add Service Request code to all faults in the group
        Boolean hasNewgroupLead = addServiceRequestToGroup(params, serviceRequestCode);
        if (hasNewgroupLead) {
            eventDao.updateGroupWithNewLead(username, params, eventGroupId);
        } else {
            eventDao.updateGroup(username, params, eventGroupId);
        }
    }
    
    public Boolean addServiceRequestToGroup(List<FaultGroupInputParam> params, String serviceRequestCode) {
        Boolean hasNewgroupLead = false;
        List<FaultGroupInputParam> newParams = new ArrayList<FaultGroupInputParam>();

        for (FaultGroupInputParam param : params) {
            if (param.getFaultGroupId() == null) {
                newParams.add(param);
            }
        }

        for (FaultGroupInputParam param : newParams) {
            Event newEvent = getEventById(param.getFaultId(), false);
            if ((String) newEvent.getField(NS.EVENT_SERVICE_REQUEST)!= null) {
                serviceRequestCode = (String) newEvent.getField(NS.EVENT_SERVICE_REQUEST);
                if (serviceRequestCode != null) {
                    param.setIsFaultGroupLead(true);
                    for (FaultGroupInputParam param2 : params) {
                        if (!newEvent.getField(NS.EVENT_SERVICE_REQUEST).equals(getEventById(param2.getFaultId(), false).getField(NS.EVENT_SERVICE_REQUEST))) {
                            param2.setIsFaultGroupLead(false);
                            hasNewgroupLead = true;
                        }
                    }
                }
            }
        }

        for (FaultGroupInputParam param : params) {
            Event newEvent = getEventById(param.getFaultId(), false);
            if ((param.getIsFaultGroupLead() == false) ||
               ((param.getIsFaultGroupLead() == true) && (!(serviceRequestCode == (String) newEvent.getField(NS.EVENT_SERVICE_REQUEST))))) {
                Long faultId = param.getFaultId();
                maximoDao.addServiceRequestToFault(faultId, serviceRequestCode);
            }
        }
        if (params.size() > newParams.size()) {
            hasNewgroupLead = true;
        }
        return hasNewgroupLead;
    }
    
    public String computeGroupStatus(Event event) {
        boolean hasGroup = event.getField(Spectrum.EVENT_GROUP_ID) != null;
        boolean isGroupLead = event.getField(Spectrum.EVENT_IS_GROUP_LEAD) != null && (boolean) event.getField(Spectrum.EVENT_IS_GROUP_LEAD);
        
        if (!hasGroup) {
            return NS.FAULT_NOT_GROUPED_KEY;
        } else if (!isGroupLead) {
            return NS.FAULT_GROUPED_KEY;
        } else {
            return NS.FAULT_GROUP_LEAD_KEY;
        }
    }
}