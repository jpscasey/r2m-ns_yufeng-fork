package com.nexala.spectrum.ns.providers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelDouble;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.view.conf.DrillDownConfigurationNS;
import com.nexala.spectrum.rest.data.AbstractFleetProvider;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChannelLookupProvider;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.EventChannelDataProvider;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.SourceEventChannelData;
import com.nexala.spectrum.validation.ChannelStatusUtil;
import com.nexala.spectrum.validation.ChannelValidator;

public abstract class FleetProviderNS extends AbstractFleetProvider {
        
    protected ChannelDataProvider channelDataProvider;
    
    @Inject
    protected GpsDataProvider gpsDataProvider;
    
    protected ChannelConfiguration channelConfiguration;
    
    @Inject
    private ChannelStatusUtil channelStatusUtil;
    
    @Inject
    protected ChannelLookupProvider lookUp;
    
    @Inject
    protected EventChannelDataProvider eventChannelValueProvider;
    
    public FleetProviderNS(ChannelConfiguration channelConfiguration, ChannelDataProvider channelDataProvider,
            ChannelValidator channelValidator, UnitProvider unitProvider,
            DrillDownConfigurationNS configuration, ApplicationConfiguration applicationConfiguration, 
            UserConfigurationProvider userConfigProvider, GpsDataProvider gpsDataProvider) {
        super(channelConfiguration, channelDataProvider, channelValidator, unitProvider, configuration, applicationConfiguration, userConfigProvider);
        
        this.channelDataProvider = channelDataProvider;
        this.channelConfiguration = channelConfiguration;
        this.gpsDataProvider = gpsDataProvider;
    }
    
    @Override
    public List<DataSet> getLiveData() {
        List<DataSet> data = channelDataProvider.getLiveChannelData();
        List<SourceEventChannelData> eventChannelData = eventChannelValueProvider.getEventChannelData();
        List<DataSet> gpsData = gpsDataProvider.getLiveGpsData();
        overrideLatestTimestamp(data, eventChannelData, gpsData);
        DataUtils.addEventChannelsIntoDatasets(eventChannelData, data, channelConfiguration);
        DataUtils.mergeDatasets(data, gpsData);
        DataUtils.loadLookupValues(lookUp, channelConfiguration, data);

        return data;
    }
    
    @Override
	public List<DataSet> getFleet(List<DataSet> vehicles) {
        List<DataSet> dataSetList = new ArrayList<DataSet>();
        channelValidator.validate(vehicles);
        Map<Integer, List<DataSet>> data = DataUtils.getFormation(vehicles);
        
        for (Integer id : data.keySet()) {      	
            DataSet dataSet = getFleetDataSet(id, data.get(id));
            dataSetList.add(dataSet);
        }
        
        return dataSetList;
    }
    
    private DataSet getFleetDataSet(Integer id, List<DataSet> data) {
        String headcode = null;
        int serviceStatusPriority = 0;
        Set<Integer> unitIdSet = new HashSet<Integer>();
        Map<Integer, String> unitNumberMap = new HashMap<Integer, String>();
        Map<Integer, Integer> unitOperationalMap = new HashMap<Integer, Integer>();
        String unitType = null;
        String location = null;
        Long newerTimestamp = null;
        Long olderTimestamp = null;
        Double speed = null;
        int p1FaultNumber = 0;
        int p2FaultNumber = 0;
        int p3FaultNumber = 0;
        int p4FaultNumber = 0;
        int p5FaultNumber = 0;
        int faultRecoveryNumber = 0;
        int faultNotAcknowledgedNumber = 0;
        int unitStatus = 0;
        int unitPosition = 1;
        long locationTime = 0;
        long speedTime = 0;            
        String activeCab = null;
        int hasOperationalUnits = 0;
        
        ChannelCategory speedCategory = ChannelCategory.NODATA;
        ChannelCategory tractionGroupCategory = ChannelCategory.NODATA;
        ChannelCategory brakesGroupCategory = ChannelCategory.NODATA;
        ChannelCategory airSupplyGroupCategory = ChannelCategory.NODATA;
        ChannelCategory hvacGroupCategory = ChannelCategory.NODATA;
        ChannelCategory toiletsGroupCategory = ChannelCategory.NODATA;
        ChannelCategory communicationGroupCategory = ChannelCategory.NODATA;
        ChannelCategory doorsGroupCategory = ChannelCategory.NODATA;
        ChannelCategory safetyGroupCategory = ChannelCategory.NODATA;
        ChannelCategory lightingGroupCategory = ChannelCategory.NODATA;
        ChannelCategory highVoltageGroupCategory = ChannelCategory.NODATA;
        ChannelCategory lowVoltageGroupCategory = ChannelCategory.NODATA;
        ChannelCategory othersGroupCategory = ChannelCategory.NODATA;
        ChannelCategory operationalUnitCategory = ChannelCategory.NODATA;

        List<ChannelCollection> vehiclesChannels = DataUtils.getVehicleChannels(data);
        for (ChannelCollection channels : vehiclesChannels) {
            Channel<?> temp = channels.getByName(Spectrum.HEADCODE);

            // headcode
            if (headcode == null && temp != null && temp.getValue() != null) {
                headcode = temp.getStringValue();
            }
            
            temp = channels.getByName(Spectrum.SERVICE_STATUS);
            if (temp != null && temp.getValue() != null) {
                int priority = getServiceStatusPriority(temp.getStringValue());
                if (priority > serviceStatusPriority) {
                    serviceStatusPriority = priority;
                }
            }
            
            // units
            temp = channels.getByName(Spectrum.UNIT_NUMBER);
            if (temp != null && temp.getValue() != null) {
                String unitNumber = temp.getStringValue();

                temp = channels.getByName(Spectrum.UNIT_POSITION);
                if (temp != null && temp.getValue() != null) {
                    unitPosition = Integer.valueOf(temp.getStringValue());
                }

                unitNumberMap.put(unitPosition, unitNumber);
            }
            
            temp = channels.getByName(Spectrum.UNIT_TYPE);
            if (temp != null && temp.getValue() != null && unitPosition == 1) {
                unitType = temp.getStringValue();
            }
            
            // leading cab            
            activeCab = getLeadingVehicle(channels);
            
            // last update time
            temp = channels.getByName(Spectrum.TIMESTAMP);
            //get timestamp of current record
            long currentTime=0;
            if(null!=temp && null!=temp.getStringValue() && !temp.getStringValue().isEmpty()){
            	currentTime = Long.valueOf(temp.getStringValue());
            }
            if (temp != null && temp.getValue() != null) {
                if (newerTimestamp == null || currentTime > newerTimestamp) {
                    newerTimestamp = currentTime;
                }
                if (olderTimestamp == null || currentTime < olderTimestamp) {
                    olderTimestamp = currentTime;
                }
            }
                        
            // location
            temp = channels.getByName(Spectrum.LOCATION);
            if (temp != null && temp.getValue() != null && (location == null || currentTime > locationTime)) {
                location = temp.getStringValue();
                locationTime = currentTime;
            }
            
            // speed
            temp = channels.getById(getSpeedChannel(channels));
            if (temp != null && temp.getValue() != null && (speed == null || currentTime > speedTime)) {
                speed = Double.valueOf(temp.getStringValue());
                speedCategory = temp.getCategory();
                speedTime = currentTime;
            }

            // Faults and warnings are handled at a UNIT level
            // Check if this Unit has already been processed before
            // Adding to Fault Counter
            temp = channels.getByName(Spectrum.UNIT_ID);
            if (temp != null && temp.getValue() != null) {
                if (!unitIdSet.contains(Integer.valueOf(temp.getStringValue()))) {
                    // P1 NUMBER
                    Channel<?> p1Faults = channels.getByName(NS.FAULT_P1_NUMBER);
                    if (p1Faults != null && p1Faults.getValue() != null) {
                        p1FaultNumber += Integer.valueOf(p1Faults.getStringValue());
                    }
                    // P2 NUMBER
                    Channel<?> p2Faults = channels.getByName(NS.FAULT_P2_NUMBER);
                    if (p2Faults != null && p2Faults.getValue() != null) {
                        p2FaultNumber += Integer.valueOf(p2Faults.getStringValue());
                    }
                    // P3 NUMBER
                    Channel<?> p3Faults = channels.getByName(NS.FAULT_P3_NUMBER);
                    if (p3Faults != null && p3Faults.getValue() != null) {
                        p3FaultNumber += Integer.valueOf(p3Faults.getStringValue());
                    }
                    // P4 NUMBER
                    Channel<?> p4Faults = channels.getByName(NS.FAULT_P4_NUMBER);
                    if (p4Faults != null && p4Faults.getValue() != null) {
                        p4FaultNumber += Integer.valueOf(p4Faults.getStringValue());
                    }
                    // P5 NUMBER
                    Channel<?> p5Faults = channels.getByName(NS.FAULT_P5_NUMBER);
                    if (p5Faults != null && p5Faults.getValue() != null) {
                        p5FaultNumber += Integer.valueOf(p5Faults.getStringValue());
                    }
                    // FAULTS WITH RECOVERY NUMBER
                    Channel<?> faultsWithRecovery = channels.getByName(NS.FAULT_NUMBER_RECOVERY);
                    if (faultsWithRecovery != null && faultsWithRecovery.getValue() != null) {
                    	faultRecoveryNumber += Integer.valueOf(faultsWithRecovery.getStringValue());
                    }
                    // FAULTS NOT ACKNOWLEDGED NUMBER
                    Channel<?> faultsNotAcknowledged = channels.getByName(NS.FAULT_NUMBER_NOT_ACKNOWLEDGED);
                    if (faultsNotAcknowledged != null && faultsNotAcknowledged.getValue() != null) {
                    	faultNotAcknowledgedNumber += Integer.valueOf(faultsNotAcknowledged.getStringValue());
                    }
                    // OPERATIONAL UNITS
                    Channel<?> operationalUnit = channels.getByName(NS.OPERATIONAL_UNITS_COUNT);
                    if (operationalUnit != null && operationalUnit.getValue() != null) {
                        unitStatus = Integer.valueOf(operationalUnit.getStringValue());
                        
                        unitOperationalMap.put(unitPosition, unitStatus);
                        
                        // If any unit in the same formation is in operation then the value of 
                        // hasOperationalUnits should be true
                        hasOperationalUnits = (unitStatus == 1) ? unitStatus : hasOperationalUnits;
                    }
                }

                // unit ids
                unitIdSet.add(Integer.valueOf(temp.getStringValue()));
            }
            
            // Unit Operating
            if (unitStatus > 0) {
                operationalUnitCategory = ChannelCategory.ON;
            } else {
                operationalUnitCategory = ChannelCategory.OFF;
            }

            // Traction
            tractionGroupCategory = getChannelGroupCategory(
                    NS.TRACTION_GROUP, tractionGroupCategory, channels);

            // Brakes
            brakesGroupCategory = getChannelGroupCategory(
                    NS.BRAKES_GROUP, brakesGroupCategory, channels);

            // Air Supply 
            airSupplyGroupCategory = getChannelGroupCategory(
                    NS.AIR_SUPPLY_GROUP, airSupplyGroupCategory, channels);

            // HVAC 
            hvacGroupCategory = getChannelGroupCategory(
                    NS.HVAC_GROUP, hvacGroupCategory, channels);

            // Toilets
            toiletsGroupCategory = getChannelGroupCategory(
                    NS.TOILETS_GROUP, toiletsGroupCategory, channels);

            // Communication
            communicationGroupCategory = getChannelGroupCategory(
                    NS.COMMUNICATION_GROUP, communicationGroupCategory, channels);

            // Doors
            doorsGroupCategory = getChannelGroupCategory(
                    NS.DOORS_GROUP, doorsGroupCategory, channels);
            
            // Safety
            safetyGroupCategory = getChannelGroupCategory(
                    NS.SAFETY_GROUP, safetyGroupCategory, channels);
            
            // Lighting
            lightingGroupCategory = getChannelGroupCategory(
                    NS.LIGHTING_GROUP, lightingGroupCategory, channels);
            
            // High Voltage system
            highVoltageGroupCategory = getChannelGroupCategory(
                    NS.HIGH_VOLTAGE_GROUP, highVoltageGroupCategory, channels);
            
            // Low Voltage system 
            lowVoltageGroupCategory = getChannelGroupCategory(
                    NS.LOW_VOLTAGE_GROUP, lowVoltageGroupCategory, channels);
            
            // Others
            othersGroupCategory = getChannelGroupCategory(
                    NS.OTHER_GROUP, othersGroupCategory, channels);
        }
        
        ChannelCollection channels = new ChannelCollection();
        
        channels.put(new ChannelString(Spectrum.HEADCODE, headcode, getHeadcodeCategory(serviceStatusPriority)));
        channels.put(new ChannelLong(Spectrum.TIMESTAMP, newerTimestamp, getTimestampCategory(olderTimestamp, newerTimestamp)));
        channels.put(new ChannelString(Spectrum.UNIT_TYPE, unitType, ChannelCategory.NORMAL));
        channels.put(new ChannelString(NS.ACTIVE_CAB, activeCab));
        channels.put(new ChannelString(Spectrum.LOCATION, location, ChannelCategory.NORMAL));
        channels.put(new ChannelDouble(NS.SPEED_VIEW, speed, speedCategory));
        channels.put(new ChannelInt(NS.FAULT_P1_NUMBER, p1FaultNumber, getEventNumberCategory(p1FaultNumber)));
        channels.put(new ChannelInt(NS.FAULT_P2_NUMBER, p2FaultNumber, getEventNumberCategory(p2FaultNumber)));
        channels.put(new ChannelInt(NS.FAULT_P3_NUMBER, p3FaultNumber, getEventNumberCategory(p3FaultNumber)));
        channels.put(new ChannelInt(NS.FAULT_P4_NUMBER, p4FaultNumber, getEventNumberCategory(p4FaultNumber)));
        channels.put(new ChannelInt(NS.FAULT_P5_NUMBER, p5FaultNumber, getEventNumberCategory(p5FaultNumber)));
        channels.put(new ChannelInt(NS.FAULT_NUMBER_RECOVERY, faultRecoveryNumber, getEventNumberCategory(faultRecoveryNumber)));
        channels.put(new ChannelInt(NS.FAULT_NUMBER_NOT_ACKNOWLEDGED, faultNotAcknowledgedNumber, getEventNumberCategory(faultNotAcknowledgedNumber)));
        channels.put(new ChannelInt(NS.OPERATIONAL_UNITS_COUNT, unitStatus, operationalUnitCategory));
        channels.put(new ChannelString(NS.TRACTION_GROUP, null, tractionGroupCategory));
        channels.put(new ChannelString(NS.BRAKES_GROUP, null, brakesGroupCategory));
        channels.put(new ChannelString(NS.DOORS_GROUP, null, doorsGroupCategory));
        channels.put(new ChannelString(NS.SAFETY_GROUP, null, safetyGroupCategory));
        channels.put(new ChannelString(NS.HVAC_GROUP, null, hvacGroupCategory));
        channels.put(new ChannelString(NS.LIGHTING_GROUP, null, lightingGroupCategory));
        channels.put(new ChannelString(NS.COMMUNICATION_GROUP, null, communicationGroupCategory));
        channels.put(new ChannelString(NS.TOILETS_GROUP, null, toiletsGroupCategory));
        channels.put(new ChannelString(NS.HIGH_VOLTAGE_GROUP, null, highVoltageGroupCategory));
        channels.put(new ChannelString(NS.LOW_VOLTAGE_GROUP, null, lowVoltageGroupCategory));
        channels.put(new ChannelString(NS.AIR_SUPPLY_GROUP, null, airSupplyGroupCategory));
        channels.put(new ChannelString(NS.OTHER_GROUP, null, othersGroupCategory));
        
        
        int hasP1Faults = p1FaultNumber > 0 ? 1 : 0;
        int hasP2Faults = p2FaultNumber > 0 ? 1 : 0;
        int hasP3Faults = p3FaultNumber > 0 ? 1 : 0;
        int hasP4Faults = p4FaultNumber > 0 ? 1 : 0;
        int hasP5Faults = p5FaultNumber > 0 ? 1 : 0;
        int hasFaultWithRecovery = faultRecoveryNumber > 0 ? 1 : 0;
        int hasFaultNotAcknowledged = faultNotAcknowledgedNumber > 0 ? 1 : 0;

        
        channels.put(new ChannelInt(NS.HAS_P1_FAULTS, hasP1Faults, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_P2_FAULTS, hasP2Faults, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_P3_FAULTS, hasP3Faults, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_P4_FAULTS, hasP4Faults, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_P5_FAULTS, hasP5Faults, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_FAULTS_RECOVERY, hasFaultWithRecovery, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_FAULTS_UNACKNOWLEDGED, hasFaultNotAcknowledged, ChannelCategory.NORMAL));
        channels.put(new ChannelInt(NS.HAS_OPERATIONAL_UNITS, hasOperationalUnits, ChannelCategory.NORMAL));

        String formattedDate = null;
        if (newerTimestamp != null) {
            DateFormat format = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
            Date date = new Date(newerTimestamp);
            formattedDate = format.format(date);
        }
        
        channels.put(new ChannelString(NS.FLEET_SUMMARY_DATE, formattedDate, ChannelCategory.NORMAL));
        

        StringBuilder formation = new StringBuilder();
        for (Integer unitId : unitIdSet) {
            if (formation.length() > 0) {
                formation.append(",");
            }
            formation.append(unitId);
        }
        channels.put(new ChannelString(Spectrum.FORMATION,
                formation.toString(), ChannelCategory.NORMAL));
        
        // Create channels for Unit1/2/3
        SortedSet<Integer> positions = new TreeSet<Integer>(
                unitNumberMap.keySet());
        List<String> unitNumberList = new ArrayList<String>();
        List<Integer> unitStatusList = new ArrayList<Integer>();
        for (Integer currentPosition : positions) {
            unitNumberList.add(unitNumberMap.get(currentPosition));
            unitStatusList.add(unitOperationalMap.get(currentPosition));
        }

        String[] unitNumbers = unitNumberList.toArray(new String[0]);
        Integer[] unitStatusGroup = unitStatusList.toArray(new Integer[0]);

        String unitNumber = unitNumbers.length > 0 ? unitNumbers[0] : null;
        ChannelCategory operationalUnit1Category = ChannelCategory.NORMAL;
        Integer Unit1Status = (unitStatusGroup.length > 0) ? unitStatusGroup[0] : null;
        if (Unit1Status != null) {
        	operationalUnit1Category = (Unit1Status > 0) ? ChannelCategory.ON : ChannelCategory.OFF;
        }
        channels.put(new ChannelString(Spectrum.UNIT_NUMBER1, unitNumber, operationalUnit1Category));
        channels.put(new ChannelInt(NS.IS_UNIT1_IN_OPERATION, Unit1Status, ChannelCategory.NORMAL));

        unitNumber = (unitNumbers.length > 1 ? unitNumbers[1] : null);
        Integer Unit2Status = (unitStatusGroup.length > 1) ? unitStatusGroup[1] : null;
        ChannelCategory operationalUnit2Category = ChannelCategory.NORMAL;
        if (Unit2Status != null) {
        	operationalUnit2Category = (Unit2Status > 0) ? ChannelCategory.ON : ChannelCategory.OFF;
        }
        channels.put(new ChannelString(Spectrum.UNIT_NUMBER2, unitNumber, operationalUnit2Category));
        channels.put(new ChannelInt(NS.IS_UNIT2_IN_OPERATION, Unit2Status, ChannelCategory.NORMAL));

        unitNumber = (unitNumbers.length > 2 ? unitNumbers[2] : null);
        Integer Unit3Status = (unitStatusGroup.length > 2) ? unitStatusGroup[2] : null;
        ChannelCategory operationalUnit3Category = ChannelCategory.NORMAL;
        if (Unit3Status != null) {
        	operationalUnit3Category = (Unit3Status > 0) ? ChannelCategory.ON : ChannelCategory.OFF;
        }
        channels.put(new ChannelString(Spectrum.UNIT_NUMBER3, unitNumber, operationalUnit3Category));
        channels.put(new ChannelInt(NS.IS_UNIT3_IN_OPERATION, Unit3Status, ChannelCategory.NORMAL));
        
        return new DataSetImpl(id, channels);
    }

    private int getServiceStatusPriority(String serviceStatus) {
        if (NS.OUT_OF_SERVICE.equalsIgnoreCase(serviceStatus)) {
            return 1;
        } else if (NS.READY_FOR_SERVICE.equalsIgnoreCase(serviceStatus)) {
            return 2;
        } else if (NS.IN_SERVICE.equalsIgnoreCase(serviceStatus)) {
            return 3;
        }
        return 0;
    }
    
    private ChannelCategory getHeadcodeCategory(int priority) {

        if (priority == 1) {
            return ChannelCategory.NORMAL;
        } else if (priority == 2) {
            return ChannelCategory.OFF;
        } else if (priority == 3) {
            return ChannelCategory.ON;
        }
        return ChannelCategory.NODATA;
    }
    
    private ChannelCategory getTimestampCategory(Long olderTimestamp, Long newerTimestamp) {

        if (newerTimestamp == null) {
            return ChannelCategory.NODATA;
        }

        if (olderTimestamp != null) {
            long timeDiff = Math.abs(System.currentTimeMillis() - newerTimestamp);
            timeDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiff);
            if (timeDiff >= 30) {
                return ChannelCategory.FAULT;
            }

            timeDiff = Math.abs(newerTimestamp - olderTimestamp);
            timeDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiff);
            if (timeDiff >= 10) {
                return ChannelCategory.WARNING;
            }
        }

        return ChannelCategory.NORMAL;
    }
    
    private ChannelCategory getEventNumberCategory(int eventNumber) {
        if (eventNumber == 0) {
            return ChannelCategory.NORMAL;
        }

        return ChannelCategory.FAULT;
    }
    
    private ChannelCategory getChannelGroupCategory(String name,
            ChannelCategory currentCategory, ChannelCollection channels) {

        ChannelGroupConfig groupConf = channelConfiguration.getGroup(name);
        if (groupConf != null) {
            for (ChannelConfig channelConfig : groupConf.getChannelConfigList()) {
                Channel<?> channel = channels.getById(channelConfig.getId());
                if (channel != null && channel.getValue() != null) {
                    ChannelCategory newCategory = channel.getCategory();

                    if (channelStatusUtil
                            .getChannelCategoryPriority(newCategory) > channelStatusUtil
                            .getChannelCategoryPriority(currentCategory)) {
                        currentCategory = newCategory;
                    }
                }
            }
        }

        return currentCategory;
    }
    
    // Scan source event channel data for event latest timestamps, replace timestamp channel in channel data if later value is found
    protected void overrideLatestTimestamp(List<DataSet> data, List<SourceEventChannelData> eventChannelData, List<DataSet> gpsData) {
    	Map<Integer, Long> eventLatestTimestamps = new HashMap<Integer, Long>();
    	
    	for(SourceEventChannelData ecv : eventChannelData) {
    		if (!eventLatestTimestamps.containsKey(ecv.getSourceId())) {
            	eventLatestTimestamps.put(ecv.getSourceId(), ecv.getLatestTimestamp().getTime());
            } else {
            	if(eventLatestTimestamps.get(ecv.getSourceId()) < ecv.getLatestTimestamp().getTime()) {
            		eventLatestTimestamps.put(ecv.getSourceId(), ecv.getLatestTimestamp().getTime());
            	}
            }
    	}
    	
    	for (DataSet ds : data) {            
            // replace timestamp channel value if the event channels have a more recent timestamp
            if (eventLatestTimestamps.containsKey(ds.getSourceId())) {
            	if ((Long) ds.getChannels().getByName(Spectrum.TIMESTAMP).getValue() < eventLatestTimestamps.get(ds.getSourceId()) ) {
            		ds.getChannels().getByName(Spectrum.TIMESTAMP, ChannelLong.class).setValue(eventLatestTimestamps.get(ds.getSourceId()));
            	}
            }
        }
    	
        if (gpsData != null) {
            for (DataSet ds : data) {
                for (DataSet gpsDs : gpsData) {
                    if (ds.getSourceId().equals(gpsDs.getSourceId())) {
                        if (gpsDs.getTimestamp() != null) {
                            if (gpsDs.getTimestamp() > (Long) ds.getChannels().getByName(Spectrum.TIMESTAMP).getValue()) {
                                ds.getChannels().getByName(Spectrum.TIMESTAMP, ChannelLong.class).setValue(gpsDs.getTimestamp());
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected abstract int getSpeedChannel(ChannelCollection channels);
    protected abstract String getLeadingVehicle(ChannelCollection channels);

}
