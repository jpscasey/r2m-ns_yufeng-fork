package com.nexala.spectrum.ns.providers;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.ns.db.dao.ChannelValueGpsDao;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.validation.ChannelValidator;

public class GpsDataProviderNS implements GpsDataProvider {

    protected EventDao eventDao;
    
    @Inject
    private ChannelValueGpsDao channelValueGpsDao;
    
    @Inject
    private ChannelValidator validationEngine;
    
    @Override
    public List<DataSet> getLiveGpsData() {
        List<DataSet> data = channelValueGpsDao.getLiveGpsData();
        validate(data);
        return data;
    }
    
    @Override
    public List<DataSet> getHistoricGpsData(long timestamp) {
        List<DataSet> data = channelValueGpsDao.getHistoricGpsData(timestamp);
        validate(data);
        return data;
    }
    
    @Override
    public List<Event> getLiveByUnit(Integer unitId) {
        return eventDao.liveByUnit(unitId);
    }
    
    private void validate(List<DataSet> data) {
        validationEngine.validate(data);
    }
}