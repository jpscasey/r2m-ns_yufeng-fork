package com.nexala.spectrum.ns.providers;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.rest.data.AbstractGroupProvider;
import com.nexala.spectrum.rest.data.ChannelLookupProvider;
import com.nexala.spectrum.validation.ChannelStatusUtil;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;

public class GroupProviderNS extends AbstractGroupProvider {

    @Inject
    private ChannelConfiguration configuration;

    @Inject
    private ChannelStatusUtil statusUtil;

    @Inject
    private ChannelDataConfiguration channelDataConfiguration;

    @Inject
    private ChannelLookupProvider channelLookupProvider;

    @Override
    public ChannelConfiguration getChannelConfiguration() {
        return configuration;
    }

    @Override
    public ChannelStatusUtil getChannelStatusUtil() {
        return statusUtil;
    }

    @Override
    public ChannelDataConfiguration getChannelDataConfiguration() {
        return channelDataConfiguration;
    }

    @Override
    public ChannelLookupProvider getChannelLookupProvider() {
        return channelLookupProvider;
    }
}
