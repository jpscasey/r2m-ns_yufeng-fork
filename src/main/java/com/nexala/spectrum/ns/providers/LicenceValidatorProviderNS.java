package com.nexala.spectrum.ns.providers;

import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LicenceResult;
import com.nexala.spectrum.licensing.LicenceValidator;
import com.nexala.spectrum.licensing.LicenceValidatorProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.UnitDesc;

// TODO Default Spectrum-base impl
public class LicenceValidatorProviderNS implements LicenceValidatorProvider {

    @Override
    public Map<Class<?>, LicenceValidator<?>> getValidators() {

        Map<Class<?>, LicenceValidator<?>> result = new HashMap<Class<?>, LicenceValidator<?>>();

        result.put(UnitDesc.class, new LicenceValidator<UnitDesc>() {
            @Override
            public LicenceResult getLicenceResult(UnitDesc obj, Licence licence) {
                return new LicenceResult(true, "");
            }
        });

        result.put(Event.class, new LicenceValidator<Event>() {
            @Override
            public LicenceResult getLicenceResult(Event obj, Licence licence) {
                return new LicenceResult(true, "");
            }
        });

        result.put(DataSet.class, new LicenceValidator<DataSet>() {
            @Override
            public LicenceResult getLicenceResult(DataSet obj, Licence licence) {
                return new LicenceResult(true, "");
            }
        });

        result.put(DownloadInfo.class, new LicenceValidator<DownloadInfo>() {
            @Override
            public LicenceResult getLicenceResult(DownloadInfo obj,
                    Licence licence) {
                return new LicenceResult(true, "");
            }
        });

        result.put(Unit.class, new LicenceValidator<Unit>() {
            @Override
            public LicenceResult getLicenceResult(Unit obj, Licence licence) {
                return new LicenceResult(true, "");
            }
        });

        return result;
    }
}
