package com.nexala.spectrum.ns.providers;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.Location;
import com.nexala.spectrum.db.dao.LocationDao;
import com.nexala.spectrum.rest.data.LocationProvider;

public class LocationProviderNS implements LocationProvider{
	
	@Inject
	private LocationDao locationDao;

	@Override
	public Location getLocationByCode(String code) {
		return locationDao.getLocation(code);
	}

	@Override
	public Location getVehicleCurrentLocation(Integer vehicleId) {
		// NS does not provide vehicle-level data
		return null;
	}

}
