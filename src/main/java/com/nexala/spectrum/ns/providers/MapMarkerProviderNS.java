package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.mapping.Coordinate;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.rest.data.DetectorProvider;
import com.nexala.spectrum.view.conf.maps.DefaultMapMarkerProvider;
import com.nexala.spectrum.view.conf.maps.DetectorSiteMarker;

public class MapMarkerProviderNS extends DefaultMapMarkerProvider {
    
    @Inject
    protected Map<String, DetectorProvider> detectorProviders;
    
    @Override
    protected List<?> getExtraMarkerData(Coordinate upperLeft, Coordinate lowerRight, Object data,
            String markerType) {
        
        List<DetectorSiteMarker> result = new ArrayList<DetectorSiteMarker>();
        
        if(NS.MAP_DETECTOR_MARKER.equals(markerType)){
            for (String fleetId : detectorProviders.keySet()) {
                result.addAll(detectorProviders.get(fleetId).getDetectorData());
            }        
        } 
        
        return result;
    }
}
