package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.rest.data.AbstractMapProvider;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;

public abstract class MapProviderNS extends AbstractMapProvider {
	@Inject
    private ChannelDataProvider dataProvider;
	
	@Inject
    private GpsDataProvider gpsDataProvider;

    @Inject
    private UnitDao unitDao;

    @Override
    public List<DataSet> getLiveData() {
        List<DataSet> data = dataProvider.getLiveChannelData();
        List<DataSet> gpsData = gpsDataProvider.getLiveGpsData();
        DataUtils.mergeDatasets(data, gpsData);
        
        return getMapDataList(data, null);
    }
    
    @Override
    public List<DataSet> getHistoricFleetData(long timestamp) {
        List<DataSet> data = dataProvider.getHistoricFleetData(timestamp);
        List<DataSet> gpsData = gpsDataProvider.getHistoricGpsData(timestamp);
        DataUtils.mergeDatasets(data, gpsData);
        
        return getMapDataList(data, timestamp);
    }
    
    @Override
    public DataSet unit(int unitId, Long timestamp) {
        DataSet unitDataSet = getDataSet(unitId, dataProvider.getUnitData(unitId, timestamp, 0));

        List<Unit> units = unitDao.findUnitFormationByUnitId(unitId);

        if (units.size() > 1) {
            StringBuffer formation = new StringBuffer();
            StringBuffer fleetFormationID = new StringBuffer();
            for (Unit unit : units) {
                if (formation.length() > 0) {
                    formation.append(", ");
                    fleetFormationID.append(" ");
                }
                formation.append(unit.getUnitNumber());
                fleetFormationID.append(unit.getId());
            }
            unitDataSet.getChannels().getByName(Spectrum.FORMATION, ChannelString.class).setValue(formation.toString());
            unitDataSet.getChannels().getByName(Spectrum.FLEET_FORMATION_ID, ChannelString.class)
                    .setValue(fleetFormationID.toString());
        }

        return unitDataSet;
    }
    
    public List<DataSet> getMapDataList(List<DataSet> fleetData, Long timestamp) {
        List<DataSet> mapDataList = new ArrayList<DataSet>();

        Map<Integer, List<DataSet>> dataSetMap = DataUtils.getFormation(fleetData);

        for (Integer id : dataSetMap.keySet()) {
            if (isActive(dataSetMap.get(id), timestamp)) {
                mapDataList.add(getDataSet(id, dataSetMap.get(id)));
            }
        }

        return mapDataList;
    }
    
    // method which will be overridden by each fleet map provider
    // in order to make available a different channel data set for each fleet
    protected abstract DataSet getDataSet(int id, List<DataSet> data);

    protected boolean isValid(Channel<?> ch) {
        return ch != null && ch.getValue() != null ? true : false;
    }
    
    protected String getTrainIcon(int unitStatus, int p1s, int p2s, int p3s, int p4s, int p5s) {
        if (unitStatus == 0) {
            return NS.TRAIN_ICON_INACTIVE;
        } else if (p1s > 0) {
            return NS.TRAIN_ICON_P1;
        } else if (p2s > 0) {
            return NS.TRAIN_ICON_P2;
        } else if (p3s > 0) {
            return NS.TRAIN_ICON_P3;
        } else if (p4s > 0) {
            return NS.TRAIN_ICON_P4;
        }
        return NS.TRAIN_ICON_P5;
    }
    
    protected String getMapFilterGroups(int p1s, int p2s, int p3s, int p4s, int p5s, int faultsRecovery, int faultsUnack, int unitStatus) {
        StringBuilder mapFilterGroups = new StringBuilder();
        String comma = ",";
        if (unitStatus > 0) {
            mapFilterGroups.append(NS.HAS_OPERATIONAL_UNITS).append(comma);
        } if (p1s > 0) {
            mapFilterGroups.append(NS.HAS_P1_FAULTS).append(comma);
        } if (p2s > 0) {
            mapFilterGroups.append(NS.HAS_P2_FAULTS).append(comma);
        } if (p3s > 0) {
            mapFilterGroups.append(NS.HAS_P3_FAULTS).append(comma);
        } if (p4s > 0) {
            mapFilterGroups.append(NS.HAS_P4_FAULTS).append(comma);
        } if (p5s > 0) {
            mapFilterGroups.append(NS.HAS_P5_FAULTS).append(comma);
        } if (faultsRecovery > 0) {
            mapFilterGroups.append(NS.HAS_FAULTS_RECOVERY).append(comma);
        } if (faultsUnack > 0) {
            mapFilterGroups.append(NS.HAS_FAULTS_UNACKNOWLEDGED).append(comma);
        }
        
        if (mapFilterGroups.length() > 0) {
            mapFilterGroups.setLength(mapFilterGroups.length() - 1);
        } else {
            mapFilterGroups.append(NS.HAS_NO_FAULTS);
        }
        
        return mapFilterGroups.toString();
    }
    
    protected String getMapFilterTextGroups(String unitType) {
        StringBuilder mapFilterTextGroups = new StringBuilder();
        String comma = ",";
        if (unitType != null) {
        	mapFilterTextGroups.append(Spectrum.UNIT_TYPE).append(":").append(unitType).append(comma);
        }
        
        if (mapFilterTextGroups.length() > 0) {
        	mapFilterTextGroups.setLength(mapFilterTextGroups.length() - 1);
        }
        return mapFilterTextGroups.toString();
    }
}
