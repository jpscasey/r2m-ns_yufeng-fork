package com.nexala.spectrum.ns.providers;

import com.nexala.spectrum.licensing.LoginUser;
import com.nexala.spectrum.ns.db.beans.MaximoServiceRequestResponse;
import com.nexala.spectrum.rest.data.beans.Event;

public interface MaximoServiceRequestProvider {

    MaximoServiceRequestResponse createServiceRequest(Event event, long rowVersion, LoginUser username);
    
    void createAutomatedServiceRequest(String faultCode, Integer unitId, Long eventId, String username);
}
