package com.nexala.spectrum.ns.providers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.LocaleUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultMetaExtraField;
import com.nexala.spectrum.db.dao.EventCommentDao;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.db.dao.FaultMetaDao;
import com.nexala.spectrum.db.dao.FaultMetaExtraFieldDao;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.LoginUser;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.db.beans.FirstEventRequestService;
import com.nexala.spectrum.ns.db.beans.MaximoServiceRequestResponse;
import com.nexala.spectrum.ns.db.dao.MaximoServiceRequestDao;
import com.nexala.spectrum.rest.data.beans.Event;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

public class MaximoServiceRequestProviderNS implements MaximoServiceRequestProvider {

    private static final Logger LOGGER = Logger.getLogger(MaximoServiceRequestProviderNS.class);
    
    private static final int LONG_DESCRIPTION_LIMIT = 32000;
    
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); //2016-12-17T09:30:47.001Z
    
    private static final DateFormat DATE_FORMAT_SERVICE_REQUEST = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); //01/08/2018 12:19:16
    
    private ApplicationConfiguration appConf;
    
    private EventDao eventDao;
    
    private EventCommentDao eventCommentDao;
    
    private MaximoServiceRequestDao serviceRequestDao;
        
    private HttpClient httpClient;
    
    private HttpPost httpPost;
    
    private ResponseHandler<String> responseHandler;
    
    private boolean longDescriptionLimitReached;
    
    private LoginUser userLogin;
    
    private FaultMetaExtraFieldDao fmemDao;
    
    private final FaultMetaDao fmDao;
    
    @Inject
    public MaximoServiceRequestProviderNS(EventDao eventDao, EventCommentDao eventCommentDao,
            MaximoServiceRequestDao serviceRequestDao, ApplicationConfiguration appConf, FaultMetaExtraFieldDao fmemDao, FaultMetaDao fmDao) {
        this.appConf = appConf;
        this.eventDao = eventDao;
        this.eventCommentDao = eventCommentDao;
        this.serviceRequestDao = serviceRequestDao;
        this.httpClient = new DefaultHttpClient();
        this.httpPost = new HttpPost(appConf.get().getString("r2m.maximo.createServiceRequestUrl"));
        this.httpPost.addHeader("content-type", "application/json");
        this.responseHandler = new BasicResponseHandler();
        this.fmemDao = fmemDao;
        this.fmDao = fmDao;
    }
    
    // for unit testing
    public MaximoServiceRequestProviderNS(EventDao eventDao, EventCommentDao eventCommentDao,
    		MaximoServiceRequestDao serviceRequestDao, HttpClient httpClient, HttpPost httpPost,
    		ResponseHandler<String> responseHandler, ApplicationConfiguration appConf, FaultMetaExtraFieldDao fmemDao, FaultMetaDao fmDao) {
        this.appConf = appConf;
    	this.eventDao = eventDao;
        this.eventCommentDao = eventCommentDao;
        this.serviceRequestDao = serviceRequestDao;
        this.httpClient = httpClient;
        this.httpPost = httpPost;
        this.responseHandler = responseHandler;
        this.fmemDao = fmemDao;
        this.fmDao = fmDao;
    }
    
    @Override
    public MaximoServiceRequestResponse createServiceRequest(Event event, long rowVersion, LoginUser user) {
        Integer eventId = (Integer) event.getField(Spectrum.EVENT_ID);
        
        // Optimistic lock
        eventDao.updateLastUpdateTime(eventId, rowVersion);
        
        longDescriptionLimitReached = false;
        
        userLogin=user;
        
        // collect parameters
        JSONObject parameters = buildJsonRequestFromEvent(event);
        
        // Send HTTP request
        MaximoServiceRequestResponse serviceRequestResponse = sendCreateServiceRequestToMaximo(parameters);
        String serviceRequestId = serviceRequestResponse.getServiceRequestId();
        
        // Update DB with response
        boolean createdByRules = false;
        serviceRequestDao.saveServiceRequest(eventId.longValue(), serviceRequestId, createdByRules);
        
        // build the response to return to the client
        serviceRequestResponse.setMaxLimitReached(longDescriptionLimitReached);
        serviceRequestResponse.setServiceRequestId(serviceRequestId);
        
        return serviceRequestResponse;
    }

    private JSONObject buildJsonRequestFromEvent(Event event) {
        JSONObject result = new JSONObject();
        Locale locale = null;
        String username = null;
        if(event.getField("automatedRequest") != null && ((Boolean)event.getField("automatedRequest"))) {
            locale = Locale.getDefault();
            username = "R2M";
        } else {
            locale =  LocaleUtils.toLocale(userLogin.getLocale());
            username = userLogin.getLogin();
        }
        result.put("username", username);
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(locale);

        if (event.getField(Spectrum.EVENT_DESCRIPTION) != null) {
            result.put("description", event.getField(Spectrum.EVENT_DESCRIPTION));
        }
        if (event.getField(Spectrum.LONG_DESCRIPTION) != null) {
            StringBuilder longDescBuilder = new StringBuilder(event.getField(Spectrum.LONG_DESCRIPTION).toString());
            
            if (event.getField("comments") != null) {
                for (EventComment comment : (List<EventComment>) event.getField("comments")) {
                    longDescBuilder.append('\n');
                    longDescBuilder.append(DATE_FORMAT_SERVICE_REQUEST.format(new Date(comment.getTimestamp())));
                    longDescBuilder.append(" - ");
                    longDescBuilder.append(comment.getUserName());
                    longDescBuilder.append(" - ");
                    extractAction(longDescBuilder, bundle, comment); 
                    longDescBuilder.append(" - ");
                    longDescBuilder.append(comment.getText());
                }
            }
            
            String longDescriptionField = longDescBuilder.toString();
            
            result.put("longDescription", longDescriptionField);
        }
        if (event.getField(NS.EVENT_SERVICE_REQUEST_PRIORITY) != null) {
            result.put("vkbPriority", event.getField(NS.EVENT_SERVICE_REQUEST_PRIORITY));
        }
        if (event.getField(NS.EVENT_SYSTEM_CODE) != null) {
            result.put("classification", event.getField(NS.EVENT_SYSTEM_CODE));
        }
        if (event.getField(NS.EVENT_MAXIMO_FAULT_CODE) != null) {
            result.put("diagnosticalCode", event.getField(NS.EVENT_MAXIMO_FAULT_CODE));
        }
        if (event.getField(NS.EVENT_CONTROL_ROOM_PRIORITY) != null) {
            result.put("mbnUrgency", event.getField(NS.EVENT_CONTROL_ROOM_PRIORITY));
        }
        if (event.getField(Spectrum.EVENT_LOCATION) != null) {
            result.put("positionCode", event.getField(Spectrum.EVENT_LOCATION));
        }
        if (event.getField(NS.EVENT_QUALITY_PROFILE) != null) {
            String qProfile = event.getField(NS.EVENT_QUALITY_PROFILE).toString();
            result.put("qProfileCode", qProfile.substring(qProfile.indexOf('-') + 1));
        }
        if (event.getField(Spectrum.EVENT_HEADCODE) != null) {
            result.put("trainNumber", event.getField(Spectrum.EVENT_HEADCODE));
        }
        if (event.getField(NS.EVENT_SAFEY_PRIORITY) != null) {
            result.put("safetyDisruptionLevel", event.getField(NS.EVENT_SAFEY_PRIORITY));
        }
        if (event.getField(NS.EVENT_MAINTENANCE_FUNCTION) != null) {
            result.put("workorderLocationType", event.getField(NS.EVENT_MAINTENANCE_FUNCTION));
        }
        if (event.getField(Spectrum.EVENT_CREATE_TIME) != null) {
        	DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            result.put("notificationTimestamp", DATE_FORMAT.format(new Date(Long.valueOf(event.getField(Spectrum.EVENT_CREATE_TIME).toString()))));
        }
        if (event.getField(Spectrum.EVENT_UNIT_NUMBER) != null) {
            result.put("materialNumber", event.getField(Spectrum.EVENT_UNIT_NUMBER));
        }

        return result;
    }
    
    private void extractAction(StringBuilder longDescBuilder, ResourceBundleUTF8 bundle, EventComment comment) {
		
		boolean isCommentAvailable=comment.getAction().equals("ack") || comment.getAction().equals("deac") || comment.getAction().equals("req") || comment.getAction().equals("com");
		
		if(null!= bundle && isCommentAvailable) {
			longDescBuilder.append(bundle.getString(comment.getAction()));
		}
	}
    private MaximoServiceRequestResponse sendCreateServiceRequestToMaximo(JSONObject parameters) {
    	MaximoServiceRequestResponse serviceRequestResponse = new MaximoServiceRequestResponse();
    	String serviceRequestId = null;
        try {
        	String jsonString = parameters.toJSONString();
        	
        	// get the content of longDescription field with regex in order to see if the length exceeds         	
        	Pattern p = Pattern.compile("\\{\\\"longDescription\\\":\\\"(.*?)\\\",");
        	Matcher m = p.matcher(jsonString);
        	if (m.find()) {
        		String longDescField = m.group(1);
        		jsonString=jsonString.replace("\\/","/");    		
        		// check if long description field reaches the limit of 32000 characters, if true
                // then get only the last 32000 characters of the message and save in a flag that the limit has been reached
                if (longDescField.length() > LONG_DESCRIPTION_LIMIT) {
                	longDescriptionLimitReached = true;
                	longDescField = longDescField.substring(longDescField.length() - LONG_DESCRIPTION_LIMIT);
                	
                	jsonString = jsonString.replace(m.group(1), longDescField);
                	
                	// after the previous statement jsonString should have the correct length and with valid json code
                }
        	}
        	
            httpPost.setEntity(new StringEntity(jsonString));
            httpPost.addHeader("Content-Type", "application/json");
            
            // add Basic Authentication for ESB
            String ba_User = appConf.get().getString("r2m.maximo.esbBasicAuthUser");
            String ba_Pass = appConf.get().getString("r2m.maximo.esbBasicAuthPassword");
            String encoding = Base64.getEncoder().encodeToString((ba_User + ":" + ba_Pass).getBytes());
            httpPost.addHeader("Authorization", "Basic " + encoding);
            
            HttpResponse response = httpClient.execute(httpPost);
            
            String responseString = responseHandler.handleResponse(response);
            
            // Uncomment below lines (1 at a time) to test OK or ERR responses respectively
            //responseString = "{\"statusCode\": \"OK\",\"statusMessageCode\": \"ESB-RTM-010\",\"statusMessageText\": \"ServiceRequest data successfully processed\",\"statusMessageDetails\": \"\",\"statusMessageTimeStamp\": \"2016-12-17T09:30:47.001Z\",\"notificationNumber\": \"1221\"}";
            //responseString = "{\"statusCode\": \"ERR\",\"statusMessageCode\": \"ESB-RTM-003\",\"statusMessageText\": \"Message validation failed\",\"statusMessageDetails\": \"<details>\",\"statusMessageTimeStamp\": \"2016-12-17T09:30:47.001Z\",\"notificationNumber\": \"\"}";
            
            JSONObject jsonObject = (JSONObject) JSONValue.parse(responseString);

            String statusCode = jsonObject.get("statusCode").toString();
            String statusMessageCode = "";
            String statusMessageText = "";
            String statusMessageDescription = "";
            
            serviceRequestResponse.setStatusCode(statusCode);
            
            if (jsonObject.containsKey("statusMessageCode")) {
            	statusMessageCode = jsonObject.get("statusMessageCode").toString();
            }
            if (jsonObject.containsKey("statusMessageText")) {
            	statusMessageText = jsonObject.get("statusMessageText").toString();
            }
            if (jsonObject.containsKey("statusMessageDescription")) {
            	statusMessageDescription = jsonObject.get("statusMessageDescription").toString();
            }
            
            if (statusCode.equals("OK")) {
                serviceRequestId = jsonObject.get("notificationNumber").toString();
            } else if (statusCode.equals("ERR")){            	
            	serviceRequestId = jsonObject.get("notificationNumber").toString();
            	serviceRequestResponse.setStatusMessageCode(statusMessageCode);
            	serviceRequestResponse.setStatusMessageText(statusMessageText);
            	serviceRequestResponse.setStatusMessageDescription(statusMessageDescription);
            } else {
                StringBuilder statusFull = new StringBuilder();
                if (!statusMessageCode.isEmpty()) {
                    statusFull.append(statusMessageCode + ", ");
                }
                if (!statusMessageText.isEmpty()) {
                    statusFull.append(statusMessageText + ", ");
                }
                if (!statusMessageDescription.isEmpty()) {
                    statusFull.append(statusMessageDescription);
                }
                String errorMsg = statusFull.toString();
                LOGGER.error(errorMsg);
                throw new RuntimeException(errorMsg);
            }
            
        } catch (IOException e) {
            LOGGER.error("Error sending Maximo CreateServiceRequest request", e);
            throw new RuntimeException(e);
        }

        serviceRequestResponse.setServiceRequestId(serviceRequestId);
        return serviceRequestResponse;
    }

    @Override
    public void createAutomatedServiceRequest(String faultCode, Integer unitId, Long eventId, String username) {
    	
    	// check if there's already an active request in database with the same pair
    	// faultcode and unitid then don't send another Service Request but add the event
    	// to the event group of the active service request
		
		List<FirstEventRequestService> firtEventRequestServiceList = serviceRequestDao.findEventWithServiceRequest(faultCode, unitId);

		if (firtEventRequestServiceList.size() <= 0) {
	        Event event = getEventById(eventId);
	        event.setField("comments", eventCommentDao.getComments(eventId.intValue()));
	        event.setField("automatedRequest", true);
	        // collect parameters
	        JSONObject parameters = buildJsonRequestFromEvent(event);
	        
	        // Send HTTP request
	        MaximoServiceRequestResponse serviceRequestResponse = sendCreateServiceRequestToMaximo(parameters);
	        String serviceRequestId = serviceRequestResponse.getServiceRequestId();
	        
	        // Update DB with response
	        boolean createdByRules = true;
	        serviceRequestDao.saveServiceRequest(eventId, serviceRequestId, createdByRules);
		} else {
    		// get the first event in database which sent the service request
    		FirstEventRequestService oldEvent = firtEventRequestServiceList.get(0);
    		
    		Integer serviceRequestId = oldEvent.getServiceRequestId();
    		
    		// update the current event inserting the existing service request id previously created
    		serviceRequestDao.updateFaultServiceRequest(eventId, serviceRequestId);
    		
    		Integer groupId = oldEvent.getFaultGroupId();
    		
    		// if a group of faults doesn't already exist for the same service request 
    		// create a new group adding to the group the new event and the first one
			// otherwise add the current fault to the existing group of faults

    		if (groupId == 0) {
    			serviceRequestDao.createServiceRequestFaultGroup(oldEvent.getFaultId(), eventId, username);
    		} else {
    			serviceRequestDao.addFaultToGroup(groupId, eventId);   			
    		}
    	}
    }
    
    private Event getEventById(Long eventId) {
        Event event = eventDao.getEventById(eventId);
        Integer faultMetaId = (Integer) event.getField(Spectrum.EVENT_FAULT_META);
        FaultMeta fm = fmDao.findById(faultMetaId);
        if(fm != null) {
            event.setField("longDescription", fm.getAdditionalInfo());
        }
        setFaultMetaExtraFields(faultMetaId,event);
        
        return event;
    }
    
    private void setFaultMetaExtraFields(Integer faultMetaId,Event event) {
        if (faultMetaId != null) {
            List<FaultMetaExtraField> fmExtraFields = fmemDao.get(faultMetaId);
            
            for (FaultMetaExtraField field : fmExtraFields) {
                String label = field.getValue();
                String code = field.getField();
                event.setField(code, label);
            }
        }
    }
}
