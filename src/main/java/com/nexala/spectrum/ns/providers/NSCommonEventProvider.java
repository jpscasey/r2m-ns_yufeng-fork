package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.rest.data.DefaultCommonEventProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.beans.Dropdown;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;
import com.typesafe.config.Config;

public class NSCommonEventProvider extends DefaultCommonEventProvider {

    private Config config;

    @Inject
    public NSCommonEventProvider(Map<String, EventProvider> eventProviders, CommonFleetConfiguration fleetConf,
            ApplicationConfiguration config) {
        super(eventProviders, fleetConf);
        this.config = config.get();
    }

    @Override
    public Map<String, List<Dropdown>> getAdditionnalDropDownValues(List<Fleet> fleets) {
        Map<String, List<Dropdown>> mapDropDown = new HashMap<String, List<Dropdown>>();
        
        mapDropDown.put("code", getCodeDropDownList());
        mapDropDown.put("type", getTypeDropDownList());
        mapDropDown.put("fleetId", getFleetDropDownList(fleets));
        mapDropDown.put("category", getCategoryDropDownList());
        mapDropDown.put("unitType", getUnitTypeDropDownList(fleets));
        mapDropDown.put("hasRecovery", getHasRecoveryDropDownList());
        mapDropDown.put("hasExternalReference", getHasExternalReferenceDropDownList());
        mapDropDown.put("isAcknowledged", getBooleanDropDownList());
        mapDropDown.put("createdByRule", getBooleanDropDownList());
        mapDropDown.put("unitStatus", getBooleanDropDownList());
        // project specific
        mapDropDown.put("serviceRequestStatus", getSrStatusDropDownList());
        return mapDropDown;
    }

    private List<Dropdown> getSrStatusDropDownList() {
        List<Dropdown> dropdownListSrStatus = new ArrayList<Dropdown>();

        for (Config filterConfig : config.getConfigList("r2m.eventAnalysis.filters")) {
            if (filterConfig.getString("name").equals(NS.SERVICE_REQUEST_STATUS)) {
                if (filterConfig.hasPath("values")) {
                    for (Config filterOption : filterConfig.getConfigList("values")) {
                        dropdownListSrStatus.add(new Dropdown(-1, filterOption.getString("value")));
                    }
                }
            }
        }

        return dropdownListSrStatus;
    }
}
