package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.analysis.EventSource;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.web.service.RuleTestDataProvider;

public class RuleTestDataProviderNS implements RuleTestDataProvider<GenericEvent> {

    @Inject
    private UnitDao unitDao;

    @Override
    public List<EventSource> getTestTargets() {
        List<EventSource> testTargets = new ArrayList<EventSource>();

        for (Unit unit : unitDao.findAll()) {
            Integer unitId = (int) unit.getId();
            String unitNumber = unit.getUnitNumber();
            testTargets.add(new EventSource(unitId, unitNumber));
        }

        return testTargets;
    }
}
