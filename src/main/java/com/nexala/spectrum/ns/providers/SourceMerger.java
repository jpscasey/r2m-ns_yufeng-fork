package com.nexala.spectrum.ns.providers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;

import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.GenericEventChannelLoader;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.db.beans.EventChannelValue;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;

/**
 * Provide tools to merge 2 datasources together.
 * @author BBaudry
 *
 */
public class SourceMerger {

    private DataSetTimestampComparator comparator;
    
    private ChannelManager cm;
    
    private GenericEventChannelLoader loader;
    
    @Inject
    public SourceMerger(DataSetTimestampComparator comparator, ChannelManager cm, GenericEventChannelLoader loader) {
        this.comparator = comparator;
        this.cm = cm;
        this.loader = loader;
    }
    
    /**
     * Merge live channel data with live eventChannelData.
     * One record for each values will be created, except the one from eventChannelEnrichmentData 
     * which are used only to populate all the event channels if we didn't received all the values in the live event channels.
     * liveChannelEnrichmentData is used to merge the most recent channel value with a live event channel if the event channel
     * comes in a separate message to the channel value.
     * All the records need to be from the same unit.
     * @param liveChannelData the live channel values
     * @param liveChannelEnrichmentData the previous channel values
     * @param liveEventChannelData the live event channel values
     * @param eventChannelEnrichmentData the previous event channel values
     * @return the different sources merged together
     */
    public List<GenericEvent> merge(List<DataSet> liveChannelData, List<DataSet> liveChannelEnrichmentData, List<EventChannelValue> liveEventChannelData, 
            List<EventChannelValue> eventChannelEnrichmentData, int maxIntervalMs) {

        // Build the list of timestamps for which we have a record
        Set<Long> timestamps = new TreeSet<>();
        TreeSet<DataSet> channelData = new TreeSet<>(comparator);
        
        if (liveChannelData == null) {
            liveChannelData = Collections.emptyList();
        }
        
        if (liveChannelEnrichmentData == null) {
            liveChannelEnrichmentData = Collections.emptyList();
        }
        
        if (liveEventChannelData == null) {
            liveEventChannelData = Collections.emptyList();
        }
        
        if (eventChannelEnrichmentData == null) {
            eventChannelEnrichmentData = Collections.emptyList();
        }
        
        for (DataSet chanData : liveChannelEnrichmentData) {
            channelData.add(chanData);
        }
        
        for (DataSet chanData : liveChannelData) {
            timestamps.add(chanData.getTimestamp());
            channelData.add(chanData);
        }
        
        for (DataSet eventChanData : liveEventChannelData) {
            timestamps.add(eventChanData.getTimestamp());
        }
        
        // Index the event channel values by channel id and timestamp
        Set<Integer> availableChannelsIds = new HashSet<>();
        Map<Integer, TreeSet<DataSet>> eventByChanId = new HashMap<>();
        
        for (EventChannelValue event : liveEventChannelData) {
            indexEventChannelValue(availableChannelsIds, eventByChanId, event);
        }
        
        for (EventChannelValue event : eventChannelEnrichmentData) {
            indexEventChannelValue(availableChannelsIds, eventByChanId, event);
        }
        
        // Merge the records together
        List<GenericEvent> result = new ArrayList<>();
        
        for (Long timestamp : timestamps) {
            
            GenericEvent event = cm.getEventInstance(timestamp);
            
            DataSet target = new DataSetImpl(null, timestamp, null);
            DataSet chanValue = channelData.floor(target);
            
            if (chanValue != null && timestamp - chanValue.getTimestamp() < maxIntervalMs) {
                loader.loadDataSet(event, chanValue);
            }
            
            for (TreeSet<DataSet> current : eventByChanId.values()) {
                // Iterate over the different event channel ids, to find the closest record for each event channels
                DataSet eventChan = current.floor(target);
                
                if (eventChan != null && timestamp - eventChan.getTimestamp() < maxIntervalMs) {
                    EventChannelValue val = (EventChannelValue) eventChan;
                    int pos = cm.getChannelPosition(null, "ch" + val.getChannelId()); // TODO get rid of the stupid ch here too!
                    event.put(pos, val.getValue());
                }
                
            }
            
            result.add(event);
            
        }
        
        return result;
    }
    
    /**
     * Merge live channel data with live eventChannelData and gpsChannelData for SLT fleet.
     * One record for each values will be created, except the one from eventChannelEnrichmentData 
     * which are used only to populate all the event channels if we didn't received all the values in the live event channels.
     * liveChannelEnrichmentData is used to merge the most recent channel value with a live event channel if the event channel
     * comes in a separate message to the channel value.
     * All the records need to be from the same unit.
     * @param liveChannelData the live channel values
     * @param liveEventChannelData the live event channel values
     * @param eventChannelEnrichmentData the previous event channel values
     * @param gpsChannelData the live gps channel values
     * @return the different sources merged together
     */
    public List<GenericEvent> mergeSlt(List<DataSet> liveChannelData, List<DataSet> liveChannelEnrichmentData, List<EventChannelValue> liveEventChannelData, 
            List<EventChannelValue> eventChannelEnrichmentData, List<DataSet> liveGpsChannelData, List<DataSet> gpsChannelEnrichmentData, int maxIntervalMs) {

        // Build the list of timestamps for which we have a record
        Set<Long> timestamps = new TreeSet<>();
        TreeSet<DataSet> channelData = new TreeSet<>(comparator);
        TreeSet<DataSet> gpsChannelData = new TreeSet<>(comparator);
        
        if (liveChannelData == null) {
            liveChannelData = Collections.emptyList();
        }
        
        if (liveChannelEnrichmentData == null) {
            liveChannelEnrichmentData = Collections.emptyList();
        }
        
        if (liveEventChannelData == null) {
            liveEventChannelData = Collections.emptyList();
        }
        
        if (eventChannelEnrichmentData == null) {
            eventChannelEnrichmentData = Collections.emptyList();
        }
        
        if (liveGpsChannelData == null) {
            liveGpsChannelData = Collections.emptyList();
        }
        
        if (gpsChannelEnrichmentData == null) {
            gpsChannelEnrichmentData = Collections.emptyList();
        }
        
        for (DataSet chanData : liveChannelData) {
            timestamps.add(chanData.getTimestamp());
            channelData.add(chanData);
        }
        
        for (DataSet chanData : liveChannelEnrichmentData) {
            channelData.add(chanData);
        }
        
        for (DataSet eventChanData : liveEventChannelData) {
            timestamps.add(eventChanData.getTimestamp());
        }
        
        for (DataSet gpsChanData : liveGpsChannelData) {
            timestamps.add(gpsChanData.getTimestamp());
            gpsChannelData.add(gpsChanData);
        }
        
        for (DataSet gpsChanData : gpsChannelEnrichmentData) {
            gpsChannelData.add(gpsChanData);
        }
        
        // Index the event channel values by channel id and timestamp
        Set<Integer> availableChannelsIds = new HashSet<>();
        Map<Integer, TreeSet<DataSet>> eventByChanId = new HashMap<>();
        
        for (EventChannelValue event : liveEventChannelData) {
            indexEventChannelValue(availableChannelsIds, eventByChanId, event);
        }
        
        for (EventChannelValue event : eventChannelEnrichmentData) {
            indexEventChannelValue(availableChannelsIds, eventByChanId, event);
        }
        
        // Merge the records together
        List<GenericEvent> result = new ArrayList<>();
        
        for (Long timestamp : timestamps) {
            
            GenericEvent event = cm.getEventInstance(timestamp);
            
            DataSet target = new DataSetImpl(null, timestamp, null);
            DataSet chanValue = channelData.floor(target);            
            DataSet gpsChanValue = gpsChannelData.floor(target);
            
            if (chanValue != null && timestamp - chanValue.getTimestamp() < maxIntervalMs) {
                loader.loadDataSet(event, chanValue);
            }
            
            if (gpsChanValue != null && timestamp - gpsChanValue.getTimestamp() < maxIntervalMs) {
                loader.loadDataSet(event, gpsChanValue);
            }            
            
            for (TreeSet<DataSet> current : eventByChanId.values()) {
                // Iterate over the different event channel ids, to find the closest record for each event channels
                DataSet eventChan = current.floor(target);
                
                if (eventChan != null && timestamp - eventChan.getTimestamp() < maxIntervalMs) {
                    EventChannelValue val = (EventChannelValue) eventChan;
                    int pos = cm.getChannelPosition(null, "ch" + val.getChannelId()); // TODO get rid of the stupid ch here too!
                    event.put(pos, val.getValue());
                }
                
            }
            
            result.add(event);
            
        }
        
        return result;
    }

    /**
     * Update the map of events by channel id and timestamp.
     * @param availableChannelsIds
     * @param eventByChanIdAndTime
     * @param event
     */
    private void indexEventChannelValue(Set<Integer> availableChannelsIds,
            Map<Integer, TreeSet<DataSet>> eventByChanIdAndTime, EventChannelValue event) {
        Integer chanId = event.getChannelId();
        availableChannelsIds.add(chanId);
        
        TreeSet<DataSet> eventChanVal = eventByChanIdAndTime.get(chanId);
        
        if (eventChanVal == null) {
            eventChanVal = new TreeSet<>(comparator);
            eventByChanIdAndTime.put(chanId, eventChanVal);
        }
        
        eventChanVal.add(event);
    }

    public static class DataSetTimestampComparator implements Comparator<DataSet> {
        @Override
        public int compare(DataSet o1, DataSet o2) {
            return o1.getTimestamp().compareTo(o2.getTimestamp());
        }
    }
    
    
    private class EventChanValKey {
        
        private Long timestamp;
        
        private Integer channelId;
        
        public EventChanValKey(Long timestamp, Integer channelId) {
            this.timestamp = timestamp;
            this.channelId = channelId;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
        }

        public Integer getChannelId() {
            return channelId;
        }

        public void setChannelId(Integer channelId) {
            this.channelId = channelId;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
            result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            EventChanValKey other = (EventChanValKey) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (channelId == null) {
                if (other.channelId != null)
                    return false;
            } else if (!channelId.equals(other.channelId))
                return false;
            if (timestamp == null) {
                if (other.timestamp != null)
                    return false;
            } else if (!timestamp.equals(other.timestamp))
                return false;
            return true;
        }

        private SourceMerger getOuterType() {
            return SourceMerger.this;
        }
    }

}
