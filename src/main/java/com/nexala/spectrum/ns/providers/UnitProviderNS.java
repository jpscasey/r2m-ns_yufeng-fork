package com.nexala.spectrum.ns.providers;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.UnitSearchResult;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.ns.db.dao.UnitSearchDao;
import com.nexala.spectrum.rest.data.AbstractUnitProvider;
import com.nexala.spectrum.rest.data.beans.UnitDescList;

public abstract class UnitProviderNS extends AbstractUnitProvider {
    
    @Inject
    private UnitSearchDao searchDao;

    @Override
    public UnitDescList searchUnits(String substr) throws DataAccessException {
        UnitDescList descList = new UnitDescList("Train Number", "Unit Number");

        // FIXME no licencing on unit lookup..
        // dataLicence.getLicencedUnit()..

        for (UnitSearchResult unit : searchDao.getFleetUnits(getFleetCode(), substr)) {
            descList.addRecord(unit.getId(), unit.getUnitNumber(),
                    // descriptors [headcode, unitnumber]
                    getFleetCode(), unit.getHeadcode(), unit.getUnitNumber());
        }

        return descList;
    }
}
