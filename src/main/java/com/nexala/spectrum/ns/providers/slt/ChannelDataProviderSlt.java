package com.nexala.spectrum.ns.providers.slt;

import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.providers.ChannelDataProviderNS;

public class ChannelDataProviderSlt extends ChannelDataProviderNS {

	@Override
    protected int getVTSpeedChannel(ChannelCollection channel) {
        return ChannelId.CH8124;
    }
    
	@Override
    protected int getVTLatitudeChannel(ChannelCollection channel) {
        return ChannelId.CH8086;
    }
    
	@Override
    protected int getVTLongitudeChannel(ChannelCollection channel) {
        return ChannelId.CH8087;
    }
    
}
