package com.nexala.spectrum.ns.providers.slt;

import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.UnitProviderNS;

public class UnitProviderSlt extends UnitProviderNS {

    @Override
    public String getFleetCode() {
        return NS.FLEET_SLT;
    }
}
