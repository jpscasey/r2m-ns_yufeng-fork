package com.nexala.spectrum.ns.providers.sng;

import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.ns.ChannelIdSng;
import com.nexala.spectrum.ns.providers.ChannelDataProviderNS;

public class ChannelDataProviderSng extends ChannelDataProviderNS {

	@Override
    protected int getVTSpeedChannel(ChannelCollection channel) {
        return ChannelIdSng.CH2003;
    }
    
	@Override
    protected int getVTLatitudeChannel(ChannelCollection channel) {
        return ChannelIdSng.CH2001;
    }
    
	@Override
    protected int getVTLongitudeChannel(ChannelCollection channel) {
        return ChannelIdSng.CH2002;
    }

}
