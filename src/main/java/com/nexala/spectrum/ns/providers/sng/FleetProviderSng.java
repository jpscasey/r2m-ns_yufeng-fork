package com.nexala.spectrum.ns.providers.sng;

import com.google.inject.Inject;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.ns.ChannelIdSng;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.FleetProviderNS;
import com.nexala.spectrum.ns.view.conf.DrillDownConfigurationNS;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.validation.ChannelValidator;

public class FleetProviderSng extends FleetProviderNS {

    @Inject
    public FleetProviderSng(ChannelConfiguration channelConfiguration, ChannelDataProvider channelDataProvider,
            ChannelValidator channelValidator, UnitProvider unitProvider, DrillDownConfigurationNS configuration,
            ApplicationConfiguration applicationConfiguration, UserConfigurationProvider userConfigProvider, GpsDataProvider gpsDataProvider) {
        super(channelConfiguration, channelDataProvider, channelValidator, unitProvider, configuration,
                applicationConfiguration, userConfigProvider, gpsDataProvider);
    }
    
    @Override
    protected int getSpeedChannel(ChannelCollection channels) {
        // Is this the right channel?     
        return ChannelIdSng.CH2003;
    }
    
    @Override
    protected String getLeadingVehicle(ChannelCollection channels) {
        String value = null;
        
        Channel<?> temp = channels.getByName(NS.ACTIVE_CAB);
        
        if (temp != null && temp.getValue() != null) {
            value = temp.getStringValue();
        }
        
        return value;
    }
}
