package com.nexala.spectrum.ns.providers.sng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelDouble;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.ns.ChannelIdSng;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.rest.data.AbstractUnitDetailProvider;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.rest.data.beans.Diagram;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.Restriction;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.UnitDetail;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;

public class UnitDetailProviderSng extends AbstractUnitDetailProvider {

    @Inject
    private ChannelDataProvider channelDataProvider;

    @Inject
    private EventProvider eventProvider;

    @Inject
    public UnitDetailProviderSng(UnitProvider unitProvider, UnitDetailConfiguration unitDetailConfiguration) {
        super(unitProvider, unitDetailConfiguration);
    }
    
    @Override
    public UnitDetail getUnitDetail(int unitId, long timestamp) {
        Unit stock = unitProvider.searchUnit(unitId);
        List<StockDesc> tabs = unitDetailConfiguration.getTabs(stock);

        List<DataSet> data = channelDataProvider.getUnitData(unitId, null, Spectrum.RECORD_BY_TIME);

        List<Event> liveEvents = eventProvider.getLiveByUnit(unitId);

        List<Event> recentEvents = eventProvider.getRecentByUnit(unitId, Spectrum.UNIT_DETAIL_NUMBER_OF_RECENT_EVENTS);

        UnitDetail ud = new UnitDetail();
        ud.setUnit(stock);
        ud.setTabs(tabs);
        ud.setInfoData(getInfoDataSng(unitId, data));
        ud.setDiagramData(getDiagramDataSng(unitId, data, liveEvents));
        ud.setLive(liveEvents);
        ud.setRecent(recentEvents);

        return ud;
    }
    
    private DataSet getInfoDataSng(int unitId, List<DataSet> data) {
        String headcode = null;
        String unitNumber = null;
        Long latestTimestamp = null;
        String location = null;
        String diagram = null;
        Double speed = null;
        String leadingVehicle = null;
        int faultNumber = 0;
        int warningNumber = 0;
        String unitStatus = null;
        String unitMaintenanceLocation = null;
        Long unitLastMaintenanceDate = null;
        DataSet unitDataSet = null;

        for (DataSet dataSet : data) {
            if(Integer.valueOf(dataSet.getChannels().getByName(Spectrum.UNIT_ID).getStringValue()).equals(unitId)){
                unitDataSet = dataSet;
            }
        }
        
        ChannelCollection channels = unitDataSet.getChannels();

        Channel<?> temp;

        // TIMESTAMP
        Long vehicleTimestamp = null;
        temp = channels.getByName(Spectrum.TIMESTAMP);
        if (isValid(temp)) {
            vehicleTimestamp = Long.valueOf(temp.getStringValue());
            if (latestTimestamp == null || vehicleTimestamp > latestTimestamp) {
                latestTimestamp = vehicleTimestamp;
            }
        }

        // HEADCODE
        temp = channels.getByName(Spectrum.HEADCODE);
        if (isValid(temp) && headcode == null) {
            headcode = temp.getStringValue();
        }

        // LOCATION
        temp = channels.getByName(Spectrum.LOCATION);
        if (isValid(temp) && unitNumber == null) {
            location = temp.getStringValue();
        }

        // UNIT NUMBER
        temp = channels.getByName(Spectrum.UNIT_NUMBER);
        if (isValid(temp) && unitNumber == null) {
            unitNumber = temp.getStringValue();
        }

        // LEADING VEHICLE
        leadingVehicle = getLeadingCab(channels);
        
        // DIAGRAM
        temp = channels.getByName(Spectrum.DIAGRAM);
        if (isValid(temp) && diagram == null) {
            diagram = temp.getStringValue();
        }

        // SPEED 
        // Is this the right channel?
        temp = channels.getById(ChannelIdSng.CH2003);
        if (isValid(temp) && speed == null) {
            speed = temp.getValueAsDouble();
        }

        // FAULT NUMBER
        // Faults are attached to the UNIT, not the VEHICLE
        // For each UNIT, if the faultNumber has already changed, don't
        // change it again
        if (faultNumber == 0) {
            temp = channels.getByName(Spectrum.FAULT_NUMBER);
            if (temp != null && temp.getValue() != null) {
                faultNumber += Integer.valueOf(temp.getStringValue());
            }
        }

        // WARNING NUMBER
        // Warnings are attached to the UNIT, not the VEHICLE
        // For each UNIT, if the warningNumber has already changed, don't
        // change it again
        if (warningNumber == 0) {
            temp = channels.getByName(Spectrum.WARNING_NUMBER);
            if (temp != null && temp.getValue() != null) {
                warningNumber += Integer.valueOf(temp.getStringValue());
            }
        }

        // UNIT STATUS
        temp = channels.getByName(NS.UNIT_STATUS);
        if (isValid(temp) && unitStatus == null) {
            unitStatus = temp.getStringValue();
        }

        // MAINTENANCE LOCATION
        temp = channels.getByName(NS.UNIT_MAINTENANCE_LOCATION);
        if (isValid(temp) && unitMaintenanceLocation == null) {
            unitMaintenanceLocation = temp.getStringValue();
        }

        // LAST MAINTENANCE DATE
        temp = channels.getByName(NS.UNIT_LAST_MAINTENANCE_DATE);
        if (isValid(temp) && unitLastMaintenanceDate == null) {
            unitLastMaintenanceDate = Long.valueOf(temp.getStringValue());
        }

        ChannelCollection unitChannels = new ChannelCollection();
        unitChannels.put(new ChannelLong(Spectrum.TIMESTAMP, latestTimestamp, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(Spectrum.HEADCODE, headcode, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(NS.LEADING_VEHICLE_VIEW, leadingVehicle, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(Spectrum.LOCATION, location, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(Spectrum.UNIT_NUMBER, unitNumber, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(Spectrum.DIAGRAM, diagram, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelDouble(NS.SPEED_VIEW, speed, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelInt(Spectrum.FAULT_NUMBER, faultNumber, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelInt(Spectrum.WARNING_NUMBER, warningNumber, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(NS.UNIT_STATUS, unitStatus, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelString(NS.UNIT_MAINTENANCE_LOCATION, unitMaintenanceLocation, ChannelCategory.NORMAL));
        unitChannels.put(new ChannelLong(NS.UNIT_LAST_MAINTENANCE_DATE, unitLastMaintenanceDate, ChannelCategory.NORMAL));

        return new DataSetImpl(unitId, unitChannels);
    }

    private String getLeadingCab(ChannelCollection channelCollection){
        // TODO
        return null;
    }
    
    private Map<Integer, Diagram> getDiagramDataSng(int unitId, List<DataSet> data, List<Event> events) {

        Map<Integer, Diagram> diagram = new HashMap<Integer, Diagram>();

        for (DataSet dataSet : data) {
            boolean leading = false;
            boolean forward = false;
            boolean reverse = false;
            String leadingCab = null;
            ChannelCollection channels = dataSet.getChannels();

            int vehicleId = ((ChannelInt) channels.getByName(Spectrum.UNIT_ID)).getValue();

            Long timestamp = null;
            ChannelLong timestampChannel = (ChannelLong) channels.getByName(Spectrum.TIMESTAMP);
            if (timestampChannel != null) {
                timestamp = timestampChannel.getValue();
            }

            leadingCab = getLeadingCab(channels);

            ChannelCategory vehicleCategory = getDiagramCategory(events);

            Map<String, ChannelCategory> categories = new HashMap<String, ChannelCategory>();
            categories.put("vehicle", vehicleCategory);

            diagram.put(Integer.valueOf(vehicleId), new Diagram(vehicleId, leading, forward, reverse, leadingCab,
                    categories, events, new ArrayList<Restriction>(), timestamp));
        }

        return diagram;
    }
    
    private ChannelCategory getDiagramCategory(List<Event> events) {
        ChannelCategory category = ChannelCategory.NODATA;
        Integer priority = null;

        for (Event event : events) {
            if (!event.getField(Spectrum.EVENT_CATEGORY).toString().equalsIgnoreCase("Test")) {
                Integer currentPriority = (Integer) event.getField("typeId");
                if (currentPriority != null && (priority == null || currentPriority < priority)) {
                    priority = currentPriority;
                }

                if (currentPriority == 1) {
                    break;
                }
            }
        }

        if (priority != null) {
            if (priority == 1) {
                category = ChannelCategory.FAULT;
            } else if (priority == 2) {
                category = ChannelCategory.WARNING;
            } else if (priority == 3) {
                category = ChannelCategory.OFF;
            } else if (priority == 4) {
                category = ChannelCategory.ON;
            } else if (priority == 5) {
                category = ChannelCategory.NORMAL;
            }
        }

        return category;
    }
    
    @Override
    public DataSet getInfoData(int stockId) {
        return null;
    }

    @Override
    public Map<Integer, Diagram> getDiagramData(int stockId) {
        return null;
    }

    @Override
    public DataSet getVehiclesTimestamp(List<DataSet> data) {
        return null;
    }

    @Override
    public String getUnitInfoLabel(List<DataSet> data) {
        return null;
    }
    
    private boolean isValid(Channel<?> ch) {
        return ch != null && ch.getValue() != null ? true : false;
    }
    
    @Override
    public List<GenericBean> getWorkOrders(int unitId) {
        List<GenericBean> result = woDao.getWorkOrdersByUnitId(unitId);
        String woURL = "";
        String srURL = "";
        String url;
        
        for(Map<String, String> hm : unitDetailConfiguration.getMaximoUrls()){
            if(hm.containsKey("Work Order")){ 
                woURL = hm.get("Work Order"); 
            } else if (hm.containsKey("Service Request")) {
                srURL = hm.get("Service Request");
            }            
        }
                
        for(GenericBean gb : result){
            if(gb.getFields().get("maximoType").equals("Work Order")) {
                url = "<a href='" + woURL + gb.getFields().get("maximoId") + "' target='_blank'>" + gb.getFields().get("maximoId");
            } else {
                url = "<a href='" + srURL + gb.getFields().get("maximoId") + "' target='_blank'>" + gb.getFields().get("maximoId");
            }
            
            gb.setField("maximoId", url);
        }
        
        return result;
    }
}
