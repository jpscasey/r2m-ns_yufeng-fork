package com.nexala.spectrum.ns.providers.sng;

import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.UnitProviderNS;

public class UnitProviderSng extends UnitProviderNS {

    @Override
    public String getFleetCode() {
        return NS.FLEET_SNG;
    }
}
