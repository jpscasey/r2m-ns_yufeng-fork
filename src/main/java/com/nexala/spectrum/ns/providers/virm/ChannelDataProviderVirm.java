package com.nexala.spectrum.ns.providers.virm;

import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.ns.ChannelIdVirm;
import com.nexala.spectrum.ns.providers.ChannelDataProviderNS;

public class ChannelDataProviderVirm extends ChannelDataProviderNS {

	@Override
    protected int getVTSpeedChannel(ChannelCollection channel) {
        return ChannelIdVirm.CH8003;
    }
    
	@Override
    protected int getVTLatitudeChannel(ChannelCollection channel) {
        return ChannelIdVirm.CH8001;
    }
    
	@Override
    protected int getVTLongitudeChannel(ChannelCollection channel) {
        return ChannelIdVirm.CH8002;
    }

}
