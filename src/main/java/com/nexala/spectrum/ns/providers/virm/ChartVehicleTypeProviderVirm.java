package com.nexala.spectrum.ns.providers.virm;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.ChartVehicleType;
import com.nexala.spectrum.db.dao.charting.ChartVehicleTypeDao;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;

public class ChartVehicleTypeProviderVirm implements ChartVehicleTypeProvider {
    
    @Inject
    ChartVehicleTypeDao dao;

    @Override
    public List<ChartVehicleType> getAllChartVehicleTypes() {
        return dao.findAll();
    }
}