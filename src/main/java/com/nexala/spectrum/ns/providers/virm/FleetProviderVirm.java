package com.nexala.spectrum.ns.providers.virm;

import com.google.inject.Inject;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.FleetProviderNS;
import com.nexala.spectrum.ns.view.conf.DrillDownConfigurationNS;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.GpsDataProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.validation.ChannelValidator;

public class FleetProviderVirm extends FleetProviderNS {

    @Inject
    public FleetProviderVirm(ChannelConfiguration channelConfiguration, ChannelDataProvider channelDataProvider,
            ChannelValidator channelValidator, UnitProvider unitProvider, DrillDownConfigurationNS configuration,
            ApplicationConfiguration applicationConfiguration, UserConfigurationProvider userConfigProvider, GpsDataProvider gpsDataProvider) {
        super(channelConfiguration, channelDataProvider, channelValidator, unitProvider, configuration,
                applicationConfiguration, userConfigProvider, gpsDataProvider);
    }

    @Override
    protected int getSpeedChannel(ChannelCollection channels) {
    	
    	ChannelBoolean chCab1 = (ChannelBoolean) channels.getById(ChannelId.CH63);
        ChannelBoolean chCab2 = (ChannelBoolean) channels.getById(ChannelId.CH108);
        
        if (isValid(chCab1) && isValid(chCab2)) {
        	if (chCab1.getValue() && !chCab2.getValue()) {
        		return ChannelId.CH101;
        	} else if (!chCab1.getValue() && chCab2.getValue()) {
        		return ChannelId.CH143;
        	}
        }
    	
        return 0;
    }

	@Override
	protected String getLeadingVehicle(ChannelCollection channels) {
	    String value = null;
        
        Channel<?> temp = channels.getByName(NS.ACTIVE_CAB);
        
        if (temp != null && temp.getValue() != null) {
            value = temp.getStringValue();
        }
        
        return value;
	}

	private boolean isValid(Channel<?> ch) {
        return ch != null && ch.getValue() != null ? true : false;
    }
}
