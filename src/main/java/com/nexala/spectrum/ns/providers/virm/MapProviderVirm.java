package com.nexala.spectrum.ns.providers.virm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelBoolean;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelDouble;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.ChannelIdVirm;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.MapProviderNS;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;

public class MapProviderVirm extends MapProviderNS {

	@Override
    protected DataSet getDataSet(int id, List<DataSet> data) {

        ChannelString headcode = new ChannelString(Spectrum.HEADCODE, null, ChannelCategory.NODATA);
        ChannelString location = new ChannelString(Spectrum.LOCATION, null, ChannelCategory.NODATA);
        ChannelLong timestamp = new ChannelLong(Spectrum.TIMESTAMP, null, ChannelCategory.NODATA);
        ChannelDouble latitude = new ChannelDouble(NS.GPS_LATITUDE_VIEW, null, ChannelCategory.NODATA);
        ChannelDouble longitude = new ChannelDouble(NS.GPS_LONGITUDE_VIEW, null, ChannelCategory.NODATA);
        ChannelDouble speed = new ChannelDouble(NS.SPEED_VIEW, null, ChannelCategory.NODATA);
        ChannelString diagram = new ChannelString(Spectrum.DIAGRAM, null, ChannelCategory.NODATA);
        ChannelString fleetId = new ChannelString(Spectrum.FLEET_ID, null, ChannelCategory.NODATA);
        ChannelInt vehicleId = new ChannelInt(Spectrum.VEHICLE_ID, null, ChannelCategory.NODATA);
        ChannelString activeCabCh = new ChannelString(NS.LEADING_VEHICLE_VIEW, "", ChannelCategory.NODATA);

        Map<Integer, String> unitNumberMap = new HashMap<Integer, String>();
        Map<Integer, String> unitIdMap = new HashMap<Integer, String>();
        Set<Integer> unitIdSet = new HashSet<Integer>();
        
        int p1FaultNumber = 0;
        int p2FaultNumber = 0;
        int p3FaultNumber = 0;
        int p4FaultNumber = 0;
        int p5FaultNumber = 0;
        int faultRecoveryNumber = 0;
        int faultNotAcknowledgedNumber = 0;
        int unitStatus = 0;
        String unitType = null;        
        
        for (DataSet dataSet : data) {
            ChannelCollection channels = dataSet.getChannels();

            Channel<?> temp;
            
            // TIMESTAMP
            Long vehicleTimestamp = null;
            temp = channels.getByName(Spectrum.TIMESTAMP);
            if (isValid(temp)) {
                vehicleTimestamp = Long.valueOf(temp.getStringValue());
                if (timestamp.getValue() == null || vehicleTimestamp > timestamp.getValue()) {
                    
                    ChannelDouble lat = (ChannelDouble) channels.getById(ChannelId.CH8001);
                    ChannelDouble lng = (ChannelDouble) channels.getById(ChannelId.CH8002);
                    
                    if (isValid(lat) && isValid(lng)) {
                        
                        timestamp.setValue(vehicleTimestamp);
                        timestamp.setCategory(temp.getCategory());
                        
                        // LATITUDE
                        latitude.setValue(lat.getValue());
                        latitude.setCategory(lat.getCategory());

                        // LONGITUDE
                        longitude.setValue(lng.getValue());
                        longitude.setCategory(lng.getCategory());
                    }
                }
            }

            // HEADCODE
            temp = channels.getByName(Spectrum.HEADCODE);
            if (isValid(temp) && headcode.getValue() == null) {
                headcode.setValue(temp.getStringValue());
                headcode.setCategory(temp.getCategory());
            }

            // CAB ACTIVE
            ChannelBoolean chCab1 = (ChannelBoolean) channels.getById(ChannelId.CH63);
            ChannelBoolean chCab2 = (ChannelBoolean) channels.getById(ChannelId.CH108);
            
            if (isValid(chCab1) && isValid(chCab2)) {
            	if (chCab1.getValue() && !chCab2.getValue()) {
            		activeCabCh.setValue(NS.CAB1);
            	} else if (!chCab1.getValue() && chCab2.getValue()) {
            		activeCabCh.setValue(NS.CAB2);
            	}
            }

            // LOCATION
            temp = channels.getByName(Spectrum.LOCATION);
            if (isValid(temp) && (location.getValue() == null  || vehicleTimestamp.equals(timestamp.getValue()))) {
                location.setValue(temp.getStringValue());
                location.setCategory(temp.getCategory());
            }

            // DIAGRAM
            temp = channels.getByName(Spectrum.DIAGRAM);
            if (isValid(temp) && diagram.getValue() == null) {
                diagram.setValue(temp.getStringValue());
                diagram.setCategory(temp.getCategory());
            }

            // SPEED
            if (activeCabCh.getValue().compareTo("") != 0) {
            	if (activeCabCh.getValue() == NS.CAB1) {
            		temp = channels.getById(ChannelIdVirm.CH101);
            	} else if (activeCabCh.getValue() == NS.CAB2) {
            		temp = channels.getById(ChannelIdVirm.CH143);
            	}
            	
            	if (isValid(temp) && (speed.getValue() == null || vehicleTimestamp.equals(timestamp.getValue()))) {
                    speed.setValue(temp.getValueAsDouble());
                    speed.setCategory(temp.getCategory());
                }
            }
            
            temp = channels.getByName(Spectrum.FLEET_CODE);
            if (isValid(temp) && fleetId.getValue() == null) {
                fleetId.setValue(temp.getStringValue());
                fleetId.setCategory(temp.getCategory());
            }

            temp = channels.getByName(Spectrum.VEHICLE_ID);
            if (isValid(temp) && vehicleId.getValue() == null) {
                vehicleId.setValue(Integer.valueOf(temp.getStringValue()));
                vehicleId.setCategory(temp.getCategory());
            }

            temp = channels.getByName(Spectrum.UNIT_POSITION);
            int unitPosition = 1;
            if (temp != null && temp.getValue() != null) {
                unitPosition = Integer.valueOf(temp.getStringValue());
            }
            
            temp = channels.getByName(Spectrum.UNIT_TYPE);
            if (temp != null && temp.getValue() != null) {
            	unitType = temp.getStringValue();
            }

            temp = channels.getByName(Spectrum.UNIT_NUMBER);
            if (temp != null && temp.getValue() != null) {
                String unitNumber = temp.getStringValue();

                unitNumberMap.put(unitPosition, unitNumber);
            }

            temp = channels.getByName(Spectrum.UNIT_ID);
            if (temp != null && temp.getValue() != null) {
                String unitId = temp.getStringValue();

                unitIdMap.put(unitPosition, unitId);
                
                if (!unitIdSet.contains(Integer.valueOf(temp.getStringValue()))) {
                    // P1 NUMBER
                    Channel<?> p1Faults = channels.getByName(NS.FAULT_P1_NUMBER);
                    if (p1Faults != null && p1Faults.getValue() != null) {
                        p1FaultNumber += Integer.valueOf(p1Faults.getStringValue());
                    }
                    // P2 NUMBER
                    Channel<?> p2Faults = channels.getByName(NS.FAULT_P2_NUMBER);
                    if (p2Faults != null && p2Faults.getValue() != null) {
                        p2FaultNumber += Integer.valueOf(p2Faults.getStringValue());
                    }
                    // P3 NUMBER
                    Channel<?> p3Faults = channels.getByName(NS.FAULT_P3_NUMBER);
                    if (p3Faults != null && p3Faults.getValue() != null) {
                        p3FaultNumber += Integer.valueOf(p3Faults.getStringValue());
                    }
                    // P4 NUMBER
                    Channel<?> p4Faults = channels.getByName(NS.FAULT_P4_NUMBER);
                    if (p4Faults != null && p4Faults.getValue() != null) {
                        p4FaultNumber += Integer.valueOf(p4Faults.getStringValue());
                    }
                    // P5 NUMBER
                    Channel<?> p5Faults = channels.getByName(NS.FAULT_P5_NUMBER);
                    if (p5Faults != null && p5Faults.getValue() != null) {
                        p5FaultNumber += Integer.valueOf(p5Faults.getStringValue());
                    }
                    // FAULTS WITH RECOVERY NUMBER
                    Channel<?> faultsWithRecovery = channels.getByName(NS.FAULT_NUMBER_RECOVERY);
                    if (faultsWithRecovery != null && faultsWithRecovery.getValue() != null) {
                        faultRecoveryNumber += Integer.valueOf(faultsWithRecovery.getStringValue());
                    }
                    // FAULTS NOT ACKNOWLEDGED NUMBER
                    Channel<?> faultsNotAcknowledged = channels.getByName(NS.FAULT_NUMBER_NOT_ACKNOWLEDGED);
                    if (faultsNotAcknowledged != null && faultsNotAcknowledged.getValue() != null) {
                        faultNotAcknowledgedNumber += Integer.valueOf(faultsNotAcknowledged.getStringValue());
                    }
                    // FAULTS WITH OPERATIONAL UNITS
                    Channel<?> operationalUnit = channels.getByName(NS.OPERATIONAL_UNITS_COUNT);
                    if (operationalUnit != null && operationalUnit.getValue() != null) {
                        if (!operationalUnit.getStringValue().equals("0")) {
                            unitStatus = 1;
                        } else {
                            unitStatus = 0;
                        }
                    }
                }
                
                // unit ids
                unitIdSet.add(Integer.valueOf(temp.getStringValue()));
            }

        }

        SortedSet<Integer> positions = new TreeSet<Integer>(
                unitNumberMap.keySet());
        StringBuffer formation = new StringBuffer();
        StringBuffer fleetFormationID = new StringBuffer();
        for (Integer unitPosition : positions) {
            if (formation.length() > 0) {
                formation.append(", ");
                fleetFormationID.append(" ");
            }
            formation.append(unitNumberMap.get(unitPosition));
            if (unitIdMap.size() > 0) {
                fleetFormationID.append(unitIdMap.get(unitPosition));
            } else {
                fleetFormationID.append(id);
            }
        }

        ChannelCollection channels = new ChannelCollection();
        channels.put(timestamp);
        channels.put(headcode);
        channels.put(latitude);
        channels.put(longitude);
        channels.put(location);
        channels.put(diagram);
        channels.put(speed);
        channels.put(fleetId);
        channels.put(vehicleId);
        channels.put(activeCabCh);
        channels.put(new ChannelString(Spectrum.FORMATION, formation.toString(), ChannelCategory.NORMAL));
        channels.put(new ChannelString(Spectrum.FLEET_FORMATION_ID, fleetFormationID.toString(), ChannelCategory.NORMAL));
        channels.put(new ChannelString(Spectrum.TRAIN_ICON, getTrainIcon(unitStatus, p1FaultNumber, p2FaultNumber, p3FaultNumber, p4FaultNumber, p5FaultNumber), ChannelCategory.NORMAL));
        channels.put(new ChannelString(Spectrum.MAP_FILTER_GROUPS, 
                getMapFilterGroups(p1FaultNumber, p2FaultNumber, p3FaultNumber, p4FaultNumber, p5FaultNumber, faultRecoveryNumber, faultNotAcknowledgedNumber, unitStatus), 
                ChannelCategory.NORMAL));
        channels.put(new ChannelString(Spectrum.MAP_FILTER_TEXT_GROUPS, getMapFilterTextGroups(unitType), ChannelCategory.NORMAL));

        return new DataSetImpl(id, channels);
    }
}
