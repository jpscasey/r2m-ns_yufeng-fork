package com.nexala.spectrum.ns.providers.virm;

import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.ns.providers.UnitProviderNS;

public class UnitProviderVirm extends UnitProviderNS {

    @Override
    public String getFleetCode() {
        return NS.FLEET_VIRM;
    }

}
