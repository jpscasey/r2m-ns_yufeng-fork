package com.nexala.spectrum.ns.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.licensing.LoginUser;
import com.nexala.spectrum.ns.db.beans.MaximoServiceRequestResponse;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.beans.Event;

@Path("/maximoServiceRequest")
@Singleton
public class MaximoServiceRequestService {

    private Map<String, MaximoServiceRequestProvider> maximoProviders;

    private Map<String, EventProvider> eventProviders;
    
    @Inject
    public MaximoServiceRequestService(Map<String, MaximoServiceRequestProvider> maximoProviders, Map<String, EventProvider> eventProviders) {
        this.maximoProviders = maximoProviders;
        this.eventProviders = eventProviders;
    }
    
    @POST
    @LoggingData
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MaximoServiceRequestResponse createServiceRequest(@Context HttpServletRequest request, @Context HttpServletResponse response,
    		Map<String, String> parameters) {
        
    	String fleetId = parameters.get("fleetId");
        Long eventId = Long.valueOf(parameters.get("eventId"));
        long rowVersion = Long.valueOf(parameters.get("rowVersion"));
        
        LoginUser user = Licence.get(request, response, true).getLoginUser();
        
        Event event = eventProviders.get(fleetId).getEventById(eventId, false);
        event.setField("description", parameters.get("description"));
        event.setField("longDescription", parameters.get("longDescription"));
        event.setField("eventCode", parameters.get("eventCode"));
        event.setField("serviceRequestPriority", parameters.get("serviceRequestPriority"));
        event.setField("systemCode", parameters.get("systemCode"));
        event.setField("controlRoomPriority", parameters.get("controlRoomPriority"));
        event.setField("locationCode", parameters.get("locationCode"));
        event.setField("qualityProfile", parameters.get("qualityProfile"));
        event.setField("headcode", parameters.get("headcode"));
        event.setField("safetyPriority", parameters.get("safetyPriority"));
        event.setField("maintenanceFunction", parameters.get("workorderLocationType"));
        event.setField("notificationTimestamp", parameters.get("createTime"));
        event.setField("materialNumber", parameters.get("unitNumber"));
        
        List<EventComment> eventComments = eventProviders.get(fleetId).getComments(eventId.intValue());
        event.setField("comments", eventComments);
                
        return maximoProviders.get(fleetId).createServiceRequest(event, rowVersion, user);
        
    }
}
