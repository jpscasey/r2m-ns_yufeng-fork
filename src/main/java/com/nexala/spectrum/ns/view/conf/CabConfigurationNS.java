package com.nexala.spectrum.ns.view.conf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.ns.ChannelId;
import com.nexala.spectrum.ns.NS;
import com.nexala.spectrum.view.conf.cab.Button;
import com.nexala.spectrum.view.conf.cab.ButtonSettings;
import com.nexala.spectrum.view.conf.cab.CabConfiguration;
import com.nexala.spectrum.view.conf.cab.CabDomIdGenerator;
import com.nexala.spectrum.view.conf.cab.ClearButton;
import com.nexala.spectrum.view.conf.cab.Component;
import com.nexala.spectrum.view.conf.cab.DialSettings;
import com.nexala.spectrum.view.conf.cab.Gauge;
import com.nexala.spectrum.view.conf.cab.GaugeSettings;
import com.nexala.spectrum.view.conf.cab.Label;
import com.nexala.spectrum.view.conf.cab.LabelSettings;
import com.nexala.spectrum.view.conf.cab.Led;
import com.nexala.spectrum.view.conf.cab.LedSettings;
import com.nexala.spectrum.view.conf.cab.Lever;
import com.nexala.spectrum.view.conf.cab.LeverSettings;
import com.nexala.spectrum.view.conf.cab.Panel;
import com.nexala.spectrum.view.conf.cab.Panel.COLOR;
import com.nexala.spectrum.view.conf.cab.Switch;
import com.nexala.spectrum.view.conf.cab.SwitchSettings;
import com.nexala.spectrum.view.conf.cab.Value;
import com.nexala.spectrum.view.conf.cab.ValueSettings;
import com.nexala.spectrum.view.conf.cab.ValueSettings.TextAnchor;

public class CabConfigurationNS implements CabConfiguration {

    private final CabDomIdGenerator idGen;
    private final List<Component> components;
    private final List<Label> labels;
    private final List<Integer> channelIds;
    private ChannelConfig channelConf;

    private ChannelConfiguration channelConfiguration;

    private static final ButtonSettings BLUE_BUTTON = new ButtonSettings().setFill(ButtonSettings.BLUE)
            .setGlowColor(ButtonSettings.BLUE_GLOW);
    private static final ButtonSettings RED_BUTTON = new ButtonSettings().setFill(ButtonSettings.RED)
            .setGlowColor(ButtonSettings.RED_GLOW);
    private static final ButtonSettings GREEN_BUTTON = new ButtonSettings().setFill("#C7EA46")
            .setGlowColor(ButtonSettings.YELLOW_GLOW);
    private static final ButtonSettings EMG_BUTTON = new ButtonSettings().setFill("#C21807").setHighlightFill("#FC3D32")
            .setGlowColor(ButtonSettings.RED_GLOW);
    private static final ButtonSettings WHITE_BUTTON = new ButtonSettings().setFill("#FFFFFF")
            .setHighlightFill("#FFFFFF").setGlowColor(ButtonSettings.YELLOW_GLOW);

    private static final LedSettings GREEN_LED = new LedSettings().setOffColor("green").setOnColor("#C7EA46");

    @Inject
    public CabConfigurationNS(ChannelConfiguration conf) {
        channelConfiguration = conf;
        idGen = new CabDomIdGenerator();
        components = new ArrayList<Component>();
        labels = new ArrayList<Label>();
        channelIds = getChannelIdList();

        createMainPanel();
    }

    private List<Integer> getChannelIdList() {
        List<Integer> list = new ArrayList<Integer>();

        list.add(ChannelId.CH5804); // DIA9002
        list.add(ChannelId.CH5805); // DIA9003
        list.add(ChannelId.CH5830); // DIA9026
        list.add(ChannelId.CH5831); // DIA9027

        list.add(ChannelId.CH5802); // DIA9000
        list.add(ChannelId.CH5809); // DIA9007

        list.add(ChannelId.CH5832); // DIA9028
        list.add(ChannelId.CH5814); // DIA9016

        list.add(ChannelId.CH5828); // DIA9024
        list.add(ChannelId.CH5829); // DIA9025
        list.add(ChannelId.CH5827); // DIA9023
        list.add(ChannelId.CH5826); // DIA9022

        list.add(ChannelId.CH302); // DBC_ITrainSpeedInt_15

        list.add(ChannelId.CH5835); // DIA902B
        list.add(ChannelId.CH5836); // DIA902C

        list.add(ChannelId.CH5837); // DIA902D
        list.add(ChannelId.CH5838); // DIA902E
        list.add(ChannelId.CH5839); // DIA902F

        list.add(ChannelId.CH5211); // CAB0101

        return list;
    }

    private void createMainPanel() {
        int mainX = 40;
        int mainY = 50;
        int mainH = 410;
        int mainW = 810;

        // main wrapper panel
        components.add(new Panel(idGen.nextId(), mainX, mainY, mainH, mainW));

        createLeftTopPanel(mainX, mainY);
        createLeftMiddlePanel(mainX, mainY);
        createLeftBottomPanel(mainX, mainY);
        createCentralPanel(mainX, mainY);
        createRightPanel(mainX, mainY);

        components.add(new Label(mainX + 350, mainY + 285 + 65, 50, 150, "SLT").addClassName("titleLabel"));
    }

    private void createLeftTopPanel(int mainX, int mainY) {
        int panelX = mainX + 10;
        int panelY = mainY + 10;
        int panelH = 250;
        int panelW = 250;

        int leftWidgetBoxX = panelX + 30;
        int rightWidgetBoxX = panelX + 160;
        int firstRowWidgetsY = panelY + 10;
        int secondRowWidgetsY = panelY + 80;

        int ledExteriorBoxSize = 50;
        int ledInteriorBoxSize = 44;

        int ledH = 13;
        int ledW = 33;

        components.add(new Panel(idGen.nextId(), panelX, panelY, panelH, panelW).setColor(COLOR.GREY));

        components
                .add(new Panel(idGen.nextId(), leftWidgetBoxX, firstRowWidgetsY, ledExteriorBoxSize, ledExteriorBoxSize)
                        .setColor(COLOR.WHITE));
        components.add(
                new Panel(idGen.nextId(), rightWidgetBoxX, firstRowWidgetsY, ledExteriorBoxSize, ledExteriorBoxSize)
                        .setColor(COLOR.WHITE));
        components.add(
                new Panel(idGen.nextId(), leftWidgetBoxX, secondRowWidgetsY, ledExteriorBoxSize, ledExteriorBoxSize)
                        .setColor(COLOR.WHITE));
        components.add(
                new Panel(idGen.nextId(), rightWidgetBoxX, secondRowWidgetsY, ledExteriorBoxSize, ledExteriorBoxSize)
                        .setColor(COLOR.WHITE));

        components.add(new Panel(idGen.nextId(), leftWidgetBoxX + 3, firstRowWidgetsY + 3, ledInteriorBoxSize,
                ledInteriorBoxSize));
        components.add(new Panel(idGen.nextId(), rightWidgetBoxX + 3, firstRowWidgetsY + 3, ledInteriorBoxSize,
                ledInteriorBoxSize));
        components.add(new Panel(idGen.nextId(), leftWidgetBoxX + 3, secondRowWidgetsY + 3, ledInteriorBoxSize,
                ledInteriorBoxSize));
        components.add(new Panel(idGen.nextId(), rightWidgetBoxX + 3, secondRowWidgetsY + 3, ledInteriorBoxSize,
                ledInteriorBoxSize));

        components.add(new Led(idGen.nextId(), leftWidgetBoxX + 9, firstRowWidgetsY + 20, ledH, ledW, GREEN_LED,
                getChannelName(ChannelId.CH5804)));
        components.add(new Led(idGen.nextId(), rightWidgetBoxX + 9, firstRowWidgetsY + 20, ledH, ledW, GREEN_LED,
                getChannelName(ChannelId.CH5805)));
        components.add(new Led(idGen.nextId(), leftWidgetBoxX + 9, secondRowWidgetsY + 20, ledH, ledW, GREEN_LED,
                getChannelName(ChannelId.CH5830)));
        components.add(new Led(idGen.nextId(), rightWidgetBoxX + 9, secondRowWidgetsY + 20, ledH, ledW, GREEN_LED,
                getChannelName(ChannelId.CH5831)));

        components.add(new Label(leftWidgetBoxX - 25, firstRowWidgetsY + 53, 20, 100, "Cabine 1 Actief")
                .addClassName("blackLabel"));
        components.add(new Label(rightWidgetBoxX - 25, firstRowWidgetsY + 53, 20, 100, "Cabine 2 Actief")
                .addClassName("blackLabel"));
        components.add(new Label(leftWidgetBoxX - 25, secondRowWidgetsY + 53, 20, 100, "Stroomafnemer 1")
                .addClassName("blackLabel"));
        components.add(new Label(rightWidgetBoxX - 25, secondRowWidgetsY + 53, 20, 100, "Stroomafnemer 2")
                .addClassName("blackLabel"));

        components.add(new Panel(idGen.nextId(), leftWidgetBoxX, panelY + 155, 85, 50).setColor(COLOR.WHITE));

        components.add(new Switch(idGen.nextId(), leftWidgetBoxX + 14, panelY + 165, 25, 25,
                new SwitchSettings().setOffAngle(135L).setOnAngle(45L), getChannelName(ChannelId.CH5802)));
        components
                .add(new Label(leftWidgetBoxX + 6, panelY + 185, 20, 100, "off").addClassName("blackLabel leftLabel"));
        components
                .add(new Label(leftWidgetBoxX + 35, panelY + 185, 20, 100, "on").addClassName("blackLabel leftLabel"));

        components.add(new Switch(idGen.nextId(), leftWidgetBoxX + 14, panelY + 205, 25, 25,
                new SwitchSettings().setOffAngle(135L).setOnAngle(45L), getChannelName(ChannelId.CH5809)));
        components
                .add(new Label(leftWidgetBoxX + 6, panelY + 225, 20, 100, "off").addClassName("blackLabel leftLabel"));
        components
                .add(new Label(leftWidgetBoxX + 35, panelY + 225, 20, 100, "on").addClassName("blackLabel leftLabel"));

        components.add(new Label(leftWidgetBoxX + 55, panelY + 170, 20, 100, "In opzending")
                .addClassName("blackLabel leftLabel"));
        components.add(
                new Label(leftWidgetBoxX + 55, panelY + 211, 20, 100, "Afslepen").addClassName("blackLabel leftLabel"));

    }

    private void createLeftMiddlePanel(int mainX, int mainY) {
        int panelX = mainX + 10;
        int panelY = mainY + 270;
        int panelH = 60;
        int panelW = 250;

        components.add(new Panel(idGen.nextId(), panelX, panelY, panelH, panelW).setColor(COLOR.GREY));
        components.add(new Button(idGen.nextId(), panelX + 30, panelY + 6, 50, 50, RED_BUTTON,
                NS.CIRCUIT_BREAKER_SWITCH));
        components.add(
                new Label(panelX + 85, panelY + 24, 20, 150, "Snelschakelaaruit").addClassName("blackLabel leftLabel"));
    }

    private void createLeftBottomPanel(int mainX, int mainY) {
        int panelX = mainX + 10;
        int panelY = mainY + 340;
        int panelH = 60;
        int panelW = 250;

        components.add(new Panel(idGen.nextId(), panelX, panelY, panelH, panelW).setColor(COLOR.GREY));
        components.add(new Button(idGen.nextId(), panelX + 30, panelY + 6, 50, 50, WHITE_BUTTON,
                getChannelName(ChannelId.CH5814)));
        components.add(new Value(idGen.nextId(), panelX + 43, panelY + 23, 20, 30,
                getValueSettings(), NS.DIA_9016_VALUE));
        components
                .add(new Label(panelX + 85, panelY + 24, 20, 150, "Storingsrit").addClassName("blackLabel leftLabel"));
    }

    private void createCentralPanel(int mainX, int mainY) {
        int panelX = mainX + 270;
        int panelY = mainY + 10;
        int panelH = 310;
        int panelW = 300;

        components.add(new Panel(idGen.nextId(), panelX, panelY, panelH, panelW).setColor(COLOR.GREY));

        createPanelWithButtonAndLabel(panelX + 10, panelY + 10, RED_BUTTON, ChannelId.CH5828, "Sluimer");
        createPanelWithButtonAndLabel(panelX + 83, panelY + 10, RED_BUTTON, ChannelId.CH5829, "Reinigen");
        createPanelWithButtonAndLabel(panelX + 156, panelY + 10, BLUE_BUTTON, ChannelId.CH5827, "Gereed");
        createPanelWithButtonAndLabel(panelX + 228, panelY + 10, GREEN_BUTTON, ChannelId.CH5826, "Rijvaardig");

        components.add(new Panel(idGen.nextId(), panelX + 55, panelY + 110, 190, 190));
        // Add speed gauge
        String speedChannel = getChannelName(ChannelId.CH302);
        components.add(new Gauge(idGen.nextId(), panelX + 61, panelY + 116, 180, 180, new GaugeSettings()
                .setDials(Arrays.asList(new DialSettings().setAngleStart(-225D).setAngleEnd(45D)
                        .setArcStrokeFill("#333").setStartValue(0D).setEndValue(160D).setMajorStrokeFill("white")
                        .setMajorStrokeLength(-10D).setMinorStrokeLength(-6D).setMinorStrokeFill("white")
                        .setMinorIncrement(5D).setMinorStrokeWidth(2D).setMajorIncrement(10D).setId(speedChannel)
                        .setLabelIncrement(10D).setLabelRadius(0.80D).setPointerFill("white").setPointerStroke("gray")
                        .setRadius(0.55D).setPointerRadius(0.54D).setDisplayValueFontSize(12)))
                .setFontSize(12).setPivotRadius(0.15D)
                .setLabels(Arrays.asList(new LabelSettings().setText("KM/H").setFontSize(10D))), speedChannel));
    }

    private void createPanelWithButtonAndLabel(int xOffset, int yOffset, ButtonSettings buttonSettings, int channelId,
            String label) {
        components.add(new Panel(idGen.nextId(), xOffset, yOffset, 60, 60).setColor(COLOR.WHITE));
        components.add(new Button(idGen.nextId(), xOffset + 16, yOffset + 8, 30, 30, buttonSettings,
                getChannelName(channelId)));
        components.add(new Label(xOffset, yOffset + 40, 20, 60, label).addClassName("blackLabel"));
    }

    private void createRightPanel(int mainX, int mainY) {
        int panelX = mainX + 580;
        int panelY = mainY + 10;
        int panelH = 390;
        int panelW = 220;

        components.add(new Panel(idGen.nextId(), panelX, panelY, panelH, panelW).setColor(COLOR.GREY));

        components.add(new Lever(idGen.nextId(), panelX + 130, panelY + 10, 150, 70,
                new LeverSettings().setValues(new String[] { "A", "O", "V" }), NS.DIRECTION_LEVER));

        components.add(new Lever(idGen.nextId(), panelX + 10, panelY + 170, 190, 90,
                new LeverSettings().setValues(new String[] { "--", "-", "O", "+" }), NS.TRACTION_BRAKE_LEVER));

        components.add(new ClearButton(idGen.nextId(), panelX + 139, panelY + 300, 50, 50, EMG_BUTTON,
                getChannelName(ChannelId.CH5211)));
        components.add(new Label(panelX + 140, panelY + 350, 20, 50, "Noodknop").addClassName("blackLabel"));
    }

    public String getChannelName(int channelId) {
        channelConf = channelConfiguration.getConfig(channelId);
        return channelConf.getShortName() != null ? channelConf.getShortName() : "";
    }

    /**
     * Return a pre-configured value settings.
     * 
     * @return instance of the ValueSettings
     */
    protected static ValueSettings getValueSettings() {
        return new ValueSettings().setDefaultColor("red")
                .setCssClassValue("titleLabel")
                .setTextAnchor(TextAnchor.START)
                .setColorForChanCategory(ChannelCategory.FAULT, "red")
                .setColorForChanCategory(ChannelCategory.FAULT1, "red")
                .setColorForChanCategory(ChannelCategory.FAULT2, "red")
                .setColorForChanCategory(ChannelCategory.WARNING, "orange")
                .setColorForChanCategory(ChannelCategory.WARNING1, "orange")
                .setColorForChanCategory(ChannelCategory.WARNING2, "orange")
                .setColorForChanCategory(ChannelCategory.INVALID, "purple");
    }

    @Override
    public List<Component> getLayout() {
        List<Component> layout = new ArrayList<Component>();
        layout.addAll(components);
        layout.addAll(labels);

        return layout;
    }

    @Override
    public List<Label> getLabels() {
        return labels;
    }

    @Override
    public int getWidth() {
        return 883;
    }

    @Override
    public int getHeight() {
        return 700;
    }

    @Override
    public List<Integer> getChannelIds() {
        return channelIds;
    }

    @Override
    public boolean hasTransientStates() {
        return false;
    }

    @Override
    public int getDataInterval() {
        return 5000;
    }

    @Override
    public int getDataIntervalLimit() {
        return 300000;
    }

    @Override
    public boolean isOnlyOneCabConfig() {
        return true;
    }
}
