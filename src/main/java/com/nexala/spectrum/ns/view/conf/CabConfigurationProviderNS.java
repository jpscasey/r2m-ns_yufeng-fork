package com.nexala.spectrum.ns.view.conf;

import com.google.inject.Inject;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.cab.AbstractCabConfigurationProvider;
import com.nexala.spectrum.view.conf.cab.CabConfiguration;

public class CabConfigurationProviderNS extends AbstractCabConfigurationProvider {

    @Inject
    PluginFactory factory;

    @Inject
    CabConfiguration configuration;

    /**
     * @see com.nexala.spectrum.rest.service.CabConfigurationProvider#getCabConfiguration(java.lang.Integer)
     */
    @Override
    public CabConfiguration getCabConfiguration(Integer vehicleId) {
        return configuration;
    }

    @Override
    public String getPlugin() {
        return null;
    }
}
