package com.nexala.spectrum.ns.view.conf;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.CalculatedChannel;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.ns.db.dao.HistoricEventChannelValueDao;

public class CalculatedChannelConfigurationNS implements CalculatedChannelConfiguration {

    @Inject
    private ChannelConfiguration channelConf;

    @Inject
    private HistoricEventChannelValueDao ecvDao;

    private ChannelCollection cc = new ChannelCollection();

    @Override
    public List<CalculatedChannel> getCalculatedChannels() {

        List<CalculatedChannel> calcChans = newArrayList();

        return calcChans;
    }
    
    private ChannelCollection channelListToCollection(List<Channel<?>> list){
        ChannelCollection cc = new ChannelCollection();
        for (Channel<?> ch : list){
            if (ch != null && ch.getValue() != null){
                cc.put(ch);
            }
        }
        return cc;
    }

    private boolean isValid(Channel<?> ch) {
        return ch != null && ch.getValue() != null ? true : false;
    }
}
