package com.nexala.spectrum.ns.view.conf;

import static com.nexala.spectrum.view.conf.DrillDownGridStyle.DRILLDOWN_CHANNELNAME;
import static com.nexala.spectrum.view.conf.DrillDownGridStyle.DRILLDOWN_COMMON;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.rest.data.beans.Formation;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.DrillDownConfiguration;
import com.nexala.spectrum.view.conf.TextColumnBuilder;

public class DrillDownConfigurationNS implements DrillDownConfiguration {

    private Column createUnitColumn(Unit unit) {
        return new TextColumnBuilder().displayName(unit.getUnitNumber())
                .name(unit.getId().toString()).width(55)
                .styles(DRILLDOWN_COMMON).sortable(false).build();
    }

    @Override
    public List<Column> getColumns(String fleetId, List<Formation> formationList) {
        List<Column> columns = new ArrayList<Column>();

        columns.add(new TextColumnBuilder().name(Spectrum.COLUMN_CHANNEL_DESCRIPTION)
                .width(255).styles(DRILLDOWN_CHANNELNAME).sortable(false)
                .build());

        columns.add(new TextColumnBuilder()
                .name(Spectrum.COLUMN_ALWAYS_DISPLAYED).visible(false).build());

        for (Formation formation : formationList) {
            Unit unit = formation.getUnit();
            if (unit != null) {
                Column col = createUnitColumn(unit);
                columns.add(col);
            }
        }

        return columns;
    }
    
}
