package com.nexala.spectrum.ns.view.conf;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.conf.DefaultUnitConfiguration;
import com.nexala.spectrum.view.conf.DetailScreen;
import com.nexala.spectrum.view.conf.MapConfiguration;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;
import com.nexala.spectrum.view.conf.unit.DataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.RecoveryConfiguration;
import com.nexala.spectrum.view.conf.unit.StaticDataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;

public class UnitConfigurationVirmImpl extends DefaultUnitConfiguration {

    @Inject
    public UnitConfigurationVirmImpl(ApplicationConfiguration appConfig) {
        super(appConfig);
    }

    @Inject(optional = false)
    private UnitDetailConfiguration unitConf;

    @Inject(optional = false)
    private ChannelDataConfiguration channelConf;

    @Inject(optional = false)
    private StaticDataPlotConfiguration staticDataplotConf;

    @Inject(optional = false)
    private DataPlotConfiguration chartConf;

    @Inject(optional = false)
    private MapConfiguration mapConf;

    @Inject(optional = true)
    private CabConfigurationProviderNS cabConf;

    @Inject(optional = true)
    private RecoveryConfiguration recoveryConf;

    @Override
    public int getUnitSelectorWidth() {
        return 250;
    }

    @Override
    public List<DetailScreen> getScreens(Licence licence) {
        List<DetailScreen> screens = new ArrayList<DetailScreen>();

        if (unitConf != null && licence.hasResourceAccess(unitConf.getRequiredRole())) {
            screens.add(unitConf);
        }

        if (channelConf != null && licence.hasResourceAccess(channelConf.getRequiredRole())) {
            screens.add(channelConf);
        }

        if (staticDataplotConf != null && licence.hasResourceAccess(staticDataplotConf.getRequiredRole())) {
            screens.add(staticDataplotConf);
        }

        if (chartConf != null && licence.hasResourceAccess(chartConf.getRequiredRole())) {
            screens.add(chartConf);
        }

        if (mapConf != null && licence.hasResourceAccess(mapConf.getRequiredRole())) {
            screens.add((DetailScreen) mapConf);
        }

        if (cabConf != null && licence.hasResourceAccess(cabConf.getRequiredRole())) {
            screens.add(cabConf);
        }

        if (recoveryConf != null && licence.hasResourceAccess(recoveryConf.getRequiredRole())) {
            screens.add(recoveryConf);
        }

        return screens;
    }
}