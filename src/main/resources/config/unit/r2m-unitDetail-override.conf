r2m: {
    unitDetail: {
    	showEventHistoryButton: true,
        headerFields: [
          {
              name: "TimeStamp",
              displayName: "TimeStamp",
              format: "{date}"
          },
          {
              name: "UnitNumber",
              displayName: "Unit Number"
          },         
          {
              name: "Speed",
              displayName: "Speed"
          }, 
          {
              name: "Location",
              displayName: "Location"
          },          
          {
              name: "Headcode",
              displayName: "Train Number"
          },       
          {
              name: "LeadingVehicle",
              displayName: "Leading Vehicle"
          },               
          {
              name: "UnitStatus",
              displayName: "Asset Status",
          },
          {
              name: "UnitMaintLoc",
              displayName: "Maximo Location",
          },
          {
          	  name: "LastMaintDate",
          	  displayName: "Date Last Maintained",
          	  format: "{date}"
          },       
          {
              name: "Diagram",
              displayName: "Diagram",
              format: "",
              visible: false
          }
        ],
        
        udPanel: {
            rows: [
                {
                    position: 1,
                    cells: [
                        {
                            title: "Live Events",
                            name: "liveEvents",
                            mode: "liveEvents",
                            filters: [
                            	{
                                 name: "isAcknowledged",
                                 label: "Events",
                                 type: "multiple_select",
                                 defaultOptions: [
                                     {
                                      id: "all",
                                      displayName: "All"
                                     },
                                     {
                                      id: "true",
                                      displayName: "Acknowledged"
                                     },
                                     {
                                      id: "false",
                                      displayName: "Not Acknowledged"
                                     }
                                 ]
                                },
                                {
                                 name: "faultTypeId",
                                 label: "Priority",
                                 type: "multiple_select",
                                 defaultOptions: [
                                     {
                                      id: "all",
                                      displayName: "All"
                                     }
                                 ]
                                },
                                {
                                 name: "categoryId",
                                 label: "Category",
                                 type: "multiple_select",
                                 defaultOptions: [
                                     {
                                      id: "allExceptTest",
                                      displayName: "All Except Test" 
                                     },
                                     {
                                      id: "all",
                                      displayName: "All"
                                     }
                                 ]
                                }
                                
                            ],
                            columns: [
                                {
                                    name: "id",
                                    handler: "openEventDetail",
                                    visible: false
                                },
                                {
                                    displayName: "",
                                    name: "typeColor",
                                    handler: "openEventDetail",
                                    format: "<div class = 'eventIcon' style = 'background: {string}'></div>",
                                    width: 20
                                },
                                {
                                    displayName: "Date",
                                    name: "createTime",
                                    handler: "openEventDetail",
                                    format: "{date}",
                                    width: 50
                                },
                                {
                                    displayName: "Train Number",
                                    name: "headcode",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Code",
                                    name: "eventCode",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Description",
                                    name: "description",
                                    handler: "openEventDetail",
                                    width: 120
                                },
                                {
                                    displayName: "Maximo ID",
                                    name: "maximoId",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Category",
                                    name: "category",
                                    handler: "openEventDetail",
                                    width: 50
                                }
                                
                            ],
                            rowMouseOver: [
                                {
                                 label: "Type",
                                 value: "type"
                                },
                                {
                                 label: "Category",
                                 value: "category"
                                },
                                {
                                 label: "Description",
                                 value: "description"
                                }
                            ]
                        },
                        {
                            title: "Recent Events",
                            name: "recentEvents",
                            mode: "recentEvents",
                            filters: [
                            	{
                                 name: "faultTypeId",
                                 label: "Priority",
                                 type: "multiple_select",
                                 defaultOptions: [
                                     {
                                      id: "all",
                                      displayName: "All"
                                     }
                                 ]
                                },
                                {
                                 name: "categoryId",
                                 label: "Category",
                                 type: "multiple_select",
                                 defaultOptions: [
                                     {
                                      id: "allExceptTest",
                                      displayName: "All Except Test" 
                                     },
                                     {
                                      id: "all",
                                      displayName: "All"
                                     }
                                 ]
                                }
                            ],
                            columns: [
                                {
                                    name: "id",
                                    handler: "openEventDetail",
                                    visible: false
                                },
                                {
                                    displayName: "",
                                    name: "typeColor",
                                    handler: "openEventDetail",
                                    format: "<div class = 'eventIcon' style = 'background: {string}'></div>",
                                    width: 20
                                },
                                {
                                    displayName: "Date",
                                    name: "createTime",
                                    handler: "openEventDetail",
                                    format: "{date}",
                                    width: 50
                                },
                                {
                                    displayName: "Train Number",
                                    name: "headcode",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Code",
                                    name: "eventCode",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Description",
                                    name: "description",
                                    handler: "openEventDetail",
                                    width: 120
                                },
                                {
                                    displayName: "Maximo ID",
                                    name: "maximoId",
                                    handler: "openEventDetail",
                                    width: 50
                                },
                                {
                                    displayName: "Category",
                                    name: "category",
                                    handler: "openEventDetail",
                                    width: 50
                                }
                            ],
                            rowMouseOver: [
                                {
                                 label: "Type",
                                 value: "type"
                                },
                                {
                                 label: "Category",
                                 value: "category"
                                },
                                {
                                 label: "Description",
                                 value: "description"
                                }
                            ]
                        }
                    ]
                },
                {
                    position: 2,
                    cells: [
                        {
                            title: "Maintenance Records",
                            name: "workOrders",
                            mode: "workOrders",
                            filters: [
                            	{
                            		name: "reporter",
                            		label: "RTM-O only",
                            		type: "checkbox",
                            		checkedAsDefault: false,
                            		includeValues: true,
                            		defaultOptions: [
                            			{
                            				id: "RTM-O",
                            				displayName: "RTM-O"
                            			}
                            		]
                            	},
                            	{
                            		name: "status",
                            		label: "Exclude closed WO/SR",
                            		type: "checkbox",
                            		checkedAsDefault: false,
                            		includeValues: false,
                            		defaultOptions: [
                            			{
                            				id: "SLUITEN",
                            				displayName: "SLUITEN"
                            			},
                            			{
                            				id: "GESLOTEN",
                            				displayName: "GESLOTEN"
                            			},
                                        {
                                            id: "ANNUL",
                                            displayName: "ANNUL"
                                        },
                                        {
                                            id: "BWHIST",
                                            displayName: "BWHIST"
                                        },
                                        {
                                            id: "GEREED",
                                            displayName: "GEREED"
                                        }
                            		]
                            	},
                            	{
                            		name: "symptom",
                            		label: "Symptom",
                            		type: "input_text"
                            	}
                            ],
                            columns: [
                                {
                                    name: "id",
                                    visible: false
                                },
                                {
                                    displayName: "Maximo ID",
                                    name: "maximoId",
                                    type: "text",
                                    width: 50,
                                    sortable: true
                                },
                                {
                                    displayName: "Date",
                                    name: "createTime",
                                    format: "{date}",
                                    type: "timestamp",
                                    width: 50,
                                    sortable: true
                                },
                                {
                                    displayName: "Symptom",
                                    name: "symptom",
                                    width: 40,
                                    type: "text",
                                    sortable: true
                                },
                                {
                                    displayName: "Description",
                                    name: "description",
                                    type: "text",
                                    width: 120,
                                    sortable: true
                                },
                                {
                                    displayName: "Maintenance Type",
                                    name: "maintenanceType",
                                    type: "text",
                                    width: 70,
                                    sortable: true
                                },
                                {
                                    displayName: "Status",
                                    name: "status",
                                    type: "text",
                                    width: 35,
                                    sortable: true
                                },
                                {
                                    displayName: "Operational Priority",
                                    name: "operationalPriority",
                                    type: "analogue",
                                    width: 70,
                                    sortable: true
                                },
                                {
                                    displayName: "Safety Priority",
                                    name: "safetyPriority",
                                    type: "analogue",
                                    width: 50,
                                    sortable: true
                                },
                                {
                                    displayName: "Fault Code",
                                    name: "faultCode",
                                    type: "text",
                                    width: 50,
                                    sortable: true
                                },
                                {
                                    displayName: "Work Location",
                                    name: "workLocation",
                                    type: "text",
                                    width: 50,
                                    sortable: true
                                },
                                {
                                    displayName: "Reporter",
                                    name: "reporter",
                                    type: "text",
                                    width: 90,
                                    sortable: true
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        workOrder: [
            {
             id: "maximoId",
             db: "MaximoID",
             type: "string"
            },
            {
             id: "createTime",
             db: "CreateTime",
             type: "timestamp"
            },
            {
             id: "symptom",
             db: "Symptom",
             type: "string"
            },
            {
             id: "description",
             db: "Description",
             type: "string"
            },
            {
             id: "maintenanceType",
             db: "MaintenanceType",
             type: "string"
            },
            {
             id: "status",
             db: "Status",
             type: "string"
            },
            {
             id: "operationalPriority",
             db: "MbnUrgency",
             type: "int"
            },
            {
             id: "safetyPriority",
             db: "VkbPriority",
             type: "string"
            },
            {
             id: "faultCode",
             db: "FaultCode",
             type: "string"
            },
            {
             id: "workLocation",
             db: "WorkLocation",
             type: "string"
            },
            {
             id: "reporter",
             db: "NotificationSource",
             type: "string"
            },
            {
             id: "maximoType",
             db: "MaximoType",
             type: "string"
            }
        ],
        maximoURLs: [
            {
             maximoType: "Work Order",
             url: "https://"${?MAXIMO_URL}"/maximo/ui/?event=loadapp&value=ntplusawo&login=true&additionalevent=useqbe&additionaleventvalue=wonum=="
            },
            {
             maximoType: "Service Request",
             url: "https://"${?MAXIMO_URL}"/maximo/ui/?event=loadapp&value=ntpluspsr&login=true&additionalevent=useqbe&additionaleventvalue=ticketid="
            }
        ],
        plugins: {
            name: "nx.ns.diagram.DiagramImpl",
            sources: [
                "js/nexala.ns.diagram.js",
                "js/nexala.ns.diagram.cab.Bx.js",
                "js/nexala.ns.diagram.cab.mBx.js",
                "js/nexala.ns.diagram.cab.mABK.js"
                
                "js/nexala.ns.diagram.cab.mBvk1.js"
                "js/nexala.ns.diagram.cab.mBvk2.js"
                "js/nexala.ns.diagram.cab.ABv3_4.js"
                "js/nexala.ns.diagram.cab.ABv5.js"
                "js/nexala.ns.diagram.cab.ABv6.js"
                "js/nexala.ns.diagram.cab.mBv7.js"
            ]
        }
    }
}