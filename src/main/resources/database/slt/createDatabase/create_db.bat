@echo on
@REM setlocal
@echo param1 - %1
@echo param2 - %2

IF "%~1"=="" (GOTO missingparameters) 
@echo DB_NAME - %1
SET DB_NAME=%1 

IF "%~2"=="" (GOTO missingparameters) 
@echo setserverdata - %2
SET SQLSERVER=%2

:dbstart
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i create_spectrum_db.sql -d master% -E
:script001
sqlcmd -S %SQLSERVER% -v db_name=%DB_NAME% -i schema_and_data_before_flyway.sql -d %DB_NAME% -E -b

@GOTO end

:missingparameters
@echo Missing parameters

:end
