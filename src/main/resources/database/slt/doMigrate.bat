CALL doPrepare.bat

IF ERRORLEVEL 1 (
    ECHO ERROR - Environment configuration incorrect
    EXIT /B 1
)

SET FW_LOCATIONS=filesystem:.\migrations,filesystem:.\views,filesystem:.\functions,filesystem:.\procedures

ECHO INFO - Migrating %FW_ENVIRONMENT% at %FW_URL%

flyway -url=%FW_URL%^
    -user=%FW_USERNAME%^
    -password=%FW_PASSWORD%^
    -locations=%FW_LOCATIONS%^
    -mixed=true^
    -outOfOrder=true^
    -cleanDisabled=true^
    migrate
