SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_CheckZero', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_CheckZero' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_CheckZero () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_CheckZero]
	(@val varchar(50))  
	RETURNS int 
as
begin
	declare @numVal int, @charCount int
	
	if (@val is null)
		return -2;
	
	set @charCount = 1

	while @charCount <= len(@val)
	begin
		if (substring(@val, @charCount, 1) <> '0')
			return -1;
		set @charCount = @charCount + 1;
	end

	return 0;
end -- function
GO
