SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_DataForGPSVisualizer', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_DataForGPSVisualizer' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_DataForGPSVisualizer () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_DataForGPSVisualizer]
(
	@LocationName varchar(50)
	, @MinLat decimal(8,6)
	, @MaxLat decimal(8,6)
	, @MinLng decimal(8,6)
	, @MaxLng decimal(8,6)
)
RETURNS varchar(1000)
AS
BEGIN
	DECLARE @string varchar(1000)
	
	SET @string = 'name,latitude,longitude
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MaxLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MaxLat AS varchar(9)) + ', ' + CAST(@MaxLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MaxLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9)) + '
' + @LocationName + ', ' + CAST(@MinLat AS varchar(9)) + ', ' + CAST(@MinLng AS varchar(9))

	RETURN @string
END
GO
