SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_FleetFormationByUnitID', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_FleetFormationByUnitID' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_FleetFormationByUnitID () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION  [dbo].[FN_FleetFormationByUnitID]
(
	@UnitID INT
	, @Datetime DATETIME2(2)
)
RETURNS INT
AS
BEGIN

	DECLARE @FleetFormationID INT
	
	SET @FleetFormationID = (
				SELECT TOP (1) ff.ID
				FROM dbo.FleetFormation ff
				CROSS APPLY dbo.SplitString(ff.UnitIDList,',') ss
				WHERE ss.Item = @UnitID
					AND ff.ValidFrom >= @Datetime
					AND ff.ValidTo < @Datetime
				ORDER BY ff.ValidTo DESC)

	RETURN @FleetFormationID 

END -- function
GO
