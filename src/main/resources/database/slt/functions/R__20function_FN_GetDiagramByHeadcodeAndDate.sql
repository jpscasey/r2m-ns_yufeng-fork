SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GetDiagramByHeadcodeAndDate', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GetDiagramByHeadcodeAndDate' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GetDiagramByHeadcodeAndDate () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_GetDiagramByHeadcodeAndDate]
(
	@Headcode char(4)
	, @StartDate datetime2(3)
	, @EndDate datetime2(3)
)
RETURNS varchar(500)
AS
BEGIN
	DECLARE @result varchar(500) = '';
	
	

	WITH CteJourneyList AS
	(
		SELECT 
			JourneyID
			, COUNT(*) AS RecordNumber
		FROM TrainPassage tp
		WHERE Headcode = @Headcode
			AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		GROUP BY 
			JourneyID
	)
	SELECT
		@result = @result + 
			ISNULL(
				RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
				+ ' ' + sl.Tiploc
				+ '-' + el.Tiploc
				+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
			, '') + ' ('+ CAST(SUM(RecordNumber) AS varchar(5)) + ') ' + HeadcodeDesc + '
'
	FROM CteJourneyList
	INNER JOIN Journey ON CteJourneyList.JourneyID = Journey.ID
	LEFT JOIN SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN Location el ON el.ID = esp.LocationID
	GROUP BY
		StartTime
		, sl.Tiploc
		, el.Tiploc
		, EndTime
		, HeadcodeDesc

	SET @result = LEFT(@result, LEN(@result) - 2 )

	-- Return the result of the function
	RETURN @result

END
GO
