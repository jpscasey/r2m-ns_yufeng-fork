SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update Vehicle table
------------------------------------------------------------

RAISERROR ('-- change VehicleNumber and Type in Vehicle table for 26_AB Vehicles', 0, 1) WITH NOWAIT
GO

BEGIN

   update [dbo].[Vehicle] 
   set type = 'B3', VehicleNumber = replace(VehicleNumber, 'AB', 'B3') 
   where VehicleNumber like '26%AB' or (UnitID like '26%' and type = 'AB');

END
GO