SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Add New Channels
------------------------------------------------------------

RAISERROR ('-- add records in Channel table', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT Channel ON;

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId)
VALUES (8086, NULL, 8086, 1, 'Laatste breedtegraad', 'VT_GPSLatitude', 'A331','decimal (9,6)', 2, NULL, '°', 0, 5320, 'SLTValues', NULL, 'Laatste breedtegraad', NULL, 1 , 0, NULL, NULL, 0, NULL, 2, 2, 6);

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId) 
VALUES (8087, NULL, 8087, 1, 'Laatste lengtegraad', 'VT_GPSLongitude', 'A332','decimal (9,6)', 2, NULL, '°', 0, 255, 'SLTValues', NULL, 'Laatste lengtegraad', NULL, 1 , 0, NULL, NULL, 0, NULL, 1, 1, 6);
SET IDENTITY_INSERT Channel OFF;