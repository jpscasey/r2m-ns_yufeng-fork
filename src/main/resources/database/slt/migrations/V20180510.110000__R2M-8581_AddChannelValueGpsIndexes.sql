
/****** Object:  Index [IX_CL_ChannelValueGps_TimeStamp_UnitId]    Script Date: 10/05/2018 11:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_TimeStamp_UnitId')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_TimeStamp_UnitId] ON [dbo].[ChannelValueGps]
(
	[TimeStamp] ASC,
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO

/****** Object:  Index [IX_ChannelValueGps_UnitID_TimeStamp]    Script Date: 10/05/2018 11:26:30 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_UnitId_TimeStamp')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_UnitId_TimeStamp] ON [dbo].[ChannelValueGps]
(
	[UnitId] ASC,
	[TimeStamp] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
GO

