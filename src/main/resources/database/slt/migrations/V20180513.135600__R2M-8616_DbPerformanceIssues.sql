SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update Vehicle table
------------------------------------------------------------

RAISERROR ('-- New indexes needed for perrformance ', 0, 1) WITH NOWAIT
GO

IF CHARINDEX ('developer', @@VERSION) > 0 OR CHARINDEX ('Enterprise', @@VERSION) > 0 
BEGIN

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_ChannelValue_Unitid_TimeStamp_partitioncol')
		CREATE CLUSTERED INDEX [IX_ChannelValue_Unitid_TimeStamp_partitioncol] ON [dbo].[ChannelValue]
		(
			UnitID,
			[TimeStamp] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PS_ChannelValue_Date]([TimeStamp])

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_FaultMeta_CategoryID')
		CREATE NONCLUSTERED INDEX IX_FaultMeta_CategoryID ON [dbo].[FaultMeta] ([CategoryID]) INCLUDE ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_EventChannelValue_UnitID_TimeStamp_andIncludedCols')
		CREATE INDEX IX_EventChannelValue_UnitID_TimeStamp_andIncludedCols ON EventChannelValue ([UnitID],[TimeStamp]) include ([ChannelID], [Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  ON [PS_ChannelValue_Date]([TimeStamp])

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_ServiceRequest_UnitID')
		CREATE INDEX IX_ServiceRequest_UnitID ON ServiceRequest ([UnitID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_Channel_TypeID_andIncludedCols')
		CREATE INDEX IX_Channel_TypeID_andIncludedCols ON Channel ([TypeID]) include ([ID], [VehicleID], [EngColumnNo], [ChannelGroupID], [Description], [Name], [Ref], [DataType], [ChannelGroupOrder], [UOM], [MinValue], [MaxValue], [HardwareType], [StorageMethod], [Comment], [VisibleOnFaultOnly], [IsAlwaysDisplayed], [IsLookup], [DefaultValue], [IsDisplayedAsDifference], [RelatedChannelID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_EventChannelValue_TimeStampEndTime_TimeStamp')
		CREATE INDEX IX_EventChannelValue_TimeStampEndTime_TimeStamp ON EventChannelValue ([TimeStampEndTime], TimeStamp) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  ON [PS_ChannelValue_Date]([TimeStamp])

END
ELSE
BEGIN
	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_ChannelValue_Unitid_TimeStamp_partitioncol')
		CREATE CLUSTERED INDEX [IX_ChannelValue_Unitid_TimeStamp_partitioncol] ON [dbo].[ChannelValue]
		(
			UnitID,
			[TimeStamp] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_FaultMeta_CategoryID')
		CREATE NONCLUSTERED INDEX IX_FaultMeta_CategoryID ON [dbo].[FaultMeta] ([CategoryID]) INCLUDE ([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_EventChannelValue_UnitID_TimeStamp_andIncludedCols')
		CREATE INDEX IX_EventChannelValue_UnitID_TimeStamp_andIncludedCols ON EventChannelValue ([UnitID],[TimeStamp]) include ([ChannelID], [Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_ServiceRequest_UnitID')
		CREATE INDEX IX_ServiceRequest_UnitID ON ServiceRequest ([UnitID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_Channel_TypeID_andIncludedCols')
		CREATE INDEX IX_Channel_TypeID_andIncludedCols ON Channel ([TypeID]) include ([ID], [VehicleID], [EngColumnNo], [ChannelGroupID], [Description], [Name], [Ref], [DataType], [ChannelGroupOrder], [UOM], [MinValue], [MaxValue], [HardwareType], [StorageMethod], [Comment], [VisibleOnFaultOnly], [IsAlwaysDisplayed], [IsLookup], [DefaultValue], [IsDisplayedAsDifference], [RelatedChannelID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	if not exists (SELECT * FROM SYS.INDEXES WHERE NAME ='IX_EventChannelValue_TimeStampEndTime_TimeStamp')
		CREATE INDEX IX_EventChannelValue_TimeStampEndTime_TimeStamp ON EventChannelValue ([TimeStampEndTime], TimeStamp) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 

END

GO