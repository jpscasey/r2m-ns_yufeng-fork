SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update FleetStatus table
------------------------------------------------------------

RAISERROR ('-- update vt_gps channels in Channel table', 0, 1) WITH NOWAIT

GO

IF EXISTS (SELECT 1 FROM dbo.Channel WHERE ID = 8086)
BEGIN
    UPDATE Channel SET Description = 'Laatste GPS breedtegraad', HardwareType = 'Gps' WHERE ID = 8086;
END
GO

IF EXISTS (SELECT 1 FROM dbo.Channel WHERE ID = 8087)
BEGIN
    UPDATE Channel SET Description = 'Laatste GPS lengtegraad', HardwareType = 'Gps' WHERE ID = 8087;
END
GO