SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Create FaultChannelValueGps table
------------------------------------------------------------

RAISERROR ('-- Create FaultChannelValueGps table', 0, 1) WITH NOWAIT
GO


IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = 'FaultChannelValueGps')
BEGIN
	CREATE TABLE [dbo].[FaultChannelValueGps](
		[ID] [bigint] NOT NULL,
		[FaultID] [int] NOT NULL,
		[UnitID] [int] NOT NULL,
		[TimeStamp] [datetime] NOT NULL,
		[Col8086] [decimal](9, 6) NULL,
		[Col8087] [decimal](9, 6) NULL,
		CONSTRAINT [PK_FaultChannelValueGps] PRIMARY KEY CLUSTERED 
	(
		[FaultID] ASC,
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
	) ON [PRIMARY]
END

GO