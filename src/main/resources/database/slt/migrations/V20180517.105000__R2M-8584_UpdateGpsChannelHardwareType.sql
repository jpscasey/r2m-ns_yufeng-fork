SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update FleetStatus table
------------------------------------------------------------

RAISERROR ('-- update vt_gps channels in Channel table', 0, 1) WITH NOWAIT

GO

IF EXISTS (SELECT 1 FROM dbo.Channel WHERE HardwareType = 'Gps')
BEGIN
    UPDATE Channel SET HardwareType = 'Virtual Train' WHERE HardwareType = 'Gps';
END
GO