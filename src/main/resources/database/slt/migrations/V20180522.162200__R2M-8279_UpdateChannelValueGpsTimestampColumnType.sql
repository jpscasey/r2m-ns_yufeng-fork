SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update ChannelValueGps Timestamp column
------------------------------------------------------------

RAISERROR ('-- Update ChannelValueGps Timestamp column', 0, 1) WITH NOWAIT
GO


IF EXISTS (SELECT 1 FROM SYS.indexes where name ='IX_ChannelValueGps_TimeStamp_UnitId')
    DROP INDEX [IX_ChannelValueGps_TimeStamp_UnitId] ON [dbo].[ChannelValueGps]
GO

IF EXISTS (SELECT 1 FROM SYS.indexes where name ='IX_ChannelValueGps_UnitId_TimeStamp')
    DROP INDEX [IX_ChannelValueGps_UnitId_TimeStamp] ON [dbo].[ChannelValueGps]
GO


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ChannelValueGps' AND COLUMN_NAME='Timestamp')
BEGIN
    ALTER TABLE [dbo].[ChannelValueGps]
      ALTER COLUMN Timestamp datetime2(3) NOT NULL
END
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_TimeStamp_UnitId')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_TimeStamp_UnitId] ON [dbo].[ChannelValueGps]
(
    [TimeStamp] ASC,
    [UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_UnitId_TimeStamp')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_UnitId_TimeStamp] ON [dbo].[ChannelValueGps]
(
    [UnitId] ASC,
    [TimeStamp] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
GO