SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Delete duplicate entries and create unique contraint
------------------------------------------------------------

RAISERROR ('-- delete duplicate rows from EventChannelValue', 0, 1) WITH NOWAIT
GO

DELETE FROM EventChannelValue
  WHERE ID IN (
        SELECT t.ID  FROM (
            SELECT ID, RANK() OVER(PARTITION BY UnitID, ChannelID , [Timestamp] ORDER BY ID DESC) AS rank
            FROM EventChannelValue )
             AS t 
        WHERE rank > 1
    )
  
  
RAISERROR ('-- add unique contraint to EventChannelValue', 0, 1) WITH NOWAIT
GO
IF  EXISTS (SELECT * FROM sys.key_constraints WHERE name = N'U_Unit_Timestamp_Channel')
ALTER TABLE [dbo].[EventChannelValue] DROP CONSTRAINT [U_Unit_Timestamp_Channel]
GO

ALTER TABLE [dbo].[EventChannelValue]
ADD CONSTRAINT [U_Unit_Timestamp_Channel] UNIQUE (UnitID, TimeStamp, ChannelID)
GO