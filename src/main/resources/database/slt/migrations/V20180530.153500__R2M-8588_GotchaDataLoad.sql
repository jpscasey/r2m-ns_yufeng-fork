SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

-------------------------------------------------------------------------------
-- Import of static data. The static data was generated from 3 speadsheets
-- https://docs.google.com/spreadsheets/d/1sHRUF1Pn0Rk3f4g2rmPXXbNA_Z8wGUapJc7OeUQT6Kw/edit#gid=837074047
-- https://docs.google.com/spreadsheets/d/1lrPJt8sHeSierpPVxCqzXwtRj2B1WMvB7KWLTbv9-gA/edit#gid=1606251781
-- https://docs.google.com/spreadsheets/d/1mQDKU-Pp5YvIkCnHhEzZYv-Nw14kYKRKuyjX7qwMM0Y/edit#gid=2018179562
-------------------------------------------------------------------------------

RAISERROR ('-- Add AxleCount to VehicleType', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VehicleType' AND COLUMN_NAME='AxleCount')
BEGIN
	ALTER TABLE VehicleType ADD AxleCount int NULL

END 
GO


RAISERROR ('-- Insert data into ComponentType', 0, 1) WITH NOWAIT

GO
SET IDENTITY_INSERT [dbo].[ComponentType] ON 
GO
INSERT INTO ComponentType(ID, Level, Code, Type) VALUES (1, 1, 'UNIT', 'Unit')
INSERT INTO ComponentType(ID,Level, Code, Type) VALUES (2, 2, 'VEHICLE', 'Vehicle')
INSERT INTO ComponentType(ID,Level, Code, Type) VALUES (3, 3, 'AXLE', 'Axle')
INSERT INTO ComponentType(ID,Level, Code, Type) VALUES (4, 4, 'WHEEL', 'Wheel')
GO
SET IDENTITY_INSERT [dbo].[ComponentType] OFF 
GO

RAISERROR ('-- Insert data into VehicleType', 0, 1) WITH NOWAIT
GO
UPDATE Vehicle SET VehicleTypeId = NULL
DELETE FROM vehicletype
INSERT INTO VehicleType (Name, AxleCount) VALUES ('B1', 2)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('B2', 2)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('B3', 2)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mABK1', 3)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mABk2', 3)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mABk3', 3)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mABk4', 3)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mB1', 2)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mB2', 2)
INSERT INTO VehicleType (Name, AxleCount) VALUES ('mB3', 2)

GO

RAISERROR ('-- Set VehicleTypeId on vehicle table', 0, 1) WITH NOWAIT
GO

UPDATE Vehicle SET VehicleTypeId = vtype.ID 
FROM VehicleType vtype WHERE Vehicle.Type = vtype.Name

GO 
RAISERROR ('-- INSERT Units on component table', 0, 1) WITH NOWAIT
GO

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position) 
SELECT 
	1,
	NULL,
	NULL,
	ID,
	1
FROM Unit

GO 
RAISERROR ('-- INSERT Vehicles on component table', 0, 1) WITH NOWAIT
GO

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	2,
	c.ID,
	v.ID,
	u.ID,
	v.VehicleOrder
FROM Vehicle v
INNER JOIN Unit u on u.id = v.UnitId
INNER JOIN Component c on c.UnitID = u.ID AND c.ComponentTypeId = 1

GO 
RAISERROR ('-- INSERT Axles on component table', 0, 1) WITH NOWAIT
GO

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	3,
	c.ID,
	v.ID,
	v.UnitID,
	1
FROM Vehicle v
INNER JOIN VehicleType vt ON v.VehicleTypeID = vt.ID
INNER JOIN Component c on c.VehicleID = v.ID AND c.ComponentTypeId = 2
WHERE vt.AxleCount >= 1

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	3,
	c.ID,
	v.ID,
	v.UnitID,
	2
FROM Vehicle v
INNER JOIN VehicleType vt ON v.VehicleTypeID = vt.ID
INNER JOIN Component c on c.VehicleID = v.ID AND c.ComponentTypeId = 2
WHERE vt.AxleCount >= 2

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	3,
	c.ID,
	v.ID,
	v.UnitID,
	3
FROM Vehicle v
INNER JOIN VehicleType vt ON v.VehicleTypeID = vt.ID
INNER JOIN Component c on c.VehicleID = v.ID AND c.ComponentTypeId = 2
WHERE vt.AxleCount >= 3

GO 
RAISERROR ('-- INSERT Wheels on component table', 0, 1) WITH NOWAIT
GO

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	4,
	c.ID,
	c.VehicleID,
	c.UnitID,
	1
FROM Component c
WHERE c.ComponentTypeId = 3

INSERT INTO Component(ComponentTypeId, ParentId, VehicleId, UnitId, Position)
SELECT 
	4,
	c.ID,
	c.VehicleID,
	c.UnitID,
	2
FROM Component c
WHERE c.ComponentTypeId = 3

GO 
RAISERROR ('-- Static data UnitSeries', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT [dbo].[UnitSeries] ON 
GO
INSERT INTO UnitSeries(ID, AxleCount, UnitType, UnitNumberStart, UnitNumberEnd) VALUES(7,10,'SLT-IV',2401,2499)
INSERT INTO UnitSeries(ID, AxleCount, UnitType, UnitNumberStart, UnitNumberEnd) VALUES(8,14,'SLT-VI',2601,2699)
GO
SET IDENTITY_INSERT [dbo].[UnitSeries] OFF


GO 
RAISERROR ('-- Static data Speed category', 0, 1) WITH NOWAIT
GO

INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(7,0,0,64.8,0)
INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(7,1,64.8,144,1)
INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(7,2,144,1800,0)
INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(8,0,0,64.8,0)
INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(8,1,64.8,144,1)
INSERT INTO SpeedCategory(UnitSeriesId, CategoryType, SpeedFrom, SpeedTo, Valid) VALUES(8,2,144,1800,0)

GO
RAISERROR ('-- Static data LimitsCategory', 0, 1) WITH NOWAIT
GO
SET IDENTITY_INSERT [dbo].[LimitsCategory] ON 
GO
INSERT INTO LimitsCategory(ID, Category) VALUES (1, 'WheelFlat')
INSERT INTO LimitsCategory(ID, Category) VALUES (2, 'OutOfRound')
INSERT INTO LimitsCategory(ID, Category) VALUES (3, 'RMSHigh')
GO
SET IDENTITY_INSERT [dbo].[LimitsCategory] OFF 
GO

RAISERROR ('-- Static data LimitsMeasurement', 0, 1) WITH NOWAIT
GO

INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,0,1,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,1,1,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,2,1,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,0,1,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,1,1,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,2,1,0,51,90,150)

INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,0,2,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,1,2,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,2,2,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,0,2,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,1,2,0,51,90,150)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,2,2,0,51,90,150)

INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,0,3,0,10.2,18,30)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,1,3,0,10.2,18,30)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(7,2,3,0,10.2,18,30)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,0,3,0,10.2,18,30)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,1,3,0,10.2,18,30)
INSERT INTO LimitsMeasurement(UnitSeriesID, SpeedCategoryType, LimitsCategoryID, Limit0, Limit1, Limit2, Limit3) VALUES(8,2,3,0,10.2,18,30)


GO
RAISERROR ('-- Static data Tag', 0, 1) WITH NOWAIT
GO

INSERT INTO Tag(ID, ComponentId, Position) Select 13823532, ID, 'L' From Component Where UnitId=2401 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13824556, ID, 'R' From Component Where UnitId=2401 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491373, ID, 'L' From Component Where UnitId=2402 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491680, ID, 'R' From Component Where UnitId=2402 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14488865, ID, 'L' From Component Where UnitId=2403 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420211, ID, 'R' From Component Where UnitId=2403 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13979762, ID, 'R' From Component Where UnitId=2404 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13981002, ID, 'L' From Component Where UnitId=2404 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692368, ID, 'L' From Component Where UnitId=2405 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14486841, ID, 'R' From Component Where UnitId=2405 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489573, ID, 'L' From Component Where UnitId=2406 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419866, ID, 'R' From Component Where UnitId=2406 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694244, ID, 'R' From Component Where UnitId=2407 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14695104, ID, 'L' From Component Where UnitId=2407 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14486562, ID, 'R' From Component Where UnitId=2408 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490391, ID, 'L' From Component Where UnitId=2408 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692860, ID, 'L' From Component Where UnitId=2409 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693054, ID, 'R' From Component Where UnitId=2409 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693470, ID, 'R' From Component Where UnitId=2410 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691999, ID, 'L' From Component Where UnitId=2410 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14553773, ID, 'R' From Component Where UnitId=2411 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491567, ID, 'L' From Component Where UnitId=2411 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489843, ID, 'L' From Component Where UnitId=2412 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490353, ID, 'R' From Component Where UnitId=2412 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555322, ID, 'L' From Component Where UnitId=2413 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491239, ID, 'R' From Component Where UnitId=2413 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490725, ID, 'L' From Component Where UnitId=2414 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491241, ID, 'R' From Component Where UnitId=2414 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420614, ID, 'L' From Component Where UnitId=2415 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419974, ID, 'R' From Component Where UnitId=2415 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693227, ID, 'R' From Component Where UnitId=2416 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692341, ID, 'L' From Component Where UnitId=2416 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489597, ID, 'R' From Component Where UnitId=2417 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420516, ID, 'L' From Component Where UnitId=2417 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490449, ID, 'R' From Component Where UnitId=2418 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491643, ID, 'L' From Component Where UnitId=2418 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491639, ID, 'L' From Component Where UnitId=2419 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489906, ID, 'R' From Component Where UnitId=2419 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490715, ID, 'L' From Component Where UnitId=2420 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14488952, ID, 'R' From Component Where UnitId=2420 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490345, ID, 'R' From Component Where UnitId=2421 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420223, ID, 'L' From Component Where UnitId=2421 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693757, ID, 'R' From Component Where UnitId=2422 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693710, ID, 'L' From Component Where UnitId=2422 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420157, ID, 'L' From Component Where UnitId=2423 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419514, ID, 'R' From Component Where UnitId=2423 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555962, ID, 'L' From Component Where UnitId=2424 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14553803, ID, 'R' From Component Where UnitId=2424 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554125, ID, 'L' From Component Where UnitId=2425 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555125, ID, 'R' From Component Where UnitId=2425 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554189, ID, 'L' From Component Where UnitId=2426 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555970, ID, 'R' From Component Where UnitId=2426 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14556244, ID, 'L' From Component Where UnitId=2427 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555256, ID, 'R' From Component Where UnitId=2427 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555726, ID, 'R' From Component Where UnitId=2428 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14556330, ID, 'L' From Component Where UnitId=2428 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692190, ID, 'L' From Component Where UnitId=2429 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692567, ID, 'R' From Component Where UnitId=2429 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491068, ID, 'L' From Component Where UnitId=2430 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491065, ID, 'R' From Component Where UnitId=2430 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14695109, ID, 'L' From Component Where UnitId=2431 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14695113, ID, 'R' From Component Where UnitId=2431 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693247, ID, 'L' From Component Where UnitId=2432 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693095, ID, 'R' From Component Where UnitId=2432 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693736, ID, 'L' From Component Where UnitId=2433 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691226, ID, 'R' From Component Where UnitId=2433 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692187, ID, 'L' From Component Where UnitId=2434 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694229, ID, 'R' From Component Where UnitId=2434 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694338, ID, 'L' From Component Where UnitId=2435 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691864, ID, 'R' From Component Where UnitId=2435 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554123, ID, 'R' From Component Where UnitId=2436 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554398, ID, 'L' From Component Where UnitId=2436 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682583, ID, 'L' From Component Where UnitId=2437 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693827, ID, 'R' From Component Where UnitId=2437 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14684933, ID, 'R' From Component Where UnitId=2438 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694385, ID, 'L' From Component Where UnitId=2438 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682715, ID, 'L' From Component Where UnitId=2439 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691953, ID, 'R' From Component Where UnitId=2439 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490762, ID, 'R' From Component Where UnitId=2440 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419330, ID, 'L' From Component Where UnitId=2440 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694199, ID, 'R' From Component Where UnitId=2441 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682936, ID, 'L' From Component Where UnitId=2441 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420291, ID, 'L' From Component Where UnitId=2442 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692211, ID, 'R' From Component Where UnitId=2442 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694568, ID, 'L' From Component Where UnitId=2443 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694341, ID, 'R' From Component Where UnitId=2443 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692386, ID, 'R' From Component Where UnitId=2444 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694871, ID, 'L' From Component Where UnitId=2444 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694886, ID, 'L' From Component Where UnitId=2445 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694413, ID, 'R' From Component Where UnitId=2445 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692737, ID, 'R' From Component Where UnitId=2446 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693919, ID, 'L' From Component Where UnitId=2446 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691347, ID, 'L' From Component Where UnitId=2447 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692539, ID, 'R' From Component Where UnitId=2447 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693934, ID, 'R' From Component Where UnitId=2448 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692780, ID, 'L' From Component Where UnitId=2448 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693442, ID, 'L' From Component Where UnitId=2449 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691779, ID, 'R' From Component Where UnitId=2449 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693492, ID, 'L' From Component Where UnitId=2450 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691862, ID, 'R' From Component Where UnitId=2450 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691871, ID, 'R' From Component Where UnitId=2451 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691207, ID, 'L' From Component Where UnitId=2451 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693348, ID, 'L' From Component Where UnitId=2452 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693833, ID, 'R' From Component Where UnitId=2452 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693994, ID, 'R' From Component Where UnitId=2453 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14695066, ID, 'L' From Component Where UnitId=2453 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692778, ID, 'R' From Component Where UnitId=2454 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694410, ID, 'L' From Component Where UnitId=2454 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13823067, ID, 'L' From Component Where UnitId=2455 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692131, ID, 'R' From Component Where UnitId=2455 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693267, ID, 'R' From Component Where UnitId=2456 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692562, ID, 'L' From Component Where UnitId=2456 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691582, ID, 'L' From Component Where UnitId=2457 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694411, ID, 'R' From Component Where UnitId=2457 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693084, ID, 'R' From Component Where UnitId=2458 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692276, ID, 'L' From Component Where UnitId=2458 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691225, ID, 'L' From Component Where UnitId=2459 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693930, ID, 'R' From Component Where UnitId=2459 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682449, ID, 'L' From Component Where UnitId=2460 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692807, ID, 'R' From Component Where UnitId=2460 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13931982, ID, 'R' From Component Where UnitId=2461 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13932069, ID, 'L' From Component Where UnitId=2461 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13820746, ID, 'L' From Component Where UnitId=2462 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13934428, ID, 'R' From Component Where UnitId=2462 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13821638, ID, 'L' From Component Where UnitId=2463 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13934603, ID, 'R' From Component Where UnitId=2463 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691722, ID, 'R' From Component Where UnitId=2464 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694055, ID, 'L' From Component Where UnitId=2464 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13856856, ID, 'L' From Component Where UnitId=2465 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13873370, ID, 'R' From Component Where UnitId=2465 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13870652, ID, 'L' From Component Where UnitId=2466 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13870818, ID, 'R' From Component Where UnitId=2466 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13689701, ID, 'L' From Component Where UnitId=2467 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13690905, ID, 'R' From Component Where UnitId=2467 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13689902, ID, 'L' From Component Where UnitId=2468 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13692068, ID, 'R' From Component Where UnitId=2468 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13823256, ID, 'L' From Component Where UnitId=2469 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13868330, ID, 'R' From Component Where UnitId=2469 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14059854, ID, 'L' From Component Where UnitId=2601 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 13982391, ID, 'R' From Component Where UnitId=2601 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14492209, ID, 'L' From Component Where UnitId=2602 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491825, ID, 'R' From Component Where UnitId=2602 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491178, ID, 'L' From Component Where UnitId=2603 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14488963, ID, 'R' From Component Where UnitId=2603 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419869, ID, 'L' From Component Where UnitId=2604 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489680, ID, 'R' From Component Where UnitId=2604 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420389, ID, 'R' From Component Where UnitId=2605 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420409, ID, 'L' From Component Where UnitId=2605 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420126, ID, 'R' From Component Where UnitId=2606 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420125, ID, 'L' From Component Where UnitId=2606 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14492026, ID, 'R' From Component Where UnitId=2607 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491663, ID, 'L' From Component Where UnitId=2607 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554448, ID, 'L' From Component Where UnitId=2608 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14553675, ID, 'R' From Component Where UnitId=2608 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554841, ID, 'L' From Component Where UnitId=2609 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554026, ID, 'R' From Component Where UnitId=2609 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555387, ID, 'R' From Component Where UnitId=2610 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14653449, ID, 'L' From Component Where UnitId=2610 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555742, ID, 'L' From Component Where UnitId=2611 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14556568, ID, 'R' From Component Where UnitId=2611 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694487, ID, 'R' From Component Where UnitId=2612 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682495, ID, 'L' From Component Where UnitId=2612 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652372, ID, 'R' From Component Where UnitId=2613 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652349, ID, 'L' From Component Where UnitId=2613 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554161, ID, 'R' From Component Where UnitId=2614 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14556555, ID, 'L' From Component Where UnitId=2614 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554094, ID, 'L' From Component Where UnitId=2615 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555973, ID, 'R' From Component Where UnitId=2615 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420612, ID, 'R' From Component Where UnitId=2616 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14420075, ID, 'L' From Component Where UnitId=2616 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490229, ID, 'L' From Component Where UnitId=2617 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490136, ID, 'R' From Component Where UnitId=2617 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490045, ID, 'R' From Component Where UnitId=2618 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489752, ID, 'L' From Component Where UnitId=2618 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491372, ID, 'R' From Component Where UnitId=2619 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490605, ID, 'L' From Component Where UnitId=2619 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490727, ID, 'R' From Component Where UnitId=2620 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490557, ID, 'L' From Component Where UnitId=2620 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491064, ID, 'L' From Component Where UnitId=2621 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554222, ID, 'R' From Component Where UnitId=2621 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491322, ID, 'L' From Component Where UnitId=2622 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14553902, ID, 'R' From Component Where UnitId=2622 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652451, ID, 'R' From Component Where UnitId=2623 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652836, ID, 'L' From Component Where UnitId=2623 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491407, ID, 'L' From Component Where UnitId=2624 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491491, ID, 'R' From Component Where UnitId=2624 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692532, ID, 'L' From Component Where UnitId=2625 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14653695, ID, 'R' From Component Where UnitId=2625 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554100, ID, 'L' From Component Where UnitId=2626 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555788, ID, 'R' From Component Where UnitId=2626 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694589, ID, 'L' From Component Where UnitId=2627 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693441, ID, 'R' From Component Where UnitId=2627 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491015, ID, 'L' From Component Where UnitId=2628 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491565, ID, 'R' From Component Where UnitId=2628 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490540, ID, 'R' From Component Where UnitId=2629 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490726, ID, 'L' From Component Where UnitId=2629 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14653289, ID, 'R' From Component Where UnitId=2630 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652861, ID, 'L' From Component Where UnitId=2630 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692986, ID, 'R' From Component Where UnitId=2631 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694416, ID, 'L' From Component Where UnitId=2631 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682181, ID, 'L' From Component Where UnitId=2632 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682515, ID, 'R' From Component Where UnitId=2632 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682527, ID, 'R' From Component Where UnitId=2633 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694376, ID, 'L' From Component Where UnitId=2633 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693379, ID, 'R' From Component Where UnitId=2634 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693427, ID, 'L' From Component Where UnitId=2634 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14652087, ID, 'R' From Component Where UnitId=2635 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14653785, ID, 'L' From Component Where UnitId=2635 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693958, ID, 'L' From Component Where UnitId=2636 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694018, ID, 'R' From Component Where UnitId=2636 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692730, ID, 'R' From Component Where UnitId=2637 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693155, ID, 'L' From Component Where UnitId=2637 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555290, ID, 'L' From Component Where UnitId=2638 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554259, ID, 'R' From Component Where UnitId=2638 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682140, ID, 'R' From Component Where UnitId=2639 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691476, ID, 'L' From Component Where UnitId=2639 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694993, ID, 'L' From Component Where UnitId=2640 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693910, ID, 'R' From Component Where UnitId=2640 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14554866, ID, 'R' From Component Where UnitId=2641 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14555747, ID, 'L' From Component Where UnitId=2641 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694230, ID, 'R' From Component Where UnitId=2642 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14695110, ID, 'L' From Component Where UnitId=2642 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682083, ID, 'L' From Component Where UnitId=2643 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682135, ID, 'R' From Component Where UnitId=2643 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693703, ID, 'R' From Component Where UnitId=2644 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694232, ID, 'L' From Component Where UnitId=2644 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692082, ID, 'R' From Component Where UnitId=2645 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692977, ID, 'L' From Component Where UnitId=2645 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693202, ID, 'L' From Component Where UnitId=2646 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694862, ID, 'R' From Component Where UnitId=2646 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692806, ID, 'R' From Component Where UnitId=2647 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682868, ID, 'L' From Component Where UnitId=2647 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694387, ID, 'L' From Component Where UnitId=2648 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682378, ID, 'R' From Component Where UnitId=2648 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691991, ID, 'L' From Component Where UnitId=2649 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693952, ID, 'R' From Component Where UnitId=2649 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692872, ID, 'R' From Component Where UnitId=2650 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682787, ID, 'L' From Component Where UnitId=2650 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692691, ID, 'L' From Component Where UnitId=2651 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682110, ID, 'R' From Component Where UnitId=2651 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693444, ID, 'R' From Component Where UnitId=2652 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682482, ID, 'L' From Component Where UnitId=2652 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694451, ID, 'R' From Component Where UnitId=2653 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693487, ID, 'L' From Component Where UnitId=2653 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694916, ID, 'L' From Component Where UnitId=2654 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691940, ID, 'R' From Component Where UnitId=2654 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14419874, ID, 'L' From Component Where UnitId=2655 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14490554, ID, 'R' From Component Where UnitId=2655 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14694882, ID, 'L' From Component Where UnitId=2656 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682168, ID, 'R' From Component Where UnitId=2656 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14489712, ID, 'R' From Component Where UnitId=2657 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14491119, ID, 'L' From Component Where UnitId=2657 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682792, ID, 'R' From Component Where UnitId=2658 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691866, ID, 'L' From Component Where UnitId=2658 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693653, ID, 'R' From Component Where UnitId=2659 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14692195, ID, 'L' From Component Where UnitId=2659 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14682443, ID, 'R' From Component Where UnitId=2660 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693869, ID, 'L' From Component Where UnitId=2660 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693714, ID, 'L' From Component Where UnitId=2661 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14691674, ID, 'R' From Component Where UnitId=2661 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693567, ID, 'L' From Component Where UnitId=2662 AND ComponentTypeId=1
INSERT INTO Tag(ID, ComponentId, Position) Select 14693246, ID, 'R' From Component Where UnitId=2662 AND ComponentTypeId=1

GO
RAISERROR ('-- Static data Tag reader', 0, 1) WITH NOWAIT
GO
INSERT INTO TagReader(ID, Position) VALUES (11,'R')
INSERT INTO TagReader(ID, Position) VALUES (12,'R')
INSERT INTO TagReader(ID, Position) VALUES (18,'R')
INSERT INTO TagReader(ID, Position) VALUES (19,'R')
INSERT INTO TagReader(ID, Position) VALUES (21,'R')
INSERT INTO TagReader(ID, Position) VALUES (22,'R')
INSERT INTO TagReader(ID, Position) VALUES (28,'R')
INSERT INTO TagReader(ID, Position) VALUES (31,'R')
INSERT INTO TagReader(ID, Position) VALUES (34,'R')
INSERT INTO TagReader(ID, Position) VALUES (41,'R')
INSERT INTO TagReader(ID, Position) VALUES (44,'R')
INSERT INTO TagReader(ID, Position) VALUES (51,'R')
INSERT INTO TagReader(ID, Position) VALUES (54,'R')
INSERT INTO TagReader(ID, Position) VALUES (61,'R')
INSERT INTO TagReader(ID, Position) VALUES (64,'R')
INSERT INTO TagReader(ID, Position) VALUES (71,'R')
INSERT INTO TagReader(ID, Position) VALUES (74,'R')
INSERT INTO TagReader(ID, Position) VALUES (81,'R')
INSERT INTO TagReader(ID, Position) VALUES (84,'R')
INSERT INTO TagReader(ID, Position) VALUES (91,'R')
INSERT INTO TagReader(ID, Position) VALUES (94,'R')
INSERT INTO TagReader(ID, Position) VALUES (101,'R')
INSERT INTO TagReader(ID, Position) VALUES (104,'R')
INSERT INTO TagReader(ID, Position) VALUES (111,'R')
INSERT INTO TagReader(ID, Position) VALUES (114,'R')
INSERT INTO TagReader(ID, Position) VALUES (123,'R')
INSERT INTO TagReader(ID, Position) VALUES (124,'R')
INSERT INTO TagReader(ID, Position) VALUES (133,'R')
INSERT INTO TagReader(ID, Position) VALUES (134,'R')
INSERT INTO TagReader(ID, Position) VALUES (143,'R')
INSERT INTO TagReader(ID, Position) VALUES (153,'R')
INSERT INTO TagReader(ID, Position) VALUES (154,'R')
INSERT INTO TagReader(ID, Position) VALUES (163,'R')
INSERT INTO TagReader(ID, Position) VALUES (164,'R')
INSERT INTO TagReader(ID, Position) VALUES (171,'R')
INSERT INTO TagReader(ID, Position) VALUES (174,'R')
INSERT INTO TagReader(ID, Position) VALUES (183,'R')
INSERT INTO TagReader(ID, Position) VALUES (193,'R')
INSERT INTO TagReader(ID, Position) VALUES (203,'R')
INSERT INTO TagReader(ID, Position) VALUES (204,'R')
INSERT INTO TagReader(ID, Position) VALUES (213,'R')
INSERT INTO TagReader(ID, Position) VALUES (214,'R')
INSERT INTO TagReader(ID, Position) VALUES (223,'R')
INSERT INTO TagReader(ID, Position) VALUES (224,'R')
INSERT INTO TagReader(ID, Position) VALUES (233,'R')
INSERT INTO TagReader(ID, Position) VALUES (234,'R')
INSERT INTO TagReader(ID, Position) VALUES (243,'R')
INSERT INTO TagReader(ID, Position) VALUES (244,'R')
INSERT INTO TagReader(ID, Position) VALUES (253,'R')
INSERT INTO TagReader(ID, Position) VALUES (254,'R')
INSERT INTO TagReader(ID, Position) VALUES (263,'R')
INSERT INTO TagReader(ID, Position) VALUES (264,'R')
INSERT INTO TagReader(ID, Position) VALUES (273,'R')
INSERT INTO TagReader(ID, Position) VALUES (274,'R')
INSERT INTO TagReader(ID, Position) VALUES (283,'R')
INSERT INTO TagReader(ID, Position) VALUES (284,'R')
INSERT INTO TagReader(ID, Position) VALUES (293,'R')
INSERT INTO TagReader(ID, Position) VALUES (294,'R')
INSERT INTO TagReader(ID, Position) VALUES (303,'R')
INSERT INTO TagReader(ID, Position) VALUES (304,'R')
INSERT INTO TagReader(ID, Position) VALUES (313,'R')
INSERT INTO TagReader(ID, Position) VALUES (314,'R')
INSERT INTO TagReader(ID, Position) VALUES (323,'R')
INSERT INTO TagReader(ID, Position) VALUES (333,'R')
INSERT INTO TagReader(ID, Position) VALUES (334,'R')
INSERT INTO TagReader(ID, Position) VALUES (343,'R')
INSERT INTO TagReader(ID, Position) VALUES (344,'R')
INSERT INTO TagReader(ID, Position) VALUES (353,'R')
INSERT INTO TagReader(ID, Position) VALUES (354,'R')
INSERT INTO TagReader(ID, Position) VALUES (363,'R')
INSERT INTO TagReader(ID, Position) VALUES (364,'R')
INSERT INTO TagReader(ID, Position) VALUES (373,'R')
INSERT INTO TagReader(ID, Position) VALUES (374,'R')
INSERT INTO TagReader(ID, Position) VALUES (383,'R')
INSERT INTO TagReader(ID, Position) VALUES (384,'R')
INSERT INTO TagReader(ID, Position) VALUES (431,'R')
INSERT INTO TagReader(ID, Position) VALUES (432,'R')
INSERT INTO TagReader(ID, Position) VALUES (441,'R')
INSERT INTO TagReader(ID, Position) VALUES (442,'R')
INSERT INTO TagReader(ID, Position) VALUES (511,'R')
INSERT INTO TagReader(ID, Position) VALUES (512,'R')
INSERT INTO TagReader(ID, Position) VALUES (941,'R')
INSERT INTO TagReader(ID, Position) VALUES (942,'R')
INSERT INTO TagReader(ID, Position) VALUES (951,'R')
INSERT INTO TagReader(ID, Position) VALUES (952,'R')
INSERT INTO TagReader(ID, Position) VALUES (961,'R')
INSERT INTO TagReader(ID, Position) VALUES (962,'R')
INSERT INTO TagReader(ID, Position) VALUES (971,'R')
INSERT INTO TagReader(ID, Position) VALUES (972,'R')
INSERT INTO TagReader(ID, Position) VALUES (1151,'R')
INSERT INTO TagReader(ID, Position) VALUES (1152,'R')
INSERT INTO TagReader(ID, Position) VALUES (1171,'R')
INSERT INTO TagReader(ID, Position) VALUES (1172,'R')
INSERT INTO TagReader(ID, Position) VALUES (1181,'R')
INSERT INTO TagReader(ID, Position) VALUES (1182,'R')
INSERT INTO TagReader(ID, Position) VALUES (1191,'R')
INSERT INTO TagReader(ID, Position) VALUES (1192,'R')
INSERT INTO TagReader(ID, Position) VALUES (1201,'R')
INSERT INTO TagReader(ID, Position) VALUES (1202,'R')
INSERT INTO TagReader(ID, Position) VALUES (1211,'R')
INSERT INTO TagReader(ID, Position) VALUES (1212,'R')
INSERT INTO TagReader(ID, Position) VALUES (1401,'R')
INSERT INTO TagReader(ID, Position) VALUES (1402,'R')
INSERT INTO TagReader(ID, Position) VALUES (1471,'R')
INSERT INTO TagReader(ID, Position) VALUES (1472,'R')
INSERT INTO TagReader(ID, Position) VALUES (1481,'R')
INSERT INTO TagReader(ID, Position) VALUES (1482,'R')
INSERT INTO TagReader(ID, Position) VALUES (2021,'R')
INSERT INTO TagReader(ID, Position) VALUES (2022,'R')
INSERT INTO TagReader(ID, Position) VALUES (2023,'R')
INSERT INTO TagReader(ID, Position) VALUES (2024,'R')
INSERT INTO TagReader(ID, Position) VALUES (2041,'R')
INSERT INTO TagReader(ID, Position) VALUES (2042,'R')
INSERT INTO TagReader(ID, Position) VALUES (2051,'R')
INSERT INTO TagReader(ID, Position) VALUES (2052,'R')
INSERT INTO TagReader(ID, Position) VALUES (2061,'R')
INSERT INTO TagReader(ID, Position) VALUES (2062,'R')

GO

RAISERROR ('-- Link Unit to UnitSeries', 0, 1) WITH NOWAIT
GO

UPDATE Unit set UnitSeriesID=7 where UnitType ='SLT-IV'
UPDATE Unit set UnitSeriesID=8 where UnitType ='SLT-VI'
