IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'GotchaSite')
    DROP TABLE [dbo].GotchaSite
GO

CREATE TABLE [dbo].[GotchaSite] 
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SiteName] [varchar](30) NOT NULL,
	[TracksBetween] [varchar](50) NOT NULL,
	[Latitude] decimal(9,6) NOT NULL,
	[Longitude] decimal(9,6) NOT NULL
CONSTRAINT [PK_GotchaSite] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO


RAISERROR ('-- Insert data into GotchaSite', 0, 1) WITH NOWAIT

GO
SET IDENTITY_INSERT [dbo].[GotchaSite] ON 
GO
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (10, 'Voorschoten I', 'Voorschoten - Den Haag Mariahoeve', 52.12324, 4.42896)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (15, 'Voorschoten II', 'Voorschoten - Den Haag Mariahoeve', 52.12291, 4.42958)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (20, 'Amsterdam Sloterdijk', 'Amsterdam Sloterdijk - Zaandam', 52.40682, 4.8037)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (25, 'Amsterdam Sloterdijk II', 'Amsterdam Sloterdijk - Zaandam', 52.40682, 4.8037)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (30, 'Bunde / Geulle', 'Bunde - Elsloo', 50.91008, 5.74302)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (40, 'Almere - Weesp', 'Weesp Almere Muziekwijk', 52.31178, 5.0774)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (50, 'Elden', 'Elst - Arnhem Zuid', 51.94412, 5.85255)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (60, 'Groenekan / De Bilt', 'Bilthoven - Utrecht Overvecht', 52.12222, 5.1736)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (70, 'Moordrecht', 'Gouda - Nieuwerkerk a/d IJssel', 52.00217, 4.6552)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (80, 'Willemsdorp', 'Dordrecht - Lage Zwaluwe', 51.74142, 4.6376)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (90, 'Vlaardingen', 'Vlaardingen West - Maassluis', 51.90223, 4.33815)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (100, 'Gilze Rijen', 'Gilze Rijen - Tilburg Reeshof', 51.576652, 4.972507)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (110, 'Tricht', 'Geldermalsen - Culemborg', 51.89117, 5.26765)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (120, 'Staphorst', 'Zwolle - Meppel', 52.6469, 6.21795)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (130, 'Wierden', 'Wierden - Almelo', 52.36215, 6.60185)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (140, 'Lochem', 'Lochem - Goor', 52.18665, 6.48302)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (150, 'Botlek Vaanplein', 'Barendrecht Aansl - Rtd Waalhaven', 51.865513, 4.49643)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (160, 'Schiedam', 'Delft Zuid - Schiedam centrum', 51.95656, 4.38279)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (170, 'Bussum - Weesp', 'Weesp - Naarden Bussum', 52.29569, 5.0881)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (180, 'Boskoop', 'Boskoop - Waddinxveen Noord', 52.0669, 4.6462)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (190, 'Zwammerdam', 'Bodegraven - Alphen a/d Rijn', 52.10395, 4.71756)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (200, 'Zevenhuizen', 'Gouda - Zoetermeer Oost', 52.02657, 4.57702)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (210, 'Terschuur', 'Barneveld - Amersfoort', 52.163803, 5.493205)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (220, 'Zanderijen / Heerle', 'Roosendaal - Bergen op Zoom', 51.50751, 4.32776)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (230, 'Maastricht grens', 'Maastricht Randwyk - Eijsden', 50.8251, 5.71752)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (240, 'Hoofddorp', 'Schiphol - Hoofddorp', 52.296546, 4.724343)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (250, 'Geldrop', 'Geldrop - Heeze', 51.4121, 5.5566)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (260, 'Deurne', 'Helmond - Griendtsveen', 51.45032, 5.834625)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (270, 'Esch', 'Vught - Boxtel', 51.614, 5.30127)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (280, 'Geffen', 'Oss West - Rosmalen', 51.7501, 5.47802)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (290, 'Nijmegen', 'Nijmegen Heyendaal - Cuijk', 51.80928, 5.87609)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (300, 'Duiven', 'Duiven - Zevenaar', 51.95066, 5.99771)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (310, 'Haarlemmerliede', 'Haarlem - Haarlem Spaarnwoude', 52.38582, 4.70972)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (320, 'Junne', 'Marienberg - Ommen', 52.5064, 6.49883)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (330, 'Waterhuizen / Haren', 'Groningen Europapark - Kropswolde', 53.18389, 6.64778)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (340, 'Groningen westkant', 'Groningen - Groningen Noord', 53.21014, 6.55057)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (350, 'Leeuwarden westkant', 'Leeuwarden - Mantgum/Deinum', 53.19181, 5.76096)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (360, 'Zeist', 'Driebergen - Zeist Maarn', 52.06595, 5.31602)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (370, 'Meerssen', 'Maastricht - Valkenburg', 50.879723, 5.737165)
INSERT INTO [dbo].[GotchaSite] (ID, SiteName, TracksBetween, Latitude, Longitude) VALUES (380, 'Nuth', 'Nuth - Hoensbroek', 50.9166, 5.89742)
GO
SET IDENTITY_INSERT [dbo].[GotchaSite] OFF 
GO

--Add columns to the tag reader table
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TagReader' AND COLUMN_NAME='SiteID')
BEGIN
	ALTER TABLE TagReader ADD SiteID int NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TagReader' AND COLUMN_NAME='From')
BEGIN
	ALTER TABLE TagReader ADD [From] varchar(30) NULL
END 
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TagReader' AND COLUMN_NAME='To')
BEGIN
	ALTER TABLE TagReader ADD [To] varchar(30) NULL
END 
GO

--Load detector data into the tag reader table
UPDATE [dbo].[TagReader] SET SiteID = 10, [From] = 'Leiden', [To] = 'Den Haag' WHERE ID = '11';
UPDATE [dbo].[TagReader] SET SiteID = 10, [From] = 'Den Haag', [To] = 'Leiden' WHERE ID = '12';
UPDATE [dbo].[TagReader] SET SiteID = 15, [From] = 'Den Haag', [To] = 'Leiden' WHERE ID = '18';
UPDATE [dbo].[TagReader] SET SiteID = 15, [From] = 'Leiden', [To] = 'Den Haag' WHERE ID = '19';
UPDATE [dbo].[TagReader] SET SiteID = 20, [From] = 'Sloterdijk', [To] = 'Zaandam' WHERE ID = '21';
UPDATE [dbo].[TagReader] SET SiteID = 20, [From] = 'Zaandam', [To] = 'Sloterdijk' WHERE ID = '22';
UPDATE [dbo].[TagReader] SET SiteID = 25, [From] = 'Zaandam', [To] = 'Sloterdijk' WHERE ID = '28';
UPDATE [dbo].[TagReader] SET SiteID = 30, [From] = 'Maastricht', [To] = 'Eindhoven' WHERE ID = '31';
UPDATE [dbo].[TagReader] SET SiteID = 30, [From] = 'Eindhoven', [To] = 'Maastricht' WHERE ID = '34';
UPDATE [dbo].[TagReader] SET SiteID = 40, [From] = 'Lelystad', [To] = 'Weesp' WHERE ID = '41';
UPDATE [dbo].[TagReader] SET SiteID = 40, [From] = 'Weesp', [To] = 'Lelystad' WHERE ID = '44';
UPDATE [dbo].[TagReader] SET SiteID = 50, [From] = 'Nijmegen', [To] = 'Arnhem' WHERE ID = '51';
UPDATE [dbo].[TagReader] SET SiteID = 50, [From] = 'Arnhem', [To] = 'Nijmegen' WHERE ID = '54';
UPDATE [dbo].[TagReader] SET SiteID = 60, [From] = 'Amersfoort', [To] = 'Utrecht' WHERE ID = '61';
UPDATE [dbo].[TagReader] SET SiteID = 60, [From] = 'Utrecht', [To] = 'Amersfoort' WHERE ID = '64';
UPDATE [dbo].[TagReader] SET SiteID = 70, [From] = 'Gouda', [To] = 'Rotterdam' WHERE ID = '71';
UPDATE [dbo].[TagReader] SET SiteID = 70, [From] = 'Rotterdam', [To] = 'Gouda' WHERE ID = '74';
UPDATE [dbo].[TagReader] SET SiteID = 80, [From] = 'Lage Zwaluwe', [To] = 'Dordrecht' WHERE ID = '81';
UPDATE [dbo].[TagReader] SET SiteID = 80, [From] = 'Dordrecht', [To] = 'Lage Zwaluwe' WHERE ID = '84';
UPDATE [dbo].[TagReader] SET SiteID = 90, [From] = 'Rotterdam', [To] = 'Hoek van Holland' WHERE ID = '91';
UPDATE [dbo].[TagReader] SET SiteID = 90, [From] = 'Hoek van Holland', [To] = 'Rotterdam' WHERE ID = '94';
UPDATE [dbo].[TagReader] SET SiteID = 100, [From] = 'Breda', [To] = 'Tilburg' WHERE ID = '101';
UPDATE [dbo].[TagReader] SET SiteID = 100, [From] = 'Tilburg', [To] = 'Breda' WHERE ID = '104';
UPDATE [dbo].[TagReader] SET SiteID = 110, [From] = 'Utrecht / Beesd', [To] = 'Geldermalsen' WHERE ID = '111';
UPDATE [dbo].[TagReader] SET SiteID = 110, [From] = 'Geldermalsen', [To] = 'Utrecht / Beesd' WHERE ID = '114';
UPDATE [dbo].[TagReader] SET SiteID = 120, [From] = 'Meppel', [To] = 'Zwolle' WHERE ID = '123';
UPDATE [dbo].[TagReader] SET SiteID = 120, [From] = 'Zwolle', [To] = 'Meppel' WHERE ID = '124';
UPDATE [dbo].[TagReader] SET SiteID = 130, [From] = 'Almelo', [To] = 'Deventer' WHERE ID = '133';
UPDATE [dbo].[TagReader] SET SiteID = 130, [From] = 'Deventer', [To] = 'Almelo' WHERE ID = '134';
UPDATE [dbo].[TagReader] SET SiteID = 140, [From] = 'Hengelo', [To] = 'Zuthpen' WHERE ID = '143';
UPDATE [dbo].[TagReader] SET SiteID = 150, [From] = 'Rotterdam', [To] = 'Europoort' WHERE ID = '153';
UPDATE [dbo].[TagReader] SET SiteID = 150, [From] = 'Europoort', [To] = 'Rotterdam' WHERE ID = '154';
UPDATE [dbo].[TagReader] SET SiteID = 160, [From] = 'Den Haag', [To] = 'Rotterdam' WHERE ID = '163';
UPDATE [dbo].[TagReader] SET SiteID = 160, [From] = 'Rotterdam', [To] = 'Den Haag' WHERE ID = '164';
UPDATE [dbo].[TagReader] SET SiteID = 170, [From] = 'Bussum', [To] = 'Weesp' WHERE ID = '171';
UPDATE [dbo].[TagReader] SET SiteID = 170, [From] = 'Weesp', [To] = 'Bussum' WHERE ID = '174';
UPDATE [dbo].[TagReader] SET SiteID = 180, [From] = 'Gouda', [To] = 'Alphen aan den Rijn' WHERE ID = '183';
UPDATE [dbo].[TagReader] SET SiteID = 190, [From] = 'Leiden', [To] = 'Woerden' WHERE ID = '193';
UPDATE [dbo].[TagReader] SET SiteID = 200, [From] = 'Gouda', [To] = 'Den Haag' WHERE ID = '203';
UPDATE [dbo].[TagReader] SET SiteID = 200, [From] = 'Den Haag', [To] = 'Gouda' WHERE ID = '204';
UPDATE [dbo].[TagReader] SET SiteID = 210, [From] = 'Amersfoort ', [To] = 'Barneveld' WHERE ID = '213';
UPDATE [dbo].[TagReader] SET SiteID = 210, [From] = 'Barneveld', [To] = 'Amersfoort ' WHERE ID = '214';
UPDATE [dbo].[TagReader] SET SiteID = 220, [From] = 'Bergen op Zoom', [To] = 'Roosendaal' WHERE ID = '223';
UPDATE [dbo].[TagReader] SET SiteID = 220, [From] = 'Roosendaal', [To] = 'Bergen op Zoom' WHERE ID = '224';
UPDATE [dbo].[TagReader] SET SiteID = 230, [From] = 'Maastricht', [To] = 'België' WHERE ID = '233';
UPDATE [dbo].[TagReader] SET SiteID = 230, [From] = 'België', [To] = 'Maastricht' WHERE ID = '234';
UPDATE [dbo].[TagReader] SET SiteID = 240, [From] = 'Leiden', [To] = 'Schiphol' WHERE ID = '243';
UPDATE [dbo].[TagReader] SET SiteID = 240, [From] = 'Schiphol', [To] = 'Leiden' WHERE ID = '244';
UPDATE [dbo].[TagReader] SET SiteID = 250, [From] = 'Roermond', [To] = 'Eindhoven' WHERE ID = '253';
UPDATE [dbo].[TagReader] SET SiteID = 250, [From] = 'Eindhoven', [To] = 'Roermond' WHERE ID = '254';
UPDATE [dbo].[TagReader] SET SiteID = 260, [From] = 'Eindhoven', [To] = 'Venlo' WHERE ID = '263';
UPDATE [dbo].[TagReader] SET SiteID = 260, [From] = 'Venlo', [To] = 'Eindhoven' WHERE ID = '264';
UPDATE [dbo].[TagReader] SET SiteID = 270, [From] = 'Eindhoven', [To] = 'Utrecht' WHERE ID = '273';
UPDATE [dbo].[TagReader] SET SiteID = 270, [From] = 'Utrecht', [To] = 'Eindhoven' WHERE ID = '274';
UPDATE [dbo].[TagReader] SET SiteID = 280, [From] = 'Den Bosch', [To] = 'Nijmegen' WHERE ID = '283';
UPDATE [dbo].[TagReader] SET SiteID = 280, [From] = 'Nijmegen', [To] = 'Den Bosch' WHERE ID = '284';
UPDATE [dbo].[TagReader] SET SiteID = 290, [From] = 'Venlo', [To] = 'Nijmegen' WHERE ID = '293';
UPDATE [dbo].[TagReader] SET SiteID = 290, [From] = 'Nijmegen', [To] = 'Venlo' WHERE ID = '294';
UPDATE [dbo].[TagReader] SET SiteID = 300, [From] = 'Zevenaar', [To] = 'Arnhem' WHERE ID = '303';
UPDATE [dbo].[TagReader] SET SiteID = 300, [From] = 'Arnhem', [To] = 'Zevenaar' WHERE ID = '304';
UPDATE [dbo].[TagReader] SET SiteID = 310, [From] = 'Haarlem', [To] = 'Amsterdam' WHERE ID = '313';
UPDATE [dbo].[TagReader] SET SiteID = 310, [From] = 'Amsterdam', [To] = 'Haarlem' WHERE ID = '314';
UPDATE [dbo].[TagReader] SET SiteID = 320, [From] = 'Emmen', [To] = 'Zwolle' WHERE ID = '323';
UPDATE [dbo].[TagReader] SET SiteID = 330, [From] = 'Waterhuizen', [To] = 'Groningen' WHERE ID = '333';
UPDATE [dbo].[TagReader] SET SiteID = 330, [From] = 'Groningen', [To] = 'Waterhuizen' WHERE ID = '334';
UPDATE [dbo].[TagReader] SET SiteID = 340, [From] = 'Leeuwarden', [To] = 'Groningen' WHERE ID = '343';
UPDATE [dbo].[TagReader] SET SiteID = 340, [From] = 'Groningen', [To] = 'Sauwerd' WHERE ID = '344';
UPDATE [dbo].[TagReader] SET SiteID = 350, [From] = 'Harlingen', [To] = 'Leeuwarden' WHERE ID = '353';
UPDATE [dbo].[TagReader] SET SiteID = 350, [From] = 'Leeuwarden', [To] = 'Harlingen' WHERE ID = '354';
UPDATE [dbo].[TagReader] SET SiteID = 360, [From] = 'Arnhem', [To] = 'Utrecht' WHERE ID = '363';
UPDATE [dbo].[TagReader] SET SiteID = 360, [From] = 'Utrecht', [To] = 'Arnhem' WHERE ID = '364';
UPDATE [dbo].[TagReader] SET SiteID = 370, [From] = 'Valkenburg', [To] = 'Maastricht' WHERE ID = '373';
UPDATE [dbo].[TagReader] SET SiteID = 370, [From] = 'Maastricht', [To] = 'Valkenburg' WHERE ID = '374';
UPDATE [dbo].[TagReader] SET SiteID = 380, [From] = 'Sittard', [To] = 'Heerlen' WHERE ID = '383';
UPDATE [dbo].[TagReader] SET SiteID = 380, [From] = 'Heerlen', [To] = 'Sittard' WHERE ID = '384';