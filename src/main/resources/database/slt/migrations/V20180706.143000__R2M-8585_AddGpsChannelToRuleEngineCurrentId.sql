SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Add an entry to RuleEngineCurrentId for the ChannelValueGps table
------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM RuleEngineCurrentId WHERE ConfigurationName = 'SLT' AND Position = 3)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('SLT', 3, 1, NULL)
