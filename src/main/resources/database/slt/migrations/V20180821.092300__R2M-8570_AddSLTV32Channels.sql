SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Add New Channels
------------------------------------------------------------

RAISERROR ('-- add some records in Channel table', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT Channel ON;
GO

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8088, 8088, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'ATBvv-override blijft actief', 'BSS8096', 'd3915', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8089, 8089, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'Drukknop ‘Meldlampen testen’ permanent IN', 'CAB0138', 'd3916', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8090, 8090, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'Drukknop ‘Meldlampen testen’ permanent IN', 'CAB0139', 'd3917', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8091, 8091, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'Drukknop ‘Meldlampen testen’ permanent IN', 'CAB0238', 'd3918', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8092, 8092, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'Drukknop ‘Meldlampen testen’ permanent IN', 'CAB0239', 'd3919', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8093, 8093, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 91F01 for UWC electronic is released', 'DIA0468', 'd3920', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8094, 8094, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F05 for heating tapes is released', 'DIA0469', 'd3921', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8095, 8095, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F04 for hand dryer is released', 'DIA046A', 'd3922', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8096, 8096, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F03 for heating and is released', 'DIA046B', 'd3923', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8097, 8097, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 91F02 for bio reactor is released', 'DIA046C', 'd3924', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8098, 8098, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 91F01 for UWC electronic is released', 'DIA0668', 'd3925', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8099, 8099, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F05 for heating tapes is released', 'DIA0669', 'd3926', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8100, 8100, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F04 for hand dryer is released', 'DIA066A', 'd3927', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8101, 8101, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F03 for heating and is released', 'DIA066B', 'd3928', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8102, 8102, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Line circuit breaker 63F06 for bio reactor is released', 'DIA066C', 'd3929', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8103, 8103, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Ondergrens snelheid vuilwaterlozing toilet ligt buiten het bereik van 0-140 km/h', 'DIA7084', 'd3930', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8104, 8104, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Parameterfout: Bovengrens snelheid vuilwaterlozing', 'DIA7085', 'd3931', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8105, 8105, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Communication'), 'Hulpoproep rolstoelplaats 1', 'DRN807B', 'd3932', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8106, 8106, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Communication'), 'Hulpoproep rolstoelplaats 2', 'DRN807C', 'd3933', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8107, 8107, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Air Supply System'), 'HR-druk kleiner dan 5 bar', 'REM8095', 'd3934', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8108, 8108, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Communication'), 'Hulpoproep toilet', 'RIS8072', 'd3935', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8109, 8109, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Communication'), 'Hulpoproep rolstoelplaats is niet plausibel', 'RIS9072', 'd3936', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8110, 8110, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Communication'), 'Hulpoproep rolstoelplaats is niet plausibel', 'RIS9074', 'd3937', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8111, 8111, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Bioreactor 80% vol', 'SNT806F', 'd3938', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8112, 8112, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Bioreactor 95% vol', 'SNT8070', 'd3939', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8113, 8113, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Schoonwatertank toilet leeg', 'SNT8071', 'd3940', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8114, 8114, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Lek in afvalwatertank of schoonwatertank van toilet', 'SNT8073', 'd3941', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8115, 8115, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Toilet defect', 'SNT8074', 'd3942', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8116, 8116, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Vorstlozing toilet mislukt', 'SNT8075', 'd3943', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8117, 8117, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Niveausensor of verwarming van bioreactor defect', 'SNT8076', 'd3944', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8118, 8118, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Toiletpot verstopt', 'SNT8077', 'd3945', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8119, 8119, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Toilet deur defect', 'SNT807A', 'd3946', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8120, 8120, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Bioreactor defect', 'SNT807D', 'd3947', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8121, 8121, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Toilet'), 'Parameterfout snelheid afvalwaterlozing', 'SNT808E', 'd3948', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8122, 8122, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Doors'), 'Lamp DD brandt onterecht', 'DIA701E', 'd3949', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

INSERT INTO dbo.Channel(
ID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, isAlwaysDisplayed, isLookup, IsDisplayedAsDifference) VALUES 
(8123, 8123, (SELECT ID FROM dbo.ChannelGroup WHERE ChannelGroupName = 'Other'), 'Verbroken IP-verbinding WTB-Gateway', 'COM0115', 'd3950', 'bit', 1, NULL, NULL, 0, 1, 'Event', 1, 0, 0)

GO

SET IDENTITY_INSERT Channel OFF;

GO
