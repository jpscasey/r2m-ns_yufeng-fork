SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

--------------------------------------------------------------------
-- Add Speed column to ChannelValueGPS table
--------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = 'Speed')
    ALTER TABLE ChannelValueGPS
    ADD [Speed] decimal(9,3) NULL

GO

--------------------------------------------------------------------
-- Add GPS channels to dbo.Channel
--------------------------------------------------------------------

RAISERROR ('-- add records in Channel table', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT Channel ON;

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId) 
VALUES (8124, NULL, 8124, 1, 'Laatste GPS snelheid', 'VT_GPSSpeed', 'A333','decimal (9,3)', 2, NULL, 'km/h', 0, 176, 'Virtual Train', NULL, 'Laatste GPS snelheid', NULL, 1 , 0, NULL, NULL, 0, NULL, 1, 1, 6);

SET IDENTITY_INSERT Channel OFF;
GO

--------------------------------------------------------------------
-- Add speed column to FaultChannelValueGps
--------------------------------------------------------------------

RAISERROR ('-- add speed column to FaultChannelValueGps', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[FaultChannelValueGps]') AND name = 'Col8124')
    ALTER TABLE FaultChannelValueGps
    ADD [Col8124] decimal(9,3) NULL
GO
