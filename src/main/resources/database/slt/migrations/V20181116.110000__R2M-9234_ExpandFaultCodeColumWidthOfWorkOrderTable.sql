SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Expand [FaultCode] column to varchar(100)
------------------------------------------------------------

RAISERROR ('-- Expand [FaultCode] column to varchar(100)', 0, 1) WITH NOWAIT
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkOrder' AND COLUMN_NAME='FaultCode' AND (COL_LENGTH ('WorkOrder', 'FaultCode') < 100))
BEGIN
	ALTER TABLE [dbo].[WorkOrder] ALTER COLUMN [FaultCode] [varchar](100) NULL
END
GO