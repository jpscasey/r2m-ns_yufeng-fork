SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
--  Remove all existing qualityProfile links to fault configurations
------------------------------------------------------------

RAISERROR ('-- Remove all existing qualityProfile links to fault configurations', 0, 1) WITH NOWAIT
GO

UPDATE FaultMetaExtraField SET Value ='' WHERE Field = 'qualityProfile'
GO