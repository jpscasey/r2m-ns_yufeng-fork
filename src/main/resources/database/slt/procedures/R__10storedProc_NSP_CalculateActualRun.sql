SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_CalculateActualRun', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_CalculateActualRun' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_CalculateActualRun AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_CalculateActualRun]
AS
BEGIN	

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @permanentJourneyID int
	DECLARE @actualJourneyID int
	DECLARE @headcode varchar(10)
	DECLARE @dateValue date
	DECLARE @msg varchar(200)
	DECLARE @counter int
	
	-- Flag rows to be processed:
	-- This are unprocessed rows in last 30 days
	UPDATE JourneyDate SET
		RunActualRunAnalysis = 1
	FROM JourneyDate
	INNER JOIN Journey ON Journey.ID = JourneyDate.ActualJourneyID
	WHERE RunActualRunAnalysis IS NULL
		AND DateValue > DATEADD(D, -30, SYSDATETIME())
		AND 
		(CASE 
			WHEN StartTime < EndTime	-- headcode not running over midnight
				THEN DATEADD(day, DATEDIFF(day,'19000101', CAST(DateValue AS datetime2(3))), CAST(EndTime AS DATETIME2(3)))
			ELSE DATEADD(d, 1, DATEADD(day, DATEDIFF(day,'19000101', CAST(DateValue AS datetime2(3))), CAST(EndTime AS DATETIME2(7))))
		END < DATEADD(hh, -2, SYSDATETIME()))


	WHILE 1=1
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM JourneyDate WHERE RunActualRunAnalysis = 1)
			BREAK
		----------------------------------------------

		SELECT TOP 1
			@permanentJourneyID = PermanentJourneyID
			, @actualJourneyID = ActualJourneyID
			, @headcode = Headcode
			, @dateValue = DateValue
		FROM JourneyDate
		INNER JOIN Journey ON Journey.ID = PermanentJourneyID
		WHERE RunActualRunAnalysis = 1
		ORDER BY DateValue, Headcode
		
		SET @counter = ISNULL(@counter, 0) + 1
		SET @msg = CAST(SYSDATETIME() AS varchar(30)) + ': ' + CAST(@counter AS CHAR(10)) + ': ' + ISNULL(@headcode, 'NULL') + ': EXEC NSP_CalculateActualRunPerHeadcode ' + CAST(@actualJourneyID AS varchar(10)) + ', ''' + CAST(@dateValue AS varchar(10)) + ''''
		RAISERROR(@msg, 10, 1) WITH NOWAIT
		
		-- Run Actual Run Analysis Per Headcode 
		BEGIN TRY
		
			UPDATE JourneyDate SET 
				RunActualRunAnalysis = 0
				, TimestampAnalysisStarted = SYSDATETIME()
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			EXEC NSP_CalculateActualRunPerHeadcode @actualJourneyID, @dateValue
			
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = SYSDATETIME()
				, AnalysedSuccessfully = 1
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue
				
			PRINT 'Processed'
			
		END TRY
		BEGIN CATCH
		
			UPDATE JourneyDate SET 
				TimestampAnalysisCompleted = SYSDATETIME()
				, AnalysedSuccessfully = 0
			WHERE PermanentJourneyID = @permanentJourneyID
				AND DateValue = @dateValue

			SET @msg = '*** ERROR WHEN PROCESSING ' + ISNULL(@headcode, 'NULL') + ' ***'
			RAISERROR(@msg, 10, 1) WITH NOWAIT

		END CATCH
		
	END

END
GO
