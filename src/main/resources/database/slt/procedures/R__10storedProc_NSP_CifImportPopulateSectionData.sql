SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_CifImportPopulateSectionData', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_CifImportPopulateSectionData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_CifImportPopulateSectionData AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_CifImportPopulateSectionData]
	@JourneyID int
AS
BEGIN
	SET NOCOUNT ON;

	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)

	INSERT INTO [Segment]
		([JourneyID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime]
		,[IdealRunTime])
	SELECT 
		[JourneyID]
		, SegmentID
		, (SELECT StartLocationID FROM CteJourtneySegment cjs WHERE cjs.ID = MIN(CteJourtneySegment.ID)) -- StartSectionID
		, (SELECT EndLocationID FROM CteJourtneySegment cjs WHERE cjs.ID = MAX(CteJourtneySegment.ID)) -- EndSectionID
		, MIN([StartTime])		AS [StartTime]
		, MAX([EndTime])		AS [EndTime]
		, SUM(ISNULL([RecoveryTime], 0))	AS [RecoveryTime]
		, NULL					AS [IdealRunTime]
	FROM CteJourtneySegment
	GROUP BY 
		[JourneyID]
		, SegmentID


	;WITH CteJourtneySegment 
	AS
	(
		SELECT 
			@journeyID												AS [JourneyID]
			, (
				SELECT COUNT(1) 
				FROM SectionPoint TempSP 
				WHERE TempSP.JourneyID = FromSP.JourneyID 
					AND TempSP.ID <= FromSP.ID 
					AND TempSP.ScheduledPass IS NULL
			)														AS [SegmentID]
			, FromSP.ID												AS [ID]
			, FromSP.LocationID										AS [StartLocationID]
			, ToSP.LocationID										AS [EndLocationID]
			, ISNULL(FromSP.ScheduledDeparture, FromSP.ScheduledPass)	AS [StartTime]
			, ISNULL(ToSP.ScheduledArrival, ToSP.ScheduledPass)		AS [EndTime]
			, FromSP.RecoveryTime									AS [RecoveryTime]
		FROM SectionPoint FromSP 
		INNER JOIN SectionPoint ToSP ON FromSP.ID + 1 = ToSP.ID AND FromSP.JourneyID = ToSP.JourneyID
		WHERE FromSP.JourneyID = @journeyID
	)
	INSERT INTO [Section]
		([JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,[RecoveryTime])
	SELECT
		[JourneyID]
		,[SegmentID]
		,[ID]
		,[StartLocationID]
		,[EndLocationID]
		,[StartTime]
		,[EndTime]
		,ISNULL([RecoveryTime], 0)
	FROM CteJourtneySegment
	
END
GO
