SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DataArchivingBackupAndTruncate', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DataArchivingBackupAndTruncate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DataArchivingBackupAndTruncate AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DataArchivingBackupAndTruncate]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @faultStart datetime2(3)	
	DECLARE @faultStop datetime2(3)

	BEGIN TRY

		SELECT @faultStart = Min(timestamp) FROM NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive
		SELECT @faultStop = Max(timestamp) FROM NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive

		DECLARE @backupString nvarchar(100) = 'D:\!Brian\NedTrain_Spectrum_Archive_' + isnull(CONVERT(varchar(8), @faultStop, 112),'') + 'at_'+CONVERT(varchar(8), SYSDATETIME(), 112)+'.bak'

		DECLARE @msg varchar(1000) = 'Backing up database to ' + @backupString
		RAISERROR(@msg, 10, 1) WITH NOWAIT

		BACKUP DATABASE [NedTrain_Spectrum] TO DISK = @backupString
			WITH COMPRESSION, CHECKSUM, STATS = 10;

	
		RESTORE VERIFYONLY FROM DISK = @backupString
	END TRY
	BEGIN CATCH
		RETURN 1
	END CATCH
	

	TRUNCATE TABLE [NedTrain_Spectrum]..ChannelValue_Archive
	TRUNCATE TABLE [NedTrain_Spectrum]..FaultChannelValue_Archive
	TRUNCATE TABLE [NedTrain_Spectrum]..Fault_Archive

	RETURN 0
END
GO
