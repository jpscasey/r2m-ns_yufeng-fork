SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DataArchiving_MASTER', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DataArchiving_MASTER' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DataArchiving_MASTER AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DataArchiving_MASTER]
(
	@StopDate as date
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 from NedTrain_Spectrum_Archive..ChannelValue_Archive) BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(SYSDATETIME() , SYSDATETIME(),'[NSP_DataArchiving_MASTER]1 ',0,0,'Failure')
		RAISERROR('ChannelValue_Archive Has data',18,1)
	END

	DECLARE @retbit bit 

	EXEC @retbit = dbo.NSP_DataArchiving @StopDate 

	if @retbit = 1 BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(SYSDATETIME() , SYSDATETIME(),'[NSP_DataArchiving_MASTER]2',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchiving',18,1)
	END

	EXEC @retbit = dbo.NSP_DataArchivingBackupAndTruncate
	
	if @retbit = 1 BEGIN
		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(SYSDATETIME() , SYSDATETIME(),'[NSP_DataArchiving_MASTER]3',0,0,'Failure')
		RAISERROR('Problem During NSP_DataArchivingBackupAndTruncate',18,1)
	END

	INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
		VALUES(SYSDATETIME() , SYSDATETIME(),'[NSP_DataArchiving_MASTER]',0,0,'Sucess')

END
GO
