SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DeleteEventChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DeleteEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DeleteEventChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DeleteEventChannelValue]
(
@date DATE)
AS
BEGIN
/*
For some reason ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
Is not Running in the sp so additional code has been created 
The Following runs in the Job to clear the old partitions




*/

DECLARE @TopDate date
, @Boundary int

SELECT Top 1  @TopDate = CAST(value as date) ,@Boundary = boundary_id FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name ='PF_EventChannelValue_Date'
ORDER BY value ASC


SET @Boundary =@Boundary+1

BEGIN TRY
	BEGIN TRAN


		While @date > @TopDate
		BEGIN

			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE Table_Name = 'EventChannelValue_DelPart')
				DROP TABLE [dbo].[EventChannelValue_DelPart]

			CREATE TABLE [dbo].[EventChannelValue_DelPart](
				[ID] [bigint] NOT NULL,
				[UnitID] [int] NOT NULL,
				[RecordInsert] [datetime2](3) NOT NULL,
				[TimeStamp] [datetime2](3) NOT NULL,
				[TimeStampEndTime] [datetime2](3) NULL,
				[ChannelID] [int] NOT NULL,
				[Value] [bit] NOT NULL
			)

			-- Clustered index has to be created too !!!
			CREATE CLUSTERED INDEX EventChannelValue_DelPart_PartitionedIndex ON EventChannelValue_DelPart
			( timestamp ASC ) 

			ALTER TABLE dbo.EventChannelValue SWITCH PARTITION @Boundary TO dbo.[EventChannelValue_DelPart]

			DROP TABLE [EventChannelValue_DelPart]
			
	--		ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
		
			SELECT Top 1  @TopDate = CAST(value as date) ,@Boundary = boundary_id FROM  sys.partition_range_values r
			INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
			Where f.name ='PF_ChannelValue_Date'
			and value >@TopDate
			ORDER BY value ASC

			SET @Boundary =@Boundary+1
		END
	COMMIT
END TRY 
BEGIN CATCH
	SELECT 'Error' ,ERROR_NUMBER() AS ErrorNumber  
    ,ERROR_SEVERITY() AS ErrorSeverity  
    ,ERROR_STATE() AS ErrorState  
    ,ERROR_PROCEDURE() AS ErrorProcedure  
    ,ERROR_LINE() AS ErrorLine  
    ,ERROR_MESSAGE() AS ErrorMessage;  
	ROLLBACK
END CATCH

END
GO
