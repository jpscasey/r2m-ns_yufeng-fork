SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DeleteVehicle', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DeleteVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DeleteVehicle AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DeleteVehicle]
(
	@VehicleNumber varchar(16)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UnitID INT 

	SELECT @UnitID = UnitID FROM dbo.Vehicle WHERE VehicleNumber = @VehicleNumber
	
	BEGIN TRAN
	BEGIN TRY
		
	DELETE FROM dbo.ChannelValue where UnitId = @UnitID
	DELETE FROM dbo.FaultChannelValue WHERE FaultID in (SELECT ID FROM dbo.Fault WHERE FaultUnitID = @UnitID)
	DELETE FROM dbo.Fault WHERE FaultUnitID = @UnitID

	DELETE FROM dbo.Vehicle WHERE VehicleNumber = @VehicleNumber

	DELETE FROM dbo.FleetStatus WHERE unitID = @UnitID

	DELETE FROM dbo.FleetStatusHistory WHERE unitID = @UnitID

	DELETE FROM dbo.FleetStatusStaging WHERE UnitID = @UnitID

	DELETE FROM dbo.FleetFormation WHERE UnitNumberList = @VehicleNumber

	DELETE FROM dbo.Unit WHERE ID = @UnitID

	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH

END
GO
