SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_EaReport', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_EaReport' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_EaReport AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_EaReport]
(
    @FleetCode varchar(10)
    , @DateFrom datetime2(3)
    , @DateTo datetime2(3)
    -- additional parameters: need to be in alphabetical order
    , @CreatedByRule varchar(1)
    , @CategoryID varchar(100) -- faultCategoryID in Java
    , @FaultCode varchar(max)
    , @FaultTypeID varchar(50)
    , @HasRecovery varchar(1)
    , @IsAcknowledged varchar(1)
    , @LocationID varchar(max)
    , @PositionCode varchar(50)
    , @ServiceRequestStatus varchar(20)
    , @UnitID varchar(max)
    , @UnitStatus varchar(1)
    , @UnitType varchar(20)
    , @VehicleID varchar(max)   
)
AS
BEGIN

    DECLARE @FaultCategoryTable TABLE (CategoryID int)
    INSERT INTO @FaultCategoryTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@CategoryID,',')

    DECLARE @FaultTypeTable TABLE (FaultTypeID int)
    INSERT INTO @FaultTypeTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@FaultTypeID,',')

    DECLARE @LocationTable TABLE (LocationID int)
    INSERT INTO @LocationTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@LocationID,',')

    DECLARE @UnitIDTable TABLE (UnitID int)
    INSERT INTO @UnitIDTable
    SELECT convert(int,Item) from [dbo].SplitStrings_CTE(@UnitID,',')
    
    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitType,',')

    IF @CategoryID = '' SET @CategoryID = NULL
    IF @FaultCode   = '' SET @FaultCode = NULL
    IF @FaultTypeID = '' SET @FaultTypeID = NULL
    IF @LocationID = '' SET @LocationID = NULL
    IF @PositionCode = '' SET @PositionCode = NULL
    IF @UnitID = '' SET @UnitID = NULL
    IF @UnitType = '' SET @UnitType    = NULL
    IF @CreatedByRule = '' SET @CreatedByRule = NULL
    IF @IsAcknowledged = '' SET @IsAcknowledged = NULL
    IF @HasRecovery = '' SET @HasRecovery = NULL
    IF @ServiceRequestStatus = '' SET @ServiceRequestStatus = NULL
    IF @UnitStatus = '' SET @UnitStatus = NULL

    DECLARE @FaultMetaList TABLE (ID int)

    INSERT INTO @FaultMetaList
    SELECT ID
    FROM dbo.FaultMeta
    WHERE (FaultTypeID IN (SELECT * FROM @FaultTypeTable) OR @FaultTypeID IS NULL OR (@FaultTypeID = '-1' AND FaultTypeID IS NULL))
        AND (FaultCode LIKE @FaultCode OR @FaultCode IS NULL OR (@FaultCode = '-1' AND ID IS NULL))
        AND (CategoryID IN (SELECT * FROM @FaultCategoryTable) OR @CategoryID IS NULL OR (@CategoryID = '-1' AND CategoryID IS NULL))

    SELECT
        FaultID = f.ID
        , f.CreateTime
        , f.EndTime
        , f.IsCurrent

        , f.HeadCode
        , f.SetCode
        , f.FleetCode
        , f.FaultUnitNumber AS UnitNumber
        , UnitID    = f.FaultUnitID
        , f.FaultUnitID
        , FaultVehicleNumber = f.FaultUnitID -- unit level, for now
        , FaultVehicleID = NULL

        , f.FaultMetaID
        , f.FaultCode
        , f.Description

        , f.FaultTypeID
        , f.FaultType
        , f.FaultTypeColor
        , f.Priority

        , f.CategoryID
        , f.Category

        , f.Latitude
        , f.Longitude
        , f.LocationID
        , l.LocationCode
        , CreatedByRule
        , f.IsAcknowledged
        , fef.Value AS PositionCode
    FROM dbo.VW_IX_Fault f (NOEXPAND)
    LEFT JOIN dbo.Location l ON l.ID = LocationID
    LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
    LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    OUTER APPLY (SELECT Value FROM FaultMetaExtraField  fef WHERE f.FaultMetaID = fef.FaultMetaId and Field = 'positionCode' ) fef
    WHERE f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
        AND f.CreateTime BETWEEN @DateFrom AND @DateTo
        AND (@UnitID is null OR (f.FaultUnitID in (SELECT UnitID FROM @UnitIDTable)))
        AND (@LocationID IS NULL OR LocationID = @LocationID  OR (@LocationID = -1 AND LocationID IS NULL))
        AND (fef.Value    LIKE  @PositionCode       OR @PositionCode IS NULL)
        AND (@FleetCode IS NULL OR f.FleetCode = @FleetCode)
        AND (@UnitType IS NULL OR u.UnitType = @UnitType)
        AND (@CreatedByRule IS NULL OR (CAST(@CreatedByRule AS bit) = sr.CreatedByRule))
        AND (@IsAcknowledged IS NULL OR (CAST(@IsAcknowledged AS bit) = f.IsAcknowledged))
        AND (@HasRecovery IS NULL OR (CAST(@HasRecovery AS bit) = f.HasRecovery))
        AND (@ServiceRequestStatus IS NULL OR sr.Status LIKE '%' + @ServiceRequestStatus + '%')
        AND (CASE WHEN @UnitStatus = 1 and (f.UnitStatusFault ='BVD' OR f.UnitStatusFault ='OVERSTAND') then 1 
                    WHEN @UnitStatus = '0' and ((f.UnitStatusFault <>'BVD' AND f.UnitStatusFault <>'OVERSTAND') OR f.UnitStatusFault IS NULL) then 1
                    WHEN @UnitStatus IS NULL then 1
                    ELSE  0 END = 1
        )

END
GO
