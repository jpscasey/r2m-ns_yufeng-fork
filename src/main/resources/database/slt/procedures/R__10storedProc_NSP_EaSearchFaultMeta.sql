SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_EaSearchFaultMeta', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_EaSearchFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_EaSearchFaultMeta AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_EaSearchFaultMeta]
(
	@FaultString varchar(50)
	, @FaultCategoryID varchar(500)
	, @FaultTypeID varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FaultCategoryTable TABLE (FaultCategoryID int)
	INSERT INTO @FaultCategoryTable
	SELECT convert(int, item) from [dbo].SplitStrings_CTE(@FaultCategoryID,',')

	DECLARE @FaultTypeTable TABLE (FaultTypeId tinyint)
	INSERT INTO @FaultTypeTable
	SELECT convert(tinyint, item) from [dbo].SplitStrings_CTE(@FaultTypeId,',')

	IF @FaultCategoryID = '' SET @FaultCategoryID = NULL
	IF @FaultTypeId = '' SET @FaultTypeId = NULL

	SELECT
		FaultID	 = fm.ID
		, FaultDesc = fm.Description
		, FaultCode
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	INNER JOIN dbo.FaultCategory fc ON fc.ID = fm.CategoryID
	WHERE
		(@FaultTypeId IS NULL OR ft.ID IN (SELECT FaultTypeId FROM @FaultTypeTable))
		AND (@FaultCategoryId IS NULL OR fc.ID IN (SELECT FaultCategoryID FROM @FaultCategoryTable))
		AND
		(
			Description LIKE '%' + @FaultString + '%'
			OR
			FaultCode LIKE '%' + @FaultString + '%'
		)

END
GO
