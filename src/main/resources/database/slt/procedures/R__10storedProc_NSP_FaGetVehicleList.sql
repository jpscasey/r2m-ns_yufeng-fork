SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaGetVehicleList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaGetVehicleList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaGetVehicleList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaGetVehicleList]
(
	@VehicleType varchar(50)
	, @VehicleString varchar(50)
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		VehicleID	= ID
		, VehicleNumber
	FROM dbo.Vehicle
	WHERE VehicleNumber LIKE '%' + @VehicleString + '%' 
		AND (Type = @VehicleType OR @VehicleType IS NULL)
		AND (UnitID = @UnitID OR @UnitID IS NULL)
		
END
GO
