SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaReportFaultByFaultCode', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaReportFaultByFaultCode' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaReportFaultByFaultCode AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByFaultCode]
(
	@DateFrom datetime2(3)
	, @DateTo datetime2(3)
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (ID = @VehicleID OR @VehicleID IS NULL)
		AND 
		(Type = @VehicleType OR @VehicleType IS NULL)
		AND
		(UnitID = @UnitID OR @UnitID IS NULL)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)


	SELECT
		f.FaultMetaID
		, f.FaultCode
		, f.Description
		, f.Category
		, Type = f.FaultType
		, EventCount			= COUNT(*)
		, TotalFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime))) / COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		f.FaultMetaID
		, f.FaultCode
		, f.Description
		, f.Category
		, f.FaultType
	ORDER BY EventCount DESC
		

END
GO
