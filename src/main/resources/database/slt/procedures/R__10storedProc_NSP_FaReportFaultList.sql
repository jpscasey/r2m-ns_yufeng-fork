SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaReportFaultList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaReportFaultList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaReportFaultList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaReportFaultList]
(
	@DateFrom datetime2(3)
	, @DateTo datetime2(3)
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF @LocationID = -1 SET @LocationID = NULL

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND 
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
			f.ID	
			, f.CreateTime	
			, f.HeadCode 
			, f.SetCode	
			, f.FaultCode	
			, l.LocationCode 
			, f.IsCurrent 
			, f.EndTime	
			, f.RecoveryID	
			, f.Latitude 
			, f.Longitude	
			, f.FaultMetaID	
			, f.FaultUnitNumber	
			, v.ID as FaultVehicleID	
			, FaultVehicleNumber = v.VehicleNumber
			, f.IsDelayed 
			, f.Description	
			, f.Summary	
			, f.FaultType
			, f.FaultTypeColor
			, f.Priority
			, f.Category
			, f.ReportingOnly
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR f.LocationID = @LocationID)
	ORDER BY CreateTime DESC
		
END
GO
