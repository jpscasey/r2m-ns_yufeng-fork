SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FleetSummary', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummary AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FleetSummary]
AS
	BEGIN

		SET NOCOUNT ON;
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

		-- Get totals for Vehicle based faults
		CREATE TABLE #currentUnitFault (
			UnitID int NOT NULL PRIMARY KEY 
			, P1FaultNumber int NULL
			, P2FaultNumber int NULL
			, P3FaultNumber int NULL
			, P4FaultNumber int NULL
			, P5FaultNumber int NULL
			, RecoveryNumber int NULL
			, NotAcknowledgedNumber int NULL
		)
		
		INSERT INTO #currentUnitFault
		SELECT 
			f.FaultUnitID
			, SUM(CASE f.FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
			, SUM(CASE f.FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
			, SUM(CASE f.HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
			, SUM(CASE f.IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
			
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
		WHERE f.FaultUnitID IS NOT NULL
			AND f.ReportingOnly = 0
			AND IsCurrent = 1
		GROUP BY f.FaultUnitID

		-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
		CREATE TABLE #LatestChannelValue (
			UnitID int PRIMARY KEY
			, LastTimeStamp datetime2(3)
		)
		INSERT #LatestChannelValue( UnitID , LastTimeStamp)
		SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
		FROM dbo.ChannelValue c WITH (NOLOCK)
		GROUP BY c.UnitID
		
		
		-- return data
		SELECT 
			fs.UnitID
			, fs.UnitID AS VehicleID
			, fs.UnitNumber
			, fs.UnitType
			, fs.FleetFormationID
			, fs.UnitPosition
			, fs.Headcode
			, fs.Diagram
			, fs.Loading
			, fs.FleetCode
			, fs.ActiveCab
			, P1FaultNumber		= ISNULL(vf.P1FaultNumber,0)
			, P2FaultNumber		= ISNULL(vf.P2FaultNumber,0)
			, P3FaultNumber		= ISNULL(vf.P3FaultNumber,0)
			, P4FaultNumber		= ISNULL(vf.P4FaultNumber,0)
			, P5FaultNumber		= ISNULL(vf.P5FaultNumber,0)
			, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
			, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
			, UnitStatus		= CASE fs.UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END
			, VehicleList
			, ServiceStatus		= CASE 
				WHEN fs.Headcode IS NOT NULL
					THEN 'Ready for Service'
				WHEN fs.Headcode IS NOT NULL
					THEN 'In Service'
				ELSE 'Out of Service'
			END
			,cv.[UpdateRecord]
			,cv.[RecordInsert]
			-- temporary solution for datetime and JTDS driver 
			,CAST(cv.[TimeStamp] AS datetime2(3)) AS TimeStamp,
	-- Channel Data 
			[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20], 
				[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32], [Col33], [Col34], [Col35], [Col36], [Col37], [Col38], [Col39], 
				[Col40], [Col41], [Col42], [Col43], [Col44], [Col45], [Col46], [Col47], [Col48], [Col49], [Col50], [Col51], [Col52], [Col53], [Col54], [Col55], [Col56], [Col57], [Col58], 
				[Col59], [Col60], [Col61], [Col62], [Col63], [Col64], [Col65], [Col66], [Col67], [Col68], [Col69], [Col70], [Col71], [Col72], [Col73], [Col74], [Col75], [Col76], [Col77], 
				[Col78], [Col79], [Col80], [Col81], [Col82], [Col83], [Col84], [Col85], [Col86], [Col87], [Col88], [Col89], [Col90], [Col91], [Col92], [Col93], [Col94], [Col95], [Col96], 
				[Col97], [Col98], [Col99], [Col100], [Col101], [Col102], [Col103], [Col104], [Col105], [Col106], [Col107], [Col108], [Col109], [Col110], [Col111], [Col112], [Col113], [Col114], 
				[Col115], [Col116], [Col117], [Col118], [Col119], [Col120], [Col121], [Col122], [Col123], [Col124], [Col125], [Col126], [Col127], [Col128], [Col129], [Col130], [Col131], 
				[Col132], [Col133], [Col134], [Col135], [Col136], [Col137], [Col138], [Col139], [Col140], [Col141], [Col142], [Col143], [Col144], [Col145], [Col146], [Col147], [Col148], 
				[Col149], [Col150], [Col151], [Col152], [Col153], [Col154], [Col155], [Col156], [Col157], [Col158], [Col159], [Col160], [Col161], [Col162], [Col163], [Col164], [Col165], 
				[Col166], [Col167], [Col168], [Col169], [Col170], [Col171], [Col172], [Col173], [Col174], [Col175], [Col176], [Col177], [Col178], [Col179], [Col180], [Col181], [Col182], 
				[Col183], [Col184], [Col185], [Col186], [Col187], [Col188], [Col189], [Col190], [Col191], [Col192], [Col193], [Col194], [Col195], [Col196], [Col197], [Col198], [Col199], 
				[Col200], [Col201], [Col202], [Col203], [Col204], [Col205], [Col206], [Col207], [Col208], [Col209], [Col210], [Col211], [Col212], [Col213], [Col214], [Col215], [Col216], 
				[Col217], [Col218], [Col219], [Col220], [Col221], [Col222], [Col223], [Col224], [Col225], [Col226], [Col227], [Col228], [Col229], [Col230], [Col231], [Col232], [Col233], 
				[Col234], [Col235], [Col236], [Col237], [Col238], [Col239], [Col240], [Col241], [Col242], [Col243], [Col244], [Col245], [Col246], [Col247], [Col248], [Col249], [Col250], 
				[Col251], [Col252], [Col253], [Col254], [Col255], [Col256], [Col257], [Col258], [Col259], [Col260], [Col261], [Col262], [Col263], [Col264], [Col265], [Col266], [Col267], 
				[Col268], [Col269], [Col270], [Col271], [Col272], [Col273], [Col274], [Col275], [Col276], [Col277], [Col278], [Col279], [Col280], [Col281], [Col282], [Col283], [Col284], 
				[Col285], [Col286], [Col287], [Col288], [Col289], [Col290], [Col291], [Col292], [Col293], [Col294], [Col295], [Col296], [Col297], [Col298], [Col299], [Col300], [Col301], 
				[Col302], [Col303], [Col304], [Col305], [Col306], [Col307], [Col308], [Col309], [Col310], [Col311], [Col312], [Col313], [Col314], [Col315], [Col316], [Col317], [Col318], 
				[Col319], [Col320], [Col321], [Col322], [Col323], [Col324], [Col325], [Col326], [Col327], [Col328], [Col329], [Col330], [Col331], [Col332], [Col333], [Col334], [Col335], 
				[Col336], [Col337], [Col338], [Col339], [Col340], [Col341], [Col342], [Col343], [Col344], [Col345], [Col346], [Col347], [Col348], [Col349], [Col350], [Col351], [Col352], 
				[Col353], [Col354], [Col355], [Col356], [Col357], [Col358], [Col359], [Col360], [Col361], [Col362], [Col363], [Col364], [Col365], [Col366], [Col367], [Col368], [Col369], 
				[Col370], [Col371], [Col372], [Col373], [Col374], [Col375], [Col376], [Col377], [Col378], [Col379], [Col380], [Col381], [Col382], [Col383], [Col384], [Col385], [Col386], 
				[Col387], [Col388], [Col389], [Col390], [Col391], [Col392], [Col393], [Col394], [Col395], [Col396], [Col397], [Col398], [Col399],
				[Col400], [Col401], [Col402], [Col403], [Col404], [Col405], [Col406], [Col407], [Col408], [Col409], [Col410], [Col411], [Col412], [Col413], [Col414], [Col415], [Col416], 
				[Col417], [Col418], [Col419], [Col420], [Col421], [Col422], [Col423], [Col424], [Col425], [Col426], [Col427], [Col428], [Col429], [Col430], [Col431], [Col432], [Col433], 
				[Col434], [Col435], [Col436], [Col437], [Col438], [Col439], [Col440], [Col441], [Col442], [Col443], [Col444], [Col445], [Col446], [Col447], [Col448], [Col449], [Col450], 
				[Col451], [Col452], [Col453], [Col454], [Col455], [Col456], [Col457], [Col458], [Col459], [Col460], [Col461], [Col462], [Col463], [Col464], [Col465], [Col466], [Col467], 
				[Col468], [Col469], [Col470], [Col471], [Col472], [Col473], [Col474], [Col475], [Col476], [Col477], [Col478], [Col479], [Col480], [Col481], [Col482], [Col483], [Col484], 
				[Col485], [Col486], [Col487], [Col488], [Col489], [Col490], [Col491], [Col492], [Col493], [Col494], [Col495], [Col496], [Col497], [Col498], [Col499],	
				[Col500], [Col501], [Col502], [Col503], [Col504], [Col505], [Col506], [Col507], [Col508], [Col509], [Col510], [Col511], [Col512], [Col513], [Col514], [Col515], [Col516], 
				[Col517], [Col518], [Col519], [Col520], [Col521], [Col522], [Col523], [Col524], [Col525], [Col526], [Col527], [Col528], [Col529], [Col530], [Col531], [Col532], [Col533], 
				[Col534], [Col535], [Col536], [Col537], [Col538], [Col539], [Col540], [Col541], [Col542], [Col543], [Col544], [Col545], [Col546], [Col547], [Col548], [Col549], [Col550], 
				[Col551], [Col552], [Col553], [Col554], [Col555], [Col556], [Col557], [Col558], [Col559], [Col560], [Col561], [Col562], [Col563], [Col564], [Col565], [Col566], [Col567], 
				[Col568], [Col569], [Col570], [Col571], [Col572], [Col573], [Col574], [Col575], [Col576], [Col577], [Col578], [Col579], [Col580], [Col581], [Col582], [Col583], [Col584], 
				[Col585], [Col586], [Col587], [Col588], [Col589], [Col590], [Col591], [Col592], [Col593], [Col594], [Col595], [Col596], [Col597], [Col598], [Col599],
				[Col600], [Col601], [Col602], [Col603], [Col604], [Col605], [Col606], [Col607], [Col608], [Col609], [Col610], [Col611], [Col612], [Col613], [Col614], [Col615], [Col616], 
				[Col617], [Col618], [Col619], [Col620], [Col621], [Col622], [Col623], [Col624], [Col625], [Col626], [Col627], [Col628], [Col629], [Col630], [Col631], [Col632], [Col633], 
				[Col634], [Col635], [Col636], [Col637], [Col638], [Col639], [Col640], [Col641], [Col642], [Col643], [Col644], [Col645], [Col646], [Col647], [Col648], [Col649], [Col650], 
				[Col651], [Col652], [Col653], [Col654], [Col655], [Col656], [Col657], [Col658], [Col659], [Col660], [Col661], [Col662], [Col663], [Col664], [Col665], [Col666], [Col667],
				[Col668], [Col669], [Col670], [Col671], [Col672], [Col673], [Col674], [Col675], [Col676], [Col677], [Col678], [Col679], [Col680], [Col681], [Col682], [Col683], [Col684],
				[Col685], [Col686], [Col687], [Col688], [Col689], [Col690], [Col691], [Col692], [Col693], [Col694], [Col695], [Col696], [Col697], [Col698], [Col699], [Col700], [Col701],
				[Col702], [Col703], [Col704], [Col705], [Col706], [Col707], [Col708], [Col709], [Col710], [Col711], [Col712], [Col713], [Col714], [Col715]
				,[Col3000],[Col3001],[Col3002],[Col3003],[Col3004],[Col3005],[Col3006]
				,[Col3007],[Col3008],[Col3009],[Col3010],[Col3011],[Col3012],[Col3013]
				,[Col3014],[Col3015],[Col3016],[Col3017],[Col3018],[Col3019],[Col3020]
				,[Col3021],[Col3022],[Col3023],[Col3024],[Col3025],[Col3026],[Col3027]
				,[Col3028],[Col3029],[Col3030],[Col3031],[Col3032],[Col3033],[Col3034]
				,[Col3035],[Col3036],[Col3037],[Col3038],[Col3039],[Col3040],[Col3041]
				,[Col3042],[Col3043],[Col3044],[Col3045],[Col3046],[Col3047],[Col3048]
				,[Col3049],[Col3050],[Col3051],[Col3052],[Col3053],[Col3054],[Col3055]
				,[Col3056],[Col3057],[Col3058],[Col3059],[Col3060],[Col3061],[Col3062]
				,[Col3063],[Col3064],[Col3065],[Col3066],[Col3067],[Col3068],[Col3069]
				,[Col3070],[Col3071],[Col3072],[Col3073],[Col3074],[Col3075],[Col3076]
				,[Col3077],[Col3078],[Col3079],[Col3080],[Col3081],[Col3082],[Col3083]
				,[Col3084],[Col3085],[Col3086],[Col3087],[Col3088],[Col3089],[Col3090]
				,[Col3091],[Col3092],[Col3093],[Col3094],[Col3095],[Col3096],[Col3097]
				,[Col3098],[Col3099],[Col3100],[Col3101],[Col3102],[Col3103],[Col3104]
				,[Col3105],[Col3106],[Col3107],[Col3108],[Col3109],[Col3110],[Col3111]
				,[Col3112],[Col3113],[Col3114],[Col3115],[Col3116],[Col3117],[Col3118]
				,[Col3119],[Col3120],[Col3121],[Col3122],[Col3123],[Col3124],[Col3125]
				,[Col3126],[Col3127],[Col3128],[Col3129],[Col3130],[Col3131],[Col3132]
				,[Col3133],[Col3134],[Col3135],[Col3136],[Col3137],[Col3138],[Col3139]
				,[Col3140],[Col3141],[Col3142],[Col3143],[Col3144],[Col3145],[Col3146]
				,[Col3147],[Col3148],[Col3149],[Col3150],[Col3151],[Col3152],[Col3153]
				,[Col3154],[Col3155],[Col3156],[Col3157],[Col3158],[Col3159],[Col3160]
				,[Col3161],[Col3162],[Col3163],[Col3164],[Col3165],[Col3166],[Col3167]
				,[Col3168],[Col3169],[Col3170],[Col3171],[Col3172],[Col3173],[Col3174]
				,[Col3175],[Col3176],[Col3177],[Col3178],[Col3179],[Col3180],[Col3181]
				,[Col3182],[Col3183],[Col3184],[Col3185],[Col3186],[Col3187],[Col3188]
				,[Col3189],[Col3190],[Col3191],[Col3192],[Col3193],[Col3194],[Col3195]
				,[Col3196],[Col3197],[Col3198],[Col3199],[Col3200],[Col3201],[Col3202]
				,[Col3203],[Col3204],[Col3205],[Col3206],[Col3207],[Col3208],[Col3209]
				,[Col3210],[Col3211],[Col3212],[Col3213],[Col3214],[Col3215],[Col3216]
				,[Col3217],[Col3218],[Col3219],[Col3220],[Col3221],[Col3222],[Col3223]
				,[Col3224],[Col3225],[Col3226],[Col3227],[Col3228],[Col3229],[Col3230]
				,[Col3231],[Col3232],[Col3233],[Col3234],[Col3235],[Col3236],[Col3237]
				,[Col3238],[Col3239],[Col3240],[Col3241],[Col3242],[Col3243],[Col3244]
				,[Col3245],[Col3246],[Col3247],[Col3248],[Col3249],[Col3250],[Col3251]
				,[Col3252],[Col3253],[Col3254],[Col3255],[Col3256],[Col3257],[Col3258]
				,[Col3259],[Col3260],[Col3261],[Col3262],[Col3263],[Col3264],[Col3265]
				,[Col3266],[Col3267],[Col3268],[Col3269],[Col3270],[Col3271],[Col3272]
				,[Col3273],[Col3274],[Col3275],[Col3276],[Col3277],[Col3278],[Col3279]
				,[Col3280],[Col3281],[Col3282],[Col3283],[Col3284],[Col3285],[Col3286]
				,[Col3287],[Col3288],[Col3289],[Col3290],[Col3291],[Col3292],[Col3293]
				,[Col3294],[Col3295],[Col3296],[Col3297],[Col3298],[Col3299],[Col3300]
				,[Col3301],[Col3302],[Col3303],[Col3304],[Col3305],[Col3306],[Col3307]
				,[Col3308],[Col3309],[Col3310],[Col3311],[Col3312],[Col3313],[Col3314]
				,[Col3315],[Col3316],[Col3317],[Col3318],[Col3319],[Col3320],[Col3321]
				,[Col3322],[Col3323],[Col3324],[Col3325],[Col3326],[Col3327],[Col3328]
				,[Col3329],[Col3330],[Col3331],[Col3332],[Col3333],[Col3334],[Col3335]
				,[Col3336],[Col3337],[Col3338],[Col3339],[Col3340],[Col3341],[Col3342]
				,[Col3343],[Col3344],[Col3345],[Col3346],[Col3347],[Col3348],[Col3349]
				,[Col3350],[Col3351],[Col3352],[Col3353],[Col3354],[Col3355],[Col3356]
				,[Col3357],[Col3358],[Col3359],[Col3360],[Col3361],[Col3362],[Col3363]
				,[Col3364],[Col3365],[Col3366],[Col3367],[Col3368],[Col3369],[Col3370]
				,[Col3371],[Col3372],[Col3373],[Col3374],[Col3375],[Col3376],[Col3377]
				,[Col3378],[Col3379],[Col3380],[Col3381],[Col3382],[Col3383],[Col3384]
				,[Col3385],[Col3386],[Col3387],[Col3388],[Col3389],[Col3390],[Col3391]
				,[Col3392],[Col3393],[Col3394],[Col3395],[Col3396],[Col3397],[Col3398]
				,[Col3399],[Col3400],[Col3401],[Col3402],[Col3403],[Col3404],[Col3405]
				,[Col3406],[Col3407],[Col3408],[Col3409],[Col3410],[Col3411],[Col3412]
				,[Col3413],[Col3414],[Col3415],[Col3416],[Col3417],[Col3418],[Col3419]
				,[Col3420],[Col3421],[Col3422],[Col3423],[Col3424],[Col3425],[Col3426]
				,[Col3427],[Col3428],[Col3429],[Col3430],[Col3431],[Col3432],[Col3433]
				,[Col3434],[Col3435],[Col3436],[Col3437],[Col3438],[Col3439],[Col3440]
				,[Col3441],[Col3442],[Col3443],[Col3444],[Col3445],[Col3446],[Col3447]
				,[Col3448],[Col3449],[Col3450],[Col3451],[Col3452],[Col3453],[Col3454]
				,[Col3455],[Col3456],[Col3457],[Col3458],[Col3459],[Col3460],[Col3461]
				,[Col3462],[Col3463],[Col3464],[Col3465],[Col3466],[Col3467],[Col3468]
				,[Col3469],[Col3470],[Col3471],[Col3472],[Col3473],[Col3474],[Col3475]
				,[Col3476],[Col3477],[Col3478],[Col3479],[Col3480],[Col3481],[Col3482]
				,[Col3483],[Col3484],[Col3485],[Col3486],[Col3487],[Col3488],[Col3489]
				,[Col3490],[Col3491],[Col3492],[Col3493],[Col3494],[Col3495],[Col3496]
				,[Col3497],[Col3498],[Col3499],[Col3500],[Col3501],[Col3502],[Col3503]
				,[Col3504],[Col3505],[Col3506],[Col3507],[Col3508],[Col3509],[Col3510]
				,[Col3511],[Col3512],[Col3513],[Col3514],[Col3515],[Col3516],[Col3517]
				,[Col3518],[Col3519]
		FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
		LEFT JOIN #LatestChannelValue vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
		LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
		LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cv.ID = cvd.ID
		LEFT JOIN #currentUnitFault vf ON fs.UnitID = vf.UnitID

END
GO
