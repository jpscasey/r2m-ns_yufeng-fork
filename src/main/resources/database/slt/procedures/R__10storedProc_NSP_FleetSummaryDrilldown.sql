SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FleetSummaryDrilldown', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FleetSummaryDrilldown' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummaryDrilldown AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FleetSummaryDrilldown]
(
    @UnitIdList varchar(30)
    , @SelectColList varchar(max)
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @sql varchar(max)
    DECLARE @cols AS NVARCHAR(MAX)
    DECLARE @colNames AS NVARCHAR(MAX)

--  SELECT * FROM Channel

    SELECT item  INTO #TEMPCols 
    FROM dbo.SplitString(replace(replace(@SelectColList,'Col',''),' ',''),',')

    select @cols = STUFF((SELECT ',' + QUOTENAME(ID) 
                    from Channel
                    where ID IN (SELECT item FROM #TEMPCols)
                    AND HardwareType = 'Event'
                    group by ID
                    order by ID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

    select @colNames = STUFF((SELECT ','+ QUOTENAME(ID)+' as col' + cast(ID as varchar(10))
                    from Channel
                    where ID IN (SELECT item FROM #TEMPCols)
                    AND HardwareType = 'Event'
                    group by ID
                    order by ID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
    
    SET @sql = '
        SELECT DISTINCT
            cv.UnitID
            , ' + @SelectColList + '
            , fs.UnitPosition
        FROM dbo.VW_FleetStatus fs
        INNER JOIN dbo.VW_UnitLastChannelValueTimestamp vlc ON vlc.UnitID = fs.UnitID
        LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
        LEFT JOIN dbo.ChannelValueDoor cvd WITH (NOLOCK) ON cvd.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cvd.UnitID'
        
        IF @colNames is not null
        SET @sql = @sql + ' LEFT JOIN ( SELECT ' + @colNames + ',UnitID from 
                     (
                        select cast(value as tinyint) as value, ChannelID,UnitID
                        from EventChannelLatestValue
                        where UnitID in (' + @UnitIdList + ')
                    ) x
                    pivot 
                    (
                        MAX(value)
                        for ChannelID in (' + @cols + ')
                    ) p  
                    ) eclv ON vlc.UnitID = eclv.UnitID'
                    
        SET @sql = @sql + ' WHERE fs.UnitID in (' + @UnitIdList + ')
        ORDER BY 
            fs.UnitPosition'

    EXEC (@sql)
    DROP TABLE #TEMPCols

END
GO
