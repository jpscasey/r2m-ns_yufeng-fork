SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GeniusImportProcessFile', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GeniusImportProcessFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GeniusImportProcessFile AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GeniusImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET DATEFORMAT Mdy

	IF (SELECT COUNT(1) FROM LoadGeniusData WHERE LoadGeniusFileID = @FileID) > 0 
	BEGIN

		DECLARE @bstToGmtTimeOffset smallint
		SET @bstToGmtTimeOffset = - dbo.FN_GetGmtToBstTimeDiff(SYSDATETIME())
	
		BEGIN TRAN
		
		TRUNCATE TABLE [GeniusStaging]

		INSERT INTO dbo.GeniusStaging
			([GeniusInterfaceFileID]
			,[DiagramNumber]
			,Headcode
			,[TrainStartLocation]
			,[TrainEndLocation]
			,[TrainStartTime]
			,[TrainEndTime]
			,[UnitNumber]
			,[UnitPosition])
		SELECT 
			@FileID																--LoadGeniusFileID
			, REPLACE(REPLACE(Diagram, CHAR(10), ''), CHAR(13), '')				--Diagram
			, LEFT(REPLACE(REPLACE(Headcode, CHAR(10), ''), CHAR(13), ''), 4)	--Headcode
			, RTRIM(REPLACE(REPLACE(LocationStart, CHAR(10), ''), CHAR(13), ''))--LocationStart
			, RTRIM(REPLACE(REPLACE(LocationEnd, CHAR(10), ''), CHAR(13), ''))	--LocationEnd
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeStart)					--TimeStart
			, DATEADD(hh, @bstToGmtTimeOffset , DatetimeEnd)					--TimeEnd
			, REPLACE(REPLACE(REPLACE(UnitNumber, CHAR(10), ''), CHAR(13), ''),'455','5') --UnitNumber
			, CASE 
				WHEN ISNUMERIC(UnitPosition) = 1
					THEN UnitPosition
				ELSE NULL
			END														--Position
		FROM LoadGeniusData 
		WHERE LoadGeniusFileID = @FileID
			AND Headcode IS NOT NULL
			AND Headcode <> ''
			
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK
			UPDATE LoadGeniusFile SET 
				Status = 'ERROR - Insert to LoadGeniusData failed'
			WHERE ID = @FileID
		END
		ELSE
			COMMIT
			
	END

END
GO
