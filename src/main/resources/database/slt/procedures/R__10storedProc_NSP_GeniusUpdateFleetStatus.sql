SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GeniusUpdateFleetStatus', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GeniusUpdateFleetStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GeniusUpdateFleetStatus AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GeniusUpdateFleetStatus] 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @currentTimestamp datetime2(3)	= DATEADD(ms, - DATEPART(ms, SYSDATETIME()), SYSDATETIME()) --timestamp with ms = 000
	DECLARE @gmtToBstTimeOffset smallint	= 0		-- FCC stores channel data with timestamps including DLS -- dbo.FN_GetGmtToBstTimeDiff(GETUTCDATE())
	DECLARE @headcodeBeforeDeparture int	= 20	-- number of minutes before departure time that Headcode is displayed
	DECLARE @headcodeAfterArrival int		= 60	-- number of minutes after arrival time that Headcode is displayed (unless new headcode is on
	DECLARE @unitID int
	DECLARE @fleetFormationID int
	

	IF OBJECT_ID('tempdb.dbo.#FleetConfig') IS NOT NULL
		DROP TABLE #FleetConfig
		
	;WITH CteHeadcode
	AS
	(
		-- Current headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 1 AS Priority
		FROM FleetStatusStaging
		WHERE @currentTimestamp >= TrainStartTime 
			AND @currentTimestamp < TrainEndTime

		UNION ALL
		
		-- Following headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 2 AS Priority
		FROM FleetStatusStaging
		WHERE 
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN DATEADD(n, - @headcodeBeforeDeparture, TrainStartTime) AND TrainEndTime

		UNION ALL

		-- Finished headcodes
		SELECT DISTINCT
			ID	
			, Headcode	
			, TrainStartTime	
			, TrainEndTime	
			, UnitID	
			, 3 AS Priority
		FROM FleetStatusStaging
		WHERE
			(
				@currentTimestamp < TrainStartTime 
				OR @currentTimestamp >= TrainEndTime
			)
			AND @currentTimestamp BETWEEN TrainStartTime AND DATEADD(n, @headcodeAfterArrival, TrainEndTime)
	)
	SELECT 
		ID AS UnitID	
		, UnitNumber
		, CASE
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 1 ORDER BY TrainStartTime DESC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 2 ORDER BY TrainStartTime ASC)
			WHEN EXISTS (SELECT * FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3)
				THEN (SELECT TOP 1 ID FROM CteHeadcode WHERE CteHeadcode.UnitID = Unit.ID AND Priority = 3 ORDER BY TrainEndTime DESC)
		END HeadcodeID
	INTO #FleetConfig
	FROM Unit

	WHILE 1 = 1 
	BEGIN
		
		SET @unitID = (SELECT MIN(UnitID) FROM #FleetConfig WHERE UnitID > ISNULL(@unitID, 0))
		IF @unitID IS NULL BREAK
		------------------------------------------------------------------------------------------------------
		
		SET @fleetFormationID = NULL
		
		BEGIN TRY
		
			--Insert formation if does not exist
			SET @fleetFormationID = (
				SELECT ID
				FROM dbo.FleetFormation ff
				WHERE EXISTS 
					(SELECT 1
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
						AND TrainStartTime = ff.ValidFrom
						AND TrainEndTime = ff.ValidTo
						AND CoupledUnits = ff.UnitNumberList
					) 
				)
		
			IF @fleetFormationID IS NULL
			BEGIN
			
				INSERT INTO dbo.FleetFormation(
					UnitNumberList
					, UnitIDList
					, ValidFrom
					, ValidTo)
				SELECT 
					CoupledUnits
					, ''
					, TrainStartTime
					, TrainEndTime
				FROM #FleetConfig fc
				LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID 
				WHERE fc.UnitID = @unitID
					AND TrainStartTime IS NOT NULL
					AND TrainEndTime IS NOT NULL
					AND CoupledUnits IS NOT NULL

				IF @@ROWCOUNT > 0
					SET @fleetFormationID = (SELECT MAX(ID) FROM FleetFormation)
				
			END

			-- Check if unit status to be updated
			IF EXISTS (
				SELECT 1
				FROM dbo.FleetStatus
				LEFT JOIN (
					SELECT 
						UnitID				= fc.UnitID
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				) AS CteFleetConfig ON FleetStatus.UnitID = CteFleetConfig.UnitID
			WHERE ISNULL(FleetStatus.Diagram, 'NULL') <> ISNULL(HeadcodeDiagram, 'NULL')
				AND FleetStatus.UnitID = @unitID
				AND FleetStatus.ValidTo IS NULL	
)
			
			BEGIN

				-- 1. Close Headcodes that are completed
				UPDATE FleetStatus SET
					ValidTo = @currentTimestamp
				WHERE UnitID = @unitID
					AND ValidTo IS NULL

				-- 2. Add new headcodes
				;WITH CteFleetConfig
				AS
				(
					SELECT 
						UnitID				= fc.UnitID
						, UnitNumber		= fc.UnitNumber
						, Diagram			= DiagramNumber
						, Headcode			= LEFT(Headcode, 4)
						, LocationStart		= TrainStartLocationTiploc
						, LocationEnd		= TrainEndLocationTiploc
						, LocationIdStart	= TrainStartLocationId
						, LocationIdEnd		= TrainEndLocationId
						, TimeStart			= TrainStartTime
						, TimeEnd			= TrainEndTime
						, Position			= UnitPosition
						, CoupledUnits		= REPLACE(CoupledUnits, ';',',')
						, LEFT(Headcode, 4) 
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainStartTime))+ 100 AS char(3)), 2)
						+ ' ' + TrainStartLocationTiploc
						+ '-' + TrainEndLocationTiploc
						+ ' ' + RIGHT(CAST(DATEPART(hh, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime)) + 100 AS char(3)), 2) + RIGHT(CAST(DATEPART(n, DATEADD(hh, @gmtToBstTimeOffset, TrainEndTime))+ 100 AS char(3)), 2) 
						AS HeadcodeDiagram
					FROM #FleetConfig fc
					LEFT JOIN FleetStatusStaging ON HeadcodeID = FleetStatusStaging.ID
					WHERE fc.UnitID = @unitID
				)
				INSERT INTO dbo.FleetStatus(
					UnitID
					, ValidFrom
					, ValidTo
					, Setcode
					, Diagram
					, Headcode
					, IsValid
					, Loading
					, ConfigDate
					, UnitList
					, LocationIDHeadcodeStart
					, LocationIDHeadcodeEnd
					, UnitPosition
					, FleetFormationID
				)
				SELECT
					UnitID
					, ValidFrom		= @currentTimestamp
					, ValidTo		= NULL
					, Setcode		= NULL
					, Diagram		= CteFleetConfig.HeadcodeDiagram
					, Headcode		= CteFleetConfig.Headcode
					, IsValid		= 1
					, Loading		= NULL
					, ConfigDate	= SYSDATETIME()
					, UnitList		= CoupledUnits
					, LocationIDHeadcodeStart	= LocationIdStart
					, LocationIDHeadcodeEnd		= LocationIdEnd
					, UnitPosition				= ISNULL(Position, 1)
					, FleetFormationID			= @fleetFormationID
				FROM CteFleetConfig
				WHERE UnitID = @unitID
				
			END
			
		END TRY
		BEGIN CATCH
			PRINT 'Error: Unknown error when updating headcode'
			PRINT 'UnitID			' + CAST(@unitID AS varchar(10))
			PRINT 'CurrentTimestamp ' + CONVERT(char(20), @currentTimestamp, 121)
		END CATCH


	END -- WHILE

END -- stored procedure
GO
