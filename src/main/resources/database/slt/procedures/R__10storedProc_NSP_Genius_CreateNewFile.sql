SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_CreateNewFile', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_CreateNewFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_CreateNewFile AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_CreateNewFile]
	 @file varchar(255)
	,@fileTimeStamp datetime2(3)
	,@fileId int out
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	creates a new entry for a new import file in the Genius 
**	Interface tables	
**	Return values: returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-22		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
		-- insert filename
		insert dbo.GeniusInterfaceFile (Name ,FileTimeStamp)
		values (@file,@fileTimeStamp) ;
	
		set @fileId = scope_identity();	
	
	end try
	begin catch
		exec dbo.NSP_RethrowError;
		return -1;
	end catch	
	
end
GO
