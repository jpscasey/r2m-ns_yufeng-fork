SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_ImportDataFromStaging', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_ImportDataFromStaging' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_ImportDataFromStaging AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_ImportDataFromStaging]
	 @fileId int
	,@cutoffTime time = '03:00'	 
as 
/******************************************************************************
**	Name: NSP_Genius_ImportDataFromStaging
**	Description:	imports the data from the staging table into the Spectrum
**	data tables
**	
**	Return values: returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History 
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-25		HeZ	creation on database
** 2012-03-14	HeZ	adapted for real life data from Genius
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	DECLARE @unitNumber varchar(10) = (
		SELECT TOP 1 UnitNumber 
		FROM dbo.GeniusStaging 
		WHERE GeniusInterfaceFileID = @fileId 
		AND UnitNumber IS NOT NULL
	)
	DECLARE @unitID int = (SELECT ID FROM dbo.Unit WHERE UnitNumber = @unitNumber)
	DECLARE @logText varchar(2000);
	DECLARE @rowNumber int
	--begin tran

	begin try

--debug
--PRINT ISNULL(@unitId, '-5000')

	BEGIN TRAN
	DELETE dbo.FleetStatusStaging WHERE UnitID = @unitId;
	
	WITH CteGeniusStaging AS
	(
	select distinct
	 gs.DiagramNumber	as DiagramNumber
	,@UnitID	as UnitId
	,gs.HeadCode	as HeadCode
	,gs.TrainStartLocation	as TrainStartLocation
	,glts.ID	as TrainStartLocationId
	,gs.TrainStartTime	as TrainStartTime
	,te.LegEndLocation	as TrainEndLocation
	,glte.ID	as TrainEndLocationId
	,te.LegEndTime	as TrainEndTime
	,gs.LegStartLocation	as LegStartLocation
	,glls.ID	as LegStartLocationId
	,gs.LegStartTime	as LegStartTime
	,gs.LegEndLocation	as LegEndLocation
	,glle.ID	as LegEndLocationId
	,gs.LegEndTime	as LegEndTime
	,gs.LegLength	as LegLength
	,gs.CoupledUnits	as CoupledUnits	
	,isnull(gs.UnitPosition, 1)	as UnitPosition
	,gs.VehicleFormation	as VehicleFormation
	,/*d.DiagramDate*/ NULL	as DiagramDate
	,row_number() over 
	(partition by gs.DiagramNumber 
	 order by gs.LegStartTime asc)	as SequenceNumber
	from
	dbo.GeniusStaging gs
	inner hash join (
	select HeadCode, DiagramNumber, LegEndLocation, LegEndTime
	from dbo.GeniusStaging gs2
	where LegEndTime = (select max(LegEndTime) from dbo.GeniusStaging gs3
	where gs2.DiagramNumber = gs3.DiagramNumber
	and gs2.HeadCode = gs3.HeadCode
	and gs3.GeniusInterfaceFileID = @fileId)
	and gs2.GeniusInterfaceFileID = @fileId) as te
	on gs.DiagramNumber = te.DiagramNumber and gs.HeadCode = te.HeadCode
	left outer join dbo.Location glts on glts.Tiploc = gs.TrainStartLocation
	left outer join dbo.Location glte on glte.Tiploc = gs.TrainEndLocation
	left outer join dbo.Location glls on glls.Tiploc = gs.LegStartLocation
	left outer join dbo.Location glle on glle.Tiploc = gs.LegEndLocation
	where 
	gs.GeniusInterfaceFileID = @fileId
	)
	INSERT INTO [dbo].[FleetStatusStaging]
		([UnitId]
		,[DiagramNumber]
		,[HeadCode]
		,[TrainStartLocationId]
		,[TrainStartLocationTiploc]
		,[TrainStartTime]
		,[TrainEndLocationId]
		,[TrainEndLocationTiploc]
		,[TrainEndTime]
		,[StartLocationId]
		,[StartLocationTiploc]
		,[StartTime]
		,[EndLocationId]
		,[EndLocationTipLoc]
		,[EndTime]
		,[Length]
		,[CoupledUnits]
		,[UnitPosition]
		,[VehicleFormation]
		,[SequenceNumber])
	 SELECT
	[UnitId]
		,DiagramNumber
		,[HeadCode]
				
		,[TrainStartLocationId]
		,[TrainStartLocation]
		,dbo.FN_ConvertBstToGmt([TrainStartTime])
				
		,[TrainEndLocationId]
		,[TrainEndLocation]
		,dbo.FN_ConvertBstToGmt([TrainEndTime])
				
		,[LegStartLocationId]
		,[LegStartLocation]
		,dbo.FN_ConvertBstToGmt([LegStartTime])
				
		,[LegEndLocationId]
		,[LegEndLocation]
		,dbo.FN_ConvertBstToGmt([LegEndTime])
				
		,[LegLength]
		,[CoupledUnits]
		,[UnitPosition]
		,[VehicleFormation]
		,[SequenceNumber]
	FROM CteGeniusStaging
	
	SET @rowNumber = @@ROWCOUNT

	COMMIT


	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileId, @status = 'File fully imported.'
	select @logText = convert(varchar, @rowNumber) + ' Rows successfully imported.'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;
	--commit;
	
	end try
	begin catch
	--if @@trancount > 0 rollback;
		SELECT @logText = ERROR_MESSAGE();	
	exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
