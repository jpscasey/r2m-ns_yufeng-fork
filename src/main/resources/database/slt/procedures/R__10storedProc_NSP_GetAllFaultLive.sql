SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetAllFaultLive', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetAllFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetAllFaultLive AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetAllFaultLive]
AS

    SET NOCOUNT ON;

    SELECT
        f.ID    
        , CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode 
        , null as LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , IsDelayed 
        , Description   
        , Summary   
        , FaultTypeID 
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , null AS MaximoId
        , null AS MaximoServiceRequestStatus
		, null AS CreatedByRule
		, CountSinceLastMaint
    FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        WHERE IsCurrent = 1
    ORDER BY CreateTime DESC
GO
