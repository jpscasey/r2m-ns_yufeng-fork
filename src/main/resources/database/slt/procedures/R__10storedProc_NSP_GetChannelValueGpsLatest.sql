SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetChannelValueGpsLatest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetChannelValueGpsLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetChannelValueGpsLatest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetChannelValueGpsLatest]
AS
	BEGIN
	
		-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
		CREATE TABLE #LatestGpsValue (
			UnitID int PRIMARY KEY
			, LastTimeStamp datetime2(3)
		)
		INSERT #LatestGpsValue( UnitID , LastTimeStamp)
		SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
		FROM dbo.VW_ChannelValueGps c
		GROUP BY c.UnitID
		
	
	
		SELECT
		
		CAST([Timestamp] AS datetime2(3)) AS Timestamp,
		fs.FleetFormationID,
		fs.UnitId,
		Col8086,--Latitude
		Col8087,--Longitude
		Col8124,--Speed
		l.LocationCode AS Location,
		ServiceStatus		= CASE 
				WHEN fs.Headcode IS NOT NULL
					AND fs.LocationIDHeadcodeStart = l.ID
					THEN 'Ready for Service'
				WHEN fs.Headcode IS NOT NULL
					THEN 'In Service'
				ELSE 'Out of Service'
			END
		
		
		FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
		LEFT JOIN #LatestGpsValue vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
		LEFT JOIN dbo.VW_ChannelValueGps cvg WITH (NOLOCK) ON cvg.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cvg.UnitID
		LEFT JOIN dbo.Location l ON l.ID = (
				SELECT TOP 1 LocationID
				FROM dbo.LocationArea
				WHERE cvg.Col8086 BETWEEN MinLatitude AND MaxLatitude
					AND cvg.Col8087 BETWEEN MinLongitude AND MaxLongitude
				ORDER BY Priority
		)
		WHERE Col8086 IS NOT NULL AND Col8087 IS NOT NULL
	
	END
GO