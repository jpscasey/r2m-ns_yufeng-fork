SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetCurrentFaults', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetCurrentFaults' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetCurrentFaults AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetCurrentFaults]
    @N int
    , @isLive         bit          = NULL   --Live
    , @isAcknowledged bit          = NULL   --Acknowledged
    , @FleetCodes     varchar(100) = NULL   --Fleet
    , @FaultTypeIDs   varchar(100) = NULL   --Severity
    , @CategoryIDs    varchar(100) = NULL   --Category
    , @HasRecoveryOnly    bit      = NULL   --IAS
    , @UnitTypes   	  varchar(100) = NULL   --UnitType
    , @UnitStatus     bit     	   = NULL   --UnitStatus
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @FleetTable TABLE (ID varchar(100))
    INSERT INTO @FleetTable
    SELECT * from dbo.SplitStrings_CTE(@FleetCodes,',')

    DECLARE @TypeTable TABLE (ID int)
    INSERT INTO @TypeTable
    SELECT * from dbo.SplitStrings_CTE(@FaultTypeIDs,',')

    DECLARE @CategoryTable TABLE (ID int)
    INSERT INTO @CategoryTable
    SELECT * from dbo.SplitStrings_CTE(@CategoryIDs,',')

    DECLARE @UnitTypeTable TABLE (ID varchar(100))
    INSERT INTO @UnitTypeTable
    SELECT * from dbo.SplitStrings_CTE(@UnitTypes,',')

    IF @FleetCodes   = '' SET @FleetCodes   = NULL
    IF @FaultTypeIDs = '' SET @FaultTypeIDs = NULL
    IF @CategoryIDs  = '' SET @CategoryIDs  = NULL
    IF @UnitTypes    = '' SET @UnitTypes    = NULL

    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode 
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , IsAcknowledged
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, CountSinceLastMaint
    FROM 
        dbo.VW_IX_Fault f
        LEFT JOIN dbo.Location ON f.LocationID = Location.ID
        LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
        LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
        LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE 
            (f.IsCurrent      = @isLive                             OR @isLive         IS NULL)
        AND (f.IsAcknowledged = @isAcknowledged                     OR @isAcknowledged IS NULL)
        AND (f.FaultTypeID    IN (SELECT ID FROM @TypeTable)        OR @FaultTypeIDs   IS NULL)
        AND (f.CategoryID     IN (SELECT ID FROM @CategoryTable)    OR @CategoryIDs    IS NULL)
        AND (f.FleetCode      IN (SELECT ID FROM @FleetTable)       OR @FleetCodes     IS NULL)
        AND (f.HasRecovery    = @HasRecoveryOnly                    OR @HasRecoveryOnly    IS NULL
            OR @HasRecoveryOnly = 0)
        AND (u.UnitType    IN (SELECT ID FROM @UnitTypeTable)    OR @UnitTypes     IS NULL)
        AND (CASE WHEN @UnitStatus = 1 and (u.UnitStatus = 'BVD' or u.UnitStatus = 'OVERSTAND') then 1 
					WHEN @UnitStatus = '0' then 1
					WHEN @UnitStatus IS NULL then 1
					ELSE  0 END = 1
				)
        AND ReportingOnly = 0
    ORDER BY 
        f.CreateTime DESC

END
GO
