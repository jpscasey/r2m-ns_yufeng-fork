SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueGps', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueGps' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueGps AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueGps]
    @DatetimeStart datetime2(3)
    , @DatetimeEnd datetime2(3)
    , @UnitID int = NULL
    , @UpdateRecord bit = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        gcv.ID
        ,gcv.UnitID
        ,gcv.Col8086 --Latitude
        ,gcv.Col8087 --Longitude
        ,gcv.Col8124 --Speed
        ,gcv.TimeStamp
		,LocationId = l.ID
		,Tiploc = l.Tiploc
		,LocationName = l.LocationName
    FROM dbo.VW_ChannelValueGps gcv
	LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE gcv.Col8086 BETWEEN MinLatitude AND MaxLatitude
            AND gcv.Col8087 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
	)
    WHERE gcv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
        AND (gcv.UnitID = @UnitID OR @UnitID IS NULL)
    ORDER BY TimeStamp

END
GO
