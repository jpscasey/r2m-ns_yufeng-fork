SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueGpsLatest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueGpsLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueGpsLatest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueGpsLatest]
    @DatetimeStart DATETIME2(3)
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH ChannelValueGpsLatest
    AS (
        SELECT MAX(cvg.TimeStamp) AS TimeStamp
            ,cvg.UnitID
        FROM dbo.ChannelValueGps cvg
        WHERE cvg.TimeStamp <= @DatetimeStart
            AND cvg.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND cvg.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY cvg.UnitID
        )
    SELECT   
		gcv.ID
        ,gcv.UnitID
        ,gcv.Col8086 --Latitude
        ,gcv.Col8087 --Longitude
        ,gcv.Col8124 --Speed
        ,gcv.TimeStamp
		,LocationId = l.ID
		,Tiploc = l.Tiploc
		,LocationName = l.LocationName
	FROM ChannelValueGpsLatest cvgl
	JOIN [dbo].[VW_ChannelValueGps] gcv ON cvgl.UnitID = gcv.UnitID AND cvgl.TimeStamp = gcv.TimeStamp		
	LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE gcv.Col8086 BETWEEN MinLatitude AND MaxLatitude
            AND gcv.Col8087 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
	)
END
GO
