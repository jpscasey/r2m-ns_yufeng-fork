SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValue_L2', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValue_L2' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValue_L2 AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue_L2]
    @DatetimeStart datetime2(3)
    , @DatetimeEnd datetime2(3)
    , @UnitID int = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        f.ID
        , FaultMetaID       = f.FaultMetaID
        , FaultCode         = fm.FaultCode
        , FaultCategoryID   = fm.CategoryID
        , FaultCategory     = fc.Category
        , FaultTypeID       = fm.FaultTypeID
        , FaultType         = ft.Name
        , UnitID         = u.ID
        , UnitNumber        = u.UnitNumber
        , UnitType          = u.UnitType
        , TimeStamp         = f.CreateTime
		, FaultCount		= f.CountSinceLastMaint
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm
        ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.Unit u
        ON f.FaultUnitID = u.ID
    INNER JOIN dbo.FaultType ft
        ON fm.FaultTypeID = ft.ID
    INNER JOIN dbo.FaultCategory fc
        ON fm.CategoryID = fc.ID
    WHERE f.CreateTime BETWEEN @DatetimeStart AND @DatetimeEnd
    AND (u.ID = @UnitID OR @UnitID IS NULL)
    ORDER BY TimeStamp
    
END
GO
