SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultDetail]
(
    @FaultID int
)
AS
BEGIN

        SELECT
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode
            , fmextra.Value as MaximoFaultCode
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            --, UnitNumber   
            , FleetCode  
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType
            , FaultTypeColor
            , f.FaultTypeID
            , Category
            , f.CategoryID
            , ReportingOnly
            , IsAcknowledged
            , '' as TransmissionStatus
            , fme.E2MFaultCode
            , fme.E2MDescription
            , fme.E2MPriorityCode
            , fme.E2MPriority
            , fm.Url
            , fm.AdditionalInfo
            , f.HasRecovery
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
            , CreatedByRule
            , RowVersion
            , f.FaultGroupID
            , f.IsGroupLead
            , f.RuleName
            , f.RecoveryStatus
			, f.CountSinceLastMaint
			, UnitStatus = CASE WHEN f.UnitStatusFault in ('OVERSTAND', 'BVD') THEN 1 ELSE 0 END
			, f.Priority as serviceRequestPriority
			, (fm.AdditionalInfo + ' ' + ea.value) as longDescription
			, sc.value as systemCode
			, crp.value as controlRoomPriority
			, qp.value as qualityProfile
			, sp.value as safetyPriority
			, mf.value as maintenanceFunction
			, pc.value as locationCode
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
        INNER JOIN dbo.FaultMetaExtraField fmextra ON fmextra.FaultMetaId = f.FaultMetaID AND fmextra.Field='MaximoFaultCode'
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
        LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID -- todo: this join is done in the view, why do it again?
        -- get external references
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
		LEFT JOIN dbo.FaultMetaExtraField ea on (ea.FaultMetaId = fm.ID and ea.Field = 'engineerAdvice')
		LEFT JOIN dbo.FaultMetaExtraField sc on (sc.FaultMetaId = fm.ID and sc.Field = 'systemCode')
		LEFT JOIN dbo.FaultMetaExtraField crp on (crp.FaultMetaId = fm.ID and crp.Field = 'controlRoomPriority')
		LEFT JOIN dbo.FaultMetaExtraField qp on (qp.FaultMetaId = fm.ID and qp.Field = 'qualityProfile')
		LEFT JOIN dbo.FaultMetaExtraField sp on (sp.FaultMetaId = fm.ID and sp.Field = 'safetyPriority')
		LEFT JOIN dbo.FaultMetaExtraField mf on (mf.FaultMetaId = fm.ID and mf.Field = 'maintenanceFunction')
		LEFT JOIN dbo.FaultMetaExtraField pc on (pc.FaultMetaId = fm.ID and pc.Field = 'positionCode')
        WHERE f.ID = @FaultID

END
GO
