SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultEventChannelValueByID', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultEventChannelValueByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelValueByID AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueByID]
    @ID int,
    @Datetime datetime2(3)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.EventChannelValue), 0) - 1
    ELSE
        SET @ID = (
            SELECT TOP 1 (EventChannelValue.ID - 1) 
            FROM dbo.EventChannelValue WITH(NOLOCK)
            WHERE EventChannelValue.ID > @ID
            ORDER BY EventChannelValue.ID ASC
        )

    SELECT TOP 10000 ecv.ID
        ,ecv.UnitID
        ,ecv.ChannelID
        ,ecv.TimeStamp
        ,ecv.Value
    FROM dbo.EventChannelValue ecv
    WHERE ecv.ID > @ID AND ecv.TimeStamp <= @Datetime ORDER BY ID ASC
END
GO
