SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultLive', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultLive' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLive AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLive]
    @N int --Limit
    , @FaultTypeID int  = NULL  --Severity
    , @CategoryID int   = NULL  --Category
AS

    SET NOCOUNT ON;
    
    SELECT TOP (@N)
        f.ID    
        , f.CreateTime    
        , HeadCode  
        , SetCode   
        , FaultCode   
        , LocationCode  
        , IsCurrent 
        , EndTime   
        , RecoveryID    
        , Latitude  
        , Longitude 
        , FaultMetaID   
        , FaultUnitID    
        , FaultUnitNumber
        , PrimaryUnitNumber     = up.UnitNumber
        , SecondaryUnitNumber   = us.UnitNumber
        , TertiaryUnitNumber    = ut.UnitNumber
        , IsDelayed 
        , f.Description   
        , Summary   
        , FaultType
        , FaultTypeColor
        , Category
        , CategoryID
        , ReportingOnly
        , RecoveryStatus
        , FleetCode
        , HasRecovery
        , MaximoID = sr.ExternalCode
        , MaximoServiceRequestStatus = sr.Status
        , CreatedByRule
		, CountSinceLastMaint
    FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
    LEFT JOIN Location ON f.LocationID = Location.ID
    LEFT JOIN dbo.Unit up ON up.ID = f.PrimaryUnitID
    LEFT JOIN dbo.Unit us ON us.ID = f.SecondaryUnitID
    LEFT JOIN dbo.Unit ut ON ut.ID = f.TertiaryUnitID
    -- get external references
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
    WHERE  f.FaultTypeID = COALESCE(NULLIF(@FaultTypeID, ''), f.FaultTypeID)
        AND f.CategoryID = COALESCE(NULLIF(@CategoryID, ''), f.CategoryID)
        AND ReportingOnly = 0
        AND IsCurrent = 1
    ORDER BY f.CreateTime DESC
GO
