SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultLiveCount', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultLiveCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultLiveCount AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultLiveCount]
AS
BEGIN
	SET NOCOUNT ON

	SELECT COUNT(*) AS FaultCount
	FROM dbo.VW_IX_Fault WITH (NOEXPAND, NOLOCK)
	WHERE ReportingOnly = 0
	AND IsCurrent = 1

END
GO
