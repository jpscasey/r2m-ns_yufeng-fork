SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricChannelData', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelData AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelData]
    @DateTimeEnd datetime2(3)
    , @Interval int -- number of seconds
    , @UnitID int
    , @SelectColList varchar(4000)
    , @N int = NULL --record number
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @sql varchar(max)

    IF @DateTimeEnd IS NULL 
        SET @DateTimeEnd = SYSDATETIME()
    ELSE
        SET @DateTimeEnd = DATEADD(ss, 1, @DateTimeEnd)
    
    DECLARE @DateTimeStart datetime2(3) = DATEADD(s, - @Interval, @DateTimeEnd)
    
    SET @sql = '
SELECT TOP (' + CAST(ISNULL(@N, 30000) AS varchar(10)) + ')
    TimeStamp = CAST(cv.Timestamp AS DATETIME2(3))
    , UnitID = ' + CAST(@UnitID AS varchar(10)) + '
    , ' + @SelectColList + '
FROM dbo.ChannelValue cv
INNER JOIN dbo.ChannelValueDoor cvd

ON 
   cv.ID = cvd.ID
    WHERE cv.UnitID = '+cast(@unitID as varchar(10))+'
    AND cv.Timestamp BETWEEN ''' + CONVERT(varchar(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(varchar(23), @DateTimeEnd, 121) + '''
ORDER BY 1 ASC'

    EXEC (@sql)
    
END

GO
