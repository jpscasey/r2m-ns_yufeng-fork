SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertChannelLimitAnalog', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertChannelLimitAnalog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelLimitAnalog AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelLimitAnalog]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @ErrorLowMin varchar(10)
	, @ErrorLowMax varchar(10)
	, @WarningLowMin varchar(10)
	, @WarningLowMax varchar(10)
	, @NormalMin varchar(10)
	, @NormalMax varchar(10)
	, @WarningHighMin varchar(10)
	, @WarningHighMax varchar(10)
	, @ErrorHighMin varchar(10)
	, @ErrorHighMax varchar(10)
	, @InvalidLowMax varchar(10) = NULL
	, @InvalidHighMin varchar(10) = NULL
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM dbo.Channel WHERE ID = @ChannelID)


--	PRINT '** ' + CAST(@ChannelID AS varchar(10)) + ' - ' + ISNULL(@ChannelName, 'NULL') + ' details:'

	-- ERROR LOW
	IF ISNUMERIC(@ErrorLowMin) = 1 OR ISNUMERIC(@ErrorLowMax) = 1 
	BEGIN
		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7
			, @ChannelName + ' - FAULT LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMin) = 1 THEN @ErrorLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorLowMax) = 1 THEN @ErrorLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Warning LOW
	IF ISNUMERIC(@WarningLowMin) = 1 OR ISNUMERIC(@WarningLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningLowMin) = 1 THEN @WarningLowMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningLowMax) = 1 THEN @WarningLowMax
			END 
			, 1
			, 0
			
	END
	
	
	-- Normal
	IF ISNUMERIC(@NormalMin) = 1 OR ISNUMERIC(@NormalMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@NormalMin) = 1 THEN @NormalMin
			END 
			, CASE	
				WHEN ISNUMERIC(@NormalMax) = 1 THEN @NormalMax
			END 
			, 1
			, 1
			
	END
	
	-- Warning HIGH
	IF ISNUMERIC(@WarningHighMin) = 1 OR ISNUMERIC(@WarningHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@WarningHighMin) = 1 THEN @WarningHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@WarningHighMax) = 1 THEN @WarningHighMax
			END 
			, 0
			, 1
			
	END

	
	-- ERROR HIGH
	IF ISNUMERIC(@ErrorHighMin) = 1 OR ISNUMERIC(@ErrorHighMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMin) = 1 THEN @ErrorHighMin
			END 
			, CASE	
				WHEN ISNUMERIC(@ErrorHighMax) = 1 THEN @ErrorHighMax
			END 
			, 0
			, 1
			
	END


	-- INVALID High
	IF ISNUMERIC(@InvalidHighMin) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID HIGH' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, CASE	
				WHEN ISNUMERIC(@InvalidHighMin) = 1 THEN @InvalidHighMin
			END 
			, NULL
			, 0
			, 0
			
	END
	
	-- INVALID Low
	IF ISNUMERIC(@InvalidLowMax) = 1 
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, -1 --INVALID
			, @ChannelName + ' - INVALID LOW' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID
			, @ChannelID
			, NULL
			, CASE	
				WHEN ISNUMERIC(@InvalidLowMax) = 1 THEN @InvalidLowMax
			END 
			, 0
			, 0
			
	END

END
GO
