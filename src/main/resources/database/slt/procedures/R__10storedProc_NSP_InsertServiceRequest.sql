SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertServiceRequest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertServiceRequest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertServiceRequest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertServiceRequest](
    @FaultID int
    , @ServiceRequestId varchar(50)
	, @CreatedByRule bit
)
AS
BEGIN

	DECLARE @ServiceRequestInternalID int
	DECLARE @FaultGroupID int

	CREATE TABLE #temp (
		serviceRequestPriority varchar NULL
		, longDescription varchar(max) NULL
		, systemCode varchar(max) NULL
		, controlRoomPriority varchar(max) NULL
		, qualityProfile varchar(max) NULL
		, safetyPriority varchar(max) NULL
		, maintenanceFunction varchar(max) NULL
		, locationCode varchar(max) NULL
		, diagnosticalCode varchar(50) NULL
	)

	INSERT INTO #temp
	SELECT f.Priority as serviceRequestPriority
		, (ea.value + ' ' + fm.AdditionalInfo) as longDescription
		, sc.value as systemCode
		, crp.value as controlRoomPriority
		, qp.value as qualityProfile
		, sp.value as safetyPriority
		, mf.value as maintenanceFunction
		, pc.value as locationCode
		, fm.FaultCode
	FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
	LEFT JOIN dbo.Location ON Location.ID = LocationID
	LEFT JOIN dbo.FaultMetaE2M fme ON fme.FaultMetaID = f.FaultMetaID
	LEFT JOIN dbo.FaultMeta fm ON fm.ID = f.FaultMetaID
	LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
	LEFT JOIN dbo.FaultMetaExtraField ea on (ea.FaultMetaId = fm.ID and ea.Field = 'engineerAdvice')
	LEFT JOIN dbo.FaultMetaExtraField sc on (sc.FaultMetaId = fm.ID and sc.Field = 'systemCode')
	LEFT JOIN dbo.FaultMetaExtraField crp on (crp.FaultMetaId = fm.ID and crp.Field = 'controlRoomPriority')
	LEFT JOIN dbo.FaultMetaExtraField qp on (qp.FaultMetaId = fm.ID and qp.Field = 'qualityProfile')
	LEFT JOIN dbo.FaultMetaExtraField sp on (sp.FaultMetaId = fm.ID and sp.Field = 'safetyPriority')
	LEFT JOIN dbo.FaultMetaExtraField mf on (mf.FaultMetaId = fm.ID and mf.Field = 'maintenanceFunction')
	LEFT JOIN dbo.FaultMetaExtraField pc on (pc.FaultMetaId = fm.ID and pc.Field = 'positionCode')
	WHERE f.ID = @FaultID

	INSERT INTO ServiceRequest (FaultID
		, UnitID
		, ExternalCode
		, Status
		, CreateTime
		, LastUpdateTime
		, CreatedByRule
		, Description
		, VkbPriority
		, Classification
		, MbnUrgency
		, PositionCode
		, QProfileCode
		, SafetyDisruptionLevel
		, WorkorderLocationType
		, DiagnosticalCode
		, NotificationParty
	) 
	VALUES (		
		@FaultID
		, (SELECT FaultUnitID FROM dbo.Fault WHERE ID = @FaultID)
		, @ServiceRequestId
		, 'NIEUW'
		, SYSDATETIME()
		, SYSDATETIME()
		, @CreatedByRule
		, (SELECT t.longDescription FROM #temp t)
		, (SELECT t.serviceRequestPriority FROM #temp t)
		, (SELECT t.systemCode FROM #temp t)
		, (SELECT t.controlRoomPriority FROM #temp t)
		, (SELECT t.locationCode FROM #temp t)
		, (SELECT t.qualityProfile FROM #temp t)
		, (SELECT t.safetyPriority FROM #temp t)
		, (SELECT t.maintenanceFunction FROM #temp t)
		, (SELECT t.diagnosticalCode FROM #temp t)
		, 'R2M'
	)
	
	SELECT @ServiceRequestInternalID = @@IDENTITY
	SET @FaultGroupID = (SELECT FaultGroupID FROM FAULT WHERE ID = @FaultID)

	UPDATE Fault
	SET LastUpdateTime = SYSDATETIME(),
	    ServiceRequestID = @ServiceRequestInternalID,
	    IsAcknowledged = 1,
	    AcknowledgedTime = SYSDATETIME()
	WHERE ID = @FaultID
		OR ( @FaultGroupID IS NOT NULL AND FaultGroupID = @FaultGroupID )
END
GO
