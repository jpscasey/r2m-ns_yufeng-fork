SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaDelayDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaDelayDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaDelayDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaDelayDetail]
(
	@TrainPassageID int
	, @SegmentID int
	, @SectionID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	IF @SectionID <> 0
		-- select data for Section
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM dbo.TrainPassageSection
		INNER JOIN dbo.TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN dbo.DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SectionID = @SectionID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	 
	ELSE
		-- select data for Segment
		SELECT 
			COUNT(*) AS DelayNumber
			, CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) AS TotalDuration
			, Name
			, ChartColor 
		FROM dbo.TrainPassageSection
		INNER JOIN dbo.TrainPassageSectionDelayReason ON TrainPassageSection.ID = TrainPassageSectionID
		INNER JOIN dbo.DelayReason ON DelayReasonID = DelayReason.ID
		WHERE TrainPassageID = @TrainPassageID
			AND SegmentID = @SegmentID
			AND DelayDuration >= 1000
		GROUP BY 
			Name
			, ChartColor
	
END
GO
