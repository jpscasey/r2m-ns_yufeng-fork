SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaDelaySummary', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaDelaySummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaDelaySummary AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaDelaySummary]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @ServiceGroupID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT 
		DelayReasonID
		, TotalDuration		= CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
	GROUP BY 
		DelayReasonID
		, Name
		, ChartColor	
	ORDER BY TotalDuration DESC
	
END
GO
