SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaDelaySummaryDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaDelaySummaryDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaDelaySummaryDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaDelaySummaryDetail]
(
	@DateStart date
	, @DateEnd date
	, @LocationID int
	, @DelayReasonID int
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
		JourneyID			= tp.JourneyID
		, TrainPassageID	= tp.ID
		, Headcode 
		, DelayDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, DelayName			= dr.Name
		, TiplocStart		= sl.Tiploc
		, TiplocEnd			= el.Tiploc
		, JourneyDate		= CAST(tps.ActualStartDatetime AS date)
		, LeadingVehicle	= v.VehicleNumber
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection tps ON tp.ID = tps.TrainPassageID 
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.Location sl ON s.StartLocationID = sl.ID
	INNER JOIN dbo.Location el ON s.EndLocationID = el.ID
	INNER JOIN dbo.TrainPassageSectionDelayReason tpsdr ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.DelayReason dr ON tpsdr.DelayReasonID = dr.ID
	LEFT JOIN dbo.Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE 
		(s.StartLocationID = @LocationID OR @LocationID IS NULL)
		AND DelayDuration >= 1000
		AND tp.StartDatetime BETWEEN @DateStart AND @DateEnd
		AND DelayReasonID = @DelayReasonID
	ORDER BY DelayDuration DESC
		, tps.ActualStartDatetime
		, Headcode

END
GO
