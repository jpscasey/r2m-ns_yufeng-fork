SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaGetHeadcodeList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaGetHeadcodeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaGetHeadcodeList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaGetHeadcodeList]
(
	@HeadcodeString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @currentDate date = SYSDATETIME()

	SELECT
		JourneyID		= Journey.ID
		, Headcode
		, HeadcodeDesc	= HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, ValidFrom
		, ValidTo
		, Period		= 
			CASE
				WHEN @currentDate BETWEEN ValidFrom AND ValidTo 
					THEN 'Current'
				ELSE 'Previous'
			END
	FROM dbo.Journey
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Headcode LIKE '%' + @HeadcodeString + '%'
		AND ValidTo > DATEADD(YYYY, -1, SYSDATETIME())
		AND StpType = 'P'
	ORDER BY 
		ValidTo DESC
		, ValidFrom ASC

END
GO
