SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaJourneyTime', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaJourneyTime' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaJourneyTime AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaJourneyTime]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @journeyNumber int
	
	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM dbo.VW_TrainPassageValid tp	
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	)
	

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------
	
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM dbo.Journey 
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all segments for @JourneyID in selected time ranges
	---------------------------------------------------------------------------	
	IF OBJECT_ID('tempdb..#TempSegment') IS NOT NULL
		DROP TABLE #TempSegment
		
	SELECT
		SectionID		= 0 --This is segment level data SecionID = 0 to allow grouping results
		, SegmentID
		, TrainPassageID
		, ActualTime	= SUM(DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)) 
		, StartDate		= CAST(StartDatetime AS date)
	INTO #TempSegment
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND TrainPassageSection.ActualStartDatetime IS NOT NULL
		AND TrainPassageSection.ActualEndDatetime IS NOT NULL
	GROUP BY
		SegmentID
		, TrainPassageID
		, StartDatetime
	-- to exclude segments not having Actual Times for all sections
	HAVING COUNT(*) = (
		SELECT COUNT(*) 
		FROM dbo.Section 
		WHERE JourneyID = @JourneyID 
			AND Section.SegmentID = TrainPassageSection.SegmentID
		)

	---------------------------------------------------------------------------
	-- create temporary table with all sections for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#TempSection') IS NOT NULL
		DROP TABLE #TempSection

	SELECT
		SectionID			= TrainPassageSection.SectionID
		, SegmentID
		, TrainPassageID
		, ActualTime		= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
		, StartDate			= CAST(tp.StartDatetime AS date)
	INTO #TempSection
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSection ON TrainPassageID = tp.ID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate

	
	---------------------------------------------------------------------------
	-- Data to be displayed in OpsAnalysis table
	---------------------------------------------------------------------------

	SELECT
		SegmentID			= Segment.ID
		, SectionID			= 0
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime) 
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSegment WHERE SegmentID = Segment.ID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
	FROM dbo.Segment
	LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID

	UNION ALL
	
	SELECT
		SegmentID
		, SectionID			= Section.ID
		, StartLoc			= StartLocation.Tiploc
		, EndLoc			= EndLocation.Tiploc
		, Booked			= dbo.FN_ConvertSecondToTime(
			CASE	-- handling over midnight sections
				WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
					THEN DATEDIFF(s, StartTime, EndTime) + 86400 
				WHEN DATEDIFF(s, StartTime, EndTime) > 43200
					THEN DATEDIFF(s, StartTime, EndTime) - 86400 
				ELSE DATEDIFF(s, StartTime, EndTime)
			END)
		, RecoveryTime		= dbo.FN_ConvertSecondToTime(RecoveryTime)
		, OptimalRunTime	= dbo.FN_ConvertSecondToTime(IdealRunTime)
		, ActualMin			= (SELECT dbo.FN_ConvertSecondToTime(MIN(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMax			= (SELECT dbo.FN_ConvertSecondToTime(MAX(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMean		= (SELECT dbo.FN_ConvertSecondToTime(AVG(ActualTime)) FROM #TempSection WHERE Section.ID = SectionID)
		, ActualMinDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime ASC)
		, ActualMaxDate		= (SELECT TOP 1 StartDate FROM #TempSection WHERE Section.ID = SectionID ORDER BY ActualTime DESC)
	FROM dbo.Section
	LEFT JOIN dbo.Location AS StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location AS EndLocation ON EndLocation.ID = EndLocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SegmentID, SectionID

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------
	;WITH CteChartData AS 
	(
		SELECT
			StartLoc		= StartLocation.Tiploc
			, EndLoc		= EndLocation.Tiploc 
			, Booked		=
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, StartTime, EndTime) < - 43200
						THEN DATEDIFF(s, StartTime, EndTime) + 86400 
					WHEN DATEDIFF(s, StartTime, EndTime) > 43200
						THEN DATEDIFF(s, StartTime, EndTime) - 86400 
					ELSE DATEDIFF(s, StartTime, EndTime)
				END
			, RecoveryTime 
			, IdealRunTime
			, ActualMin		= (SELECT MIN(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMax		= (SELECT MAX(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMean	= (SELECT AVG(ActualTime) FROM #TempSegment WHERE SegmentID = Segment.ID)
			, ActualMinDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime ASC)
			, ActualMaxDate	= (SELECT TOP 1 StartDate FROM #TempSegment WHERE SegmentID = Segment.ID AND ActualTime IS NOT NULL ORDER BY ActualTime DESC)
			, SegmentID		= Segment.ID
		FROM dbo.Segment
		LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
		LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
		WHERE JourneyID = @JourneyID
	)
	SELECT 
		StartLoc
		, EndLoc
		, OptimalDiff		= IdealRunTime - Booked
		, ActualMinDiff		= ActualMin - Booked
		, ActualMaxDiff		= ActualMax - Booked
		, ActualMeanDiff	= ActualMean - Booked
		, ActualMinDate
		, ActualMaxDate
	FROM CteChartData
	ORDER BY SegmentID ASC

END
GO
