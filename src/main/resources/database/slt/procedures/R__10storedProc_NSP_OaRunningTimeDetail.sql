SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaRunningTimeDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaRunningTimeDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaRunningTimeDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaRunningTimeDetail]
(
	@DateStart date
	, @DateEnd date
	, @JourneyID int
	, @SectionID int
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		StartLocation		= sl.Tiploc
		, EndLocation		= el.Tiploc
		, BookedStartTime	= s.StartTime
		, BookedEndTime		= s.EndTime
		, BookedTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime)))
		, TrainPassageDate 	= CONVERT(date, tps.ActualDatetimeStart)
		, TrainPassageWeekDay 	= LEFT(DATENAME(W, tps.ActualDatetimeStart), 3)
		, ActualStartTime	= CONVERT(time(0),tps.ActualDatetimeStart)
		, ActualEndTime		= CONVERT(time(0),tps.ActualDatetimeEnd)
		, ActualTime		= dbo.FN_ConvertSecondToTime(dbo.FN_ValidateTimeDiff(DATEDIFF(S, tps.ActualDatetimeStart, tps.ActualDatetimeEnd)))
		, LeadingVehicle	= v.VehicleNumber
		, AverageSpeed		= tps.AverageSpeed
		, TotalEnergy		= tps.EnergyRealMove + tps.EnergyReactiveStop
		
	FROM dbo.TrainPassageSegment tps
	INNER JOIN dbo.Segment s ON 
		s.ID = tps.SegmentID
		AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.Location sl ON sl.ID = s.StartLocationID
	INNER JOIN dbo.Location el ON el.ID = s.EndLocationID
	LEFT JOIN dbo.Vehicle v ON v.ID = tps.LeadingVehicleID
	WHERE tps.JourneyID = @JourneyID
		AND tps.SegmentID = @SectionID
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
		
	ORDER BY tps.ActualDatetimeStart
	
END
GO
