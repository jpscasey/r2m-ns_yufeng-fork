SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaTimetableByHeadcode', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaTimetableByHeadcode' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaTimetableByHeadcode AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaTimetableByHeadcode]
(
	@StartDate datetime2(3)
	, @EndDate datetime2(3)
	, @N int = NULL
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	-- number of records returned
	IF @N IS NULL OR @N < 0
		SET @N = 1000

	
	SELECT 
		TrainPassageID		= tp.ID								
		, Headcode			= tp.Headcode
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber 
		, Setcode			= tp.Setcode
		, DelayOnArrival	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) / 60. 
		, DelayOnDeparture	= dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0)))) / 60.
	INTO #DelayAtDestination
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180) 
	
	SELECT
		TrainPassageID			= tp.ID
		, Headcode				= tp.Headcode
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate
		AND ( @excludeDelays = 0 OR (tp.ID IN (select dad.TrainPassageID from #DelayAtDestination dad ))) 
	GROUP BY 
		tp.ID
		, tp.Headcode
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode

	
	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival) 
			, MaxDelayOnArrival = MAX(DelayOnArrival) 
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
	SELECT TOP (@N)
		Headcode
		, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(SectionPointTypeNumber_S)
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_E))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(SectionPointTypeNumber_S) + SUM(SectionPointTypeNumber_B))
			END
		
	

	FROM #DelayAtStation
	GROUP BY
		Headcode		
	ORDER BY 4 DESC 

END
GO
