SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaTimetableByHeadcodeDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaTimetableByHeadcodeDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaTimetableByHeadcodeDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaTimetableByHeadcodeDetail]
(
	@StartDate datetime2(3)
	, @EndDate datetime2(3)
	, @Headcode varchar(4)
	, @excludeDelays Bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	
	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds
	
	SELECT 
		ValidID		= tp.ID
	INTO #ValidTrainPassages
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)

	SELECT
		TrainPassageID		= tp.ID
		, JourneyID			= tp.JourneyID
		, Headcode			= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram			= dbo.FN_GetDiagramByHeadcodeAndDate(tp.Headcode, tp.StartDatetime, tp.StartDatetime)
		, DateStart			= CAST(tp.StartDatetime AS date)
		, VehicleNumberA	= va.VehicleNumber
		, VehicleNumberB	= vb.VehicleNumber
		, tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60.
		, MaxDelayOnArrival		= MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, DelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(
			CASE
				WHEN SectionPointType = 'E'
					THEN DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))
			END) / 60.)
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.Journey j ON j.ID = tp.JourneyID
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND tp.Headcode = @Headcode
		AND tp.id IN (SELECT vtp.ValidID from #ValidTrainPassages vtp)
	GROUP BY 
		tp.ID
		, tp.JourneyID
		, j.StartTime
		, tp.Headcode
		, j.HeadcodeDesc
		, tp.StartDatetime	--CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		
	SELECT
		ds.Headcode
		, ds.Diagram
		, TrainPassageID
		, JourneyID
		, ds.DateStart
		, DestinationDelay		= DelayOnArrival 
		, AvgStationDelay		= AvgDelayOnArrival
		, MaxStationDelay		= MaxDelayOnArrival
		, ExtendedDwellsPerc	= CASE
			WHEN SectionPointTypeNumber_S > 0
				THEN 100.0 * ExtendedDwellsNumber / SectionPointTypeNumber_S
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * EarlyArrivalsNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_E)
			END
		, LateDeparturesPerc	= CASE
			WHEN SectionPointTypeNumber_S + 1 > 0
				THEN 100.0 * LateDeparturesNumber / (SectionPointTypeNumber_S + SectionPointTypeNumber_B)
			END
	FROM #DelayAtStation AS ds
	ORDER BY ds.DateStart ASC

END
GO
