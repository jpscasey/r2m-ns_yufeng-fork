SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaTimetableByLocationDetail', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaTimetableByLocationDetail' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaTimetableByLocationDetail AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaTimetableByLocationDetail]
(
	@StartDate datetime2(3)
	, @EndDate datetime2(3)
	, @LocationID int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT ymd

	DECLARE @extendedDwellLimit int = 60	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int = 60		-- in seconds
	DECLARE @lateDepartureLimit int = 60	-- in seconds

	SELECT
		TrainPassageID			= tp.ID
		, Location				= l.Tiploc
		, LocationID			= l.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		, VehicleNumberA		= va.VehicleNumber
		, VehicleNumberB		= vb.VehicleNumber
		, Setcode				= tp.Setcode
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) / 60. 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) / 60.
		, MIN(ScheduledArrival) AS ScheduledArrival
		, MIN(ScheduledDeparture) AS ScheduledDeparture
		, ExtendedDwellsNumber	= SUM(
			CASE 
				WHEN
					-- timing point is a Station 
					SectionPointType = 'S' 
					-- dwell time > booked dwell time + @extendedDwellLimit
					AND dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime)) > dbo.FN_ValidateTimeDiff(DATEDIFF(s, ScheduledArrival, ScheduledDeparture)) + @extendedDwellLimit
				THEN 1
			END)
		, EarlyArrivalsNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'E') 
					AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
				THEN 1
			END)
		, LateDeparturesNumber	= SUM(
			CASE 
				WHEN SectionPointType IN ('S', 'B') 
					AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
				THEN 1
			END)
		, SectionPointTypeNumber_B = SUM(CASE SectionPointType WHEN 'B' THEN 1 END)
		, SectionPointTypeNumber_S = SUM(CASE SectionPointType WHEN 'S' THEN 1 END)
		, SectionPointTypeNumber_E = SUM(CASE SectionPointType WHEN 'E' THEN 1 END)
	INTO #DelayAtStation
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	INNER JOIN dbo.Location l ON sp.LocationID = l.ID
	WHERE SectionPointType IN ('B', 'S', 'E')
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
		AND l.ID = @LocationID
		AND ( @excludeDelays = 0 OR dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0)))) < 180)
	GROUP BY 
		tp.ID
		, l.Tiploc
		, l.ID
		, CAST(tp.StartDatetime AS date)
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode


--SELECT * FROM #DelayAtStation WHERE Location = 'KNGX'
	
/*	;WITH CteDelayAtDestination AS
	(
		SELECT 
			Headcode
			, AvgDelayOnArrival = AVG(DelayOnArrival) 
			, MaxDelayOnArrival = MAX(DelayOnArrival) 
		FROM #DelayAtDestination
		GROUP BY
			Headcode
	)
*/	SELECT
		Location
		, LocationID
		, DateStart
--, Diagram				= dbo.FN_GetDiagramByHeadcodeAndDate(Headcode, @StartDate, @EndDate) --CAST('0000 xxxx yyyy 0000' AS varchar(30)) 
		, NumberOfPassages		= COUNT(*)					
--		, AvgDestinationDelay	= (SELECT AvgDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, AvgStationDelay		= AVG(AvgDelayOnArrival)
		, MaxStationDelay		= MAX(AvgDelayOnArrival)
--		, MaxDestinationDelay	= (SELECT MaxDelayOnArrival FROM CteDelayAtDestination WHERE CteDelayAtDestination.Headcode = #DelayAtStation.Headcode) 
		, ExtendedDwellsPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) <> 0 
				THEN 100.0 * SUM(ExtendedDwellsNumber) / SUM(ISNULL(SectionPointTypeNumber_S, 0))
			END
		, EarlyArrivalsPerc		= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)) <> 0 
				THEN 100.0 * SUM(EarlyArrivalsNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_E, 0)))
			END
		, LateDeparturesPerc	= CASE
			WHEN SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)) <> 0 
				THEN 100.0 * SUM(LateDeparturesNumber) / (SUM(ISNULL(SectionPointTypeNumber_S, 0)) + SUM(ISNULL(SectionPointTypeNumber_B, 0)))
			END
		
	FROM #DelayAtStation
	GROUP BY
		Location		
		, LocationID
		, DateStart
	ORDER BY DateStart ASC 

END
GO
