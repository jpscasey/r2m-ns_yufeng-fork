SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_PopulateOtherFaultCategoryMetaSNG', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_PopulateOtherFaultCategoryMetaSNG' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_PopulateOtherFaultCategoryMetaSNG AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_PopulateOtherFaultCategoryMetaSNG]
AS

BEGIN

DECLARE @ID int =0
DECLARE @sql varchar(max)

	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql = 'EXEC [dbo].[NSP_InsertFaultCategory_SNG] ' + cast(ID as varchar(10)) +' , '''+Category + ''', ' +cast(ReportingOnly as varchar(10)) 
			,@ID = ID
		FROM FaultCategory
		where ID > @ID
		ORDER BY ID
	--	SELECT @sql
		IF @sql is null BREAK;

		BEGIN TRY
			EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

	SET @ID = 0
	WHILE 1=1
	BEGIN
		SET @sql = null

		SELECT Top 1 @sql= 'EXEC [dbo].[NSP_InsertFaultMeta_SNG] ' + cast(ID as varchar(10)) +' , '''
		+ isnull(Username,'null') + ''', ''' 
		+isnull(FaultCode,'null') + ''', ''' 
		+isnull(REPLACE(Description,'''',''''''),'null') + ''', ''' 
		+isnull(REPLACE(Summary,'''',''''''),'null') + ''', ''' 
		+isnull(REPLACE(AdditionalInfo,'''',''''''),'null') + ''', ''' 
		+isnull(Url,'null') + ''', ' 
		+isnull(cast(CategoryID as varchar(10)),'null') + ', ' 
		+isnull(cast(HasRecovery as varchar(10)),'null') + ', ''' 
		+isnull(RecoveryProcessPath,'null') + ''', '
		+isnull(cast(+FaultTypeID as varchar(10)),'null') + ', '
		+isnull(cast(GroupID  as varchar(10)),'null')
		,@ID = ID
		FROM FaultMeta
		where ID > @ID
		ORDER BY ID

		IF @sql is null BREAK;

		BEGIN TRY
		EXEC (@SQL)
		END TRY
		BEGIN CATCH
			Print '' 
		END CATCH

	END

END
GO
