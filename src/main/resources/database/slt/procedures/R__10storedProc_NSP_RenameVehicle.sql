SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_RenameVehicle', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_RenameVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_RenameVehicle AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_RenameVehicle]
(
	@OldVehicleNumber varchar(16)
	,@NewVehicleNumber varchar(16)
)
AS
BEGIN

	SET NOCOUNT ON;
	IF EXISTS ( SELECT Top 1 1 FROM dbo.Vehicle WHERE VehicleNumber =@NewVehicleNumber) RETURN 0 

	UPDATE dbo.Vehicle SET VehicleNumber=@NewVehicleNumber WHERE VehicleNumber=@OldVehicleNumber
	UPDATE dbo.Unit SET UnitNumber=@NewVehicleNumber WHERE UnitNumber=@OldVehicleNumber
	UPDATE dbo.FleetFormation SET UnitNumberList=@NewVehicleNumber WHERE UnitNumberList=@OldVehicleNumber

END
GO
