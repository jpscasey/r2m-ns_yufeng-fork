SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_RethrowError', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_RethrowError' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_RethrowError AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_RethrowError]
as
/******************************************************************************
** Name: usp_RethrowError
** Description: Re raise the original value with MessageNr: 50000 (default)
**		 
**	 Example: ....
**				begin catch
**				 if xact_state() <> 0 rollback transaction
**				 EXEC usp_RethrowError;
**				end catch
**		 
**		 with seterror : Sets the @@ERROR and ERROR_NUMBER values to 50000, 
**		 regardless of the severity level
**
**	 Called by: every catch block if it's necessary	
** 
** Return values: 
*******************************************************************************
** Change History
*******************************************************************************
** Date:			Author:	 Description:
** 2012-02-01	 HeZ		 creation on database
*******************************************************************************/	
begin

	SET NOCOUNT ON;
	-- Return if there is no error information to retrieve.
	IF ERROR_NUMBER() IS NULL
		RETURN;

	DECLARE 
		@ErrorMessage	NVARCHAR(4000),
		@ErrorNumber	 INT,
		@ErrorSeverity	INT,
		@ErrorState	 INT,
		@ErrorLine		INT,
		@ErrorProcedure NVARCHAR(200);

	-- Assign variables to error-handling functions that 
	-- capture information for RAISERROR.
	SELECT 
		@ErrorNumber =	 ERROR_NUMBER(),
		@ErrorSeverity =	ERROR_SEVERITY(),
		@ErrorState =		ERROR_STATE(),
		@ErrorLine =		ERROR_LINE(),
		@ErrorProcedure =	ISNULL(ERROR_PROCEDURE(), '-');

	-- Build the message string that will contain original
	-- error information.
	SELECT @ErrorMessage = 
		N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
			'Message: '+ ERROR_MESSAGE();

	-- Raise an error: msg_str parameter of RAISERROR will contain
	-- the original error information.
	RAISERROR 
		(
		@ErrorMessage, 
		@ErrorSeverity, 
		1,				
		@ErrorNumber,	-- parameter: original error number.
		@ErrorSeverity, -- parameter: original error severity.
		@ErrorState,	 -- parameter: original error state.
		@ErrorProcedure, -- parameter: original error procedure name.
		@ErrorLine		-- parameter: original error line number.
		) with seterror;

END
GO
