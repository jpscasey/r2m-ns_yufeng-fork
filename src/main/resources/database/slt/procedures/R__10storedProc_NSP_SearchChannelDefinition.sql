SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SearchChannelDefinition', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SearchChannelDefinition' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchChannelDefinition AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SearchChannelDefinition]
	@ChannelId int = NULL,
	@Keyword varchar(50) = NULL
AS
BEGIN

	SET NOCOUNT ON;
	IF @ChannelId IS NULL
	BEGIN
		IF @Keyword IS NULL OR LEN(@Keyword) = 0
			SET @Keyword = '%'
		ELSE
			SET @Keyword = '%' + @Keyword + '%'
	END

	SELECT
			vcd.ID
			, vcd.VehicleID
			, vcd.ColumnID
			, vcd.ChannelGroupID
			, vcd.Name
			, vcd.Description
			, vcd.ChannelGroupOrder
			, vcd.Type
			, vcd.UOM
			, vcd.HardwareType
			, vcd.StorageMethod
			, vcd.Ref
			, vcd.MinValue
			, vcd.MaxValue
			, vcd.Comment
			, vcd.VisibleOnFaultOnly
			, vcd.IsAlwaysDisplayed
			, vcd.IsLookup
			, vcd.DefaultValue
			, vcd.IsDisplayedAsDifference
			, vcd.RelatedChannelID
			, vcd.RelatedChannelName
			, vcd.DataType
	FROM dbo.VW_ChannelDefinition as vcd
	INNER JOIN dbo.ChannelGroup as cgd ON vcd.ChannelGroupID = cgd.ID
	WHERE
		(@ChannelId IS NULL
			AND (vcd.ID LIKE @Keyword
				OR cgd.Notes LIKE @Keyword
				OR vcd.Description LIKE @Keyword
				OR vcd.Name LIKE @Keyword
				OR vcd.Ref LIKE @Keyword
				OR vcd.Type LIKE @Keyword
				OR vcd.UOM LIKE @Keyword))
		OR
		(@ChannelId IS NOT NULL AND vcd.ID = @ChannelId)
	ORDER BY vcd.ID
END
GO
