SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SearchFault', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SearchFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFault AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SearchFault]
    @N int --Limit
    , @DateFrom datetime2(3)
    , @DateTo datetime2(3)
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @Keyword varchar(50)
AS
BEGIN 
    IF @Keyword IS NULL OR LEN(@Keyword) = 0
        SET @Keyword = '%'
    ELSE
        SET @Keyword = '%' + @Keyword + '%'
        
        DECLARE @sqlcmd NVARCHAR(MAX);
        DECLARE @params NVARCHAR(MAX);
        DECLARE @KeywordForDescription varchar(50);
        
        
        SET @sqlcmd = '
        SELECT TOP ' + Convert(nvarchar(20),@N) + '
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode  
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , f.FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
            , CASE 
                WHEN CreatedByRule IS NULL THEN 0
                ELSE CreatedByRule
              END AS CreatedByRule
            , f.FaultGroupID
            , f.IsGroupLead
            , f.RowVersion
            , f.CountSinceLastMaint
            , fef.Value AS PositionCode
        FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        OUTER APPLY (SELECT Value FROM FaultMetaExtraField  fef WHERE f.FaultMetaID = fef.FaultMetaId and Field = ''positionCode'' ) fef
        WHERE (1 = 1) 
            AND f.CreateTime BETWEEN @DateFrom AND @DateTo'
        
        IF NOT @Keyword IS NULL 
        BEGIN
	        SELECT @KeywordForDescription = '%' + @Keyword + '%'
	        SELECT @Keyword = @Keyword + '%'
	        SET @sqlcmd = @sqlcmd +'
        
            AND (
                HeadCode                LIKE @Keyword 
                OR  FaultUnitNumber     LIKE @Keyword 
                OR  up.UnitNumber       LIKE @Keyword 
                OR  us.UnitNumber       LIKE @Keyword 
                OR  ut.UnitNumber       LIKE @Keyword 
                OR  Category            LIKE @Keyword 
                OR  f.Description       LIKE @Keyword
                OR  LocationCode        LIKE @Keyword 
                OR  FaultType           LIKE @Keyword
            )'
	        
	        END

    IF @SearchType = 'Live'
        SET @sqlcmd = @sqlcmd + 'AND f.IsCurrent = 1 '
    ELSE IF @SearchType = 'NoLive'
        SET @sqlcmd = @sqlcmd + 'AND f.IsCurrent = 0 '
        
    SET @sqlcmd = @sqlcmd + 'ORDER BY CreateTime DESC '
        
    --PRINT @sqlcmd
    SET @params = N'@DateFrom datetime2(3), @DateTo datetime2(3), @Keyword NVARCHAR(50), @KeywordForDescription  NVARCHAR(50)';
    EXECUTE sp_executesql @sqlcmd, @params, @DateFrom = @DateFrom, @DateTo = @DateTo, @Keyword=@Keyword, @KeywordForDescription= @KeywordForDescription;
END
GO
