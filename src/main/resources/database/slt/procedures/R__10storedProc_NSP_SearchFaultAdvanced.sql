SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SearchFaultAdvanced', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SearchFaultAdvanced' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvanced AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvanced](
    @N int --limit
    , @DateFrom datetime2(3)
    , @DateTo datetime2(3)
    , @SearchType varchar(10) --options: Live, NoLive, All
    , @FaultCode varchar(100)
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    -- additional parameters: need to be in alphabetical order
    , @IsCreatedByRule bit = NULL
    , @HasMaximoSR bit = NULL
    , @HasRecovery bit = NULL
    , @Headcode varchar(10) = NULL
    , @IsAcknowledged bit = NULL
    , @Location varchar(10) = NULL
    , @MaximoID varchar(20) = NULL
    , @PositionCode varchar(50) = NULL
    , @ServiceRequestStatus varchar(20) = NULL
    , @UnitNumber varchar(10) = NULL
    , @UnitStatus bit = NULL
    , @UnitType varchar(20) = NULL
)
AS
BEGIN
    SET NOCOUNT ON

    SELECT * INTO #TypeTable from dbo.SplitStrings_CTE(@Type,',')

    IF @FaultCode = ''  SET @FaultCode = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @Description = ''   SET @Description = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Location = ''   SET @Location = NULL
    IF @Type = ''       SET @Type = NULL
    IF @UnitType = ''   SET @UnitType = NULL
    IF @ServiceRequestStatus = ''   SET @ServiceRequestStatus = NULL
    IF @PositionCode = ''   SET @PositionCode = NULL
        
    DECLARE @sqlcmd NVARCHAR(MAX);
    DECLARE @params NVARCHAR(MAX);

    SET @sqlcmd = '
        SELECT TOP ' + Convert(nvarchar(20),@N) + '
            f.ID    
            , f.CreateTime    
            , HeadCode  
            , SetCode   
            , f.FaultCode   
            , LocationCode  
            , IsCurrent 
            , EndTime   
            , RecoveryID    
            , RecoveryStatus
            , Latitude  
            , Longitude    
            , FaultMetaID   
            , FaultUnitID    
            , FaultUnitNumber
            , IsDelayed 
            , f.Description   
            , f.Summary   
            , FaultType  
            , FaultTypeColor
            , Category
            , f.CategoryID
            , ReportingOnly
            , PrimaryUnitNumber     = up.UnitNumber
            , SecondaryUnitNumber   = us.UnitNumber
            , TertiaryUnitNumber    = ut.UnitNumber
            , RecoveryStatus
            , FleetCode 
            , f.AdditionalInfo
            , f.HasRecovery
            , f.IsAcknowledged
            , MaximoID = sr.ExternalCode
            , MaximoServiceRequestStatus = sr.Status
            , f.UnitStatusFault
            , CreatedByRule
            , f.FaultGroupID
            , f.IsGroupLead
            , f.RowVersion
            , f.CountSinceLastMaint
            , fef.Value AS PositionCode
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        INNER JOIN dbo.Unit up ON f.PrimaryUnitID = up.ID
        LEFT JOIN dbo.Unit us ON f.SecondaryUnitID = us.ID
        LEFT JOIN dbo.Unit ut ON f.TertiaryUnitID = ut.ID
        LEFT JOIN dbo.Unit u  ON u.ID  = f.FaultUnitID
        LEFT JOIN dbo.Location ON Location.ID = LocationID
        -- get external references
        LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
        OUTER APPLY (SELECT Value FROM dbo.FaultMetaExtraField  fef WHERE f.FaultMetaID = fef.FaultMetaId and Field = ''positionCode'' ) fef
        WHERE f.CreateTime BETWEEN @DateFrom AND @DateTo '
        
        IF @SearchType = 'Live'
            SET @sqlcmd = @sqlcmd + ' AND f.IsCurrent = 1 '
        ELSE IF @SearchType = 'NoLive'
            SET @sqlcmd = @sqlcmd + ' AND f.IsCurrent = 0 '
            
            IF @Headcode IS NOT NULL AND @Headcode <> '' AND @Headcode <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + 'AND f.Headcode LIKE @Headcode '
                SET @Headcode = replace(replace(@Headcode,  char(39), char(39) + char(39)),';','') 
            END

            IF @CategoryID IS NOT NULL
            BEGIN
                SET @sqlcmd = @sqlcmd + 'AND f.CategoryID = '+cast(@CategoryID as varchar(2))+' '
            END
            
            
            IF @Description IS NOT NULL AND @Description <> '' AND @Description <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + 'AND f.Description LIKE @Description '
                SET @Description = replace(replace(@Description,  char(39), char(39) + char(39)),';','')
            END
            
            IF @UnitNumber IS NOT NULL AND @UnitNumber <> '' AND @UnitNumber <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + 'AND FaultUnitNumber LIKE @UnitNumber '
                SET @UnitNumber = replace(replace(@UnitNumber,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @Location IS NOT NULL AND @Location <> '' AND @Location <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND LocationCode LIKE @Location '
                SET @Location = replace(replace(@Location,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @Type IS NOT NULL AND @Type <> '' AND @Type <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND f.FaultTypeID IN (SELECT Item FROM #TypeTable) '
                SET @Type = replace(replace(@Type,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @FaultCode IS NOT NULL AND @FaultCode <> '' AND @FaultCode <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND f.FaultCode LIKE @FaultCode '
                SET @FaultCode = replace(replace(@FaultCode,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @HasRecovery IS NOT NULL
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND f.HasRecovery = '+cast(@HasRecovery as varchar(2))+' '
            END
            
            IF @IsAcknowledged IS NOT NULL
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND f.IsAcknowledged = '+cast(@IsAcknowledged as varchar(2))+' '
            END
            
            IF @UnitType IS NOT NULL AND @UnitType <> '' AND @UnitType <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND u.UnitType LIKE @UnitType '
                SET @UnitType = replace(replace(@UnitType,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @MaximoID IS NOT NULL AND @MaximoID <> '' AND @MaximoID <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND sr.ExternalCode LIKE @MaximoID '
                SET @MaximoID = replace(replace(@MaximoID,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @PositionCode IS NOT NULL AND @PositionCode <> '' AND @PositionCode <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND fef.Value LIKE @PositionCode '
                SET @PositionCode = replace(replace(@PositionCode,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @HasMaximoSR IS NOT NULL
            BEGIN
                SET @sqlcmd = @sqlcmd +' AND (
                    sr.ExternalCode IS NOT NULL AND '+cast(@HasMaximoSR as varchar(2))+' = 1 
                    OR sr.ExternalCode IS NULL AND '+cast(@HasMaximoSR as varchar(2))+' = 0 
                ) '
            END
            
            IF @IsCreatedByRule IS NOT NULL
            BEGIN
            SET @sqlcmd = @sqlcmd +' AND (
                    '+cast(@IsCreatedByRule as varchar(2))+' = 1 AND sr.CreatedByRule = 1 
                    OR '+cast(@IsCreatedByRule as varchar(2))+' = 0 AND sr.CreatedByRule IS NULL
                ) '
            END
            
            IF @ServiceRequestStatus IS NOT NULL AND @ServiceRequestStatus <> '' AND @ServiceRequestStatus <> '-1'
            BEGIN
                SET @sqlcmd = @sqlcmd + ' AND sr.Status LIKE @ServiceRequestStatus '
                SET @ServiceRequestStatus = replace(replace(@ServiceRequestStatus,  char(39), char(39) + char(39)),';','') 
            END
            
            IF @UnitStatus IS NOT NULL 
            BEGIN
            SET @sqlcmd = @sqlcmd +' AND (
            CASE WHEN '+cast(@UnitStatus as varchar(2))+' = 1 and (f.UnitStatusFault = ''BVD'' OR f.UnitStatusFault = ''OVERSTAND'') then 1 
                    WHEN '+cast(@UnitStatus as varchar(2))+' = 0 and ((f.UnitStatusFault <> ''BVD'' AND f.UnitStatusFault <> ''OVERSTAND'') OR f.UnitStatusFault IS NULL) then 1
                    WHEN '+cast(@UnitStatus as varchar(2))+' IS NULL then 1
                    ELSE  0 END = 1
                ) '
            END
            SET @sqlcmd = @sqlcmd + ' ORDER BY f.CreateTime DESC '

            SET @params = N'@DateFrom datetime2(3), @DateTo datetime2(3), @Headcode varchar(10), @CategoryID int, @Description varchar(50), @UnitNumber varchar(10), @Location varchar(10), @Type varchar(10), @FaultCode varchar(100), @HasRecovery bit, @IsAcknowledged bit, @UnitType varchar(20), @MaximoID varchar(20), @PositionCode varchar(50), @HasMaximoSR bit, @IsCreatedByRule bit, @ServiceRequestStatus varchar(20), @UnitStatus bit';
            --PRINT @sqlcmd
            EXECUTE sp_executesql @sqlcmd, @params, @DateFrom = @DateFrom, @DateTo = @DateTo, @Headcode = @Headcode, @CategoryID = @CategoryID, @Description = @Description, @UnitNumber = @UnitNumber, @Location = @Location, @Type = @Type, @FaultCode = @FaultCode, @HasRecovery = @HasRecovery, @IsAcknowledged = @IsAcknowledged, @UnitType = @UnitType, @MaximoID = @MaximoID, @PositionCode = @PositionCode, @HasMaximoSR = @HasMaximoSR, @IsCreatedByRule = @IsCreatedByRule, @ServiceRequestStatus = @ServiceRequestStatus, @UnitStatus = @UnitStatus;
    
END
GO