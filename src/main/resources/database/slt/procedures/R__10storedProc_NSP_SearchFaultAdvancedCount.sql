SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SearchFaultAdvancedCount', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SearchFaultAdvancedCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultAdvancedCount AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultAdvancedCount](
    @DateFrom datetime2(3)
    , @DateTo datetime2(3)
    , @SearchType varchar(10) --options: Live, NoLive, All 
    , @FaultMetaID int
    , @Type varchar(10)
    , @CategoryID int
    , @Description varchar(50)
    , @Headcode varchar(10) = NULL
    , @UnitNumber varchar(10) = NULL
    , @Vehicle varchar(10) = NULL
    , @Location varchar(10) = NULL
	, @HasRecovery bit = NULL
)
AS
BEGIN 

    DECLARE @TypeTable TABLE (TypeNumber varchar(100))
    INSERT INTO @TypeTable
    SELECT * from [dbo].SplitStrings_CTE(@Type,',')
    
    IF @FaultMetaID = 0 SET @FaultMetaID = NULL
    IF @CategoryID = 0  SET @CategoryID = NULL
    IF @Headcode = ''   SET @Headcode = NULL
    IF @UnitNumber = '' SET @UnitNumber = NULL
    IF @Vehicle = ''    SET @Vehicle = NULL
    IF @Location = ''   SET @Location = NULL
        
    IF @SearchType = 'Live'
        
        SELECT  COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    IN (Select * from @TypeTable) OR @Type IS NULL)
				AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
				AND IsCurrent = 1
        
    IF @SearchType = 'NoLive'
        
        SELECT  COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        LEFT JOIN Location ON Location.ID = LocationID
            WHERE CreateTime BETWEEN @DateFrom AND @DateTo
                AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
                AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
                AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
                AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
                AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
                AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
                AND (FaultTypeID    IN (Select * from @TypeTable) OR @Type IS NULL)
				AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
				AND IsCurrent = 0

    IF @SearchType = 'All'
        
        SELECT  COUNT(*)
        FROM VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        LEFT JOIN Location ON Location.ID = LocationID
        WHERE CreateTime BETWEEN @DateFrom AND @DateTo
            AND (HeadCode       LIKE '%' + @Headcode + '%'      OR @Headcode IS NULL)
            AND (FaultUnitNumber     LIKE '%' + @UnitNumber + '%'    OR @UnitNumber IS NULL)
            AND (CategoryID     = @CategoryID                   OR @CategoryID IS NULL)
            AND (Description    LIKE '%' + @Description + '%'   OR @Description IS NULL)
            AND (LocationCode   LIKE '%' + @Location + '%'      OR @Location IS NULL)
            AND (FaultMetaID    = @FaultMetaID                  OR @FaultMetaID IS NULL)
            AND (FaultTypeID    IN (Select * from @TypeTable) OR @Type IS NULL)
			AND (f.HasRecovery    = @HasRecovery                  OR @HasRecovery IS NULL)
END
GO
