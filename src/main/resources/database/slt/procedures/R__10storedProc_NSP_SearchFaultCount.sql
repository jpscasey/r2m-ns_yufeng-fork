SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SearchFaultCount', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SearchFaultCount' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SearchFaultCount AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SearchFaultCount]
	@DateFrom datetime2(3)
	, @DateTo datetime2(3)
	, @SearchType varchar(10) --options: Live, NoLive, All 
	, @Keyword varchar(50)
AS
BEGIN

	SET NOCOUNT ON;
	 IF @Keyword IS NULL OR LEN(@Keyword) = 0
		SET @Keyword = '%'
	ELSE
		SET @Keyword = '%' + @Keyword + '%'

	IF @SearchType = 'Live'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				)
				AND IsCurrent = 1
		
	IF @SearchType = 'NoLive'
		
		SELECT COUNT(*)
			
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				)
				AND IsCurrent = 0
		
	IF @SearchType = 'All'
		
		SELECT COUNT(*)
		FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
		LEFT JOIN dbo.Location ON Location.ID = LocationID
			WHERE (1 = 1) 
				AND CreateTime BETWEEN @DateFrom AND @DateTo
				AND (
					HeadCode				LIKE @Keyword 
					OR FaultUnitNumber	 LIKE @Keyword
					OR Category			LIKE @Keyword 
					OR Description		 LIKE @Keyword
					OR LocationCode		LIKE @Keyword 
					OR FaultType			LIKE @Keyword
				) 
END
GO
