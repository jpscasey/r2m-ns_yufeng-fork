SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateFaultCategory', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateFaultCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateFaultCategory AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_UpdateFaultCategory](
	@ID int
	, @Category varchar(50)
	, @ReportingOnly bit
)
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRAN
	BEGIN TRY

		UPDATE dbo.FaultCategory SET
			Category = @Category
			, ReportingOnly = @ReportingOnly
		WHERE ID = @ID	
		
		EXEC NSP_UpdateFaultCategory_VIRM @ID,@Category,@ReportingOnly
		
		EXEC NSP_UpdateFaultCategory_SNG @ID,@Category,@ReportingOnly

		COMMIT TRAN
				
	END TRY
	BEGIN CATCH
		
		EXEC dbo.NSP_RethrowError;
		ROLLBACK TRAN;
		
	END CATCH
	
END
GO
