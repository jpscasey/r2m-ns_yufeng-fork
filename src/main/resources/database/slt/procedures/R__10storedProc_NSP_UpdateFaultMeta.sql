SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateFaultMeta', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateFaultMeta AS BEGIN RETURN(1) END;')
END
GO


ALTER PROCEDURE [dbo].[NSP_UpdateFaultMeta](
    @ID int
    , @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
    , @HasRecovery bit
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
    , @FromDate datetime2(3) = NULL
    , @ToDate datetime2(3) = NULL
)
AS
    BEGIN
        
        SET NOCOUNT ON;

        BEGIN TRAN
        BEGIN TRY

            UPDATE dbo.FaultMeta SET
                Username        = @Username
                , FaultCode  = @FaultCode
                , Description   = @Description 
                , Summary       = @Summary
                , AdditionalInfo = @AdditionalInfo
                , Url           = @Url
                , CategoryID    = @CategoryID
                , HasRecovery   = @HasRecovery
                , RecoveryProcessPath   = @RecoveryProcessPath
                , FaultTypeID   = @FaultTypeID
                , GroupID       = @GroupID
                , FromDate      = @FromDate
                , ToDate        = @ToDate
            WHERE ID = @ID

            COMMIT TRAN
        
        END TRY
        BEGIN CATCH

            EXEC dbo.NSP_RethrowError;
            ROLLBACK TRAN;

        END CATCH

    END
    
GO
