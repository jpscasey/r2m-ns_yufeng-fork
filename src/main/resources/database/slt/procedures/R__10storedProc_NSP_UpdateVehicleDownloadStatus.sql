SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateVehicleDownloadStatus', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateVehicleDownloadStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateVehicleDownloadStatus AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_UpdateVehicleDownloadStatus]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @vehicleDownloadProgressID int
	DECLARE @eventCategory int
	DECLARE @eventCode int
	DECLARE @vehicleID int
	DECLARE @vehicleDownloadID int
	DECLARE @vehicleDownloadStatus int
	
	WHILE 1=1
	BEGIN
		---------------------------------------------------------------------------------------

		SET @vehicleDownloadProgressID = (SELECT TOP 1 ID FROM VehicleDownloadProgress WHERE IsProcessed = 0 ORDER BY RecordInsert ASC)

		SET @eventCategory = NULL
		SET @eventCode = NULL
		SET @vehicleID = NULL
		SET @vehicleDownloadStatus = NULL
		SET @vehicleDownloadID = NULL
		
		SELECT
			@vehicleID = VehicleID
			, @eventCategory = EventCategory
			, @eventCode = EventCode
		FROM dbo.VehicleDownloadProgress
		WHERE ID = @vehicleDownloadProgressID

		IF @vehicleDownloadProgressID IS NULL 
			BREAK
			
		-- Find VehicleDownload.ID for vehicle that is not Completed or Failed
		-- VehicleDownload Status:
		-- 1 - Requested
		-- 2 - In Progress
		-- 0 - Completed
		-- 3 - Failed
		SELECT 
			@vehicleDownloadID = ID 
			, @vehicleDownloadStatus = Status
		FROM dbo.VehicleDownload 
		WHERE VehicleID = @vehicleID
			AND Status <> 0 -- Comlpeted
			AND Status <> 3 -- Failed

			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
					
		IF @vehicleDownloadID IS NOT NULL
			-- or Completed download 
			OR (@eventCategory = 1000 AND @eventCode = 0)

		BEGIN
		
			PRINT @vehicleDownloadID
			PRINT @eventCategory
			PRINT @eventCode
			
			-- @eventCategory = 24 - RMD Events
			------------------------------------------
			-- Ignored statuses:
			-- 0 Connected to TMS
			-- 3 Auto download request

			-- Failure statuses:
			-- 1 Connect timed out
			-- 5 Download timed out	Failed (RMD)
			-- 6 Download failed	Failed (RMD)
			-- 7 Got a download command and TMS log not enabled	Failed (RMD)
			-- 8 ATMS Buffer Overrun	Failed (RMD)
			IF @eventCategory = 24 AND @eventCode IN (1,5,6,7,8)
				UPDATE dbo.VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID
	
			-- 2 Manual download request	Requested
			-- 4 Download completed ok	Confirmed
			IF @eventCategory = 24 AND @eventCode IN (2, 4)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			
			-- @eventCategory = 255 - PL Events
			------------------------------------------
			-- 32 File upload failed: CRC Error Failed (PL)
			-- 33 File upload failed: Timeout	Failed (PL)
			-- 34 File upload failed: Aborted	Failed (PL)
			-- 35 File upload failed: Unknown	Failed (PL)
			IF @eventCategory = 255 AND @eventCode IN (32,33,34,35,36)
				UPDATE dbo.VehicleDownload SET
					Status = 3	-- Failed
				WHERE ID = @vehicleDownloadID

			-- 36 File transferred OK	Transferred
			IF @eventCategory = 255 AND @eventCode IN (36)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progress
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1000 - File Loader Events
			------------------------------------------
			-- 0 File Imported Successfully (User Requested Download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NOT NULL
				UPDATE dbo.VehicleDownload SET
					Status = 0	-- Completed
					, DateTimeCompleted = SYSDATETIME()
				WHERE ID = @vehicleDownloadID

			-- 0 File Imported Successfully (Scheduled download)
			IF @eventCategory = 1000 AND @eventCode IN (0) AND @vehicleDownloadID IS NULL
			BEGIN
				PRINT 'inserted @vehicleDownloadID:'
			
				SET @vehicleDownloadStatus = -1 --current status -1 as not existing

				INSERT INTO dbo.VehicleDownload
					(VehicleID
					,Status
					,DateTimeRequested
					,DateTimeCompleted
					,IsManual
					,FileType)
				SELECT
					VehicleID			= @vehicleID
					,Status				= 0
					,DateTimeRequested	= SYSDATETIME()
					,DateTimeCompleted	= SYSDATETIME()
					,IsManual			= 0
					,FileType			= 6

				SET @vehicleDownloadID = SCOPE_IDENTITY()
				
				
				PRINT @vehicleDownloadID
				
			END
			
			-- 1 File Imported Failed
			IF @eventCategory = 1000 AND @eventCode IN (1)
				UPDATE dbo.VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID

			-- @eventCategory = 1001 - Scheduling Service Events
			------------------------------------------
			-- 0 Flag updated in SQLite DB successfully
			IF @eventCategory = 1001 AND @eventCode IN (0)
				UPDATE dbo.VehicleDownload SET
					Status = 2 -- In Progressd
				WHERE ID = @vehicleDownloadID

			-- 1 Flag updated in SQLite DB failed
			IF @eventCategory = 1001 AND @eventCode IN (1)
				UPDATE dbo.VehicleDownload SET
					Status = 3 -- Failed
				WHERE ID = @vehicleDownloadID
		END

		
		-- Flag file as processed
		UPDATE dbo.VehicleDownloadProgress SET 
			IsProcessed = 1
			, VehicleDownloadID = @vehicleDownloadID
			, StatusUpdated = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN 1
				ELSE 0
			END
			, StatusUpdatedDatetime = CASE
				WHEN @vehicleDownloadStatus <> (SELECT Status FROM VehicleDownload WHERE ID = @vehicleDownloadID) THEN SYSDATETIME()
				ELSE NULL
			END
		WHERE ID = @vehicleDownloadProgressID
		
	END
END
GO
