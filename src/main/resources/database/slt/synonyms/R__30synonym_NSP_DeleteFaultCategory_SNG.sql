SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_DeleteFaultCategory_SNG
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_SNG varchar(50) = '${DBNAME_SNG}'

DECLARE @NSP_DeleteFaultCategory_SNG varchar(50)
SET @NSP_DeleteFaultCategory_SNG = 'NSP_DeleteFaultCategory_SNG'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_DeleteFaultCategory_SNG + ''')
DROP SYNONYM [dbo].[NSP_DeleteFaultCategory_SNG]

CREATE SYNONYM [dbo].[NSP_DeleteFaultCategory_SNG] FOR ' + @DBNAME_SNG + '.[dbo].[NSP_DeleteFaultCategory]'
EXEC( @query)

COMMIT

GO