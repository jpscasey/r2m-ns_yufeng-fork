SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_DeleteFaultCategory_VIRM
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_VIRM varchar(50) = '${DBNAME_VIRM}'

DECLARE @NSP_DeleteFaultCategory_VIRM varchar(50)
SET @NSP_DeleteFaultCategory_VIRM = 'NSP_DeleteFaultCategory_VIRM'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_DeleteFaultCategory_VIRM + ''')
DROP SYNONYM [dbo].[NSP_DeleteFaultCategory_VIRM]

CREATE SYNONYM [dbo].[NSP_DeleteFaultCategory_VIRM] FOR ' + @DBNAME_VIRM + '.[dbo].[NSP_DeleteFaultCategory]'
EXEC( @query)

COMMIT

GO