SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_InsertFaultCategory_SNG
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_SNG varchar(50) = '${DBNAME_SNG}'

DECLARE @NSP_InsertFaultCategory_SNG varchar(50)
SET @NSP_InsertFaultCategory_SNG = 'NSP_InsertFaultCategory_SNG'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_InsertFaultCategory_SNG + ''')
DROP SYNONYM [dbo].[NSP_InsertFaultCategory_SNG]

CREATE SYNONYM [dbo].[NSP_InsertFaultCategory_SNG] FOR ' + @DBNAME_SNG + '.[dbo].[NSP_InsertFaultCategory]'
EXEC( @query)

COMMIT

GO