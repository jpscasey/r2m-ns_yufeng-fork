SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_InsertFaultCategory_VIRM
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_VIRM varchar(50) = '${DBNAME_VIRM}'

DECLARE @NSP_InsertFaultCategory_VIRM varchar(50)
SET @NSP_InsertFaultCategory_VIRM = 'NSP_InsertFaultCategory_VIRM'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_InsertFaultCategory_VIRM + ''')
DROP SYNONYM [dbo].[NSP_InsertFaultCategory_VIRM]

CREATE SYNONYM [dbo].[NSP_InsertFaultCategory_VIRM] FOR ' + @DBNAME_VIRM + '.[dbo].[NSP_InsertFaultCategory]'
EXEC( @query)

COMMIT

GO