SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_UpdateFaultCategory_SNG
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_SNG varchar(50) = '${DBNAME_SNG}'

DECLARE @NSP_UpdateFaultCategory_SNG varchar(50)
SET @NSP_UpdateFaultCategory_SNG = 'NSP_UpdateFaultCategory_SNG'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_UpdateFaultCategory_SNG + ''')
DROP SYNONYM [dbo].[NSP_UpdateFaultCategory_SNG]

CREATE SYNONYM [dbo].[NSP_UpdateFaultCategory_SNG] FOR ' + @DBNAME_SNG + '.[dbo].[NSP_UpdateFaultCategory]'
EXEC( @query)

COMMIT

GO