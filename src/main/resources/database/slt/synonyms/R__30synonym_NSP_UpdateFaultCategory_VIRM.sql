SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------
-- Drop and create synonym for NSP_UpdateFaultCategory_VIRM
----------------------------------------------------------------------

BEGIN TRAN 

DECLARE @DBNAME_VIRM varchar(50) = '${DBNAME_VIRM}'

DECLARE @NSP_UpdateFaultCategory_VIRM varchar(50)
SET @NSP_UpdateFaultCategory_VIRM = 'NSP_UpdateFaultCategory_VIRM'

DECLARE @query varchar(max)

SET @query = '
IF EXISTS (SELECT * FROM sys.synonyms WHERE name = ''' + @NSP_UpdateFaultCategory_VIRM + ''')
DROP SYNONYM [dbo].[NSP_UpdateFaultCategory_VIRM]

CREATE SYNONYM [dbo].[NSP_UpdateFaultCategory_VIRM] FOR ' + @DBNAME_VIRM + '.[dbo].[NSP_UpdateFaultCategory]'
EXEC( @query)

COMMIT

GO