SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_IX_Fault', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_IX_Fault' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_IX_Fault AS SELECT 1 AS X;')
END
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_CL_U_VW_IX_Fault_ID')
    DROP INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_CreateTime')
    DROP INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_FaultType_CreateTime')
    DROP INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged')
    DROP INDEX [IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged] ON [dbo].[VW_IX_Fault] 
GO

IF EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_EndTime')
    DROP INDEX [IX_VW_IX_Fault_IsCurrent_EndTime] ON [dbo].[VW_IX_Fault] 
GO


ALTER VIEW [dbo].[VW_IX_Fault] WITH SCHEMABINDING
	AS
		SELECT 
		f.ID    
		, f.CreateTime    
		, f.HeadCode  
		, f.SetCode   
		, fm.FaultCode 
		, f.LocationID 
		, f.IsCurrent 
		, f.EndTime   
		, f.RecoveryID    
		, f.Latitude  
		, f.Longitude 
		, f.FaultMetaID   
		, f.FaultUnitID
		, FaultUnitNumber		= uf.UnitNumber 
		, f.PrimaryUnitID
		, f.SecondaryUnitID
		, f.TertiaryUnitID
		, f.IsDelayed 
		, fm.Description   
		, fm.Summary   
		, fm.CategoryID
		, fc.Category
		, fc.ReportingOnly
		, fm.FaultTypeID
		, FaultType			= ft.Name
		, FaultTypeColor	= ft.DisplayColor
		, f.IsAcknowledged	
		, f.AcknowledgedTime
		, ft.Priority
		, f.RecoveryStatus
		, FleetCode			= fl.Code
		, fm.AdditionalInfo
		, fm.HasRecovery
		, f.RowVersion
		, f.FaultGroupID
		, f.IsGroupLead
		, f.RuleName
		, f.ServiceRequestID
		, f.CountSinceLastMaint
		, uf.UnitStatus
		, f.UnitStatus AS UnitStatusFault --UnitStatus at time of the fault
		FROM dbo.Fault f
		INNER JOIN dbo.FaultMeta fm		ON f.FaultMetaID = fm.ID
		INNER JOIN dbo.FaultType ft		ON ft.ID = fm.FaultTypeID
		INNER JOIN dbo.FaultCategory fc	ON fm.CategoryID = fc.ID
		INNER JOIN dbo.Unit uf			ON uf.ID = f.FaultUnitID
		INNER JOIN dbo.Fleet fl			ON fl.ID = uf.FleetID
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_CL_U_VW_IX_Fault_ID]    Script Date: 06/04/2018 10:41:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_CL_U_VW_IX_Fault_ID] ON [dbo].[VW_IX_Fault]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Fault_CreateTime]    Script Date: 06/04/2018 10:41:47 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [IX_VW_IX_Fault_FaultType_CreateTime]    Script Date: 06/04/2018 10:41:47 ******/
CREATE NONCLUSTERED INDEX [IX_VW_IX_Fault_FaultType_CreateTime] ON [dbo].[VW_IX_Fault]
(
	[FaultCode] ASC,
	[CreateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged')
    CREATE NONCLUSTERED INDEX IX_VW_IX_Fault_IsCurrent_ReportingOnly_IsAcknowledged
    ON [dbo].[VW_IX_Fault] (IsCurrent, [ReportingOnly], [IsAcknowledged])


IF NOT EXISTS (SELECT * FROM SYS.SYSINDEXES WHERE NAME='IX_VW_IX_Fault_IsCurrent_EndTime')
    CREATE NONCLUSTERED INDEX IX_VW_IX_Fault_IsCurrent_EndTime
    ON [dbo].[VW_IX_Fault] (IsCurrent, [EndTime] DESC)