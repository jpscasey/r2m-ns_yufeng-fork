SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_IX_FleetStatus', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_IX_FleetStatus' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_IX_FleetStatus AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_IX_FleetStatus] WITH SCHEMABINDING 
AS
--
-- Please use VW_FleetStatus View
--

	SELECT
		fs.ID AS ID 
		,Unit.ID AS UnitID
		,UnitNumber
		,UnitType
		,UnitPosition
		,UnitOrientation
		,FleetFormationID
		,Diagram
		,Headcode
		,Loading
		,'' AS VehicleList
		,1 AS VehicleTypeID
		,LocationIDHeadcodeStart
		,LocationIDHeadcodeEnd
        ,Fleet.Code AS FleetCode
		, Unit.UnitStatus AS UnitStatus
		,ActiveCab
	FROM dbo.FleetStatus fs
	INNER JOIN dbo.Unit ON Unit.ID = fs.UnitID
    INNER JOIN dbo.Fleet ON Unit.FleetID = Fleet.ID
	WHERE ValidTo IS NULL
GO
