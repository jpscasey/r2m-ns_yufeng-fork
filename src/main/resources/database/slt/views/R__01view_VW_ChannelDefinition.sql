SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_ChannelDefinition', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_ChannelDefinition' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_ChannelDefinition AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_ChannelDefinition]
AS
    SELECT     
        c.ID
        , c.VehicleID
        , c.EngColumnNo AS ColumnID
        , c.ChannelGroupID
        , c.Name AS Name
        , c.Description AS Description
        , c.ChannelGroupOrder
        , ct.Name AS Type
        , c.UOM
        , c.HardwareType
        , c.StorageMethod
        , c.Ref
        , c.MinValue
        , c.MaxValue
        , c.Comment
        , c.VisibleOnFaultOnly
        , c.IsAlwaysDisplayed
        , c.IsLookup
        , c.DefaultValue
        , c.IsDisplayedAsDifference
        , rc.Name AS RelatedChannelName
        , rc.ID AS RelatedChannelID
		, c.DataType
    FROM dbo.Channel AS c 
    INNER JOIN dbo.ChannelType AS ct ON c.TypeID = ct.ID
    LEFT JOIN RelatedChannel rc ON c.RelatedChannelID = rc.ID
GO
