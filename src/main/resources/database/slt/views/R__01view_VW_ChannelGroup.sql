SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_ChannelGroup', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_ChannelGroup' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_ChannelGroup AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_ChannelGroup]
AS
SELECT 
	EngColumnNo
	, Channel.Name
	, ChannelGroupName
	, ChannelType.Name AS ChannelType
	, 1 as VehicleTypeID
	, ChannelGroupOrder
FROM Channel 
INNER JOIN ChannelGroup ON ChannelGroupID = ChannelGroup.ID
INNER JOIN ChannelType ON TypeID = ChannelType.ID
GO
