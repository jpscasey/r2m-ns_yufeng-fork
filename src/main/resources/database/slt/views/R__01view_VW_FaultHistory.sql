SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_FaultHistory', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_FaultHistory' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_FaultHistory AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_FaultHistory]
AS

	SELECT 
		f.ID
		, f.FaultMetaID
		, fv.UnitNumber			AS UnitNumber
		, f.CreateTime			AS CreateTime
		, f.IsCurrent			AS IsCurrent
		, f.RecoveryID			AS RecoveryID
		, fv.VehicleNumber		AS FaultVehicle
		, f.EndTime				AS EndTime
		, f.Headcode			AS HeadCode
		, l.Tiploc				AS LocationCode
		, f.Latitude			AS Latitude
		, f.Longitude			AS Longitude
		, f.FaultCode			AS FaultCode
		, f.FaultType
		, f.FaultTypeColor
		, f.Description			AS FaultDescription
		, f.Summary				AS FaultSummary
		, f.Category			AS FaultCategory
		, fv.FleetID			AS FleetID
		, fv.FleetCode			AS FleetCode
		, f.AdditionalInfo		AS AdditionalInfo
		, f.HasRecovery			AS HasRecovery
		, f.RowVersion          AS RowVersion

	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN VW_Vehicle fv ON fv.UnitID = f.FaultUnitID
	LEFT JOIN Location l ON l.ID = LocationID
GO
