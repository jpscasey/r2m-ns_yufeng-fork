SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_TrainPassageValid', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_TrainPassageValid' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_TrainPassageValid AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_TrainPassageValid]
AS
	SELECT
		ID
		,JourneyID
		,Headcode
		,Setcode
		,StartDatetime
		,EndDatetime
		,AnalyseDatetime
		,EndAVehicleID
		,EndBVehicleID
		,Loading
--		,ToBeProcessed
--		,TimestampAnalysisStarted
--		,TimestampAnalysisCompleted
		,IsIncludedInAnalysis
	FROM dbo.TrainPassage 
	WHERE IsIncludedInAnalysis = 1
GO
