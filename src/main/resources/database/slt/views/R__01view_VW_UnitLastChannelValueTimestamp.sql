SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_UnitLastChannelValueTimestamp', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_UnitLastChannelValueTimestamp' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_UnitLastChannelValueTimestamp AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_UnitLastChannelValueTimestamp]
AS 
    SELECT 
        UnitID      = Unit.ID
        , UnitNumber
        , LastTimeStamp = (
            SELECT TOP 1 TimeStamp
            FROM ChannelValue
            WHERE UnitID = Unit.ID  --TimeStamp > DATEADD(d, -7, SYSDATETIME()) AND UnitID = Unit.ID 
            ORDER BY TimeStamp DESC
        )
    FROM Unit
GO
