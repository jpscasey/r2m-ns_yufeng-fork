SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_CompareJorney', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_CompareJorney' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_CompareJorney () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_CompareJorney]
(
	@JourneyID1 int
	, @JourneyID2 int
)
RETURNS bit
AS
BEGIN

	DECLARE @isIdentical bit
	
	IF NOT EXISTS 
	(
		SELECT 1 
		FROM
		(
			SELECT 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
			FROM [SectionPoint]
			WHERE [JourneyID] = @JourneyID1
			
			UNION ALL
			
			SELECT 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
			FROM [SectionPoint]
			WHERE [JourneyID] = @JourneyID2
		) tmp
		GROUP BY 
				[ID]
				,[LocationID]
				,[ScheduledArrival]
				,[ScheduledDeparture]
				,[ScheduledPass]
				,[PublicArrival]
				,[PublicDeparture]
				,[RecoveryTime]
				,[SectionPointType]
		HAVING COUNT(*) = 1
	)
	
		SET @isIdentical = 1
	ELSE
		SET @isIdentical = 0
	
	RETURN @isIdentical
END
GO
