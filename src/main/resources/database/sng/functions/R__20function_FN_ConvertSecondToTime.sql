SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_ConvertSecondToTime', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_ConvertSecondToTime' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_ConvertSecondToTime () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_ConvertSecondToTime]
(
	@NumberOfSeconds int
)
RETURNS varchar(7)
AS
BEGIN
	DECLARE @string char(7)

	IF @NumberOfSeconds BETWEEN 0 AND 24*60*60
		SET @string =
			CASE 
				WHEN @NumberOfSeconds < 600
					THEN '0' + CAST((@NumberOfSeconds - (@NumberOfSeconds % 60)) / 60 AS varchar(3))
				ELSE CAST((@NumberOfSeconds - (@NumberOfSeconds % 60)) / 60 AS varchar(3))
			END
			+ ':'
			+ RIGHT(CAST(100 + @NumberOfSeconds % 60 AS CHAR(3)), 2)
	ELSE
		SET @string = '-'
	-- Return the result of the function
	RETURN @string

END
GO
