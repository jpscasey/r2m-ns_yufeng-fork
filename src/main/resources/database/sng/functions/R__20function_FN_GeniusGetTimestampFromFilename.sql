SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GeniusGetTimestampFromFilename', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GeniusGetTimestampFromFilename' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GeniusGetTimestampFromFilename () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_GeniusGetTimestampFromFilename]
(
	@Filename varchar(100)
)
RETURNS datetime2(0)
AS
BEGIN

	--FileFormat:
	-- DateProcessed_TimeProcessed_Unit_DateGenerated_TimeGenereated
	
	-- Find datetime file was generated
	DECLARE @separator char(1) = '_';
	
	DECLARE @p1 tinyint = CHARINDEX(@separator, @Filename, 0);
	DECLARE @p2 tinyint = CHARINDEX(@separator, @Filename, @p1 + 1);
	DECLARE @p3 tinyint = CHARINDEX(@separator, @Filename, @p2 + 1);
	
	DECLARE @dateStringDay varchar(10)	= SUBSTRING(@Filename, @p3 + 1, 2);
	DECLARE @dateStringMonth varchar(10)	= SUBSTRING(@Filename, @p3 + 3, 2);
	DECLARE @dateStringYear varchar(10)	= SUBSTRING(@Filename, @p3 + 5, 4);
	DECLARE @timeStringHour varchar(10)	= SUBSTRING(@Filename, @p3 + 10, 2);
	DECLARE @timeStringMinute varchar(10)	= SUBSTRING(@Filename, @p3 + 12, 2);
	
	DECLARE @date datetime2(0) = CONVERT(datetime2(0), @dateStringYear + '-' + @dateStringMonth + '-' + @dateStringDay + ' ' + @timeStringHour + ':' + @timeStringMinute)


	RETURN @date;
END
GO
