SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GeniusGetUnitFromFilename', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GeniusGetUnitFromFilename' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GeniusGetUnitFromFilename () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_GeniusGetUnitFromFilename]
(
	@Filename varchar(100)
)
RETURNS varchar(100)
AS
BEGIN

	--FileFormat:
	-- DateProcessed_TimeProcessed_Unit_DateGenerated_TimeGenereated
	
	-- Find datetime file was generated
	DECLARE @separator char(1) = '_';
	
	DECLARE @p1 tinyint = CHARINDEX(@separator, @Filename, 0);
	DECLARE @p2 tinyint = CHARINDEX(@separator, @Filename, @p1 + 1);
	DECLARE @p3 tinyint = CHARINDEX(@separator, @Filename, @p2 + 1);

	DECLARE @unit varchar(100) = SUBSTRING(@Filename, @p2 + 1, @p3 - @p2 - 1)

	RETURN @unit;
END
GO
