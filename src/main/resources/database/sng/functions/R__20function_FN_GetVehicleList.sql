SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GetVehicleList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GetVehicleList' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GetVehicleList () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION  [dbo].[FN_GetVehicleList]
(
	@FleetStatusID varchar(100)
)
RETURNS varchar(100)
AS
BEGIN

	DECLARE @vehicleList varchar(150)

	SET @vehicleList = ''

		SELECT @vehicleList = @vehicleList + ', ' + VehicleNumber 
		FROM dbo.FleetStatusVehicle
		INNER JOIN Vehicle ON Vehicle.ID = VehicleID
		WHERE FleetStatusID = @FleetStatusID
		ORDER BY VehiclePosition

	IF LEN(@vehicleList) > 2
		SET @vehicleList = RIGHT(@vehicleList, LEN(@vehicleList)-2) 

	RETURN @vehicleList 

END
GO
