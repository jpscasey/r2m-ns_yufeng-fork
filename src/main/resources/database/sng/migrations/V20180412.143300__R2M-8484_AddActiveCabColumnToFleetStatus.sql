SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update FleetStatus table
------------------------------------------------------------

RAISERROR ('-- add ActiveCab column in FleetStatus table', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'FleetStatus' AND COLUMN_NAME='ActiveCab')
BEGIN
    ALTER TABLE [dbo].[FleetStatus]
      ADD ActiveCab varchar(12) NULL
END
GO