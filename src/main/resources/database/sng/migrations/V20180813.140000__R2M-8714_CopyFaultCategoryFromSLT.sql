SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------
-- Populate FaultCategory table from SLT fleet database
----------------------------------------------------------------

RAISERROR ('-- Update FaultCategory table for VIRM', 0, 1) WITH NOWAIT
GO

BEGIN TRAN 

DECLARE @SLT_DBNAME varchar(100) = '${DBNAME_SLT}'
DECLARE @query varchar(max)

ALTER TABLE dbo.FaultMeta NOCHECK CONSTRAINT ALL  

ALTER TABLE dbo.FaultCategory NOCHECK CONSTRAINT ALL  

DELETE FROM dbo.FaultCategory

SET IDENTITY_INSERT FaultCategory ON
SET @query = '
    INSERT INTO dbo.FaultCategory (ID,Category,ReportingOnly)
    SELECT ID,Category,ReportingOnly
    FROM ' + @SLT_DBNAME + '.dbo.FaultCategory'
EXEC( @query)

SET IDENTITY_INSERT FaultCategory OFF

ALTER TABLE dbo.FaultMeta WITH CHECK CHECK CONSTRAINT ALL;

ALTER TABLE dbo.FaultCategory WITH CHECK CHECK CONSTRAINT ALL;

COMMIT

GO