SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------------------
-- R2M-9109 - Implement conversions for channels that is decoded by using mt mask
----------------------------------------------------------------------------------

RAISERROR ('-- Updating Channel Mask for MT', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS ic WHERE ic.TABLE_NAME = 'ChannelMask' AND ic.COLUMN_NAME = 'Conversion' )
BEGIN
	ALTER TABLE dbo.ChannelMask ADD Conversion decimal(14,12) ;
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS ic WHERE ic.TABLE_NAME = 'ChannelMask' AND ic.COLUMN_NAME = 'DataType' )
BEGIN
	ALTER TABLE dbo.ChannelMask ADD DataType varchar(20) null;
END
GO