SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
--  SNG unit 2700 does not show the drawing on Unit Summary due to missing Vehicle records.
------------------------------------------------------------

RAISERROR ('-- SNG unit 2700 does not show the drawing on Unit Summary due to missing Vehicle records.', 0, 1) WITH NOWAIT
GO

IF exists (select ID from [dbo].[unit] where UnitNumber = '2700')
BEGIN
SET IDENTITY_INSERT [dbo].[Vehicle] ON

INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'2700A1', N'A1', 1, N'1', (select ID from [dbo].[unit] where UnitNumber = '2700'), N'cab 1', CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'2700A2', N'A2', 4, N'1', (select ID from [dbo].[unit] where UnitNumber = '2700'), N'cab 2', CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'2700B', N'B', 2, N'0', (select ID from [dbo].[unit] where UnitNumber = '2700'), N'', CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'2700C1', N'C1', 3, N'0', (select ID from [dbo].[unit] where UnitNumber = '2700'), N'', CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), CAST(N'2018-01-22T16:33:06.1000000' AS DateTime2), 1, 1, NULL, NULL, NULL, NULL)

SET IDENTITY_INSERT [dbo].[Vehicle] OFF
END

GO