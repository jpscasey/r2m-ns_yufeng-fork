SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_CifImportGetNextFilename', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_CifImportGetNextFilename' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_CifImportGetNextFilename AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_CifImportGetNextFilename]
(
	@FileNameInitial varchar(20)
	, @FileNameNext varchar(20) OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @fileLast varchar(20)

	-- get latest successfully processed file (without .cif extention)
	SET @fileLast = (
		SELECT TOP 1 LEFT(OriginalFileName, CHARINDEX('.', OriginalFileName) - 1) 
		FROM LoadCifFile
		WHERE Status = 'Processed'
		ORDER BY FileID DESC)

	IF @FileNameInitial IS NOT NULL
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			LEFT(@fileLast, LEN(@fileLast) -1 )
			+ CASE RIGHT(@fileLast, 1)
				WHEN 'Z' THEN 'A'
				ELSE CHAR(ASCII(RIGHT(@fileLast, 1)) + 1)
			END
	END
	ELSE
	BEGIN
		-- generate name for next file 
		SET @FileNameNext = 
			CASE 
				WHEN CHARINDEX('.', @FileNameInitial) > 1 
					THEN LEFT(@FileNameInitial, CHARINDEX('.', @FileNameInitial) - 1) 
				ELSE @FileNameInitial
			END
	END

	-- convert generated filename to upper-cases
	SET @FileNameNext = UPPER(@FileNameNext)
	
END -- stored procedure
GO
