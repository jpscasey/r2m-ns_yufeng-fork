SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_CifImportParseData', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_CifImportParseData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_CifImportParseData AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_CifImportParseData]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @sqlQuery varchar(max)
	--------------
	-- HD Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineHD') IS NOT NULL
		DROP TABLE ##CifLineHD

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'HD' AND Name <> 'Spare'
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineHD
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''HD''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BS Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBS') IS NOT NULL
		DROP TABLE ##CifLineBS

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BS' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBS
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BS''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- BX Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineBX') IS NOT NULL
		DROP TABLE ##CifLineBX

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'BX' AND Name NOT IN('Spare', 'Reserved field')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineBX
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''BX''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LO Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLO') IS NOT NULL
		DROP TABLE ##CifLineLO

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LO' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLO
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LO''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- LI Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLI') IS NOT NULL
		DROP TABLE ##CifLineLI

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LI' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLI
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LI''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--------------
	-- CR Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineCR') IS NOT NULL
		DROP TABLE ##CifLineCR

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'CR' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineCR
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''CR''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)


	--------------
	-- LT Lines
	--------------

	IF OBJECT_ID('tempdb..##CifLineLT') IS NOT NULL
		DROP TABLE ##CifLineLT

	SET @sqlQuery = ''

	SELECT 
		@sqlQuery = ISNULL(@sqlQuery, '') + '
		, SUBSTRING(Data, ' + (SELECT CAST(ISNULL(SUM(Size),0)+1 AS varchar) FROM LoadCifFileFormat cf2 WHERE cf1.FieldType = cf2.FieldType AND cf2.FieldOrder < cf1.FieldOrder) + ', ' + CAST(Size AS varchar) + ') AS [' + RTRIM(Header) + ']'
	FROM [LoadCifFileFormat] cf1
	WHERE FieldType = 'LT' AND Name NOT IN('Spare')
	ORDER BY cf1.FieldOrder

	SET @sqlQuery = 'SELECT 
		ID, FileLineNumber
		' + @sqlQuery + '
		, Data 
	INTO ##CifLineLT
	FROM LoadCifFileData 
	WHERE LEFT(Data, 2) = ''LT''
		AND FileID = ' + CAST(@FileID AS varchar) + '
	ORDER BY FileLineNumber'

	--PRINT @sqlQuery
	EXEC (@sqlQuery)

	--Add indexes
	CREATE NONCLUSTERED INDEX IX_LI ON [dbo].[##CifLineLI] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LO ON [dbo].[##CifLineLO] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_LT ON [dbo].[##CifLineLT] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BS ON [dbo].[##CifLineBS] ([FileLineNumber])
	CREATE NONCLUSTERED INDEX IX_BX ON [dbo].[##CifLineBX] ([FileLineNumber])

END
GO
