SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DataArchiving', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DataArchiving' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DataArchiving AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DataArchiving]
(
	@StopDate as date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @outputChannelValue TABLE(
		ID bigint);
	
	DECLARE @outputFault TABLE(
		ID bigint);

	
	DECLARE 
		 @batch int = 10000
		,@loopmax int = 10000
		,@archived int = 0
		,@stop datetime2 
		,@tempStop datetime2
		,@start datetime2 
		,@id int
		,@d datetime2
		,@t datetime2 = SYSDATETIME()
		,@cnt int 
		,@loop int = 0
		,@unitid int = 0
		,@msg varchar(1000)
		,@StartTime datetime2 = SYSDATETIME();


	WHILE 1 = 1
	BEGIN

		SET @start = (SELECT MIN(timestamp)	FROM dbo.ChannelValue)

		SELECT
			@stop = @StopDate 
			, @t = SYSDATETIME()
			, @archived = 0
			, @tempStop = DATEADD(N, 5, @start)

		IF @tempStop > @stop
		BEGIN

			INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , SYSDATETIME(),'NSP_DataArchiving',0,@loop,'Data Archiving Finished')
			BREAK;

		END
	


	--	SET @msg = 'Start: ' + convert(varchar, @start, 121)+ '; Stop: ' +convert(varchar, @stop, 121) + '; TempStop: ' + convert(varchar, @tempStop, 121)

	--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
	--		VALUES(@StartTime , SYSDATETIME(),'NSP_DataArchiving',0,@loop,@msg)
	
		

		---------------------------------------------------------------------------
		-- Fault and FaultChannelValue tables
		---------------------------------------------------------------------------
		BEGIN TRAN

		BEGIN TRY

			SET @archived = 0
			DELETE @outputFault

			INSERT NedTrain_Spectrum_Archive.dbo.Fault_Archive
			OUTPUT INSERTED.ID
				INTO @outputFault
			SELECT TOP (@batch / 10) * FROM dbo.Fault 
			WHERE CreateTime <= DATEADD(n, 5, @tempStop)
				AND CreateTime <= @stop
			ORDER BY 
				CreateTime ASC
				, ID ASC;
		
			SET @archived = @@ROWCOUNT;

			IF @archived > 0
			BEGIN

				INSERT NedTrain_Spectrum_Archive.dbo.FaultChannelValue_Archive
				SELECT *
				FROM dbo.FaultChannelValue fcv 
				WHERE FaultID IN (SELECT ID FROM @outputFault)

				DELETE dbo.FaultChannelValue
				WHERE FaultID IN (SELECT ID FROM @outputFault)
			
				DELETE dbo.Fault
				WHERE ID IN (SELECT ID FROM @outputFault)
					AND CreateTime <= DATEADD(n, 5, @tempStop)
			
			END
		
			COMMIT;
		
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK;
			EXEC dbo.nsp_RethrowError;
			RETURN 1
		END CATCH

		INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , SYSDATETIME(),'NSP_DataArchiving',@archived,@loop,'Faults and FaultChannelValues: ' + convert(varchar(10), @archived) 
			+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, SYSDATETIME())) + ' ms')
	



		---------------------------------------------------------------------------
		-- ChannelValue table
		---------------------------------------------------------------------------
		BEGIN TRAN

		BEGIN TRY
	
			DELETE @outputChannelValue
		
			INSERT NedTrain_Spectrum_Archive.dbo.ChannelValue_Archive
			OUTPUT INSERTED.ID
				INTO @outputChannelValue
			SELECT TOP(@batch) * 
			FROM dbo.ChannelValue 
			WHERE TimeStamp <= @tempStop
			ORDER BY TimeStamp asc;
		
			DELETE
			FROM dbo.ChannelValue
			WHERE ID IN (SELECT ID FROM @outputChannelValue)
		
			SELECT @archived = @@ROWCOUNT;
		
			COMMIT;
		
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK;
			EXEC dbo.nsp_RethrowError;
			RETURN 1
		END CATCH

			INSERT INTO dbo.ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
			VALUES(@StartTime , SYSDATETIME(),'NSP_DataArchiving',@archived,@loop,'ChannelValue:				 ' + convert(varchar(10), @archived)
			+ ' rows, Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, SYSDATETIME())) + ' ms')


		WHILE @@TRANCOUNT > 0
			ROLLBACK;


	--	SET @msg = 'Iteration: ' + convert(varchar, @loop)
	--		+ ';		Duration: ' + convert(varchar(10), DATEDIFF(ms, @t, SYSDATETIME())) + ' ms'
	--		+ ';		Open Transactions: ' + convert(varchar(10), @@trancount);

	--	INSERT INTO ArchivingLog(StartTime,FinishTime,TableName,Records,Runs,ErrorMsg) 
	--		VALUES(@StartTime , SYSDATETIME(),'NSP_DataArchiving',0,@loop,@msg)
		------------------------------------------------------------------------------
		SET @loop = @loop + 1
		IF @loop > @loopmax
			BREAK

		WAITFOR DELAY '00:00:02'

	END

END
GO
