SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DeleteFaultMeta', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DeleteFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DeleteFaultMeta AS BEGIN RETURN(1) END;')
END
GO


ALTER PROCEDURE [dbo].[NSP_DeleteFaultMeta](
    @ID int
)
AS
BEGIN

    SET NOCOUNT ON;
    
    BEGIN TRAN
    BEGIN TRY
        
        DELETE FROM dbo.FaultMetaExtraField 
        WHERE FaultMetaId = @ID
        
        DELETE FROM dbo.FaultMetaExtraField 
        WHERE FaultMetaId IN (SELECT ID FROM dbo.FaultMeta WHERE ParentID = @ID)
    
        DELETE dbo.FaultMeta 
        WHERE ID = @ID
        
        DELETE dbo.FaultMeta 
        WHERE ParentID = @ID
        
        COMMIT TRAN
                
    END TRY
    BEGIN CATCH
        
        IF ERROR_NUMBER() = 547 -- FOREIGN KEY ERROR
            BEGIN
                RAISERROR ('ERR_FAULT_EXIST', 14, 1) WITH NOWAIT
            END
        ELSE
            BEGIN
                EXEC NSP_RethrowError;
            END
            
        ROLLBACK TRAN;
        
    END CATCH
    
END


GO
