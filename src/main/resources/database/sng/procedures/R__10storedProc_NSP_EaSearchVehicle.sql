SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_EaSearchVehicle', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_EaSearchVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_EaSearchVehicle AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_EaSearchVehicle]
(
	@VehicleType varchar(50)
	, @VehicleString varchar(50)
	, @UnitID varchar(max) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleTypeTable TABLE (VehicleType varchar(50))
	INSERT INTO @VehicleTypeTable
	SELECT * from [dbo].SplitStrings_CTE(@VehicleType,',')

	DECLARE @UnitIdTable TABLE (UnitID int)
	INSERT INTO @UnitIdTable
	SELECT convert(int, item) from [dbo].SplitStrings_CTE(@UnitID,',')

	SELECT
		FleetCode = f.Code
		, VehicleID	= v.ID
		, VehicleNumber = v.VehicleNumber
		, VehicleType = v.Type
	FROM Vehicle v
	JOIN Unit u ON u.ID = v.UnitID
	JOIN Fleet f ON f.ID = u.FleetID
	WHERE VehicleNumber LIKE '%' + @VehicleString + '%'
		AND (@VehicleType IS NULL OR v.Type IN (Select VehicleType from @VehicleTypeTable))
		AND (@UnitID IS NULL OR v.UnitID IN (Select UnitID from @UnitIdTable))
END
GO
