SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaGetLocationList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaGetLocationList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaGetLocationList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaGetLocationList]
(
	@LocationString varchar(50)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		LocationID		= ID
		, Tiploc
		, LocationName
		, LocationCode
	FROM dbo.Location
	WHERE TIPLOC LIKE '%' + @LocationString + '%'
		OR LocationCode LIKE '%' + @LocationString + '%'
		OR LocationName LIKE '%' + @LocationString + '%'
	ORDER BY Tiploc
	
END
GO
