SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaReportFaultByLocation', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaReportFaultByLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaReportFaultByLocation AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByLocation]
(
	@DateFrom datetime2(3)
	, @DateTo datetime2(3)
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT ID
	FROM dbo.FaultMeta
	WHERE FaultTypeID = ISNULL(@FaultTypeID, FaultTypeID)
		AND ID = ISNULL(@FaultMetaID, ID)

	SELECT
		LocationID				= ISNULL(f.LocationID, -1) 
		, LocationName			= ISNULL(l.LocationName, 'N/A')
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, f.CreateTime, ISNULL(f.EndTime, f.CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
		, Lat					= ISNULL(Lat,0)
		, Lng					= ISNULL(Lng,0)
		, DisplayColor			= FaultTypeColor
		, Priority
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.Vehicle v ON v.UnitID = FaultUnitID
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (@LocationID IS NULL OR LocationID = @LocationID)
	GROUP BY 
		f.LocationID
		, l.LocationName
		, f.FaultTypeColor
		, f.Priority
		, l.Lat
		, l.Lng
	ORDER BY EventCount DESC

END
GO
