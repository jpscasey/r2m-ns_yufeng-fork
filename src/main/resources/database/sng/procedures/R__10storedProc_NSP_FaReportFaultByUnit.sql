SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaReportFaultByUnit', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaReportFaultByUnit' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaReportFaultByUnit AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaReportFaultByUnit]
(
	@DateFrom datetime2(3)
	, @DateTo datetime2(3)
	, @FaultTypeID int
	, @FaultMetaID int
	, @VehicleType varchar(10)
	, @VehicleID int
	, @LocationID int
	, @UnitID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @VehicleList TABLE (ID int)
	DECLARE @FaultMetaList TABLE (ID int)

	INSERT INTO @VehicleList
	SELECT ID 
	FROM dbo.Vehicle 
	WHERE (@VehicleID IS NULL OR ID = @VehicleID)
		AND 
		(@VehicleType IS NULL OR Type = @VehicleType)
		AND
		(@UnitID IS NULL OR UnitID = @UnitID)

	INSERT INTO @FaultMetaList
	SELECT fm.ID
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND	fm.ID = ISNULL(@FaultMetaID, fm.ID)	

	SELECT 
		UnitID					= v.UnitID 
		, UnitNumber			= v.UnitNumber
		, FaultCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 1 THEN 1 END), 0)
		, WarningCount			= ISNULL(SUM(CASE WHEN f.FaultTypeID = 2 THEN 1 END), 0)
		, TotalFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime)))
		, AverageFaultDuration	= SUM(DATEDIFF(s, CreateTime, ISNULL(EndTime, CreateTime))) / COUNT(*)
		, EventCount			= COUNT(*)
	FROM dbo.VW_IX_Fault f (NOEXPAND)
	INNER JOIN dbo.VW_Vehicle v ON v.UnitID = f.FaultUnitID
	WHERE v.ID IN (SELECT ID FROM @VehicleList)
		AND f.FaultMetaID IN (SELECT ID FROM @FaultMetaList)
		AND f.CreateTime BETWEEN @DateFrom AND @DateTo
		AND (f.LocationID = @LocationID OR @LocationID IS NULL)
	GROUP BY 
		v.UnitID 
		, v.UnitNumber	
	ORDER BY EventCount DESC
		

END
GO
