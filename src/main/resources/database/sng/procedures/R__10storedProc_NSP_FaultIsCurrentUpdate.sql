SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaultIsCurrentUpdate', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaultIsCurrentUpdate' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaultIsCurrentUpdate AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaultIsCurrentUpdate]
(
@FaultID int
,@IsCurrent bit
,@Username varchar(255)
,@Version varbinary(8)
,@deactivateTime datetime2(3)
)
AS
BEGIN 

DECLARE @rowcount int
	
UPDATE Fault set IsCurrent = @IsCurrent, EndTime = @deactivateTime, LastUpdateTime = SYSDATETIME() where ID = @FaultID AND RowVersion = @Version

SELECT
   @rowcount = @@ROWCOUNT
   
IF @rowcount = 1
INSERT INTO FaultStatusHistory(FaultID,UserName,IsCurrent,timestamp) VALUES(@FaultID,@Username,@IsCurrent,@deactivateTime)


SELECT @rowcount
END
GO
