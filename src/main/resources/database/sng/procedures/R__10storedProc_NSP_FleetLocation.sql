SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FleetLocation', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FleetLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetLocation AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FleetLocation]
AS
/******************************************************************************
**  Name:           NSP_FleetLocation
**  Description:    Returns live channel data for the application
**  Call frequency: Every 5s
**  Parameters:     None
**  Return values:  0 if successful, else an error is raised
*******************************************************************************/
BEGIN
    SET NOCOUNT ON;
    
    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        UnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
        , UnitStatus varchar(20) NULL
    )
    
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
        , SUM(CASE UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END) AS UnitStatus
    FROM dbo.VW_IX_Fault WITH (NOEXPAND)
    WHERE FaultUnitID IS NOT NULL
        AND Category <> 'Test'
        AND IsCurrent = 1
    GROUP BY FaultUnitID

    -- return data
    SELECT 
        fs.UnitID
        , fs.UnitNumber
        , fs.UnitType
        , fs.FleetFormationID
        , fs.UnitPosition
        , fs.Headcode
        , fs.Diagram
        , fs.Loading
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM dbo.Location
            INNER JOIN dbo.LocationArea ON LocationID = Location.ID
            WHERE 
                (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                    AND Col2 BETWEEN MinLongitude AND MaxLongitude)
            ORDER BY Priority
        )
        , FaultNumber           = ISNULL(vf.FaultNumber,0)
        , WarningNumber     = ISNULL(vf.WarningNumber,0)
        , UnitStatus        =ISNULL(vf.UnitStatus,0)
        , VehicleList
        , ServiceStatus         = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                        SELECT TOP 1 LocationID
                        FROM dbo.LocationArea
                        WHERE 
                            (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                                AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                        ORDER BY Priority
                    )
                    THEN 'Ready for Service'
                WHEN fs.Headcode IS NOT NULL
                    THEN 'In Service'
                ELSE 'Out of Service'
            END
        ,cv.UpdateRecord
        ,cv.RecordInsert
        -- temporary solution for datetime and JTDS driver 
        ,cv.[TimeStamp] AS 'TimeStamp'
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
        , ServiceStatusEKE      = CASE 
            WHEN fs.Headcode IS NOT NULL AND fs.LocationIDHeadcodeStart = (
                    SELECT TOP 1 LocationID
                    FROM LocationArea
                    WHERE 
                        (Col1 IS NOT NULL AND Col2 IS NOT NULL AND Col1 BETWEEN MinLatitude AND MaxLatitude
                            AND Col2 BETWEEN MinLongitude AND MaxLongitude)
                    ORDER BY Priority
                )
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END    
    FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
    INNER JOIN dbo.VW_UnitLastChannelValueTimestamp ulc WITH (NOLOCK) ON ulc.UnitID = fs.UnitID
    LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = ulc.LastTimeStamp AND ulc.UnitID = cv.UnitID
    LEFT JOIN @currentVehicleFault vf ON fs.UnitID = vf.UnitID
END
GO
