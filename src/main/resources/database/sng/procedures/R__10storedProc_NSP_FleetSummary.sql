SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FleetSummary', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FleetSummary AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FleetSummary]
AS
    BEGIN

        SET NOCOUNT ON;
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

        -- Get totals for Vehicle based faults
        CREATE TABLE #currentUnitFault (
            UnitID int NOT NULL PRIMARY KEY 
            , P1FaultNumber int NULL
            , P2FaultNumber int NULL
            , P3FaultNumber int NULL
            , P4FaultNumber int NULL
            , P5FaultNumber int NULL
            , RecoveryNumber int NULL
            , NotAcknowledgedNumber int NULL
        )
        
        INSERT INTO #currentUnitFault
        SELECT 
            f.FaultUnitID
            , SUM(CASE f.FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
            , SUM(CASE f.FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
            , SUM(CASE f.FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
            , SUM(CASE f.FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
            , SUM(CASE f.FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
            , SUM(CASE f.HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
            , SUM(CASE f.IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
            
        FROM dbo.VW_IX_Fault f WITH (NOEXPAND, NOLOCK)
        WHERE f.FaultUnitID IS NOT NULL
            AND f.ReportingOnly = 0
            AND IsCurrent = 1
        GROUP BY f.FaultUnitID

        -- Get Latest ChannelValue timestamp for each of the vehicles transmitting
        CREATE TABLE #LatestChannelValue (
            UnitID int PRIMARY KEY
            , LastTimeStamp datetime2(3)
        )
        INSERT #LatestChannelValue( UnitID , LastTimeStamp)
        SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
        FROM dbo.ChannelValue c WITH (NOLOCK)
        GROUP BY c.UnitID
        
        
        -- return data
        SELECT 
            fs.UnitID
            , fs.UnitID AS VehicleID
            , fs.UnitNumber
            , fs.UnitType
            , fs.FleetFormationID
            , fs.UnitPosition
            , fs.Headcode
            , fs.Diagram
            , fs.Loading
            , fs.FleetCode
            , fs.ActiveCab
            , Location = l.LocationCode 
            , P1FaultNumber     = ISNULL(vf.P1FaultNumber,0)
            , P2FaultNumber     = ISNULL(vf.P2FaultNumber,0)
            , P3FaultNumber     = ISNULL(vf.P3FaultNumber,0)
            , P4FaultNumber     = ISNULL(vf.P4FaultNumber,0)
            , P5FaultNumber     = ISNULL(vf.P5FaultNumber,0)
            , FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
            , FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
            , UnitStatus        = CASE fs.UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END
            , VehicleList
            , ServiceStatus     = CASE 
                WHEN fs.Headcode IS NOT NULL
                    AND fs.LocationIDHeadcodeStart = l.ID
                    THEN 'Ready for Service'
                WHEN fs.Headcode IS NOT NULL
                    THEN 'In Service'
                ELSE 'Out of Service'
            END
            ,cv.[UpdateRecord]
            ,cv.[RecordInsert]
            -- temporary solution for datetime and JTDS driver 
            ,CAST(cv.[TimeStamp] AS datetime2(3)) AS TimeStamp,
    -- Channel Data 
        [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
        FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
        LEFT JOIN #LatestChannelValue vlc WITH (NOLOCK) ON vlc.UnitID = fs.UnitID
        LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = vlc.LastTimeStamp AND vlc.UnitID = cv.UnitID
        LEFT JOIN #currentUnitFault vf ON fs.UnitID = vf.UnitID
        LEFT JOIN dbo.Location l ON l.ID = (
                SELECT TOP 1 LocationID 
                FROM dbo.LocationArea 
                WHERE cv.Col1 BETWEEN MinLatitude AND MaxLatitude
                    AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude
                ORDER BY Priority
        )

END
GO
