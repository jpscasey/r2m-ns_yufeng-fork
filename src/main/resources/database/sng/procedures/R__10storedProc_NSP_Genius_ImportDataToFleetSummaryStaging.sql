SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_ImportDataToFleetSummaryStaging', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_ImportDataToFleetSummaryStaging' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_ImportDataToFleetSummaryStaging AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_ImportDataToFleetSummaryStaging]
AS
BEGIN

	SET NOCOUNT ON;
	--------------------------------------------------------------------------------
	-- Ignore files for units not existing in Spectrum DB
	--------------------------------------------------------------------------------

	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - UnitNumber does not exist in Unit table'
	WHERE ProcessingStatus = 'Data loaded from file'
	AND	dbo.FN_GeniusGetUnitFromFilename(Name) NOT IN (SELECT UnitNumber FROM Unit)

	--------------------------------------------------------------------------------
	-- Ignore obsolete files
	--------------------------------------------------------------------------------
	;WITH CteFilesToProcess
	AS
	(
	SELECT 
	ID
	, TimeGenerated	= dbo.FN_GeniusGetTimestampFromFilename(Name)
	, Unit	= dbo.FN_GeniusGetUnitFromFilename(Name)
	, RowNumberPerUnit	= ROW_NUMBER() OVER (PARTITION BY dbo.FN_GeniusGetUnitFromFilename(Name) ORDER BY dbo.FN_GeniusGetTimestampFromFilename(Name) DESC)
	FROM dbo.GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	)
	UPDATE GeniusInterfaceFile SET
	ProcessingStatus = 'Ignored - Oboslete data for unit'
	WHERE ID IN (
	SELECT ID 
	FROM CteFilesToProcess
	WHERE RowNumberPerUnit <> 1
	)


	--------------------------------------------------------------------------------
	-- Validate and Copy to FleetSummaryStaging
	--------------------------------------------------------------------------------
	DECLARE @FileID int
	WHILE 1 = 1
	BEGIN
	SET @FileID = (
	SELECT MIN(ID)
	FROM GeniusInterfaceFile
	WHERE ProcessingStatus = 'Data loaded from file'
	AND ID > ISNULL(@FileID, 0)
	)
	IF @FileID IS NULL BREAK
	-----------------------------------------------------------

	-- Truncate GeniusStaging table
	TRUNCATE TABLE dbo.GeniusStaging
	
	PRINT '--------------------------------------------------------------------------------'
	
	PRINT 'NSP_Genius_ValidateAndCopy FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ValidateAndCopy @FileID
	
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Loaded to Staging Table'
	BEGIN
	PRINT 'NSP_Genius_ImportDataFromStaging FileID: ' + CAST(@FileID AS varchar(30))
	EXEC NSP_Genius_ImportDataFromStaging @FileID
	END
	
	-- If File status has not changed rais error
	IF (SELECT ProcessingStatus FROM GeniusInterfaceFile WHERE ID = @FileID) = 'Data loaded from file'
	BEGIN
	
	EXEC dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Error - Unknown error in NSP_Genius_ImportDataToFleetSummaryStaging'
	END
	END

END
GO
