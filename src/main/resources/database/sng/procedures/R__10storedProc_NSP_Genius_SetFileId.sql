SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_SetFileId', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_SetFileId' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_SetFileId AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_SetFileId]
	 @fileid int
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**	status to 'loaded'	
**	Return values: returns -1 if an error occurred
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-09-21		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
	-- set file id for loaded records
	update
	dbo.GeniusInterfaceData
	set
	GeniusInterfaceFileId = @fileid
	where
	GeniusInterfaceFileId is null
	;
	
	-- update file processing status
	exec dbo.NSP_Genius_UpdateFileStatus @fileId = @fileid, @status = 'Data loaded from file'

	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
