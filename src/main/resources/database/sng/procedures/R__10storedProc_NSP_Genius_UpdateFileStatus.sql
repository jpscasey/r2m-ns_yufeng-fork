SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_UpdateFileStatus', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_UpdateFileStatus' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_UpdateFileStatus AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_UpdateFileStatus]
	 @fileId int = null
	,@fileName varchar(255) = null
	,@status varchar(255)
as
/******************************************************************************
**	Name: NSP_Genius_CreateNewFile
**	Description:	updates the Load table with the file id and sets the file 
**					status to 'loaded'	
**	Return values: returns -1 if an error occurred
*******************************************************************************
**	Parameters:
**	Input
**	-----------
**	 @fileId		id of the imported file
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:				Author:		Description:
**	2011-09-21			HeZ			creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
		-- either filename or file id must be provided
		if (@fileId is null and @fileName is null)
		begin
			raiserror(N'Either file name or file Id must be provided!', 11, 1);
			return -1;
		end
		
		-- update the status
		update
			dbo.GeniusInterfaceFile
		set
			ProcessingStatus = @status
		where
			ID = isnull(@fileId, ID)
			and Name = isnull(@fileName, Name)
		;
		
	end try
	begin catch
		exec dbo.NSP_RethrowError;
		return -1;
	end catch	
		
	return 0;
end
GO
