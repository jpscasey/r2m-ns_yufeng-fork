SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_WriteLog', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_WriteLog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_WriteLog AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_WriteLog]
	 @fileId int 
	,@logType tinyint = 1
	,@logText varchar(500)
as 
/******************************************************************************
**	Name: NSP_Genius_WriteLog
**	Description:	
**	Return values: returns 0 if successful
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:	Author:	Description:
**	2011-10-28		HeZ	creation on database
*******************************************************************************/	

begin
	SET NOCOUNT ON;

	begin try
	-- write log entry
	insert
	dbo.GeniusLog
	(GeniusInterfaceFileId
	,LogType
	,LogText)
	values
	(@fileId
	,@logType
	,@logText)
	;
	
	end try
	begin catch
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
