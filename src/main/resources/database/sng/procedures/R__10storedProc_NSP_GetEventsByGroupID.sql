SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetEventsByGroupID', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetEventsByGroupID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetEventsByGroupID AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetEventsByGroupID] (
	@FaultGroupID int
)
AS
BEGIN

	SELECT 
		  f.ID
		, Headcode
		, f.FleetCode
		, FaultUnitNumber
		, FaultCode
		, LocationCode
		, FaultUnitID
		, RecoveryID
		, Latitude
		, Longitude
		, f.FaultMetaID
		, IsCurrent
		, IsDelayed
		, ReportingOnly
		, f.Description
		, Summary
		, Category
		, CategoryID
		, f.CreateTime
		, EndTime
		, FaultType
		, FaultTypeColor
		, HasRecovery
		, MaximoID = sr.ExternalCode
		, MaximoServiceRequestStatus = sr.Status
		, CreatedByRule
		, IsGroupLead
		, AdditionalInfo
		, IsAcknowledged
		, RowVersion
		, FaultGroupID
		, f.CountSinceLastMaint
		, fef.Value AS PositionCode
	FROM 
		dbo.VW_IX_Fault f with (noexpand, nolock)
		LEFT JOIN dbo.Location ON f.LocationID = Location.ID
		LEFT JOIN dbo.ServiceRequest sr ON f.ServiceRequestID = sr.ID
		LEFT JOIN dbo.FaultMetaExtraField fef ON f.FaultMetaID = fef.FaultMetaId and Field = 'positionCode'
	WHERE 
		f.FaultGroupID = @FaultGroupID

END
GO
