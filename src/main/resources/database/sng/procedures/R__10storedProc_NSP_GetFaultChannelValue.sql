SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValue]
    @DatetimeStart datetime2(3)
    , @DatetimeEnd datetime2(3)
    , @UnitID int = NULL
    , @UpdateRecord bit = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT 
        cv.UnitID
        , Headcode = CAST(NULL AS varchar(7))
        , u.UnitNumber
        , u.UnitType
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = Location.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , [TimeStamp]           = CONVERT(datetime2(3), cv.TimeStamp)
        , Location.ID AS LocationID
        , Location.Tiploc AS Tiploc
        , Location.LocationName AS LocationName
        ,
        -- Channel Values
        [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
    FROM dbo.ChannelValue cv WITH (NOLOCK)
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.FleetStatus fs ON
        cv.TimeStamp > ValidFrom 
        AND cv.TimeStamp <= ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
        AND cv.UnitID = fs.UnitID
    LEFT JOIN dbo.Location ON Location.ID = (
                                        SELECT TOP 1 LocationID 
                                        FROM dbo.LocationArea 
                                        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
                                            AND col2 BETWEEN MinLongitude AND MaxLongitude
                                        ORDER BY Priority
                                    )
    WHERE cv.TimeStamp BETWEEN @DatetimeStart AND @DatetimeEnd
        AND (cv.UnitID = @UnitID OR @UnitID IS NULL)
        AND (cv.UpdateRecord = @UpdateRecord OR @UpdateRecord IS NULL)
    ORDER BY TimeStamp

END
GO
