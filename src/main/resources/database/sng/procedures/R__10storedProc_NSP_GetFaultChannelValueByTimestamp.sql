SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueByTimestamp', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueByTimestamp' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueByTimestamp AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueByTimestamp]
(
    @LastTime bigint,
	@RuleEngineDelay int
)
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        
    DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'
    DECLARE @LowerTime datetime2(3) = (SELECT MIN(Timestamp) FROM dbo.ChannelValue WHERE Timestamp > DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) )
    IF @LowerTime is NULL
    	SET @LowerTime = DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
    SET @LowerTime = DATEADD(ss, 300, @LowerTime)
    
	DECLARE @UpperTime datetime2(3) = (SELECT MAX(Timestamp) FROM dbo.ChannelValue WHERE Timestamp <= SYSDATETIME())	
	SET @UpperTime = DATEADD(ss, -@RuleEngineDelay, @UpperTime)
	IF @LowerTime < @UpperTime
		SET @UpperTime = @LowerTime
        
    SELECT 
        cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , UnitStatus    = u.UnitStatus
        , UnitMaintLoc  = u.UnitMaintLoc
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime2(3), cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,

        -- Channel Values
        [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]

    FROM dbo.ChannelValue cv 
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN dbo.FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
    WHERE cv.Timestamp <= @UpperTime AND cv.Timestamp > DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
    ORDER BY ID

END
GO
