SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueGpsByID', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueGpsByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueGpsByID AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueGpsByID]
    @ID int,
    @Datetime datetime2(3)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.VW_ChannelValueGps), 0) - 1
    ELSE
        SET @ID = (
            SELECT TOP 1 (VW_ChannelValueGps.ID - 1) 
            FROM dbo.VW_ChannelValueGps WITH(NOLOCK)
            WHERE VW_ChannelValueGps.ID > @ID
            ORDER BY VW_ChannelValueGps.ID ASC
        )

    SELECT TOP 10000 gcv.ID
        ,gcv.UnitID
        ,gcv.Col2001 --Latitude
        ,gcv.Col2002 --Longitude
        ,gcv.Col2003 --Speed
        ,gcv.TimeStamp
		,LocationId = l.ID
		,Tiploc = l.Tiploc
		,LocationName = l.LocationName
    FROM dbo.VW_ChannelValueGps gcv
	LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE gcv.Col2001 BETWEEN MinLatitude AND MaxLatitude
            AND gcv.Col2002 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
	)
    WHERE gcv.ID > @ID AND gcv.TimeStamp <= @Datetime ORDER BY ID ASC

END
GO
