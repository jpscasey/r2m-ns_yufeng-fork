IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueGpsLatestSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueGpsLatestSince AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueGpsLatestSince]
	@LastTime bigint,
	@MaxTimeDifferenceMinutes int
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'
	SELECT  
		gcv.ID
        ,gcv.UnitID
        ,gcv.Col2001 --Latitude
        ,gcv.Col2002 --Longitude
        ,gcv.Col2003 --Speed
        ,gcv.TimeStamp
		,LocationId = l.ID
		,Tiploc = l.Tiploc
		,LocationName = l.LocationName
    FROM dbo.VW_ChannelValueGps gcv
	LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE gcv.Col2001 BETWEEN MinLatitude AND MaxLatitude
            AND gcv.Col2002 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
	)
	INNER JOIN (
			SELECT
				MAX(Timestamp) Timestamp,
				UnitID
			FROM [dbo].[VW_ChannelValueGps]
			WHERE 
				DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime))  <= Timestamp AND Timestamp < DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
			GROUP BY 
				UnitID	
		) AS t2
		ON
			gcv.UnitID = t2.UnitID
			AND gcv.Timestamp = t2.Timestamp
	WHERE 
		DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime))  <= gcv.Timestamp AND gcv.Timestamp < DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
	
END
GO