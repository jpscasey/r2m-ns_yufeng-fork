SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueLatest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueLatest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueLatest]
    @DatetimeStart DATETIME2(3)
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH ChannelValueLatest
    AS (
        SELECT MAX(cv.TimeStamp) AS TimeStamp
            ,cv.UnitID
        FROM dbo.ChannelValue cv
        WHERE cv.TimeStamp <= @DatetimeStart
            AND cv.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND cv.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY cv.UnitID
        )
    SELECT  
		cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , UnitStatus    = u.UnitStatus
        , UnitMaintLoc  = u.UnitMaintLoc
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime2(3), cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,

        -- Channel Values
        [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]

    FROM ChannelValueLatest cvl
    JOIN [dbo].[ChannelValue] cv ON cvl.UnitID = cv.UnitID AND cvl.TimeStamp = cv.TimeStamp 
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN dbo.FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
END
GO
