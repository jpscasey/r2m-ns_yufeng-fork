IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueLatestSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueLatestSince AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueLatestSince]
	@LastTime bigint,
	@MaxTimeDifferenceMinutes int
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'

	SELECT  
        cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , UnitStatus    = u.UnitStatus
        , UnitMaintLoc  = u.UnitMaintLoc
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime2(3), cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,

        -- Channel Values
        [Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]

    FROM dbo.ChannelValue cv
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN dbo.FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
	INNER JOIN (
		SELECT
			MAX(Timestamp) Timestamp,
			UnitID
		FROM [dbo].[ChannelValue]
		WHERE 
			DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime)) <= Timestamp AND Timestamp <= DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
		GROUP BY 
			UnitID	
	) AS t2
	ON
		cv.UnitID = t2.UnitID
		AND cv.Timestamp = t2.Timestamp
	WHERE 
		DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime)) <= cv.Timestamp AND cv.Timestamp <= DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
END
GO