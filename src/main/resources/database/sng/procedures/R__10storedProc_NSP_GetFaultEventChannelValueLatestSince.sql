SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultEventChannelValueLatestSince', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultEventChannelValueLatestSince' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelValueLatestSince AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatestSince]
	@LastTime bigint,
	@MaxTimeDifferenceMinutes int
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'

	SELECT 
		t1.ID, 
		t1.UnitID, 
		t1.ChannelID, 
		t1.TimeStamp, 
		t1.Value
	FROM
		[dbo].[EventChannelValue] t1 
		INNER JOIN (
			SELECT 
				Max(Timestamp) Timestamp, 
				UnitID, 
				ChannelID
			FROM [dbo].[EventChannelValue] 
			WHERE 
				DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime))  <= Timestamp AND Timestamp <= DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
			GROUP BY 
				UnitID, ChannelID
		) AS t2 
		ON 
			t1.UnitID = t2.UnitID
			AND t1.ChannelID = t2.ChannelID
			AND t1.TimeStamp = t2.TimeStamp
	WHERE 
		DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000-@MaxTimeDifferenceMinutes*60, @OriginTime))  <= t1.Timestamp AND t1.Timestamp <= DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) 
END
GO
