SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetLastChannelDataForVehicle', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetLastChannelDataForVehicle' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetLastChannelDataForVehicle AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetLastChannelDataForVehicle]
    @DateTimeStart datetime2(3)
    , @VehicleID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, SYSDATETIME())
                
    ;WITH CteLatestRecords 
    AS
    (
        SELECT 
            VehicleID   = Vehicle.ID
            , UnitID    = Vehicle.UnitID
            , LastTimeStamp = (
                SELECT TOP 1 TimeStamp
                FROM dbo.ChannelValue
                WHERE  TimeStamp BETWEEN DATEADD(d, -7, @DateTimeStart) AND @DateTimeStart
                    AND ChannelValue.UnitID = Vehicle.UnitID 
                ORDER BY TimeStamp DESC
            )
        FROM VW_Vehicle AS Vehicle
        WHERE Vehicle.ID = @VehicleID
    )   

    SELECT 
        UnitNumber
        , Headcode  = CONVERT(varchar(10), NULL)
        , Diagram   = CONVERT(varchar(10), NULL)
        , Vehicle.ID AS VehicleID
        , Vehicle.Type AS VehicleType
        , Vehicle.VehicleTypeID AS VehicleTypeID
        , Vehicle.VehicleNumber AS VehicleNumber
        , (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM dbo.Location
            INNER JOIN dbo.LocationArea ON LocationID = Location.ID
            WHERE col1 BETWEEN MinLatitude AND MaxLatitude
                AND col2 BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) AS Location
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,CAST(cv.[TimeStamp] AS datetime2(3)) AS TimeStamp      
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
    FROM dbo.VW_Vehicle Vehicle 
    INNER JOIN CteLatestRecords     ON Vehicle.UnitID = CteLatestRecords.UnitID
    LEFT JOIN dbo.ChannelValue cv       ON Vehicle.UnitID = cv.UnitID  AND CteLatestRecords.LastTimeStamp = cv.TimeStamp

END
GO
