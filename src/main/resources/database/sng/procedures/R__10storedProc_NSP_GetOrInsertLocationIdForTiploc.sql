SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetOrInsertLocationIdForTiploc', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetOrInsertLocationIdForTiploc' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetOrInsertLocationIdForTiploc AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetOrInsertLocationIdForTiploc]
	@Tiploc varchar(20)
	, @LocationID int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @LocationID = (SELECT TOP 1 ID FROM dbo.Location WHERE Tiploc = @tiploc)
	 					
	IF @LocationID IS NULL --insert tiploc if not exists in database
	BEGIN
		INSERT INTO dbo.Location (LocationCode, LocationName, Tiploc, Type)
		SELECT @tiploc, @tiploc, @tiploc, 'U'

		SET @locationID = @@IDENTITY
	END

END
GO
