SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelValue]
    @DateTimeStart datetime2(3)
    , @UnitID int
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @setcode varchar(50)
    DECLARE @headcode varchar(50)
    
    IF @UnitID IS NULL
    BEGIN
        PRINT 'Unit Number not in a database'
        RETURN -1 
    END


    IF @DateTimeStart IS NULL
        SET @DateTimeStart = DATEADD(hh, 1, SYSDATETIME())

    -- Get totals for Vehicle based faults
    DECLARE @currentVehicleFault TABLE(
        FaultUnitID int NULL
        , FaultNumber int NULL
        , WarningNumber int NULL
        , P1FaultNumber int NULL
        , P2FaultNumber int NULL
        , P3FaultNumber int NULL
        , P4FaultNumber int NULL
        , P5FaultNumber int NULL
        , RecoveryNumber int NULL
        , NotAcknowledgedNumber int NULL
    )
        
    INSERT INTO @currentVehicleFault
    SELECT 
        FaultUnitID
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS WarningNumber
        , SUM(CASE FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
        , SUM(CASE FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
        , SUM(CASE FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
        , SUM(CASE FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
        , SUM(CASE FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
        , SUM(CASE HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
        , SUM(CASE IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
    FROM dbo.VW_IX_Fault WITH (NOEXPAND, NOLOCK)
    WHERE FaultUnitID  = @UnitID
        AND Category <> 'Test'
        AND ReportingOnly = 0
        AND IsCurrent = 1
    GROUP BY FaultUnitID

    ;WITH CteLatestRecords 
    AS
    (
        SELECT 
            Unit.ID AS UnitID
            , (
                SELECT TOP 1 TimeStamp
                FROM dbo.ChannelValue WITH (NOLOCK)
                WHERE TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
                    AND UnitID =    Unit.ID 
                ORDER BY TimeStamp DESC
            ) AS MaxChannelValueTimeStamp
        FROM dbo.Unit
        WHERE ID = @UnitID
    )   

    SELECT 
        UnitNumber  = u.UnitNumber
        , UnitID    = u.ID
        , UnitStatus = u.UnitStatus
        , UnitMaintLoc = u.UnitMaintLoc
        , LastMaintDate = CAST(u.[LastMaintDate] AS datetime2(3))
        , Headcode  = fs.Headcode
        , Diagram   = fs.Diagram
        , FaultNumber   = ISNULL(vf.FaultNumber,0)
        , WarningNumber = ISNULL(vf.WarningNumber,0)
        , P1FaultNumber     = ISNULL(vf.P1FaultNumber,0)
        , P2FaultNumber     = ISNULL(vf.P2FaultNumber,0)
        , P3FaultNumber     = ISNULL(vf.P3FaultNumber,0)
        , P4FaultNumber     = ISNULL(vf.P4FaultNumber,0)
        , P5FaultNumber     = ISNULL(vf.P5FaultNumber,0)
        , FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
        , FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
        , Location      = (
            SELECT TOP 1 
                LocationCode -- CRS
            FROM Location
            INNER JOIN LocationArea ON LocationID = Location.ID
            WHERE Col1 BETWEEN MinLatitude AND MaxLatitude
                AND Col2 BETWEEN MinLongitude AND MaxLongitude
                AND LocationArea.Type = 'C'
            ORDER BY Priority
        )
        ,cv.[UpdateRecord]
        ,cv.[RecordInsert]
        ,fs.FleetCode
        ,TimeStamp      = CAST(cv.[TimeStamp] AS datetime2(3))

        -- Channel Data 
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20]
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
    FROM dbo.Unit u 
    INNER JOIN CteLatestRecords     ON u.ID = CteLatestRecords.unitID
    LEFT JOIN dbo.ChannelValue cv WITH (NOLOCK)      ON u.ID = cv.UnitID  AND CteLatestRecords.MaxChannelValueTimeStamp = cv.TimeStamp
    LEFT JOIN @currentVehicleFault vf ON u.ID = vf.FaultUnitID
    LEFT JOIN dbo.VW_FleetStatus fs     ON u.ID = fs.UnitID

END
GO
