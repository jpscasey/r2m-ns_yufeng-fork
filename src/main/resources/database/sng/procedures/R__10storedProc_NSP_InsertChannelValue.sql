SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
        @Timestamp datetime2(3),
        @UnitID int,
        @UpdateRecord bit 
        ,@Col1 BigInt = NULL,@Col2 BigInt = NULL,@Col3 BigInt = NULL,@Col4 BigInt = NULL,@Col5 BigInt = NULL,@Col6 BigInt = NULL,@Col7 BigInt = NULL
        ,@Col8 BigInt = NULL,@Col9 BigInt = NULL,@Col10 BigInt = NULL,@Col11 BigInt = NULL,@Col12 BigInt = NULL,@Col13 BigInt = NULL,@Col14 BigInt = NULL
        ,@Col15 BigInt = NULL,@Col16 BigInt = NULL,@Col17 BigInt = NULL,@Col18 BigInt = NULL,@Col19 BigInt = NULL,@Col20 BigInt = NULL,@Col21 BigInt = NULL
        ,@Col22 BigInt = NULL,@Col23 BigInt = NULL,@Col24 BigInt = NULL,@Col25 BigInt = NULL,@Col26 BigInt = NULL,@Col27 BigInt = NULL,@Col28 BigInt = NULL
        ,@Col29 BigInt = NULL,@Col30 BigInt = NULL,@Col31 BigInt = NULL,@Col32 BigInt = NULL
        )
        AS
        BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
    DECLARE @CVID BIGINT
    
INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
           ,[UpdateRecord]
           ,[col1],[col2],[col3],[col4],[col5],[col6],[col7],[col8],[col9],[col10],[col11],[col12],[col13],[col14],[col15],[col16]
        ,[col17],[col18],[col19],[col20],[col21],[col22],[col23],[col24],[col25],[col26],[col27],[col28],[col29],[col30],[col31],[col32]
           )
    OUTPUT inserted.ID INTO @IDs(ID) 
    SELECT TOP 1
        @UnitID
        ,@Timestamp
        ,@UpdateRecord
        ,ISNULL(@Col1,COL1),ISNULL(@Col2,COL2),ISNULL(@Col3,COL3),ISNULL(@Col4,COL4),ISNULL(@Col5,COL5),ISNULL(@Col6,COL6),ISNULL(@Col7,COL7)
        ,ISNULL(@Col8,COL8),ISNULL(@Col9,COL9),ISNULL(@Col10,COL10),ISNULL(@Col11,COL11),ISNULL(@Col12,COL12),ISNULL(@Col13,COL13),ISNULL(@Col14,COL14)
        ,ISNULL(@Col15,COL15),ISNULL(@Col16,COL16),ISNULL(@Col17,COL17),ISNULL(@Col18,COL18),ISNULL(@Col19,COL19),ISNULL(@Col20,COL20),ISNULL(@Col21,COL21)
        ,ISNULL(@Col22,COL22),ISNULL(@Col23,COL23),ISNULL(@Col24,COL24),ISNULL(@Col25,COL25),ISNULL(@Col26,COL26),ISNULL(@Col27,COL27),ISNULL(@Col28,COL28)
        ,ISNULL(@Col29,COL29),ISNULL(@Col30,COL30),ISNULL(@Col31,COL31),ISNULL(@Col32,COL32)
    FROM dbo.ChannelValue WITH (NOLOCK)
    WHERE UnitID = @UnitID
    ORDER BY 1 DESC
    
    SELECT @CVID = ID FROM @IDs

END
GO
