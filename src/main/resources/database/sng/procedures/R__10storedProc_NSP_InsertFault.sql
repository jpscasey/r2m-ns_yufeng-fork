SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertFault', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertFault' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertFault AS BEGIN RETURN(1) END;')
END
GO


ALTER PROCEDURE [dbo].[NSP_InsertFault]
    @FaultCode varchar(100)
    , @FleetCode varchar(50)
    , @TimeCreate datetime2(3)
    , @TimeEnd datetime2(3) = NULL
    , @FaultUnitID int
    , @RuleName varchar(255)
    , @Context varchar(50)
    , @IsDelayed bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @rowcount int
    DECLARE @faultMetaID int
    DECLARE @faultMetaRecovery varchar(100) 
    DECLARE @faultID int 
    DECLARE @faultLocationID int 
    DECLARE @faultLat decimal(9,6)
    DECLARE @faultLng decimal(9,6)
    DECLARE @faultCount int
    DECLARE @unitStatus varchar(20)
    DECLARE @fleetID int

    IF (SELECT COUNT(*) FROM dbo.FaultMeta  fm
        INNER JOIN dbo.Fleet fl ON fm.FleetID = fl.ID
        WHERE fm.FaultCode = @FaultCode AND fl.Code = @FleetCode) > 0
    BEGIN

        -- Get the fleet ID
        SELECT @fleetID = ID FROM Fleet WHERE Code = @FleetCode
        
        DECLARE @fromDate datetime2(3)
        DECLARE @toDate datetime2(3)

        SELECT @fromDate = FromDate, @toDate = ToDate
        FROM dbo.FaultMeta 
        WHERE FaultCode = @FaultCode AND ParentID IS NOT NULL AND FleetID = @fleetID

        DECLARE @dateDiffFrom int = YEAR(@fromDate) - YEAR(@TimeCreate)
        DECLARE @dateDiffTo int = YEAR(@toDate) - YEAR(@TimeCreate)

        DECLARE @translatedDateFrom datetime2(3) = DATEADD(yyyy, -@dateDiffFrom, @fromDate)
        DECLARE @translatedDateTo datetime2(3) = DATEADD(yyyy, -@dateDiffFrom, @toDate)

        DECLARE @translatedDateFromPlus1 datetime2(3) = DATEADD(yyyy, -@dateDiffTo, @fromDate)
        DECLARE @translatedDateToPlus1 datetime2(3) = DATEADD(yyyy, -@dateDiffTo, @toDate)

        IF ((@TimeCreate BETWEEN @translatedDateFrom AND @translatedDateTo) OR (@TimeCreate BETWEEN @translatedDateFromPlus1 AND @translatedDateToPlus1))
            SELECT
                @faultMetaID = ID 
                , @faultMetaRecovery = RecoveryProcessPath
            FROM dbo.FaultMeta 
            WHERE FaultCode = @FaultCode AND ParentID IS NOT NULL AND FleetID = @fleetID
        ELSE 
            SELECT
                @faultMetaID = ID 
                , @faultMetaRecovery = RecoveryProcessPath
            FROM dbo.FaultMeta 
            WHERE FaultCode = @FaultCode AND ParentID IS NULL AND FleetID = @fleetID
    END
    ELSE
    BEGIN
        RAISERROR ('FaultCode does not exist',1,1)
        RETURN -4
    END

    -- check if fault is already inserted
    IF EXISTS (
        SELECT 1 
        FROM dbo.Fault 
        WHERE CreateTime = @TimeCreate 
            AND FaultMetaID = @faultMetaID
            AND FaultUnitID = @FaultUnitID
    )
    BEGIN
        RAISERROR ('Duplicate Fault',1,1)
        RETURN -1
    END     
    
    -- insert into Fault table

    -- get  DECLARE @faultLocationID int 
    SELECT TOP 1
        @faultLat = Latitude
        , @faultLng = Longitude
        , @faultLocationID = l.ID
        FROM dbo.ChannelValueGps
        LEFT JOIN dbo.Location l ON l.ID = (
            SELECT TOP 1 LocationID 
            FROM dbo.LocationArea 
            WHERE Latitude BETWEEN MinLatitude AND MaxLatitude
                AND Longitude BETWEEN MinLongitude AND MaxLongitude
            ORDER BY Priority
        ) 
        WHERE UnitID = @FaultUnitID
            AND TimeStamp <= @TimeCreate
            AND TimeStamp > DATEADD(s, -30, @TimeCreate)
        ORDER BY TimeStamp DESC
    
    BEGIN TRAN

        DECLARE @headcode varchar(10)
        DECLARE @setcode varchar(10)            
            
        SELECT 
            @headcode = Headcode
            , @setcode = Setcode
        FROM dbo.FleetStatus
        WHERE 
            UnitID = @FaultUnitID
            AND @TimeCreate >= ValidFrom 
            AND @TimeCreate < ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME()))  -- in case there is a time difference between app and db server
         
        -- get UnitStatus at the time of the Fault
        SELECT 
        	@unitStatus = UnitStatus
        FROM dbo.Unit
        WHERE ID = @faultUnitId
        
         -- get current count of faults
        SELECT TOP 1 
            @faultCount = FaultCounter 
            FROM FaultCount fc 
            WHERE fc.UnitID = @faultUnitID 
            AND fc.FaultMetaID = @faultMetaID

        -- if there is no entry in FaultCount for this UnitID/FaultMetaID, create one
        IF @faultCount IS NULL
        BEGIN
            INSERT INTO FaultCount (UnitID, FaultMetaID, FaultCounter)
            VALUES (@faultUnitID, @faultMetaID, 0)

            SET @faultCount = 0
        END
        
        SET @faultCount = @faultCount + 1   
            
        ;WITH CteNonEmptyRecord AS
        (
            SELECT 1 AS ID
        )
        INSERT INTO dbo.[Fault]
            ([CreateTime]
            ,[HeadCode]
            ,[SetCode]
            ,[LocationID]
            ,[IsCurrent]
            ,[EndTime]
            ,[Latitude]
            ,[Longitude]
            ,[FaultMetaID]
            ,[PrimaryUnitID]
            ,[FaultUnitID]
            ,[RuleName]
            ,[IsDelayed]
            ,[CountSinceLastMaint]
            ,[RecoveryStatus]
            ,[UnitStatus])
        SELECT
            @TimeCreate         --[CreateTime]
            ,@headcode
            ,@setCode           --[SetCode]
            ,@faultLocationID   --[LocationID]
            ,1                  --[IsCurrent]
            ,@TimeEnd           --[EndTime]
            ,@faultLat          --[Latitude]
            ,@faultLng          --[Longitude]
            ,@faultMetaID       --[FaultMetaID]
            ,@FaultUnitID       --PrimaryUnitID
            ,@FaultUnitID
            ,@RuleName
            ,ISNULL(@IsDelayed, 0)
            ,@faultCount
            ,RecoveryStatus = CASE
                WHEN @faultMetaRecovery IS NOT NULL THEN 'Ready'
                ELSE NULL          
            END
            ,@unitStatus  

        -- increment the fault counter
        UPDATE dbo.[FaultCount]
        SET FaultCounter = @faultCount
        WHERE UnitID = @FaultUnitID AND FaultMetaID = @faultMetaID

        SELECT
            @rowcount = @@ROWCOUNT
            , @faultID = @@IDENTITY
        
    IF @rowcount <> 1
    BEGIN
        ROLLBACK
        RAISERROR ('Error when inserting into Fault table',1,1)
        RETURN -2
    END 
    ELSE
    BEGIN
        COMMIT
    END
    
    --insert Channel data
    INSERT INTO dbo.[FaultChannelValue]
    (
            [ID]
        ,[FaultID]
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20] 
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]
        )
        SELECT TOP (1)
        [ID]
        ,@faultID
        ,[UpdateRecord]
        ,[UnitID]
        ,[RecordInsert]
        ,[TimeStamp]
        ,[Col1], [Col2], [Col3], [Col4], [Col5], [Col6], [Col7], [Col8], [Col9], [Col10], [Col11], [Col12], [Col13], [Col14], [Col15], [Col16], [Col17], [Col18], [Col19], [Col20] 
        ,[Col21], [Col22], [Col23], [Col24], [Col25], [Col26], [Col27], [Col28], [Col29], [Col30], [Col31], [Col32]

    FROM dbo.ChannelValue 
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(SECOND, -30, @TimeCreate) AND @TimeCreate
        ORDER BY TimeStamp desc

        DECLARE @StopDATE datetime2 = DATEADD(Day ,-1 ,@TimeCreate)

        INSERT INTO dbo.FaultEventChannelValue (ID ,FaultID,UnitID,ChannelID,Value,TimeStamp)
        SELECT ID, @faultID ,@FaultUnitID, ChannelID, Value,timestamp FROM dbo.EventChannelValue where 
        ID in ( SELECT MAX(ID) FROM dbo.EventChannelValue cv where UnitID = @FaultUnitID
                        AND TimeStamp BETWEEN @StopDATE 
                        AND @TimeCreate GROUP BY ChannelID)
                        
    INSERT INTO dbo.[FaultChannelValueGps]
    (
            [ID]
        ,[FaultID]
        ,[UnitID]
        ,[TimeStamp]
         ,[Col2001],[Col2002],[Col2003]
        )
        SELECT TOP (1)
        [ID]
        ,@faultID
        ,[UnitID]
        ,[TimeStamp]
         ,[Latitude],[Longitude],[Speed]
         
    FROM dbo.ChannelValueGps
    WHERE UnitID = @FaultUnitID
        AND TimeStamp BETWEEN DATEADD(SECOND, -30, @TimeCreate) AND @TimeCreate
        ORDER BY TimeStamp desc

    -- returning faultID
    RETURN @faultID

END


GO
