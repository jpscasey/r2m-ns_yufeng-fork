SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertFaultMeta', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertFaultMeta' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultMeta AS BEGIN RETURN(1) END;')
END
GO


ALTER PROCEDURE [dbo].[NSP_InsertFaultMeta](
    @ID int
    , @Username varchar(50)
    , @FaultCode varchar(100)
    , @Description varchar(100)
    , @Summary varchar(1000)
    , @AdditionalInfo varchar(3700)
    , @Url varchar(500)
    , @CategoryID int
    , @HasRecovery bit
    , @RecoveryProcessPath varchar(100)
    , @FaultTypeID tinyint
    , @GroupID int
    , @FleetID int
    , @FromDate datetime2(3) = NULL
    , @ToDate datetime2(3) = NULL
    , @ParentID int = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@ID IS NULL)
    BEGIN
    INSERT dbo.FaultMeta(
        Username
        , FaultCode
        , Description
        , Summary
        , AdditionalInfo
        , Url
        , CategoryID
        , HasRecovery
        , RecoveryProcessPath
        , FaultTypeID
        , GroupID
        , FleetID
        , FromDate
        , ToDate
        , ParentID
    )
    SELECT 
        Username     = @Username
        , FaultCode  = @FaultCode
        , Description   = @Description 
        , Summary       = @Summary
        , AdditionalInfo = @AdditionalInfo
        , Url           = @Url
        , CategoryID    = @CategoryID
        , HasRecovery   = @HasRecovery
        , RecoveryProcessPath   = @RecoveryProcessPath
        , FaultTypeID   = @FaultTypeID
        , GroupID       = @GroupID
        , FleetID		= @FleetID
        , FromDate      = @FromDate
        , ToDate        = @ToDate
        , ParentID      = @ParentID
    END
    ELSE
    BEGIN
    SET IDENTITY_INSERT FaultMeta ON

    INSERT dbo.FaultMeta(
        ID  
        , Username
        , FaultCode
        , Description
        , Summary
        , AdditionalInfo
        , Url
        , CategoryID
        , HasRecovery
        , RecoveryProcessPath
        , FaultTypeID
        , GroupID
        , FleetID
        , FromDate
        , ToDate
        , ParentID
    )
    SELECT 
        ID          = @ID
        , Username   = @Username
        , FaultCode  = @FaultCode
        , Description   = @Description 
        , Summary       = @Summary
        , AdditionalInfo = @AdditionalInfo
        , Url           = @Url
        , CategoryID    = @CategoryID
        , HasRecovery   = @HasRecovery
        , RecoveryProcessPath   = @RecoveryProcessPath
        , FaultTypeID   = @FaultTypeID
        , GroupID       = @GroupID
        , FleetID		= @FleetID
        , FromDate      = @FromDate
        , ToDate        = @ToDate
        , ParentID      = @ParentID
    
        SET IDENTITY_INSERT FaultMeta OFF
    END

    
SELECT SCOPE_IDENTITY() AS InsertedID


END

GO