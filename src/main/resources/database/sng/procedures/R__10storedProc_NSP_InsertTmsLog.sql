SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertTmsLog', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertTmsLog' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertTmsLog AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertTmsLog]
	@VehicleNo varchar(10)
	, @FaultDate date
	, @FaultTime time
	, @CarNo varchar(10)
	, @FaultNoMostSignificantByte int
	, @FaultNoLessSignificantByte int
	, @TsnCplMostSignificantByte int
	, @TsnCplLessSignificantByte int
	, @CabByte int
	, @McsByte int
	, @DirectionByte int
	, @Voltage bit
	, @SpeedSet bit
	, @BrakeCont bit
	, @EmerBrakeIsolated bit
	, @EmerBrakeState bit
	, @Stabled bit
	, @Mlp bit
	, @BrakePr bit
	, @Battery bit
	, @Loadshed bit
	, @TractShoreSup bit
	, @AuxShoreSup bit
	, @CarBmCoupled bit
	, @CarBjCoupled bit
	, @Tmcc1 bit
	, @Speed int
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @vehicleNumber varchar(10) = REPLACE(@VehicleNo, 'v','');
	DECLARE @faultNo int		= @FaultNoMostSignificantByte + @FaultNoLessSignificantByte;
	DECLARE @tsnCpl int			= @TsnCplMostSignificantByte + @TsnCplLessSignificantByte;
	DECLARE @cab int			= @CabByte / 16;
	DECLARE @mcs int			= (@McsByte % 16) / 4;
	DECLARE @direction int		= @DirectionByte % 4;
	DECLARE @emerBrake int;
	DECLARE @faultMetaID int;
	DECLARE @faultUnitID int;
	DECLARE @faultID int;
	DECLARE @createTime datetime2(3) = CONVERT(varchar(10), @faultDate, 121) + ' ' + CONVERT(varchar(13), @faultTime);
	
	IF(@EmerBrakeIsolated = 1)
		SET @emerBrake = 2;
	ELSE
		SET @emerBrake = @EmerBrakeState;

	-- Add record in TmsFaultLookup, if corresponding FaultNo does not exist
	IF NOT EXISTS (SELECT 1 FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo) 
	BEGIN

		INSERT INTO [dbo].[TmsFaultLookup]
			([FaultNo]
			,[Description]
			,[FixingInstr]
			,[SwRef]
			,[TmsCategoryID]
			,[TmsFunctionID])
		SELECT
			@faultNo
			,'Unknown FaultNo: ' + CONVERT(varchar(100), @faultNo)
			,'?'
			,'?'
			,0
			,0

	END
	
	-- get @faultMetaID
	SET @faultMetaID = (
		SELECT ID 
		FROM dbo.FaultMeta
		WHERE FaultCode = 'F_TMS_' + CONVERT(varchar(10), @faultNo)
	)

	IF @faultMetaID IS NULL
	BEGIN

		INSERT INTO dbo.FaultMeta(
			FaultCode	
			, Description	
			, Summary	
			, CategoryID	
			, FaultTypeID
			, RecoveryProcessPath)
		SELECT
			'F_TMS_' + CONVERT(varchar(10), @faultNo)
			, (SELECT top 1 Description FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT top 1 Description FROM dbo.TmsFaultLookup WHERE FaultNo = @faultNo)
			, (SELECT top 1 ID FROM dbo.FaultCategory WHERE Category = 'TMS')
			, (SELECT top 1 ID FROM dbo.FaultType WHERE Name = 'Fault')
			, NULL
		
		SET @faultMetaID = SCOPE_IDENTITY()

	END
	
	-- get @faultVehicleID -- first vehicle on the unit
	SET @faultUnitID = (
		SELECT UnitID 
		FROM dbo.Vehicle 
		WHERE VehicleNumber = @vehicleNumber
	)
	
	IF @faultUnitID IS NOT NULL
		AND @faultMetaID IS NOT NULL
		AND @createTime IS NOT NULL
		AND NOT EXISTS (
			SELECT top 1 1
			FROM dbo.Fault
			WHERE FaultUnitID = @faultUnitID
				AND FaultMetaID = @faultMetaID
				AND CreateTime = CONVERT(datetime2(3), @createTime)
			)
	BEGIN
		
		INSERT INTO dbo.[Fault] (
			CreateTime
			,HeadCode
			,SetCode
			,LocationID
			,IsCurrent
			,EndTime
			,Latitude
			,Longitude
			,FaultMetaID
			,FaultUnitID
			,IsDelayed)
		SELECT
			CreateTime	= @createTime
			,HeadCode	= NULL
			,SetCode	= NULL
			,LocationID	= NULL
			,IsCurrent	= 0
			,EndTime	= DATEADD(n, 1, @createTime)
			,Latitude	= NULL
			,Longitude	= NULL
			,FaultMetaID	= @faultMetaID
			,FaultUnitID	= @faultUnitID
			,IsDelayed		= 0
			
		SET @faultID = SCOPE_IDENTITY()
		
	END
	
	INSERT INTO dbo.TmsFault (
		FaultID
		, VehicleNumber
		, FaultDateTime
		, CarNo
		, FaultNo
		, TsnCplId
		, CapId
		, McsId
		, DirectionId
		, VoltageId
		, SpeedSetId
		, BrakeContId
		, EmerBrakeId
		, StabledId
		, MlpId
		, BrakePrId
		, BatteryId
		, LoadshedId
		, TracShoreSupId
		, AuxShoreSupId
		, CarBmCoupledId
		, CarBjCoupledId
		, Tmcc1Id
		, Speed)
	VALUES (
		@faultID
		, @vehicleNumber
		, @createTime
		, @CarNo
		, @faultNo
		, @tsnCpl
		, @cab
		, @mcs
		, @direction
		, @voltage
		, @speedSet
		, @brakeCont
		, @emerBrake
		, @stabled
		, @mlp
		, @brakePr
		, @battery
		, @loadshed
		, @tractShoreSup
		, @auxShoreSup
		, @carBmCoupled
		, @carBjCoupled
		, @tmcc1
		, @speed
		);

END
GO
