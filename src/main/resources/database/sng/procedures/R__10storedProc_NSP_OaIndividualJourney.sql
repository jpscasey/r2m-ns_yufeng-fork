SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaIndividualJourney', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaIndividualJourney' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaIndividualJourney AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaIndividualJourney]
(
	@TrainPassageID int
)
AS
BEGIN 

	SET NOCOUNT ON;

	DECLARE @JourneyID int = (SELECT JourneyID FROM TrainPassage WHERE ID = @TrainPassageID)

	---------------------------------------------------------------------------
	-- Header information
	---------------------------------------------------------------------------

	SELECT 
		CASE StpType
			WHEN 'O' THEN HeadcodeDesc + ' STP'
			ELSE HeadcodeDesc
		END AS Headcode
		, StartDatetime	
		, ISNULL(va.VehicleNumber, '') + ' - ' + ISNULL(vb.VehicleNumber, '') AS VehicleNumber
		, AnalyseDatetime
	FROM dbo.TrainPassage tp 
	INNER JOIN dbo.Journey ON Journey.ID = JourneyID
	LEFT JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	LEFT JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE tp.ID = @TrainPassageID
	
	---------------------------------------------------------------------------
	-- Load section data into a temporary table
	---------------------------------------------------------------------------
	;WITH CteTrainPassageSection
	AS
	(
		SELECT
			TrainPassageSectionID	= ID 
			, TrainPassageSection.SectionID
			, ActualStartDatetime
			, ActualEndDatetime
			, ActualStartTime		= CAST(ActualStartDatetime AS Time(0)) 
			, ActualEndTime			= CAST(ActualEndDatetime AS Time(0))
			, ActualTime			= DATEDIFF(s, ActualStartDatetime, ActualEndDatetime)
			, TotalAttributed		= (SELECT CAST(SUM(ROUND(DelayDuration / 1000.0, 0)) AS int) FROM dbo.TrainPassageSectionDelayReason WHERE TrainPassageSectionID = TrainPassageSection.ID AND DelayDuration >= 1000)
		FROM dbo.TrainPassageSection
		WHERE TrainPassageID = @TrainPassageID
	)
	SELECT
		SegmentID				= s.SegmentID
		, SectionID				= s.ID
		, ID					= TrainPassageSectionID
		, StartLoc				= StartLocation.Tiploc
		, EndLoc				= EndLocation.Tiploc
		, StartTime				= CAST(s.StartTime AS datetime2(3))
		, EndTime				= CAST(s.EndTime AS datetime2(3))
		, BookedTime			= DATEDIFF(s, CAST(s.StartTime AS datetime2(3)), CAST(s.EndTime AS datetime2(3)))
		, ActualTime			= ActualTime
		, ActualStartDatetime
		, ActualEndDatetime
		, ScheduleDeviationSection = 
				ActualTime - 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) < - 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) + 86400 
					WHEN DATEDIFF(s, s.StartTime, s.EndTime) > 43200
						THEN DATEDIFF(s, s.StartTime, s.EndTime) - 86400 
					ELSE DATEDIFF(s, s.StartTime, s.EndTime)
				END
		, BookedArrivalTime		= s.EndTime
		, ActualArrivalTime		= tps.ActualEndTime
		, ScheduleDeviationCumulative = 
				CASE	-- handling over midnight sections
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) < - 43200 -- -12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) + 86400 -- +24h
					WHEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) > 43200 -- 12h
						THEN DATEDIFF(s, s.EndTime, tps.ActualEndTime) - 86400 -- -24h
					ELSE DATEDIFF(s, s.EndTime, tps.ActualEndTime) 
				END
		, TotalAttributed		= TotalAttributed
		, RecoveryTime			= RecoveryTime
		, OptimalJourneyDate	= CONVERT(date, ir.StartDatetime)
		, OptimalTime			= ir.SectionTime
	INTO #SectionData
	FROM dbo.Section s
	LEFT JOIN dbo.Location AS StartLocation ON StartLocation.ID = s.StartLocationID
	LEFT JOIN dbo.Location AS EndLocation ON EndLocation.ID = s.EndLocationID
	LEFT JOIN CteTrainPassageSection tps ON SectionID = s.ID
	LEFT JOIN dbo.IdealRun ir ON 
		ir.JourneyID = s.JourneyID
		AND ir.SectionID = s.ID
		AND ir.ValidTo IS NULL
	WHERE s.JourneyID = @journeyID
	ORDER BY s.ID	

	---------------------------------------------------------------------------
	-- Main report dataset (calculate segment data, merge with sections, return)
	---------------------------------------------------------------------------
	
	SELECT
		SegmentID	
		, SectionID	
		, ID	
		, StartLoc	
		, EndLoc	
		, BookedTime					= dbo.FN_ConvertSecondToTime(BookedTime)
		, ActualTime					= dbo.FN_ConvertSecondToTime(ActualTime)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(ScheduleDeviationSection)
		, BookedArrivalTime				= CONVERT(char(8), BookedArrivalTime, 108)
		, ActualArrivalTime				= CONVERT(char(8), ActualArrivalTime, 108)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime(ScheduleDeviationCumulative)
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(TotalAttributed)
		, RecoveryTime					= 			
			CASE RecoveryTime
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(RecoveryTime) 
			END
		, OptimalJourneyDate
		, OptimalTime					= dbo.FN_ConvertSecondToTime(OptimalTime)
	FROM #SectionData
	
	UNION ALL
	
	SELECT
		SegmentID	
		, SectionID						= 0
		, ID							= 0
		, StartLoc						= (SELECT TOP 1 StartLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
		, EndLoc						= (SELECT TOP 1 EndLoc FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, BookedTime					= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ActualTime = dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, ScheduleDeviationSection		= dbo.FN_ConvertSecondToTime(
			DATEDIFF(s
				, (SELECT TOP 1 ActualStartDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 ActualEndDatetime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
			- DATEDIFF(s
				, (SELECT TOP 1 StartTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID ASC)
				, (SELECT TOP 1 EndTime FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
			)
		)
		, BookedArrivalTime				= (SELECT TOP 1 CONVERT(char(8), BookedArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ActualArrivalTime				= (SELECT TOP 1 CONVERT(char(8), ActualArrivalTime, 108) FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC)
		, ScheduleDeviationCumulative	= dbo.FN_ConvertSecondToTime((SELECT TOP 1 ScheduleDeviationCumulative FROM #SectionData sd2 WHERE sd1.SegmentID = sd2.SegmentID ORDER BY sd2.SectionID DESC))
		, TotalAttributed				= dbo.FN_ConvertSecondToTime(SUM(TotalAttributed))
		, RecoveryTime					= 			
			CASE SUM(RecoveryTime)
				WHEN 0 THEN '-'
				ELSE dbo.FN_ConvertSecondToTime(SUM(RecoveryTime)) 
			END
		-- THIS IS NOT POPULATED: need more feedback how should it be calculated
		, OptimalJourneyDate			= NULL --MIN(OptimalJourneyDate)	
		, OptimalTime					= dbo.FN_ConvertSecondToTime(SUM(OptimalTime))
	FROM #SectionData sd1
	GROUP BY 		
		SegmentID
	ORDER BY 		
		SegmentID
		, SectionID


	---------------------------------------------------------------------------
	-- Return Delay Breakdown	
	---------------------------------------------------------------------------
	SELECT
		SegmentID			= tps.SegmentID 
		, SectionID			= tps.SectionID
		, LocationStart		= sl.Tiploc
		, LocationEnd		= el.Tiploc
		, TrainPassageSectionID
		, DelayReasonID
		, TotalDuration		= CAST(ROUND(DelayDuration / 1000.0, 0) AS int)
		, Name				= dr.Name
		, ChartColor		= dr.ChartColor
	FROM dbo.TrainPassageSectionDelayReason tpsdr
	INNER JOIN dbo.TrainPassageSection tps ON tps.ID = tpsdr.TrainPassageSectionID
	INNER JOIN dbo.Section s ON s.ID = tps.SectionID AND s.JourneyID = tps.JourneyID
	INNER JOIN dbo.DelayReason dr ON DelayReasonID = dr.ID
	LEFT JOIN dbo.Location sl ON sl.ID = s.StartLocationID
	LEFT JOIN dbo.Location el ON el.ID = s.EndLocationID
		WHERE TrainPassageID = @TrainPassageID
		AND DelayDuration >= 1000

END
GO
