SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaJourneyProfile', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaJourneyProfile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaJourneyProfile AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaJourneyProfile] 
(
	@TrainPassageID int
	, @SectionID int = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Date datetime2(3) = (SELECT StartDatetime FROM dbo.TrainPassage WHERE ID = @TrainPassageID)
	DECLARE @JourneyID int = (SELECT JourneyID FROM dbo.TrainPassage WHERE ID = @TrainPassageID)

	DECLARE @gmtToBstTimeOffset smallint = dbo.FN_GetGmtToBstTimeDiff(CONVERT(varchar(10), @Date, 121) + ' 04:00:00.000')
	DECLARE @bstToGmtTimeOffset smallint = - @gmtToBstTimeOffset

	-- Sections - veritical markers are based on this dataset
	IF OBJECT_ID('tempdb..#Section') IS NOT NULL
		DROP TABLE #Section

	SELECT
		SectionID	= Section.ID
		, IsStation	= 
			CASE
				WHEN SectionPoint.SectionPointType IN ('B', 'S', 'E') 
					THEN 1
				ELSE 0
			END
		, StartLoc	= StartLocation.Tiploc 
		, EndLoc	= EndLocation.Tiploc
		-- if distance cannot be calulcated base on LocationSection table, will be calculated in straight line:
		, Distance	= ISNULL(Distance, dbo.FN_CalculateDistance (StartLocation.Lat, StartLocation.Lng, EndLocation.Lat, EndLocation.Lng, 0))
	INTO #Section
	FROM dbo.Section
	INNER JOIN dbo.SectionPoint ON SectionPoint.ID = Section.ID AND SectionPoint.JourneyID = Section.JourneyID
	LEFT JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.Location EndLocation ON EndLocation.ID = EndLocationID
	WHERE Section.JourneyID = @JourneyID
	ORDER BY Section.ID

	SELECT 
		SectionID
		, IsStation
		, StartLoc 
		, EndLoc
		, Distance
	FROM #Section
		

	-- Ideal Run data
	IF OBJECT_ID('tempdb..#IdealRunData') IS NOT NULL
		DROP TABLE #IdealRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col6 AS CIRSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #IdealRunData
	FROM dbo.Section
	INNER JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	LEFT JOIN dbo.IdealRun ON IdealRun.SectionID = Section.ID AND IdealRun.JourneyID = Section.JourneyID
	LEFT JOIN dbo.IdealRunChannelValue ON IdealRun.ID = IdealRunChannelValue.IdealRunID --ON VehicleID = LeadingVehicleID AND TimeStamp BETWEEN StartDatetime AND DATEADD(s, SectionTime, StartDatetime)
	WHERE Section.JourneyID = @JourneyID 
		AND IdealRun.ValidTo IS NULL
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)

	;WITH CteIdealRunData AS
	(
		SELECT
			SectionID	
			, SectionIDRecordID	
			, CIRSpeed	
			, col3	
			, col4	
			, StartLocationLat	
			, StartLocationLng
			, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
		FROM #IdealRunData 
		WHERE SectionIDRecordID = 1
		
		UNION ALL 
		
		SELECT
			ird.SectionID	
			, ird.SectionIDRecordID	
			, ird.CIRSpeed	
			, ird.col3	
			, ird.col4	
			, ird.StartLocationLat	
			, ird.StartLocationLng
			, CteIdealRunData.SectionDistance + dbo.FN_CalculateDistance (CteIdealRunData.col3, CteIdealRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
		FROM #IdealRunData ird
		INNER JOIN CteIdealRunData ON ird.SectionIDRecordID = CteIdealRunData.SectionIDRecordID + 1 AND ird.SectionID = CteIdealRunData.SectionID
	)
	SELECT 
		SectionID
		, CIRSpeed
		, SectionDistance
	FROM CteIdealRunData
	ORDER BY SectionID
		, SectionIDRecordID
	OPTION (MAXRECURSION 20000)
	
	--Actual Run Data
	--DECLARE @trainPassageID int = (SELECT TOP 1 ID FROM VW_TrainPassageValid WHERE JourneyID = @JourneyID AND CAST(StartDatetime AS date) = @Date)
	
	IF OBJECT_ID('tempdb..#ActualRunData') IS NOT NULL
	DROP TABLE #ActualRunData
		
	SELECT 		
		Section.ID AS SectionID
		,ROW_NUMBER() OVER (PARTITION BY Section.ID ORDER BY Timestamp ASC) AS SectionIDRecordID
		, col4 AS ActualSpeed
		, col3
		, col4
		, StartLocation.Lat AS StartLocationLat
		, StartLocation.Lng AS StartLocationLng
	INTO #ActualRunData
	FROM dbo.Section
	INNER JOIN dbo.Location StartLocation ON StartLocation.ID = StartLocationID
	INNER JOIN dbo.TrainPassageSection ON Section.JourneyID = TrainPassageSection.JourneyID AND Section.ID = SectionID
	LEFT JOIN dbo.VW_TrainPassageValid AS TrainPassage ON TrainPassage.ID = TrainPassageID
	LEFT JOIN dbo.Vehicle v ON v.ID = LeadingVehicleID
	LEFT JOIN dbo.ChannelValue cv ON cv.UnitID = v.UnitID AND TimeStamp BETWEEN DATEADD(hh, @bstToGmtTimeOffset, ActualStartDatetime) AND DATEADD(hh, @bstToGmtTimeOffset, ActualEndDatetime)

	WHERE TrainPassageID = @trainPassageID
		AND ISNULL(col3, 0) > 0 
		AND ISNULL(col4, 0) > -90 
		AND (@SectionID IS NULL OR @SectionID = Section.ID)
	
		
	;WITH CteActualRunData AS
		(
			SELECT
				SectionID	
				, SectionIDRecordID	
				, ActualSpeed	
				, col3	
				, col4	
				, StartLocationLat	
				, StartLocationLng
				, dbo.FN_CalculateDistance (StartLocationLat, StartLocationLng, col3, col4, 0) AS SectionDistance
			FROM #ActualRunData 
			WHERE SectionIDRecordID = 1
			
			UNION ALL 
			
			SELECT
				ird.SectionID	
				, ird.SectionIDRecordID	
				, ird.ActualSpeed	
				, ird.col3	
				, ird.col4	
				, ird.StartLocationLat	
				, ird.StartLocationLng
				, CteActualRunData.SectionDistance + dbo.FN_CalculateDistance (CteActualRunData.col3, CteActualRunData.col4, ird.col3, ird.col4, 0) AS SectionDistance
			FROM #ActualRunData ird
			INNER JOIN CteActualRunData ON ird.SectionIDRecordID = CteActualRunData.SectionIDRecordID + 1 AND ird.SectionID = CteActualRunData.SectionID
		)
		SELECT 
			SectionID
			, ActualSpeed
			, SectionDistance
		FROM CteActualRunData
		ORDER BY SectionID
			, SectionIDRecordID
		OPTION (MAXRECURSION 20000)
					
END
GO
