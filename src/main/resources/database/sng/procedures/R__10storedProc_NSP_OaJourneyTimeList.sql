SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaJourneyTimeList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaJourneyTimeList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaJourneyTimeList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaJourneyTimeList]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	SET @EndDate = DATEADD(d, 1, @EndDate)
	DECLARE @actualJourneyID int
	

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @actualJourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END

	---------------------------------------------------------------------------
	-- Chart data
	---------------------------------------------------------------------------

	SELECT
		TrainPassageID			= tp.ID
		, DateStart				= CAST(tp.StartDatetime AS date)
		--Scotrail:
		, SetFormation			= (SELECT UnitNumber FROM VW_Vehicle WHERE VW_Vehicle.ID = EndAVehicleID) + ' (' + ISNULL(va.VehicleNumber, '') + '/' + ISNULL(vb.VehicleNumber, '') + ')'
		--ECML:
		--, SetFormation			= ISNULL(tp.Setcode, '') + ' / ' + ISNULL(va.VehicleNumber, '') + ' / ' + ISNULL(vb.VehicleNumber, '') 
		, AvgDelayOnArrival		= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledArrival, CAST(ActualArrivalDatetime AS time(0))))) --/ 60 
		, AvgDelayOnDeparture	= AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(S, ScheduledDeparture, CAST(ActualDepartureDatetime AS time(0))))) --/ 60
		, IsIncludedInAnalysis 
	FROM dbo.TrainPassage tp
	INNER JOIN dbo.SectionPoint sp ON sp.JourneyID = tp.JourneyID
	LEFT JOIN dbo.TrainPassageSectionPoint tpsp ON sp.ID = tpsp.SectionPointID AND tpsp.TrainPassageID = tp.ID
	INNER JOIN dbo.Vehicle va ON EndAVehicleID = va.ID
	INNER JOIN dbo.Vehicle vb ON EndBVehicleID = vb.ID
	WHERE SectionPointType = 'E' -- Check delay in Headcode End Location --IN ('B', 'S', 'E')
		AND tp.JourneyID IN (@JourneyID, @actualJourneyID)
		AND tp.StartDatetime >= @StartDate AND tp.StartDatetime < @EndDate 
	GROUP BY 
		tp.ID
		, CAST(tp.StartDatetime AS date)
		, EndAVehicleID
		, va.VehicleNumber
		, vb.VehicleNumber
		, tp.Setcode
		, IsIncludedInAnalysis 
	ORDER BY DateStart DESC
			
END
GO
