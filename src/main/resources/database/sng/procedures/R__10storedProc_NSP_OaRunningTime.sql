SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaRunningTime', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaRunningTime' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaRunningTime AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaRunningTime]
(
	@DateStart date
	, @DateEnd date
	, @LocationIdStart int
	, @LocationIdEnd int
	, @excludeDelays Bit = 0
	, @ServiceGroupID int = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @DateEnd = DATEADD(d, 1, @DateEnd)
	
	SELECT
		JourneyID				= s.JourneyID
		, JourneySegmenID		= s.ID
		, Headcode				= j.Headcode 
		, HeadcodeDescription	= j.HeadcodeDesc + ' ' + CONVERT(char(5), j.StartTime, 121)
		, Diagram				= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + jsl.Tiploc
			+ ' - ' + jel.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), j.EndTime, 108), ':', ''), 4) + 10000, 4)
		, NoOfPassages		= COUNT(*)
		, BookedTime		= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(S, s.StartTime, s.EndTime))))
		, RecoveryTime		= dbo.FN_ConvertSecondToTime((
				SELECT SUM(RecoveryTime)
				FROM Section sc
				WHERE sc.SegmentID = s.ID
					AND sc.JourneyID = s.JourneyID
			)
		)

		-- OptimalTime - calculation should be simplified:
		, OptimalTime		= MIN(OptimalTrainPassage.OptimalTime) -- There is only 1 record so MIN/MAX grouping function doesnt matter
		, ActualMin			= dbo.FN_ConvertSecondToTime(MIN(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMax			= dbo.FN_ConvertSecondToTime(MAX(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
		, ActualMean		= dbo.FN_ConvertSecondToTime(AVG(dbo.FN_ValidateTimeDiff(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd))))
	
		, TrainPassageIdOptimal				= MIN(OptimalTrainPassage.TrainPassageID)	-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMin			= MIN(MinTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		, TrainPassageIdActualMax			= MIN(MaxTrainPassage.TrainPassageID)		-- There is only 1 record so MIN/MAX grouping function doesnt matter
		
	FROM dbo.Segment s 
	INNER JOIN dbo.Journey j ON j.ID = s.JourneyID
	INNER JOIN dbo.TrainPassageSegment tps ON 
		s.JourneyID = tps.JourneyID
		AND s.ID = tps.SegmentID
	INNER JOIN dbo.Location jsl ON jsl.ID = j.StartLocationID
	LEFT JOIN dbo.Location jel ON jel.ID = j.EndLocationID
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM dbo.TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) ASC
	) AS MinTrainPassage
	OUTER APPLY (
		SELECT TOP 1 TrainPassageID
		FROM dbo.TrainPassageSegment tps1
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd) DESC
	) AS MaxTrainPassage
	OUTER APPLY (
		SELECT TOP 1 
			TrainPassageID
			, OptimalTime = dbo.FN_ConvertSecondToTime(DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd))
		FROM dbo.TrainPassageSegment tps1
		INNER JOIN dbo.Segment s1 ON
			s1.JourneyID = tps1.JourneyID
			AND s1.ID = tps1.SegmentID
		WHERE tps1.JourneyID = s.JourneyID 
			AND tps1.SegmentID = s.ID
			AND tps1.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		ORDER BY
			ABS(
				DATEDIFF(S, s1.StartTime, s1.EndTime) -- booked time
				- DATEDIFF(s, tps1.ActualDatetimeStart, tps1.ActualDatetimeEnd)
			) ASC
	) AS OptimalTrainPassage
	
	WHERE 
		s.StartLocationID = @LocationIdStart
		AND s.EndLocationID = @LocationIdEnd
		AND tps.ActualDatetimeStart BETWEEN @DateStart AND @DateEnd
		AND ( @excludeDelays = 0 OR 
				(DATEDIFF(s, ActualDatetimeStart, ActualDatetimeEnd)
				- DATEDIFF(S, s.StartTime, s.EndTime)) -- booked time
			 < 180)
	GROUP BY 
		s.ID
		, s.JourneyID
		, j.Headcode 
		, j.HeadcodeDesc
		, j.StartTime
		, j.EndTime
		, jsl.Tiploc
		, jel.Tiploc

END
GO
