SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_RuleEngineUpdateCurrentIds', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_RuleEngineUpdateCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_RuleEngineUpdateCurrentIds AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_RuleEngineUpdateCurrentIds]
	@ConfigurationName varchar(100),
	@Value1 bigint = NULL,
	@Value2 bigint = NULL,
	@Value3 bigint = NULL

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LastUpdateTime datetime2(3);
	SET @LastUpdateTime = SYSDATETIME()

	;MERGE RuleEngineCurrentId AS r
	USING (

		SELECT Position = '1', ConfigurationName = @ConfigurationName, Value = @Value1, LastUpdateTime = @LastUpdateTime
		UNION
		SELECT Position = '2', ConfigurationName = @ConfigurationName, Value = @Value2, LastUpdateTime = @LastUpdateTime
		UNION
		SELECT Position = '3', ConfigurationName = @ConfigurationName, Value = @Value3, LastUpdateTime = @LastUpdateTime

	) AS nr
	ON r.Position = nr.Position
		AND r.ConfigurationName = nr.ConfigurationName
	WHEN MATCHED THEN
	UPDATE SET
		Value = nr.Value,
		LastUpdateTime = nr.LastUpdateTime
	WHEN NOT MATCHED THEN
	INSERT (
		ConfigurationName
		, Position
		, Value
		, LastUpdateTime)
	VALUES(
		nr.ConfigurationName
		, nr.Position
		, nr.Value
		, nr.LastUpdateTime);

END
GO
