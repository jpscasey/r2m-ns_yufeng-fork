SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SimulateSLTChannelValues', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SimulateSLTChannelValues' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SimulateSLTChannelValues AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SimulateSLTChannelValues]
AS
BEGIN

	SET NOCOUNT ON;

	-- will select a randon location from table
	-- will select a randon number of randon channels and call proc to insert channelvalue
	DECLARE @Lower int
	DECLARE @Upper int
	DECLARE @UnitId int 
	DECLARE @lat decimal (9,6)
	DECLARE @lng decimal (9,6)
	DECLARE @EventValue int
	DECLARE @LocationIdRnd int
	DECLARE @ChannelEventIDRnd int
	DECLARE @SpeedRnd int
	DECLARE @TimestampEndTime datetime2(3)
	DECLARE @Timestamp datetime2(3)
	DECLARE @NumberOfChannelsRnd int
	DECLARE @StringSql nvarchar(max)

	SELECT TOP 1 @UnitId = UnitId, @Timestamp = Timestamp
	FROM EventChannelValue
	ORDER BY TimeStamp desc

	-- decide hoe many channels will be passed
	SET @Lower = 10
	SET @Upper = 50
	SELECT @NumberOfChannelsRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

	-- randon speeed
	SET @Lower = 0
	SET @Upper = 120
	SELECT @SpeedRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	-- decide location many channels will be passed
	SET @Lower = (SELECT MIN (ID) from dbo.Location)
	SET @Upper = (SELECT MAx (ID) from dbo.Location)
	SELECT @LocationIdRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))
	
	SELECT	@lat = lat,
			@lng = lng
	FROM dbo.Location
	WHERE id = @LocationIdRnd

	SET @StringSql = ''

	WHILE @NumberOfChannelsRnd > 0
	BEGIN

		SET @Lower = 5
		SET @Upper = 663
		SELECT @ChannelEventIDRnd = FLOOR(@Lower + RAND() * (@Upper - @Lower + 1))

		SET @Lower = 0
		SET @Upper = 1
		SELECT @EventValue = convert(bit,FLOOR(@Lower + RAND() * (@Upper - @Lower + 1)))

		If CHARINDEX ('col' + convert(varchar(5), @ChannelEventIDRnd)+'=' , @StringSql) < 1 -- so I do not repeat the colum name
			SET @StringSql = @StringSql + ', @col' + convert(varchar(5), @ChannelEventIDRnd) + '=' + convert(varchar(1), @EventValue)


		SET @NumberOfChannelsRnd = @NumberOfChannelsRnd - 1

	END

	SELECT @StringSql = 'EXEC [dbo].[NSP_InsertChannelValue] @Timestamp = ''' + Convert(varchar(19),@Timestamp) + ''', @UnitID=' + convert(varchar(5), @UnitID) + ', @UpdateRecord=0, @col1 = ' + convert(varchar(10), @lat) + ', @col2 = ' + convert(varchar(10), @lng) + ', @col4 = ' + convert(varchar(10), @SpeedRnd) + @StringSql 

	EXEC sp_executesql @StringSql
END
GO
