SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_SimulateSLTClosingEvent', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_SimulateSLTClosingEvent' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_SimulateSLTClosingEvent AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_SimulateSLTClosingEvent]
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UnitId int
	DECLARE @ChannelId int
	DECLARE @TimeStamp datetime2
	DECLARE @TimestampEndTime datetime2 = SYSDATETIME()

	SELECT	TOP 1 @UnitId = UnitID,
			@ChannelId = ChannelId,
			@TimeStamp = TimeStamp
	FROM dbo.EventChannelValue
	WHERE TimeStampEndTime is null

	IF @@ROWCOUNT > 0
		EXEC [dbo].NSP_InsertEventChannelValue @Timestamp = @Timestamp, @TimestampEndTime = @TimestampEndTime, @UnitId = @UnitId, @ChannelId = @ChannelId, @Value = 0
END
GO
