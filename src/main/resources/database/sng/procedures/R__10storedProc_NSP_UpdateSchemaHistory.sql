SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateSchemaHistory', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateSchemaHistory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
    EXEC ('CREATE PROCEDURE dbo.NSP_UpdateSchemaHistory AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_UpdateSchemaHistory]
AS
BEGIN   
    -- r2m-combine schema builder
    DECLARE @schema varchar(max)

    SET @schema = '{'
    SELECT @schema += 'col' + CAST(EngColumnNo AS varchar(10)) 
                    + ' ' + 
                    CASE
                        WHEN DataType = 'bit' THEN 'boolean'
                        WHEN DataType = 'int' THEN 'int32'
                        WHEN DataType = 'smallint' THEN 'int32'
                        WHEN DataType = 'tinyint' THEN 'int32'
                        WHEN DataType = 'bigint' AND MinValue < 0 THEN 'int64'
                        WHEN DataType = 'bigint' AND MinValue >= 0 THEN 'uint64'
                        WHEN DataType LIKE '%datetime%' then 'uint64'
                        WHEN DataType LIKE '%time%' then 'string'
                        WHEN DataType LIKE '%char%' then 'string'
                        ELSE 'float64' 
                    END
                    + ' nullable'
                    + ','
    FROM dbo.Channel

    -- remove trailing comma
    SET @schema = STUFF(@schema, LEN(@schema), 1, '') + '}'

    -- update sbo.SchemaHistory
    DECLARE @schema_hash int = CHECKSUM(@schema)

    IF NOT EXISTS (SELECT * FROM dbo.SchemaHistory WHERE schema_hash = @schema_hash) 
        BEGIN
            UPDATE dbo.SchemaHistory SET ValidTo = SYSDATETIME() WHERE ValidTo IS NULL
            INSERT INTO dbo.SchemaHistory (ChannelSchema, ValidFrom) VALUES (@schema, SYSDATETIME())
        END

END
GO