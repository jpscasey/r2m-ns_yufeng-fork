SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_FleetStatus', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_FleetStatus' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_FleetStatus AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_FleetStatus]
AS
SELECT
	ID 
	,UnitID
	,UnitNumber
	,UnitType
	,UnitPosition
	,UnitOrientation
	,FleetFormationID
	,Diagram
	,Headcode
	,Loading
	,VehicleList
	,VehicleTypeID
	,LocationIDHeadcodeStart
	,LocationIDHeadcodeEnd
    ,FleetCode
	,UnitStatus
	,ActiveCab
FROM VW_IX_FleetStatus --WITH (NOEXPAND)
GO
