SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_ReportAdhesion', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_ReportAdhesion' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_ReportAdhesion AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_ReportAdhesion] 
AS
SELECT
    TimeRange	= CASE
        WHEN Timestamp < DATEADD(n, -1440, SYSDATETIME()) THEN 3
        WHEN Timestamp < DATEADD(n, -360, SYSDATETIME()) THEN 2
		ELSE 1
    END
    , WSP		= Value
	, GCourse
	, Latitude
	, Longitude
	, LocationCode = (
		SELECT TOP 1 LocationCode --CRS
		FROM Location
		INNER JOIN LocationArea ON LocationID = Location.ID
		WHERE Latitude BETWEEN MinLatitude AND MaxLatitude
			AND Longitude BETWEEN MinLongitude AND MaxLongitude
			AND LocationArea.Type = 'C'
		ORDER BY Priority
	)
	, Timestamp		= CONVERT(datetime2(3), Timestamp)
	, v.VehicleNumber
FROM ReportAdhesion AS ra
INNER JOIN Vehicle AS v ON ra.VehicleID = v.ID
WHERE TimeStamp >= DATEADD(n, -7*1440, SYSDATETIME())
	AND EventType = 1 -- WSP
GO
