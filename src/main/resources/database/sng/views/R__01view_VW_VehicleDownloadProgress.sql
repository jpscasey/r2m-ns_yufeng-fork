SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter view dbo.VW_VehicleDownloadProgress', 0, 1) WITH NOWAIT
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'VW_VehicleDownloadProgress' AND TABLE_TYPE = 'VIEW')
BEGIN
	EXEC ('CREATE VIEW dbo.VW_VehicleDownloadProgress AS SELECT 1 AS X;')
END
GO
ALTER VIEW [dbo].[VW_VehicleDownloadProgress]
AS
SELECT 
	[ID]
	,[VehicleID]
	,[VehicleDownloadID]
	,[VehicleEventID]
	,[RecordInsert]
	,[EventCategory]
	,[EventCode]
	,[IsProcessed]
	,[StatusUpdated]
	,[StatusUpdatedDatetime]
	,[DetailedStatusDesc] = CASE
		WHEN EventCategory = 24 AND EventCode = 0 THEN '[INFO] RMD - Connected to TMS'
		WHEN EventCategory = 24 AND EventCode = 1 THEN '[FAILED] RMD - Connect timed out'
		WHEN EventCategory = 24 AND EventCode = 2 THEN '[IN PROGRESS] RMD - Manual download request'
		WHEN EventCategory = 24 AND EventCode = 3 THEN '[INFO] RMD - Auto download request'
		WHEN EventCategory = 24 AND EventCode = 4 THEN '[IN PROGRESS] RMD - Download completed ok'
		WHEN EventCategory = 24 AND EventCode = 5 THEN '[FAILED] RMD - Download timed out'
		WHEN EventCategory = 24 AND EventCode = 6 THEN '[FAILED] RMD - Download failed'
		WHEN EventCategory = 24 AND EventCode = 7 THEN '[FAILED] RMD - Got a download command and TMS log not enabled'
		WHEN EventCategory = 24 AND EventCode = 8 THEN '[FAILED] RMD - ATMS Buffer Overrun'

		WHEN EventCategory = 255 AND EventCode = 32 THEN '[FAILED] PL - File upload failed: CRC Error'
		WHEN EventCategory = 255 AND EventCode = 33 THEN '[FAILED] PL - File upload failed: Timeout'
		WHEN EventCategory = 255 AND EventCode = 34 THEN '[FAILED] PL - File upload failed: Aborted'
		WHEN EventCategory = 255 AND EventCode = 35 THEN '[FAILED] PL - File upload failed: Unknown'
		WHEN EventCategory = 255 AND EventCode = 36 THEN '[IN PROGRESS] PL - FileTransfer OK'

		WHEN EventCategory = 1000 AND EventCode = 0 THEN '[COMPLETED] File load - File load completed'
		WHEN EventCategory = 1000 AND EventCode = 1 THEN '[FAILED] File load - File load failed'

		WHEN EventCategory = 1001 AND EventCode = 0 THEN '[IN PROGRESS] SQLiteUpdate - Update OK'
		WHEN EventCategory = 1001 AND EventCode = 1 THEN '[FAILED] SQLiteUpdate - Failed'

	END
FROM [dbo].[VehicleDownloadProgress]
GO
