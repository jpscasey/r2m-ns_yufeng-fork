CALL doPrepare.bat

IF ERRORLEVEL 1 (
    ECHO ERROR - Environment configuration incorrect
    EXIT /B 1
)

SET FW_BASELINE_VERSION=20171001.010101

ECHO INFO - Baselining %FW_ENVIRONMENT% at %FW_URL%

flyway -url=%FW_URL%^
    -user=%FW_USERNAME%^
    -password=%FW_PASSWORD%^
    -baselineVersion=%FW_BASELINE_VERSION%^
    -mixed=true^
    -outOfOrder=true^
    -cleanDisabled=true^
    baseline
