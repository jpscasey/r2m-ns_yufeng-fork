SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_CalculateDistance', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_CalculateDistance' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_CalculateDistance () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_CalculateDistance]
	(@LatitudeA 	float = NULL, 
	 @LongitudeA 	float = NULL,
	 @LatitudeB 	float = NULL, 
	 @LongitudeB 	float = NULL,
	 @InKilometers	BIT = 0
	 )
RETURNS float
AS
BEGIN
	
	DECLARE @Distance FLOAT

	SET @Distance = (SIN(RADIANS(@LatitudeA)) *
              SIN(RADIANS(@LatitudeB)) +
              COS(RADIANS(@LatitudeA)) *
              COS(RADIANS(@LatitudeB)) *
              COS(RADIANS(@LongitudeA - @LongitudeB)))

	--Get distance in miles
	IF @Distance > 1 
		SET @Distance = 1
	
	IF @Distance < -1
		SET @Distance = -1
	
  	SET @Distance = (DEGREES(ACOS(@Distance))) * 69.09

	--If specified, convert to kilometers
	IF @InKilometers = 1
		SET @Distance = @Distance * 1.609344 

	RETURN @Distance

END
GO
