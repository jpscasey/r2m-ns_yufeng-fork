SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_CamelCase', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_CamelCase' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_CamelCase () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_CamelCase](
	@Str varchar(8000)
)
RETURNS varchar(8000) AS
BEGIN
	DECLARE @Result varchar(2000)

	SET @Str = LOWER(@Str) + ' '

	SET @Result = ''

	WHILE 1=1
	BEGIN

		IF PATINDEX('%[ ,_,-,/,.]%',@Str) = 0 BREAK

		SET @Result = @Result + UPPER(Left(@Str, 1)) + SUBSTRING(@Str, 2, PATINDEX('%[ ,_,-,/,.]%', @Str) - 1)
		SET @Str = SUBSTRING(@Str, PATINDEX('%[ ,_,-,/,.]%', @Str) + 1, LEN(@Str))

	END

	SET @Result = Left(@Result,Len(@Result))

	RETURN @Result

END
GO
