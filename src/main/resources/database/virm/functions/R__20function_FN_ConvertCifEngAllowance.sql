SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_ConvertCifEngAllowance', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_ConvertCifEngAllowance' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_ConvertCifEngAllowance () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_ConvertCifEngAllowance]
(
	@CifEngAllowance char(2)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @engAllowanceSec int
	
	SET @CifEngAllowance = RIGHT(' ' + RTRIM(@CifEngAllowance),2)

	SET @engAllowanceSec = CASE RIGHT(@CifEngAllowance, 1)
		WHEN 'H' THEN CAST(SUBSTRING(@CifEngAllowance, 1, 1) AS int) * 60 + 30
		ELSE CAST(@CifEngAllowance AS int) * 60
	END

	RETURN @engAllowanceSec
END
GO
