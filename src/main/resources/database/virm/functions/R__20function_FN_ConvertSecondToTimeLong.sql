SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_ConvertSecondToTimeLong', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_ConvertSecondToTimeLong' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_ConvertSecondToTimeLong () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_ConvertSecondToTimeLong]
(
	@NumberOfSeconds int
)
RETURNS varchar(8)
AS
BEGIN
	DECLARE @string char(8)

	SET @string = CONVERT(char(8), DATEADD(s, @NumberOfSeconds, 0), 108)
	
	IF @NumberOfSeconds IS NULL OR @string IS NULL
		SET @string = '-'
		
	-- Return the result of the function
	RETURN @string

END
GO
