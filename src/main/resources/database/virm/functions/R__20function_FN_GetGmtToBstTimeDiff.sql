SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GetGmtToBstTimeDiff', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GetGmtToBstTimeDiff' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GetGmtToBstTimeDiff () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_GetGmtToBstTimeDiff] 
(
	@GmtDatetime datetime2(3)
)
RETURNS smallint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @timeOffset smallint
	

	SET @timeOffset = (SELECT TOP 1 TimeOffset FROM GmtToBstConversion WHERE @GmtDatetime BETWEEN GmtDatetimeFrom AND GmtDatetimeTo)

	RETURN @timeOffset

END
GO
