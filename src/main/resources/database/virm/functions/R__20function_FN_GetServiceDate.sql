SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_GetServiceDate', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_GetServiceDate' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_GetServiceDate () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_GetServiceDate] 
(	
	@DateRunsFrom date
	, @DateRunsTo date
	, @DaysRun char(7)
)
RETURNS 
@result TABLE 
(
	DateValue date
)
AS
BEGIN

	;WITH CteDate AS
	(
		SELECT @DateRunsFrom DateValue
		UNION ALL
		SELECT DATEADD(d, 1, DateValue)
		FROM CteDate   
		WHERE DATEADD(d, 1, DateValue) <= @DateRunsTo
	)
	INSERT INTO @result (
		DateValue)
	SELECT DateValue
	FROM CteDate
	WHERE 
		-- Function uses DATENAME (not DATEPART) to be independenf from server setting DATEFIRST
		(DATENAME(WEEKDAY, DateValue) = 'Monday'		AND SUBSTRING(@DaysRun, 1,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Tuesday'	AND SUBSTRING(@DaysRun, 2,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Wednesday'	AND SUBSTRING(@DaysRun, 3,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Thursday'	AND SUBSTRING(@DaysRun, 4,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Friday'	AND SUBSTRING(@DaysRun, 5,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Saturday'	AND SUBSTRING(@DaysRun, 6,1) = '1')
		 OR (DATENAME(WEEKDAY, DateValue) = 'Sunday'	AND SUBSTRING(@DaysRun, 7,1) = '1')
	OPTION (MAXRECURSION 366)
	
	RETURN 
END
GO
