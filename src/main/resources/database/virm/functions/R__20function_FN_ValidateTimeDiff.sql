SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter function dbo.FN_ValidateTimeDiff', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='FN_ValidateTimeDiff' AND ROUTINE_TYPE = 'FUNCTION' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE FUNCTION dbo.FN_ValidateTimeDiff () RETURNS INT AS BEGIN RETURN(1) END;')
END
GO

ALTER FUNCTION [dbo].[FN_ValidateTimeDiff]
(
	@DiffInSeconds int
)
RETURNS int
AS
BEGIN
	DECLARE @validatedInt int
	
	
	SET @validatedInt =	CASE
			WHEN @DiffInSeconds < - 43200
				THEN @DiffInSeconds + 86400 
			WHEN @DiffInSeconds > 43200
				THEN @DiffInSeconds - 86400 
			ELSE @DiffInSeconds
		END
		
	RETURN @validatedInt

END
GO
