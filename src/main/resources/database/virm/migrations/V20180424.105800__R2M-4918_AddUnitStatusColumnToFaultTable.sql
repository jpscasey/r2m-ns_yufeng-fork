SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update Fault table
------------------------------------------------------------

RAISERROR ('-- add UnitStatus column in Fault table', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Fault' AND COLUMN_NAME='UnitStatus')
BEGIN
    ALTER TABLE [dbo].[Fault]
      ADD UnitStatus varchar(20) NULL
END
GO