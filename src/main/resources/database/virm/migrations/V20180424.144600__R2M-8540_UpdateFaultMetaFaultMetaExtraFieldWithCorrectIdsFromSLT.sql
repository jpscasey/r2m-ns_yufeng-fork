/*
--Script caused errors on other environments due to hardcoding DB names as some DB names weren't included
--Script was run successfully on the necessary environments and is no longer required to run
--See R2M-8651 for details
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update FaultMeta, FaultMetaExtraField, Fault tables for
-- all fleets with correct FaultMetaId's in accordance with
-- SLT database
------------------------------------------------------------

RAISERROR ('-- Update FaultMeta, FaultMetaExtraField, Fault table for VIRM', 0, 1) WITH NOWAIT
GO

BEGIN TRAN 

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FaultMeta_bak_replace_VIRM'))
BEGIN
    DROP TABLE FaultMeta_bak_replace_VIRM;
    DROP TABLE FaultMetaExtraField_bak_replace_VIRM;
END

DECLARE @SLT_DBNAME varchar(100)
SELECT @SLT_DBNAME = name FROM sys.databases where name = 'NedTrain_Spectrum_SLT' OR name = 'NS_Dev_Spectrum' OR name = 'NS_Test_Spectrum'
DECLARE @query varchar(max)

ALTER TABLE dbo.FaultMeta NOCHECK CONSTRAINT ALL  
ALTER TABLE dbo.Fault NOCHECK CONSTRAINT ALL    

SELECT * INTO FaultMeta_bak_replace_VIRM FROM FaultMeta
SELECT * INTO FaultMetaExtraField_bak_replace_VIRM FROM FaultMetaExtraField

DELETE FROM dbo.FaultMetaExtraField
DELETE FROM dbo.FaultMeta

SET IDENTITY_INSERT FaultMeta ON
SET @query = '
	INSERT INTO dbo.FaultMeta (ID,FaultCode,Description,Summary,CategoryID,RecoveryProcessPath,FaultTypeID,Url,GroupID,AdditionalInfo,Username,HasRecovery,FromDate,ToDate,ParentID)
	SELECT ID,FaultCode,Description,Summary,CategoryID,RecoveryProcessPath,FaultTypeID,Url,GroupID,AdditionalInfo,Username,HasRecovery,FromDate,ToDate,ParentID
	FROM ' + @SLT_DBNAME + '.dbo.FaultMeta'
EXEC( @query)

SET IDENTITY_INSERT FaultMeta OFF

SET @query = '
	INSERT INTO dbo.FaultMetaExtraField (FaultMetaId,Field,Value)
	SELECT FaultMetaId,Field,Value FROM ' + @SLT_DBNAME + '.dbo.FaultMetaExtraField'
EXEC( @query)

UPDATE f SET f.faultMetaID = fm.ID 
FROM Fault f 
INNER JOIN FaultMeta_bak_replace_VIRM fm_old  ON f.FaultMetaID = fm_old.ID 
INNER JOIN dbo.FaultMeta fm ON fm.FaultCode = fm_old.FaultCode 
AND (
(fm.ParentID is null AND fm_old.ParentID is null)
OR
(SELECT FaultCode FROM FaultMeta fm_parent_new where fm_parent_new.ID = fm.ParentID) = (SELECT FaultCode FROM FaultMeta_bak_replace_VIRM fm_parent_old where fm_parent_old.ID = fm_old.ParentID)
)

ALTER TABLE dbo.FaultMeta WITH CHECK CHECK CONSTRAINT ALL;
ALTER TABLE dbo.Fault WITH CHECK CHECK CONSTRAINT ALL;

DROP TABLE FaultMeta_bak_replace_VIRM;
DROP TABLE FaultMetaExtraField_bak_replace_VIRM;

COMMIT

GO
*/