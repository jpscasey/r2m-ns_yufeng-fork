SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Create gotcha schema
------------------------------------------------------------

RAISERROR ('-- Create gotcha schema', 0, 1) WITH NOWAIT
GO



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Unit' AND COLUMN_NAME='UnitSeriesID')
BEGIN

	DROP INDEX [IX_Unit_UnitSeriesID] ON [dbo].[Unit]
	ALTER TABLE [dbo].[Unit] DROP CONSTRAINT [FK_UnitSeries_Unit]
	ALTER TABLE Unit DROP COLUMN UnitSeriesID 
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SpeedCategory')
    DROP TABLE [dbo].SpeedCategory
GO
--------------------- LimitsMeasurement ------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LimitsMeasurement')
    DROP TABLE [dbo].LimitsMeasurement
GO
--------------------- LimitsCategory ------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LimitsCategory')
    DROP TABLE [dbo].LimitsCategory
GO
--------------------- GotchaLevel ------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'GotchaLevel')
    DROP TABLE [dbo].GotchaLevel
GO
--------------------- Unit Series ------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'UnitSeries')
    DROP TABLE [dbo].UnitSeries
GO


--------------------- Component tree history -----------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ComponentTreeHistory')
    DROP TABLE [dbo].ComponentTreeHistory
GO
--------------------- Wheel Measurement ----------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'WheelMeasurement')
    DROP TABLE [dbo].WheelMeasurement
GO
--------------------- Tag ----------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Tag')
    DROP TABLE [dbo].Tag
GO
------------------------ Component ------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Component')
    DROP TABLE [dbo].Component
GO
-------------------------- Component Type -----------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ComponentType')
    DROP TABLE [dbo].ComponentType
GO

-------------------------- Component Type -----------------------

CREATE TABLE [dbo].[ComponentType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Level] [tinyint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Type] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ComponentType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------ Component ------------------------------

CREATE TABLE [dbo].[Component](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponentTypeId] [int] NOT NULL,
	[ParentId] [int],
	[VehicleId] [int],
	[UnitId] [int],
	[Position] [smallint] NOT NULL,
 CONSTRAINT [PK_Component] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Component]  WITH CHECK ADD CONSTRAINT [FK_ComponentType_Component] FOREIGN KEY(ComponentTypeId)
REFERENCES [dbo].[ComponentType] (ID)
GO

ALTER TABLE [dbo].[Component]  WITH CHECK ADD CONSTRAINT [FK_Component_Component] FOREIGN KEY(ParentId)
REFERENCES [dbo].[Component] (ID)
GO

ALTER TABLE [dbo].[Component]  WITH CHECK ADD CONSTRAINT [FK_Vehicle_Component] FOREIGN KEY(VehicleId)
REFERENCES [dbo].[Vehicle] (ID)
GO

ALTER TABLE [dbo].[Component]  WITH CHECK ADD CONSTRAINT [FK_Unit_Component] FOREIGN KEY(UnitId)
REFERENCES [dbo].[Unit] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_Component_ParentId] ON [dbo].[Component]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_Component_VehicleId] ON [dbo].[Component]
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_Component_UnitId] ON [dbo].[Component]
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_Component_ComponentTypeId] ON [dbo].[Component]
(
	[ComponentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

--------------------- Component tree history -----------------------

CREATE TABLE [dbo].[ComponentTreeHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponentId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[VehicleId] [int],
	[UnitId] [int],
	[ValidFrom] datetime2(3) NOT NULL ,
	[ValidTo] datetime2(3) NOT NULL,
	[Position] [smallint] NOT NULL,
 CONSTRAINT [PK_ComponentTreeHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ComponentTreeHistory]  WITH CHECK ADD CONSTRAINT [FK_Component_ComponentTreeHistory] FOREIGN KEY(ComponentId)
REFERENCES [dbo].[Component] (ID)
GO

ALTER TABLE [dbo].[ComponentTreeHistory]  WITH CHECK ADD CONSTRAINT [FK_ParentComponent_ComponentTreeHistory] FOREIGN KEY(ParentId)
REFERENCES [dbo].[Component] (ID)
GO

ALTER TABLE [dbo].[ComponentTreeHistory]  WITH CHECK ADD CONSTRAINT [FK_Vehicle_ComponentTreeHistory] FOREIGN KEY(VehicleId)
REFERENCES [dbo].[Vehicle] (ID)
GO

ALTER TABLE [dbo].[ComponentTreeHistory]  WITH CHECK ADD CONSTRAINT [FK_Unit_ComponentTreeHistory] FOREIGN KEY(UnitId)
REFERENCES [dbo].[Unit] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_ComponentTreeHistory_ComponentId] ON [dbo].[ComponentTreeHistory]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_ComponentTreeHistory_ParentId] ON [dbo].[ComponentTreeHistory]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_ComponentTreeHistory_VehicleId] ON [dbo].[ComponentTreeHistory]
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_ComponentTreeHistory_UnitId] ON [dbo].[ComponentTreeHistory]
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

--------------------- Wheel Measurement ----------------------------

CREATE TABLE [dbo].[WheelMeasurement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ComponentId] [int] NOT NULL,
	[Timestamp] datetime2(3) NOT NULL ,
	[RecordInserted] datetime2(3) NOT NULL ,
	[Mean] decimal(7,2) NOT NULL ,
	[Peak] decimal(7,2) NOT NULL ,
	[RMSLow] decimal(7,2) NOT NULL ,
	[RMSHigh] decimal(7,2) NOT NULL ,
	[WheelFlat] decimal(7,2) NOT NULL ,
	[OutOfRound] decimal(7,2) NOT NULL ,
	[ValidWDD] decimal(7,2) NOT NULL ,
	[Valid] [bit] NOT NULL,
	
 CONSTRAINT [PK_WheelMeasurement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WheelMeasurement]  WITH CHECK ADD CONSTRAINT [FK_Component_WheelMeasurement] FOREIGN KEY(ComponentId)
REFERENCES [dbo].[Component] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_WheelMeasurement_ComponentId] ON [dbo].[WheelMeasurement]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO


--------------------- Unit Series ------------------------
CREATE TABLE [dbo].[UnitSeries](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AxleCount] [int] NOT NULL,
	[UnitType] [varchar](16) NOT NULL,
	[UnitNumberStart] [int] NOT NULL,
	[UnitNumberEnd] [int] NOT NULL,
	
 CONSTRAINT [PK_VehicleSerie] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO


--------------------- Speed category ------------------------

CREATE TABLE [dbo].[SpeedCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnitSeriesId] [int] NOT NULL,
	[CategoryType] [int] NOT NULL,
	[SpeedFrom] decimal(6,2) NOT NULL,
	[SpeedTo] decimal(6,2) NOT NULL,
	[Valid] [bit] NOT NULL,
	
 CONSTRAINT [PK_SpeedCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SpeedCategory]  WITH CHECK ADD CONSTRAINT [FK_UnitSeries_SpeedCategory] FOREIGN KEY(UnitSeriesId)
REFERENCES [dbo].[UnitSeries] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_SpeedCategory_UnitSeriesId] ON [dbo].[SpeedCategory]
(
	[UnitSeriesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

--------------------- LimitsCategory ------------------------

CREATE TABLE [dbo].[LimitsCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](10) NOT NULL,
 CONSTRAINT [PK_LimitsCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

--------------------- LimitsMeasurement ------------------------
CREATE TABLE [dbo].[LimitsMeasurement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UnitSeriesID] [int] NOT NULL,
	[SpeedCategoryType] [int] NOT NULL,
	[LimitsCategoryID] [int] NOT NULL,
	[Limit0] [int] NOT NULL,
	[Limit1] [int] NOT NULL,
	[Limit2] [int] NOT NULL,
	[Limit3] [int] NOT NULL,
	
 CONSTRAINT [PK_LimitsMeasurement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LimitsMeasurement]  WITH CHECK ADD CONSTRAINT [FK_UnitSeries_LimitsMeasurement] FOREIGN KEY(UnitSeriesID)
REFERENCES [dbo].[UnitSeries] (ID)
GO
ALTER TABLE [dbo].[LimitsMeasurement]  WITH CHECK ADD CONSTRAINT [FK_LimitsCategory_LimitsMeasurement] FOREIGN KEY(LimitsCategoryID)
REFERENCES [dbo].[LimitsCategory] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_LimitsMeasurement_UnitSeriesId] ON [dbo].[LimitsMeasurement]
(
	[UnitSeriesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_LimitsMeasurement_LimitsCategoryID] ON [dbo].[LimitsMeasurement]
(
	[LimitsCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO


--------------------- GotchaLevel ------------------------
CREATE TABLE [dbo].[GotchaLevel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponentID] [int] NOT NULL,
	[GotchaLevel] [int] NOT NULL,
	[ValidFrom] datetime2(3) NOT NULL,
	[ValidTo] datetime2(3),
	
 CONSTRAINT [PK_GotchaLevel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GotchaLevel]  WITH CHECK ADD CONSTRAINT [FK_Component_GotchaLevel] FOREIGN KEY(ComponentID)
REFERENCES [dbo].[Component] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_GotchaLevel_ComponentID] ON [dbo].[GotchaLevel]
(
	[ComponentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

--------------------- Tag ----------------------------
CREATE TABLE [dbo].[Tag](
	[ID] [int] NOT NULL,
	[ComponentId] [int] NOT NULL,
	[Position] char(1) NOT NULL,
	
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tag]  WITH CHECK ADD CONSTRAINT [FK_Component_Tag] FOREIGN KEY(ComponentID)
REFERENCES [dbo].[Component] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_Tag_ComponentID] ON [dbo].[Tag]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

--------------------- TagReader -----------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TagReader')
    DROP TABLE [dbo].TagReader
GO
CREATE TABLE [dbo].[TagReader](
	[ID] [int] NOT NULL,
	[Position] char(1) NOT NULL,
 CONSTRAINT [PK_TagReader] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO


RAISERROR ('-- Add AxleCount to VehicleType', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VehicleType' AND COLUMN_NAME='AxleCount')
BEGIN
	ALTER TABLE VehicleType ADD AxleCount int NULL

END 
GO


RAISERROR ('-- Add UnitSeries to Unit table', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Unit' AND COLUMN_NAME='UnitSeries')
BEGIN
	ALTER TABLE Unit ADD UnitSeriesID int NULL
	
	ALTER TABLE [dbo].[Unit]  WITH CHECK ADD CONSTRAINT [FK_UnitSeries_Unit] FOREIGN KEY(UnitSeriesID)
	REFERENCES [dbo].[UnitSeries] (ID)
	
	CREATE NONCLUSTERED INDEX [IX_Unit_UnitSeriesID] ON [dbo].[Unit]
	(
		[UnitSeriesID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	SET ANSI_PADDING ON

END 
GO




