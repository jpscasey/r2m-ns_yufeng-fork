SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Create gotcha schema
------------------------------------------------------------

RAISERROR ('-- Update gotcha schema', 0, 1) WITH NOWAIT
GO


--------------------- Wheel Measurement ----------------------------
DROP TABLE WheelMeasurement
CREATE TABLE [dbo].[AxleMeasurement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ComponentId] [int] NOT NULL,
	[Timestamp] datetime2(3) NOT NULL ,
	[RecordInserted] datetime2(3) NOT NULL ,
	[TrainSpeed] decimal(7,2) NOT NULL ,
	[MeanRightWheel] decimal(7,2) NOT NULL ,
	[PeakRightWheel] decimal(7,2) NOT NULL ,
	[RMSLowRightWheel] decimal(7,2) NOT NULL ,
	[RMSHighRightWheel] decimal(7,2) NOT NULL ,
	[WheelFlatRightWheel] decimal(7,2) NOT NULL ,
	[OutOfRoundRightWheel] decimal(7,2) NOT NULL ,
	[ValidWDDRightWheel] decimal(7,2) NOT NULL ,
	[MeanLeftWheel] decimal(7,2) NOT NULL ,
	[PeakLeftWheel] decimal(7,2) NOT NULL ,
	[RMSLowLeftWheel] decimal(7,2) NOT NULL ,
	[RMSHighLeftWheel] decimal(7,2) NOT NULL ,
	[WheelFlatLeftWheel] decimal(7,2) NOT NULL ,
	[OutOfRoundLeftWheel] decimal(7,2) NOT NULL ,
	[ValidWDDLeftWheel] decimal(7,2) NOT NULL ,
	[Valid] [bit] NOT NULL,
	
 CONSTRAINT [PK_AxleMeasurement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AxleMeasurement]  WITH CHECK ADD CONSTRAINT [FK_Component_AxleMeasurement] FOREIGN KEY(ComponentId)
REFERENCES [dbo].[Component] (ID)
GO

CREATE NONCLUSTERED INDEX [IX_WheelMeasurement_ComponentId] ON [dbo].[AxleMeasurement]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO

