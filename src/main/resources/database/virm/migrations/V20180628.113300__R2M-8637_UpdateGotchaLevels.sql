SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Set the gotcha levels to -1 
------------------------------------------------------------
insert into GotchaLevel (ComponentID, GotchaLevel, ValidFrom, ValidTo) SELECT ID, -1, GETDATE(), NULL FROM Component where ComponentTypeId = 3



