SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Update VehicleOrder for VIRM. The order of cabins in the unit diagram will be reversed for all units
------------------------------------------------------------

  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 1
  WHERE VehicleNumber LIKE '%mBvk1%'
  
  
  -- 8XXX Units
  
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 2
  WHERE VehicleNumber LIKE '%ABv5%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 3
  WHERE VehicleNumber LIKE '%mBv7%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 4
  WHERE VehicleNumber LIKE '%ABv6%'
  AND VehicleNumber LIKE '8%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 5
  WHERE VehicleNumber LIKE '%ABv3/4%'
  AND VehicleNumber LIKE '8%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 6
  WHERE VehicleNumber LIKE '%mBvk2%'
  AND VehicleNumber LIKE '8%'
  
  
  -- 9XXX Units
  
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 2
  WHERE VehicleNumber LIKE '%ABv6%'
  AND VehicleNumber LIKE '9%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 3
  WHERE VehicleNumber LIKE '%ABv3/4%'
  AND VehicleNumber LIKE '9%'
  
  UPDATE [dbo].[Vehicle]
  SET VehicleOrder = 4
  WHERE VehicleNumber LIKE '%mBvk2%'
  AND VehicleNumber LIKE '9%'