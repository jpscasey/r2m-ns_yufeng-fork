SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON


----------------------------------------------------------------
--  R2M-8881 Add FleetId column to the FaultMeta table
--  update FaultMeta table
----------------------------------------------------------------
RAISERROR ('--Add column FleetID to table FaultMeta', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS ic WHERE ic.TABLE_NAME = 'FaultMeta' AND ic.COLUMN_NAME = 'FleetID' )
BEGIN
	ALTER TABLE [dbo].[FaultMeta] ADD FleetID smallint 
END
GO


-- Delete current UNIQUE INDEX IX_U_FaultMeta_FaultCode_ParentID
RAISERROR ('--Delete current UNIQUE INDEX [IX_U_FaultMeta_FaultCode_ParentID]', 0, 1) WITH NOWAIT
GO

-- If IX_U_FaultMeta_FaultCode_ParentID is created as an index
IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'IX_U_FaultMeta_FaultCode_ParentID')
DROP INDEX [dbo].[FaultMeta].[IX_U_FaultMeta_FaultCode_ParentID]
GO


-- If IX_U_FaultMeta_FaultCode_ParentID is created a s a constraint
IF EXISTS (SELECT * FROM sys.key_constraints WHERE name = N'IX_U_FaultMeta_FaultCode_ParentID')
ALTER TABLE [dbo].[FaultMeta] DROP CONSTRAINT [IX_U_FaultMeta_FaultCode_ParentID]
GO


-- Create new UNIQUE INDEX
RAISERROR ('--Create new UNIQUE INDEX [IX_U_FaultMeta_FaultCode_FleetID_ParentID]', 0, 1) WITH NOWAIT
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = N'IX_U_FaultMeta_FaultCode_FleetID_ParentID')
DROP INDEX [dbo].[FaultMeta].[IX_U_FaultMeta_FaultCode_FleetID_ParentID]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_U_FaultMeta_FaultCode_FleetID_ParentID] ON [dbo].[FaultMeta]
(
	[FaultCode] ASC,
	[FleetID] ASC,
	[ParentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


RAISERROR ('--Add Foreign Key to column FaultMeta.FleetID', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FaultMeta_Fleet]'))
BEGIN
	ALTER TABLE [dbo].[FaultMeta] ADD CONSTRAINT [FK_FaultMeta_Fleet] FOREIGN KEY([FleetID]) REFERENCES [dbo].[Fleet] (ID)
	
	ALTER TABLE [dbo].[FaultMeta] CHECK CONSTRAINT [FK_FaultMeta_Fleet]
END
GO