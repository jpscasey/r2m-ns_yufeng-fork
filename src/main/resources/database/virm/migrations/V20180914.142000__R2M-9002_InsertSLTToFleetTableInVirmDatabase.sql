SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------
-- R2M-9002 Rules Editor - Error when saving SLT Fault Configs
-- Insert a new row in Fleet table in VIRM fleet database
----------------------------------------------------------------

RAISERROR ('-- Update Fleet table for VIRM', 0, 1) WITH NOWAIT
GO

BEGIN TRAN

DECLARE @SLT_DBNAME varchar(100) = '${DBNAME_SLT}'
DECLARE @VIRM_DBNAME varchar(100) = '${DBNAME_VIRM}'
DECLARE @query varchar(max)

SET @query = '
IF NOT EXISTS (SELECT * FROM [' + @VIRM_DBNAME + '].[dbo].[Fleet] WHERE Code = ''SLT'' )
BEGIN
	SET IDENTITY_INSERT [' + @VIRM_DBNAME + '].[dbo].[Fleet] ON

	INSERT INTO [' + @VIRM_DBNAME + '].[dbo].[Fleet] (ID, Name, Code)
	SELECT ID, Name, Code 
	FROM [' + @SLT_DBNAME + '].[dbo].[Fleet]
	WHERE Code = ''SLT'' 

	SET IDENTITY_INSERT [' + @VIRM_DBNAME + '].[dbo].[Fleet] OFF
END'

EXEC(@query)

COMMIT

GO