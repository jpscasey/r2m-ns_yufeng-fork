SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON
------------------------------------------------------------
--Update RuleEngineCurrentId values
------------------------------------------------------------
RAISERROR ('-- Update RuleEngineCurrentId values', 0, 1) WITH NOWAIT
GO

DECLARE @ChannelValueLastTime datetime = (Select TimeStamp from ChannelValue where id = (Select Value from RuleEngineCurrentId Where ConfigurationName = 'VIRM' and Position = 1))

UPDATE RuleEngineCurrentId 
SET Value = CAST(DATEDIFF(ss, '1970-01-01 00:00:00.0000000', @ChannelValueLastTime) AS bigint)*1000
Where ConfigurationName = 'VIRM' and Position = 1

DECLARE @EventChannelValueLastTime datetime = (Select TimeStamp from EventChannelValue where id = (Select Value from RuleEngineCurrentId Where ConfigurationName = 'VIRM' and Position = 2))

UPDATE RuleEngineCurrentId 
SET Value = CAST(DATEDIFF(ss, '1970-01-01 00:00:00.0000000', @EventChannelValueLastTime) AS bigint)*1000
Where ConfigurationName = 'VIRM' and Position = 2