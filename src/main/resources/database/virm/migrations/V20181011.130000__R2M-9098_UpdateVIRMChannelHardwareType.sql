SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

--------------------------------------------------------------------
-- R2M-9098 - VIRM Data view - Some channels break the data plots
-- Update the HardwareType for Channle 1Actueel koppel (mBvk1), 2Actueel koppel (mBvk1) and 7Actueel koppel (mBvk1)
--------------------------------------------------------------------

RAISERROR ('-- Update the HardwareType for Channle 1Actueel koppel (mBvk1), 2Actueel koppel (mBvk1) and 7Actueel koppel (mBvk1)', 0, 1) WITH NOWAIT
GO

UPDATE [dbo].[Channel] SET HardWareType = 'Sensor Channels' WHERE ID = 146 AND Name = '1Actueel koppel (mBvk1)'
UPDATE [dbo].[Channel] SET HardWareType = 'Sensor Channels' WHERE ID = 147 AND Name = '2Actueel koppel (mBvk1)'
UPDATE [dbo].[Channel] SET HardWareType = 'Sensor Channels' WHERE ID = 148 AND Name = '7Actueel koppel (mBvk1)'
GO