SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

--------------------------------------------------------------------
-- R2M-9098 - Change Unit type off the VIRM1  fleet
--------------------------------------------------------------------

RAISERROR ('-- Change Unit type off the VIRM1  fleet', 0, 1) WITH NOWAIT
GO

-- Add missing vehicles for Unit '9468'
IF NOT EXISTS (SELECT * FROM [dbo].[Vehicle] v JOIN [dbo].[Unit] u ON v.UnitID = u.ID WHERE u.UnitNumber = '9468')
BEGIN
	DECLARE @unitId int
	
	SELECT @unitId = ID FROM [dbo].[Unit] WHERE UnitNumber = '9468'
	
	INSERT INTO [dbo].[Vehicle] (VehicleNumber, Type, VehicleOrder, CabEnd, UnitID, Comments, ConfigDate, LastUpdate, IsValid, HardwareTypeID, OtmrSn, NoOpenRestriction, NoP1WorkOrders, VehicleTypeID)
	VALUES ('9468mBvk1', 'mBvk1', 1, 1, @unitId, 'cab 2', GETDATE(), GETDATE(), 1, 1, NULL, NULL, NULL, 1),
	 ('9468ABv6', 'ABv6', 2, 0, @unitId, '', GETDATE(), GETDATE(), 1, 1, NULL, NULL, NULL, 5),
	 ('9468ABv3/4', 'ABv3_4', 3, 0, @unitId, '', GETDATE(), GETDATE(), 1, 1, NULL, NULL, NULL, 3),
	 ('9468mBvk2', 'mBvk2', 4, 1, @unitId, 'cab 1', GETDATE(), GETDATE(), 1, 1, NULL, NULL, NULL, 2)
	
END 


-- Update the Unit type
-- Change to 'VIRM-1M IV'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9401'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9402'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9403'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9404'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9405'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9406'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9407'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9409'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9411'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9412'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9413'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9416'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9417'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9418'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9419'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9420'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9422'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9423'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9425'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9426'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9427'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9430'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9431'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9434'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9443'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9450'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9468'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9469'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9473'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9477'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9478'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9479'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M IV' WHERE UnitNumber = '9480'

-- Change to VIRM-1M VI
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8608'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8614'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8621'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8624'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8635'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8637'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8640'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8652'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8658'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8663'
UPDATE [dbo].[Unit] SET UnitType = 'VIRM-1M VI' WHERE UnitNumber = '8671'

GO
