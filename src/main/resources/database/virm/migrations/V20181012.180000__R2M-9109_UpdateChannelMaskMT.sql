SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

----------------------------------------------------------------------------------
-- R2M-9109 - Implement conversions for channels that is decoded by using mt mask
----------------------------------------------------------------------------------

RAISERROR ('-- Updating Channel Mask for MT', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS ic WHERE ic.TABLE_NAME = 'ChannelMask' AND ic.COLUMN_NAME = 'Conversion' )
BEGIN
	ALTER TABLE dbo.ChannelMask ADD Conversion decimal(14,12) ;
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS ic WHERE ic.TABLE_NAME = 'ChannelMask' AND ic.COLUMN_NAME = 'DataType' )
BEGIN
	ALTER TABLE dbo.ChannelMask ADD DataType varchar(20) null;
END
GO

Update dbo.ChannelMask Set Conversion = 100*1.0/POWER(2,14)
where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '64B5_HR_DRUK');

Update dbo.ChannelMask Set Conversion = 100*1.0/POWER(2,15)
where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '6Treinleiding druk');

Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '64B5_HR_DRUK');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '6Treinleiding druk');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '73A8_MW_UBAT_VOLT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '73A2_M_UT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '73A2_M_IT_GEM');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '72A1_BELADING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '72A1_IN_Gevraagd_Koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '7Actueel koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '72A1_ILACT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '72A1_METING_LIJNSPANNING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '72A1_Inverter_Frequentie');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '13A8_MW_UBAT_VOLT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '13A2_M_UT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '13A2_M_IT_GEM');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '12A1_BELADING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '12A1_IN_Gevraagd_Koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '1Actueel koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '12A1_ILACT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '12A1_METING_LIJNSPANNING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '12A1_Inverter_Frequentie');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '18A2_ATB_Treinsnelheid');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '18A2_ATB_Bewaakte_Snelheid');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '23A8_MW_UBAT_VOLT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '23A2_M_UT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '23A2_M_IT_GEM');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '22A1_BELADING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '22A1_IN_Gevraagd_Koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '2Actueel koppel');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '22A1_ILACT');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '22A1_METING_LIJNSPANNING');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '22A1_Inverter_Frequentie');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '28A2_ATB_Treinsnelheid');
Update dbo.ChannelMask Set DataType = 'short' where MaskName='mt' and ChannelID=(SELECT ID from dbo.Channel c where c.Name = '28A2_ATB_Bewaakte_Snelheid');
