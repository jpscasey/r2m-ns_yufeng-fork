SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

--------------------------------------------------------------------
-- R2M-9134 - Add ChannelValueGPS to VIRM database
--------------------------------------------------------------------

RAISERROR ('-- Add ChannelValueGPS to VIRM database', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='ChannelValueGps')
    CREATE TABLE ChannelValueGps (
        [ID] int IDENTITY(1,1) NOT NULL, 
        [Timestamp] datetime2(3) NOT NULL,
        [UnitID] int NOT NULL, 
        [Latitude] decimal(9,6) NOT NULL, 
        [Longitude] decimal(9,6) NOT NULL, 
        [Speed] decimal(9,3) NULL
        CONSTRAINT [PK_ChannelValueGps] PRIMARY KEY CLUSTERED
        (
            [ID] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
    ) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChannelValueGps_Unit]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]'))
ALTER TABLE [dbo].[ChannelValueGps] WITH NOCHECK ADD CONSTRAINT [FK_ChannelValueGps_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([ID])
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_TimeStamp_UnitId')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_TimeStamp_UnitId] ON [dbo].[ChannelValueGps]
(
    [TimeStamp] ASC,
    [UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChannelValueGps]') AND name = N'IX_ChannelValueGps_UnitId_TimeStamp')
CREATE NONCLUSTERED INDEX [IX_ChannelValueGps_UnitId_TimeStamp] ON [dbo].[ChannelValueGps]
(
    [UnitId] ASC,
    [TimeStamp] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
GO

--------------------------------------------------------------------
-- R2M-9134 - Add GPS channels to dbo.Channel
--------------------------------------------------------------------

RAISERROR ('-- add records in Channel table', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT Channel ON;

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId)
VALUES (8001, NULL, 8001, 1, 'Laatste GPS breedtegraad', 'VT_GPSLatitude', 'A8001','decimal (9,6)', 2, NULL, '°', 0, 5320, 'Virtual Train', NULL, 'Laatste GPS breedtegraad', NULL, 1 , 0, NULL, NULL, 0, NULL, 2, 2, 6);

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId) 
VALUES (8002, NULL, 8002, 1, 'Laatste GPS lengtegraad', 'VT_GPSLongitude', 'A8002','decimal (9,6)', 2, NULL, '°', 0, 255, 'Virtual Train', NULL, 'Laatste GPS lengtegraad', NULL, 1 , 0, NULL, NULL, 0, NULL, 1, 1, 6);

INSERT INTO [dbo].[Channel] (ID, VehicleID, EngColumnNo, ChannelGroupID, Description, Name, Ref, DataType, TypeID, ChannelGroupOrder, UOM, MinValue, MaxValue, HardwareType, StorageMethod, Comment, VisibleOnFaultOnly, IsAlwaysDisplayed, IsLookup, DefaultValue, FleetGroup, IsDisplayedAsDifference, RelatedChannelID, VehicleIn4Car, VehicleIn6Car, ChannelCategoryId) 
VALUES (8003, NULL, 8003, 1, 'Laatste GPS snelheid', 'VT_GPSSpeed', 'A8003','decimal (9,3)', 2, NULL, 'km/h', 0, 176, 'Virtual Train', NULL, 'Laatste GPS snelheid', NULL, 1 , 0, NULL, NULL, 0, NULL, 1, 1, 6);

SET IDENTITY_INSERT Channel OFF;
GO


------------------------------------------------------------
-- Create FaultChannelValueGps table
------------------------------------------------------------

RAISERROR ('-- Create FaultChannelValueGps table', 0, 1) WITH NOWAIT
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = 'FaultChannelValueGps')
BEGIN
    CREATE TABLE [dbo].[FaultChannelValueGps](
        [ID] [bigint] NOT NULL,
        [FaultID] [int] NOT NULL,
        [UnitID] [int] NOT NULL,
        [TimeStamp] [datetime] NOT NULL,
        [Col8001] [decimal](9,6) NULL,
        [Col8002] [decimal](9,6) NULL,
        [Col8003] [decimal](9,3) NULL
        CONSTRAINT [PK_FaultChannelValueGps] PRIMARY KEY CLUSTERED 
    (
        [FaultID] ASC,
        [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
    ) ON [PRIMARY]
END

GO
