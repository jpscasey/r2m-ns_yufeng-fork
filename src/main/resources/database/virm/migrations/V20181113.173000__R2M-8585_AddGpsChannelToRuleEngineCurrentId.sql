SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Add an entry to RuleEngineCurrentId for the ChannelValueGps table
------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM RuleEngineCurrentId WHERE ConfigurationName = 'VIRM' AND Position = 3)
	INSERT INTO RuleEngineCurrentId (ConfigurationName, Position, Value, LastUpdateTime) VALUES ('VIRM', 3, 1, NULL)
	
DECLARE @GpsChannelValueLastTime datetime = GETDATE();

UPDATE RuleEngineCurrentId 
SET Value = CAST(DATEDIFF(ss, '1970-01-01 00:00:00.0000000', @GpsChannelValueLastTime) AS bigint)*1000
Where ConfigurationName = 'VIRM' and Position = 3
