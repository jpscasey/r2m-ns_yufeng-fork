SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
-- Add 'maximoFaultCode' for existing fault configurations.
------------------------------------------------------------

RAISERROR ('-- add some records in FaultMetaExtraField table', 0, 1) WITH NOWAIT
GO

INSERT INTO dbo.FaultMetaExtraField (FaultMetaId, Field, Value) 
SELECT FaultMetaId,'MaximoFaultCode' AS Field,'Default' AS Value FROM dbo.FaultMetaExtraField GROUP BY FaultMetaId

GO