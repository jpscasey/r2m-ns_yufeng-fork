/*
MAin script this will change All Columns that have a datetime type to be datetime2

THis should not be run on live without been tested and signed off.

Note Any Large tables should not use this method and should use one of the other methods

To Exclude a table add its name to the #ExcludeTables table

This script Drops indexes and defaults used on the columns and recreates them after the column has been changed.


*/

CREATE TABLE #ExcludeTables
(
	tablename varchar(200)

)

INSERT INTO #ExcludeTables 
VALUES ('FAULT')
,('FaultChannelValue')

DECLARE @TableName varchar(max) = ''
DECLARE @ColName varchar(max) = ''
DECLARE @Alter varchar(max) = ''

DECLARE @object_id int

WhILE 1=1
BEGIN 

SELECT TOP 1 @TableName = t.name , @ColName = c.name , @Alter = 'ALTER TABLE ' + t.name + ' ALTER COLUMN '+ c.name +' DATETIME2(3) ' + CASE when c.is_nullable = 0 THEN 'NOT NULL' ELSE 'NULL' END FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name not in (SELECT tablename FROM #ExcludeTables)
AND T.name <> 'schema_version'
ORDER BY t.name, c.name

IF @@ROWCOUNT =0 BREAK



PRINT  @TableName +'.'+@ColName

SET @object_id = OBJECT_ID(@TableName)

DECLARE @DefaultAdd varchar(max) =NULL
DECLARE @DefaultDrop varchar(max) =NULL

SELECT @DefaultAdd = 'ALTER TABLE '+@TableName+' ADD CONSTRAINT ['+dc.name+'] DEFAULT' + dc.[definition] +' FOR ['+c.name+']'
,@DefaultDrop = 'ALTER TABLE '+@TableName+' DROP CONSTRAINT ['+dc.name+']'

FROM sys.columns c WITH (NOWAIT)
 JOIN sys.default_constraints dc ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
 where c.name = @ColName and c.object_id = @object_id



DECLARE @PrimaryAdd varchar(max) =NULL
DECLARE @PrimaryDrop varchar(max) =NULL


IF EXiSTS (SELECT 1 FROM
sys.key_constraints k WITH (NOWAIT)
iNNER JOIN sys.index_columns ic WITH (NOWAIT) ON ic.[object_id] = k.parent_object_id AND ic.index_id = k.unique_index_id  
iNNER JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
							AND k.parent_object_id = @object_id 
							AND c.name = @ColName)
BEGIN

SELECT @PrimaryAdd = ISNULL((SELECT CHAR(9) + 'ALTER TABLE '+@TableName+' ADD CONSTRAINT [' + k.name + '] PRIMARY KEY '+case when i.type_desc ='CLUSTERED' ThEN 'CLUSTERED' ELSE 'NONCLUSTERED' END +' (' + 
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id    
					     ORDER BY ic.key_ordinal
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
			+'WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON'+CASE WHEN i.fill_factor =0 then '' ELSE ' , FILLFACTOR = '+Cast(i.fill_factor as varchar(4)) END+') ON [PRIMARY] '
            FROM sys.key_constraints k WITH (NOWAIT)
			LEFT JOiN Sys.indexes i ON i.object_id = k.parent_object_id AND k.name = i.name
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') 


SELECT @PrimaryDROP = ISNULL((SELECT CHAR(9) + 'ALTER TABLE '+@TableName+' DROP CONSTRAINT [' + k.name + ']'
 FROM sys.key_constraints k WITH (NOWAIT)
			LEFT JOiN Sys.indexes i ON i.object_id = k.parent_object_id AND k.name = i.name
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') 

END

DECLARE @IndexAdd varchar(max) =NULL
DECLARE @IndexDrop varchar(max) =NULL
;WITH index_column AS 
(
    SELECT 
          ic.[object_id]
        , ic.index_id
        , ic.is_descending_key
        , ic.is_included_column
        , c.name
		, ic.key_ordinal
    FROM sys.index_columns ic WITH (NOWAIT)
    JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
    WHERE ic.[object_id] = @object_id
)

SELECT @IndexAdd = ISNULL((
		SELECT STUFF(
	(SELECT ' 
	CREATE' + CASE WHEN i.is_unique = 1 THEN ' UNIQUE' ELSE '' END 
                + ' '+ CASE WHeN i.type_desc = 'CLUSTERED' THEN 'CLUSTERED' ELSE 'NONCLUSTERED' END + '  INDEX [' + i.name + '] ON ' + t.name + ' (' +
                STUFF((
                SELECT ', [' + c.name + ']' + CASE WHEN c.is_descending_key = 1 THEN ' DESC' ELSE ' ASC' END
                FROM index_column c
                WHERE c.is_included_column = 0
                    AND c.index_id = i.index_id
					AND c.[object_id] = i.object_id
					ORDER BY c.key_ordinal
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')'  
                + ISNULL(CHAR(13) + 'INCLUDE (' + 
                    STUFF((
                    SELECT ', [' + c.name + ']'
                    FROM index_column c
                    WHERE c.is_included_column = 1
                        AND c.index_id = i.index_id
						AND c.[object_id] = i.object_id
						ORDER BY c.key_ordinal
		
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')', '')
						+ CASE WHEN i.filter_definition is not null then ' WHERE '+i.filter_definition ELSE '' END
					+ ' WITH (PAD_INDEX = '+ CASE WHEN i.is_padded =1 THEN 'ON' ELSE 'OFF'END+
					', STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, 
ALLOW_PAGE_LOCKS = ON'+CASE WHEN i.fill_factor =0 then '' ELSE ' , FILLFACTOR = '+Cast(i.fill_factor as varchar(4)) END  +')'
	
        FROM sys.indexes i WITH (NOWAIT)
		INNER JOIN sys.tables t on t.object_id = i.object_id
		INNER JOIN sys.index_columns ic2 WITH (NOWAIT) ON ic2.index_id = i.index_id AND ic2.[object_id] = i.object_id
		JOIN sys.columns c2 WITH (NOWAIT) ON ic2.[object_id] = c2.[object_id] AND ic2.column_id = c2.column_id
        WHERE 1=1
            AND i.is_primary_key = 0
			 and c2.name = @ColName
			and i.object_id = @object_id
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') 
	),'')




;WITH index_column AS 
(
    SELECT 
          ic.[object_id]
        , ic.index_id
        , ic.is_descending_key
        , ic.is_included_column
        , c.name
		, ic.key_ordinal
    FROM sys.index_columns ic WITH (NOWAIT)
    JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
    WHERE ic.[object_id] = @object_id and c.name = @ColName
)	
SELECT @IndexDrop = ISNULL((
		SELECT STUFF(
	(SELECT distinct ' 
	DROP   INDEX [' + i.name + '] ON ' + t.name 
	
        FROM sys.indexes i WITH (NOWAIT)
		INNER JOIN sys.tables t on t.object_id = i.object_id
		INNER JOIN index_column ic ON ic.index_id = i.index_id AND ic.[object_id] = i.object_id
        WHERE 1=1
            AND i.is_primary_key = 0
			and i.object_id = @object_id
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') 
	),'')



SELECT @TableNAme , @ColName ,@DefaultAdd , @DefaultDrop, @Alter,@IndexAdd , @IndexDrop ,@PrimaryAdd, @PrimaryDrop

PRINT ' DefaultDrop'
EXEC (@DefaultDrop)
PRINT '@PrimaryDrop'
EXEC (@PrimaryDrop)
PRINT ' IndexDrop'
EXEC (@IndexDrop)
PRINT ' MAin'
EXEC (@Alter)
PRINT ' Add Default'
EXEC (@DefaultAdd)
PRINT '@PrimaryAdd'
EXEC (@PrimaryAdd)
PRINT ' Add Index'
EXEC (@IndexAdd)


END









