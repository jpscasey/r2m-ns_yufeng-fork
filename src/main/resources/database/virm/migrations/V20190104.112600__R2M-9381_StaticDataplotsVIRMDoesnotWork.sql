SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
--  Space in unit type VIRM-1M VI looks like space, but actually it is not space. Correcting it to actual space.
------------------------------------------------------------

RAISERROR ('-- Space in unit type VIRM-1M VI looks like space, but actually it is not space. Correcting it to actual space.', 0, 1) WITH NOWAIT
GO

update [dbo].[Unit] set UnitType = 'VIRM-1M VI' where UnitType = 'VIRM-1M VI'
GO