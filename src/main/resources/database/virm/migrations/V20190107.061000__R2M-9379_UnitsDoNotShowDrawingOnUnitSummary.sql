SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

------------------------------------------------------------
--  VIRM units 8610, 9544, and 8702 do not show their drawing on Unit Summary due to missing Vehicle records.
------------------------------------------------------------

RAISERROR ('-- VIRM units 8610, 9544, and 8702 do not show their drawing on Unit Summary due to missing Vehicle records.', 0, 1) WITH NOWAIT
GO

SET IDENTITY_INSERT [dbo].[Vehicle] ON

INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610ABv3/4', N'ABv3_4', 5, N'0', 179, N'', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 3)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610ABv5', N'ABv5', 2, N'0', 179, N'', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 4)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610ABv6', N'ABv6', 4, N'0', 179, N'', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 5)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610mBv7', N'mBv7', 3, N'0', 179, N'', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 6)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610mBvk1', N'mBvk1', 1, N'1', 179, N'cab 2', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 1)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8610mBvk2', N'mBvk2', 6, N'1', 179, N'cab 1', CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), CAST(N'2018-03-25T00:19:09.3870000' AS DateTime2), 1, 1, NULL, NULL, NULL, 2)

INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'9544ABv3/4', N'ABv3_4', 3, N'0', 182, N'', CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), 1, 1, NULL, NULL, NULL, 3)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'9544ABv6', N'ABv6', 2, N'0', 182, N'', CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), 1, 1, NULL, NULL, NULL, 5)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'9544mBvk1', N'mBvk1', 1, N'1', 182, N'cab 2', CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), 1, 1, NULL, NULL, NULL, 1)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'9544mBvk2', N'mBvk2', 4, N'1', 182, N'cab 1', CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), CAST(N'2018-03-25T00:19:09.7470000' AS DateTime2), 1, 1, NULL, NULL, NULL, 2)

INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702ABv3/4', N'ABv3_4', 5, N'0', 180, N'', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 3)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702ABv5', N'ABv5', 2, N'0', 180, N'', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 4)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702ABv6', N'ABv6', 4, N'0', 180, N'', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 5)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702mBv7', N'mBv7', 3, N'0', 180, N'', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 6)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702mBvk1', N'mBvk1', 1, N'1', 180, N'cab 2', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 1)
INSERT [dbo].[Vehicle] ([ID], [VehicleNumber], [Type], [VehicleOrder], [CabEnd], [UnitID], [Comments], [ConfigDate], [LastUpdate], [IsValid], [HardwareTypeID], [OtmrSn], [NoOpenRestriction], [NoP1WorkOrders], [VehicleTypeID]) 
VALUES ((select max(v.id) + 1 from [dbo].[Vehicle] v), N'8702mBvk2', N'mBvk2', 6, N'1', 180, N'cab 1', CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), CAST(N'2018-03-25T00:19:09.6070000' AS DateTime2), 1, 1, NULL, NULL, NULL, 2)

SET IDENTITY_INSERT [dbo].[Vehicle] OFF

GO