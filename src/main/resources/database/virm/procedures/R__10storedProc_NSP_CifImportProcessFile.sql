SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_CifImportProcessFile', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_CifImportProcessFile' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_CifImportProcessFile AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_CifImportProcessFile]
(
	@FileID int
)
AS
BEGIN

	SET NOCOUNT ON;
	SET DATEFORMAT mdy;
	SET datefirst 7;
	-- operaor code - only records with this code will be processed:
	-- SR	- Scotrail
	-- GR	- Eastcoast
	-- SW	- SWT
	DECLARE @atocCode varchar(10) = 'SR'
	
	------------------------------------------------------------------------------
	-- Check file status, porcess only if status is imported
	------------------------------------------------------------------------------
	IF @FileID IS NOT NULL AND (SELECT Status FROM LoadCifFile WHERE FileID = @FileID) <> 'Imported'
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Error - File status different than Imported' WHERE FileID = @FileID
		RAISERROR('Error - File status different than Imported', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Processing' WHERE FileID = @FileID
	END

	------------------------------------------------------------------------------
	-- Parse file data into a set or temporary tables, one for each row type 
	------------------------------------------------------------------------------
	DECLARE @spResult int 
	EXEC @spResult = NSP_CifImportParseData @FileID

	IF @spResult <> 0
	BEGIN
		UPDATE dbo.LoadCifFile SET Status = 'Error - Error when parsing file' WHERE FileID = @FileID
		PRINT 'Error - Error when parsing file'
		RAISERROR('Error - Error when parsing file', 16, 1)
		RETURN
	END

	------------------------------------------------------------------------------
	-- Reset Status column
	------------------------------------------------------------------------------
	UPDATE dbo.LoadCifFileData SET 
		Status = NULL
	WHERE FileID = @FileID

	------------------------------------------------------------------------------
	-- Check this is correct file, not 1 month old
	------------------------------------------------------------------------------
	DECLARE @fileType char(1) -- F - Full, U - update
	DECLARE @dateOfExtract char(6)

	SELECT 
		@fileType = [Bleed-off/UpdateInd]
		, @dateOfExtract = DateofExtract
	FROM ##CifLineHD

/*
	IF DATEDIFF(d, CAST('20'+ SUBSTRING(@dateOfExtract, 5,2) + '-' + SUBSTRING(@dateOfExtract, 3,2) + '-' + SUBSTRING(@dateOfExtract, 1,2) AS date), SYSDATETIME()) > 20 
	BEGIN
		UPDATE LoadCifFile SET Status = 'Error - File older than 20 days' WHERE FileID = @FileID
		PRINT 'Error - File older than 20 days'
		RAISERROR('Error - File older than 20 days', 16, 1)
		RETURN
	END
*/

	------------------------------------------------------------------------------
	-- Delete all records from today going forward (for Full extract only)
	------------------------------------------------------------------------------
	IF @fileType = 'F' 
	BEGIN
		DELETE FROM dbo.JourneyDate
		WHERE DateValue >= CAST(SYSDATETIME() AS date)
	END		
	
	------------------------------------------------------------------------------
	-- Start import:
	------------------------------------------------------------------------------
	DECLARE @bsLine int
	
	IF OBJECT_ID('tempdb..#tempJourney') IS NOT NULL
		DROP TABLE #tempJourney

	CREATE TABLE #tempJourney 
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[TrainUID] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
		[Headcode] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
		[StartLocationID] [int] NULL,
		[EndLocationID] [int] NULL,
		[StartTime] [time](0) NULL,
		[EndTime] [time](0) NULL,
		[ValidFrom] [datetime2(3)] NOT NULL,
		[ValidTo] [datetime2(3)] NOT NULL,
		[DaysRun] [char](7) COLLATE Latin1_General_CI_AS NOT NULL,
		[StpType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	IF OBJECT_ID('tempdb..#tempSectionPoint') IS NOT NULL
		DROP TABLE #tempSectionPoint

	CREATE TABLE #tempSectionPoint
	(
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LocationID] [int] NOT NULL,
		[LocationSuffix] [tinyint] NOT NULL,
		[ScheduledArrival] [time](0) NULL,
		[ScheduledDeparture] [time](0) NULL,
		[ScheduledPass] [time](0) NULL,
		[PublicArrival] [time](0) NULL,
		[PublicDeparture] [time](0) NULL,
		[RecoveryTime] [int] NOT NULL,
		[SectionPointType] [char](1) COLLATE Latin1_General_CI_AS NOT NULL
	)

	-- loop on BS records
	WHILE 1=1
	BEGIN
		SET @bsLine = (SELECT MIN(FileLineNumber) FROM ##CifLineBS WHERE FileLineNumber > ISNULL(@bsLine, 0))
		IF @bsLine IS NULL 
			BREAK
		-----------------------------------------------------------------------
		DECLARE @bsTransactionType char(1) = (SELECT TransactionType FROM ##CifLineBS WHERE FileLineNumber = @bsLine)

		DECLARE @headcode varchar(10) = NULL
		DECLARE @dateRunsFrom date = NULL
		DECLARE @dateRunsTo date = NULL
		DECLARE @daysRun char(7) = NULL
		DECLARE @bankHolidayRunning char(1) = NULL
		DECLARE @journeyID int = NULL
		DECLARE @stpType char(1) = NULL
		DECLARE @trainUID char(6) = NULL
		DECLARE @printOut varchar(200) = NULL
		DECLARE @logEntry varchar(200) = NULL
		DECLARE @existingJourneyID int = NULL
		DECLARE @useExistingJourneyID bit = NULL

		SELECT 
			@headcode = TrainIdentity
			, @dateRunsFrom = DateRunsFrom
			, @dateRunsTo = DateRunsTo
			, @daysRun = DaysRun
			, @bankHolidayRunning = BankHolidayRunning
			, @stpType = STPIndicator
			, @trainUID = TrainUID
		FROM ##CifLineBS 
		WHERE FileLineNumber = @bsLine

		SET @printOut = CAST(@bsLine AS char(6))
			 + ' | ' + CAST(@trainUID AS char(8))
			 + ' | ' + CAST(@headcode as char(8))
			 + ' | ' + CAST(@dateRunsFrom as char(12))
			 + ' | ' + CAST(@dateRunsTo as char(10))
			 + ' | ' + CAST(@daysRun as char(7))
			 + ' | ' + CAST(@bankHolidayRunning as char(9))
			 + ' | ' + CAST(@bsTransactionType as char(7))
			 + ' | ' + CAST(@stpType as char(7))
			 + ' | '
		
		IF @bsTransactionType NOT IN ('N') -- Delete
		BEGIN
			SET @logEntry = @printOut + 'Not processed - Transaction Type not supported'
			UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
		END

		IF @bsTransactionType IN ('N') -- New
		BEGIN 

			-- C - Stp cancellation of Permanent Schedule
			IF @stpType = 'C' 
			BEGIN
				
				UPDATE dbo.JourneyDate SET
					ActualJourneyID = NULL
				FROM JourneyDate
				INNER JOIN dbo.Journey ON Journey.ID = PermanentJourneyID
				WHERE TrainUID = @trainUID
					AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))

				SET @logEntry = @printOut + 'Stp cancellation of Permanent Schedule. Updated: ' + STR(@@ROWCOUNT)

				UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
						
			END
			
			-- N - New Stp Schedule (not an overlay)
			IF @stpType = 'N'
			BEGIN
				SET @logEntry = @printOut + 'No Action: New Stp Schedule (not an overlay)'

				UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID
				
			END

			-- P - New Pemanenet
			-- O - New Overlay Schedule
			IF @stpType IN ('P', 'O')
			BEGIN
			
				-- Check if Headcode is for required ATOC Operator
				IF (SELECT ATOCCode FROM ##CifLineBX WHERE FileLineNumber = @bsLine + 1) <> @atocCode
				BEGIN
					SET @logEntry = @printOut + 'Headcode for another operator'
					UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
					CONTINUE
				END 
				
				TRUNCATE TABLE #tempJourney
				TRUNCATE TABLE #tempSectionPoint

				INSERT INTO #tempJourney
					([TrainUID]
					,[Headcode]
					,[ValidFrom]
					,[ValidTo]
					,[DaysRun]
					,[StpType])
				SELECT
					TrainUID		--[TrainUID]
					,TrainIdentity	--[Headcode]
					,DateRunsFrom	--[ValidFrom]
					,DateRunsTo		--[ValidTo]
					,DaysRun		--[DaysRun]
					,STPIndicator	--[StpType]
				FROM ##CifLineBS 
				WHERE FileLineNumber = @bsLine


					-- loop on all lines for a Headcode (Journey)
					DECLARE @lineNumber int = @bsLine
					
					WHILE 1 = 1
					BEGIN
					
						SET @lineNumber = @lineNumber + 1
						DECLARE @lineType char(2) = (SELECT LEFT(Data, 2) FROM dbo.LoadCifFileData WHERE FileID = @FileID AND FileLineNumber = @lineNumber)
						
						IF @lineType IN ('ZZ','BS') -- ZZ - end of file, BS - next headcode
						BREAK
						-----------------------------------------
						
						IF @lineType NOT IN ('LO', 'LU', 'LI', 'CR', 'LT') -- we are interested only in these lines
						CONTINUE
						-----------------------------------------
						
						DECLARE @tiploc varchar(8)
						DECLARE @locationID int
						
						-- Origin Location
						IF @lineType = 'LO'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLO WHERE FileLineNumber = @lineNumber)

							EXEC dbo.NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								,0			--[LocationSuffix] 
								,NULL		--[ScheduledArrival]
								,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
								,NULL		--[ScheduledPass]
								,NULL		--[PublicArrival]
								,dbo.FN_ConvertCifTime([PublicDeparture])	--[PublicDeparture]
									,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)	--[RecoveryTime]
								,'B'			--[SectionPointType]
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
							
							UPDATE #tempJourney SET
								StartLocationID = @locationID
								, StartTime = dbo.FN_ConvertCifTime([ScheduledDeparture]) 
							FROM ##CifLineLO 
							WHERE FileLineNumber = @lineNumber
			 
						END
						
						-- Intermediate Location
						IF @lineType = 'LI'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLI WHERE FileLineNumber = @lineNumber)

							EXEC dbo.NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT

							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								, CASE 
									WHEN ISNUMERIC(LocationSuffix) = 1 
										THEN LocationSuffix
									ELSE 1
								END
								,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
								,dbo.FN_ConvertCifTime([ScheduledDeparture]) --[ScheduledDeparture]
								,dbo.FN_ConvertCifTime([ScheduledPass])		--[ScheduledPass]
								,CASE 
									WHEN [PublicArrival] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicArrival])
								END			--[PublicArrival]
								,CASE 
									WHEN [PublicDeparture] = '0000' THEN NULL
									ELSE dbo.FN_ConvertCifTime([PublicDeparture])
								END			--[PublicDeparture]
								,dbo.FN_ConvertCifEngAllowance(EngineeringAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PathingAllowance)
								+ dbo.FN_ConvertCifEngAllowance(PerformanceAllowance)
								--[RecoveryTime]
								,CASE 
									WHEN LEN([ScheduledPass]) > 0 THEN 'T'	--Timing Point
									ELSE 'S'	--Station
								END			--[SectionPointType]
							FROM ##CifLineLI
							WHERE FileLineNumber = @lineNumber

						END

						-- Terminating Location
						IF @lineType = 'LT'
						BEGIN
							SET @tiploc = (SELECT [Location] FROM ##CifLineLT WHERE FileLineNumber = @lineNumber)

							EXEC NSP_GetOrInsertLocationIdForTiploc @tiploc, @locationID OUTPUT
												
							INSERT INTO #tempSectionPoint
								([LocationID]
								,[LocationSuffix] 
								,[ScheduledArrival]
								,[ScheduledDeparture]
								,[ScheduledPass]
								,[PublicArrival]
								,[PublicDeparture]
								,[RecoveryTime]
								,[SectionPointType])
							SELECT
								@locationID --[LocationID]
								,0			--[LocationSuffix] 
								,dbo.FN_ConvertCifTime([ScheduledArrival])	--[ScheduledArrival]
								,NULL		--[ScheduledDeparture]
								,NULL		--[ScheduledPass]
								,dbo.FN_ConvertCifTime([PublicArrival])		--[PublicArrival]
								,NULL		--[PublicDeparture]
								,0			--[RecoveryTime]
								,'E'			--[SectionPointType]
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber

							UPDATE #tempJourney SET
								EndLocationID = @locationID
								, EndTime = dbo.FN_ConvertCifTime([ScheduledArrival])
							FROM ##CifLineLT
							WHERE FileLineNumber = @lineNumber
								
							BREAK -- We dont need any more records for this headcode
						END			
						
					END --WHILE -- loop on all lines for a Headcode (Journey)

					-- Check if journey is already in database
					SET @existingJourneyID = (
						SELECT ID
						FROM Journey 
						WHERE 
							[Headcode] = @headcode
							AND [ValidFrom] = @dateRunsFrom
							AND [ValidTo] = @dateRunsTo
							AND [TrainUID] = @trainUID
							AND [DaysRun] = @daysRun
							AND [StpType] = @stpType
					)
					
					-- if exists check if section points are the same
					IF @existingJourneyID IS NOT NULL 
					BEGIN

							
						-- delete JourneyDate records if not a full extract
						IF @fileType <> 'F'
						BEGIN
							
							DELETE dbo.JourneyDate
							WHERE PermanentJourneyID = @existingJourneyID
								AND DateValue >= CAST(SYSDATETIME() AS date)
							
						END	
						
							
						IF NOT EXISTS 
						(
							SELECT 1 
							FROM
							(
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
								FROM dbo.[SectionPoint]
								WHERE [JourneyID] = @existingJourneyID
								
								UNION ALL
								
								SELECT 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]				
								FROM #tempSectionPoint

							) tmp
							GROUP BY 
									[ID]
									,[LocationID]
									,[ScheduledArrival]
									,[ScheduledDeparture]
									,[ScheduledPass]
									,[PublicArrival]
									,[PublicDeparture]
									,[RecoveryTime]
									,[SectionPointType]
							HAVING COUNT(*) = 1
						)
						-- Jounreys in CIF and in DB are identincal:
						BEGIN
							SET @useExistingJourneyID = 1
							
							-- update @journeyID with existing ID
							SET @journeyID = @existingJourneyID
						
							UPDATE dbo.Journey SET 
								LastFileID = @FileID
							WHERE ID = @journeyID
							
						END
						ELSE
						-- Jounreys in CIF and in DB are different:
						BEGIN
							SET @useExistingJourneyID = 0

							-- close existing Journey
							UPDATE dbo.Journey SET 
								[ValidTo] = SYSDATETIME()
							WHERE ID = @existingJourneyID
							
						END
						
					END
					ELSE
					BEGIN
						SET @useExistingJourneyID = 0
					END --IF @existingJourneyID IS NOT NULL 

					IF @useExistingJourneyID = 0
					BEGIN
						-- insert Journey read from Cif file
					
						-- Populate Journey table
						INSERT INTO dbo.[Journey]
							([Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,[FileID]
							,[LineNumber]
							,[HeadcodeDesc]
							,[LastFileID])
						SELECT
							[Headcode]
							,[StartLocationID]
							,[EndLocationID]
							,[StartTime]
							,[EndTime]
							,[ValidFrom]
							,[ValidTo]
							,[TrainUID]
							,[StpType]
							,[DaysRun]
							,@FileID
							,@bsLine
							,@headcode + ' (' + [dbo].[FN_GetDaysRunDescription](DaysRun) + ')'
							,@FileID
						FROM #tempJourney

						SET @journeyID = @@IDENTITY

						-- Populate SectionPoint table
						INSERT INTO dbo.[SectionPoint]
							([JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType])
						SELECT
							@journeyID --[JourneyID]
							,[ID]
							,[LocationID]
							,[LocationSuffix]
							,[ScheduledArrival]
							,[ScheduledDeparture]
							,[ScheduledPass]
							,[PublicArrival]
							,[PublicDeparture]
							,[RecoveryTime]
							,[SectionPointType]
						FROM #tempSectionPoint
						
						-- Populate Segment and Section 
						EXEC dbo.NSP_CifImportPopulateSectionData @journeyID

					END -- ELSE - IF @useExistingJourneyID = 1

					-- Populate HeadcodeID
					IF @stpType = 'P'
					BEGIN

						-- for existing JourneyID populate only dates from today forward
						INSERT INTO dbo.JourneyDate
							([PermanentJourneyID]
							,[DateValue]
							,[ActualJourneyID])
						SELECT
							@journeyID
							,DateValue
							,@journeyID
						FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun)
						WHERE DateValue >= CAST(SYSDATETIME() AS date) OR @useExistingJourneyID = 0

						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Perm Sched, Existing JourneyID:' 
								ELSE 'Perm Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
						
					END
					ELSE -- @stpType = '0'
					BEGIN
						
						-- for existing JourneyID populate only dates from today forward
						UPDATE dbo.JourneyDate SET
							[ActualJourneyID] = @journeyID
						FROM dbo.JourneyDate
						INNER JOIN dbo.Journey ON Journey.ID = PermanentJourneyID
						WHERE TrainUID = @trainUID
							AND DateValue IN (SELECT DateValue FROM dbo.FN_GetServiceDate(@dateRunsFrom, @dateRunsTo, @daysRun))
							AND dbo.FN_CompareJorney(PermanentJourneyID, @journeyID) = 0
							AND (DateValue >= CAST(SYSDATETIME() AS date) OR @useExistingJourneyID = 0)
					
						SET @logEntry = @printOut 
							+ CASE @useExistingJourneyID
								WHEN 1 THEN 'Over Sched, Existing JourneyID:' 
								ELSE 'Over Sched, New JourneyID:'
							END
							+ LTRIM(STR(@journeyID)) + ' days updated: ' + LTRIM(STR(@@ROWCOUNT))
						
						UPDATE dbo.LoadCifFileData SET Status = @logEntry WHERE FileLineNumber = @bsLine AND FileID = @FileID 
							
					END
					
			END -- IF @stpType IN ('P', 'O') 

		END -- IF @bsTransactionType IN ('N')
		
	END
	
	-- File was fully processed
	UPDATE dbo.LoadCifFile SET Status = 'Processed' WHERE FileID = @FileID

END -- end stored procedure
GO
