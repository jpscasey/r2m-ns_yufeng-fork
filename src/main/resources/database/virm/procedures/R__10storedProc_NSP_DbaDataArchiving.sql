SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DbaDataArchiving', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DbaDataArchiving' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DbaDataArchiving AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DbaDataArchiving]
AS
BEGIN

	SET NOCOUNT ON; 

	RETURN

	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE ChannelValue', 10, 1) WITH NOWAIT;
	SET NOCOUNT ON;
	DECLARE @msg varchar(150)
	DECLARE @timestamp datetime2(3) = (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM dbo.ChannelValue (NOLOCK)) --'2012-04-01 00:00:00'
	DECLARE @maxTimestamp datetime2(3) = DATEADD(D, -7, SYSDATETIME())
	DECLARE @t datetime2(3)
	DECLARE @backupFile varchar(1000) 
	DECLARE @interval int = 30
	DECLARE @interval_OLD int = 1440 -- interval to be used for data older than 1 month - ScotRail can insert a spurious record from a LONG time ago, screwing up the time to complete the archiving
	DECLARE @sql varchar(1000)

	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM dbo.ChannelValue 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(ms, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
		SET @timestamp = DATEADD(n, @interval, @timestamp);
				
		WAITFOR DELAY '00:00:01'
				
	END
	-------------------------------------------------------------------------------
	-- VehicleEvent
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE VehicleEvent', 10, 1) WITH NOWAIT;
	SET @t				= NULL
	SET @msg			= NULL
	SET @timestamp		= (SELECT ISNULL(MIN(Timestamp),SYSDATETIME()) FROM dbo.VehicleEvent (NOLOCK)) --'2012-04-01 00:00:00'
	SET @maxTimestamp	= DATEADD(D, -7, SYSDATETIME())
	SET @interval		= 30 --mins
	
	
	WHILE 1=1
	BEGIN

		IF @timestamp > @maxTimestamp
			BREAK

		SET @t = SYSDATETIME()

		DELETE FROM dbo.VehicleEvent 
		WHERE TimeStamp <= @timestamp
		OPTION (MAXDOP 1)
		
		SET @msg = 
			'Timestamp: ' 
			+ CONVERT(char(23), @timestamp, 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @@ROWCOUNT), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
		
	
		IF @timestamp < DATEADD(month,-1,SYSDATETIME())
		BEGIN
			SET @timestamp = DATEADD(n, @interval_OLD, @timestamp)
		END
		ELSE
		BEGIN
			SET @timestamp = DATEADD(n, @interval, @timestamp);
		END
				
		WAITFOR DELAY '00:00:01'
				
	END


	-------------------------------------------------------------------------------
	-- ChannelValue
	-------------------------------------------------------------------------------
	RAISERROR('
	DELETE Fault', 10, 1) WITH NOWAIT;

	DECLARE @chunk int	= 1000
	DECLARE @rowCount int
	DECLARE @rowCountFaultChannelValue int
	SET @t				= NULL
	SET @msg			= NULL
	
	DECLARE @FaultToDelete TABLE(
		ID int
	)


	WHILE 1=1
	BEGIN
		SET @t = SYSDATETIME()
		
		INSERT INTO @FaultToDelete(ID)
		SELECT TOP (@chunk) ID
		FROM dbo.Fault 
		WHERE CreateTime < DATEADD(d, -7, SYSDATETIME());
	
		DELETE dbo.FaultChannelValue
		WHERE FaultID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCountFaultChannelValue = @@ROWCOUNT
			
		DELETE dbo.Fault
		WHERE ID IN (SELECT ID FROM @FaultToDelete)
		
		SET @rowCount = @@ROWCOUNT 

		IF @rowCount = 0 
			BREAK

		SET @msg = 
			'Current Time: ' 
			+ CONVERT(char(23), SYSDATETIME(), 121)
			+ '	Delete duration: ' 
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(23), DATEDIFF(s, @t, SYSDATETIME())), 10), 'NULL')
			+ '	Records deleted: '
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @rowCount), 10), 'NULL')
			+ '/'
			+ ISNULL(RIGHT('		 ' + CONVERT(varchar(20), @rowCountFaultChannelValue), 10), 'NULL');

		RAISERROR(@msg, 10, 1) WITH NOWAIT;
			
		WAITFOR DELAY '00:00:01'
	END

END -- stored procedure
GO
