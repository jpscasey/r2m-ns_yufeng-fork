SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_DeleteChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_DeleteChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_DeleteChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_DeleteChannelValue]
(
@date DATE)
AS
BEGIN
/*
For some reason ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
Is not Running in the sp so additional code has been created 
The Following runs in the Job to clear the old partitions

DECLARE @TopDate date
SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name ='PF_ChannelValue_Date'
order by value

While @date > @TopDate
BEGIN

	ALTER PARTITION FUNCTION [PF_ChannelValue_Date]()MERGE RANGE (CONVERT(VARCHAR(8), @topDate, 112))
			
	SELECT Top 1 @TopDate = CAST(value as date)  FROM  sys.partition_range_values r
	INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
	Where f.name ='PF_ChannelValue_Date'
	order by value

END


*/

DECLARE @TopDate date
, @Boundary int

SELECT Top 1  @TopDate = CAST(value as date) ,@Boundary = boundary_id FROM  sys.partition_range_values r
INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
Where f.name ='PF_ChannelValue_Date'
ORDER BY value ASC


SET @Boundary =@Boundary+1

BEGIN TRY
	BEGIN TRAN


		While @date > @TopDate
		BEGIN
		
			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE Table_NAme = 'ChannelValue_DelPart')
				DROP TABLE ChannelValue_DelPart

			CREATE TABLE ChannelValue_DelPart(
				[ID] [bigint] NOT NULL,
				[UpdateRecord] [bit] NOT NULL,
				[UnitID] [int] NOT NULL,
				[RecordInsert] [datetime2](3) NOT NULL,
				[TimeStamp] [datetime2](3) NOT NULL,
				Col1 decimal (9,6) NULL
, Col2 decimal (9,6) NULL
, Col3 Bit NULL
, Col4 Bit NULL
, Col5 Bit NULL
, Col6 Bit NULL
, Col7 Bit NULL
, Col8 Bit NULL
, Col9 Bit NULL
, Col10 Bit NULL
, Col11 Bit NULL
, Col12 Bit NULL
, Col13 Bit NULL
, Col14 Bit NULL
, Col15 Bit NULL
, Col16 Bit NULL
, Col17 Bit NULL
, Col18 Bit NULL
, Col19 Bit NULL
, Col20 Bit NULL
, Col21 decimal(5,1) NULL
, Col22 decimal(5,1) NULL
, Col23 Bit NULL
, Col24 Bit NULL
, Col25 Bit NULL
, Col26 Bit NULL
, Col27 Bit NULL
, Col28 Bit NULL
, Col29 Bit NULL
, Col30 Bit NULL
, Col31 Bit NULL
, Col32 Bit NULL
, Col33 Bit NULL
, Col34 Bit NULL
, Col35 Bit NULL
, Col36 Integer NULL
, Col37 Bit NULL
, Col38 decimal(5,1) NULL
, Col39 Bit NULL
, Col40 Bit NULL
, Col41 Bit NULL
, Col42 Bit NULL
, Col43 Bit NULL
, Col44 Bit NULL
, Col45 Bit NULL
, Col46 Bit NULL
, Col47 Bit NULL
, Col48 Bit NULL
, Col49 Integer NULL
, Col50 Bit NULL
, Col51 Bit NULL
, Col52 Integer NULL
, Col53 Bit NULL
, Col54 Bit NULL
, Col55 Integer NULL
, Col56 Bit NULL
, Col57 Bit NULL
, Col58 Bit NULL
, Col59 Integer NULL
, Col60 Bit NULL
, Col61 decimal(5,1) NULL
, Col62 Bit NULL
, Col63 Bit NULL
, Col64 Bit NULL
, Col65 Bit NULL
, Col66 Bit NULL
, Col67 Bit NULL
, Col68 Bit NULL
, Col69 Bit NULL
, Col70 Bit NULL
, Col71 Bit NULL
, Col72 Bit NULL
, Col73 Bit NULL
, Col74 Bit NULL
, Col75 Bit NULL
, Col76 Bit NULL
, Col77 Bit NULL
, Col78 Bit NULL
, Col79 Bit NULL
, Col80 Bit NULL
, Col81 Bit NULL
, Col82 Bit NULL
, Col83 Bit NULL
, Col84 Bit NULL
, Col85 Bit NULL
, Col86 Bit NULL
, Col87 Bit NULL
, Col88 Bit NULL
, Col89 Bit NULL
, Col90 Bit NULL
, Col91 Bit NULL
, Col92 Bit NULL
, Col93 Integer NULL
, Col94 Bit NULL
, Col95 Bit NULL
, Col96 Integer NULL
, Col97 Bit NULL
, Col98 Bit NULL
, Col99 Integer NULL
, Col100 Bit NULL
, Col101 Integer NULL
, Col102 Integer NULL
, Col103 Bit NULL
, Col104 Integer NULL
, Col105 Bit NULL
, Col106 decimal(5,1) NULL
, Col107 Bit NULL
, Col108 Bit NULL
, Col109 Bit NULL
, Col110 Bit NULL
, Col111 Bit NULL
, Col112 Bit NULL
, Col113 Bit NULL
, Col114 Bit NULL
, Col115 Bit NULL
, Col116 Bit NULL
, Col117 Bit NULL
, Col118 Bit NULL
, Col119 Bit NULL
, Col120 Bit NULL
, Col121 Bit NULL
, Col122 Bit NULL
, Col123 Bit NULL
, Col124 Bit NULL
, Col125 Bit NULL
, Col126 Bit NULL
, Col127 Bit NULL
, Col128 Bit NULL
, Col129 Bit NULL
, Col130 Bit NULL
, Col131 Bit NULL
, Col132 Bit NULL
, Col133 Bit NULL
, Col134 Bit NULL
, Col135 Integer NULL
, Col136 Bit NULL
, Col137 Bit NULL
, Col138 Integer NULL
, Col139 Bit NULL
, Col140 Bit NULL
, Col141 Integer NULL
, Col142 Bit NULL
, Col143 Integer NULL
, Col144 Integer NULL
, Col145 Bit NULL
, Col1001 BigInt NULL
, Col1002 BigInt NULL
, Col1003 BigInt NULL
, Col1004 BigInt NULL
, Col1005 BigInt NULL
, Col1006 BigInt NULL
, Col1007 BigInt NULL
, Col1008 BigInt NULL
, Col1009 BigInt NULL
, Col1010 BigInt NULL
, Col1011 BigInt NULL
, Col1012 BigInt NULL
, Col1013 BigInt NULL
, Col1014 BigInt NULL
, Col1015 BigInt NULL
, Col1016 BigInt NULL
, Col1017 BigInt NULL
, Col1018 BigInt NULL
, Col1019 BigInt NULL
, Col1020 BigInt NULL
, Col1021 BigInt NULL
, Col1022 BigInt NULL
, Col1023 BigInt NULL
, Col1024 BigInt NULL
, Col1025 BigInt NULL
, Col1026 BigInt NULL
, Col1027 BigInt NULL
, Col1028 BigInt NULL
, Col1029 BigInt NULL
, Col1030 BigInt NULL
, Col1031 BigInt NULL
, Col1032 BigInt NULL
, Col1033 BigInt NULL
, Col1034 BigInt NULL
, Col1035 BigInt NULL
, Col1036 BigInt NULL
, Col1037 BigInt NULL
, Col1038 BigInt NULL
, Col1039 BigInt NULL
, Col1040 BigInt NULL
, Col1041 BigInt NULL
, Col1042 BigInt NULL
, Col1043 BigInt NULL
, Col1044 BigInt NULL
, Col1045 BigInt NULL
, Col1046 BigInt NULL
, Col1047 BigInt NULL
, Col1048 BigInt NULL
, Col1049 BigInt NULL
, Col1050 BigInt NULL
, Col1051 BigInt NULL
, Col1052 BigInt NULL
, Col1053 BigInt NULL
, Col1054 BigInt NULL
, Col1055 BigInt NULL
, Col1056 BigInt NULL
, Col1057 BigInt NULL
, Col1058 BigInt NULL
, Col1059 BigInt NULL
, Col1060 BigInt NULL
, Col1061 BigInt NULL
, Col1062 BigInt NULL
, Col1063 BigInt NULL
, Col1064 BigInt NULL
, Col1065 BigInt NULL
, Col1066 BigInt NULL
, Col1067 BigInt NULL
, Col1068 BigInt NULL
, Col1069 BigInt NULL
, Col1070 BigInt NULL
, Col1071 BigInt NULL
, Col1072 BigInt NULL
, Col1073 BigInt NULL
, Col1074 BigInt NULL
, Col1075 BigInt NULL
, Col1076 BigInt NULL
, Col1077 BigInt NULL
, Col1078 BigInt NULL
, Col1079 BigInt NULL
, Col1080 BigInt NULL
, Col1081 BigInt NULL
, Col1082 BigInt NULL
, Col1083 BigInt NULL
, Col1084 BigInt NULL
, Col1085 BigInt NULL
, Col1086 BigInt NULL
, Col1087 BigInt NULL
, Col1088 BigInt NULL
, Col1089 BigInt NULL
, Col1090 BigInt NULL
, Col1091 BigInt NULL
, Col1092 BigInt NULL
, Col1093 BigInt NULL
, Col1094 BigInt NULL
, Col1095 BigInt NULL
, Col1096 BigInt NULL
, Col1097 BigInt NULL
, Col1098 BigInt NULL
, Col1099 BigInt NULL
, Col1100 BigInt NULL
, Col1101 BigInt NULL
, Col1102 BigInt NULL
, Col1103 BigInt NULL
, Col1104 BigInt NULL
, Col1105 BigInt NULL
, Col1106 BigInt NULL
, Col1107 BigInt NULL
, Col1108 BigInt NULL
, Col1109 BigInt NULL
, Col1110 BigInt NULL
, Col1111 BigInt NULL
, Col1112 BigInt NULL
, Col1113 BigInt NULL
, Col1114 BigInt NULL
, Col1115 BigInt NULL
, Col1116 BigInt NULL
, Col1117 BigInt NULL
, Col1118 BigInt NULL
, Col1119 BigInt NULL
, Col1120 BigInt NULL
, Col1121 BigInt NULL
, Col1122 BigInt NULL
, Col1123 BigInt NULL
, Col1124 BigInt NULL
, Col1125 BigInt NULL
, Col1126 BigInt NULL
, Col1127 BigInt NULL
, Col1128 BigInt NULL
, Col1129 BigInt NULL
, Col1130 BigInt NULL
, Col1131 BigInt NULL
, Col1132 BigInt NULL
, Col1133 BigInt NULL
, Col1134 BigInt NULL
, Col1135 BigInt NULL
, Col1136 BigInt NULL
, Col1137 BigInt NULL
, Col1138 BigInt NULL
, Col1139 BigInt NULL
, Col1140 BigInt NULL
, Col1141 BigInt NULL
, Col1142 BigInt NULL
, Col1143 BigInt NULL
, Col1144 BigInt NULL
, Col1145 BigInt NULL
, Col1146 BigInt NULL
, Col1147 BigInt NULL
, Col1148 BigInt NULL
, Col1149 BigInt NULL
, Col1150 BigInt NULL
, Col1151 BigInt NULL
, Col1152 BigInt NULL
, Col1153 BigInt NULL
, Col1154 BigInt NULL
, Col1155 BigInt NULL
, Col1156 BigInt NULL
, Col1157 BigInt NULL
, Col1158 BigInt NULL
, Col1159 BigInt NULL
, Col1160 BigInt NULL
, Col1161 BigInt NULL
, Col1162 BigInt NULL
, Col1163 BigInt NULL
, Col1164 BigInt NULL
, Col1165 BigInt NULL
, Col1166 BigInt NULL
, Col1167 BigInt NULL
, Col1168 BigInt NULL
, Col1169 BigInt NULL
, Col1170 BigInt NULL
, Col1171 BigInt NULL
, Col1172 BigInt NULL
, Col1173 BigInt NULL
, Col1174 BigInt NULL
, Col1175 BigInt NULL
, Col1176 BigInt NULL
, Col1177 BigInt NULL
, Col1178 BigInt NULL
, Col1179 BigInt NULL
, Col1180 BigInt NULL
, Col1181 BigInt NULL
, Col1182 BigInt NULL
, Col1183 BigInt NULL
, Col1184 BigInt NULL
, Col1185 BigInt NULL
, Col1186 BigInt NULL
, Col1187 BigInt NULL
, Col1188 BigInt NULL
, Col1189 BigInt NULL
, Col1190 BigInt NULL
, Col1191 BigInt NULL
, Col1192 BigInt NULL
, Col1193 BigInt NULL
, Col1194 BigInt NULL
, Col1195 BigInt NULL
, Col1196 BigInt NULL
, Col1197 BigInt NULL
, Col1198 BigInt NULL
, Col1199 BigInt NULL
, Col1200 BigInt NULL
, Col1201 BigInt NULL
, Col1202 BigInt NULL
, Col1203 BigInt NULL
, Col1204 BigInt NULL
, Col1205 BigInt NULL
, Col1206 BigInt NULL
, Col1207 BigInt NULL
, Col1208 BigInt NULL
, Col1209 BigInt NULL
, Col1210 BigInt NULL
, Col1211 BigInt NULL
, Col1212 BigInt NULL
, Col1213 BigInt NULL

			)

			-- Clustered index has to be created too !!!
			CREATE CLUSTERED INDEX ChannelValue_DelPart_PartitionedIndex ON ChannelValue_DelPart
			( timestamp ASC ) 

			ALTER TABLE ChannelValue SWITCH PARTITION @Boundary TO ChannelValue_DelPart

			DROP TABLE ChannelValue_DelPart
			
		
			SELECT Top 1  @TopDate = CAST(value as date) ,@Boundary = boundary_id FROM  sys.partition_range_values r
			INNER JOIN sys.partition_functions f ON f.function_id = r.function_id
			Where f.name ='PF_ChannelValue_Date'
			and value >@TopDate
			ORDER BY value ASC

			SET @Boundary =@Boundary+1
		END
	COMMIT
END TRY 
BEGIN CATCH
	SELECT 'Error' ,ERROR_NUMBER() AS ErrorNumber  
    ,ERROR_SEVERITY() AS ErrorSeverity  
    ,ERROR_STATE() AS ErrorState  
    ,ERROR_PROCEDURE() AS ErrorProcedure  
    ,ERROR_LINE() AS ErrorLine  
    ,ERROR_MESSAGE() AS ErrorMessage;  
	ROLLBACK
END CATCH

END
GO
