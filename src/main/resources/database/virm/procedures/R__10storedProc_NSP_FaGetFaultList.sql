SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_FaGetFaultList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_FaGetFaultList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_FaGetFaultList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_FaGetFaultList]
(
	@FaultTypeID int
	, @FaultString varchar(50)
)
AS
BEGIN


	SET NOCOUNT ON;
	SELECT 
		FaultID		= fm.ID
		, FaultDesc	= fm.Description
	FROM dbo.FaultMeta fm
	INNER JOIN dbo.FaultType ft ON ft.ID = fm.FaultTypeID
	WHERE ft.ID = ISNULL(@FaultTypeID, ft.ID)
		AND
		(
			Description LIKE '%' + @FaultString + '%'
			OR
			FaultCode LIKE '%' + @FaultString + '%'
		)


END
GO
