SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_Genius_ValidateAndCopy', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_Genius_ValidateAndCopy' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_Genius_ValidateAndCopy AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_Genius_ValidateAndCopy]
	 @FileId int
AS
/******************************************************************************
**	Name:	NSP_Genius_ValidateAndCopy
**	Description:	Checks the imported data against various rules
**	if all checks complete, data is copied to staging table	
**	Return values: Returns the fileId as an output
*******************************************************************************
** CVS Properties
*******************************************************************************
**	$$Author: radek $$
**	$$Date: 2012/11/08 08:18:48 $$
**	$$Revision: 1.2 $$
**	$$Source: /home/cvs/spectrum/scotrail/src/main/database/update_spectrum_db_009.sql,v $$
*******************************************************************************/	

begin
	SET NOCOUNT ON;
	
	DECLARE @unitNo varchar(16);
	DECLARE @logText varchar(500);
	
	begin try
	-- check 1: only one unit per file
	if (select count(distinct UnitNumber) 
	from dbo.GeniusInterfaceData
	where GeniusInterfaceFileId = @fileId) <> 1
	begin
	raiserror(N'File contains more than one Unit Number!', 11, 1);
	exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File contains more than one Unit Number!';	
	end
	
	-- check 2: is unit on this side of the fleet?
	select top(1)
	@unitNo = UnitNumber
	from
	dbo.GeniusInterfaceData
	where 
	GeniusInterfaceFileId = @fileId
	;
	
	if not exists (select 1 from dbo.Unit where UnitNumber = @unitNo)
	begin
		-- unit doesn't exist on this side
		exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. Unit Number not in database.'
		exec dbo.NSP_Genius_WriteLog @fileId, 2, 'File closed. Unit Number not in database.';	
		raiserror(N'File closed. Unit Number not in database.', 11, 1);
		--return 0;	
	end
	
	select @logText = 'Importing data for Unit: ' + @unitNo;
	
	exec dbo.NSP_Genius_WriteLog @fileId, 1, @logText;	

	-- check 3: only deallocations may have no Headcode
	if exists ( 
	select 1 
	from dbo.GeniusInterfaceData 
	where GeniusInterfaceFileId = @fileId
	and HeadCode is null
	and dbo.fn_CheckZero(DiagramId) <> 0)
	begin
	-- Headcode missing
	exec NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'File closed. headcode missing.'
	exec NSP_Genius_WriteLog @fileId, 2, 'For at least one Leg the headcode is missing.';	
	raiserror(N'For at least one Leg the headcode is missing.', 11, 1);
	end

	
	BEGIN TRAN
	
	DELETE FROM dbo.GeniusStaging
	WHERE UnitNumber = @unitNo
	
	INSERT INTO dbo.GeniusStaging
	(GeniusInterfaceFileID
	,DiagramNumber
	,UnitNumber
	,HeadCode
	,TrainStartLocation
	,TrainStartTime
	,TrainEndLocation
	,TrainEndTime
	,LegStartLocation
	,LegStartTime
	,LegEndLocation
	,LegEndTime
	,LegLength
	,VehicleFormation
	,CoupledUnits
	,UnitPosition
	,SequenceNumber)
	SELECT 
	 GeniusInterfaceFileId
	,isnull(DiagramId, 'NA-' + UnitNumber)
	,UnitNumber
	,isnull(HeadCode, 'NA-'+UnitNumber)
	,TrainStartLocation
	,convert(datetime2(3), TrainStartTime, 103)
	,(	
	SELECT TOP 1 LegEndLocation
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndLocation
	,(	
	SELECT TOP 1 convert(datetime2(3), LegEndTime, 103)
	FROM dbo.GeniusInterfaceData gid2
	WHERE GeniusInterfaceFileId = @fileId
	AND gid1.HeadCode = gid2.HeadCode
	AND gid1.TrainStartLocation = gid2.TrainStartLocation
	AND gid1.TrainStartTime = gid2.TrainStartTime
	ORDER BY LegEndTime DESC
	)	--TrainEndTime
	,LegStartLocation
	,convert(datetime2(3), LegStartTime, 103)
	,LegEndLocation
	,convert(datetime2(3), LegEndTime, 103)
	,convert(decimal(5,2), isnull(LegLength, 0))
	,VehicleFormation
	,CoupledUnits
	,convert(tinyint, UnitPosition)
	,convert(int, isnull(SequenceNumber, 0))
	FROM 
	dbo.GeniusInterfaceData gid1
	WHERE 
	GeniusInterfaceFileId = @fileId
	;
	IF @@ERROR <> 0
	ROLLBACK
	ELSE 
	COMMIT TRAN
	
	
	exec dbo.NSP_Genius_UpdateFileStatus @FileId = @fileId, @status = 'Loaded to Staging Table'
	exec dbo.NSP_Genius_WriteLog @fileId, 1, 'Loaded to Staging Table';	
	
	end try
	begin catch
		SELECT @logText = ERROR_MESSAGE();
		exec dbo.NSP_Genius_WriteLog @fileId, 2, @logText;
	exec dbo.NSP_RethrowError;
	return -1;
	end catch	
	
	return 0;
end
GO
