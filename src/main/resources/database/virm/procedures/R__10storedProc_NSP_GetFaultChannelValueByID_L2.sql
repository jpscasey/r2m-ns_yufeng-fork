SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueByID_L2', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueByID_L2' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueByID_L2 AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueByID_L2]
(
    @ID bigint
)
AS
BEGIN
    SET NOCOUNT ON;

    IF @ID IS NULL
        SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.Fault), 0) - 1;
	
	DECLARE @MinID BIGINT = (SELECT MIN(ID) -1  FROM dbo.Fault WHERE ID > @ID)
	
	IF @MinID > @ID SET @ID = @MinID;

    DECLARE @dataSetSize int = 5000
    
    SELECT 
        f.ID
        , FaultMetaID       = f.FaultMetaID
        , FaultCode         = fm.FaultCode
        , FaultCategoryID   = fm.CategoryID
        , FaultCategory     = fc.Category
        , FaultTypeID       = fm.FaultTypeID
        , FaultType         = ft.Name
        , UnitID            = u.ID
        , UnitNumber        = u.UnitNumber
        , UnitType          = u.UnitType
        , timeStamp         = f.CreateTime
		, FaultCount		= f.CountSinceLastMaint
    FROM dbo.Fault f
    INNER JOIN dbo.FaultMeta fm ON f.FaultMetaID = fm.ID
    INNER JOIN dbo.Unit u ON f.FaultUnitID = u.ID
    INNER JOIN dbo.FaultType ft ON fm.FaultTypeID = ft.ID
    INNER JOIN dbo.FaultCategory fc ON fm.CategoryID = fc.ID
    WHERE f.ID BETWEEN (@ID + 1) AND (@ID + @dataSetSize)
    ORDER BY ID

END
GO
