SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueGpsByTimestamp', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueGpsByTimestamp' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueGpsByTimestamp AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueGpsByTimestamp]
    @LastTime bigint,
    @Datetime datetime2(3)
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'
    SELECT TOP 10000 gcv.ID
        ,gcv.UnitID
        ,gcv.Col8001 --Latitude
        ,gcv.Col8002 --Longitude
        ,gcv.Col8003 --Speed
        ,gcv.TimeStamp 
		,LocationId = l.ID
		,Tiploc = l.Tiploc
		,LocationName = l.LocationName
    FROM dbo.VW_ChannelValueGps gcv
	LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE gcv.Col8001 BETWEEN MinLatitude AND MaxLatitude
            AND gcv.Col8002 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
	)
    WHERE gcv.TimeStamp >  DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime))  AND gcv.TimeStamp <= @Datetime ORDER BY ID ASC

END
GO
