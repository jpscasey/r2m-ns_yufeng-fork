SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultChannelValueLatest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultChannelValueLatest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultChannelValueLatest]
    @DatetimeStart DATETIME2(3)
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH ChannelValueLatest
    AS (
        SELECT MAX(cv.TimeStamp) AS TimeStamp
            ,cv.UnitID
        FROM dbo.ChannelValue cv
        WHERE cv.TimeStamp <= @DatetimeStart
            AND cv.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND cv.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY cv.UnitID
        )
    SELECT  
		cv.ID
        , UnitID        = u.ID
        , UnitNumber    = u.UnitNumber
        , UnitType      = u.UnitType
        , UnitStatus    = u.UnitStatus
        , UnitMaintLoc  = u.UnitMaintLoc
        , Headcode      = fs.Headcode
        , FleetCode     = fl.Code
        , ServiceStatus = CASE 
            WHEN fs.Headcode IS NOT NULL
                AND fs.LocationIDHeadcodeStart = l.ID
                THEN 'Ready for Service'
            WHEN fs.Headcode IS NOT NULL
                THEN 'In Service'
            ELSE 'Out of Service'
        END
        , TimeStamp         = CONVERT(datetime2(3), cv.TimeStamp)
        , LocationID    = l.ID
        , Tiploc        = l.Tiploc
        , LocationName  = l.LocationName ,
        -- Channel Values
		[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
		,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
		,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
		,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
		,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
		,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
		,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
		,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
		,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
		,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
		,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
		,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
		,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
		,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
		,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
		,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
		,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
		,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] 
	FROM ChannelValueLatest cvl
    JOIN [dbo].[ChannelValue] cv ON cvl.UnitID = cv.UnitID AND cvl.TimeStamp = cv.TimeStamp     
    INNER JOIN dbo.Unit u ON cv.UnitID = u.ID
    LEFT JOIN dbo.Fleet fl ON u.FleetID = fl.ID
    LEFT JOIN dbo.FleetStatusHistory fs ON
        cv.UnitID = fs.UnitID 
        AND cv.TimeStamp BETWEEN ValidFrom AND ISNULL(ValidTo, DATEADD(d, 1, SYSDATETIME())) -- adding 1 day in case live data is inserted with 
    LEFT JOIN dbo.Location l ON l.ID = (
        SELECT TOP 1 LocationID 
        FROM dbo.LocationArea 
        WHERE col1 BETWEEN MinLatitude AND MaxLatitude
            AND col2 BETWEEN MinLongitude AND MaxLongitude
            AND LocationArea.Type = 'C'
        ORDER BY Priority
    )
END
GO
