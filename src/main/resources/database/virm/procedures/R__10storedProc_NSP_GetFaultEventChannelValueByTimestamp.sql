SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultEventChannelValueByTimestamp', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultEventChannelValueByTimestamp' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelValueByTimestamp AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueByTimestamp]
    @LastTime bigint,
    @Datetime datetime2(3)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @OriginTime datetime2(3) = '1970-01-01 00:00:00.000'
	SELECT TOP 10000 ecv.ID
		,ecv.UnitID
		,ecv.ChannelID
        ,TimeStamp = CONVERT(datetime2(3), ecv.TimeStamp)
		,ecv.Value
	FROM dbo.EventChannelValue ecv
    WHERE TimeStamp > DATEADD(ms, @LastTime%1000, DATEADD(ss, @LastTime/1000, @OriginTime)) AND TimeStamp <= @Datetime ORDER BY ID ASC
END
GO
