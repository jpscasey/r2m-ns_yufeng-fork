SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultEventChannelValueLatest', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultEventChannelValueLatest' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelValueLatest AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatest]
    @DatetimeStart DATETIME2(3)
    ,@UnitID INT = NULL
    ,@MaxTimeDifferenceMinutes int
AS
BEGIN	
	WITH EventChannelValueLatest
    AS (
        SELECT MAX(ecv.TimeStamp) AS TimeStamp
            ,ecv.UnitID
            ,ecv.ChannelID
        FROM dbo.EventChannelValue ecv
        WHERE ecv.TimeStamp <= @DatetimeStart
            AND ecv.TimeStamp >= DATEADD(MINUTE, - @MaxTimeDifferenceMinutes, @DatetimeStart)
            AND ecv.UnitID = @UnitID OR @UnitID IS NULL
        GROUP BY ecv.UnitID
            ,ecv.ChannelID
        )
    SELECT ecv.ID
    ,ecv.UnitID
    ,ecv.ChannelID
    ,ecv.TimeStamp
    ,ecv.Value
    FROM EventChannelValueLatest ecvl
    JOIN dbo.EventChannelValue ecv
        ON ecvl.UnitID = ecv.UnitID
        AND ecvl.ChannelID = ecv.ChannelID
        AND ecvl.TimeStamp = ecv.TimeStamp
        
END
GO
