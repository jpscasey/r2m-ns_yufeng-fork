SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultEventChannelValueLatestByID', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultEventChannelValueLatestByID' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultEventChannelValueLatestByID AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultEventChannelValueLatestByID]
	@ID int
AS
BEGIN
	SET NOCOUNT ON;

	IF @ID IS NULL
		SET @ID = ISNULL((SELECT MAX(ID) FROM dbo.EventChannelValue), 0) - 1
	ELSE
		SET @ID = (
			SELECT TOP 1 (EventChannelValue.ID - 1) 
			FROM dbo.EventChannelValue WITH(NOLOCK)
			WHERE EventChannelValue.ID > @ID
			ORDER BY EventChannelValue.ID ASC
		)
	
	
	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.UnitID
			,ecv.ChannelID
		FROM dbo.EventChannelValue ecv
		WHERE ecv.ID <= @ID
			AND ecv.TimeStamp >= DATEADD(DAY, - 1, SYSDATETIME())
		GROUP BY ecv.UnitID
			,ecv.ChannelID
	)
	SELECT ecv.ID
	,ecv.UnitID
	,ecv.ChannelID
	,ecv.TimeStamp
	,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN dbo.EventChannelValue ecv
		ON ecvl.UnitID = ecv.UnitID
		AND ecvl.ChannelID = ecv.ChannelID
		AND ecvl.TimeStamp = ecv.TimeStamp
END
GO
