SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultFormation', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultFormation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultFormation AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultFormation]
(
	@FaultID int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @primaryUnitID int
	DECLARE @secondaryUnitID int
	DECLARE @tertiaryUnitID int
	DECLARE @faultUnitID int
	
	SELECT 
		@primaryUnitID		= PrimaryUnitID
		, @secondaryUnitID	= SecondaryUnitID
		, @tertiaryUnitID	= TertiaryUnitID 
		, @faultUnitID		= FaultUnitID
	FROM dbo.Fault
	WHERE ID = @FaultID
	
	;WITH CteUnit AS
	(
		SELECT 
			UnitID		= @primaryUnitID
			, UnitOrder	= 1
		UNION 
		SELECT 
			UnitID		= @secondaryUnitID
			, UnitOrder	= 2
		UNION 
		SELECT 
			UnitID		= @tertiaryUnitID
			, UnitOrder	= 3
		
	)
	SELECT 
		UnitID			= u.ID 
		, UnitNumber
		, UnitOrder
		, IsFaultUnit	= CASE u.ID
			WHEN @faultUnitID THEN CONVERT(bit, 1)
			ELSE CONVERT(bit, 0)
		END
	FROM CteUnit
	INNER JOIN dbo.Unit u ON u.ID = CteUnit.UnitID
	
END
GO
