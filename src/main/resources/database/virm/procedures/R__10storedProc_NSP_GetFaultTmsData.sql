SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_GetFaultTmsData', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_GetFaultTmsData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_GetFaultTmsData AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_GetFaultTmsData]
	@FaultID int
AS
BEGIN

	SET NOCOUNT ON;
		
	SELECT
		tf.VehicleNumber
		, tf.FaultDateTime
		, tf.CarNo
		, tf.FaultNo
		, tfl.Description
		, tfl.SwRef
		, tfc.Value	AS CategoryName
		, tff.Value	AS FunctionName
		, cablookup.Value AS Cab
		, mcslookup.Value AS MCS
		, dirlookup.Value AS Direction
		, vltlookup.Value AS Voltage
		, spdlookup.Value AS SpeedSet
		, bctlookup.Value AS BrkCont
		, ebklookup.Value AS EmmerBrk
		, stdlookup.Value AS Stabled
		, mlplookup.Value AS MLP
		, bprlookup.Value AS BrkPr
		, batlookup.Value AS Battery
		, ldslookup.Value AS Loadshed
		, tshlookup.Value AS TracShoreSup
		, ashlookup.Value AS AuxShoreSup
		, bmclookup.Value AS CarBMCoupled
		, bjclookup.Value AS CarBJCoupled
		, tmclookup.Value AS TMCC1
		, tf.Speed
	FROM dbo.TmsFault tf
	LEFT JOIN dbo.TmsFaultLookup tfl ON tf.FaultNo = tfl.FaultNo
	LEFT JOIN dbo.TmsLookup AS tfc ON tfl.TmsCategoryID = tfc.ValueID AND tfc.Type = 'tmsFaultCategory'
	LEFT JOIN dbo.TmsLookup AS tff ON tfl.TmsFunctionID = tff.ValueID AND tff.Type = 'tmsFaultFunction'
	LEFT JOIN dbo.TmsLookup AS cablookup ON tf.CapId = cablookup.ValueId AND cablookup.Type = 'cab'
	LEFT JOIN dbo.TmsLookup AS mcslookup ON tf.McsId = mcslookup.ValueId AND mcslookup.Type = 'mcs'
	LEFT JOIN dbo.TmsLookup AS dirlookup ON tf.DirectionId = dirlookup.ValueId AND dirlookup.Type = 'direction'
	LEFT JOIN dbo.TmsLookup AS vltlookup ON tf.VoltageId = vltlookup.ValueId AND vltlookup.Type = 'voltage'
	LEFT JOIN dbo.TmsLookup AS spdlookup ON tf.SpeedSetId = spdlookup.ValueId AND spdlookup.Type = 'speedSet'
	LEFT JOIN dbo.TmsLookup AS bctlookup ON tf.BrakeContId = bctlookup.ValueId AND bctlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS ebklookup ON tf.EmerBrakeId = ebklookup.ValueId AND ebklookup.Type = 'emergencyBrake'
	LEFT JOIN dbo.TmsLookup AS stdlookup ON tf.StabledId = stdlookup.ValueId AND stdlookup.Type = 'stabled'
	LEFT JOIN dbo.TmsLookup AS mlplookup ON tf.MlpId = mlplookup.ValueId AND mlplookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS bprlookup ON tf.BrakePrId = bprlookup.ValueId AND bprlookup.Type = 'brakeState'
	LEFT JOIN dbo.TmsLookup AS batlookup ON tf.BatteryId = batlookup.ValueId AND batlookup.Type = 'battery'
	LEFT JOIN dbo.TmsLookup AS ldslookup ON tf.LoadshedId = ldslookup.ValueId AND ldslookup.Type = 'loadshed'
	LEFT JOIN dbo.TmsLookup AS tshlookup ON tf.TracShoreSupId = tshlookup.ValueId AND tshlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS ashlookup ON tf.AuxShoreSupId = ashlookup.ValueId AND ashlookup.Type = 'shoreSupState'
	LEFT JOIN dbo.TmsLookup AS bmclookup ON tf.CarBmCoupledId = bmclookup.ValueId AND bmclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS bjclookup ON tf.CarBjCoupledId = bjclookup.ValueId AND bjclookup.Type = 'couplingState'
	LEFT JOIN dbo.TmsLookup AS tmclookup ON tf.Tmcc1Id = tmclookup.ValueId AND tmclookup.Type = 'tmcc1'
	WHERE tf.FaultID = @FaultID
	
END
GO
