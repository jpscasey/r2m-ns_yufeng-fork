SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricChannelValueGps', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricChannelValueGps' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricChannelValueGps AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricChannelValueGps](
    @DateTimeStart datetime2(3)
    , @UnitID int
)
AS
	BEGIN
		
		IF @UnitID IS NULL
    	BEGIN
        	PRINT 'Unit Number not in a database'
        	RETURN -1 
    	END
    	
    	IF @DateTimeStart IS NULL
        	SET @DateTimeStart = DATEADD(hh, 1, SYSDATETIME())
        	
        	
        ;WITH CteLatestRecords 
    	AS
   		(
     		SELECT 
           		Unit.ID AS UnitID
            	, (
            	    SELECT TOP 1 TimeStamp
          	  	    FROM dbo.VW_ChannelValueGps
          	 	    WHERE TimeStamp BETWEEN DATEADD(hh, -24, @DateTimeStart) AND @DateTimeStart
          	 	       AND UnitID = Unit.ID 
             	    ORDER BY TimeStamp DESC
            	) AS MaxChannelValueGpsTimeStamp
        	FROM dbo.Unit
        	WHERE ID = @UnitID
  		) 
		
		SELECT		
		CAST([TimeStamp] AS datetime2(3)) AS TimeStamp,
		fs.UnitID,
		Col8001,--Latitude
		Col8002,--Longitude
		Col8003,--Speed
		l.LocationCode AS Location
		FROM dbo.VW_FleetStatus fs WITH (NOLOCK)
		INNER JOIN CteLatestRecords clr WITH (NOLOCK) ON clr.UnitID = fs.UnitID
		LEFT JOIN dbo.VW_ChannelValueGps cvg WITH (NOLOCK) ON cvg.TimeStamp = clr.MaxChannelValueGpsTimeStamp AND cvg.UnitID = clr.UnitID 
		LEFT JOIN dbo.Location l ON l.ID = (
				SELECT TOP 1 LocationID
				FROM dbo.LocationArea
				WHERE cvg.Col8001 BETWEEN MinLatitude AND MaxLatitude
					AND cvg.Col8002 BETWEEN MinLongitude AND MaxLongitude
				ORDER BY Priority
		)
		WHERE fs.UnitId = @UnitId
	
	END	
GO
