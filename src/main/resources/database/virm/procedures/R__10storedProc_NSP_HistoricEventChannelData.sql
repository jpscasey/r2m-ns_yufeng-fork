SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricEventChannelData', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricEventChannelData' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelData AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelData]
    @DateTimeEnd DATETIME2(3)
    ,@Interval INT -- number of seconds
    ,@UnitID INT
    ,@SelectColList VARCHAR(4000)
AS

BEGIN
    SET NOCOUNT ON;

    DECLARE @sql varchar(max)

    IF @DateTimeEnd IS NULL
        SET @DateTimeEnd = SYSDATETIME()

    DECLARE @DateTimeStart datetime2(3) = DATEADD(s, - @Interval, @DateTimeEnd)

    DECLARE @vehicleTypeID int = (SELECT VehicleTypeID FROM VW_Vehicle WHERE ID = @UnitID)

    SET @sql = '
SELECT
    TimeStamp = CAST(TimeStamp AS DATETIME2(3))
    ,ChannelID
    ,Value
	,UnitID
FROM EventChannelValue
WHERE
    UnitID = ' + CAST(@UnitID AS VARCHAR(10)) + '
    AND ChannelID IN (' + @SelectColList + ')' + '
    AND Timestamp BETWEEN ''' + CONVERT(VARCHAR(23), @DateTimeStart, 121) + ''' AND ''' + CONVERT(VARCHAR(23), @DateTimeEnd, 121)  + ''''

    EXEC (@sql)
END
GO
