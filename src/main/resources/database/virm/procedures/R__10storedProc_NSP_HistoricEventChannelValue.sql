SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricEventChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricEventChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricEventChannelValue] 
    @timestamp DATETIME2(3)
    ,@unitId INT
AS

BEGIN

SET NOCOUNT ON;
DECLARE  @timestampInternal DATETIME2(3)

	IF @timestamp IS NULL
		SET @timestampInternal = DATEADD(hh, 1, SYSDATETIME())
	ELSE 
		SET @timestampInternal = @timestamp

	;WITH EventChannelValueLatest
	AS (
		SELECT MAX(ecv.TimeStamp) AS TimeStamp
			,ecv.ChannelID
		FROM EventChannelValue ecv 
		WHERE ecv.UnitID = @unitId
			AND ecv.TimeStamp BETWEEN DATEADD(DAY, -1, @timestampInternal) AND @timestampInternal
		GROUP BY ecv.ChannelID
		)
	SELECT ecv.TimeStamp
	    ,ecv.UnitID
		,ecv.ChannelID
		,ecv.Value
	FROM EventChannelValueLatest ecvl
	JOIN EventChannelValue ecv 
		ON ecvl.TimeStamp = ecv.TimeStamp
		AND ecvl.ChannelID = ecv.ChannelID
        AND ecv.UnitID = @unitId
END
GO
