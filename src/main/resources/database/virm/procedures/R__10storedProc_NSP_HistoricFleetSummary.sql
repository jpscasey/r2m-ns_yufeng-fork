SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_HistoricFleetSummary', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_HistoricFleetSummary' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_HistoricFleetSummary AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_HistoricFleetSummary](
	@DateTimeEnd datetime2(3)
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

	DECLARE @interval int = 3600 -- value in seconds 
	DECLARE @DateTimeStart datetime2(3) = DATEADD(s, - @interval, @DateTimeEnd);
	
	IF @DateTimeEnd IS NULL 
		SET @DateTimeEnd = SYSDATETIME()
	
	-- Get Latest ChannelValue timestamp for each of the vehicles transmitting
	CREATE TABLE #LatestChannelValue (
		UnitID int PRIMARY KEY
		, LastTimeStamp datetime2(3)
	)
	
	INSERT #LatestChannelValue( UnitID , LastTimeStamp)
	SELECT c.UnitID, MAX(c.TimeStamp) LastTimeStamp
	FROM dbo.ChannelValue c
	WHERE 
		c.Timestamp >= @DateTimeStart
		AND c.Timestamp <= @DateTimeEnd
	GROUP BY c.UnitID
	
	-- Get totals for Vehicle based faults
	CREATE TABLE #currentUnitFault (
		UnitID int NOT NULL PRIMARY KEY 
		, P1FaultNumber int NULL
		, P2FaultNumber int NULL
		, P3FaultNumber int NULL
		, P4FaultNumber int NULL
		, P5FaultNumber int NULL
		, RecoveryNumber int NULL
		, NotAcknowledgedNumber int NULL
		, UnitStatus varchar(20) NULL
	)
	
	INSERT INTO #currentUnitFault
	SELECT 
		f.FaultUnitID
		, SUM(CASE f.FaultTypeID WHEN 1 THEN 1 ELSE 0 END) AS P1FaultNumber
		, SUM(CASE f.FaultTypeID WHEN 2 THEN 1 ELSE 0 END) AS P2FaultNumber
		, SUM(CASE f.FaultTypeID WHEN 3 THEN 1 ELSE 0 END) AS P3FaultNumber
		, SUM(CASE f.FaultTypeID WHEN 4 THEN 1 ELSE 0 END) AS P4FaultNumber
		, SUM(CASE f.FaultTypeID WHEN 5 THEN 1 ELSE 0 END) AS P5FaultNumber
		, SUM(CASE f.HasRecovery WHEN 1 THEN 1 ELSE 0 END) AS RecoveryNumber
		, SUM(CASE f.IsAcknowledged WHEN 0 THEN 1 ELSE 0 END) AS NotAcknowledgedNumber
		, SUM(CASE f.UnitStatus WHEN 'BVD' THEN 1 WHEN 'OVERSTAND' THEN 1 ELSE 0 END) AS UnitStatus
	FROM dbo.VW_IX_Fault f WITH (NOEXPAND)
	WHERE f.FaultUnitID IS NOT NULL
		AND f.ReportingOnly = 0
		AND IsCurrent = 1
	GROUP BY f.FaultUnitID

	SELECT 
		v.UnitID
		, v.UnitNumber
		, v.UnitType
		, fs.FleetFormationID
		, fs.UnitPosition
		, Diagram				= fs.Diagram
		, Headcode			 = fs.Headcode
		, VehicleID			 = v.ID
		, VehicleNumber		 = v.VehicleNumber
		, VehiclePosition		= v.VehicleOrder
		, VehicleType			= v.Type
		, Location			 = l.LocationCode 
		, P1FaultNumber		= ISNULL(vf.P1FaultNumber,0)
		, P2FaultNumber		= ISNULL(vf.P2FaultNumber,0)
		, P3FaultNumber		= ISNULL(vf.P3FaultNumber,0)
		, P4FaultNumber		= ISNULL(vf.P4FaultNumber,0)
		, P5FaultNumber		= ISNULL(vf.P5FaultNumber,0)
		, FaultRecoveryNumber = ISNULL(vf.RecoveryNumber,0)
		, FaultNotAcknowledgedNumber = ISNULL(vf.NotAcknowledgedNumber,0)
		, UnitStatus		=ISNULL(vf.UnitStatus,0)
		, ServiceStatus = CASE 
			WHEN fs.Headcode IS NOT NULL
				AND fs.LocationIDHeadcodeStart = l.ID
				THEN 'Ready for Service'
			WHEN fs.Headcode IS NOT NULL
				THEN 'In Service'
			ELSE 'Out of Service'
		END
		,TimeStamp			 = CAST(cv.[TimeStamp] AS datetime2(3))
		,cv.[Col1]
		,cv.[Col2]
		,cv.[Col3]
		,cv.[Col4]
		,cv.[Col101]
		,cv.[Col143]
		--,cv.[Col6]
		--,cv.[Col30]
		,v.FleetCode
	FROM dbo.VW_Vehicle v 
	INNER JOIN #LatestChannelValue lcv ON v.UnitID = lcv.UnitID
	INNER JOIN dbo.ChannelValue cv WITH (NOLOCK) ON cv.TimeStamp = lcv.LastTimeStamp AND cv.UnitID = lcv.UnitID
	LEFT JOIN #currentUnitFault vf ON v.UnitID = vf.UnitID
	LEFT JOIN dbo.FleetStatusHistory fs ON v.UnitID = fs.UnitID 
		AND cv.TimeStamp BETWEEN fs.ValidFrom AND ISNULL(fs.ValidTo, CONVERT(datetime2(3),DATEADD(d, 1, SYSDATETIME()))) -- adding 1 day in case live data is inserted with 
	LEFT JOIN dbo.Location l ON l.ID = (
		SELECT TOP 1 la.LocationID 
		FROM dbo.LocationArea  la
		WHERE cv.col1 BETWEEN la.MinLatitude AND la.MaxLatitude
			AND cv.col2 BETWEEN la.MinLongitude AND la.MaxLongitude
		ORDER BY la.Priority
	)
	
END
GO
