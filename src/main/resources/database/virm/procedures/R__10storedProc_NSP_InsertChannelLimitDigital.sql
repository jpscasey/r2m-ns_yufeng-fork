SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertChannelLimitDigital', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertChannelLimitDigital' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelLimitDigital AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelLimitDigital]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @Red varchar(1)
	, @Orange varchar(1)
	, @Green varchar(1)
	, @Blue varchar(1)
	, @Yellow varchar(1)
	, @Grey varchar(1)
)
AS 
BEGIN

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	SET @ChannelName = (SELECT Name FROM dbo.Channel WHERE ID = @ChannelID)
	
	-- @Red - FAULT
	IF ISNUMERIC(@Red) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 7 --Fault
			, @ChannelName + ' - FAULT' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
		
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Red, @Red, 1, 1
			
	END

	-- @Orange - WARNING
	IF ISNUMERIC(@Orange) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 4
			, @ChannelName + ' - WARNING' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Orange, @Orange, 1, 1
			
	END
	
	-- @Green - NORMAL
	IF ISNUMERIC(@Green) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 1 --Fault
			, @ChannelName + ' - NORMAL' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Green, @Green, 1, 1
			
	END

	-- @Blue -- OFF
	IF ISNUMERIC(@Blue) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 3
			, @ChannelName + ' - OFF' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Blue, @Blue, 1, 1
			
	END

	-- @Yellow -- ON
	IF ISNUMERIC(@Yellow) = 1
	BEGIN

		SET @ChannelRuleID = NULL
		
		INSERT INTO [dbo].[ChannelRule]([ChannelID],[ChannelStatusID],[Name])
		SELECT
			@ChannelID
			, 2
			, @ChannelName + ' - ON' 

		SET @ChannelRuleID = SCOPE_IDENTITY()
			
		INSERT INTO [dbo].[ChannelRuleValidation]([ChannelRuleID],[ChannelID],[MinValue],[MaxValue],[MinInclusive],[MaxInclusive])
		SELECT 
			@ChannelRuleID, @ChannelID, @Yellow, @Yellow, 1, 1
			
	END
		
END
GO
