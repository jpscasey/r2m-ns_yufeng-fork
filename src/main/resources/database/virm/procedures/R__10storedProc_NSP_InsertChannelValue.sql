SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertChannelValue](
		@Timestamp datetime2(3),
		@UnitID int,
	    @UpdateRecord bit ,
		@Col1 decimal (9,6) = NULL ,@Col2 decimal (9,6) = NULL ,@Col3 Bit = NULL ,@Col4 Bit = NULL ,@Col5 Bit = NULL ,@Col6 Bit = NULL ,@Col7 Bit = NULL ,@Col8 Bit = NULL ,@Col9 Bit = NULL ,@Col10 Bit = NULL ,
@Col11 Bit = NULL ,@Col12 Bit = NULL ,@Col13 Bit = NULL ,@Col14 Bit = NULL ,@Col15 Bit = NULL ,@Col16 Bit = NULL ,@Col17 Bit = NULL ,@Col18 Bit = NULL ,@Col19 Bit = NULL ,@Col20 Bit = NULL ,
@Col21 decimal(5,1) = NULL ,@Col22 decimal(5,1) = NULL ,@Col23 Bit = NULL ,@Col24 Bit = NULL ,@Col25 Bit = NULL ,@Col26 Bit = NULL ,@Col27 Bit = NULL ,@Col28 Bit = NULL ,@Col29 Bit = NULL ,@Col30 Bit = NULL ,
@Col31 Bit = NULL ,@Col32 Bit = NULL ,@Col33 Bit = NULL ,@Col34 Bit = NULL ,@Col35 Bit = NULL ,@Col36 Integer = NULL ,@Col37 Bit = NULL ,@Col38 decimal(5,1) = NULL ,@Col39 Bit = NULL ,@Col40 Bit = NULL ,
@Col41 Bit = NULL ,@Col42 Bit = NULL ,@Col43 Bit = NULL ,@Col44 Bit = NULL ,@Col45 Bit = NULL ,@Col46 Bit = NULL ,@Col47 Bit = NULL ,@Col48 Bit = NULL ,@Col49 Integer = NULL ,@Col50 Bit = NULL ,
@Col51 Bit = NULL ,@Col52 Integer = NULL ,@Col53 Bit = NULL ,@Col54 Bit = NULL ,@Col55 Integer = NULL ,@Col56 Bit = NULL ,@Col57 Bit = NULL ,@Col58 Bit = NULL ,@Col59 Integer = NULL ,@Col60 Bit = NULL ,
@Col61 decimal(5,1) = NULL ,@Col62 Bit = NULL ,@Col63 Bit = NULL ,@Col64 Bit = NULL ,@Col65 Bit = NULL ,@Col66 Bit = NULL ,@Col67 Bit = NULL ,@Col68 Bit = NULL ,@Col69 Bit = NULL ,@Col70 Bit = NULL ,
@Col71 Bit = NULL ,@Col72 Bit = NULL ,@Col73 Bit = NULL ,@Col74 Bit = NULL ,@Col75 Bit = NULL ,@Col76 Bit = NULL ,@Col77 Bit = NULL ,@Col78 Bit = NULL ,@Col79 Bit = NULL ,@Col80 Bit = NULL ,@Col81 Bit = NULL ,
@Col82 Bit = NULL ,@Col83 Bit = NULL ,@Col84 Bit = NULL ,@Col85 Bit = NULL ,@Col86 Bit = NULL ,@Col87 Bit = NULL ,@Col88 Bit = NULL ,@Col89 Bit = NULL ,@Col90 Bit = NULL ,@Col91 Bit = NULL ,@Col92 Bit = NULL ,
@Col93 Integer = NULL ,@Col94 Bit = NULL ,@Col95 Bit = NULL ,@Col96 Integer = NULL ,@Col97 Bit = NULL ,@Col98 Bit = NULL ,@Col99 Integer = NULL ,@Col100 Bit = NULL ,@Col101 Integer = NULL ,
@Col102 Integer = NULL ,@Col103 Bit = NULL ,@Col104 Integer = NULL ,@Col105 Bit = NULL ,@Col106 decimal(5,1) = NULL ,@Col107 Bit = NULL ,@Col108 Bit = NULL ,@Col109 Bit = NULL ,@Col110 Bit = NULL ,
@Col111 Bit = NULL ,@Col112 Bit = NULL ,@Col113 Bit = NULL ,@Col114 Bit = NULL ,@Col115 Bit = NULL ,@Col116 Bit = NULL ,@Col117 Bit = NULL ,@Col118 Bit = NULL ,@Col119 Bit = NULL ,@Col120 Bit = NULL ,
@Col121 Bit = NULL ,@Col122 Bit = NULL ,@Col123 Bit = NULL ,@Col124 Bit = NULL ,@Col125 Bit = NULL ,@Col126 Bit = NULL ,@Col127 Bit = NULL ,@Col128 Bit = NULL ,@Col129 Bit = NULL ,@Col130 Bit = NULL ,
@Col131 Bit = NULL ,@Col132 Bit = NULL ,@Col133 Bit = NULL ,@Col134 Bit = NULL ,@Col135 Integer = NULL ,@Col136 Bit = NULL ,@Col137 Bit = NULL ,@Col138 Integer = NULL ,@Col139 Bit = NULL ,@Col140 Bit = NULL ,
@Col141 Integer = NULL ,@Col142 Bit = NULL ,@Col143 Integer = NULL ,@Col144 Integer = NULL ,@Col145 Bit = NULL ,@Col1001 BigInt = NULL ,@Col1002 BigInt = NULL ,@Col1003 BigInt = NULL ,@Col1004 BigInt = NULL ,
@Col1005 BigInt = NULL ,@Col1006 BigInt = NULL ,@Col1007 BigInt = NULL ,@Col1008 BigInt = NULL ,@Col1009 BigInt = NULL ,@Col1010 BigInt = NULL ,@Col1011 BigInt = NULL ,@Col1012 BigInt = NULL ,
@Col1013 BigInt = NULL ,@Col1014 BigInt = NULL ,@Col1015 BigInt = NULL ,@Col1016 BigInt = NULL ,@Col1017 BigInt = NULL ,@Col1018 BigInt = NULL ,@Col1019 BigInt = NULL ,@Col1020 BigInt = NULL ,
@Col1021 BigInt = NULL ,@Col1022 BigInt = NULL ,@Col1023 BigInt = NULL ,@Col1024 BigInt = NULL ,@Col1025 BigInt = NULL ,@Col1026 BigInt = NULL ,@Col1027 BigInt = NULL ,@Col1028 BigInt = NULL ,
@Col1029 BigInt = NULL ,@Col1030 BigInt = NULL ,@Col1031 BigInt = NULL ,@Col1032 BigInt = NULL ,@Col1033 BigInt = NULL ,@Col1034 BigInt = NULL ,@Col1035 BigInt = NULL ,@Col1036 BigInt = NULL ,
@Col1037 BigInt = NULL ,@Col1038 BigInt = NULL ,@Col1039 BigInt = NULL ,@Col1040 BigInt = NULL ,@Col1041 BigInt = NULL ,@Col1042 BigInt = NULL ,@Col1043 BigInt = NULL ,@Col1044 BigInt = NULL ,
@Col1045 BigInt = NULL ,@Col1046 BigInt = NULL ,@Col1047 BigInt = NULL ,@Col1048 BigInt = NULL ,@Col1049 BigInt = NULL ,@Col1050 BigInt = NULL ,@Col1051 BigInt = NULL ,@Col1052 BigInt = NULL ,
@Col1053 BigInt = NULL ,@Col1054 BigInt = NULL ,@Col1055 BigInt = NULL ,@Col1056 BigInt = NULL ,@Col1057 BigInt = NULL ,@Col1058 BigInt = NULL ,@Col1059 BigInt = NULL ,@Col1060 BigInt = NULL ,
@Col1061 BigInt = NULL ,@Col1062 BigInt = NULL ,@Col1063 BigInt = NULL ,@Col1064 BigInt = NULL ,@Col1065 BigInt = NULL ,@Col1066 BigInt = NULL ,@Col1067 BigInt = NULL ,@Col1068 BigInt = NULL ,
@Col1069 BigInt = NULL ,@Col1070 BigInt = NULL ,@Col1071 BigInt = NULL ,@Col1072 BigInt = NULL ,@Col1073 BigInt = NULL ,@Col1074 BigInt = NULL ,@Col1075 BigInt = NULL ,@Col1076 BigInt = NULL ,
@Col1077 BigInt = NULL ,@Col1078 BigInt = NULL ,@Col1079 BigInt = NULL ,@Col1080 BigInt = NULL ,@Col1081 BigInt = NULL ,@Col1082 BigInt = NULL ,@Col1083 BigInt = NULL ,@Col1084 BigInt = NULL ,
@Col1085 BigInt = NULL ,@Col1086 BigInt = NULL ,@Col1087 BigInt = NULL ,@Col1088 BigInt = NULL ,@Col1089 BigInt = NULL ,@Col1090 BigInt = NULL ,@Col1091 BigInt = NULL ,@Col1092 BigInt = NULL ,
@Col1093 BigInt = NULL ,@Col1094 BigInt = NULL ,@Col1095 BigInt = NULL ,@Col1096 BigInt = NULL ,@Col1097 BigInt = NULL ,@Col1098 BigInt = NULL ,@Col1099 BigInt = NULL ,@Col1100 BigInt = NULL ,
@Col1101 BigInt = NULL ,@Col1102 BigInt = NULL ,@Col1103 BigInt = NULL ,@Col1104 BigInt = NULL ,@Col1105 BigInt = NULL ,@Col1106 BigInt = NULL ,@Col1107 BigInt = NULL ,@Col1108 BigInt = NULL ,
@Col1109 BigInt = NULL ,@Col1110 BigInt = NULL ,@Col1111 BigInt = NULL ,@Col1112 BigInt = NULL ,@Col1113 BigInt = NULL ,@Col1114 BigInt = NULL ,@Col1115 BigInt = NULL ,@Col1116 BigInt = NULL ,
@Col1117 BigInt = NULL ,@Col1118 BigInt = NULL ,@Col1119 BigInt = NULL ,@Col1120 BigInt = NULL ,@Col1121 BigInt = NULL ,@Col1122 BigInt = NULL ,@Col1123 BigInt = NULL ,@Col1124 BigInt = NULL ,
@Col1125 BigInt = NULL ,@Col1126 BigInt = NULL ,@Col1127 BigInt = NULL ,@Col1128 BigInt = NULL ,@Col1129 BigInt = NULL ,@Col1130 BigInt = NULL ,@Col1131 BigInt = NULL ,@Col1132 BigInt = NULL ,
@Col1133 BigInt = NULL ,@Col1134 BigInt = NULL ,@Col1135 BigInt = NULL ,@Col1136 BigInt = NULL ,@Col1137 BigInt = NULL ,@Col1138 BigInt = NULL ,@Col1139 BigInt = NULL ,@Col1140 BigInt = NULL ,
@Col1141 BigInt = NULL ,@Col1142 BigInt = NULL ,@Col1143 BigInt = NULL ,@Col1144 BigInt = NULL ,@Col1145 BigInt = NULL ,@Col1146 BigInt = NULL ,@Col1147 BigInt = NULL ,@Col1148 BigInt = NULL ,
@Col1149 BigInt = NULL ,@Col1150 BigInt = NULL ,@Col1151 BigInt = NULL ,@Col1152 BigInt = NULL ,@Col1153 BigInt = NULL ,@Col1154 BigInt = NULL ,@Col1155 BigInt = NULL ,@Col1156 BigInt = NULL ,
@Col1157 BigInt = NULL ,@Col1158 BigInt = NULL ,@Col1159 BigInt = NULL ,@Col1160 BigInt = NULL ,@Col1161 BigInt = NULL ,@Col1162 BigInt = NULL ,@Col1163 BigInt = NULL ,@Col1164 BigInt = NULL ,
@Col1165 BigInt = NULL ,@Col1166 BigInt = NULL ,@Col1167 BigInt = NULL ,@Col1168 BigInt = NULL ,@Col1169 BigInt = NULL ,@Col1170 BigInt = NULL ,@Col1171 BigInt = NULL ,@Col1172 BigInt = NULL ,
@Col1173 BigInt = NULL ,@Col1174 BigInt = NULL ,@Col1175 BigInt = NULL ,@Col1176 BigInt = NULL ,@Col1177 BigInt = NULL ,@Col1178 BigInt = NULL ,@Col1179 BigInt = NULL ,@Col1180 BigInt = NULL ,
@Col1181 BigInt = NULL ,@Col1182 BigInt = NULL ,@Col1183 BigInt = NULL ,@Col1184 BigInt = NULL ,@Col1185 BigInt = NULL ,@Col1186 BigInt = NULL ,@Col1187 BigInt = NULL ,@Col1188 BigInt = NULL ,
@Col1189 BigInt = NULL ,@Col1190 BigInt = NULL ,@Col1191 BigInt = NULL ,@Col1192 BigInt = NULL ,@Col1193 BigInt = NULL ,@Col1194 BigInt = NULL ,@Col1195 BigInt = NULL ,@Col1196 BigInt = NULL ,
@Col1197 BigInt = NULL ,@Col1198 BigInt = NULL ,@Col1199 BigInt = NULL ,@Col1200 BigInt = NULL ,@Col1201 BigInt = NULL ,@Col1202 BigInt = NULL ,@Col1203 BigInt = NULL ,@Col1204 BigInt = NULL ,
@Col1205 BigInt = NULL ,@Col1206 BigInt = NULL ,@Col1207 BigInt = NULL ,@Col1208 BigInt = NULL ,@Col1209 BigInt = NULL ,@Col1210 BigInt = NULL ,@Col1211 BigInt = NULL ,@Col1212 BigInt = NULL ,
@Col1213 BigInt = NULL ,@Col146 Integer = NULL ,@Col147 Integer = NULL ,@Col148 Integer = NULL
		)
		AS
		BEGIN

SET NOCOUNT ON;

DECLARE @IDs TABLE(ID BIGINT);
	DECLARE @CVID BIGINT


INSERT INTO [dbo].[ChannelValue]
           ([UnitID]
           ,[TimeStamp]
		   ,[UpdateRecord]
		   ,[Col1] ,[Col2] ,[Col3] ,[Col4] ,[Col5] ,[Col6] ,[Col7] ,[Col8] ,[Col9] ,[Col10] ,[Col11] ,[Col12] ,[Col13] ,[Col14] ,[Col15] ,[Col16] ,[Col17] ,[Col18] ,[Col19] ,[Col20] ,[Col21] ,[Col22] ,[Col23] ,[Col24] ,[Col25]
 ,[Col26] ,[Col27] ,[Col28] ,[Col29] ,[Col30] ,[Col31] ,[Col32] ,[Col33] ,[Col34] ,[Col35] ,[Col36] ,[Col37] ,[Col38] ,[Col39] ,[Col40] ,[Col41] ,[Col42] ,[Col43] ,[Col44] ,[Col45] ,[Col46] ,[Col47] ,[Col48] ,[Col49]
 ,[Col50] ,[Col51] ,[Col52] ,[Col53] ,[Col54] ,[Col55] ,[Col56] ,[Col57] ,[Col58] ,[Col59] ,[Col60] ,[Col61] ,[Col62] ,[Col63] ,[Col64] ,[Col65] ,[Col66] ,[Col67] ,[Col68] ,[Col69] ,[Col70] ,[Col71] ,[Col72] ,[Col73]
 ,[Col74] ,[Col75] ,[Col76] ,[Col77] ,[Col78] ,[Col79] ,[Col80] ,[Col81] ,[Col82] ,[Col83] ,[Col84] ,[Col85] ,[Col86] ,[Col87] ,[Col88] ,[Col89] ,[Col90] ,[Col91] ,[Col92] ,[Col93] ,[Col94] ,[Col95] ,[Col96] ,[Col97]
 ,[Col98] ,[Col99] ,[Col100] ,[Col101] ,[Col102] ,[Col103] ,[Col104] ,[Col105] ,[Col106] ,[Col107] ,[Col108] ,[Col109] ,[Col110] ,[Col111] ,[Col112] ,[Col113] ,[Col114] ,[Col115] ,[Col116] ,[Col117] ,[Col118] ,[Col119]
 ,[Col120] ,[Col121] ,[Col122] ,[Col123] ,[Col124] ,[Col125] ,[Col126] ,[Col127] ,[Col128] ,[Col129] ,[Col130] ,[Col131] ,[Col132] ,[Col133] ,[Col134] ,[Col135] ,[Col136] ,[Col137] ,[Col138] ,[Col139] ,[Col140] 
 ,[Col141] ,[Col142] ,[Col143] ,[Col144] ,[Col145] ,[Col1001] ,[Col1002] ,[Col1003] ,[Col1004] ,[Col1005] ,[Col1006] ,[Col1007] ,[Col1008] ,[Col1009] ,[Col1010] ,[Col1011] ,[Col1012] ,[Col1013] ,[Col1014] ,[Col1015] 
 ,[Col1016] ,[Col1017] ,[Col1018] ,[Col1019] ,[Col1020] ,[Col1021] ,[Col1022] ,[Col1023] ,[Col1024] ,[Col1025] ,[Col1026] ,[Col1027] ,[Col1028] ,[Col1029] ,[Col1030] ,[Col1031] ,[Col1032] ,[Col1033] ,[Col1034] 
 ,[Col1035] ,[Col1036] ,[Col1037] ,[Col1038] ,[Col1039] ,[Col1040] ,[Col1041] ,[Col1042] ,[Col1043] ,[Col1044] ,[Col1045] ,[Col1046] ,[Col1047] ,[Col1048] ,[Col1049] ,[Col1050] ,[Col1051] ,[Col1052] ,[Col1053]
 ,[Col1054] ,[Col1055] ,[Col1056] ,[Col1057] ,[Col1058] ,[Col1059] ,[Col1060] ,[Col1061] ,[Col1062] ,[Col1063] ,[Col1064] ,[Col1065] ,[Col1066] ,[Col1067] ,[Col1068] ,[Col1069] ,[Col1070] ,[Col1071] ,[Col1072] 
 ,[Col1073] ,[Col1074] ,[Col1075] ,[Col1076] ,[Col1077] ,[Col1078] ,[Col1079] ,[Col1080] ,[Col1081] ,[Col1082] ,[Col1083] ,[Col1084] ,[Col1085] ,[Col1086] ,[Col1087] ,[Col1088] ,[Col1089] ,[Col1090] ,[Col1091] 
 ,[Col1092] ,[Col1093] ,[Col1094] ,[Col1095] ,[Col1096] ,[Col1097] ,[Col1098] ,[Col1099] ,[Col1100] ,[Col1101] ,[Col1102] ,[Col1103] ,[Col1104] ,[Col1105] ,[Col1106] ,[Col1107] ,[Col1108] ,[Col1109] ,[Col1110] 
 ,[Col1111] ,[Col1112] ,[Col1113] ,[Col1114] ,[Col1115] ,[Col1116] ,[Col1117] ,[Col1118] ,[Col1119] ,[Col1120] ,[Col1121] ,[Col1122] ,[Col1123] ,[Col1124] ,[Col1125] ,[Col1126] ,[Col1127] ,[Col1128] ,[Col1129] 
 ,[Col1130] ,[Col1131] ,[Col1132] ,[Col1133] ,[Col1134] ,[Col1135] ,[Col1136] ,[Col1137] ,[Col1138] ,[Col1139] ,[Col1140] ,[Col1141] ,[Col1142] ,[Col1143] ,[Col1144] ,[Col1145] ,[Col1146] ,[Col1147] ,[Col1148] 
 ,[Col1149] ,[Col1150] ,[Col1151] ,[Col1152] ,[Col1153] ,[Col1154] ,[Col1155] ,[Col1156] ,[Col1157] ,[Col1158] ,[Col1159] ,[Col1160] ,[Col1161] ,[Col1162] ,[Col1163] ,[Col1164] ,[Col1165] ,[Col1166] ,[Col1167]
 ,[Col1168] ,[Col1169] ,[Col1170] ,[Col1171] ,[Col1172] ,[Col1173] ,[Col1174] ,[Col1175] ,[Col1176] ,[Col1177] ,[Col1178] ,[Col1179] ,[Col1180] ,[Col1181] ,[Col1182] ,[Col1183] ,[Col1184] ,[Col1185] ,[Col1186]
 ,[Col1187] ,[Col1188] ,[Col1189] ,[Col1190] ,[Col1191] ,[Col1192] ,[Col1193] ,[Col1194] ,[Col1195] ,[Col1196] ,[Col1197] ,[Col1198] ,[Col1199] ,[Col1200] ,[Col1201] ,[Col1202] ,[Col1203] ,[Col1204] ,[Col1205] 
 ,[Col1206] ,[Col1207] ,[Col1208] ,[Col1209] ,[Col1210] ,[Col1211] ,[Col1212] ,[Col1213] ,[Col146] ,[Col147] ,[Col148]
           )
  	SELECT TOP 1
		@UnitID
		,@Timestamp
		,@UpdateRecord
		,ISNULL(@Col1, Col1) ,ISNULL(@Col2, Col2) ,ISNULL(@Col3, Col3) ,ISNULL(@Col4, Col4) ,ISNULL(@Col5, Col5) ,ISNULL(@Col6, Col6) ,ISNULL(@Col7, Col7) ,ISNULL(@Col8, Col8) ,ISNULL(@Col9, Col9) ,ISNULL(@Col10, Col10)
 ,ISNULL(@Col11, Col11) ,ISNULL(@Col12, Col12) ,ISNULL(@Col13, Col13) ,ISNULL(@Col14, Col14) ,ISNULL(@Col15, Col15) ,ISNULL(@Col16, Col16) ,ISNULL(@Col17, Col17) ,ISNULL(@Col18, Col18) ,ISNULL(@Col19, Col19)
 ,ISNULL(@Col20, Col20) ,ISNULL(@Col21, Col21) ,ISNULL(@Col22, Col22) ,ISNULL(@Col23, Col23) ,ISNULL(@Col24, Col24) ,ISNULL(@Col25, Col25) ,ISNULL(@Col26, Col26) ,ISNULL(@Col27, Col27) ,ISNULL(@Col28, Col28)
 ,ISNULL(@Col29, Col29) ,ISNULL(@Col30, Col30) ,ISNULL(@Col31, Col31) ,ISNULL(@Col32, Col32) ,ISNULL(@Col33, Col33) ,ISNULL(@Col34, Col34) ,ISNULL(@Col35, Col35) ,ISNULL(@Col36, Col36) ,ISNULL(@Col37, Col37)
 ,ISNULL(@Col38, Col38) ,ISNULL(@Col39, Col39) ,ISNULL(@Col40, Col40) ,ISNULL(@Col41, Col41) ,ISNULL(@Col42, Col42) ,ISNULL(@Col43, Col43) ,ISNULL(@Col44, Col44) ,ISNULL(@Col45, Col45) ,ISNULL(@Col46, Col46)
 ,ISNULL(@Col47, Col47) ,ISNULL(@Col48, Col48) ,ISNULL(@Col49, Col49) ,ISNULL(@Col50, Col50) ,ISNULL(@Col51, Col51) ,ISNULL(@Col52, Col52) ,ISNULL(@Col53, Col53) ,ISNULL(@Col54, Col54) ,ISNULL(@Col55, Col55)
 ,ISNULL(@Col56, Col56) ,ISNULL(@Col57, Col57) ,ISNULL(@Col58, Col58) ,ISNULL(@Col59, Col59) ,ISNULL(@Col60, Col60) ,ISNULL(@Col61, Col61) ,ISNULL(@Col62, Col62) ,ISNULL(@Col63, Col63) ,ISNULL(@Col64, Col64)
 ,ISNULL(@Col65, Col65) ,ISNULL(@Col66, Col66) ,ISNULL(@Col67, Col67) ,ISNULL(@Col68, Col68) ,ISNULL(@Col69, Col69) ,ISNULL(@Col70, Col70) ,ISNULL(@Col71, Col71) ,ISNULL(@Col72, Col72) ,ISNULL(@Col73, Col73)
 ,ISNULL(@Col74, Col74) ,ISNULL(@Col75, Col75) ,ISNULL(@Col76, Col76) ,ISNULL(@Col77, Col77) ,ISNULL(@Col78, Col78) ,ISNULL(@Col79, Col79) ,ISNULL(@Col80, Col80) ,ISNULL(@Col81, Col81) ,ISNULL(@Col82, Col82)
 ,ISNULL(@Col83, Col83) ,ISNULL(@Col84, Col84) ,ISNULL(@Col85, Col85) ,ISNULL(@Col86, Col86) ,ISNULL(@Col87, Col87) ,ISNULL(@Col88, Col88) ,ISNULL(@Col89, Col89) ,ISNULL(@Col90, Col90) ,ISNULL(@Col91, Col91)
 ,ISNULL(@Col92, Col92) ,ISNULL(@Col93, Col93) ,ISNULL(@Col94, Col94) ,ISNULL(@Col95, Col95) ,ISNULL(@Col96, Col96) ,ISNULL(@Col97, Col97) ,ISNULL(@Col98, Col98) ,ISNULL(@Col99, Col99) ,ISNULL(@Col100, Col100)
 ,ISNULL(@Col101, Col101) ,ISNULL(@Col102, Col102) ,ISNULL(@Col103, Col103) ,ISNULL(@Col104, Col104) ,ISNULL(@Col105, Col105) ,ISNULL(@Col106, Col106) ,ISNULL(@Col107, Col107) ,ISNULL(@Col108, Col108) 
 ,ISNULL(@Col109, Col109) ,ISNULL(@Col110, Col110) ,ISNULL(@Col111, Col111) ,ISNULL(@Col112, Col112) ,ISNULL(@Col113, Col113) ,ISNULL(@Col114, Col114) ,ISNULL(@Col115, Col115) ,ISNULL(@Col116, Col116) 
 ,ISNULL(@Col117, Col117) ,ISNULL(@Col118, Col118) ,ISNULL(@Col119, Col119) ,ISNULL(@Col120, Col120) ,ISNULL(@Col121, Col121) ,ISNULL(@Col122, Col122) ,ISNULL(@Col123, Col123) ,ISNULL(@Col124, Col124) 
 ,ISNULL(@Col125, Col125) ,ISNULL(@Col126, Col126) ,ISNULL(@Col127, Col127) ,ISNULL(@Col128, Col128) ,ISNULL(@Col129, Col129) ,ISNULL(@Col130, Col130) ,ISNULL(@Col131, Col131) ,ISNULL(@Col132, Col132) 
 ,ISNULL(@Col133, Col133) ,ISNULL(@Col134, Col134) ,ISNULL(@Col135, Col135) ,ISNULL(@Col136, Col136) ,ISNULL(@Col137, Col137) ,ISNULL(@Col138, Col138) ,ISNULL(@Col139, Col139) ,ISNULL(@Col140, Col140) 
 ,ISNULL(@Col141, Col141) ,ISNULL(@Col142, Col142) ,ISNULL(@Col143, Col143) ,ISNULL(@Col144, Col144) ,ISNULL(@Col145, Col145) ,ISNULL(@Col1001, Col1001) ,ISNULL(@Col1002, Col1002) ,ISNULL(@Col1003, Col1003) 
 ,ISNULL(@Col1004, Col1004) ,ISNULL(@Col1005, Col1005) ,ISNULL(@Col1006, Col1006) ,ISNULL(@Col1007, Col1007) ,ISNULL(@Col1008, Col1008) ,ISNULL(@Col1009, Col1009) ,ISNULL(@Col1010, Col1010) ,ISNULL(@Col1011, Col1011)
 ,ISNULL(@Col1012, Col1012) ,ISNULL(@Col1013, Col1013) ,ISNULL(@Col1014, Col1014) ,ISNULL(@Col1015, Col1015) ,ISNULL(@Col1016, Col1016) ,ISNULL(@Col1017, Col1017) ,ISNULL(@Col1018, Col1018) ,ISNULL(@Col1019, Col1019)
 ,ISNULL(@Col1020, Col1020) ,ISNULL(@Col1021, Col1021) ,ISNULL(@Col1022, Col1022) ,ISNULL(@Col1023, Col1023) ,ISNULL(@Col1024, Col1024) ,ISNULL(@Col1025, Col1025) ,ISNULL(@Col1026, Col1026) ,ISNULL(@Col1027, Col1027)
 ,ISNULL(@Col1028, Col1028) ,ISNULL(@Col1029, Col1029) ,ISNULL(@Col1030, Col1030) ,ISNULL(@Col1031, Col1031) ,ISNULL(@Col1032, Col1032) ,ISNULL(@Col1033, Col1033) ,ISNULL(@Col1034, Col1034) ,ISNULL(@Col1035, Col1035)
 ,ISNULL(@Col1036, Col1036) ,ISNULL(@Col1037, Col1037) ,ISNULL(@Col1038, Col1038) ,ISNULL(@Col1039, Col1039) ,ISNULL(@Col1040, Col1040) ,ISNULL(@Col1041, Col1041) ,ISNULL(@Col1042, Col1042) ,ISNULL(@Col1043, Col1043) 
 ,ISNULL(@Col1044, Col1044) ,ISNULL(@Col1045, Col1045) ,ISNULL(@Col1046, Col1046) ,ISNULL(@Col1047, Col1047) ,ISNULL(@Col1048, Col1048) ,ISNULL(@Col1049, Col1049) ,ISNULL(@Col1050, Col1050) ,ISNULL(@Col1051, Col1051)
 ,ISNULL(@Col1052, Col1052) ,ISNULL(@Col1053, Col1053) ,ISNULL(@Col1054, Col1054) ,ISNULL(@Col1055, Col1055) ,ISNULL(@Col1056, Col1056) ,ISNULL(@Col1057, Col1057) ,ISNULL(@Col1058, Col1058) ,ISNULL(@Col1059, Col1059)
 ,ISNULL(@Col1060, Col1060) ,ISNULL(@Col1061, Col1061) ,ISNULL(@Col1062, Col1062) ,ISNULL(@Col1063, Col1063) ,ISNULL(@Col1064, Col1064) ,ISNULL(@Col1065, Col1065) ,ISNULL(@Col1066, Col1066) ,ISNULL(@Col1067, Col1067)
 ,ISNULL(@Col1068, Col1068) ,ISNULL(@Col1069, Col1069) ,ISNULL(@Col1070, Col1070) ,ISNULL(@Col1071, Col1071) ,ISNULL(@Col1072, Col1072) ,ISNULL(@Col1073, Col1073) ,ISNULL(@Col1074, Col1074) ,ISNULL(@Col1075, Col1075)
 ,ISNULL(@Col1076, Col1076) ,ISNULL(@Col1077, Col1077) ,ISNULL(@Col1078, Col1078) ,ISNULL(@Col1079, Col1079) ,ISNULL(@Col1080, Col1080) ,ISNULL(@Col1081, Col1081) ,ISNULL(@Col1082, Col1082) ,ISNULL(@Col1083, Col1083)
 ,ISNULL(@Col1084, Col1084) ,ISNULL(@Col1085, Col1085) ,ISNULL(@Col1086, Col1086) ,ISNULL(@Col1087, Col1087) ,ISNULL(@Col1088, Col1088) ,ISNULL(@Col1089, Col1089) ,ISNULL(@Col1090, Col1090) ,ISNULL(@Col1091, Col1091)
 ,ISNULL(@Col1092, Col1092) ,ISNULL(@Col1093, Col1093) ,ISNULL(@Col1094, Col1094) ,ISNULL(@Col1095, Col1095) ,ISNULL(@Col1096, Col1096) ,ISNULL(@Col1097, Col1097) ,ISNULL(@Col1098, Col1098) ,ISNULL(@Col1099, Col1099)
 ,ISNULL(@Col1100, Col1100) ,ISNULL(@Col1101, Col1101) ,ISNULL(@Col1102, Col1102) ,ISNULL(@Col1103, Col1103) ,ISNULL(@Col1104, Col1104) ,ISNULL(@Col1105, Col1105) ,ISNULL(@Col1106, Col1106) ,ISNULL(@Col1107, Col1107)
 ,ISNULL(@Col1108, Col1108) ,ISNULL(@Col1109, Col1109) ,ISNULL(@Col1110, Col1110) ,ISNULL(@Col1111, Col1111) ,ISNULL(@Col1112, Col1112) ,ISNULL(@Col1113, Col1113) ,ISNULL(@Col1114, Col1114) ,ISNULL(@Col1115, Col1115)
 ,ISNULL(@Col1116, Col1116) ,ISNULL(@Col1117, Col1117) ,ISNULL(@Col1118, Col1118) ,ISNULL(@Col1119, Col1119) ,ISNULL(@Col1120, Col1120) ,ISNULL(@Col1121, Col1121) ,ISNULL(@Col1122, Col1122) ,ISNULL(@Col1123, Col1123)
 ,ISNULL(@Col1124, Col1124) ,ISNULL(@Col1125, Col1125) ,ISNULL(@Col1126, Col1126) ,ISNULL(@Col1127, Col1127) ,ISNULL(@Col1128, Col1128) ,ISNULL(@Col1129, Col1129) ,ISNULL(@Col1130, Col1130) ,ISNULL(@Col1131, Col1131)
 ,ISNULL(@Col1132, Col1132) ,ISNULL(@Col1133, Col1133) ,ISNULL(@Col1134, Col1134) ,ISNULL(@Col1135, Col1135) ,ISNULL(@Col1136, Col1136) ,ISNULL(@Col1137, Col1137) ,ISNULL(@Col1138, Col1138) ,ISNULL(@Col1139, Col1139)
 ,ISNULL(@Col1140, Col1140) ,ISNULL(@Col1141, Col1141) ,ISNULL(@Col1142, Col1142) ,ISNULL(@Col1143, Col1143) ,ISNULL(@Col1144, Col1144) ,ISNULL(@Col1145, Col1145) ,ISNULL(@Col1146, Col1146) ,ISNULL(@Col1147, Col1147)
 ,ISNULL(@Col1148, Col1148) ,ISNULL(@Col1149, Col1149) ,ISNULL(@Col1150, Col1150) ,ISNULL(@Col1151, Col1151) ,ISNULL(@Col1152, Col1152) ,ISNULL(@Col1153, Col1153) ,ISNULL(@Col1154, Col1154) ,ISNULL(@Col1155, Col1155)
 ,ISNULL(@Col1156, Col1156) ,ISNULL(@Col1157, Col1157) ,ISNULL(@Col1158, Col1158) ,ISNULL(@Col1159, Col1159) ,ISNULL(@Col1160, Col1160) ,ISNULL(@Col1161, Col1161) ,ISNULL(@Col1162, Col1162) ,ISNULL(@Col1163, Col1163)
 ,ISNULL(@Col1164, Col1164) ,ISNULL(@Col1165, Col1165) ,ISNULL(@Col1166, Col1166) ,ISNULL(@Col1167, Col1167) ,ISNULL(@Col1168, Col1168) ,ISNULL(@Col1169, Col1169) ,ISNULL(@Col1170, Col1170) ,ISNULL(@Col1171, Col1171)
 ,ISNULL(@Col1172, Col1172) ,ISNULL(@Col1173, Col1173) ,ISNULL(@Col1174, Col1174) ,ISNULL(@Col1175, Col1175) ,ISNULL(@Col1176, Col1176) ,ISNULL(@Col1177, Col1177) ,ISNULL(@Col1178, Col1178) ,ISNULL(@Col1179, Col1179)
 ,ISNULL(@Col1180, Col1180) ,ISNULL(@Col1181, Col1181) ,ISNULL(@Col1182, Col1182) ,ISNULL(@Col1183, Col1183) ,ISNULL(@Col1184, Col1184) ,ISNULL(@Col1185, Col1185) ,ISNULL(@Col1186, Col1186) ,ISNULL(@Col1187, Col1187)
 ,ISNULL(@Col1188, Col1188) ,ISNULL(@Col1189, Col1189) ,ISNULL(@Col1190, Col1190) ,ISNULL(@Col1191, Col1191) ,ISNULL(@Col1192, Col1192) ,ISNULL(@Col1193, Col1193) ,ISNULL(@Col1194, Col1194) ,ISNULL(@Col1195, Col1195)
 ,ISNULL(@Col1196, Col1196) ,ISNULL(@Col1197, Col1197) ,ISNULL(@Col1198, Col1198) ,ISNULL(@Col1199, Col1199) ,ISNULL(@Col1200, Col1200) ,ISNULL(@Col1201, Col1201) ,ISNULL(@Col1202, Col1202) ,ISNULL(@Col1203, Col1203)
 ,ISNULL(@Col1204, Col1204) ,ISNULL(@Col1205, Col1205) ,ISNULL(@Col1206, Col1206) ,ISNULL(@Col1207, Col1207) ,ISNULL(@Col1208, Col1208) ,ISNULL(@Col1209, Col1209) ,ISNULL(@Col1210, Col1210) ,ISNULL(@Col1211, Col1211)
 ,ISNULL(@Col1212, Col1212) ,ISNULL(@Col1213, Col1213) ,ISNULL(@Col146, Col146) ,ISNULL(@Col147, Col147) ,ISNULL(@Col148, Col148)
	FROM dbo.ChannelValue WITH (NOLOCK)
	WHERE UnitID = @UnitID
	 ORDER BY 1 DESC

END
GO
