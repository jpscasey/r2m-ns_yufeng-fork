SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertEventChannelValue', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertEventChannelValue' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertEventChannelValue AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertEventChannelValue] (
    @Timestamp DATETIME2(3)
    , @TimestampEndTime DATETIME2(3) = NULL
    , @UnitID int
    ,@ChannelId int
--    , @UnitNumber Varchar(16)
--    , @ChannelName VarChar(100)
	, @Value bit
    )
AS
/******************************************************************************
**  Name:           NSP_InsertEventChannelValue
**  Description:    Inserts a new event-based channel value for the SLT fleet
**  Call frequency: Called by the telemetry service when the event happens. 
**                  The event is considered started if a start time (timeStamp) only is given.
**                  The event is considered finished when a end date is given.
**                  
**  Parameters:     @timestamp the timestamp when the event happens
**                  @timestampEndTime the timestamp when the event stopped
**                  @UnitId the Unit ID
**                  @ChannelId the channel ID
**                  exec NSP_InsertEventChannelValue '2018-05-15 14:12:23.110','2018-05-15 14:12:23.110','2401','DIA9002',0
** Try me:          EXECUTE NSP_InsertChannelValue_Text @MCG_ILongitude=4.8,@DBC_ITrainSpeedInt_15=100,@MCG_ILatitude=53.1,@software_version='7.5.6',@UnitNumber='2401',@UpdateRecord=false,@Timestamp='2018-05-15 14:11:18.886'
******************************************************************************/
BEGIN
	DECLARE @SqlStr varchar(max)--,
    --  @ChannelId int,
    --  @UnitID int
    --SELECT TOP 1 @ChannelId = ID FROM dbo.Channel WITH (NOLOCK) where [Name] = @ChannelName
    --SELECT TOP 1 @UnitID = ID FROM dbo.Unit WITH (NOLOCK) where UnitNumber = @UnitNumber

	-- Insert a record into EventChannelValue
    INSERT INTO dbo.EventChannelValue (TimeStamp, TimestampEndTime, UnitID, ChannelId, Value)
    VALUES (@timestamp, @TimestampEndTime, @UnitId, @ChannelId, @Value)

	IF not @TimestampEndTime is null
        SET @timestamp = @TimestampEndTime-- for the latest value I'm expect that TimestampEndTime will always have the latest datetime

	-- Update EventChannelLatestValue
    UPDATE dbo.EventChannelLatestValue 
    SET TimeLastUpdated = @timestamp,
        [Value] = @value,
        LastInsertTime = SYSDATETIME()
    WHERE UnitID = @UnitId 
    and ChannelID = @ChannelId 
    and TimeLastUpdated < @timestamp
        
    IF @@ROWCOUNT = 0-- it did not find an existing record for it
        IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.EventChannelLatestValue WHERE UnitID = @UnitId  and ChannelID = @ChannelId )
        BEGIN TRY
            INSERT INTO dbo.EventChannelLatestValue(TimeLastUpdated, UnitID, ChannelID,Value, LastInsertTime)  
            VALUES (@timestamp , @UnitId, @ChannelId, @value,  SYSDATETIME())
        END TRY
        BEGIN CATCH
            -- concurrency problems might happen and it is fine
        END CATCH
END
GO
