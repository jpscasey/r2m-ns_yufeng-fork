SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertFaultExternalReference', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertFaultExternalReference' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertFaultExternalReference AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertFaultExternalReference](
    @FaultID int
    , @SystemName varchar(50)
    , @ExternalCode varchar(10)
)
AS
BEGIN
    DECLARE @ExternalReferenceID int

    INSERT INTO ExternalReference (ExternalCode, SystemName, Timestamp) VALUES (@ExternalCode, @SystemName, SYSDATETIME())
    SELECT @ExternalReferenceID = @@IDENTITY

    INSERT INTO Fault2ExternalReferenceLink (FaultID, ExternalFaultID) VALUES (@FaultID, @ExternalReferenceID)
    
	UPDATE Fault
	SET LastUpdateTime = SYSDATETIME()
	WHERE ID = @FaultID
END
GO
