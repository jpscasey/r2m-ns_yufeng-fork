SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertLocation', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertLocation' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertLocation AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertLocation]
	@Tiploc varchar(20)
	, @LocationName varchar(100)
	, @LocationCode varchar(10)
	, @Lat decimal(9,6) = NULL
	, @Lng decimal(9,6) = NULL
	, @IsDepot bit = NULL
	, @IsStation bit = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM dbo.Location WHERE Tiploc = @Tiploc)
		UPDATE dbo.Location SET
			[LocationCode] = @LocationCode
			,[LocationName] = @LocationName
			,[Tiploc] = @Tiploc
			,[Lat] = @Lat
			,[Lng] = @Lng
			,[Type] = CASE
				WHEN @IsDepot = 1 THEN 'D'
				WHEN @IsStation = 1 THEN 'S'
				ELSE 'T'
			END
		WHERE Tiploc = @Tiploc
		
	ELSE
		INSERT INTO [dbo].[Location]
			([LocationCode]
			,[LocationName]
			,[Tiploc]
			,[Lat]
			,[Lng]
			,[Type])
		SELECT
			@LocationCode
			,@LocationName
			,@Tiploc
			,@Lat
			,@Lng
			,CASE
				WHEN @IsDepot = 1 THEN 'D'
				WHEN @IsStation = 1 THEN 'S'
				ELSE 'T'
			END 

END
GO
