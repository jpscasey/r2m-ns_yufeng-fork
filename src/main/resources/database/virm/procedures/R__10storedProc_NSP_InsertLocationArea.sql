SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_InsertLocationArea', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_InsertLocationArea' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_InsertLocationArea AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_InsertLocationArea]
(
	@Tiploc varchar(20)
	, @MinLat decimal(9,6)
	, @MaxLat decimal(9,6)
	, @MinLng decimal(9,6)
	, @MaxLng decimal(9,6)
	, @Priority int
	, @Type char(1)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[LocationArea]
		([LocationID]
		,[MinLatitude]
		,[MaxLatitude]
		,[MinLongitude]
		,[MaxLongitude]
		,[Priority]
		,[Type])
	SELECT
		(SELECT ID FROM dbo.Location WHERE Tiploc = @Tiploc)
		, @MinLat
		, @MaxLat
		, @MinLng
		, @MaxLng
		, @Priority
		, @Type

END
GO
