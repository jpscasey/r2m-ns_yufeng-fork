SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaArrivalDeparture', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaArrivalDeparture' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaArrivalDeparture AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaArrivalDeparture]
(
	@JourneyID int
	, @StartDate date
	, @EndDate date
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET DATEFORMAT ymd
	DECLARE @journeyNumber int
	DECLARE @extendedDwellLimit int	-- All Dwells longer by x seconds will be tread as Extended one
	DECLARE @earlyArrivalLimit int 	-- in seconds
	DECLARE @lateDepartureLimit int -- in seconds
	
	-- Read Dwell definition limits 
	SET @extendedDwellLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_extendeddwell')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_extendeddwell')
		ELSE NULL
	END	
	
	SET @earlyArrivalLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_earlyarrival')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_earlyarrival')
		ELSE NULL
	END	
	
	SET @lateDepartureLimit = CASE 
		WHEN ISNUMERIC((SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_latedeparture')) = 1
			THEN (SELECT PropertyValue FROM dbo.Configuration WHERE PropertyName = 'OA_latedeparture')
		ELSE NULL
	END
	
	-- If not defined set up default
	IF @extendedDwellLimit IS NULL 
		SET @extendedDwellLimit = 60
		
	IF @earlyArrivalLimit IS NULL 
		SET @earlyArrivalLimit = 60
		
	IF @lateDepartureLimit IS NULL 
		SET @lateDepartureLimit = 60
	
	SET @EndDate = DATEADD(d, 1, @EndDate)

	IF DATEDIFF(d, @StartDate, @EndDate) = 1
	BEGIN 
		SET @JourneyID = (
			SELECT ActualJourneyID 
			FROM dbo.JourneyDate 
			WHERE PermanentJourneyID = @JourneyID 
				AND DateValue = @StartDate
			)
	END
	
	SET @journeyNumber = (SELECT COUNT(1) 
		FROM dbo.VW_TrainPassageValid tp 		
		WHERE 
			tp.JourneyID = @JourneyID
			AND tp.StartDatetime BETWEEN @StartDate AND @EndDate 
	)
		
	---------------------------------------------------------------------------
	-- header information
	---------------------------------------------------------------------------
	SELECT 
		Headcode		= 
			CASE StpType
				WHEN 'O' THEN HeadcodeDesc + ' STP ' + CONVERT(char(5), StartTime, 121)
				ELSE HeadcodeDesc + ' ' + CONVERT(char(5), StartTime, 121)
			END 
		, Diagram		= 
			' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), StartTime, 108), ':', ''), 4) + 10000, 4)
			+ ' ' + sl.Tiploc
			+ '-' + el.Tiploc
			+ ' ' + RIGHT(LEFT(REPLACE(CONVERT(char(5), EndTime, 108), ':', ''), 4) + 10000, 4)
		, FromDate		= @StartDate
		, ToDate		= CAST(DATEADD(d, -1, @EndDate) AS date) 
		, JourneyNumber	= @journeyNumber 
	FROM dbo.Journey 
	LEFT JOIN dbo.SectionPoint ssp ON Journey.ID = ssp.JourneyID AND ssp.SectionPointType = 'B'
	LEFT JOIN dbo.Location sl ON sl.ID = ssp.LocationID
	LEFT JOIN dbo.SectionPoint esp ON Journey.ID = esp.JourneyID AND esp.SectionPointType = 'E'
	LEFT JOIN dbo.Location el ON el.ID = esp.LocationID
	WHERE Journey.ID = @JourneyID

	---------------------------------------------------------------------------
	-- create temporary table with all section points for @JourneyID in selected time ranges
	---------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CteSectionPoint') IS NOT NULL
		DROP TABLE #CteSectionPoint
	
	SELECT
		SectionPointID
		, TrainPassageID
		, ActualArrivalTime		= DATEDIFF(s, '00:00:00.000', CAST(ActualArrivalDatetime AS time))
		, ActualDepartureTime	= DATEDIFF(s, '00:00:00.000', CAST(ActualDepartureDatetime AS time))
		, ActualArrivalDatetime
		, ActualDepartureDatetime
		, StartDatetime			= CAST(StartDatetime AS date)
		, SectionPointType
	INTO #CteSectionPoint
	FROM dbo.VW_TrainPassageValid tp
	INNER JOIN dbo.TrainPassageSectionPoint ON TrainPassageID = tp.ID
	INNER JOIN dbo.SectionPoint ON SectionPoint.ID = SectionPointID AND tp.JourneyID = SectionPoint.JourneyID
	WHERE tp.JourneyID = @JourneyID
		AND tp.StartDatetime BETWEEN @StartDate AND @EndDate


	---------------------------------------------------------------------------
	-- to avoid divide by 0 problem in next query use NULL value for @journeyNumber if it equals 0
	---------------------------------------------------------------------------
	IF @journeyNumber = 0 
		SET @journeyNumber = NULL

	---------------------------------------------------------------------------
	-- Main report dataset
	-- display Arrival Date for (S - Station, E - End location)
	-- display Departure Date for (S - Station, B - Start location, T - Timing Points)
	---------------------------------------------------------------------------
	SELECT
		Loc						= l.Tiploc 
		, BookedArrive			= ScheduledArrival 
		, BookedDeparture		= ISNULL(ScheduledDeparture, ScheduledPass)
		, RecoveryTime

		, ActualMinArrive		= CASE 
			WHEN SectionPointType IN ('S', 'E')			
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMinDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MIN(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID)
			END 
		
		, ActualMaxArrive		= CASE
			WHEN SectionPointType IN ('S', 'E') 
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMaxDeparture	= CASE 
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(MAX(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
		
		, ActualMeanArrive		= CASE
			WHEN SectionPointType IN ('S', 'E')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualArrivalTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, ActualMeanDeparture	= CASE
			WHEN SectionPointType IN ('S', 'T', 'B')
				THEN (SELECT dbo.FN_ConvertSecondToTimeLong(AVG(ActualDepartureTime)) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) 
			END
			
		, BookedDwell			= dbo.FN_ConvertSecondToTime(DATEDIFF(s, ScheduledArrival, ScheduledDeparture))
		, MinDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MIN(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
		, MaxDwell				= CASE WHEN SectionPointType IN ('S') THEN (SELECT dbo.FN_ConvertSecondToTime(MAX(DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime))) FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID) END
	
		-- Percentages 
		, [% Extended Dwell] = CASE 
			WHEN SectionPointType IN ('S') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) >= DATEDIFF(s, ScheduledArrival, ScheduledDeparture) + @extendedDwellLimit
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Early Arrival] = CASE 
			WHEN SectionPointType IN ('S', 'E') 
				THEN CAST(
					(SELECT COUNT(1)
					FROM #CteSectionPoint
					WHERE SectionPointID = SectionPoint.ID
						AND CAST(ActualArrivalDateTime AS time(0)) < DATEADD(s, - @earlyArrivalLimit, ScheduledArrival)
					) / (@journeyNumber * 0.01)
				AS decimal(9, 2))
		END
		
		, [% Late Departure] = CASE 
				WHEN SectionPointType IN ('S', 'B') 
					THEN CAST(
						(SELECT COUNT(1)
						FROM #CteSectionPoint
						WHERE SectionPointID = SectionPoint.ID
							AND CAST(ActualDepartureDateTime AS time(0)) > DATEADD(s, @lateDepartureLimit, ScheduledDeparture)
						) / (@journeyNumber * 0.01)
					AS decimal(9, 2))
			END
		
		-- Dates for Min/Max passages
		, MinArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime ASC) END
		, MaxArriveDate		= CASE WHEN SectionPointType IN ('S', 'E') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualArrivalTime IS NOT NULL ORDER BY ActualArrivalTime DESC) END
		, MinDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime ASC) END
		, MaxDepartureDate	= CASE WHEN SectionPointType IN ('S', 'T', 'B') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID AND ActualDepartureTime IS NOT NULL ORDER BY ActualDepartureTime DESC) END
		, MinDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) ASC) END
		, MaxDwellDate		= CASE WHEN SectionPointType IN ('S') THEN (SELECT TOP 1 StartDatetime FROM #CteSectionPoint WHERE SectionPointID = SectionPoint.ID ORDER BY DATEDIFF(s, ActualArrivalDatetime, ActualDepartureDatetime) DESC) END

		, IsStation			= CASE 
				WHEN SectionPointType IN ('B', 'S', 'E')
					THEN CAST(1 AS bit)
				ELSE CAST(0 AS bit)
			END
	FROM dbo.SectionPoint
	LEFT JOIN dbo.Location l ON l.ID = LocationID
	WHERE JourneyID = @JourneyID
	ORDER BY SectionPoint.ID

END
GO
