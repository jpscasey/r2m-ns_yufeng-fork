SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_OaGetSegmentList', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_OaGetSegmentList' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_OaGetSegmentList AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_OaGetSegmentList]
(
	@Keyword varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT 
		StartLocationID
		, EndLocationID
		, SegmentName		= sl.Tiploc + ' - ' + el.Tiploc
		, SegmentLongName	= sl.LocationName + ' - ' + el.LocationName
	FROM dbo.Segment s
	INNER JOIN dbo.Location sl ON s.StartLocationID = sl.ID
	INNER JOIN dbo.Location el ON s.EndLocationID = el.ID
	WHERE 
		sl.Tiploc + ' - ' + el.Tiploc LIKE '%' + @Keyword + '%'
		OR sl.LocationName + ' - ' + el.LocationName LIKE '%' + @Keyword + '%'
	
END
GO
