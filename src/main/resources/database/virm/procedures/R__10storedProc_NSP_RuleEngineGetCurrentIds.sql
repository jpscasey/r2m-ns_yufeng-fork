SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_RuleEngineGetCurrentIds', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_RuleEngineGetCurrentIds' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_RuleEngineGetCurrentIds AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_RuleEngineGetCurrentIds]
	@ConfigurationName varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Value FROM dbo.RuleEngineCurrentId WHERE ConfigurationName = @ConfigurationName ORDER BY Position asc

END
GO
