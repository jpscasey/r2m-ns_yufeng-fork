SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateChannelCategory', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateChannelCategory' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateChannelCategory AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_UpdateChannelCategory]
(
	@ChannelID int
	, @ChannelName varchar(100) 
	, @GroupOrder int
	, @Group varchar(100)
)
AS 
BEGIN

	SET NOCOUNT ON;

	SET NOCOUNT ON;
	DECLARE @ChannelRuleID int

	IF NOT EXISTS (SELECT top 1 1 FROM dbo.Channel WHERE ID = @ChannelID)
		SET @ChannelID = (SELECT top 1 ID FROM dbo.Channel WHERE Name = @ChannelName)
	
	IF @ChannelID IS NULL
	BEGIN
		PRINT '** Channel does not exist: ' + ISNULL(@ChannelName, 'NULL')
		RETURN
	END

	UPDATE dbo.Channel SET
		ChannelGroupOrder = @GroupOrder
		, ChannelGroupID = (SELECT top 1 ID FROM dbo.ChannelGroup WHERE Notes =@Group)
		, IsAlwaysDisplayed = 1
	WHERE ID = @ChannelID
END
GO
