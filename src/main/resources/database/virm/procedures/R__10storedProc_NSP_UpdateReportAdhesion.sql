SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
SET NOCOUNT ON

RAISERROR ('-- alter PROCEDURE dbo.NSP_UpdateReportAdhesion', 0, 1) WITH NOWAIT
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME ='NSP_UpdateReportAdhesion' AND ROUTINE_TYPE = 'PROCEDURE' AND SPECIFIC_SCHEMA = 'dbo')

BEGIN
	EXEC ('CREATE PROCEDURE dbo.NSP_UpdateReportAdhesion AS BEGIN RETURN(1) END;')
END
GO

ALTER PROCEDURE [dbo].[NSP_UpdateReportAdhesion]
AS

	SET NOCOUNT ON;
		
	DECLARE @reportTime datetime2(3) = DATEADD(n, -1, SYSDATETIME())
	DECLARE @reportCallFrequency int = 15 -- min

	DELETE [dbo].[ReportAdhesion] 
	WHERE TimeStamp < DATEADD(d, -7, @reportTime)
	
	
	SELECT
		ROW_NUMBER() OVER (PARTITION BY UnitID ORDER BY Timestamp) AS UnitRowNumber
		, ID
		, TimeStamp
		, UnitID
		, Col3
		, Col4
		--, ISNULL(Col54, 0)	AS WSP
		, CAST(NULL AS bit) AS WspChange
		, CAST(NULL AS datetime2(3)) AS EventEndTimestamp
	INTO #temp
	FROM dbo.ChannelValue (NOLOCK)
	WHERE
		TimeStamp BETWEEN DATEADD(n, (-2 * @reportCallFrequency + 1), @reportTime) AND @reportTime
		AND UnitID NOT IN (SELECT unitID FROM dbo.Vehicle v WHERE v.UnitID IN(352, 442)) --VehicleNumber: 77704, 62837
	
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_VehicleRowNumber ON #temp(VehicleID, VehicleRowNumber)
	CREATE NONCLUSTERED INDEX IX_Temp_VehicleID_TimeStamp ON #temp(VehicleID, TimeStamp) INCLUDE (WSP)

	UPDATE t1 SET
		WspChange = CASE
			WHEN t1.WSP = 1 AND t2.WSP = 0
				THEN 1
			ELSE 0
		END
	FROM #temp t1
	INNER JOIN #temp t2 ON t1.UnitID = t2.UnitID 
		AND t1.UnitRowNumber = t2.UnitRowNumber + 1

	UPDATE t1 SET
		[EventEndTimestamp] = (SELECT MIN(t2.Timestamp) FROM #temp t2 WHERE t1.UnitID = t2.UnitID AND t2.TimeStamp > t1.TimeStamp AND WSP = 0)
	FROM #temp t1
	WHERE WspChange = 1
	
	INSERT INTO [dbo].[ReportAdhesion]
	(	
		[ID]
		, [VehicleID]
		, [TimeStamp]
		, [Value]
		, [EventType]
		, [Latitude]
		, [Longitude]
		, [GCourse]
		, [Duration]
		, [EventEndTimestamp]
	)
	SELECT
		t.[ID]
		, v.[VehicleID]
		, t.[TimeStamp]
		, 1				--[Value]
		, 1				--[EventType] = 1 for WSP
		, t.Col1			--[Latitude]
		, t.Col2			--[Longitude]
		, 0				--[GCourse]
		, DATEDIFF(ms, Timestamp, EventEndTimestamp) / 1000.0 --[Duration]
		, [EventEndTimestamp]
	FROM #temp t
	JOIN dbo.Vehicle v ON v.UnitID = t.UnitID
	WHERE WspChange = 1
		AND ID NOT IN (SELECT ID FROM dbo.ReportAdhesion)
GO
