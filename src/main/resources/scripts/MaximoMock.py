#! python3
import datetime
import http.server
import logging
import random
import select
import ssl

# Put your IP address in HOST_NAME
HOST_NAME = "10.27.46.166"
PORT_NUMBER = 8180
# Change the value to switch between different response type
# 0 - error msg | 1 - success msg
MOCKED_RESPONSE_TYPE = 0

class Post(http.server.BaseHTTPRequestHandler):
     
	def do_GET(self):
		"""Respond to a GET request."""
		body = "Body test"
		self.send_response(200)
		logging.info("Info: Response 200 from Get sent...")
		logging.info('body:'+body)
		
		
	def do_POST(self):
		logging.info('Processing POST message...')
		if 1 == MOCKED_RESPONSE_TYPE:
			self.success_msg()
		elif 0 == MOCKED_RESPONSE_TYPE:
			self.error_msg()
		
		
	def success_msg(self):
		logging.info('Producing success response...')
		body = ''
		responseBody = ''  
		length = int(self.headers['Content-Length'])
		body = str(self.rfile.read(length))

		
		responseBody = '{"statusCode": "OK","statusMessageCode": "ESB-RTM-010","statusMessageText": "ServiceRequest data successfully processed","statusMessageDetails": "","statusMessageTimeStamp": "2016-12-17T09:30:47.001Z","notificationNumber": "' + str(random.randint(1,99999999)) + '"}'
		
		self.send_response(200)
		self.send_header('Content-Type', 'application/json;')               
		self.end_headers()
		self.wfile.write(bytes(responseBody, "utf8"))
		logging.info('body:'+body)
		logging.info(responseBody)
		
	def error_msg(self):
		logging.info('Producing error response...')
		body = ''
		responseBody = ''  
		length = int(self.headers['Content-Length'])
		body = str(self.rfile.read(length))

		
		responseBody = '{"statusCode": "ERR","statusMessageCode": "Fault","statusMessageText": "Technical fault in ServiceRequestBS. Some very long long long long long long long long long long long long message here. Technical fault in ServiceRequestBS. Some very long long long long long long long long long long long long message here Technical fault in ServiceRequestBS. Some very long long long long long long long long long long long long message here, Technical fault in ServiceRequestBS. Some very long long long long long long long long long long long long message here. Some very long long long long long long long long long long long long message here. Some very long long long long long long long long long long long long message here.","statusMessageDescription": "More details about the error..","statusMessageType": "E","statusMessageDetails": "","statusMessageTimeStamp": "2016-12-17T09:30:47.001Z","notificationNumber": "' + str(random.randint(1,99999999)) + '"}'
		
		self.send_response(200)
		self.send_header('Content-Type', 'application/json;')               
		self.end_headers()
		self.wfile.write(bytes(responseBody, "utf8"))
		logging.info('body:'+body)
		logging.info(responseBody)
 
if __name__ == '__main__':
    FORMAT = "%(asctime)-15s %(message)s"
    logging.basicConfig(level=logging.DEBUG, format=FORMAT)
    server_class = http.server.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), Post)    
    logging.info("Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info("Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))
