var nx = nx || {};
nx.ns = nx.ns || {};

nx.ns.rest = (function() {
    return {
        maximo: {
            CREATE_SERVICE_REQUEST: 'rest/maximoServiceRequest/create',

            createServiceRequest: function(parameters, success, error) {
                $.ajax(this.CREATE_SERVICE_REQUEST, {
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(parameters),
                    cache: false,
                    success: success,
                    error: error
                });
            }
        }
    };
}());