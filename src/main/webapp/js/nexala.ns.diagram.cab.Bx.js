var nx = nx || {};
nx.ns = nx.ns || {};
nx.ns.diagram = nx.ns.diagram || {};

nx.ns.diagram.cab_Bx = (function() {
    function ctor(data, vehicle, paper, scale, xOffSet, yOffSet, direction, clickHandler) {
        
        this.LEFT = -1;
        this.RIGHT = 1;
        
        this.STROKE_WIDTH = 1;
        this.FONT_SIZE = 12;
        this.BLACK_COLOUR = "#000000";
        this.WHITE_COLOUR = "#FFFFFF";
        this.YELLOW_COLOUR = "#FFFF00";
        this.RED_COLOUR = "#FF5959";
        this.ORANGE_COLOUR = "#FFB833";
        this.VIOLET_COLOUR = "#CC99FF";
        this.BLUE_COLOUR = "#0095FF";
        this.GREEN_COLOUR = "#239023";
        this.GRAY_COLOUR1 = "#444444";
        this.GRAY_COLOUR2 = "#AAAAAA";
        this.GRAY_COLOUR3 = "#777777";
        this.GRAY_COLOUR4 = "#CCCCCC";
        
        this.NORMAL = "NORMAL";
        this.ON = "ON";
        this.OFF = "OFF";
        this.WARNING = "WARNING";
        this.FAULT = "FAULT";

        this.DIAGRAM_WIDTH = 1180;

        this.data = data;
        this.vehicle = vehicle;
        this.__scale = scale;
        this.__paper = paper;
        this.__xOffSet = xOffSet;
        this.__yOffSet = yOffSet;
        this.__direction = direction;
        this.__clickHandler = clickHandler;
        
        this.__drawSet = paper.set();
        this.__drawColourSet = paper.set();
        
        this.__vehicleNumberObj = null;
    };

    ctor.prototype.__getColPos = function(pos) {
        if (this.__direction > 0) {
            return ((this.DIAGRAM_WIDTH + this.__xOffSet) - pos);
        }
        
        return (pos + this.__xOffSet);
    };

    ctor.prototype.__getRowPos = function(pos) {
        return (this.__yOffSet + pos + 10);
    };
    
    ctor.prototype.rect = function(col, row, width, height) {
        return this.rect(col, row, width, height, 0);
    };
    
    ctor.prototype.rect = function(col, row, width, height, radius) {
        if (this.__direction > 0) {
            return this.__paper.rect(col - width, row, width, height, radius);
        }
        
        return this.__paper.rect(col, row, width, height, radius);
    };

    ctor.prototype.__getScaled = function(val) {
        return (this.__scale * val);
    };

    ctor.prototype.setData = function(data) {
        this.data = data;
        
        this.__setColour();
        this.__setTooltip();
    };

    ctor.prototype.__setColour = function () {
        var colour = this.WHITE_COLOUR;
        
        if (!!this.data) {
            var category = this.data.categories.vehicle;
            if (category == this.FAULT) {
                colour = this.RED_COLOUR;
            } else if (category == this.WARNING) {
                colour = this.ORANGE_COLOUR;
            } else if (category == this.OFF) {
                colour = this.VIOLET_COLOUR;
            } else if (category == this.ON) {
                colour = this.BLUE_COLOUR;
            } else if (category == this.NORMAL) {
                colour = this.GREEN_COLOUR;
            }
        }
        
        this.__drawColourSet.attr({fill: colour});
    };

    ctor.prototype.__setTooltip = function() {
        var tooltip = '';
        
        if (!!this.data) {
            var sep = '';
            for (var index in this.data.events) {
                var event = this.data.events[index].fields;
                tooltip += sep + event.type + ': ' + event.description + ' (' + event.eventCode + ')';
                sep = '\n';
            }
            for (var index in this.data.restrictions) {
                var restriction = this.data.restrictions[index];
                //do something
            }
        }
    };
    
    ctor.prototype.draw = function(xOffSet) {
        var me = this;
        
        this.__addBase();
        this.__addFrontDetails();
        this.__addBackDetails();

        this.__addUpperDetails();
		this.__addPantographDetails();
        this.__addBottomDetails();
        
        me.__addWindow(70, 150);
        me.__addWindow(160, 150);

        me.__addWindow(500, 150);
        me.__addWindow(590, 150);

        me.__addWindow(980, 150);
        me.__addWindow(1070, 150);

        this.__addWindowDoor(300, 150);
		this.__addWindowDoor(360, 150);
		
		this.__addWindowDoor(730, 150);
		this.__addWindowDoor(790, 150);

        this.__addVehicleNumber();
        
        this.__drawSet.scale(this.__scale, this.__scale, 0, 0);

        var me = this;
        this.__drawSet.click(function() {
            if (!!me.__clickHandler) {
                me.__clickHandler(me.vehicle.id);
            }
        });
    };
    
    
    
    
    
    /*****************************************************************/
    /***    DRAWING FUNCTION FOR THE DIFFERENT PARTS OF THE CAB    ***/
    /*****************************************************************/
    
    
    ctor.prototype.__addBase = function() {
        var upperObj = this.__paper.path(
        		"M" + this.__getColPos(250) + " " + this.__getRowPos(100) +
        		"L" + this.__getColPos(950) + " " + this.__getRowPos(100) +
        		" " + this.__getColPos(920) + " " + this.__getRowPos(50) +
        		" " + this.__getColPos(290) + " " + this.__getRowPos(50) +
        " Z");
        upperObj.attr({fill:this.GRAY_COLOUR4, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(upperObj);
        
        var middleObj = this.__paper.path(
        "M" + this.__getColPos(50) + " " + this.__getRowPos(100) + 
        " L" + this.__getColPos(50) + " " + this.__getRowPos(238) + 
        " " + this.__getColPos(50) + " " + this.__getRowPos(322) + 
        " " + this.__getColPos(1150) + " " + this.__getRowPos(322) + 
        " " + this.__getColPos(1150) + " " + this.__getRowPos(100) + 
        " Z")

        middleObj.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(middleObj);
        this.__drawColourSet.push(middleObj);
        
        var lowerObj = this.__paper.path(
           "M" + this.__getColPos(50) + " " + this.__getRowPos(322) + 
           " L" + this.__getColPos(50) + " " + this.__getRowPos(337) +
		   " " + this.__getColPos(200) + " " + this.__getRowPos(337) +
		   " " + this.__getColPos(210) + " " + this.__getRowPos(357) + 
		   " " + this.__getColPos(410) + " " + this.__getRowPos(357) +
		   " " + this.__getColPos(420) + " " + this.__getRowPos(337) +
		   " " + this.__getColPos(730) + " " + this.__getRowPos(337) +
		   " " + this.__getColPos(740) + " " + this.__getRowPos(357) +
           " " + this.__getColPos(940) + " " + this.__getRowPos(357) + 
		   " " + this.__getColPos(950) + " " + this.__getRowPos(337) +
		   " " + this.__getColPos(1150) + " " + this.__getRowPos(337) +
           " " + this.__getColPos(1150) + " " + this.__getRowPos(322) + 
           " Z");
        lowerObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(lowerObj);
		
		var upperLine = this.__paper.path(
        "M" + this.__getColPos(300) + " " + this.__getRowPos(75) + 
        " L" + this.__getColPos(900) + " " + this.__getRowPos(75));
        upperLine.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(upperLine);
		
        var vertLine1 = this.__paper.path(
        "M" + this.__getColPos(575) + " " + this.__getRowPos(50) +
        " L" + this.__getColPos(575) + " " + this.__getRowPos(100));
        vertLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(vertLine1);

    };

    ctor.prototype.__addFrontDetails = function() {

     var frontObj1 = this.rect(this.__getColPos(40), this.__getRowPos(100), 15, 230, 0);
     frontObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(frontObj1);

     var frontObj2 = this.rect(this.__getColPos(30), this.__getRowPos(100), 15, 230, 0);
     frontObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(frontObj2);

     var frontObj3 = this.rect(this.__getColPos(60), this.__getRowPos(130), 200, 130, 1);
     frontObj3.attr({fill:this.GRAY_COLOUR4, stroke:this.GRAY_COLOUR4, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(frontObj3);

     var frontObj4 = this.rect(this.__getColPos(940), this.__getRowPos(130), 200, 120, 1);
     frontObj4.attr({fill:this.GRAY_COLOUR4, stroke:this.GRAY_COLOUR4, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(frontObj4);

     var frontObj5 = this.rect(this.__getColPos(475), this.__getRowPos(130), 200, 120, 1);
     frontObj5.attr({fill:this.GRAY_COLOUR4, stroke:this.GRAY_COLOUR4, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(frontObj5);

    };

    ctor.prototype.__addBackDetails = function() {

     var backObj1 = this.rect(this.__getColPos(1150), this.__getRowPos(100), 15, 230, 0);
     backObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(backObj1);

     var backObj2 = this.rect(this.__getColPos(1165), this.__getRowPos(100), 15, 230, 0);
     backObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
     this.__drawSet.push(backObj2);

    };
    
    ctor.prototype.__addUpperDetails = function() {

    };
    
	ctor.prototype.__addPantographDetails = function(){
		var panObj4 = this.__paper.path(
        "M" + this.__getColPos(100) + " " + this.__getRowPos(80) + 
        " L" + this.__getColPos(208) + " " + this.__getRowPos(45) + 
        " " + this.__getColPos(104) + " " + this.__getRowPos(12));
        panObj4.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj4);

        var panObj5 = this.__paper.path(
        "M" + this.__getColPos(155) + " " + this.__getRowPos(80) + 
        " L" + this.__getColPos(220) + " " + this.__getRowPos(53) + 
        " " + this.__getColPos(216) + " " + this.__getRowPos(42) +
        " " + this.__getColPos(110) + " " + this.__getRowPos(12));
        panObj5.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj5);
		
		var panObj6 = this.rect(this.__getColPos(100), this.__getRowPos(8), 25, 8, 1);
		panObj6.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj6);
		
		var panObj7 = this.rect(this.__getColPos(100), this.__getRowPos(80), 70, 19, 1);
		panObj7.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj7);

        var panObj8 = this.rect(this.__getColPos(217), this.__getRowPos(90), 15, 9, 1);
		panObj8.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj8);
		
		var panObj9 = this.rect(this.__getColPos(990), this.__getRowPos(90), 15, 9, 1);
		panObj9.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj9);
		
		var panObj10 = this.rect(this.__getColPos(1050), this.__getRowPos(80), 20, 19, 1);
		panObj10.attr({fill:this.GRAY_COLOUR2, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj10);
		
		var panObj11 = this.rect(this.__getColPos(1010), this.__getRowPos(65), 110, 16, 1);
		panObj11.attr({fill:this.GRAY_COLOUR2, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj11);
		
		var panObj12 = this.rect(this.__getColPos(1100), this.__getRowPos(80), 20, 19, 1);
		panObj12.attr({fill:this.GRAY_COLOUR2, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(panObj12);
	};

    ctor.prototype.__addBottomDetails = function() {
        
        this.__addHalfWheelSetFront(25, 366);
        this.__addHalfWheelSetBack(1080, 366);

        var bottomObj2 = this.__paper.path(
        "M" + this.__getColPos(200) + " " + this.__getRowPos(340) + 
        " L" + this.__getColPos(200) + " " + this.__getRowPos(360) +
		" " + this.__getColPos(210) + " " + this.__getRowPos(370) +		
        " " + this.__getColPos(210) + " " + this.__getRowPos(375) +
        " " + this.__getColPos(265) + " " + this.__getRowPos(375) +
        " " + this.__getColPos(265) + " " + this.__getRowPos(375) +
        " " + this.__getColPos(270) + " " + this.__getRowPos(360) +
		" " + this.__getColPos(230) + " " + this.__getRowPos(360) +
        " " + this.__getColPos(210) + " " + this.__getRowPos(340) + " Z");
        bottomObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomObj2);
		
		var bottomObj3 = this.__paper.path(
        "M" + this.__getColPos(300) + " " + this.__getRowPos(370) + 
        " L" + this.__getColPos(300) + " " + this.__getRowPos(375) +		
        " " + this.__getColPos(375) + " " + this.__getRowPos(375) +
        " " + this.__getColPos(385) + " " + this.__getRowPos(370) +
        " " + this.__getColPos(385) + " " + this.__getRowPos(360) +
		" " + this.__getColPos(300) + " " + this.__getRowPos(360) +
        " " + this.__getColPos(300) + " " + this.__getRowPos(370) + " Z");
        bottomObj3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomObj3);
		
		var bottomObj4 = this.rect(480, 340, 190, 35, 0);
		bottomObj4.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj4);
        
        var bottomObj5 = this.__paper.path(
        "M" + this.__getColPos(770) + " " + this.__getRowPos(360) + 
        " L" + this.__getColPos(770) + " " + this.__getRowPos(365) +
		" " + this.__getColPos(780) + " " + this.__getRowPos(375) +
        " " + this.__getColPos(870) + " " + this.__getRowPos(375) +        
        " " + this.__getColPos(870) + " " + this.__getRowPos(360) + " Z");
        bottomObj5.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomObj5);

        var bottomObj6 = this.__paper.path(
        "M" + this.__getColPos(900) + " " + this.__getRowPos(360) + 
        " L" + this.__getColPos(900) + " " + this.__getRowPos(375) + 
        " " + this.__getColPos(960) + " " + this.__getRowPos(375) +
		" " + this.__getColPos(960) + " " + this.__getRowPos(365) +
		" " + this.__getColPos(970) + " " + this.__getRowPos(365) +
		" " + this.__getColPos(970) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(960) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(940) + " " + this.__getRowPos(355) +
        " " + this.__getColPos(940) + " " + this.__getRowPos(360) + " Z");
		bottomObj6.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomObj6);
        
    };
    

    ctor.prototype.__addWindowDoor = function(col, row) {
        var door = this.rect(this.__getColPos(col), this.__getRowPos(row), 60, 172, 5);
        door.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(door);
        
        var window = this.rect(this.__getColPos(col + 12), this.__getRowPos(row + 20), 35, 80, 5);
        window.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(window);
    };

    ctor.prototype.__addWindow = function(col, row) {
        var mainWindow = this.rect(this.__getColPos(col), this.__getRowPos(row), 60, 80,5);
        mainWindow.attr({fill:this.GRAY_COLOUR3,stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(mainWindow);
    };
    
    ctor.prototype.__addWheel = function(col, row) {
        var wheel1 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 28);
        wheel1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel1.toBack();
        this.__drawSet.push(wheel1);
        
        var wheel2 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 30);
        wheel2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel2.toBack();
        this.__drawSet.push(wheel2);
        
        var wheel3 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 33);
        wheel3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel3.toBack();
        this.__drawSet.push(wheel3);
        
    }
    
    ctor.prototype.__addWheelSet = function(col, row) {
        this.__addWheel(col, row + 1);

    }
	
	ctor.prototype.__addHalfWheelSetFront = function(col, row) {
        this.__addWheel(col + 88, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row+10) +
		" L" + this.__getColPos(col + 19) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 39) + " " + this.__getRowPos(row) +		
        " " + this.__getColPos(col + 69) + " " + this.__getRowPos(row) +
        " " + this.__getColPos(col + 89) + " " + this.__getRowPos(row-20) +   
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +		
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" +  this.__getColPos(col) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+101) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+151) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+96) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
	
	ctor.prototype.__addHalfWheelSetBack = function(col, row) {
        this.__addWheel(col - 10, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col + 20) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 50) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 70) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row+10) + 
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row-20) +               
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" + this.__getColPos(col-20) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col-40) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-65) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col-80) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-40) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col-25) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
    
    ctor.prototype.__addVehicleNumber = function () {
        if (!!this.__vehicleNumberObj) {
            this.__vehicleNumberObj.remove();
        }

        this.__vehicleNumberObj = this.__paper.text(
            this.__getScaled(600 + this.__xOffSet), 
            this.__getScaled(450 + this.__yOffSet) + (this.FONT_SIZE / 2), 
            this.vehicle.vehicleNumber
        );
        
        this.__vehicleNumberObj.attr({
            "font-size": this.FONT_SIZE,
            "text-anchor": "middle"
        });
    };
    
    return ctor;
})();

