var nx = nx || {};
nx.ns = nx.ns || {};
nx.ns.diagram = nx.ns.diagram || {};

nx.ns.diagram.cab_mABK = (function() {
    function ctor(data, vehicle, paper, scale, xOffSet, yOffSet, direction, clickHandler) {
        
        this.LEFT = -1;
        this.RIGHT = 1;
        
        this.STROKE_WIDTH = 1;
        this.FONT_SIZE = 12;
        this.BLACK_COLOUR = "#000000";
        this.WHITE_COLOUR = "#FFFFFF";
        this.YELLOW_COLOUR = "#FFFF00";
        this.RED_COLOUR = "#FF5959";
        this.ORANGE_COLOUR = "#FFB833";
        this.VIOLET_COLOUR = "#CC99FF";
        this.BLUE_COLOUR = "#0095FF";
        this.GREEN_COLOUR = "#239023";
        this.GRAY_COLOUR1 = "#444444";
        this.GRAY_COLOUR2 = "#AAAAAA";
        this.GRAY_COLOUR3 = "#777777";
        this.GRAY_COLOUR4 = "#CCCCCC";
        
        this.NORMAL = "NORMAL";
        this.ON = "ON";
        this.OFF = "OFF";
        this.WARNING = "WARNING";
        this.FAULT = "FAULT";

        this.DIAGRAM_WIDTH = 1530;

        this.data = data;
        this.vehicle = vehicle;
        this.__scale = scale;
        this.__paper = paper;
        this.__xOffSet = xOffSet;
        this.__yOffSet = yOffSet;
        this.__direction = direction;
        this.__clickHandler = clickHandler;
        
        this.__drawSet = paper.set();
        this.__drawColourSet = paper.set();
        this.__activeCabDot = paper.set();
        this.__directionArrow = paper.set();
        
        this.__vehicleNumberObj = null;
    };

    ctor.prototype.__getColPos = function(pos) {
        if (this.__direction > 0) {
            return ((this.DIAGRAM_WIDTH + this.__xOffSet) - pos);
        }
        
        return (pos + this.__xOffSet);
    };

    ctor.prototype.__getRowPos = function(pos) {
        return (this.__yOffSet + pos + 10);
    };
    
    ctor.prototype.rect = function(col, row, width, height) {
        return this.rect(col, row, width, height, 0);
    };
    
    ctor.prototype.rect = function(col, row, width, height, radius) {
        if (this.__direction > 0) {
            return this.__paper.rect(col - width, row, width, height, radius);
        }
        
        return this.__paper.rect(col, row, width, height, radius);
    };

    ctor.prototype.__getScaled = function(val) {
        return (this.__scale * val);
    };

    ctor.prototype.setData = function(data, unitType) {
        this.data = data;

        this.__setColour();
        this.__setTooltip();
        
        if (data) { //Temporary solution as data should always exist in this method. When switching units, updating unitId should probably take place prior to this (see nx.rest.unitDetail.data)
            this.drawDirectionArrow(this.data.forward, this.data.reverse, unitType);
            this.drawLeadingCabDot(this.data.leadingCab, unitType);
        }
        
    };

    ctor.prototype.__setColour = function () {
        var colour = this.WHITE_COLOUR;
        
        if (!!this.data) {
            var category = this.data.categories.vehicle;
            if (category == this.FAULT) {
                colour = this.RED_COLOUR;
            } else if (category == this.WARNING) {
                colour = this.ORANGE_COLOUR;
            } else if (category == this.OFF) {
                colour = this.VIOLET_COLOUR;
            } else if (category == this.ON) {
                colour = this.BLUE_COLOUR;
            } else if (category == this.NORMAL) {
                colour = this.GREEN_COLOUR;
            }
        }
        
        this.__drawColourSet.attr({fill: colour});
    };

    ctor.prototype.__setTooltip = function() {
        var tooltip = '';
        
        if (!!this.data) {
            var sep = '';
            for (var index in this.data.events) {
                var event = this.data.events[index].fields;
                tooltip += sep + event.type + ': ' + event.description + ' (' + event.eventCode + ')';
                sep = '\n';
            }
            for (var index in this.data.restrictions) {
                var restriction = this.data.restrictions[index];
                //do something
            }
        }
    };
    
    ctor.prototype.draw = function(xOffSet) {
        
        this.__addBase();
        this.__addFrontDetails();
        this.__addBackDetails();

        this.__addUpperDetails();
        this.__addBottomDetails();
		
		this.__addCabinDoor(220,120);
		
		this.__addWindowLarge(350,150);
		this.__addWindowLarge(550,150);
        
        this.__addWindowDoor(750,150);
		this.__addWindowDoor(810,150);
        
		this.__addWindow(950, 150);
		this.__addWindow(1080, 150);
		this.__addWindow(1210, 150);
		this.__addWindow(1340, 150);
                
        this.__addVehicleNumber();
        
        this.__drawSet.scale(this.__scale, this.__scale, 0, 0);

        var me = this;
        this.__drawSet.click(function() {
            if (!!me.__clickHandler) {
                me.__clickHandler(me.vehicle.id);
            }
        });
    };
    
    
    
    
    
    /*****************************************************************/
    /***    DRAWING FUNCTION FOR THE DIFFERENT PARTS OF THE CAB    ***/
    /*****************************************************************/
    
    
    ctor.prototype.__addBase = function() {
		var upperObj = this.__paper.path(
        "M" + this.__getColPos(1530) + " " + this.__getRowPos(100) + 
        " L" + this.__getColPos(180) + " " + this.__getRowPos(100) + 
		" " + this.__getColPos(155) + " " + this.__getRowPos(110) +
		" " + this.__getColPos(130) + " " + this.__getRowPos(130) +
		" " + this.__getColPos(50) + " " + this.__getRowPos(230) +
        " " + this.__getColPos(110) + " " + this.__getRowPos(100) +
		" " + this.__getColPos(150) + " " + this.__getRowPos(75) +
        " " + this.__getColPos(222) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(237) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(250) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1360) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1373) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1388) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1530) + " " + this.__getRowPos(50) +
        " Z");
        upperObj.attr({fill:this.GRAY_COLOUR4, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(upperObj);
        
        var middleObj = this.__paper.path(
        "M" + this.__getColPos(1530) + " " + this.__getRowPos(100) + 
        " L" + this.__getColPos(1530) + " " + this.__getRowPos(322) + 
        " " + this.__getColPos(130) + " " + this.__getRowPos(322) +
		" " + this.__getColPos(110) + " " + this.__getRowPos(300) +
		" " + this.__getColPos(90) + " " + this.__getRowPos(280) +
		" " + this.__getColPos(70) + " " + this.__getRowPos(295) +
		" " + this.__getColPos(50) + " " + this.__getRowPos(250) +
		" " + this.__getColPos(50) + " " + this.__getRowPos(230) +		 
        " " + this.__getColPos(155) + " " + this.__getRowPos(110) + 
        " " + this.__getColPos(180) + " " + this.__getRowPos(100) + 
        " Z");
        middleObj.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(middleObj);
        this.__drawColourSet.push(middleObj);
		
		midLine1 = this.__paper.path(
		"M" + this.__getColPos(70) + " " + this.__getRowPos(305) +
		" L" + this.__getColPos(750) + " " + this.__getRowPos(305)
		);
		midLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine1);
		
		midLine2 = this.__paper.path(
		"M" + this.__getColPos(870) + " " + this.__getRowPos(305) +
		" L" + this.__getColPos(1530) + " " + this.__getRowPos(305)
		);
		midLine2.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine2);
        
        var lowerObj = this.__paper.path(
        "M" + this.__getColPos(445) + " " + this.__getRowPos(322) + 
        " L" + this.__getColPos(461) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(650) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(670) + " " + this.__getRowPos(350) +
		" " + this.__getColPos(970) + " " + this.__getRowPos(350) +
		" " + this.__getColPos(1000) + " " + this.__getRowPos(340) +		
        " " + this.__getColPos(1300) + " " + this.__getRowPos(340) + 
        " " + this.__getColPos(1300) + " " + this.__getRowPos(322) + 
        " Z");
        lowerObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(lowerObj);
		
		var lowerObj2 = this.__paper.path(
		"M" + this.__getColPos(230) + " " + this.__getRowPos(322) + 
        " L" + this.__getColPos(160) + " " + this.__getRowPos(380) +
		" " + this.__getColPos(80) + " " + this.__getRowPos(380) +		
        " " + this.__getColPos(50) + " " + this.__getRowPos(250) + 
        " " + this.__getColPos(130) + " " + this.__getRowPos(322) + 
        " Z");
		lowerObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(lowerObj2);
        
    };

    ctor.prototype.__addFrontDetails = function() {
		
		var frontPanel = this.__paper.path(
		"M" + this.__getColPos(120) + " " + this.__getRowPos(185) +
		" L" + this.__getColPos(160) + " " + this.__getRowPos(130) +
		" " + this.__getColPos(180) + " " + this.__getRowPos(120) +
		" " + this.__getColPos(720) + " " + this.__getRowPos(120) +
		" " + this.__getColPos(720) + " " + this.__getRowPos(250) +
		" " + this.__getColPos(180) + " " + this.__getRowPos(250) +
		" " + this.__getColPos(160) + " " + this.__getRowPos(240) +		
		" Z");
		frontPanel.attr({fill:this.GRAY_COLOUR2, "stroke-width":0});
		this.__drawSet.push(frontPanel);
		
		var backPanel = this.rect(this.__getColPos(900), this.__getRowPos(120), 627, 130, 1);
		backPanel.attr({fill:this.GRAY_COLOUR2, "stroke-width":0});
		this.__drawSet.push(backPanel);
        
		var frontLine = this.__paper.path(
		"M" + this.__getColPos(200) + " " + this.__getRowPos(320) +
		" L" + this.__getColPos(200) + " " + this.__getRowPos(280) +
		" " + this.__getColPos(300) + " " + this.__getRowPos(280) +
		" " + this.__getColPos(300) + " " + this.__getRowPos(50)
		);
		frontLine.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(frontLine);
    };

    ctor.prototype.__addBackDetails = function() {
        var backObj1 = this.rect(this.__getColPos(1530), this.__getRowPos(100), 15, 230, 0);
		backObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(backObj1);

		var backObj2 = this.rect(this.__getColPos(1545), this.__getRowPos(100), 15, 230, 0);
		backObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(backObj2);
    };
    
    ctor.prototype.__addUpperDetails = function() {
  
        var upperObj1 = this.__paper.path(
		"M" + this.__getColPos(400) + " " + this.__getRowPos(50) + 
        " L" + this.__getColPos(700) + " " + this.__getRowPos(100));
		upperObj1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(upperObj1);
    };
    
	ctor.prototype.__addPantographDetails = function(){
        var panObj4 = this.__paper.path(
        "M" + this.__getColPos(950) + " " + this.__getRowPos(116) + 
        " L" + this.__getColPos(863) + " " + this.__getRowPos(67) + 
        " " + this.__getColPos(954) + " " + this.__getRowPos(12));
        panObj4.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj4);

        var panObj5 = this.__paper.path(
        "M" + this.__getColPos(895) + " " + this.__getRowPos(123) + 
        " L" + this.__getColPos(845) + " " + this.__getRowPos(78) + 
        " " + this.__getColPos(854) + " " + this.__getRowPos(62) +
        " " + this.__getColPos(954) + " " + this.__getRowPos(12));
        panObj5.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj5);

        var panObj6 = this.__paper.path(
        "M" + this.__getColPos(951) + " " + this.__getRowPos(12) + 
        " L" + this.__getColPos(964) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(969) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(969) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(7) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(7) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(928) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(928) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(12) + 
        "Z");
        panObj6.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj6);
	};

    ctor.prototype.__addBottomDetails = function() {
                
        this.__addWheelSet(300, 366);
        this.__addHalfWheelSetBack(1450, 366);
		
		var bottomObj1 = this.__paper.path(
		"M" + this.__getColPos(570) + " " + this.__getRowPos(340) + 
        " L" + this.__getColPos(570) + " " + this.__getRowPos(370) + 
        " " + this.__getColPos(630) + " " + this.__getRowPos(370) +
        " " + this.__getColPos(630) + " " + this.__getRowPos(340) + " Z");
		bottomObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj1);
		
		var bottomObj2 = this.__paper.path(
		"M" + this.__getColPos(750) + " " + this.__getRowPos(350) + 
        " L" + this.__getColPos(770) + " " + this.__getRowPos(360) +
		" " + this.__getColPos(770) + " " + this.__getRowPos(370) +		
        " " + this.__getColPos(850) + " " + this.__getRowPos(370) +
		" " + this.__getColPos(850) + " " + this.__getRowPos(360) +
        " " + this.__getColPos(870) + " " + this.__getRowPos(350) + " Z");
		bottomObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj2);
		
		var bottomObj3 = this.rect(this.__getColPos(1000), this.__getRowPos(340), 100, 30, 1);
		bottomObj3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj3);

    };
    

    ctor.prototype.__addWindowDoor = function(col, row) {
        var door = this.rect(this.__getColPos(col), this.__getRowPos(row), 60, 172, 5);
        door.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(door);
        
        var window = this.rect(this.__getColPos(col + 12), this.__getRowPos(row + 20), 35, 80, 5);
        window.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(window);
    };
	
	ctor.prototype.__addCabinDoor = function(col, row) {
		var door = this.rect(this.__getColPos(col), this.__getRowPos(row), 50, 150, 5);
        door.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(door);
        
        var window = this.rect(this.__getColPos(col + 10), this.__getRowPos(row + 20), 30, 65, 5);
        window.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(window);
		
		var handrailLeft = this.rect(this.__getColPos(col-15), this.__getRowPos(row+25), 10, 90, 1);
		handrailLeft.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(handrailLeft);
		
		var handrailRight = this.rect(this.__getColPos(col+55), this.__getRowPos(row+25), 10, 90, 1);
		handrailRight.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(handrailRight);
	};

    ctor.prototype.__addWindow = function(col, row) {
        var mainWindow = this.rect(this.__getColPos(col), this.__getRowPos(row), 100, 80,5);
        mainWindow.attr({fill:this.GRAY_COLOUR3,stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(mainWindow);
    };
	
	ctor.prototype.__addWindowLarge = function(col, row) {
        var mainWindow = this.rect(this.__getColPos(col), this.__getRowPos(row), 150, 80,5);
        mainWindow.attr({fill:this.GRAY_COLOUR3,stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(mainWindow);
    };
    
    ctor.prototype.__addWheel = function(col, row) {
        var wheel1 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 28);
        wheel1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel1.toBack();
        this.__drawSet.push(wheel1);
        
        var wheel2 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 34);
        wheel2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel2.toBack();
        this.__drawSet.push(wheel2);
        
        var wheel3 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 35);
        wheel3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel3.toBack();
        this.__drawSet.push(wheel3);

    }
    
    ctor.prototype.__addWheelSet = function(col, row) {
        this.__addWheel(col - 10, row + 1);
        this.__addWheel(col + 178, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col + 20) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 50) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 70) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 108) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 128) + " " + this.__getRowPos(row) +		
        " " + this.__getColPos(col + 158) + " " + this.__getRowPos(row) +
        " " + this.__getColPos(col + 178) + " " + this.__getRowPos(row-20) +                
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" + this.__getColPos(col-20) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col-40) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-65) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col-80) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-40) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col-25) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+190) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+205) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+240) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col+210) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+210) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+205) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+185) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
	
	ctor.prototype.__addHalfWheelSetFront = function(col, row) {
        this.__addWheel(col + 88, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row+10) +
		" L" + this.__getColPos(col + 19) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 39) + " " + this.__getRowPos(row) +		
        " " + this.__getColPos(col + 69) + " " + this.__getRowPos(row) +
        " " + this.__getColPos(col + 89) + " " + this.__getRowPos(row-20) +   
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +		
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" +  this.__getColPos(col) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+101) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+151) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+96) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
	
	ctor.prototype.__addHalfWheelSetBack = function(col, row) {
        this.__addWheel(col - 10, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col + 20) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 50) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 70) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row+10) + 
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row-20) +               
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" + this.__getColPos(col-20) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col-40) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-65) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col-80) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-40) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col-25) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
    
    ctor.prototype.__addVehicleNumber = function () {
        if (!!this.__vehicleNumberObj) {
            this.__vehicleNumberObj.remove();
        }

        this.__vehicleNumberObj = this.__paper.text(
            this.__getScaled(600 + this.__xOffSet), 
            this.__getScaled(450 + this.__yOffSet) + (this.FONT_SIZE / 2), 
            this.vehicle.vehicleNumber
        );
        
        this.__vehicleNumberObj.attr({
            "font-size": this.FONT_SIZE,
            "text-anchor": "middle"
        });
    };
    
    ctor.prototype.drawDirectionArrow = function(forward, reverse, unitType) {  
    	var arrowX = 0;
    	var arrowY = 0;
    	if (unitType == 'SLT-IV'){
    		var arrowX = 422;
        	var arrowY = 75;
    	}else if (unitType == 'SLT-VI'){
    		var arrowX = 625;
        	var arrowY = 75;
    	}
    	if (this.__directionArrow){
        	this.__directionArrow.remove();
        }

    	var arrow = null;

        if (forward && !reverse) {
        	arrow = this.__paper.path("M" + this.__getColPos(arrowX + 80) + " "+ this.__getRowPos(arrowY + 5) 
        			+" " + this.__getColPos(arrowX + 10) + "	"+ this.__getRowPos(arrowY + 5) 
        			+" " + this.__getColPos(arrowX + 10) + " "+ this.__getRowPos(arrowY)
        			+" " + this.__getColPos(arrowX) + " "+ this.__getRowPos(arrowY + 8)
        			+" " + this.__getColPos(arrowX + 10) + "	"+ this.__getRowPos(arrowY + 16) 
        			+" " + this.__getColPos(arrowX + 10) + "	"+ this.__getRowPos(arrowY + 11) 
        			+" " + this.__getColPos(arrowX + 80) + "	"+ this.__getRowPos(arrowY + 11) +" Z");
        	this.__directionArrow.push(arrow);
        } else if (reverse && !forward) {
        	arrow = this.__paper.path("M" + this.__getColPos(arrowX) + " "+ this.__getRowPos(arrowY + 5) 
        			+" " + this.__getColPos(arrowX + 70) + "	"+ this.__getRowPos(arrowY + 5) 
        			+" " + this.__getColPos(arrowX + 70) + " "+ this.__getRowPos(arrowY)
        			+" " + this.__getColPos(arrowX + 80) + " "+ this.__getRowPos(arrowY + 8)
        			+" " + this.__getColPos(arrowX + 70) + "	"+ this.__getRowPos(arrowY + 16) 
        			+" " + this.__getColPos(arrowX + 70) + "	"+ this.__getRowPos(arrowY + 11) 
        			+" " + this.__getColPos(arrowX) + "	"+ this.__getRowPos(arrowY + 11) +" Z");
        	this.__directionArrow.push(arrow);
        }
        
        
    };
    
    ctor.prototype.drawLeadingCabDot = function(leadingCab, unitType) {
    	var xCab2Dot = 0;
    	if (unitType == 'SLT-IV'){
    		var xCab2Dot = 910;
    	}else if (unitType == 'SLT-VI'){
    		var xCab2Dot = 1311;
    	}
    	if (this.__activeCabDot){
    		this.__activeCabDot.remove();
    	}
    	if (leadingCab === "Cab1"){
    		this.drawDot(12, 12);
    	} else if (leadingCab === "Cab2"){
    		this.drawDot(xCab2Dot, 12);
    	}
    };
    
    ctor.prototype.drawDot = function(col, row) {
    	var dot = this.__paper.circle(col, row, 4.5);
    	dot.attr({
            'fill': '#00FF00', 
            'stroke-width': 1
        });
    	this.__activeCabDot.push(dot);
    };
    
    return ctor;
})();
