var nx = nx || {};
nx.ns = nx.ns || {};
nx.ns.diagram = nx.ns.diagram || {};

nx.ns.diagram.cab_mBv7 = (function() {
    function ctor(data, vehicle, paper, scale, xOffSet, yOffSet, direction, clickHandler) {
        
        this.LEFT = -1;
        this.RIGHT = 1;
        
        this.STROKE_WIDTH = 1;
        this.FONT_SIZE = 12;
        this.BLACK_COLOUR = "#000000";
        this.WHITE_COLOUR = "#FFFFFF";
        this.YELLOW_COLOUR = "#FFFF00";
        this.RED_COLOUR = "#FF5959";
        this.ORANGE_COLOUR = "#FFB833";
        this.VIOLET_COLOUR = "#CC99FF";
        this.BLUE_COLOUR = "#0095FF";
        this.GREEN_COLOUR = "#239023";
        this.GRAY_COLOUR1 = "#444444";
        this.GRAY_COLOUR2 = "#AAAAAA";
        this.GRAY_COLOUR3 = "#777777";
        this.GRAY_COLOUR4 = "#CCCCCC";
        
        this.NORMAL = "NORMAL";
        this.ON = "ON";
        this.OFF = "OFF";
        this.WARNING = "WARNING";
        this.FAULT = "FAULT";

        this.DIAGRAM_WIDTH = 1765;

        this.data = data;
        this.vehicle = vehicle;
        this.__scale = scale;
        this.__paper = paper;
        this.__xOffSet = xOffSet;
        this.__yOffSet = yOffSet;
        this.__direction = direction;
        this.__clickHandler = clickHandler;
        
        this.__drawSet = paper.set();
        this.__drawColourSet = paper.set();
        
        this.__vehicleNumberObj = null;
    };

    ctor.prototype.__getColPos = function(pos) {
        if (this.__direction > 0) {
            return ((this.DIAGRAM_WIDTH + this.__xOffSet) - pos);
        }
        
        return (pos + this.__xOffSet);
    };

    ctor.prototype.__getRowPos = function(pos) {
        return (this.__yOffSet + pos + 10);
    };
    
    ctor.prototype.rect = function(col, row, width, height) {
        return this.rect(col, row, width, height, 0);
    };
    
    ctor.prototype.rect = function(col, row, width, height, radius) {
        if (this.__direction > 0) {
            return this.__paper.rect(col - width, row, width, height, radius);
        }
        
        return this.__paper.rect(col, row, width, height, radius);
    };

    ctor.prototype.__getScaled = function(val) {
        return (this.__scale * val);
    };

    ctor.prototype.setData = function(data) {
        this.data = data;
        
        this.__setColour();
        this.__setTooltip();
    };

    ctor.prototype.__setColour = function () {
        var colour = this.WHITE_COLOUR;
        
        if (!!this.data) {
            var category = this.data.categories.vehicle;
            if (category == this.FAULT) {
                colour = this.RED_COLOUR;
            } else if (category == this.WARNING) {
                colour = this.ORANGE_COLOUR;
            } else if (category == this.OFF) {
                colour = this.VIOLET_COLOUR;
            } else if (category == this.ON) {
                colour = this.BLUE_COLOUR;
            } else if (category == this.NORMAL) {
                colour = this.GREEN_COLOUR;
            }
        }
        
        this.__drawColourSet.attr({fill: colour});
    };

    ctor.prototype.__setTooltip = function() {
        var tooltip = '';
        
        if (!!this.data) {
            var sep = '';
            for (var index in this.data.events) {
                var event = this.data.events[index].fields;
                tooltip += sep + event.type + ': ' + event.description + ' (' + event.eventCode + ')';
                sep = '\n';
            }
            for (var index in this.data.restrictions) {
                var restriction = this.data.restrictions[index];
                //do something
            }
        }
    };
    
    ctor.prototype.draw = function(xOffSet) {
        
        this.__addBase();
        this.__addFrontDetails();
        this.__addBackDetails();

        this.__addUpperDetails();
        this.__addBottomDetails();
		
		this.__addWindowDoor(240,130);
		this.__addWindowDoor(285,130);
		
		this.__addWindowDoor(1430,130);
		this.__addWindowDoor(1475,130);
		
		this.__addWindowTop(590,70);
		this.__addWindowTop(720,70);
		this.__addWindowTop(850,70);
		this.__addWindowTop(980,70);
		this.__addWindowTop(1110,70);
		this.__addWindowTop(1240,70);
        
		this.__addWindow(720, 225);
		this.__addWindow(850, 225);
		this.__addWindow(980, 225);
		this.__addWindow(1110, 225);
		this.__addWindow(1240, 225);
                
        this.__addVehicleNumber();
        
        this.__drawSet.scale(this.__scale, this.__scale, 0, 0);

        var me = this;
        this.__drawSet.click(function() {
            if (!!me.__clickHandler) {
                me.__clickHandler(me.vehicle.id);
            }
        });
    };
    
    
    
    
    
    /*****************************************************************/
    /***    DRAWING FUNCTION FOR THE DIFFERENT PARTS OF THE CAB    ***/
    /*****************************************************************/
    
    
    ctor.prototype.__addBase = function() {
        var roofBox = this.rect(this.__getColPos(55), this.__getRowPos(5), 1690, 90);
        roofBox.attr({fill:this.GRAY_COLOUR4, stroke:this.GRAY_COLOUR2, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(roofBox);
        
		/*var upperObj = this.__paper.path(
        "M" + this.__getColPos(1530) + " " + this.__getRowPos(100) + 
        " L" + this.__getColPos(180) + " " + this.__getRowPos(100) + 
		" " + this.__getColPos(155) + " " + this.__getRowPos(110) +
		" " + this.__getColPos(130) + " " + this.__getRowPos(130) +
		" " + this.__getColPos(50) + " " + this.__getRowPos(230) +
        " " + this.__getColPos(110) + " " + this.__getRowPos(100) +
		" " + this.__getColPos(150) + " " + this.__getRowPos(75) +
        " " + this.__getColPos(222) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(237) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(250) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1360) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1373) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1388) + " " + this.__getRowPos(50) + 
        " " + this.__getColPos(1530) + " " + this.__getRowPos(50) +
        " Z");
        upperObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(upperObj);*/
        
        var middleObj = this.__paper.path(
        "M" + this.__getColPos(1750) + " " + this.__getRowPos(80) +
		" " + this.__getColPos(1750) + " " + this.__getRowPos(322) + 
		" " + this.__getColPos(1430) + " " + this.__getRowPos(322) +
		" " + this.__getColPos(1390) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(1350) + " " + this.__getRowPos(375) +
		" " + this.__getColPos(455) + " " + this.__getRowPos(375) +
		" " + this.__getColPos(420) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(390) + " " + this.__getRowPos(322) +
        " " + this.__getColPos(180) + " " + this.__getRowPos(322) + 
		" " + this.__getColPos(50) + " " + this.__getRowPos(322) +
		" " + this.__getColPos(50) + " " + this.__getRowPos(80) +		 
        " Z");
        middleObj.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(middleObj);
        this.__drawColourSet.push(middleObj);
		
		/*midLine2 = this.__paper.path(
		"M" + this.__getColPos(430) + " " + this.__getRowPos(335) +
		" L" + this.__getColPos(620) + " " + this.__getRowPos(335)
		);
		midLine2.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine2);*/
		
		/*midLine3 = this.__paper.path(
		"M" + this.__getColPos(155) + " " + this.__getRowPos(295) +
		" L" + this.__getColPos(53) + " " + this.__getRowPos(320)
		);
		midLine3.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine3);*/
		
		midLine = this.__paper.path(
		"M" + this.__getColPos(50) + " " + this.__getRowPos(185) +
		" " + this.__getColPos(130) + " " + this.__getRowPos(185) +
		" " + this.__getColPos(1750) + " " + this.__getRowPos(185)
		);
		midLine.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine);
        
        /*var lowerObj = this.__paper.path(
        "M" + this.__getColPos(445) + " " + this.__getRowPos(322) + 
        " L" + this.__getColPos(461) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(650) + " " + this.__getRowPos(340) +
		" " + this.__getColPos(670) + " " + this.__getRowPos(350) +
		" " + this.__getColPos(970) + " " + this.__getRowPos(350) +
		" " + this.__getColPos(1000) + " " + this.__getRowPos(340) +		
        " " + this.__getColPos(1300) + " " + this.__getRowPos(340) + 
        " " + this.__getColPos(1300) + " " + this.__getRowPos(322) + 
        " Z");
        lowerObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(lowerObj);*/
		
		/*var lowerObj2 = this.__paper.path(
		"M" + this.__getColPos(230) + " " + this.__getRowPos(322) + 
        " L" + this.__getColPos(160) + " " + this.__getRowPos(380) +
		" " + this.__getColPos(80) + " " + this.__getRowPos(380) +		
        " " + this.__getColPos(50) + " " + this.__getRowPos(250) + 
        " " + this.__getColPos(130) + " " + this.__getRowPos(322) + 
        " Z");
		lowerObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(lowerObj2);*/
		
		var topRightBlackBox = this.rect(this.__getColPos(1720), this.__getRowPos(0), 30, 35);
        topRightBlackBox.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topRightBlackBox)
		
		var topMiddleLeftBlackBox = this.rect(this.__getColPos(165), this.__getRowPos(40), 160, 25);
        topMiddleLeftBlackBox.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topMiddleLeftBlackBox)
		
		var topMiddleRightBlackBox1 = this.rect(this.__getColPos(1435), this.__getRowPos(40), 35, 25);
        topMiddleRightBlackBox1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topMiddleRightBlackBox1)
		
		var topMiddleRightBlackBox2 = this.rect(this.__getColPos(1500), this.__getRowPos(40), 70, 25);
        topMiddleRightBlackBox2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topMiddleRightBlackBox2)
		
		var topMiddleBlackBoxLine1 = this.__paper.path(
		"M" + this.__getColPos(1575) + " " + this.__getRowPos(35) + 
		" " + this.__getColPos(1575) + " " + this.__getRowPos(80)
		);
		topMiddleBlackBoxLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine1);
		
		var topMiddleBlackBoxLine1 = this.__paper.path(
		"M" + this.__getColPos(165) + " " + this.__getRowPos(0) + 
		" " + this.__getColPos(165) + " " + this.__getRowPos(80)
		);
		topMiddleBlackBoxLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine1);
		
		var topMiddleBlackBoxLine2 = this.__paper.path(
		"M" + this.__getColPos(1435) + " " + this.__getRowPos(0) + 
		" " + this.__getColPos(1435) + " " + this.__getRowPos(80)
		);
		topMiddleBlackBoxLine2.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine2);
		
		var topMiddleBlackBoxLine2 = this.__paper.path(
		"M" + this.__getColPos(325) + " " + this.__getRowPos(0) + 
		" " + this.__getColPos(325) + " " + this.__getRowPos(80)
		);
		topMiddleBlackBoxLine2.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine2);
		
		var topMiddleBlackBoxLine3 = this.__paper.path(
		"M" + this.__getColPos(1435) + " " + this.__getRowPos(34) + 
		" " + this.__getColPos(1680) + " " + this.__getRowPos(34) +
		" " + this.__getColPos(1700) + " " + this.__getRowPos(0)
		);
		topMiddleBlackBoxLine3.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine3);
		
		var topMiddleBlackBoxLine4 = this.__paper.path(
		"M" + this.__getColPos(1595) + " " + this.__getRowPos(35) + 
		" " + this.__getColPos(1595) + " " + this.__getRowPos(80)
		);
		topMiddleBlackBoxLine4.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine4);
		
		var topMiddleBlackBoxLine5 = this.__paper.path(
		"M" + this.__getColPos(1595) + " " + this.__getRowPos(75) + 
		" " + this.__getColPos(1750) + " " + this.__getRowPos(75)
		);
		topMiddleBlackBoxLine5.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine5);
		
		var topMiddleBlackBoxLine6 = this.__paper.path(
		"M" + this.__getColPos(1435) + " " + this.__getRowPos(75) + 
		" " + this.__getColPos(1575) + " " + this.__getRowPos(75)
		);
		topMiddleBlackBoxLine6.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine6);
		
		var topMiddleBlackBoxLine7 = this.__paper.path(
		"M" + this.__getColPos(165) + " " + this.__getRowPos(75) + 
		" " + this.__getColPos(325) + " " + this.__getRowPos(75)
		);
		topMiddleBlackBoxLine7.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(topMiddleBlackBoxLine7);
		
		var topMiddleWindowRight = this.rect(this.__getColPos(1595), this.__getRowPos(160), 75, 50, 8);
        topMiddleWindowRight.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topMiddleWindowRight)
		
		var topMiddleWindowLeft = this.rect(this.__getColPos(120), this.__getRowPos(155), 40, 70, 8);
        topMiddleWindowLeft.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topMiddleWindowLeft)
		
		var midRoofLine = this.__paper.path(
		"M" + this.__getColPos(50) + " " + this.__getRowPos(322) + 
		" " + this.__getColPos(50) + " " + this.__getRowPos(0) + 
		" " + this.__getColPos(330) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1440) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1440) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1680) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1680) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1710) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1750) + " " + this.__getRowPos(0) +
		" " + this.__getColPos(1750) + " " + this.__getRowPos(100)
		);
		midRoofLine.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midRoofLine);
		
		var topBlackWindow = this.rect(this.__getColPos(450), this.__getRowPos(70), 40, 40, 5);
        topBlackWindow.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topBlackWindow)
		
		var topSmallWindow = this.rect(this.__getColPos(510), this.__getRowPos(70), 40, 40, 5);
        topSmallWindow.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(topSmallWindow)
		
		var bottomPanel = this.rect(this.__getColPos(450), this.__getRowPos(225), 100, 70, 4);
        bottomPanel.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomPanel)
		
		var bottomPanelLine1 = this.__paper.path(
		"M" + this.__getColPos(500) + " " + this.__getRowPos(225) + 
		" " + this.__getColPos(500) + " " + this.__getRowPos(295)
		);
		bottomPanelLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(bottomPanelLine1);
		
		var bottomPanelHandle = this.rect(this.__getColPos(494), this.__getRowPos(255), 12, 8);
        bottomPanelHandle.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomPanelHandle)
		
		var bottomSlimClearWindow = this.rect(this.__getColPos(645), this.__getRowPos(225), 45, 70, 5);
        bottomSlimClearWindow.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSlimClearWindow)
		
		var bottomSlimBlackWindowTop = this.rect(this.__getColPos(590), this.__getRowPos(225), 45, 40, 5);
        bottomSlimBlackWindowTop.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSlimBlackWindowTop)
		
		var bottomSlimBlackWindowBottom = this.rect(this.__getColPos(590), this.__getRowPos(255), 45, 40, 5);
        bottomSlimBlackWindowBottom.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSlimBlackWindowBottom)
		
		var bottomSlimBlackWindowLine = this.__paper.path(
		"M" + this.__getColPos(595) + " " + this.__getRowPos(280) + 
		" L" + this.__getColPos(595) + " " + this.__getRowPos(260) +
		" " + this.__getColPos(635) + " " + this.__getRowPos(260) +
		" Z"
		);
		bottomSlimBlackWindowLine.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(bottomSlimBlackWindowLine);
		
		var bottomSmallBox1 = this.rect(this.__getColPos(450), this.__getRowPos(330), 45, 25, 5);
        bottomSmallBox1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox1)
		
		var bottomSmallBox1Handle = this.rect(this.__getColPos(470), this.__getRowPos(330), 6, 6);
        bottomSmallBox1Handle.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox1Handle)
		
		var bottomSmallBox2 = this.rect(this.__getColPos(505), this.__getRowPos(330), 45, 25, 5);
        bottomSmallBox2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox2)
		
		var bottomSmallBox2Handle = this.rect(this.__getColPos(525), this.__getRowPos(330), 6, 6);
        bottomSmallBox2Handle.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox2Handle)
		
		var bottomSmallBox3 = this.rect(this.__getColPos(590), this.__getRowPos(330), 45, 25, 5);
        bottomSmallBox3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox3)
		
		var bottomSmallBox3Handle = this.rect(this.__getColPos(610), this.__getRowPos(330), 6, 6);
        bottomSmallBox3Handle.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(bottomSmallBox3Handle)
		
		midLine1 = this.__paper.path(
		"M" + this.__getColPos(430) + " " + this.__getRowPos(350) +
		" L" + this.__getColPos(1380) + " " + this.__getRowPos(350)
		);
		midLine1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(midLine1);
    };

    ctor.prototype.__addFrontDetails = function() {
		/*var frontBumper = this.__paper.path(
		"M" + this.__getColPos(25) + " " + this.__getRowPos(335) + 
		" L" + this.__getColPos(55) + " " + this.__getRowPos(320) +
		" " + this.__getColPos(43) + " " + this.__getRowPos(310) +
		" Z"
		);
		frontBumper.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(frontBumper);*/
    };

    ctor.prototype.__addBackDetails = function() {
        var backObj1 = this.__paper.rect(this.__getColPos(1750), this.__getRowPos(82), 15, 245, 0);
		backObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(backObj1);
		
		var frontObj1 = this.__paper.rect(this.__getColPos(35), this.__getRowPos(82), 15, 245, 0);
		frontObj1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontObj1);

		var backObj2 = this.__paper.rect(this.__getColPos(1765), this.__getRowPos(82), 15, 235, 0);
		backObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(backObj2);
		
		var frontObj2 = this.__paper.rect(this.__getColPos(20), this.__getRowPos(82), 15, 235, 0);
		frontObj2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontObj2);
    };
    
    ctor.prototype.__addUpperDetails = function() {
  
        /*var upperObj1 = this.__paper.path(
		"M" + this.__getColPos(400) + " " + this.__getRowPos(50) + 
        " L" + this.__getColPos(700) + " " + this.__getRowPos(100));
		upperObj1.attr({stroke:this.BLACK_COLOUR, "stroke-width":this.STROKE_WIDTH});
		this.__drawSet.push(upperObj1);*/
    };
    
	ctor.prototype.__addPantographDetails = function(){
        var panObj4 = this.__paper.path(
        "M" + this.__getColPos(950) + " " + this.__getRowPos(116) + 
        " L" + this.__getColPos(863) + " " + this.__getRowPos(67) + 
        " " + this.__getColPos(954) + " " + this.__getRowPos(12));
        panObj4.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj4);

        var panObj5 = this.__paper.path(
        "M" + this.__getColPos(895) + " " + this.__getRowPos(123) + 
        " L" + this.__getColPos(845) + " " + this.__getRowPos(78) + 
        " " + this.__getColPos(854) + " " + this.__getRowPos(62) +
        " " + this.__getColPos(954) + " " + this.__getRowPos(12));
        panObj5.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj5);

        var panObj6 = this.__paper.path(
        "M" + this.__getColPos(951) + " " + this.__getRowPos(12) + 
        " L" + this.__getColPos(964) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(969) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(969) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(964) + " " + this.__getRowPos(7) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(7) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(12) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(928) + " " + this.__getRowPos(4) + 
        " " + this.__getColPos(928) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(15) + 
        " " + this.__getColPos(933) + " " + this.__getRowPos(12) + 
        "Z");
        panObj6.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(panObj6);
	};

    ctor.prototype.__addBottomDetails = function() {
                
        this.__addWheelSet(200, 358);
        this.__addWheelSet(1450, 358);
		
		/*var bottomObj1 = this.__paper.path(
		"M" + this.__getColPos(570) + " " + this.__getRowPos(340) + 
        " L" + this.__getColPos(570) + " " + this.__getRowPos(370) + 
        " " + this.__getColPos(630) + " " + this.__getRowPos(370) +
        " " + this.__getColPos(630) + " " + this.__getRowPos(340) + " Z");
		bottomObj1.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj1);*/
		
		/*var bottomObj2 = this.__paper.path(
		"M" + this.__getColPos(750) + " " + this.__getRowPos(350) + 
        " L" + this.__getColPos(770) + " " + this.__getRowPos(360) +
		" " + this.__getColPos(770) + " " + this.__getRowPos(370) +		
        " " + this.__getColPos(850) + " " + this.__getRowPos(370) +
		" " + this.__getColPos(850) + " " + this.__getRowPos(360) +
        " " + this.__getColPos(870) + " " + this.__getRowPos(350) + " Z");
		bottomObj2.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj2);*/
		
		/*var bottomObj3 = this.__paper.rect(this.__getColPos(1000), this.__getRowPos(340), 100, 30, 1);
		bottomObj3.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(bottomObj3);*/

    };
    
	ctor.prototype.__addWindowTop = function(col, row) {
        var mainWindow = this.rect(this.__getColPos(col), this.__getRowPos(row), 100, 40, 5);
        mainWindow.attr({fill:this.GRAY_COLOUR3,stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(mainWindow);
    };

	
    ctor.prototype.__addWindowDoor = function(col, row) {
        var door = this.rect(this.__getColPos(col), this.__getRowPos(row), 40, 165, 5);
        door.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(door);
        
        var window = this.rect(this.__getColPos(col + 10), this.__getRowPos(row + 20), 20, 65, 5);
        window.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(window);
    };
	
	ctor.prototype.__addCabinDoor = function(col, row) {
		var door = this.rect(this.__getColPos(col), this.__getRowPos(row), 40, 165, 5);
        door.attr({stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(door);
        
        var window = this.rect(this.__getColPos(col + 10), this.__getRowPos(row + 20), 20, 65, 5);
        window.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(window);
		
		var handrailLeft = this.rect(this.__getColPos(col-15), this.__getRowPos(row+25), 10, 140, 1);
		handrailLeft.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(handrailLeft);
		
		var handrailRight = this.rect(this.__getColPos(col+45), this.__getRowPos(row+25), 10, 140, 1);
		handrailRight.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(handrailRight);
	};

    ctor.prototype.__addWindow = function(col, row) {
        var mainWindow = this.rect(this.__getColPos(col), this.__getRowPos(row), 100, 70,5);
        mainWindow.attr({fill:this.GRAY_COLOUR3,stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(mainWindow);
    };
    
    ctor.prototype.__addWheel = function(col, row) {
        var wheel1 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 6);
        wheel1.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel1.toBack();
        this.__drawSet.push(wheel1);
		
		var wheel1Small = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 28);
        wheel1Small.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel1Small.toBack();
        this.__drawSet.push(wheel1Small);
        
        var wheel2 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 34);
        wheel2.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel2.toBack();
        this.__drawSet.push(wheel2);
        
        var wheel3 = this.__paper.circle(this.__getColPos(col), this.__getRowPos(row), 35);
        wheel3.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheel3.toBack();
        this.__drawSet.push(wheel3);
		
		var innerWheelFill1 = this.__paper.circle(this.__getColPos(1440), this.__getRowPos(360), 8);
        innerWheelFill1.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        innerWheelFill1.toBack();
        this.__drawSet.push(innerWheelFill1);
		
		var wheelFill1 = this.__paper.circle(this.__getColPos(1440), this.__getRowPos(360), 35);
        wheelFill1.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheelFill1.toBack();
        this.__drawSet.push(wheelFill1);
		
		var innerWheelFill2 = this.__paper.circle(this.__getColPos(1628), this.__getRowPos(360), 8);
        innerWheelFill2.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        innerWheelFill2.toBack();
        this.__drawSet.push(innerWheelFill2);
		
		var wheelFill2 = this.__paper.circle(this.__getColPos(1628), this.__getRowPos(360), 35);
        wheelFill2.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width:": this.STROKE_WIDTH});
        wheelFill2.toBack();
        this.__drawSet.push(wheelFill2);
    }
    
    ctor.prototype.__addWheelSet = function(col, row) {
        this.__addWheel(col - 10, row + 1);
        this.__addWheel(col + 178, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col - 28) + " " + this.__getRowPos(row-30) + 
		" " + this.__getColPos(col - 25) + " " + this.__getRowPos(row-20) + 
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) + 
		" " + this.__getColPos(col + 45) + " " + this.__getRowPos(row+12) +		
        " " + this.__getColPos(col + 120) + " " + this.__getRowPos(row+12) +
        " " + this.__getColPos(col + 170) + " " + this.__getRowPos(row-20) +                
		" " + this.__getColPos(col + 195) + " " + this.__getRowPos(row-20) + 
		" " + this.__getColPos(col + 198) + " " + this.__getRowPos(row-30) + 
        " Z");
        midWheelObj.attr({fill:this.GRAY_COLOUR3, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var whiteWheelBlackBrakes = this.rect(this.__getColPos(240), this.__getRowPos(375), 85, 16, 2);
        whiteWheelBlackBrakes.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(whiteWheelBlackBrakes)
		
		var midWheelSmallObj = this.__paper.path(
        "M" + this.__getColPos(col + 30) + " " + this.__getRowPos(row-25) + 
		" " + this.__getColPos(col + 50) + " " + this.__getRowPos(row-15) +		
        " " + this.__getColPos(col + 120) + " " + this.__getRowPos(row-15) +
        " " + this.__getColPos(col + 140) + " " + this.__getRowPos(row-25) +                
        " Z");
        midWheelSmallObj.attr({fill:this.BLACK_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelSmallObj);
		
		var midWheelSmallestObj1 = this.__paper.path(
        "M" + this.__getColPos(col + 35) + " " + this.__getRowPos(row-25) + 
		" " + this.__getColPos(col + 42) + " " + this.__getRowPos(row-22) +		
        " " + this.__getColPos(col + 42) + " " + this.__getRowPos(row-25) +                
        " Z");
        midWheelSmallestObj1.attr({fill:this.WHITE_COLOUR, stroke:this.WHITE_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelSmallestObj1);
		
		var midWheelSmallestObj2 = this.__paper.path(
        "M" + this.__getColPos(col + 135) + " " + this.__getRowPos(row-25) + 
		" " + this.__getColPos(col + 128) + " " + this.__getRowPos(row-22) +		
        " " + this.__getColPos(col + 128) + " " + this.__getRowPos(row-25) +                
        " Z");
        midWheelSmallestObj2.attr({fill:this.WHITE_COLOUR, stroke:this.WHITE_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelSmallestObj2);
		
		/*var  frontWheelObj = this.__paper.path(
		"M" + this.__getColPos(col-20) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col-40) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-65) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col-80) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-40) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col-25) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+190) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+205) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+240) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+225) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col+210) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+210) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+205) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+185) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);*/
    }
	
	ctor.prototype.__addHalfWheelSetFront = function(col, row) {
        this.__addWheel(col + 88, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row+10) +
		" L" + this.__getColPos(col + 19) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 39) + " " + this.__getRowPos(row) +		
        " " + this.__getColPos(col + 69) + " " + this.__getRowPos(row) +
        " " + this.__getColPos(col + 89) + " " + this.__getRowPos(row-20) +   
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +		
        " Z");
        midWheelObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" +  this.__getColPos(col) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+101) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+151) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+136) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col+121) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col+116) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col+96) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
	
	ctor.prototype.__addHalfWheelSetBack = function(col, row) {
        this.__addWheel(col - 10, row + 1);
        
        var midWheelObj = this.__paper.path(
        "M" + this.__getColPos(col) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col + 20) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 50) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col + 70) + " " + this.__getRowPos(row+10) +
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row+10) + 
		" " + this.__getColPos(col + 100) + " " + this.__getRowPos(row-20) +               
        " Z");
        midWheelObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
        this.__drawSet.push(midWheelObj);
		
		var  frontWheelObj = this.__paper.path(
		"M" + this.__getColPos(col-20) + " " + this.__getRowPos(row-20) + 
        " L" + this.__getColPos(col-40) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-45) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-65) + " " + this.__getRowPos(row+5) +
		" " + this.__getColPos(col-80) + " " + this.__getRowPos(row) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-10) +
		" " + this.__getColPos(col-60) + " " + this.__getRowPos(row-20) +
		" " + this.__getColPos(col-40) + " " + this.__getRowPos(row-36) +
		" " + this.__getColPos(col-25) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-40) +
		" " + this.__getColPos(col+100) + " " + this.__getRowPos(row-20) +
		" Z");
		frontWheelObj.attr({fill:this.WHITE_COLOUR, stroke:this.BLACK_COLOUR, "stroke-width": this.STROKE_WIDTH});
		this.__drawSet.push(frontWheelObj);
    }
    
    ctor.prototype.__addVehicleNumber = function () {
        if (!!this.__vehicleNumberObj) {
            this.__vehicleNumberObj.remove();
        }

        this.__vehicleNumberObj = this.__paper.text(
            this.__getScaled(893 + this.__xOffSet), 
            this.__getScaled(450 + this.__yOffSet) + (this.FONT_SIZE / 2), 
            "mBv7"  //this.vehicle.vehicleNumber
        );
        
        this.__vehicleNumberObj.attr({
            "font-size": this.FONT_SIZE,
            "text-anchor": "middle"
        });
    };
    
    return ctor;
})();