var nx = nx || {};
nx.ns = nx.ns || {};
nx.ns.diagram = nx.ns.diagram || {};

nx.ns.diagram.DiagramImpl = (function() {
    function ctor(unit, data, clickHandler) {
        nx.unit.diagram.Diagram.call(this, unit, data, clickHandler);
        
        this.__paper = null;
        this.__formation = null;
        this.__arrow = null;
        this.__leading = null;
    };

    /** Extend nx.unit.diagram.Provider */
    ctor.prototype = new nx.unit.diagram.Diagram();

    ctor.prototype.__buildFormation = function(scale) {
        var vehicles = this._unit.vehicles;
        
        vehicles.sort(function(a, b) {
            return a.position - b.position;
        });

        var xOffSet = 0;
        var direction = this.LEFT;
        var formation = [];
        for (var index = 0; index < vehicles.length; index++) {
            var vehicle = vehicles[index];
            
            switch(vehicle.vehicleType) {
            	case "mABK1":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mABK;
            		yOffSet = 0;
            		break;
            	case "mABk2":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mABK;
            		direction = this.RIGHT;
            		yOffSet = 0;
            		break;
            	case "mB1":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mBx;
            		yOffSet = 0;
            		break;
            	case "B1":
            		var vehicleDiagramClass = nx.ns.diagram.cab_Bx;
            		direction = this.RIGHT;
            		yOffSet = 0;
            		break;
            	case "mABk4":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mABK;
            		direction = this.RIGHT;
            		yOffSet = 0;
            		break;
            	case "mABk3":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mABK;
            		yOffSet = 0;
            		break;
            	case "B2":
            		var vehicleDiagramClass = nx.ns.diagram.cab_Bx;
            		yOffSet = 0;
            		direction = this.RIGHT;
            		break;
            	case "mB2":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mBx;
            		yOffSet = 0;
            		break;
            	case "mB3":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mBx;
            		yOffSet = 0;
            		break;
            	case "AB":
            		var vehicleDiagramClass = nx.ns.diagram.cab_mBx;
            		yOffSet = 0;
            		break;
            	case "mBvk1":
                    var vehicleDiagramClass = nx.ns.diagram.cab_mBvk1;
                    yOffSet = 130;
                    scale = 0.14;
                    break;
            	case "mBvk2":
                    var vehicleDiagramClass = nx.ns.diagram.cab_mBvk2;
                    yOffSet = 130;
                    scale = 0.14;
                    direction = this.RIGHT;
                    break;
            	case "ABv3_4":
                    var vehicleDiagramClass = nx.ns.diagram.cab_ABv3_4;
                    yOffSet = 130;
                    scale = 0.14;
                    direction = this.RIGHT;
                    break;
            	case "ABv5":
                    var vehicleDiagramClass = nx.ns.diagram.cab_ABv5;
                    yOffSet = 130;
                    scale = 0.14;
                    direction = this.RIGHT;
                    break;
            	case "ABv6":
                    var vehicleDiagramClass = nx.ns.diagram.cab_ABv6;
                    yOffSet = 130;
                    scale = 0.14;
                    direction = this.RIGHT;
                    break;
            	case "mBv7":
                    var vehicleDiagramClass = nx.ns.diagram.cab_mBv7;
                    yOffSet = 130;
                    scale = 0.14;
                    direction = this.RIGHT;
                    break;
            }
            
            var vehicleDiagram = new vehicleDiagramClass(this._data[this._unit.id], vehicle, this.__paper, scale, xOffSet, yOffSet, direction, this._clickHandler);
            formation.push(vehicleDiagram);
            xOffSet += vehicleDiagram.DIAGRAM_WIDTH;
        }
        
        return formation;
    };
    
    ctor.prototype.__setLeadingVehicle = function() {
        if (!!this.__leading) {
            this.__leading.remove();
        }

        var row = null;
        var col = null;
        var diameter = null;

        if (!!row && !!col && diameter) {
            this.__leading = this.__paper.circle(col, row, diameter);
            this.__leading.attr({
                'fill': '#00FF00', 
                'stroke-width': 1
            });
        }
    };
    
    ctor.prototype.__reset = function(container) {
        this.__formation = null;
        container.html('');
    };
    
    ctor.prototype.draw = function(container) {
        this.__reset(container);
        
        var scale = 0.17;
        
        this.__paper = Raphael(container.attr('id'), 1500, 130);
        
        this.__formation = this.__buildFormation(scale);
        
        for (var index = 0; index < this.__formation.length; index++) {
            this.__formation[index].draw();
        }
        
        this.__setLeadingVehicle();
    };

    ctor.prototype._update = function(data, unit) {
        this._setData(data);
        
        for (var index = 0; this.__formation != null && index < this.__formation.length; index++) {
            var diagram = this.__formation[index];
            
            diagram.setData(data[diagram.vehicle.unitId], unit.unitType);
        }
        
        if (this.__formation != null) {
            this.__setLeadingVehicle();
        }
    };

    return ctor;
})();

nx.unit.diagram.DiagramProvider.register(nx.ns.diagram.DiagramImpl);
