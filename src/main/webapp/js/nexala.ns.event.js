var nx = nx || {};
nx.event = nx.event || {};

// This function overwrite the one in SB which is the default handler
nx.event.eventDetailSuccessfulServiceRequestHandler = function(response) {
	var msg = '';
	if (response.statusCode != 'undefined' 
		&& response.statusCode == 'ERR') {
		msg += "<div class='serviceRequest__erMsgBlock'>" +
			   "<p><b>" + $.i18n('Error Code') + "</b>: " + response.statusMessageCode + "</p>" +
			   "<p><b>" + $.i18n('Error Text') + "</b>: " + response.statusMessageText + "</p>" +
			   "<p><b>" + $.i18n('Error Description') + "</b>: " + response.statusMessageDescription + "</p>" +
			   "</div>";
	} else {
		msg += "<p>" + $.i18n('Service Request created.') + "</p>" +
		    // if the long description field was longer than the limit
		    // warn the user with a message
		    (response.maxLimitReached ? "<p>" + $.i18n("LimitReachedLongDescription") + "</p>" : "");
	}
	return msg;
};