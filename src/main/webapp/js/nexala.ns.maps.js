var nx = nx || {};
nx.maps = nx.maps || {};
nx.fl = nx.fl || {};
nx.fl.maps = nx.fl.maps || {};
nx.maps.Detail = nx.maps.Detail || {};

nx.maps.Detail.prototype.__initializeDetailPanel = function() {
    var container = this.__config.container;
    
    container.css({ 'height': this.__config.maxHeight });
    
    var toolbarDef = ["<div id='mapInfoOverlayToolbar'>"];
    toolbarDef.push("<div id='mapInfoOverlayToolbar_Content' class = 'ui-widget-header'>&nbsp;</div>");
    
    toolbarDef.push("<div id='mapInfoOverlayToolbar_Close' style='display: ", (this.__frame._isSingleUnitMode() ? 'none' : 'block'), "'></div>");
    toolbarDef.push("</div>");
    
    container.append(toolbarDef.join(""));
    
    var mapDivDef = ["<div id='mapInfoOverlayMap'>"];
    mapDivDef.push("<div id='mapInfoOverlayMapContent'></div>");
    mapDivDef.push("</div>");
    
    container.append(mapDivDef.join(""));
    
    var detailsDef = ["<div id='mapInfoOverlayDetail'>"];
    detailsDef.push("<table id='mapInfoOverlayDetailTable'>");
    detailsDef.push("<tr><th id='unitDetailHeader' colspan='2' class='header ui-widget-header'><span id='unitDetailHeaderLbl'>Details</span><span id='detailLinks'></span></th></tr>");
    
    detailsDef.push("<tr><td width='110'><div class='dataHeader'>Last Updated Time</div></td><td><div id='lastUpdatedTime' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='headcodeLbl' class='dataHeader'>Headcode</div></td><td><div id='headcode' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='diagramLbl' class='dataHeader'>Diagram</div></td><td><div id='diagram' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='formationLbl' class='dataHeader'>Formation</div></td><td><div id='formation' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='activeVehicleLbl' class='dataHeader'>Leading Vehicle</div></td><td><div id='activeVehicle' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='gpsLbl' class='dataHeader'>GPS Coordinates</div></td><td><div id='gps' class='data'>&nbsp;</div></td></tr>");
    detailsDef.push("<tr><td><div id='speedLbl' class='dataHeader'>Speed</div></td><td><div id='speed' class='data'>&nbsp;</div></td></tr>");
    
    detailsDef.push("</table>");
    detailsDef.push("</div>");

    container.append(detailsDef.join(""));

    var eventsDef = ["<div id='mapInfoOverlayEvents'>"];
    eventsDef.push("<div id='mapInfoOverlayEventsContent'>");
    eventsDef.push("<div id='mapInfoOverlayEventsTabs'>");
    eventsDef.push("<ul>");
    eventsDef.push("<li><a href='#eventsTabLive'>Live</a></li>");
    eventsDef.push("<li><a href='#eventsTabRecent'>Recent</a></li>");
    eventsDef.push("</ul>");
    eventsDef.push("<div id='eventsTabLive'>");
    eventsDef.push("<table id='mapInfoOverlayEventsLiveTable'></table>");
    eventsDef.push("</div>");
    eventsDef.push("<div id='eventsTabRecent'>");
    eventsDef.push("<table id='mapInfoOverlayEventsRecentTable'></table>");
    eventsDef.push("</div>");
    eventsDef.push("</div>");
    eventsDef.push("</div>");
    eventsDef.push("</div>");
    
    container.append(eventsDef.join(""));
    
    var tabs = $('#mapInfoOverlayEventsTabs');
    tabs.tabs();
    
    this.__addLinkButtons();
};

nx.maps.Detail.prototype.__updateTableContent = function(set) {
    if (!set) {
        return;
    }

    //update detail header
    $('#mapInfoOverlayToolbar_Content').text(set.formation);

    var unitId = set.fleetFormationID.split(" ")[0]; // Get the first unit
    
    $('#unitDetailHeader').data('unitId', unitId).data('fleetId', set.fleetId);
    $('#lastUpdatedTime').text($.format('{date}', set.timestamp));
    $('#headcode').text($.format(set.headcode));
    $('#diagram').text($.format(set.diagram));
    $('#formation').text($.format(set.formation));
    $('#activeVehicle').text($.format(set.activeVehicle));
    $('#gps').text($.format(set.lat) + ', ' + $.format(set.lng));
    $('#speed').text($.format(set.speed));
};

nx.maps.Detail.prototype.__resetTable = function() {
    $('#lastUpdatedTime').text('');
    $('#headcode').text('');
    $('#diagram').text('');
    $('#formation').text('');
    $('#activeVehicle').text('');
    $('#gps').text('');
    $('#speed').text('');
};