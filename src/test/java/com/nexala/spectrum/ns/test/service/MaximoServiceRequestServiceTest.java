package com.nexala.spectrum.ns.test.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.dao.EventCommentDao;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.ns.db.beans.MaximoServiceRequestResponse;
import com.nexala.spectrum.ns.db.dao.MaximoServiceRequestDao;
import com.nexala.spectrum.ns.providers.EventProviderNS;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProvider;
import com.nexala.spectrum.ns.providers.MaximoServiceRequestProviderNS;
import com.nexala.spectrum.ns.service.MaximoServiceRequestService;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.view.Constants;
import com.trimble.rail.security.servlet.J2EUserProfileFacade;
import com.typesafe.config.Config;

public class MaximoServiceRequestServiceTest {

    /**
     * Test that if we get a successful message back from the ESB, 
     * that the service request ID is given back to the requester
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    @Test
    public void testCreateServiceRequestHappyPath() throws ClientProtocolException, IOException {
        String fleetId = "testFleet";
        
        Event testEvent = new Event();
        testEvent.setField(Spectrum.EVENT_ID, 1);
        
        EventProvider mockEventProvider = mock(EventProviderNS.class);
        when(mockEventProvider.getEventById(1L, false)).thenReturn(testEvent);
        
        Map<String, EventProvider> eventProviderMap = new HashMap<>();
        eventProviderMap.put(fleetId, mockEventProvider);
        
        EventDao mockEventDao = mock(EventDao.class);
        EventCommentDao mockEventCommentDao = mock(EventCommentDao.class);
        MaximoServiceRequestDao mockServiceRequestDao = mock(MaximoServiceRequestDao.class);
        
        HttpClient mockHttpClient = mock(HttpClient.class);
        HttpPost mockHttpPost = mock(HttpPost.class);
        HttpResponse mockResponse = mock(HttpResponse.class);
        ResponseHandler<String> mockResponseHandler = mock(ResponseHandler.class);
        
        when(mockHttpClient.execute(mockHttpPost)).thenReturn(mockResponse);
        when(mockResponseHandler.handleResponse(mockResponse)).thenReturn("{\"statusCode\": \"OK\",\"statusMessageCode\": \"ESB-RTM-010\",\"statusMessageText\": \"ServiceRequest data successfully processed\",\"statusMessageDetails\": \"\",\"statusMessageTimeStamp\": \"2016-12-17T09:30:47.001Z\",\"notificationNumber\": \"1001\"}");
        
        // create a mock app configuration
        ApplicationConfiguration appConf = mock(ApplicationConfiguration.class);
        
        Config conf = mock(Config.class);
        when(appConf.get()).thenReturn(conf);
        
        MaximoServiceRequestProvider mockMaximoProvider = new MaximoServiceRequestProviderNS(mockEventDao, mockEventCommentDao, mockServiceRequestDao, mockHttpClient, mockHttpPost, mockResponseHandler, appConf);

        Map<String, MaximoServiceRequestProvider> maximoProviderMap = new HashMap<>();
        maximoProviderMap.put(fleetId, mockMaximoProvider);
        
        MaximoServiceRequestService serviceUnderTest = new MaximoServiceRequestService(maximoProviderMap, eventProviderMap);
        
        Map<String, String> parameters = new HashMap<>();
        parameters.put("fleetId", fleetId);
        parameters.put("eventId", "1");
        parameters.put("rowVersion", "1");
        
        // mock the profile
        J2EUserProfileFacade customProfile = mock(J2EUserProfileFacade.class);
        when(customProfile.getLocale()).thenReturn(new Locale("en"));
        
        Set<String> customSet = new HashSet<String>();
        customSet.add(Constants.ADMINISTRATION_USERS_LIC_KEY);
        when(customProfile.getRoles()).thenReturn(customSet);
        
        when(customProfile.getUsername()).thenReturn("aUsername");
        
        // mock the request and response
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        HttpSession customSession = mock(HttpSession.class);
        
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return new Licence("SPECTRUM", customProfile);
            }
        }).when(customSession).getAttribute("licence");
        
        when(request.getSession()).thenReturn(customSession);
                
        MaximoServiceRequestResponse maximoResponse = serviceUnderTest.createServiceRequest(request, response, parameters);
        
        Assert.assertNotNull("Response should not be null", maximoResponse);
        Assert.assertEquals("Response should be 1001", "1001", maximoResponse.getServiceRequestId());
    }
    

    /**
     * Test that if we get an error message back from the ESB, 
     * that a RuntimeException is thrown
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    @Test(expected=RuntimeException.class)
    public void testCreateServiceRequestError() throws ClientProtocolException, IOException {
        String fleetId = "testFleet";
        
        Event testEvent = new Event();
        testEvent.setField(Spectrum.EVENT_ID, 1);
        
        EventProvider mockEventProvider = mock(EventProviderNS.class);
        when(mockEventProvider.getEventById(1L, false)).thenReturn(testEvent);
        
        Map<String, EventProvider> eventProviderMap = new HashMap<>();
        eventProviderMap.put(fleetId, mockEventProvider);
        
        EventDao mockEventDao = mock(EventDao.class);
        EventCommentDao mockEventCommentDao = mock(EventCommentDao.class);
        HttpClient mockHttpClient = mock(HttpClient.class);
        HttpPost mockHttpPost = mock(HttpPost.class);
        HttpResponse mockResponse = mock(HttpResponse.class);
        ResponseHandler<String> mockResponseHandler = mock(ResponseHandler.class);
        MaximoServiceRequestDao mockServiceRequestDao = mock(MaximoServiceRequestDao.class);
        
        when(mockHttpClient.execute(mockHttpPost)).thenReturn(mockResponse);
        when(mockResponseHandler.handleResponse(mockResponse)).thenReturn("{\"statusCode\": \"ERR\",\"statusMessageCode\": \"ESB-RTM-003\",\"statusMessageText\": \"Message validation failed\",\"statusMessageDetails\": \"<details>\",\"statusMessageTimeStamp\": \"2016-12-17T09:30:47.001Z\",\"notificationNumber\": \"\"}");
        
        // create a mock app configuration
        ApplicationConfiguration appConf = mock(ApplicationConfiguration.class);
        
        Config conf = mock(Config.class);
        when(appConf.get()).thenReturn(conf);
        
        MaximoServiceRequestProvider mockMaximoProvider = new MaximoServiceRequestProviderNS(mockEventDao, mockEventCommentDao, mockServiceRequestDao, mockHttpClient, mockHttpPost, mockResponseHandler, appConf);
        
        Map<String, MaximoServiceRequestProvider> maximoProviderMap = new HashMap<>();
        maximoProviderMap.put(fleetId, mockMaximoProvider);
        
        MaximoServiceRequestService serviceUnderTest = new MaximoServiceRequestService(maximoProviderMap, eventProviderMap);
        
        Map<String, String> parameters = new HashMap<>();
        parameters.put("fleetId", fleetId);
        parameters.put("eventId", "1");
        parameters.put("rowVersion", "1");
        
        // mock the profile
        J2EUserProfileFacade customProfile = mock(J2EUserProfileFacade.class);
        when(customProfile.getLocale()).thenReturn(new Locale("en"));
        
        Set<String> customSet = new HashSet<String>();
        customSet.add(Constants.ADMINISTRATION_USERS_LIC_KEY);
        when(customProfile.getRoles()).thenReturn(customSet);
        
        when(customProfile.getUsername()).thenReturn("aUsername");
        
        // mock the request and response
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        HttpSession customSession = mock(HttpSession.class);
        
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return new Licence("SPECTRUM", customProfile);
            }
        }).when(customSession).getAttribute("licence");
        
        when(request.getSession()).thenReturn(customSession);
                
        serviceUnderTest.createServiceRequest(request, response, parameters);
    }
    
}
